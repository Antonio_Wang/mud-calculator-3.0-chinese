Option Strict Off
Option Explicit On
Friend Class frmRheometerChange
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents txtRCksyp As System.Windows.Forms.TextBox
	Public WithEvents txtRCksup As System.Windows.Forms.TextBox
	Public WithEvents txtRCksx As System.Windows.Forms.TextBox
	Public WithEvents txtRCksms As System.Windows.Forms.TextBox
	Public WithEvents txtRCbhms As System.Windows.Forms.TextBox
	Public WithEvents txtRCmlms As System.Windows.Forms.TextBox
	Public WithEvents txtRCtext As System.Windows.Forms.TextBox
	Public WithEvents txtRCmlx As System.Windows.Forms.TextBox
	Public WithEvents txtRCmlk As System.Windows.Forms.TextBox
	Public WithEvents txtRCmln As System.Windows.Forms.TextBox
	Public WithEvents txtRCbhx As System.Windows.Forms.TextBox
	Public WithEvents txtRCbhpv As System.Windows.Forms.TextBox
	Public WithEvents txtRCbht As System.Windows.Forms.TextBox
	Public WithEvents lblRCks As System.Windows.Forms.Label
	Public WithEvents lblRCksyp As System.Windows.Forms.Label
	Public WithEvents lblRCksup As System.Windows.Forms.Label
	Public WithEvents lblRCksx As System.Windows.Forms.Label
	Public WithEvents lblRCksms As System.Windows.Forms.Label
	Public WithEvents Line3 As System.Windows.Forms.Label
	Public WithEvents lblRCbhms As System.Windows.Forms.Label
	Public WithEvents lblRCmlms As System.Windows.Forms.Label
	Public WithEvents Line2 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Line1 As System.Windows.Forms.Label
	Public WithEvents lblRCmlx As System.Windows.Forms.Label
	Public WithEvents lblRCmlk As System.Windows.Forms.Label
	Public WithEvents lblRCmln As System.Windows.Forms.Label
	Public WithEvents lblRCml As System.Windows.Forms.Label
	Public WithEvents lblRCbhx As System.Windows.Forms.Label
	Public WithEvents lblRCbhpv As System.Windows.Forms.Label
	Public WithEvents lblRCbht As System.Windows.Forms.Label
	Public WithEvents lblRCbh As System.Windows.Forms.Label
	Public WithEvents fraRcouput As System.Windows.Forms.GroupBox
	Public WithEvents _txtRCnr_5 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_5 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCnr_4 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_4 As System.Windows.Forms.TextBox
	Public WithEvents txtRCdata As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_3 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_2 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_1 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCsd_0 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCnr_3 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCnr_1 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCnr_2 As System.Windows.Forms.TextBox
	Public WithEvents _txtRCnr_0 As System.Windows.Forms.TextBox
	Public WithEvents txtRCs As System.Windows.Forms.TextBox
	Public WithEvents txtRCn As System.Windows.Forms.TextBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lblRCdata6 As System.Windows.Forms.Label
	Public WithEvents lblRCdata5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents lblRCdata4 As System.Windows.Forms.Label
	Public WithEvents lblRCdata3 As System.Windows.Forms.Label
	Public WithEvents lblRCdata2 As System.Windows.Forms.Label
	Public WithEvents lblRCdata1 As System.Windows.Forms.Label
	Public WithEvents lblRCs As System.Windows.Forms.Label
	Public WithEvents lblRCr As System.Windows.Forms.Label
	Public WithEvents lblRCsx As System.Windows.Forms.Label
	Public WithEvents lblRCnx As System.Windows.Forms.Label
	Public WithEvents fraRCinput As System.Windows.Forms.GroupBox
	Public WithEvents lblRCprogram As System.Windows.Forms.Label
	Public WithEvents frmRCprogram As System.Windows.Forms.GroupBox
	Public WithEvents txtRCnr As Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray
	Public WithEvents txtRCsd As Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRheometerChange))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtRCdata = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.fraRcouput = New System.Windows.Forms.GroupBox
        Me.txtRCksyp = New System.Windows.Forms.TextBox
        Me.txtRCksup = New System.Windows.Forms.TextBox
        Me.txtRCksx = New System.Windows.Forms.TextBox
        Me.txtRCksms = New System.Windows.Forms.TextBox
        Me.txtRCbhms = New System.Windows.Forms.TextBox
        Me.txtRCmlms = New System.Windows.Forms.TextBox
        Me.txtRCtext = New System.Windows.Forms.TextBox
        Me.txtRCmlx = New System.Windows.Forms.TextBox
        Me.txtRCmlk = New System.Windows.Forms.TextBox
        Me.txtRCmln = New System.Windows.Forms.TextBox
        Me.txtRCbhx = New System.Windows.Forms.TextBox
        Me.txtRCbhpv = New System.Windows.Forms.TextBox
        Me.txtRCbht = New System.Windows.Forms.TextBox
        Me.lblRCks = New System.Windows.Forms.Label
        Me.lblRCksyp = New System.Windows.Forms.Label
        Me.lblRCksup = New System.Windows.Forms.Label
        Me.lblRCksx = New System.Windows.Forms.Label
        Me.lblRCksms = New System.Windows.Forms.Label
        Me.Line3 = New System.Windows.Forms.Label
        Me.lblRCbhms = New System.Windows.Forms.Label
        Me.lblRCmlms = New System.Windows.Forms.Label
        Me.Line2 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Line1 = New System.Windows.Forms.Label
        Me.lblRCmlx = New System.Windows.Forms.Label
        Me.lblRCmlk = New System.Windows.Forms.Label
        Me.lblRCmln = New System.Windows.Forms.Label
        Me.lblRCml = New System.Windows.Forms.Label
        Me.lblRCbhx = New System.Windows.Forms.Label
        Me.lblRCbhpv = New System.Windows.Forms.Label
        Me.lblRCbht = New System.Windows.Forms.Label
        Me.lblRCbh = New System.Windows.Forms.Label
        Me.fraRCinput = New System.Windows.Forms.GroupBox
        Me._txtRCnr_5 = New System.Windows.Forms.TextBox
        Me._txtRCsd_5 = New System.Windows.Forms.TextBox
        Me._txtRCnr_4 = New System.Windows.Forms.TextBox
        Me._txtRCsd_4 = New System.Windows.Forms.TextBox
        Me._txtRCsd_3 = New System.Windows.Forms.TextBox
        Me._txtRCsd_2 = New System.Windows.Forms.TextBox
        Me._txtRCsd_1 = New System.Windows.Forms.TextBox
        Me._txtRCsd_0 = New System.Windows.Forms.TextBox
        Me._txtRCnr_3 = New System.Windows.Forms.TextBox
        Me._txtRCnr_1 = New System.Windows.Forms.TextBox
        Me._txtRCnr_2 = New System.Windows.Forms.TextBox
        Me._txtRCnr_0 = New System.Windows.Forms.TextBox
        Me.txtRCs = New System.Windows.Forms.TextBox
        Me.txtRCn = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblRCdata6 = New System.Windows.Forms.Label
        Me.lblRCdata5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblRCdata4 = New System.Windows.Forms.Label
        Me.lblRCdata3 = New System.Windows.Forms.Label
        Me.lblRCdata2 = New System.Windows.Forms.Label
        Me.lblRCdata1 = New System.Windows.Forms.Label
        Me.lblRCs = New System.Windows.Forms.Label
        Me.lblRCr = New System.Windows.Forms.Label
        Me.lblRCsx = New System.Windows.Forms.Label
        Me.lblRCnx = New System.Windows.Forms.Label
        Me.frmRCprogram = New System.Windows.Forms.GroupBox
        Me.lblRCprogram = New System.Windows.Forms.Label
        Me.txtRCnr = New Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray(Me.components)
        Me.txtRCsd = New Microsoft.VisualBasic.Compatibility.VB6.TextBoxArray(Me.components)
        Me.fraRcouput.SuspendLayout()
        Me.fraRCinput.SuspendLayout()
        Me.frmRCprogram.SuspendLayout()
        CType(Me.txtRCnr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRCsd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtRCdata
        '
        Me.txtRCdata.AcceptsReturn = True
        Me.txtRCdata.AutoSize = False
        Me.txtRCdata.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCdata.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCdata.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCdata.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCdata.Location = New System.Drawing.Point(347, 215)
        Me.txtRCdata.MaxLength = 0
        Me.txtRCdata.Name = "txtRCdata"
        Me.txtRCdata.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCdata.Size = New System.Drawing.Size(69, 20)
        Me.txtRCdata.TabIndex = 7
        Me.txtRCdata.Text = "6"
        Me.txtRCdata.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtRCdata, "为了计算的准确性，要求采集的数据组数为4－6组之间，其它数据将视为非法数据。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(19, 474)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(193, 61)
        Me.PicLogo.TabIndex = 63
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(320, 520)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 10
        Me.cmdExit.Text = "Exit"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(320, 492)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 9
        Me.cmdclear.Text = "Clean"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(320, 464)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 8
        Me.cmdok.Text = "Run"
        '
        'fraRcouput
        '
        Me.fraRcouput.BackColor = System.Drawing.SystemColors.Control
        Me.fraRcouput.Controls.Add(Me.txtRCksyp)
        Me.fraRcouput.Controls.Add(Me.txtRCksup)
        Me.fraRcouput.Controls.Add(Me.txtRCksx)
        Me.fraRcouput.Controls.Add(Me.txtRCksms)
        Me.fraRcouput.Controls.Add(Me.txtRCbhms)
        Me.fraRcouput.Controls.Add(Me.txtRCmlms)
        Me.fraRcouput.Controls.Add(Me.txtRCtext)
        Me.fraRcouput.Controls.Add(Me.txtRCmlx)
        Me.fraRcouput.Controls.Add(Me.txtRCmlk)
        Me.fraRcouput.Controls.Add(Me.txtRCmln)
        Me.fraRcouput.Controls.Add(Me.txtRCbhx)
        Me.fraRcouput.Controls.Add(Me.txtRCbhpv)
        Me.fraRcouput.Controls.Add(Me.txtRCbht)
        Me.fraRcouput.Controls.Add(Me.lblRCks)
        Me.fraRcouput.Controls.Add(Me.lblRCksyp)
        Me.fraRcouput.Controls.Add(Me.lblRCksup)
        Me.fraRcouput.Controls.Add(Me.lblRCksx)
        Me.fraRcouput.Controls.Add(Me.lblRCksms)
        Me.fraRcouput.Controls.Add(Me.Line3)
        Me.fraRcouput.Controls.Add(Me.lblRCbhms)
        Me.fraRcouput.Controls.Add(Me.lblRCmlms)
        Me.fraRcouput.Controls.Add(Me.Line2)
        Me.fraRcouput.Controls.Add(Me.Label9)
        Me.fraRcouput.Controls.Add(Me.Line1)
        Me.fraRcouput.Controls.Add(Me.lblRCmlx)
        Me.fraRcouput.Controls.Add(Me.lblRCmlk)
        Me.fraRcouput.Controls.Add(Me.lblRCmln)
        Me.fraRcouput.Controls.Add(Me.lblRCml)
        Me.fraRcouput.Controls.Add(Me.lblRCbhx)
        Me.fraRcouput.Controls.Add(Me.lblRCbhpv)
        Me.fraRcouput.Controls.Add(Me.lblRCbht)
        Me.fraRcouput.Controls.Add(Me.lblRCbh)
        Me.fraRcouput.ForeColor = System.Drawing.Color.Red
        Me.fraRcouput.Location = New System.Drawing.Point(506, 106)
        Me.fraRcouput.Name = "fraRcouput"
        Me.fraRcouput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraRcouput.Size = New System.Drawing.Size(378, 472)
        Me.fraRcouput.TabIndex = 35
        Me.fraRcouput.TabStop = False
        Me.fraRcouput.Text = "计算结果"
        '
        'txtRCksyp
        '
        Me.txtRCksyp.AcceptsReturn = True
        Me.txtRCksyp.AutoSize = False
        Me.txtRCksyp.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCksyp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCksyp.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCksyp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCksyp.Location = New System.Drawing.Point(192, 314)
        Me.txtRCksyp.MaxLength = 0
        Me.txtRCksyp.Name = "txtRCksyp"
        Me.txtRCksyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCksyp.Size = New System.Drawing.Size(107, 20)
        Me.txtRCksyp.TabIndex = 57
        Me.txtRCksyp.Text = ""
        Me.txtRCksyp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCksup
        '
        Me.txtRCksup.AcceptsReturn = True
        Me.txtRCksup.AutoSize = False
        Me.txtRCksup.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCksup.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCksup.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCksup.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCksup.Location = New System.Drawing.Point(192, 341)
        Me.txtRCksup.MaxLength = 0
        Me.txtRCksup.Name = "txtRCksup"
        Me.txtRCksup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCksup.Size = New System.Drawing.Size(107, 20)
        Me.txtRCksup.TabIndex = 56
        Me.txtRCksup.Text = ""
        Me.txtRCksup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCksx
        '
        Me.txtRCksx.AcceptsReturn = True
        Me.txtRCksx.AutoSize = False
        Me.txtRCksx.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCksx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCksx.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCksx.ForeColor = System.Drawing.Color.Red
        Me.txtRCksx.Location = New System.Drawing.Point(192, 369)
        Me.txtRCksx.MaxLength = 0
        Me.txtRCksx.Name = "txtRCksx"
        Me.txtRCksx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCksx.Size = New System.Drawing.Size(116, 20)
        Me.txtRCksx.TabIndex = 55
        Me.txtRCksx.Text = ""
        Me.txtRCksx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCksms
        '
        Me.txtRCksms.AcceptsReturn = True
        Me.txtRCksms.AutoSize = False
        Me.txtRCksms.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtRCksms.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCksms.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCksms.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCksms.Location = New System.Drawing.Point(192, 405)
        Me.txtRCksms.MaxLength = 0
        Me.txtRCksms.Name = "txtRCksms"
        Me.txtRCksms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCksms.Size = New System.Drawing.Size(176, 19)
        Me.txtRCksms.TabIndex = 54
        Me.txtRCksms.Text = ""
        Me.txtRCksms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCbhms
        '
        Me.txtRCbhms.AcceptsReturn = True
        Me.txtRCbhms.AutoSize = False
        Me.txtRCbhms.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtRCbhms.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCbhms.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCbhms.Location = New System.Drawing.Point(192, 116)
        Me.txtRCbhms.MaxLength = 0
        Me.txtRCbhms.Name = "txtRCbhms"
        Me.txtRCbhms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCbhms.Size = New System.Drawing.Size(145, 20)
        Me.txtRCbhms.TabIndex = 52
        Me.txtRCbhms.Text = ""
        Me.txtRCbhms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCmlms
        '
        Me.txtRCmlms.AcceptsReturn = True
        Me.txtRCmlms.AutoSize = False
        Me.txtRCmlms.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtRCmlms.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCmlms.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCmlms.Location = New System.Drawing.Point(192, 258)
        Me.txtRCmlms.MaxLength = 0
        Me.txtRCmlms.Name = "txtRCmlms"
        Me.txtRCmlms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCmlms.Size = New System.Drawing.Size(145, 20)
        Me.txtRCmlms.TabIndex = 49
        Me.txtRCmlms.Text = ""
        Me.txtRCmlms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCtext
        '
        Me.txtRCtext.AcceptsReturn = True
        Me.txtRCtext.AutoSize = False
        Me.txtRCtext.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtRCtext.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCtext.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCtext.ForeColor = System.Drawing.Color.Red
        Me.txtRCtext.Location = New System.Drawing.Point(192, 439)
        Me.txtRCtext.MaxLength = 0
        Me.txtRCtext.Name = "txtRCtext"
        Me.txtRCtext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCtext.Size = New System.Drawing.Size(145, 23)
        Me.txtRCtext.TabIndex = 17
        Me.txtRCtext.Text = ""
        Me.txtRCtext.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCmlx
        '
        Me.txtRCmlx.AcceptsReturn = True
        Me.txtRCmlx.AutoSize = False
        Me.txtRCmlx.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCmlx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCmlx.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCmlx.ForeColor = System.Drawing.Color.Red
        Me.txtRCmlx.Location = New System.Drawing.Point(192, 228)
        Me.txtRCmlx.MaxLength = 0
        Me.txtRCmlx.Name = "txtRCmlx"
        Me.txtRCmlx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCmlx.Size = New System.Drawing.Size(116, 20)
        Me.txtRCmlx.TabIndex = 16
        Me.txtRCmlx.Text = ""
        Me.txtRCmlx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCmlk
        '
        Me.txtRCmlk.AcceptsReturn = True
        Me.txtRCmlk.AutoSize = False
        Me.txtRCmlk.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCmlk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCmlk.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCmlk.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCmlk.Location = New System.Drawing.Point(192, 198)
        Me.txtRCmlk.MaxLength = 0
        Me.txtRCmlk.Name = "txtRCmlk"
        Me.txtRCmlk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCmlk.Size = New System.Drawing.Size(107, 20)
        Me.txtRCmlk.TabIndex = 15
        Me.txtRCmlk.Text = ""
        Me.txtRCmlk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCmln
        '
        Me.txtRCmln.AcceptsReturn = True
        Me.txtRCmln.AutoSize = False
        Me.txtRCmln.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCmln.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCmln.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCmln.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCmln.Location = New System.Drawing.Point(192, 168)
        Me.txtRCmln.MaxLength = 0
        Me.txtRCmln.Name = "txtRCmln"
        Me.txtRCmln.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCmln.Size = New System.Drawing.Size(107, 20)
        Me.txtRCmln.TabIndex = 14
        Me.txtRCmln.Text = ""
        Me.txtRCmln.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCbhx
        '
        Me.txtRCbhx.AcceptsReturn = True
        Me.txtRCbhx.AutoSize = False
        Me.txtRCbhx.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCbhx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCbhx.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCbhx.ForeColor = System.Drawing.Color.Red
        Me.txtRCbhx.Location = New System.Drawing.Point(192, 89)
        Me.txtRCbhx.MaxLength = 0
        Me.txtRCbhx.Name = "txtRCbhx"
        Me.txtRCbhx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCbhx.Size = New System.Drawing.Size(116, 20)
        Me.txtRCbhx.TabIndex = 13
        Me.txtRCbhx.Text = ""
        Me.txtRCbhx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCbhpv
        '
        Me.txtRCbhpv.AcceptsReturn = True
        Me.txtRCbhpv.AutoSize = False
        Me.txtRCbhpv.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCbhpv.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCbhpv.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCbhpv.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCbhpv.Location = New System.Drawing.Point(192, 61)
        Me.txtRCbhpv.MaxLength = 0
        Me.txtRCbhpv.Name = "txtRCbhpv"
        Me.txtRCbhpv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCbhpv.Size = New System.Drawing.Size(107, 20)
        Me.txtRCbhpv.TabIndex = 12
        Me.txtRCbhpv.Text = ""
        Me.txtRCbhpv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCbht
        '
        Me.txtRCbht.AcceptsReturn = True
        Me.txtRCbht.AutoSize = False
        Me.txtRCbht.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCbht.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCbht.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCbht.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCbht.Location = New System.Drawing.Point(192, 34)
        Me.txtRCbht.MaxLength = 0
        Me.txtRCbht.Name = "txtRCbht"
        Me.txtRCbht.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCbht.Size = New System.Drawing.Size(107, 20)
        Me.txtRCbht.TabIndex = 11
        Me.txtRCbht.Text = ""
        Me.txtRCbht.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRCks
        '
        Me.lblRCks.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCks.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCks.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCks.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCks.Location = New System.Drawing.Point(121, 293)
        Me.lblRCks.Name = "lblRCks"
        Me.lblRCks.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCks.Size = New System.Drawing.Size(145, 19)
        Me.lblRCks.TabIndex = 62
        Me.lblRCks.Text = "卡森模式"
        '
        'lblRCksyp
        '
        Me.lblRCksyp.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCksyp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCksyp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCksyp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCksyp.Location = New System.Drawing.Point(8, 319)
        Me.lblRCksyp.Name = "lblRCksyp"
        Me.lblRCksyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCksyp.Size = New System.Drawing.Size(154, 18)
        Me.lblRCksyp.TabIndex = 61
        Me.lblRCksyp.Text = "τc(Pa)"
        Me.lblRCksyp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCksup
        '
        Me.lblRCksup.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCksup.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCksup.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCksup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCksup.Location = New System.Drawing.Point(8, 346)
        Me.lblRCksup.Name = "lblRCksup"
        Me.lblRCksup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCksup.Size = New System.Drawing.Size(154, 18)
        Me.lblRCksup.TabIndex = 60
        Me.lblRCksup.Text = "η∞(mPa.s)"
        Me.lblRCksup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCksx
        '
        Me.lblRCksx.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCksx.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCksx.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCksx.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCksx.Location = New System.Drawing.Point(8, 374)
        Me.lblRCksx.Name = "lblRCksx"
        Me.lblRCksx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCksx.Size = New System.Drawing.Size(154, 18)
        Me.lblRCksx.TabIndex = 59
        Me.lblRCksx.Text = "相关系数"
        Me.lblRCksx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCksms
        '
        Me.lblRCksms.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCksms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCksms.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCksms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCksms.Location = New System.Drawing.Point(8, 401)
        Me.lblRCksms.Name = "lblRCksms"
        Me.lblRCksms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCksms.Size = New System.Drawing.Size(154, 18)
        Me.lblRCksms.TabIndex = 58
        Me.lblRCksms.Text = "卡森模式方程式："
        Me.lblRCksms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Line3
        '
        Me.Line3.BackColor = System.Drawing.SystemColors.Highlight
        Me.Line3.Location = New System.Drawing.Point(19, 284)
        Me.Line3.Name = "Line3"
        Me.Line3.Size = New System.Drawing.Size(302, 1)
        Me.Line3.TabIndex = 63
        '
        'lblRCbhms
        '
        Me.lblRCbhms.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCbhms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCbhms.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCbhms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCbhms.Location = New System.Drawing.Point(8, 121)
        Me.lblRCbhms.Name = "lblRCbhms"
        Me.lblRCbhms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCbhms.Size = New System.Drawing.Size(154, 18)
        Me.lblRCbhms.TabIndex = 51
        Me.lblRCbhms.Text = "宾汉模式方程式："
        Me.lblRCbhms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCmlms
        '
        Me.lblRCmlms.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCmlms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCmlms.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCmlms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCmlms.Location = New System.Drawing.Point(8, 258)
        Me.lblRCmlms.Name = "lblRCmlms"
        Me.lblRCmlms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCmlms.Size = New System.Drawing.Size(154, 18)
        Me.lblRCmlms.TabIndex = 50
        Me.lblRCmlms.Text = "幂律模式方程式："
        Me.lblRCmlms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Line2
        '
        Me.Line2.BackColor = System.Drawing.SystemColors.Highlight
        Me.Line2.Location = New System.Drawing.Point(19, 142)
        Me.Line2.Name = "Line2"
        Me.Line2.Size = New System.Drawing.Size(302, 1)
        Me.Line2.TabIndex = 64
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(8, 444)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(154, 18)
        Me.Label9.TabIndex = 44
        Me.Label9.Text = "钻井液流变模式："
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Line1
        '
        Me.Line1.BackColor = System.Drawing.SystemColors.Highlight
        Me.Line1.Location = New System.Drawing.Point(19, 435)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(302, 1)
        Me.Line1.TabIndex = 65
        '
        'lblRCmlx
        '
        Me.lblRCmlx.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCmlx.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCmlx.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCmlx.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCmlx.Location = New System.Drawing.Point(8, 228)
        Me.lblRCmlx.Name = "lblRCmlx"
        Me.lblRCmlx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCmlx.Size = New System.Drawing.Size(154, 18)
        Me.lblRCmlx.TabIndex = 43
        Me.lblRCmlx.Text = "相关系数"
        Me.lblRCmlx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCmlk
        '
        Me.lblRCmlk.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCmlk.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCmlk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCmlk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCmlk.Location = New System.Drawing.Point(8, 198)
        Me.lblRCmlk.Name = "lblRCmlk"
        Me.lblRCmlk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCmlk.Size = New System.Drawing.Size(154, 18)
        Me.lblRCmlk.TabIndex = 42
        Me.lblRCmlk.Text = "K (Pa.s^n)"
        Me.lblRCmlk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCmln
        '
        Me.lblRCmln.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCmln.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCmln.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCmln.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCmln.Location = New System.Drawing.Point(8, 168)
        Me.lblRCmln.Name = "lblRCmln"
        Me.lblRCmln.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCmln.Size = New System.Drawing.Size(154, 18)
        Me.lblRCmln.TabIndex = 41
        Me.lblRCmln.Text = "n"
        Me.lblRCmln.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCml
        '
        Me.lblRCml.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCml.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCml.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCml.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCml.Location = New System.Drawing.Point(121, 146)
        Me.lblRCml.Name = "lblRCml"
        Me.lblRCml.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCml.Size = New System.Drawing.Size(151, 19)
        Me.lblRCml.TabIndex = 40
        Me.lblRCml.Text = "幂律模式"
        '
        'lblRCbhx
        '
        Me.lblRCbhx.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCbhx.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCbhx.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCbhx.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCbhx.Location = New System.Drawing.Point(8, 94)
        Me.lblRCbhx.Name = "lblRCbhx"
        Me.lblRCbhx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCbhx.Size = New System.Drawing.Size(154, 18)
        Me.lblRCbhx.TabIndex = 39
        Me.lblRCbhx.Text = "相关系数"
        Me.lblRCbhx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCbhpv
        '
        Me.lblRCbhpv.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCbhpv.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCbhpv.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCbhpv.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCbhpv.Location = New System.Drawing.Point(8, 66)
        Me.lblRCbhpv.Name = "lblRCbhpv"
        Me.lblRCbhpv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCbhpv.Size = New System.Drawing.Size(154, 18)
        Me.lblRCbhpv.TabIndex = 38
        Me.lblRCbhpv.Text = "PV      (mPa.s)"
        Me.lblRCbhpv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCbht
        '
        Me.lblRCbht.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCbht.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCbht.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCbht.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCbht.Location = New System.Drawing.Point(8, 39)
        Me.lblRCbht.Name = "lblRCbht"
        Me.lblRCbht.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCbht.Size = New System.Drawing.Size(154, 18)
        Me.lblRCbht.TabIndex = 37
        Me.lblRCbht.Text = "Yield Point(Pa)"
        Me.lblRCbht.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRCbh
        '
        Me.lblRCbh.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCbh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCbh.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCbh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCbh.Location = New System.Drawing.Point(121, 17)
        Me.lblRCbh.Name = "lblRCbh"
        Me.lblRCbh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCbh.Size = New System.Drawing.Size(151, 19)
        Me.lblRCbh.TabIndex = 36
        Me.lblRCbh.Text = "宾汉模式"
        '
        'fraRCinput
        '
        Me.fraRCinput.BackColor = System.Drawing.SystemColors.Control
        Me.fraRCinput.Controls.Add(Me._txtRCnr_5)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_5)
        Me.fraRCinput.Controls.Add(Me._txtRCnr_4)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_4)
        Me.fraRCinput.Controls.Add(Me.txtRCdata)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_3)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_2)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_1)
        Me.fraRCinput.Controls.Add(Me._txtRCsd_0)
        Me.fraRCinput.Controls.Add(Me._txtRCnr_3)
        Me.fraRCinput.Controls.Add(Me._txtRCnr_1)
        Me.fraRCinput.Controls.Add(Me._txtRCnr_2)
        Me.fraRCinput.Controls.Add(Me._txtRCnr_0)
        Me.fraRCinput.Controls.Add(Me.txtRCs)
        Me.fraRCinput.Controls.Add(Me.txtRCn)
        Me.fraRCinput.Controls.Add(Me.Label2)
        Me.fraRCinput.Controls.Add(Me.lblRCdata6)
        Me.fraRCinput.Controls.Add(Me.lblRCdata5)
        Me.fraRCinput.Controls.Add(Me.Label4)
        Me.fraRCinput.Controls.Add(Me.lblRCdata4)
        Me.fraRCinput.Controls.Add(Me.lblRCdata3)
        Me.fraRCinput.Controls.Add(Me.lblRCdata2)
        Me.fraRCinput.Controls.Add(Me.lblRCdata1)
        Me.fraRCinput.Controls.Add(Me.lblRCs)
        Me.fraRCinput.Controls.Add(Me.lblRCr)
        Me.fraRCinput.Controls.Add(Me.lblRCsx)
        Me.fraRCinput.Controls.Add(Me.lblRCnx)
        Me.fraRCinput.ForeColor = System.Drawing.Color.Blue
        Me.fraRCinput.Location = New System.Drawing.Point(8, 106)
        Me.fraRCinput.Name = "fraRCinput"
        Me.fraRCinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraRCinput.Size = New System.Drawing.Size(490, 307)
        Me.fraRCinput.TabIndex = 26
        Me.fraRCinput.TabStop = False
        Me.fraRCinput.Text = "数据录入"
        '
        '_txtRCnr_5
        '
        Me._txtRCnr_5.AcceptsReturn = True
        Me._txtRCnr_5.AutoSize = False
        Me._txtRCnr_5.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_5, CType(5, Short))
        Me._txtRCnr_5.Location = New System.Drawing.Point(422, 60)
        Me._txtRCnr_5.MaxLength = 0
        Me._txtRCnr_5.Name = "_txtRCnr_5"
        Me._txtRCnr_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_5.Size = New System.Drawing.Size(59, 20)
        Me._txtRCnr_5.TabIndex = 23
        Me._txtRCnr_5.Text = "3"
        Me._txtRCnr_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_5
        '
        Me._txtRCsd_5.AcceptsReturn = True
        Me._txtRCsd_5.AutoSize = False
        Me._txtRCsd_5.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_5, CType(5, Short))
        Me._txtRCsd_5.Location = New System.Drawing.Point(422, 95)
        Me._txtRCsd_5.MaxLength = 0
        Me._txtRCsd_5.Name = "_txtRCsd_5"
        Me._txtRCsd_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_5.Size = New System.Drawing.Size(59, 19)
        Me._txtRCsd_5.TabIndex = 6
        Me._txtRCsd_5.Text = "0.5"
        Me._txtRCsd_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCnr_4
        '
        Me._txtRCnr_4.AcceptsReturn = True
        Me._txtRCnr_4.AutoSize = False
        Me._txtRCnr_4.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_4, CType(4, Short))
        Me._txtRCnr_4.Location = New System.Drawing.Point(355, 60)
        Me._txtRCnr_4.MaxLength = 0
        Me._txtRCnr_4.Name = "_txtRCnr_4"
        Me._txtRCnr_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_4.Size = New System.Drawing.Size(59, 20)
        Me._txtRCnr_4.TabIndex = 22
        Me._txtRCnr_4.Text = "6"
        Me._txtRCnr_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_4
        '
        Me._txtRCsd_4.AcceptsReturn = True
        Me._txtRCsd_4.AutoSize = False
        Me._txtRCsd_4.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_4, CType(4, Short))
        Me._txtRCsd_4.Location = New System.Drawing.Point(355, 95)
        Me._txtRCsd_4.MaxLength = 0
        Me._txtRCsd_4.Name = "_txtRCsd_4"
        Me._txtRCsd_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_4.Size = New System.Drawing.Size(59, 19)
        Me._txtRCsd_4.TabIndex = 5
        Me._txtRCsd_4.Text = "1"
        Me._txtRCsd_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_3
        '
        Me._txtRCsd_3.AcceptsReturn = True
        Me._txtRCsd_3.AutoSize = False
        Me._txtRCsd_3.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_3, CType(3, Short))
        Me._txtRCsd_3.Location = New System.Drawing.Point(288, 95)
        Me._txtRCsd_3.MaxLength = 0
        Me._txtRCsd_3.Name = "_txtRCsd_3"
        Me._txtRCsd_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_3.Size = New System.Drawing.Size(59, 19)
        Me._txtRCsd_3.TabIndex = 4
        Me._txtRCsd_3.Text = "5"
        Me._txtRCsd_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_2
        '
        Me._txtRCsd_2.AcceptsReturn = True
        Me._txtRCsd_2.AutoSize = False
        Me._txtRCsd_2.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_2, CType(2, Short))
        Me._txtRCsd_2.Location = New System.Drawing.Point(221, 95)
        Me._txtRCsd_2.MaxLength = 0
        Me._txtRCsd_2.Name = "_txtRCsd_2"
        Me._txtRCsd_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_2.Size = New System.Drawing.Size(59, 19)
        Me._txtRCsd_2.TabIndex = 3
        Me._txtRCsd_2.Text = "9"
        Me._txtRCsd_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_1
        '
        Me._txtRCsd_1.AcceptsReturn = True
        Me._txtRCsd_1.AutoSize = False
        Me._txtRCsd_1.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_1, CType(1, Short))
        Me._txtRCsd_1.Location = New System.Drawing.Point(154, 95)
        Me._txtRCsd_1.MaxLength = 0
        Me._txtRCsd_1.Name = "_txtRCsd_1"
        Me._txtRCsd_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_1.Size = New System.Drawing.Size(58, 19)
        Me._txtRCsd_1.TabIndex = 2
        Me._txtRCsd_1.Text = "12"
        Me._txtRCsd_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCsd_0
        '
        Me._txtRCsd_0.AcceptsReturn = True
        Me._txtRCsd_0.AutoSize = False
        Me._txtRCsd_0.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCsd_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCsd_0.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCsd_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCsd.SetIndex(Me._txtRCsd_0, CType(0, Short))
        Me._txtRCsd_0.Location = New System.Drawing.Point(86, 95)
        Me._txtRCsd_0.MaxLength = 0
        Me._txtRCsd_0.Name = "_txtRCsd_0"
        Me._txtRCsd_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCsd_0.Size = New System.Drawing.Size(59, 19)
        Me._txtRCsd_0.TabIndex = 1
        Me._txtRCsd_0.Text = "21"
        Me._txtRCsd_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCnr_3
        '
        Me._txtRCnr_3.AcceptsReturn = True
        Me._txtRCnr_3.AutoSize = False
        Me._txtRCnr_3.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_3, CType(3, Short))
        Me._txtRCnr_3.Location = New System.Drawing.Point(288, 60)
        Me._txtRCnr_3.MaxLength = 0
        Me._txtRCnr_3.Name = "_txtRCnr_3"
        Me._txtRCnr_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_3.Size = New System.Drawing.Size(59, 20)
        Me._txtRCnr_3.TabIndex = 21
        Me._txtRCnr_3.Text = "100"
        Me._txtRCnr_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCnr_1
        '
        Me._txtRCnr_1.AcceptsReturn = True
        Me._txtRCnr_1.AutoSize = False
        Me._txtRCnr_1.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_1, CType(1, Short))
        Me._txtRCnr_1.Location = New System.Drawing.Point(154, 60)
        Me._txtRCnr_1.MaxLength = 0
        Me._txtRCnr_1.Name = "_txtRCnr_1"
        Me._txtRCnr_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_1.Size = New System.Drawing.Size(58, 20)
        Me._txtRCnr_1.TabIndex = 19
        Me._txtRCnr_1.Text = "300"
        Me._txtRCnr_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCnr_2
        '
        Me._txtRCnr_2.AcceptsReturn = True
        Me._txtRCnr_2.AutoSize = False
        Me._txtRCnr_2.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_2, CType(2, Short))
        Me._txtRCnr_2.Location = New System.Drawing.Point(221, 60)
        Me._txtRCnr_2.MaxLength = 0
        Me._txtRCnr_2.Name = "_txtRCnr_2"
        Me._txtRCnr_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_2.Size = New System.Drawing.Size(59, 20)
        Me._txtRCnr_2.TabIndex = 20
        Me._txtRCnr_2.Text = "200"
        Me._txtRCnr_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_txtRCnr_0
        '
        Me._txtRCnr_0.AcceptsReturn = True
        Me._txtRCnr_0.AutoSize = False
        Me._txtRCnr_0.BackColor = System.Drawing.SystemColors.Window
        Me._txtRCnr_0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txtRCnr_0.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._txtRCnr_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCnr.SetIndex(Me._txtRCnr_0, CType(0, Short))
        Me._txtRCnr_0.Location = New System.Drawing.Point(86, 60)
        Me._txtRCnr_0.MaxLength = 0
        Me._txtRCnr_0.Name = "_txtRCnr_0"
        Me._txtRCnr_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txtRCnr_0.Size = New System.Drawing.Size(59, 20)
        Me._txtRCnr_0.TabIndex = 18
        Me._txtRCnr_0.Text = "600"
        Me._txtRCnr_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCs
        '
        Me.txtRCs.AcceptsReturn = True
        Me.txtRCs.AutoSize = False
        Me.txtRCs.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCs.Location = New System.Drawing.Point(348, 181)
        Me.txtRCs.MaxLength = 0
        Me.txtRCs.Name = "txtRCs"
        Me.txtRCs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCs.Size = New System.Drawing.Size(68, 19)
        Me.txtRCs.TabIndex = 25
        Me.txtRCs.Text = "0.511"
        Me.txtRCs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRCn
        '
        Me.txtRCn.AcceptsReturn = True
        Me.txtRCn.AutoSize = False
        Me.txtRCn.BackColor = System.Drawing.SystemColors.Window
        Me.txtRCn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRCn.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtRCn.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRCn.Location = New System.Drawing.Point(348, 146)
        Me.txtRCn.MaxLength = 0
        Me.txtRCn.Name = "txtRCn"
        Me.txtRCn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRCn.Size = New System.Drawing.Size(68, 20)
        Me.txtRCn.TabIndex = 24
        Me.txtRCn.Text = "1.703"
        Me.txtRCn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(10, 250)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(462, 48)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "为了计算的准确性，要求采集的数据组数为4－6组之间，其它输入数据将视为非法数据。本程序在中文输入法状态下的数据输入可能出错,请用户在英文输入法状态下输入数据。"
        '
        'lblRCdata6
        '
        Me.lblRCdata6.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata6.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata6.Location = New System.Drawing.Point(422, 26)
        Me.lblRCdata6.Name = "lblRCdata6"
        Me.lblRCdata6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata6.Size = New System.Drawing.Size(59, 18)
        Me.lblRCdata6.TabIndex = 48
        Me.lblRCdata6.Text = "6"
        Me.lblRCdata6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCdata5
        '
        Me.lblRCdata5.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata5.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata5.Location = New System.Drawing.Point(355, 26)
        Me.lblRCdata5.Name = "lblRCdata5"
        Me.lblRCdata5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata5.Size = New System.Drawing.Size(59, 18)
        Me.lblRCdata5.TabIndex = 47
        Me.lblRCdata5.Text = "5"
        Me.lblRCdata5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(19, 215)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(203, 19)
        Me.Label4.TabIndex = 45
        Me.Label4.Text = "数据组数"
        '
        'lblRCdata4
        '
        Me.lblRCdata4.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata4.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata4.Location = New System.Drawing.Point(288, 26)
        Me.lblRCdata4.Name = "lblRCdata4"
        Me.lblRCdata4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata4.Size = New System.Drawing.Size(59, 18)
        Me.lblRCdata4.TabIndex = 34
        Me.lblRCdata4.Text = "4"
        Me.lblRCdata4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCdata3
        '
        Me.lblRCdata3.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata3.Location = New System.Drawing.Point(221, 26)
        Me.lblRCdata3.Name = "lblRCdata3"
        Me.lblRCdata3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata3.Size = New System.Drawing.Size(59, 18)
        Me.lblRCdata3.TabIndex = 33
        Me.lblRCdata3.Text = "3"
        Me.lblRCdata3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCdata2
        '
        Me.lblRCdata2.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata2.Location = New System.Drawing.Point(154, 26)
        Me.lblRCdata2.Name = "lblRCdata2"
        Me.lblRCdata2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata2.Size = New System.Drawing.Size(58, 18)
        Me.lblRCdata2.TabIndex = 32
        Me.lblRCdata2.Text = "2"
        Me.lblRCdata2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCdata1
        '
        Me.lblRCdata1.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCdata1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCdata1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCdata1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCdata1.Location = New System.Drawing.Point(86, 26)
        Me.lblRCdata1.Name = "lblRCdata1"
        Me.lblRCdata1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCdata1.Size = New System.Drawing.Size(59, 18)
        Me.lblRCdata1.TabIndex = 31
        Me.lblRCdata1.Text = "1"
        Me.lblRCdata1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCs
        '
        Me.lblRCs.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCs.Location = New System.Drawing.Point(10, 95)
        Me.lblRCs.Name = "lblRCs"
        Me.lblRCs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCs.Size = New System.Drawing.Size(58, 18)
        Me.lblRCs.TabIndex = 30
        Me.lblRCs.Text = "读数"
        Me.lblRCs.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCr
        '
        Me.lblRCr.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCr.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCr.Location = New System.Drawing.Point(10, 52)
        Me.lblRCr.Name = "lblRCr"
        Me.lblRCr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCr.Size = New System.Drawing.Size(68, 36)
        Me.lblRCr.TabIndex = 29
        Me.lblRCr.Text = "转速r/min"
        Me.lblRCr.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRCsx
        '
        Me.lblRCsx.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCsx.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCsx.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCsx.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCsx.Location = New System.Drawing.Point(20, 181)
        Me.lblRCsx.Name = "lblRCsx"
        Me.lblRCsx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCsx.Size = New System.Drawing.Size(310, 19)
        Me.lblRCsx.TabIndex = 28
        Me.lblRCsx.Text = "读数 至 剪切应力Pa变换系数"
        '
        'lblRCnx
        '
        Me.lblRCnx.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCnx.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCnx.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCnx.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRCnx.Location = New System.Drawing.Point(20, 146)
        Me.lblRCnx.Name = "lblRCnx"
        Me.lblRCnx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCnx.Size = New System.Drawing.Size(310, 19)
        Me.lblRCnx.TabIndex = 27
        Me.lblRCnx.Text = "转速 至 剪切速率1/s变换系数"
        '
        'frmRCprogram
        '
        Me.frmRCprogram.BackColor = System.Drawing.SystemColors.Control
        Me.frmRCprogram.Controls.Add(Me.lblRCprogram)
        Me.frmRCprogram.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frmRCprogram.Location = New System.Drawing.Point(8, 8)
        Me.frmRCprogram.Name = "frmRCprogram"
        Me.frmRCprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frmRCprogram.Size = New System.Drawing.Size(875, 90)
        Me.frmRCprogram.TabIndex = 0
        Me.frmRCprogram.TabStop = False
        Me.frmRCprogram.Text = "程序说明"
        '
        'lblRCprogram
        '
        Me.lblRCprogram.BackColor = System.Drawing.SystemColors.Control
        Me.lblRCprogram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRCprogram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRCprogram.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblRCprogram.ForeColor = System.Drawing.Color.Blue
        Me.lblRCprogram.Location = New System.Drawing.Point(10, 17)
        Me.lblRCprogram.Name = "lblRCprogram"
        Me.lblRCprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRCprogram.Size = New System.Drawing.Size(854, 62)
        Me.lblRCprogram.TabIndex = 46
        Me.lblRCprogram.Text = "由于钻井液种类较多，体系复杂，因而有必要根据实际的钻井液体系，来选择合适的流变模式进行流变计算。此程序对现场使用的宾汉、幂律模式进行一定的数学处理后，对实测数据进" & _
        "行一元线性回归，给出各模式相关系数。基本公式中给出的修正系数，也只适用于仪器常数为1.703的范氏及仿范氏粘度计。"
        '
        'txtRCnr
        '
        '
        'txtRCsd
        '
        '
        'frmRheometerChange
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 586)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.fraRcouput)
        Me.Controls.Add(Me.fraRCinput)
        Me.Controls.Add(Me.frmRCprogram)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmRheometerChange"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液流变模式判断"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraRcouput.ResumeLayout(False)
        Me.fraRCinput.ResumeLayout(False)
        Me.frmRCprogram.ResumeLayout(False)
        CType(Me.txtRCnr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRCsd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmRheometerChange
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmRheometerChange
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmRheometerChange()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月9日1:56于青海省海西洲大柴旦镇红山参2井。加入纠错功能。
	'钻井液流变模式选择计算评价
	'该程序模块在我的2年探索中接出了果实，本人的数学功底差是导致进度慢的主要原因。正确的工具书也是关键。2006年8月9日鉴。王朝
	
	'---------------------
	'相关系数变量设置
	Dim sd(5) As Single
	Dim nr(5) As Single
	Dim mlms As String
	Dim mlt As String
	Dim mlr As String
	Dim bhr As String
	Dim bhj As String
	Dim bhms As String
	
	'卡森方程式显示定义
	Dim ksms As String
	Dim kst As String
	Dim ksr As String
	'---------------------------
	
	Dim Ixy As Single
	Dim Ixx As Single
	Dim Iyy As Single
	
	Dim lIxy As Single
	Dim lIxx As Single
	Dim lIyy As Single
	
	Dim kIxy As Single
	Dim kIxx As Single
	Dim kIyy As Single
	
	
	
	Dim H As Short
	Dim i As Short
	Dim nx As Single
	Dim sx As Single
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
        '---------------------
        '相关系数变量设置
        'Dim sd(0 To 5) As Single
        'Dim nr(0 To 5) As Single

        Dim X(5) As Single
        Dim Y(5) As Single
        Dim lx(5) As Single
        Dim ly(5) As Single
        Dim kX(5) As Single
        Dim kY(5) As Single

        Dim ax As Single
        Dim bx As Single
        Dim ay As Single
        Dim by As Single
        Dim exy As Single

        Dim lax As Single
        Dim lbx As Single
        Dim lay As Single
        Dim lby As Single
        Dim lexy As Single

        Dim kax As Single
        Dim kbx As Single
        Dim kay As Single
        Dim kby As Single
        Dim kexy As Single

        Dim bhx As Single
        Dim mlx As Single
        Dim ksx As Single
        '--------------------
        '流变模式相关参数计算变量设置
        '宾汉模式参数变量设置
        Dim r As Single '转速求和值－变量
        Dim s As Single '读数求和值－变量
        Dim rs As Single '读数与转速之乘积求和－变量
        Dim r2 As Single '转速的平方求和－变量
        Dim UP As Single '宾汉模式中塑性粘度参数变量
        Dim yp As Single '宾汉模式中动切力参数变量
        Dim N As Single
        Dim K As Single
        Dim kyp As Single
        Dim kup As Single

        H = Val(txtRCdata.Text)
        If txtRCdata.Text = "" Then
            MsgBox("请检查你输入的数据!软件需要4至6组数据。", MsgBoxStyle.Information, "钻井液流变模式判断")
            Exit Sub '避免在不输入数据的情况下，继续计算导致除数为零的情况。纠错功能解决。2006年8月8日王朝
        End If
        '-----------------------------------------------------------------------------
        '仪器常数的取值
        nx = Val(txtRCn.Text)
        sx = Val(txtRCs.Text)
        '-------------------------------------------------------------------------------

        '------------------------------------------------------------------------------
        i = 0 '清零为了检查纠错
        For i = 0 To H - 1
            sd(i) = Val(txtRCsd(i).Text)
            If txtRCsd(i).Text = "" Then
                MsgBox("请检查你输入的数据!", MsgBoxStyle.Information, "钻井液流变模式判断")
                Exit Sub '纠错功能解决。2006年8月8日王朝
            End If
            nr(i) = Val(txtRCnr(i).Text)
        Next i

        For i = 0 To H - 1
            '--------------------
            '宾汉模式   最小二乘法求参数：塑性粘度、动切力    公式准备  参见：【钻井泥浆与水泥流变学手册】P60  公式
            r = nr(i) + r
            s = sd(i) + s
            rs = nr(i) * sd(i) + rs
            r2 = nr(i) ^ 2 + r2

            '--------------------
            '相关系数计算数据准备

            X(i) = nx * nr(i) '转速转换为x 剪切速率 x=1.703*N
            Y(i) = sx * sd(i) '读数转换为y 剪切应力 y=0.511*S
            lx(i) = Math.Log10(X(i)) '幕律
            ly(i) = Math.Log10(Y(i))
            kX(i) = X(i) ^ 0.5 '卡森
            kY(i) = Y(i) ^ 0.5

            '宾汉模式计算 剪切速率的求和、应力求和、速率平方求和、应力平方求和、应力与速率乘积求和
            ax = X(i) + ax
            ay = Y(i) + ay
            bx = X(i) ^ 2 + bx
            by = Y(i) ^ 2 + by
            exy = X(i) * Y(i) + exy
            '幂律模式计算，等同上例
            lax = lx(i) + lax
            lay = ly(i) + lay
            lbx = lx(i) ^ 2 + lbx
            lby = ly(i) ^ 2 + lby
            lexy = lx(i) * ly(i) + lexy
            '卡森模式数据准备计算
            kax = kX(i) + kax
            kay = kY(i) + kay
            kbx = kX(i) ^ 2 + kbx
            kby = kY(i) ^ 2 + kby
            kexy = kX(i) * kY(i) + kexy

        Next i
        '宾汉模式数据准备
        Ixx = bx - (1 / H) * ax ^ 2
        Iyy = by - (1 / H) * ay ^ 2
        Ixy = exy - (1 / H) * (ax * ay)
        '幕律模式数据准备
        lIxx = lbx - (1 / H) * lax ^ 2
        lIyy = lby - (1 / H) * lay ^ 2
        lIxy = lexy - (1 / H) * (lax * lay)
        '卡森模式数据准备
        kIxx = kbx - (1 / H) * kax ^ 2
        kIyy = kby - (1 / H) * kay ^ 2
        kIxy = kexy - (1 / H) * (kax * kay)

        '----------------------------------
        '宾汉模式参数求解公式计算如下

        UP = 300 * (s * r - H * rs) / (r ^ 2 - H * r2)
        yp = 0.51 * (rs * r - s * r2) / (r ^ 2 - H * r2)

        txtRCbht.Text = VB6.Format(yp, "0.00")
        txtRCbhpv.Text = VB6.Format(UP, "0.00")
        '----------------------------------------------
        '幂例模式参数 n  k 公式计算如下
        N = (lay * lax - H * lexy) / (lax ^ 2 - H * lbx)
        K = System.Math.Exp(((lexy * lax - lay * lbx) / (lax ^ 2 - H * lbx)) / 0.4342945) '将log10转换为ln,EXP 函数是计算自然对数的 LN 函数的反函数

        txtRCmln.Text = VB6.Format(N, "0.00")
        txtRCmlk.Text = VB6.Format(K, "0.000")
        '---------------------------------------------
        '卡森模式参数 yp  up数据计算如下
        kup = (kay * kax - H * kexy) / (kax ^ 2 - H * kbx) * 100
        kyp = ((kexy * kax - kay * kbx) / (kax ^ 2 - H * kbx)) ^ 2

        txtRCksyp.Text = VB6.Format(kyp, "0.000")
        txtRCksup.Text = VB6.Format(kup, "0.00")

        '--------------------------------------------
        '幂律/宾汉模式公式显示模块如下
        mlt = "τ="
        mlr = "×γ^"
        bhj = " + "
        bhr = "×γ"
        kst = "τ^0.5="
        ksr = "×γ^0.5"

        mlms = mlt & txtRCmlk.Text & mlr & txtRCmln.Text
        bhms = mlt & txtRCbht.Text & bhj & txtRCbhpv.Text & bhr
        ksms = kst & txtRCksyp.Text & bhj & txtRCksup.Text & ksr


        txtRCbhms.Text = bhms
        txtRCmlms.Text = mlms
        txtRCksms.Text = ksms

        '-----------------------------------------------
        '各相关系数计算
        bhx = Ixy / (Ixx * Iyy) ^ 0.5
        mlx = lIxy / (lIxx * lIyy) ^ 0.5
        ksx = kIxy / (kIxx * kIyy) ^ 0.5
        '----------------------------------------------
        '相关系数结果显示
        txtRCbhx.Text = VB6.Format(bhx, "0.00000000")
        txtRCmlx.Text = VB6.Format(mlx, "0.00000000")
        txtRCksx.Text = VB6.Format(ksx, "0.00000000")
        '选择流变模式结论显示
        If bhx > mlx Then
            If bhx > ksx Then
                txtRCtext.Text = "Bingham"
                MsgBox("钻井液流变模式：Bingham！", MsgBoxStyle.Information, "钻井液流变模式判断")
            Else
                txtRCtext.Text = "Cassson"
                MsgBox("钻井液流变模式：Casson！", MsgBoxStyle.Information, "钻井液流变模式判断")
            End If
        Else
            If mlx > ksx Then
                txtRCtext.Text = "Power-law"
                MsgBox("钻井液流变模式：Power-law！", MsgBoxStyle.Information, "钻井液流变模式判断")
            Else
                txtRCtext.Text = "Casson"
                MsgBox("钻井液流变模式：Casson！", MsgBoxStyle.Information, "钻井液流变模式判断")
            End If
        End If
    End Sub

    Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
        Me.Close()
    End Sub
    Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
        '使用该方法清除数据，可能存在bug隐患。
        Dim i As Short

        For i = 0 To H - 1
            txtRCsd(i).Text = ""

        Next i
        txtRCdata.Text = ""
        txtRCbht.Text = ""
        txtRCbhpv.Text = ""
        txtRCmln.Text = ""
        txtRCmlk.Text = ""
        txtRCbhms.Text = ""
        txtRCmlms.Text = ""
        txtRCbhx.Text = ""
        txtRCmlx.Text = ""
        txtRCksup.Text = ""
        txtRCksyp.Text = ""
        txtRCksx.Text = ""
        txtRCksms.Text = ""
        txtRCtext.Text = ""

    End Sub

    Private Sub frmRheometerChange_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************

        '************************************************
        '17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
        '以下代码调用的为About中的公司logo 图片。

        '-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型


    End Sub



    '测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
    '****************************************************
    '***************************************************

    Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
        cmdclear.Visible = False '点击截图后设置命令按钮隐藏
        cmdExit.Visible = False
        cmdok.Visible = False
        'cmdHelpSolid.Visible = False '
        System.Windows.Forms.Application.DoEvents() '





        keybd_event(18, 0, 0, 0)
        keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
        System.Windows.Forms.Application.DoEvents()
        keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
        'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
        'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("Rheology.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("Rheology.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        cmdclear.Visible = True '
        cmdExit.Visible = True
        cmdok.Visible = True

    End Sub

    Private Sub txtRCdata_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRCdata.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        H = Val(txtRCdata.Text)
        If H < 4 Then
            MsgBox("Please check that you have entered datas！The software need from 4 to 6 sets datas.", MsgBoxStyle.Information, "API Mud Rheology")
            txtRCdata.Text = ""
            Exit Sub '避免在采集的数据点数较少或超过程序要求导致，数组下标越界。纠错功能解决。2006年8月8日王朝
        Else
            If H > 6 Then
                MsgBox("Please check that you have entered datas！The software need from 4 to 6 sets datas.", MsgBoxStyle.Information, "API Mud Rheology")
                txtRCdata.Text = ""
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtRCN_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRCn.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        nx = Val(txtRCn.Text)
    End Sub

    Private Sub txtRCnr_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRCnr.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim Index As Short = txtRCnr.GetIndex(eventSender)
        Dim i As Short

        For i = 0 To H - 1
            nr(i) = Val(txtRCnr(i).Text)
        Next i
    End Sub

    Private Sub txtRCs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRCs.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        sx = Val(txtRCs.Text)
    End Sub


    Private Sub txtRCsd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRCsd.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim Index As Short = txtRCsd.GetIndex(eventSender)
        Dim i As Short

        For i = 0 To H - 1

            sd(i) = Val(txtRCsd(i).Text)
        Next i
    End Sub

End Class