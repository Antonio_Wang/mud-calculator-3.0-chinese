Option Strict Off
Option Explicit On
Friend Class frmunitVelocity
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdhelp As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmkn2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmkn1 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmnmile2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmnmile1 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmmile2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitykmmile1 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymyds2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymyds1 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymftmin2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymftmin1 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymfts2 As System.Windows.Forms.Button
	Public WithEvents cmdVelocitymfts1 As System.Windows.Forms.Button
	Public WithEvents txtVelocitykn As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitynmile As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitymile As System.Windows.Forms.TextBox
	Public WithEvents txtVelocityyd As System.Windows.Forms.TextBox
	Public WithEvents txtVelocityft2 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocityft1 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitykm3 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitykm2 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitykm1 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitym3 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitym2 As System.Windows.Forms.TextBox
	Public WithEvents txtVelocitym1 As System.Windows.Forms.TextBox
	Public WithEvents lblVelocitykn As System.Windows.Forms.Label
	Public WithEvents lblVelocitynmileh As System.Windows.Forms.Label
	Public WithEvents lblVelocitymileh As System.Windows.Forms.Label
	Public WithEvents lblVelocityyds As System.Windows.Forms.Label
	Public WithEvents lblVelocityftmin As System.Windows.Forms.Label
	Public WithEvents lblVelocityfts As System.Windows.Forms.Label
	Public WithEvents lblVelocitykmh3 As System.Windows.Forms.Label
	Public WithEvents lblVelocitykmh2 As System.Windows.Forms.Label
	Public WithEvents lblVelocitykmh As System.Windows.Forms.Label
	Public WithEvents lblVelocityms2 As System.Windows.Forms.Label
	Public WithEvents lblVelocitymmin As System.Windows.Forms.Label
	Public WithEvents lblVelocityms As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdVelocitykmkn2 = New System.Windows.Forms.Button
        Me.cmdVelocitykmkn1 = New System.Windows.Forms.Button
        Me.cmdVelocitykmnmile2 = New System.Windows.Forms.Button
        Me.cmdVelocitykmnmile1 = New System.Windows.Forms.Button
        Me.cmdVelocitykmmile2 = New System.Windows.Forms.Button
        Me.cmdVelocitykmmile1 = New System.Windows.Forms.Button
        Me.cmdVelocitymyds2 = New System.Windows.Forms.Button
        Me.cmdVelocitymyds1 = New System.Windows.Forms.Button
        Me.cmdVelocitymftmin2 = New System.Windows.Forms.Button
        Me.cmdVelocitymftmin1 = New System.Windows.Forms.Button
        Me.cmdVelocitymfts2 = New System.Windows.Forms.Button
        Me.cmdVelocitymfts1 = New System.Windows.Forms.Button
        Me.txtVelocitynmile = New System.Windows.Forms.TextBox
        Me.txtVelocitymile = New System.Windows.Forms.TextBox
        Me.txtVelocityyd = New System.Windows.Forms.TextBox
        Me.lblVelocitykn = New System.Windows.Forms.Label
        Me.lblVelocitynmileh = New System.Windows.Forms.Label
        Me.lblVelocitymileh = New System.Windows.Forms.Label
        Me.lblVelocityyds = New System.Windows.Forms.Label
        Me.lblVelocityftmin = New System.Windows.Forms.Label
        Me.lblVelocityfts = New System.Windows.Forms.Label
        Me.lblVelocitykmh3 = New System.Windows.Forms.Label
        Me.lblVelocitykmh2 = New System.Windows.Forms.Label
        Me.lblVelocitykmh = New System.Windows.Forms.Label
        Me.lblVelocityms2 = New System.Windows.Forms.Label
        Me.lblVelocitymmin = New System.Windows.Forms.Label
        Me.lblVelocityms = New System.Windows.Forms.Label
        Me.cmdhelp = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtVelocitykn = New System.Windows.Forms.TextBox
        Me.txtVelocityft2 = New System.Windows.Forms.TextBox
        Me.txtVelocityft1 = New System.Windows.Forms.TextBox
        Me.txtVelocitykm3 = New System.Windows.Forms.TextBox
        Me.txtVelocitykm2 = New System.Windows.Forms.TextBox
        Me.txtVelocitykm1 = New System.Windows.Forms.TextBox
        Me.txtVelocitym3 = New System.Windows.Forms.TextBox
        Me.txtVelocitym2 = New System.Windows.Forms.TextBox
        Me.txtVelocitym1 = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'cmdVelocitykmkn2
        '
        Me.cmdVelocitykmkn2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmkn2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmkn2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmkn2.Location = New System.Drawing.Point(269, 198)
        Me.cmdVelocitykmkn2.Name = "cmdVelocitykmkn2"
        Me.cmdVelocitykmkn2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmkn2.Size = New System.Drawing.Size(49, 18)
        Me.cmdVelocitykmkn2.TabIndex = 35
        Me.cmdVelocitykmkn2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmkn2, "1km/h = 0.5400kn")
        '
        'cmdVelocitykmkn1
        '
        Me.cmdVelocitykmkn1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmkn1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmkn1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmkn1.Location = New System.Drawing.Point(182, 198)
        Me.cmdVelocitykmkn1.Name = "cmdVelocitykmkn1"
        Me.cmdVelocitykmkn1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmkn1.Size = New System.Drawing.Size(50, 18)
        Me.cmdVelocitykmkn1.TabIndex = 34
        Me.cmdVelocitykmkn1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmkn1, "1kn = 1.8520km/h")
        '
        'cmdVelocitykmnmile2
        '
        Me.cmdVelocitykmnmile2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmnmile2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmnmile2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmnmile2.Location = New System.Drawing.Point(269, 164)
        Me.cmdVelocitykmnmile2.Name = "cmdVelocitykmnmile2"
        Me.cmdVelocitykmnmile2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmnmile2.Size = New System.Drawing.Size(49, 18)
        Me.cmdVelocitykmnmile2.TabIndex = 33
        Me.cmdVelocitykmnmile2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmnmile2, "1km/h = 0.5400nmile/h")
        '
        'cmdVelocitykmnmile1
        '
        Me.cmdVelocitykmnmile1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmnmile1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmnmile1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmnmile1.Location = New System.Drawing.Point(182, 164)
        Me.cmdVelocitykmnmile1.Name = "cmdVelocitykmnmile1"
        Me.cmdVelocitykmnmile1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmnmile1.Size = New System.Drawing.Size(50, 18)
        Me.cmdVelocitykmnmile1.TabIndex = 32
        Me.cmdVelocitykmnmile1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmnmile1, "1nmile/h = 1.8520km/h")
        '
        'cmdVelocitykmmile2
        '
        Me.cmdVelocitykmmile2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmmile2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmmile2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmmile2.Location = New System.Drawing.Point(269, 129)
        Me.cmdVelocitykmmile2.Name = "cmdVelocitykmmile2"
        Me.cmdVelocitykmmile2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmmile2.Size = New System.Drawing.Size(49, 19)
        Me.cmdVelocitykmmile2.TabIndex = 31
        Me.cmdVelocitykmmile2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmmile2, "1km/h = 0.6214mile/h")
        '
        'cmdVelocitykmmile1
        '
        Me.cmdVelocitykmmile1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitykmmile1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitykmmile1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitykmmile1.Location = New System.Drawing.Point(182, 129)
        Me.cmdVelocitykmmile1.Name = "cmdVelocitykmmile1"
        Me.cmdVelocitykmmile1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitykmmile1.Size = New System.Drawing.Size(50, 19)
        Me.cmdVelocitykmmile1.TabIndex = 30
        Me.cmdVelocitykmmile1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitykmmile1, "1mile/h = 1.6093km/h")
        '
        'cmdVelocitymyds2
        '
        Me.cmdVelocitymyds2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymyds2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymyds2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymyds2.Location = New System.Drawing.Point(269, 95)
        Me.cmdVelocitymyds2.Name = "cmdVelocitymyds2"
        Me.cmdVelocitymyds2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymyds2.Size = New System.Drawing.Size(49, 18)
        Me.cmdVelocitymyds2.TabIndex = 29
        Me.cmdVelocitymyds2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymyds2, "1m/s = 1.0936yd/s")
        '
        'cmdVelocitymyds1
        '
        Me.cmdVelocitymyds1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymyds1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymyds1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymyds1.Location = New System.Drawing.Point(182, 95)
        Me.cmdVelocitymyds1.Name = "cmdVelocitymyds1"
        Me.cmdVelocitymyds1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymyds1.Size = New System.Drawing.Size(50, 18)
        Me.cmdVelocitymyds1.TabIndex = 28
        Me.cmdVelocitymyds1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymyds1, "1yd/s = 0.9144m/s")
        '
        'cmdVelocitymftmin2
        '
        Me.cmdVelocitymftmin2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymftmin2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymftmin2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymftmin2.Location = New System.Drawing.Point(269, 60)
        Me.cmdVelocitymftmin2.Name = "cmdVelocitymftmin2"
        Me.cmdVelocitymftmin2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymftmin2.Size = New System.Drawing.Size(49, 19)
        Me.cmdVelocitymftmin2.TabIndex = 27
        Me.cmdVelocitymftmin2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymftmin2, "1m/min = 3.2808ft/min")
        '
        'cmdVelocitymftmin1
        '
        Me.cmdVelocitymftmin1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymftmin1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymftmin1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymftmin1.Location = New System.Drawing.Point(182, 60)
        Me.cmdVelocitymftmin1.Name = "cmdVelocitymftmin1"
        Me.cmdVelocitymftmin1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymftmin1.Size = New System.Drawing.Size(50, 19)
        Me.cmdVelocitymftmin1.TabIndex = 26
        Me.cmdVelocitymftmin1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymftmin1, "1ft/min =  0.3048m/min")
        '
        'cmdVelocitymfts2
        '
        Me.cmdVelocitymfts2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymfts2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymfts2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymfts2.Location = New System.Drawing.Point(269, 26)
        Me.cmdVelocitymfts2.Name = "cmdVelocitymfts2"
        Me.cmdVelocitymfts2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymfts2.Size = New System.Drawing.Size(49, 18)
        Me.cmdVelocitymfts2.TabIndex = 25
        Me.cmdVelocitymfts2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymfts2, "1m/s = 3.2808ft/s")
        '
        'cmdVelocitymfts1
        '
        Me.cmdVelocitymfts1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdVelocitymfts1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdVelocitymfts1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdVelocitymfts1.Location = New System.Drawing.Point(182, 26)
        Me.cmdVelocitymfts1.Name = "cmdVelocitymfts1"
        Me.cmdVelocitymfts1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdVelocitymfts1.Size = New System.Drawing.Size(50, 18)
        Me.cmdVelocitymfts1.TabIndex = 24
        Me.cmdVelocitymfts1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdVelocitymfts1, "1ft/s =  0.3048m/s")
        '
        'txtVelocitynmile
        '
        Me.txtVelocitynmile.AcceptsReturn = True
        Me.txtVelocitynmile.AutoSize = False
        Me.txtVelocitynmile.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitynmile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitynmile.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitynmile.Location = New System.Drawing.Point(336, 164)
        Me.txtVelocitynmile.MaxLength = 0
        Me.txtVelocitynmile.Name = "txtVelocitynmile"
        Me.txtVelocitynmile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitynmile.Size = New System.Drawing.Size(68, 19)
        Me.txtVelocitynmile.TabIndex = 16
        Me.txtVelocitynmile.Text = "0.5400"
        Me.ToolTip1.SetToolTip(Me.txtVelocitynmile, "1km/h = 0.5400nmile/h")
        '
        'txtVelocitymile
        '
        Me.txtVelocitymile.AcceptsReturn = True
        Me.txtVelocitymile.AutoSize = False
        Me.txtVelocitymile.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitymile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitymile.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitymile.Location = New System.Drawing.Point(336, 129)
        Me.txtVelocitymile.MaxLength = 0
        Me.txtVelocitymile.Name = "txtVelocitymile"
        Me.txtVelocitymile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitymile.Size = New System.Drawing.Size(68, 20)
        Me.txtVelocitymile.TabIndex = 15
        Me.txtVelocitymile.Text = "0.6214"
        Me.ToolTip1.SetToolTip(Me.txtVelocitymile, "1km/h = 0.6214mile/h")
        '
        'txtVelocityyd
        '
        Me.txtVelocityyd.AcceptsReturn = True
        Me.txtVelocityyd.AutoSize = False
        Me.txtVelocityyd.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocityyd.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocityyd.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocityyd.Location = New System.Drawing.Point(336, 95)
        Me.txtVelocityyd.MaxLength = 0
        Me.txtVelocityyd.Name = "txtVelocityyd"
        Me.txtVelocityyd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocityyd.Size = New System.Drawing.Size(68, 19)
        Me.txtVelocityyd.TabIndex = 14
        Me.txtVelocityyd.Text = "1.0936"
        Me.ToolTip1.SetToolTip(Me.txtVelocityyd, "1m/s = 1.0936yd/s")
        '
        'lblVelocitykn
        '
        Me.lblVelocitykn.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitykn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitykn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitykn.Location = New System.Drawing.Point(413, 198)
        Me.lblVelocitykn.Name = "lblVelocitykn"
        Me.lblVelocitykn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitykn.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocitykn.TabIndex = 23
        Me.lblVelocitykn.Text = "kn(节)"
        Me.ToolTip1.SetToolTip(Me.lblVelocitykn, "节")
        '
        'lblVelocitynmileh
        '
        Me.lblVelocitynmileh.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitynmileh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitynmileh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitynmileh.Location = New System.Drawing.Point(413, 164)
        Me.lblVelocitynmileh.Name = "lblVelocitynmileh"
        Me.lblVelocitynmileh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitynmileh.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocitynmileh.TabIndex = 22
        Me.lblVelocitynmileh.Text = "nmile/h"
        Me.ToolTip1.SetToolTip(Me.lblVelocitynmileh, "海哩/小时")
        '
        'lblVelocitymileh
        '
        Me.lblVelocitymileh.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitymileh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitymileh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitymileh.Location = New System.Drawing.Point(413, 129)
        Me.lblVelocitymileh.Name = "lblVelocitymileh"
        Me.lblVelocitymileh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitymileh.Size = New System.Drawing.Size(68, 19)
        Me.lblVelocitymileh.TabIndex = 21
        Me.lblVelocitymileh.Text = "mile/h"
        Me.ToolTip1.SetToolTip(Me.lblVelocitymileh, "英哩/小时")
        '
        'lblVelocityyds
        '
        Me.lblVelocityyds.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocityyds.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocityyds.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocityyds.Location = New System.Drawing.Point(413, 95)
        Me.lblVelocityyds.Name = "lblVelocityyds"
        Me.lblVelocityyds.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocityyds.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocityyds.TabIndex = 20
        Me.lblVelocityyds.Text = "yd/s"
        Me.ToolTip1.SetToolTip(Me.lblVelocityyds, "码/秒")
        '
        'lblVelocityftmin
        '
        Me.lblVelocityftmin.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocityftmin.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocityftmin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocityftmin.Location = New System.Drawing.Point(413, 60)
        Me.lblVelocityftmin.Name = "lblVelocityftmin"
        Me.lblVelocityftmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocityftmin.Size = New System.Drawing.Size(68, 19)
        Me.lblVelocityftmin.TabIndex = 19
        Me.lblVelocityftmin.Text = "ft/min"
        Me.ToolTip1.SetToolTip(Me.lblVelocityftmin, "英尺/分钟")
        '
        'lblVelocityfts
        '
        Me.lblVelocityfts.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocityfts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocityfts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocityfts.Location = New System.Drawing.Point(413, 26)
        Me.lblVelocityfts.Name = "lblVelocityfts"
        Me.lblVelocityfts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocityfts.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocityfts.TabIndex = 18
        Me.lblVelocityfts.Text = "ft/s"
        Me.ToolTip1.SetToolTip(Me.lblVelocityfts, "英尺/秒")
        '
        'lblVelocitykmh3
        '
        Me.lblVelocitykmh3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitykmh3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitykmh3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitykmh3.Location = New System.Drawing.Point(96, 198)
        Me.lblVelocitykmh3.Name = "lblVelocitykmh3"
        Me.lblVelocitykmh3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitykmh3.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocitykmh3.TabIndex = 11
        Me.lblVelocitykmh3.Text = "km/h"
        Me.ToolTip1.SetToolTip(Me.lblVelocitykmh3, "千米/小时")
        '
        'lblVelocitykmh2
        '
        Me.lblVelocitykmh2.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitykmh2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitykmh2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitykmh2.Location = New System.Drawing.Point(96, 164)
        Me.lblVelocitykmh2.Name = "lblVelocitykmh2"
        Me.lblVelocitykmh2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitykmh2.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocitykmh2.TabIndex = 10
        Me.lblVelocitykmh2.Text = "km/h"
        Me.ToolTip1.SetToolTip(Me.lblVelocitykmh2, "千米/小时")
        '
        'lblVelocitykmh
        '
        Me.lblVelocitykmh.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitykmh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitykmh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitykmh.Location = New System.Drawing.Point(96, 129)
        Me.lblVelocitykmh.Name = "lblVelocitykmh"
        Me.lblVelocitykmh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitykmh.Size = New System.Drawing.Size(68, 19)
        Me.lblVelocitykmh.TabIndex = 9
        Me.lblVelocitykmh.Text = "km/h"
        Me.ToolTip1.SetToolTip(Me.lblVelocitykmh, "千米/小时")
        '
        'lblVelocityms2
        '
        Me.lblVelocityms2.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocityms2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocityms2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocityms2.Location = New System.Drawing.Point(96, 95)
        Me.lblVelocityms2.Name = "lblVelocityms2"
        Me.lblVelocityms2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocityms2.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocityms2.TabIndex = 8
        Me.lblVelocityms2.Text = "m/s"
        Me.ToolTip1.SetToolTip(Me.lblVelocityms2, "米/秒")
        '
        'lblVelocitymmin
        '
        Me.lblVelocitymmin.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitymmin.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitymmin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitymmin.Location = New System.Drawing.Point(96, 60)
        Me.lblVelocitymmin.Name = "lblVelocitymmin"
        Me.lblVelocitymmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitymmin.Size = New System.Drawing.Size(68, 19)
        Me.lblVelocitymmin.TabIndex = 7
        Me.lblVelocitymmin.Text = "m/min"
        Me.ToolTip1.SetToolTip(Me.lblVelocitymmin, "米/分钟")
        '
        'lblVelocityms
        '
        Me.lblVelocityms.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocityms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocityms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocityms.Location = New System.Drawing.Point(96, 26)
        Me.lblVelocityms.Name = "lblVelocityms"
        Me.lblVelocityms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocityms.Size = New System.Drawing.Size(68, 18)
        Me.lblVelocityms.TabIndex = 6
        Me.lblVelocityms.Text = "m/s"
        Me.ToolTip1.SetToolTip(Me.lblVelocityms, "米/秒")
        '
        'cmdhelp
        '
        Me.cmdhelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdhelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdhelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdhelp.Location = New System.Drawing.Point(307, 241)
        Me.cmdhelp.Name = "cmdhelp"
        Me.cmdhelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdhelp.Size = New System.Drawing.Size(88, 27)
        Me.cmdhelp.TabIndex = 37
        Me.cmdhelp.Text = "帮助"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(115, 241)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 36
        Me.cmdquit.Text = "退出"
        '
        'txtVelocitykn
        '
        Me.txtVelocitykn.AcceptsReturn = True
        Me.txtVelocitykn.AutoSize = False
        Me.txtVelocitykn.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitykn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitykn.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitykn.Location = New System.Drawing.Point(336, 198)
        Me.txtVelocitykn.MaxLength = 0
        Me.txtVelocitykn.Name = "txtVelocitykn"
        Me.txtVelocitykn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitykn.Size = New System.Drawing.Size(68, 20)
        Me.txtVelocitykn.TabIndex = 17
        Me.txtVelocitykn.Text = "0.5400"
        '
        'txtVelocityft2
        '
        Me.txtVelocityft2.AcceptsReturn = True
        Me.txtVelocityft2.AutoSize = False
        Me.txtVelocityft2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocityft2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocityft2.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocityft2.Location = New System.Drawing.Point(336, 60)
        Me.txtVelocityft2.MaxLength = 0
        Me.txtVelocityft2.Name = "txtVelocityft2"
        Me.txtVelocityft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocityft2.Size = New System.Drawing.Size(68, 20)
        Me.txtVelocityft2.TabIndex = 13
        Me.txtVelocityft2.Text = "3.2808"
        '
        'txtVelocityft1
        '
        Me.txtVelocityft1.AcceptsReturn = True
        Me.txtVelocityft1.AutoSize = False
        Me.txtVelocityft1.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocityft1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocityft1.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocityft1.Location = New System.Drawing.Point(336, 26)
        Me.txtVelocityft1.MaxLength = 0
        Me.txtVelocityft1.Name = "txtVelocityft1"
        Me.txtVelocityft1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocityft1.Size = New System.Drawing.Size(68, 19)
        Me.txtVelocityft1.TabIndex = 12
        Me.txtVelocityft1.Text = "3.2808"
        '
        'txtVelocitykm3
        '
        Me.txtVelocitykm3.AcceptsReturn = True
        Me.txtVelocitykm3.AutoSize = False
        Me.txtVelocitykm3.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitykm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitykm3.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitykm3.Location = New System.Drawing.Point(19, 198)
        Me.txtVelocitykm3.MaxLength = 0
        Me.txtVelocitykm3.Name = "txtVelocitykm3"
        Me.txtVelocitykm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitykm3.Size = New System.Drawing.Size(69, 20)
        Me.txtVelocitykm3.TabIndex = 5
        Me.txtVelocitykm3.Text = "1"
        '
        'txtVelocitykm2
        '
        Me.txtVelocitykm2.AcceptsReturn = True
        Me.txtVelocitykm2.AutoSize = False
        Me.txtVelocitykm2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitykm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitykm2.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitykm2.Location = New System.Drawing.Point(19, 164)
        Me.txtVelocitykm2.MaxLength = 0
        Me.txtVelocitykm2.Name = "txtVelocitykm2"
        Me.txtVelocitykm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitykm2.Size = New System.Drawing.Size(69, 19)
        Me.txtVelocitykm2.TabIndex = 4
        Me.txtVelocitykm2.Text = "1"
        '
        'txtVelocitykm1
        '
        Me.txtVelocitykm1.AcceptsReturn = True
        Me.txtVelocitykm1.AutoSize = False
        Me.txtVelocitykm1.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitykm1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitykm1.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitykm1.Location = New System.Drawing.Point(19, 129)
        Me.txtVelocitykm1.MaxLength = 0
        Me.txtVelocitykm1.Name = "txtVelocitykm1"
        Me.txtVelocitykm1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitykm1.Size = New System.Drawing.Size(69, 20)
        Me.txtVelocitykm1.TabIndex = 3
        Me.txtVelocitykm1.Text = "1"
        '
        'txtVelocitym3
        '
        Me.txtVelocitym3.AcceptsReturn = True
        Me.txtVelocitym3.AutoSize = False
        Me.txtVelocitym3.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitym3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitym3.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitym3.Location = New System.Drawing.Point(19, 95)
        Me.txtVelocitym3.MaxLength = 0
        Me.txtVelocitym3.Name = "txtVelocitym3"
        Me.txtVelocitym3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitym3.Size = New System.Drawing.Size(69, 19)
        Me.txtVelocitym3.TabIndex = 2
        Me.txtVelocitym3.Text = "1"
        '
        'txtVelocitym2
        '
        Me.txtVelocitym2.AcceptsReturn = True
        Me.txtVelocitym2.AutoSize = False
        Me.txtVelocitym2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitym2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitym2.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitym2.Location = New System.Drawing.Point(19, 60)
        Me.txtVelocitym2.MaxLength = 0
        Me.txtVelocitym2.Name = "txtVelocitym2"
        Me.txtVelocitym2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitym2.Size = New System.Drawing.Size(69, 20)
        Me.txtVelocitym2.TabIndex = 1
        Me.txtVelocitym2.Text = "1"
        '
        'txtVelocitym1
        '
        Me.txtVelocitym1.AcceptsReturn = True
        Me.txtVelocitym1.AutoSize = False
        Me.txtVelocitym1.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtVelocitym1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVelocitym1.ForeColor = System.Drawing.Color.Blue
        Me.txtVelocitym1.Location = New System.Drawing.Point(19, 26)
        Me.txtVelocitym1.MaxLength = 0
        Me.txtVelocitym1.Name = "txtVelocitym1"
        Me.txtVelocitym1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVelocitym1.Size = New System.Drawing.Size(69, 19)
        Me.txtVelocitym1.TabIndex = 0
        Me.txtVelocitym1.Text = "1"
        '
        'frmunitVelocity
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(495, 291)
        Me.Controls.Add(Me.cmdhelp)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdVelocitykmkn2)
        Me.Controls.Add(Me.cmdVelocitykmkn1)
        Me.Controls.Add(Me.cmdVelocitykmnmile2)
        Me.Controls.Add(Me.cmdVelocitykmnmile1)
        Me.Controls.Add(Me.cmdVelocitykmmile2)
        Me.Controls.Add(Me.cmdVelocitykmmile1)
        Me.Controls.Add(Me.cmdVelocitymyds2)
        Me.Controls.Add(Me.cmdVelocitymyds1)
        Me.Controls.Add(Me.cmdVelocitymftmin2)
        Me.Controls.Add(Me.cmdVelocitymftmin1)
        Me.Controls.Add(Me.cmdVelocitymfts2)
        Me.Controls.Add(Me.cmdVelocitymfts1)
        Me.Controls.Add(Me.txtVelocitykn)
        Me.Controls.Add(Me.txtVelocitynmile)
        Me.Controls.Add(Me.txtVelocitymile)
        Me.Controls.Add(Me.txtVelocityyd)
        Me.Controls.Add(Me.txtVelocityft2)
        Me.Controls.Add(Me.txtVelocityft1)
        Me.Controls.Add(Me.txtVelocitykm3)
        Me.Controls.Add(Me.txtVelocitykm2)
        Me.Controls.Add(Me.txtVelocitykm1)
        Me.Controls.Add(Me.txtVelocitym3)
        Me.Controls.Add(Me.txtVelocitym2)
        Me.Controls.Add(Me.txtVelocitym1)
        Me.Controls.Add(Me.lblVelocitykn)
        Me.Controls.Add(Me.lblVelocitynmileh)
        Me.Controls.Add(Me.lblVelocitymileh)
        Me.Controls.Add(Me.lblVelocityyds)
        Me.Controls.Add(Me.lblVelocityftmin)
        Me.Controls.Add(Me.lblVelocityfts)
        Me.Controls.Add(Me.lblVelocitykmh3)
        Me.Controls.Add(Me.lblVelocitykmh2)
        Me.Controls.Add(Me.lblVelocitykmh)
        Me.Controls.Add(Me.lblVelocityms2)
        Me.Controls.Add(Me.lblVelocitymmin)
        Me.Controls.Add(Me.lblVelocityms)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitVelocity"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "速度(Velocity)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitVelocity
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitVelocity
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitVelocity()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月7日22:57于青海省海西洲青海油田南八仙油区仙104井。8月1日加入纠错功能。
	'速度单位换算
	
	
	Dim m1 As Single
	Dim m2 As Single
	Dim m3 As Single
	Dim km1 As Single
	Dim km2 As Single
	Dim km3 As Single
	
	Dim ft1 As Single
	Dim ft2 As Single
	Dim yd As Single
	
	Dim mile As Single
	Dim nmile As Single
	Dim kn As Single
	
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdVelocitykmkn1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmkn1.Click '6-1
		kn = Val(txtVelocitykn.Text) '利用内置数据进行计算的补充语句
		If txtVelocitykn.Text = "" Or kn <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			km3 = kn * 1.852
		End If
		
		txtVelocitykm3.Text = CStr(km3)
	End Sub
	
	Private Sub cmdVelocitykmkn2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmkn2.Click '6-2
		km3 = Val(txtVelocitykm3.Text)
		If txtVelocitykm3.Text = "" Or km3 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			kn = km3 * 0.54
		End If
		
		txtVelocitykn.Text = CStr(kn)
	End Sub
	
	Private Sub cmdVelocitykmmile1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmmile1.Click '4-1
		mile = Val(txtVelocitymile.Text)
		If txtVelocitymile.Text = "" Or mile <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			km1 = mile * 1.6093
		End If
		
		txtVelocitykm1.Text = CStr(km1)
	End Sub
	
	Private Sub cmdVelocitykmmile2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmmile2.Click '4-2
		km1 = Val(txtVelocitykm1.Text)
		If txtVelocitykm1.Text = "" Or km1 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			mile = km1 * 0.6214
		End If
		
		txtVelocitymile.Text = CStr(mile)
	End Sub
	
	Private Sub cmdVelocitykmnmile1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmnmile1.Click '5-1
		nmile = Val(txtVelocitynmile.Text)
		If txtVelocitynmile.Text = "" Or nmile <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			km2 = nmile * 1.852
		End If
		
		txtVelocitykm2.Text = CStr(km2)
	End Sub
	
	Private Sub cmdVelocitykmnmile2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitykmnmile2.Click '5-2
		km2 = Val(txtVelocitykm2.Text)
		If txtVelocitykm2.Text = "" Or km2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			nmile = km2 * 0.54
		End If
		
		txtVelocitynmile.Text = CStr(nmile)
	End Sub
	
	Private Sub cmdVelocitymftmin1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymftmin1.Click '2-1
		ft2 = Val(txtVelocityft2.Text)
		If txtVelocityft2.Text = "" Or ft2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			m2 = ft2 * 0.3048
		End If
		
		txtVelocitym2.Text = CStr(m2)
	End Sub
	
	Private Sub cmdVelocitymftmin2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymftmin2.Click '2-2
		m2 = Val(txtVelocitym2.Text)
		If txtVelocitym2.Text = "" Or m2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			ft2 = m2 * 3.2808
		End If
		
		txtVelocityft2.Text = CStr(ft2)
	End Sub
	
	Private Sub cmdVelocitymfts1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymfts1.Click '1-1
		ft1 = Val(txtVelocityft1.Text)
		If txtVelocityft1.Text = "" Or ft1 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			m1 = ft1 * 0.3048
		End If
		
		txtVelocitym1.Text = CStr(m1)
	End Sub
	
	Private Sub cmdVelocitymfts2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymfts2.Click '1-2
		m1 = Val(txtVelocitym1.Text)
		If txtVelocitym1.Text = "" Or m1 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			ft1 = m1 * 3.2808
		End If
		
		txtVelocityft1.Text = CStr(ft1)
	End Sub
	
	Private Sub cmdVelocitymyds1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymyds1.Click '3-1
		yd = Val(txtVelocityyd.Text)
		If txtVelocityyd.Text = "" Or yd <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			m3 = yd * 0.9144
		End If
		
		txtVelocitym3.Text = CStr(m3)
	End Sub
	
	Private Sub cmdVelocitymyds2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdVelocitymyds2.Click '3-2
		m3 = Val(txtVelocitym3.Text)
		If txtVelocitym3.Text = "" Or m3 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			yd = m3 * 1.0936
		End If
		
		txtVelocityyd.Text = CStr(yd)
	End Sub
	
	Private Sub txtVelocityft1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocityft1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ft1 = Val(txtVelocityft1.Text)
	End Sub
	Private Sub txtVelocityft2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocityft2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ft2 = Val(txtVelocityft2.Text)
	End Sub
	
	Private Sub txtVelocitykm1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitykm1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		km1 = Val(txtVelocitykm1.Text)
	End Sub
	
	Private Sub txtVelocitykm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitykm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		km2 = Val(txtVelocitykm2.Text)
	End Sub
	
	Private Sub txtVelocitykm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitykm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		km3 = Val(txtVelocitykm3.Text)
	End Sub
	
	Private Sub txtVelocitykn_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitykn.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kn = Val(txtVelocitykn.Text)
	End Sub
	
	Private Sub txtVelocitym1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitym1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m1 = Val(txtVelocitym1.Text)
	End Sub
	Private Sub txtVelocitym2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitym2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m2 = Val(txtVelocitym2.Text)
	End Sub
	
	Private Sub txtVelocitym3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitym3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m3 = Val(txtVelocitym3.Text)
	End Sub
	
	Private Sub txtVelocitymile_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitymile.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mile = Val(txtVelocitymile.Text)
	End Sub
	
	Private Sub txtVelocitynmile_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocitynmile.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		nmile = Val(txtVelocitynmile.Text)
	End Sub
	
	Private Sub txtVelocityyd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVelocityyd.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		yd = Val(txtVelocityyd.Text)
	End Sub
End Class