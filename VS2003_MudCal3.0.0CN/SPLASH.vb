Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmSplash
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents tmrSplash As System.Windows.Forms.Timer
    '注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Public WithEvents lblCopyright As System.Windows.Forms.Label
    Public WithEvents lblCompany As System.Windows.Forms.Label
    Public WithEvents lblWarning As System.Windows.Forms.Label
    Public WithEvents lblVersion As System.Windows.Forms.Label
    Public WithEvents lblPlatform As System.Windows.Forms.Label
    Public WithEvents lblProductName As System.Windows.Forms.Label
    Public WithEvents lblLicenseTo As System.Windows.Forms.Label
    Public WithEvents lblCompanyProduct As System.Windows.Forms.Label
    Public WithEvents Frame1 As System.Windows.Forms.GroupBox
    Public WithEvents picLogo As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tmrSplash = New System.Windows.Forms.Timer(Me.components)
        Me.picLogo = New System.Windows.Forms.PictureBox
        Me.lblCopyright = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblPlatform = New System.Windows.Forms.Label
        Me.lblProductName = New System.Windows.Forms.Label
        Me.lblLicenseTo = New System.Windows.Forms.Label
        Me.lblCompanyProduct = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tmrSplash
        '
        Me.tmrSplash.Enabled = True
        Me.tmrSplash.Interval = 3000
        '
        'picLogo
        '
        Me.picLogo.BackColor = System.Drawing.SystemColors.HighlightText
        Me.picLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.picLogo.ForeColor = System.Drawing.Color.Red
        Me.picLogo.Location = New System.Drawing.Point(16, 96)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(125, 61)
        Me.picLogo.TabIndex = 0
        Me.picLogo.TabStop = False
        '
        'lblCopyright
        '
        Me.lblCopyright.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCopyright.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCopyright.Location = New System.Drawing.Point(136, 224)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCopyright.Size = New System.Drawing.Size(320, 17)
        Me.lblCopyright.TabIndex = 4
        Me.lblCopyright.Text = "本软件执行GPL－3.0开源许可协议 2003-2022 Wang Chao"
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompany.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompany.Location = New System.Drawing.Point(296, 200)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompany.Size = New System.Drawing.Size(161, 17)
        Me.lblCompany.TabIndex = 3
        Me.lblCompany.Text = "2012-03-31/设置开启"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblCompany.Visible = False
        '
        'lblWarning
        '
        Me.lblWarning.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblWarning.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWarning.Font = New System.Drawing.Font("宋体", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWarning.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWarning.Location = New System.Drawing.Point(10, 244)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWarning.Size = New System.Drawing.Size(451, 15)
        Me.lblWarning.TabIndex = 2
        Me.lblWarning.Text = "警告：本软件著作权属个人所有，并保留未授予的一切权利，受版权法的保护。"
        Me.lblWarning.Visible = False
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblVersion.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVersion.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVersion.Location = New System.Drawing.Point(256, 176)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVersion.Size = New System.Drawing.Size(29, 17)
        Me.lblVersion.TabIndex = 5
        Me.lblVersion.Text = "版本"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPlatform
        '
        Me.lblPlatform.AutoSize = True
        Me.lblPlatform.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblPlatform.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPlatform.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlatform.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPlatform.Location = New System.Drawing.Point(196, 144)
        Me.lblPlatform.Name = "lblPlatform"
        Me.lblPlatform.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPlatform.Size = New System.Drawing.Size(270, 17)
        Me.lblPlatform.TabIndex = 6
        Me.lblPlatform.Text = "平台：Windows9X操作系统/通过开启Visable显示"
        Me.lblPlatform.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPlatform.UseMnemonic = False
        Me.lblPlatform.Visible = False
        '
        'lblProductName
        '
        Me.lblProductName.AutoSize = True
        Me.lblProductName.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblProductName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProductName.Font = New System.Drawing.Font("Times New Roman", 32.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductName.ForeColor = System.Drawing.Color.Black
        Me.lblProductName.Location = New System.Drawing.Point(152, 88)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProductName.Size = New System.Drawing.Size(112, 53)
        Me.lblProductName.TabIndex = 8
        Me.lblProductName.Text = "产品"
        '
        'lblLicenseTo
        '
        Me.lblLicenseTo.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblLicenseTo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLicenseTo.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblLicenseTo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLicenseTo.Location = New System.Drawing.Point(128, 24)
        Me.lblLicenseTo.Name = "lblLicenseTo"
        Me.lblLicenseTo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLicenseTo.Size = New System.Drawing.Size(288, 20)
        Me.lblLicenseTo.TabIndex = 1
        Me.lblLicenseTo.Text = "授权:最终用户非商业性使用"
        Me.lblLicenseTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblLicenseTo.UseMnemonic = False
        '
        'lblCompanyProduct
        '
        Me.lblCompanyProduct.AutoSize = True
        Me.lblCompanyProduct.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lblCompanyProduct.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyProduct.Font = New System.Drawing.Font("宋体", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyProduct.ForeColor = System.Drawing.Color.Black
        Me.lblCompanyProduct.Location = New System.Drawing.Point(128, 56)
        Me.lblCompanyProduct.Name = "lblCompanyProduct"
        Me.lblCompanyProduct.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyProduct.Size = New System.Drawing.Size(303, 22)
        Me.lblCompanyProduct.TabIndex = 7
        Me.lblCompanyProduct.Text = "Zhongyuan Petroleum Engineering Ltd "
        Me.lblCompanyProduct.Visible = False
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Frame1.Controls.Add(Me.picLogo)
        Me.Frame1.Controls.Add(Me.lblCopyright)
        Me.Frame1.Controls.Add(Me.lblCompany)
        Me.Frame1.Controls.Add(Me.lblWarning)
        Me.Frame1.Controls.Add(Me.lblVersion)
        Me.Frame1.Controls.Add(Me.lblPlatform)
        Me.Frame1.Controls.Add(Me.lblProductName)
        Me.Frame1.Controls.Add(Me.lblLicenseTo)
        Me.Frame1.Controls.Add(Me.lblCompanyProduct)
        Me.Frame1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Red
        Me.Frame1.Location = New System.Drawing.Point(8, 4)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(472, 266)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        '
        'frmSplash
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 12)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(487, 278)
        Me.ControlBox = False
        Me.Controls.Add(Me.Frame1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("宋体", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(210, 138)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSplash"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmSplash
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmSplash
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmSplash()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	
	Private Sub frmSplash_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		frmSplash.DefInstance.Close()
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmSplash_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		

		'Dim WinPath As String, SoundFile As String
		'WinPath = WindowsDirectory()
		
		'这可以被更改为 ..Windows\Media\*.wav for WinXP
		'SoundFile = Dir(WinPath & "\Media\*.wav")

		
		'ComputerName = GetComputerNameWC


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.picLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.picLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************

        '**************************************
        '/
        '/定位窗口在父窗口内的显示位置，获取坐标数据；引入activate消除了显示过程中出现的窗口最大化最小化等控件显示。
        frmSplash.DefInstance.Left = (MDIFrmMud.DefInstance.Width - frmSplash.DefInstance.Width) / 2
        frmSplash.DefInstance.Top = (MDIFrmMud.DefInstance.Height - frmSplash.DefInstance.Height) / 3
        
        frmSplash.DefInstance.Activate()
        frmSplash.DefInstance.Show()
        

        '*********************

        FillSysInfo()

        'lblLicenseTo.Caption = "授权用户名：" & UserName()
        'lblVersion.Caption = "版本信息： " & App.Major & "." & App.Minor & "." & App.Revision

        lblLicenseTo.Text = "授权最终用户：" & UserName() & "非商业性使用" '2012年3月29日英语版本化
        'UPGRADE_ISSUE: App 属性 App.Revision 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
        lblVersion.Text = "Version： " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMajorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMinorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileBuildPart
        lblProductName.Text = System.Reflection.Assembly.GetExecutingAssembly.GetName.Name



	End Sub
	

    Private Sub tmrSplash_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrSplash.Tick
        If tmrSplash.Interval = 3000 Then

            frmSplash.DefInstance.Close()
            ' Me.Close()     '升级至VB.NET代码后，无法顺利调用关闭，修改测试。20220729
        End If
    End Sub
    '#########################################################################

    ' Return the user's name.
    Private Function UserName() As String
        Const UNLEN As Short = 256 ' Max user name length.
        Dim user_name As String
        Dim name_len As Integer

        user_name = Space(UNLEN + 1)
        name_len = Len(user_name)
        If GetUserName(user_name, name_len) = 0 Then
            UserName = "<unknown>"
        Else
            UserName = VB.Left(user_name, name_len - 1)
        End If
    End Function

    Private Sub FillSysInfo()
        Dim strTmp As String
        Dim myVer As MYVERSION



        '操作系统信息。
        Dim YourSystem As SystemInfo
        GetSystemInfo(YourSystem)

        'UPGRADE_WARNING: 未能解析对象 myVer 的默认属性。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"”
        myVer = WindowsVersion()

        If myVer.lMajorVersion = 4 Then
            If myVer.lExtraInfo = VER_PLATFORM_WIN32_NT Then
                strTmp = "Windows NT Ver : "
            ElseIf myVer.lExtraInfo = VER_PLATFORM_WIN32_WINDOWS Then
                strTmp = "Windows 95 Ver : "
            End If
        Else
            If myVer.lMajorVersion = 5 Then
                strTmp = "Windows XP "
                'lblPlatform.Caption = "应用平台：" & strTmp & myVer.lMajorVersion & _
                '"." & myVer.lMinorVersion & " Build " & myVer.lBuildNumber

                '以下内容为2012年3月29日英语化添加修改
                lblPlatform.Text = "Operation System:" & strTmp & myVer.lMajorVersion & "." & myVer.lMinorVersion & " Build " & myVer.lBuildNumber

            Else
                'strTmp = "Windows 版本 "
                'lblPlatform.Caption = "应用平台：" & strTmp & myVer.lMajorVersion & _
                '"." & myVer.lMinorVersion & "." & myVer.lBuildNumber


                '以下内容为2012年3月29日英语化添加修改
                strTmp = "Windows Ver "
                lblPlatform.Text = "Operation System:" & strTmp & myVer.lMajorVersion & "." & myVer.lMinorVersion & "." & myVer.lBuildNumber
            End If
        End If

        ' CPU 信息。
        'lblLicenseTo.Caption = "CPU: " & YourSystem.dwProcessorType

    End Sub

    Private Sub Frame1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Frame1.Click
        frmSplash.DefInstance.Close()
    End Sub
End Class