Option Strict Off
Option Explicit On
Friend Class frmunitVVel
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtVVelm3h As System.Windows.Forms.TextBox
	Public WithEvents txtVVelm3min As System.Windows.Forms.TextBox
	Public WithEvents txtVVelm3s As System.Windows.Forms.TextBox
	Public WithEvents txtVVellmin As System.Windows.Forms.TextBox
	Public WithEvents txtVVells As System.Windows.Forms.TextBox
	Public WithEvents txtVVelft3min As System.Windows.Forms.TextBox
	Public WithEvents txtVVelft3s As System.Windows.Forms.TextBox
	Public WithEvents txtVVelgalminus As System.Windows.Forms.TextBox
	Public WithEvents txtVVelgalminuk As System.Windows.Forms.TextBox
	Public WithEvents txtVVelbbld As System.Windows.Forms.TextBox
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents lblVVelinput As System.Windows.Forms.Label
	Public WithEvents lblVVelm3h As System.Windows.Forms.Label
	Public WithEvents lblVVelm3min As System.Windows.Forms.Label
	Public WithEvents lblVVelm3s As System.Windows.Forms.Label
	Public WithEvents lblVVellmin As System.Windows.Forms.Label
	Public WithEvents lblVVells As System.Windows.Forms.Label
	Public WithEvents lblVVelft3min As System.Windows.Forms.Label
	Public WithEvents lblVVelft3s As System.Windows.Forms.Label
	Public WithEvents lblVVelgalminus As System.Windows.Forms.Label
	Public WithEvents lblVVelgalminuk As System.Windows.Forms.Label
	Public WithEvents lblVVelbbld As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblVVelm3h = New System.Windows.Forms.Label
        Me.lblVVelm3min = New System.Windows.Forms.Label
        Me.lblVVelm3s = New System.Windows.Forms.Label
        Me.lblVVellmin = New System.Windows.Forms.Label
        Me.lblVVells = New System.Windows.Forms.Label
        Me.lblVVelft3min = New System.Windows.Forms.Label
        Me.lblVVelft3s = New System.Windows.Forms.Label
        Me.lblVVelgalminus = New System.Windows.Forms.Label
        Me.lblVVelgalminuk = New System.Windows.Forms.Label
        Me.lblVVelbbld = New System.Windows.Forms.Label
        Me.txtVVelm3h = New System.Windows.Forms.TextBox
        Me.txtVVelm3min = New System.Windows.Forms.TextBox
        Me.txtVVelm3s = New System.Windows.Forms.TextBox
        Me.txtVVellmin = New System.Windows.Forms.TextBox
        Me.txtVVells = New System.Windows.Forms.TextBox
        Me.txtVVelft3min = New System.Windows.Forms.TextBox
        Me.txtVVelft3s = New System.Windows.Forms.TextBox
        Me.txtVVelgalminus = New System.Windows.Forms.TextBox
        Me.txtVVelgalminuk = New System.Windows.Forms.TextBox
        Me.txtVVelbbld = New System.Windows.Forms.TextBox
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdClear = New System.Windows.Forms.Button
        Me.lblVVelinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblVVelm3h
        '
        Me.lblVVelm3h.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelm3h.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelm3h.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelm3h.Location = New System.Drawing.Point(115, 43)
        Me.lblVVelm3h.Name = "lblVVelm3h"
        Me.lblVVelm3h.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelm3h.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelm3h.TabIndex = 21
        Me.lblVVelm3h.Text = "m^3/h"
        Me.ToolTip1.SetToolTip(Me.lblVVelm3h, "立方米每时")
        '
        'lblVVelm3min
        '
        Me.lblVVelm3min.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelm3min.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelm3min.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelm3min.Location = New System.Drawing.Point(115, 78)
        Me.lblVVelm3min.Name = "lblVVelm3min"
        Me.lblVVelm3min.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelm3min.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelm3min.TabIndex = 20
        Me.lblVVelm3min.Text = "m^3/min"
        Me.ToolTip1.SetToolTip(Me.lblVVelm3min, "立方米每分")
        '
        'lblVVelm3s
        '
        Me.lblVVelm3s.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelm3s.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelm3s.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelm3s.Location = New System.Drawing.Point(115, 112)
        Me.lblVVelm3s.Name = "lblVVelm3s"
        Me.lblVVelm3s.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelm3s.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelm3s.TabIndex = 19
        Me.lblVVelm3s.Text = "m^3/s"
        Me.ToolTip1.SetToolTip(Me.lblVVelm3s, "立方米每秒")
        '
        'lblVVellmin
        '
        Me.lblVVellmin.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVellmin.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVellmin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVellmin.Location = New System.Drawing.Point(115, 146)
        Me.lblVVellmin.Name = "lblVVellmin"
        Me.lblVVellmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVellmin.Size = New System.Drawing.Size(69, 19)
        Me.lblVVellmin.TabIndex = 18
        Me.lblVVellmin.Text = "l/min"
        Me.ToolTip1.SetToolTip(Me.lblVVellmin, "升每分")
        '
        'lblVVells
        '
        Me.lblVVells.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVells.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVells.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVells.Location = New System.Drawing.Point(115, 181)
        Me.lblVVells.Name = "lblVVells"
        Me.lblVVells.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVells.Size = New System.Drawing.Size(69, 18)
        Me.lblVVells.TabIndex = 17
        Me.lblVVells.Text = "l/s"
        Me.ToolTip1.SetToolTip(Me.lblVVells, "升每秒")
        '
        'lblVVelft3min
        '
        Me.lblVVelft3min.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelft3min.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelft3min.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelft3min.Location = New System.Drawing.Point(278, 43)
        Me.lblVVelft3min.Name = "lblVVelft3min"
        Me.lblVVelft3min.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelft3min.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelft3min.TabIndex = 16
        Me.lblVVelft3min.Text = "ft^3/min"
        Me.ToolTip1.SetToolTip(Me.lblVVelft3min, "立方英尺每分")
        '
        'lblVVelft3s
        '
        Me.lblVVelft3s.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelft3s.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelft3s.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelft3s.Location = New System.Drawing.Point(278, 78)
        Me.lblVVelft3s.Name = "lblVVelft3s"
        Me.lblVVelft3s.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelft3s.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelft3s.TabIndex = 15
        Me.lblVVelft3s.Text = "ft^3/s"
        Me.ToolTip1.SetToolTip(Me.lblVVelft3s, "立方英尺每秒")
        '
        'lblVVelgalminus
        '
        Me.lblVVelgalminus.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelgalminus.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelgalminus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelgalminus.Location = New System.Drawing.Point(278, 112)
        Me.lblVVelgalminus.Name = "lblVVelgalminus"
        Me.lblVVelgalminus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelgalminus.Size = New System.Drawing.Size(88, 18)
        Me.lblVVelgalminus.TabIndex = 14
        Me.lblVVelgalminus.Text = "gal(US)/min"
        Me.ToolTip1.SetToolTip(Me.lblVVelgalminus, "加仑(美)每分")
        '
        'lblVVelgalminuk
        '
        Me.lblVVelgalminuk.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelgalminuk.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelgalminuk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelgalminuk.Location = New System.Drawing.Point(278, 146)
        Me.lblVVelgalminuk.Name = "lblVVelgalminuk"
        Me.lblVVelgalminuk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelgalminuk.Size = New System.Drawing.Size(88, 19)
        Me.lblVVelgalminuk.TabIndex = 13
        Me.lblVVelgalminuk.Text = "gal(UK)/min"
        Me.ToolTip1.SetToolTip(Me.lblVVelgalminuk, "加仑(英)每分")
        '
        'lblVVelbbld
        '
        Me.lblVVelbbld.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelbbld.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelbbld.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelbbld.Location = New System.Drawing.Point(278, 181)
        Me.lblVVelbbld.Name = "lblVVelbbld"
        Me.lblVVelbbld.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelbbld.Size = New System.Drawing.Size(69, 18)
        Me.lblVVelbbld.TabIndex = 12
        Me.lblVVelbbld.Text = "bbl/d"
        Me.ToolTip1.SetToolTip(Me.lblVVelbbld, "桶每日")
        '
        'txtVVelm3h
        '
        Me.txtVVelm3h.AcceptsReturn = True
        Me.txtVVelm3h.AutoSize = False
        Me.txtVVelm3h.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelm3h.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelm3h.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelm3h.Location = New System.Drawing.Point(19, 43)
        Me.txtVVelm3h.MaxLength = 0
        Me.txtVVelm3h.Name = "txtVVelm3h"
        Me.txtVVelm3h.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelm3h.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelm3h.TabIndex = 11
        Me.txtVVelm3h.Text = ""
        '
        'txtVVelm3min
        '
        Me.txtVVelm3min.AcceptsReturn = True
        Me.txtVVelm3min.AutoSize = False
        Me.txtVVelm3min.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelm3min.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelm3min.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelm3min.Location = New System.Drawing.Point(19, 78)
        Me.txtVVelm3min.MaxLength = 0
        Me.txtVVelm3min.Name = "txtVVelm3min"
        Me.txtVVelm3min.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelm3min.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelm3min.TabIndex = 10
        Me.txtVVelm3min.Text = ""
        '
        'txtVVelm3s
        '
        Me.txtVVelm3s.AcceptsReturn = True
        Me.txtVVelm3s.AutoSize = False
        Me.txtVVelm3s.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelm3s.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelm3s.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelm3s.Location = New System.Drawing.Point(19, 112)
        Me.txtVVelm3s.MaxLength = 0
        Me.txtVVelm3s.Name = "txtVVelm3s"
        Me.txtVVelm3s.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelm3s.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelm3s.TabIndex = 9
        Me.txtVVelm3s.Text = ""
        '
        'txtVVellmin
        '
        Me.txtVVellmin.AcceptsReturn = True
        Me.txtVVellmin.AutoSize = False
        Me.txtVVellmin.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVellmin.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVellmin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVellmin.Location = New System.Drawing.Point(19, 146)
        Me.txtVVellmin.MaxLength = 0
        Me.txtVVellmin.Name = "txtVVellmin"
        Me.txtVVellmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVellmin.Size = New System.Drawing.Size(88, 20)
        Me.txtVVellmin.TabIndex = 8
        Me.txtVVellmin.Text = ""
        '
        'txtVVells
        '
        Me.txtVVells.AcceptsReturn = True
        Me.txtVVells.AutoSize = False
        Me.txtVVells.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVells.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVells.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVells.Location = New System.Drawing.Point(19, 181)
        Me.txtVVells.MaxLength = 0
        Me.txtVVells.Name = "txtVVells"
        Me.txtVVells.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVells.Size = New System.Drawing.Size(88, 19)
        Me.txtVVells.TabIndex = 7
        Me.txtVVells.Text = ""
        '
        'txtVVelft3min
        '
        Me.txtVVelft3min.AcceptsReturn = True
        Me.txtVVelft3min.AutoSize = False
        Me.txtVVelft3min.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelft3min.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelft3min.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelft3min.Location = New System.Drawing.Point(182, 43)
        Me.txtVVelft3min.MaxLength = 0
        Me.txtVVelft3min.Name = "txtVVelft3min"
        Me.txtVVelft3min.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelft3min.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelft3min.TabIndex = 6
        Me.txtVVelft3min.Text = ""
        '
        'txtVVelft3s
        '
        Me.txtVVelft3s.AcceptsReturn = True
        Me.txtVVelft3s.AutoSize = False
        Me.txtVVelft3s.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelft3s.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelft3s.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelft3s.Location = New System.Drawing.Point(182, 78)
        Me.txtVVelft3s.MaxLength = 0
        Me.txtVVelft3s.Name = "txtVVelft3s"
        Me.txtVVelft3s.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelft3s.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelft3s.TabIndex = 5
        Me.txtVVelft3s.Text = ""
        '
        'txtVVelgalminus
        '
        Me.txtVVelgalminus.AcceptsReturn = True
        Me.txtVVelgalminus.AutoSize = False
        Me.txtVVelgalminus.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelgalminus.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelgalminus.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelgalminus.Location = New System.Drawing.Point(182, 112)
        Me.txtVVelgalminus.MaxLength = 0
        Me.txtVVelgalminus.Name = "txtVVelgalminus"
        Me.txtVVelgalminus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelgalminus.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelgalminus.TabIndex = 4
        Me.txtVVelgalminus.Text = ""
        '
        'txtVVelgalminuk
        '
        Me.txtVVelgalminuk.AcceptsReturn = True
        Me.txtVVelgalminuk.AutoSize = False
        Me.txtVVelgalminuk.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelgalminuk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelgalminuk.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelgalminuk.Location = New System.Drawing.Point(182, 146)
        Me.txtVVelgalminuk.MaxLength = 0
        Me.txtVVelgalminuk.Name = "txtVVelgalminuk"
        Me.txtVVelgalminuk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelgalminuk.Size = New System.Drawing.Size(88, 20)
        Me.txtVVelgalminuk.TabIndex = 3
        Me.txtVVelgalminuk.Text = ""
        '
        'txtVVelbbld
        '
        Me.txtVVelbbld.AcceptsReturn = True
        Me.txtVVelbbld.AutoSize = False
        Me.txtVVelbbld.BackColor = System.Drawing.SystemColors.Window
        Me.txtVVelbbld.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVVelbbld.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVVelbbld.Location = New System.Drawing.Point(182, 181)
        Me.txtVVelbbld.MaxLength = 0
        Me.txtVVelbbld.Name = "txtVVelbbld"
        Me.txtVVelbbld.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVVelbbld.Size = New System.Drawing.Size(88, 19)
        Me.txtVVelbbld.TabIndex = 2
        Me.txtVVelbbld.Text = ""
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(77, 215)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 1
        Me.cmdquit.Text = "退出"
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(230, 215)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(88, 27)
        Me.cmdClear.TabIndex = 0
        Me.cmdClear.Text = "清除"
        '
        'lblVVelinput
        '
        Me.lblVVelinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblVVelinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVVelinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVVelinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVVelinput.Location = New System.Drawing.Point(10, 9)
        Me.lblVVelinput.Name = "lblVVelinput"
        Me.lblVVelinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVVelinput.Size = New System.Drawing.Size(241, 18)
        Me.lblVVelinput.TabIndex = 22
        Me.lblVVelinput.Text = "输入体积流率单位换算数据："
        '
        'frmunitVVel
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(373, 254)
        Me.Controls.Add(Me.txtVVelm3h)
        Me.Controls.Add(Me.txtVVelm3min)
        Me.Controls.Add(Me.txtVVelm3s)
        Me.Controls.Add(Me.txtVVellmin)
        Me.Controls.Add(Me.txtVVells)
        Me.Controls.Add(Me.txtVVelft3min)
        Me.Controls.Add(Me.txtVVelft3s)
        Me.Controls.Add(Me.txtVVelgalminus)
        Me.Controls.Add(Me.txtVVelgalminuk)
        Me.Controls.Add(Me.txtVVelbbld)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.lblVVelinput)
        Me.Controls.Add(Me.lblVVelm3h)
        Me.Controls.Add(Me.lblVVelm3min)
        Me.Controls.Add(Me.lblVVelm3s)
        Me.Controls.Add(Me.lblVVellmin)
        Me.Controls.Add(Me.lblVVells)
        Me.Controls.Add(Me.lblVVelft3min)
        Me.Controls.Add(Me.lblVVelft3s)
        Me.Controls.Add(Me.lblVVelgalminus)
        Me.Controls.Add(Me.lblVVelgalminuk)
        Me.Controls.Add(Me.lblVVelbbld)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.Name = "frmunitVVel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "体积流率单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitVVel
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitVVel
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitVVel()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月1日11:23于青海省海西洲大柴旦红山参2井
	'体积速率单位换算
	
	Dim m3h As Single
	Dim m3min As Single
	Dim m3s As Single
	Dim lmin As Single
	Dim ls As Single
	Dim ft3min As Single
	Dim ft3s As Single
	Dim galminus As Single
	Dim galminuk As Single
	Dim bbld As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitVVel.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtVVelbbld_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelbbld.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		bbld = Val(txtVVelbbld.Text)
		
		m3h = bbld * 0.0066
		m3min = bbld * 0.00011041
		m3s = bbld * 0.0000018401
		lmin = bbld * 0.1104
		ls = bbld * 0.0018
		ft3min = bbld * 0.0039
		ft3s = bbld * 0.000064984
		galminus = bbld * 0.0292
		galminuk = bbld * 0.0243
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		'txtVVelbbld.Text = bbld
	End Sub
	
	Private Sub txtVVelft3min_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelft3min.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ft3min = Val(txtVVelft3min.Text)
		
		m3h = ft3min * 1.699
		m3min = ft3min * 0.0283
		m3s = ft3min * 0.0005
		lmin = ft3min * 28.3169
		ls = ft3min * 0.4719
		ft3s = ft3min * 0.0167
		galminus = ft3min * 7.4805
		galminuk = ft3min * 6.2288
		bbld = ft3min * 256.475
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		'txtVVelft3min.Text = ft3min
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	
	Private Sub txtVVelft3s_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelft3s.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ft3s = Val(txtVVelft3s.Text)
		
		m3h = ft3s * 101.9407
		m3min = ft3s * 1.699
		m3s = ft3s * 0.0283
		lmin = ft3s * 1699.011
		ls = ft3s * 28.3168
		ft3min = ft3s * 60#
		galminus = ft3s * 448.8312
		galminuk = ft3s * 373.73
		bbld = ft3s * 15388#
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		'txtVVelft3s.Text = ft3s
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	
	
	Private Sub txtVVelgalminuk_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelgalminuk.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		galminuk = Val(txtVVelgalminuk.Text)
		
		m3h = galminuk * 0.2728
		m3min = galminuk * 0.0045
		m3s = galminuk * 0.000075768
		lmin = galminuk * 4.5461
		ls = galminuk * 0.0758
		ft3min = galminuk * 0.1605
		ft3s = galminuk * 0.0027
		galminus = galminuk * 1.201
		bbld = galminuk * 41.1754
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		'txtVVelgalminuk.Text = galminuk
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	Private Sub txtVVelgalminus_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelgalminus.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		galminus = Val(txtVVelgalminus.Text)
		
		m3h = galminus * 0.2271
		m3min = galminus * 0.0038
		m3s = galminus * 0.00006309
		lmin = galminus * 3.7854
		ls = galminus * 0.0631
		ft3min = galminus * 0.1337
		ft3s = galminus * 0.0022
		galminuk = galminus * 0.8327
		bbld = galminus * 34.2857
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		'txtVVelgalminus.Text = galminus
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	Private Sub txtVVellmin_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVellmin.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lmin = Val(txtVVellmin.Text)
		
		m3h = lmin * 0.06
		m3min = lmin * 0.001
		m3s = lmin * 0.000016667
		ls = lmin * 0.0167
		ft3min = lmin * 0.0353
		ft3s = lmin * 0.0005886
		galminus = lmin * 0.2642
		galminuk = lmin * 0.22
		bbld = lmin * 9.0573
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		'txtVVellmin.Text = lmin
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	
	Private Sub txtVVells_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVells.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ls = Val(txtVVells.Text)
		
		m3h = ls * 3.6
		m3min = ls * 0.06
		m3s = ls * 0.001
		lmin = ls * 60#
		ft3min = ls * 2.1189
		ft3s = ls * 0.0353
		galminus = ls * 15.8503
		galminuk = ls * 13.1981
		bbld = ls * 543.4396
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		'txtVVells.Text = ls
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	Private Sub txtVVelm3h_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelm3h.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m3h = Val(txtVVelm3h.Text)
		
		m3min = m3h * 0.0167
		m3s = m3h * 0.00027778
		lmin = m3h * 16.6667
		ls = m3h * 0.2778
		ft3min = m3h * 0.5886
		ft3s = m3h * 0.0098
		galminus = m3h * 4.4209
		galminuk = m3h * 3.6663
		bbld = m3h * 150.9555
		
		'txtVVelm3h.Text = m3h
		txtVVelm3min.Text = CStr(m3min)
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
		
	End Sub
	
	Private Sub txtVVelm3min_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelm3min.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m3min = Val(txtVVelm3min.Text)
		
		m3h = m3min * 60
		m3s = m3min * 0.0167
		lmin = m3min * 1000#
		ls = m3min * 16.6667
		ft3min = m3min * 35.3147
		ft3s = m3min * 0.5886
		galminus = m3min * 264.172
		galminuk = m3min * 219.9692
		bbld = m3min * 9057.3272
		
		txtVVelm3h.Text = CStr(m3h)
		'txtVVelm3min.Text = m3min
		txtVVelm3s.Text = CStr(m3s)
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
	
	Private Sub txtVVelm3s_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVVelm3s.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		m3s = Val(txtVVelm3s.Text)
		
		m3h = m3s * 3600#
		m3min = m3s * 60#
		lmin = m3s * 60000#
		ls = m3s * 1000#
		ft3min = m3s * 2118.8797
		ft3s = m3s * 35.3147
		galminus = m3s * 15850#
		galminuk = m3s * 13198#
		bbld = m3s * 543440#
		
		txtVVelm3h.Text = CStr(m3h)
		txtVVelm3min.Text = CStr(m3min)
		'txtVVelm3s.Text = m3s
		txtVVellmin.Text = CStr(lmin)
		txtVVells.Text = CStr(ls)
		txtVVelft3min.Text = CStr(ft3min)
		txtVVelft3s.Text = CStr(ft3s)
		txtVVelgalminus.Text = CStr(galminus)
		txtVVelgalminuk.Text = CStr(galminuk)
		txtVVelbbld.Text = CStr(bbld)
	End Sub
End Class