Option Strict Off
Option Explicit On
Friend Class frmBVel
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents txtBVlc As System.Windows.Forms.TextBox
	Public WithEvents txtBVgvs As System.Windows.Forms.TextBox
	Public WithEvents txtBVvs As System.Windows.Forms.TextBox
	Public WithEvents txtBVhvs As System.Windows.Forms.TextBox
	Public WithEvents lblBVlc As System.Windows.Forms.Label
	Public WithEvents lblBVgvs As System.Windows.Forms.Label
	Public WithEvents lblBVvs As System.Windows.Forms.Label
	Public WithEvents lblBVhvs As System.Windows.Forms.Label
	Public WithEvents fraDataOuput As System.Windows.Forms.GroupBox
	Public WithEvents txtBVtyp As System.Windows.Forms.TextBox
	Public WithEvents txtBVupv As System.Windows.Forms.TextBox
	Public WithEvents txtBVq As System.Windows.Forms.TextBox
	Public WithEvents txtBVmw As System.Windows.Forms.TextBox
	Public WithEvents txtBVprc As System.Windows.Forms.TextBox
	Public WithEvents txtBVdrc As System.Windows.Forms.TextBox
	Public WithEvents txtBVdp As System.Windows.Forms.TextBox
	Public WithEvents txtBVdh As System.Windows.Forms.TextBox
	Public WithEvents lblBVtyp As System.Windows.Forms.Label
	Public WithEvents lblBVupv As System.Windows.Forms.Label
	Public WithEvents lblBVdh As System.Windows.Forms.Label
	Public WithEvents lblBVmw As System.Windows.Forms.Label
	Public WithEvents lblBVprc As System.Windows.Forms.Label
	Public WithEvents lblBVdrc As System.Windows.Forms.Label
	Public WithEvents lblBVdp As System.Windows.Forms.Label
	Public WithEvents lblBVq As System.Windows.Forms.Label
	Public WithEvents fraDataInput As System.Windows.Forms.GroupBox
	Public WithEvents lblBVprogram As System.Windows.Forms.Label
	Public WithEvents fraProgram As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picCuttingCarrying As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBVel))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picCuttingCarrying = New System.Windows.Forms.PictureBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.fraDataOuput = New System.Windows.Forms.GroupBox
        Me.txtBVlc = New System.Windows.Forms.TextBox
        Me.txtBVgvs = New System.Windows.Forms.TextBox
        Me.txtBVvs = New System.Windows.Forms.TextBox
        Me.txtBVhvs = New System.Windows.Forms.TextBox
        Me.lblBVlc = New System.Windows.Forms.Label
        Me.lblBVgvs = New System.Windows.Forms.Label
        Me.lblBVvs = New System.Windows.Forms.Label
        Me.lblBVhvs = New System.Windows.Forms.Label
        Me.fraDataInput = New System.Windows.Forms.GroupBox
        Me.txtBVtyp = New System.Windows.Forms.TextBox
        Me.txtBVupv = New System.Windows.Forms.TextBox
        Me.txtBVq = New System.Windows.Forms.TextBox
        Me.txtBVmw = New System.Windows.Forms.TextBox
        Me.txtBVprc = New System.Windows.Forms.TextBox
        Me.txtBVdrc = New System.Windows.Forms.TextBox
        Me.txtBVdp = New System.Windows.Forms.TextBox
        Me.txtBVdh = New System.Windows.Forms.TextBox
        Me.lblBVtyp = New System.Windows.Forms.Label
        Me.lblBVupv = New System.Windows.Forms.Label
        Me.lblBVdh = New System.Windows.Forms.Label
        Me.lblBVmw = New System.Windows.Forms.Label
        Me.lblBVprc = New System.Windows.Forms.Label
        Me.lblBVdrc = New System.Windows.Forms.Label
        Me.lblBVdp = New System.Windows.Forms.Label
        Me.lblBVq = New System.Windows.Forms.Label
        Me.fraProgram = New System.Windows.Forms.GroupBox
        Me.lblBVprogram = New System.Windows.Forms.Label
        Me.fraDataOuput.SuspendLayout()
        Me.fraDataInput.SuspendLayout()
        Me.fraProgram.SuspendLayout()
        Me.SuspendLayout()
        '
        'picCuttingCarrying
        '
        Me.picCuttingCarrying.Image = CType(resources.GetObject("picCuttingCarrying.Image"), System.Drawing.Image)
        Me.picCuttingCarrying.Location = New System.Drawing.Point(724, 14)
        Me.picCuttingCarrying.Name = "picCuttingCarrying"
        Me.picCuttingCarrying.Size = New System.Drawing.Size(160, 560)
        Me.picCuttingCarrying.TabIndex = 32
        Me.picCuttingCarrying.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picCuttingCarrying, "GMP10 Rig ,Talara Piura  Peru")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(584, 420)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 31
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(454, 442)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 11
        Me.cmdExit.Text = "Exit"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(454, 414)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 10
        Me.cmdclear.Text = "Clean"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(454, 386)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 9
        Me.cmdok.Text = "Run"
        '
        'fraDataOuput
        '
        Me.fraDataOuput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataOuput.Controls.Add(Me.txtBVlc)
        Me.fraDataOuput.Controls.Add(Me.txtBVgvs)
        Me.fraDataOuput.Controls.Add(Me.txtBVvs)
        Me.fraDataOuput.Controls.Add(Me.txtBVhvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVlc)
        Me.fraDataOuput.Controls.Add(Me.lblBVgvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVhvs)
        Me.fraDataOuput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataOuput.ForeColor = System.Drawing.Color.Red
        Me.fraDataOuput.Location = New System.Drawing.Point(392, 129)
        Me.fraDataOuput.Name = "fraDataOuput"
        Me.fraDataOuput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataOuput.Size = New System.Drawing.Size(318, 183)
        Me.fraDataOuput.TabIndex = 17
        Me.fraDataOuput.TabStop = False
        Me.fraDataOuput.Text = "计算结果"
        '
        'txtBVlc
        '
        Me.txtBVlc.AcceptsReturn = True
        Me.txtBVlc.AutoSize = False
        Me.txtBVlc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVlc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVlc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVlc.Location = New System.Drawing.Point(213, 148)
        Me.txtBVlc.MaxLength = 0
        Me.txtBVlc.Name = "txtBVlc"
        Me.txtBVlc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVlc.Size = New System.Drawing.Size(88, 20)
        Me.txtBVlc.TabIndex = 15
        Me.txtBVlc.Text = ""
        '
        'txtBVgvs
        '
        Me.txtBVgvs.AcceptsReturn = True
        Me.txtBVgvs.AutoSize = False
        Me.txtBVgvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVgvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVgvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVgvs.Location = New System.Drawing.Point(213, 108)
        Me.txtBVgvs.MaxLength = 0
        Me.txtBVgvs.Name = "txtBVgvs"
        Me.txtBVgvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVgvs.Size = New System.Drawing.Size(88, 20)
        Me.txtBVgvs.TabIndex = 14
        Me.txtBVgvs.Text = ""
        '
        'txtBVvs
        '
        Me.txtBVvs.AcceptsReturn = True
        Me.txtBVvs.AutoSize = False
        Me.txtBVvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVvs.Location = New System.Drawing.Point(213, 68)
        Me.txtBVvs.MaxLength = 0
        Me.txtBVvs.Name = "txtBVvs"
        Me.txtBVvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVvs.Size = New System.Drawing.Size(88, 20)
        Me.txtBVvs.TabIndex = 13
        Me.txtBVvs.Text = ""
        '
        'txtBVhvs
        '
        Me.txtBVhvs.AcceptsReturn = True
        Me.txtBVhvs.AutoSize = False
        Me.txtBVhvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVhvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVhvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVhvs.Location = New System.Drawing.Point(213, 28)
        Me.txtBVhvs.MaxLength = 0
        Me.txtBVhvs.Name = "txtBVhvs"
        Me.txtBVhvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVhvs.Size = New System.Drawing.Size(88, 20)
        Me.txtBVhvs.TabIndex = 12
        Me.txtBVhvs.Text = ""
        '
        'lblBVlc
        '
        Me.lblBVlc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVlc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVlc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVlc.Location = New System.Drawing.Point(10, 148)
        Me.lblBVlc.Name = "lblBVlc"
        Me.lblBVlc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVlc.Size = New System.Drawing.Size(196, 20)
        Me.lblBVlc.TabIndex = 29
        Me.lblBVlc.Text = "环空净化能力LC(无因次量)"
        '
        'lblBVgvs
        '
        Me.lblBVgvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVgvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVgvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVgvs.Location = New System.Drawing.Point(10, 108)
        Me.lblBVgvs.Name = "lblBVgvs"
        Me.lblBVgvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVgvs.Size = New System.Drawing.Size(196, 20)
        Me.lblBVgvs.TabIndex = 28
        Me.lblBVgvs.Text = "举升速度(m/s)"
        '
        'lblBVvs
        '
        Me.lblBVvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVvs.Location = New System.Drawing.Point(10, 68)
        Me.lblBVvs.Name = "lblBVvs"
        Me.lblBVvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVvs.Size = New System.Drawing.Size(196, 20)
        Me.lblBVvs.TabIndex = 27
        Me.lblBVvs.Text = "岩屑颗粒滑落速度(m/s)"
        '
        'lblBVhvs
        '
        Me.lblBVhvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVhvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVhvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVhvs.Location = New System.Drawing.Point(10, 28)
        Me.lblBVhvs.Name = "lblBVhvs"
        Me.lblBVhvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVhvs.Size = New System.Drawing.Size(196, 20)
        Me.lblBVhvs.TabIndex = 26
        Me.lblBVhvs.Text = "环空返速 (m/s)"
        '
        'fraDataInput
        '
        Me.fraDataInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataInput.Controls.Add(Me.txtBVtyp)
        Me.fraDataInput.Controls.Add(Me.txtBVupv)
        Me.fraDataInput.Controls.Add(Me.txtBVq)
        Me.fraDataInput.Controls.Add(Me.txtBVmw)
        Me.fraDataInput.Controls.Add(Me.txtBVprc)
        Me.fraDataInput.Controls.Add(Me.txtBVdrc)
        Me.fraDataInput.Controls.Add(Me.txtBVdp)
        Me.fraDataInput.Controls.Add(Me.txtBVdh)
        Me.fraDataInput.Controls.Add(Me.lblBVtyp)
        Me.fraDataInput.Controls.Add(Me.lblBVupv)
        Me.fraDataInput.Controls.Add(Me.lblBVdh)
        Me.fraDataInput.Controls.Add(Me.lblBVmw)
        Me.fraDataInput.Controls.Add(Me.lblBVprc)
        Me.fraDataInput.Controls.Add(Me.lblBVdrc)
        Me.fraDataInput.Controls.Add(Me.lblBVdp)
        Me.fraDataInput.Controls.Add(Me.lblBVq)
        Me.fraDataInput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataInput.ForeColor = System.Drawing.Color.Red
        Me.fraDataInput.Location = New System.Drawing.Point(8, 129)
        Me.fraDataInput.Name = "fraDataInput"
        Me.fraDataInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataInput.Size = New System.Drawing.Size(376, 351)
        Me.fraDataInput.TabIndex = 16
        Me.fraDataInput.TabStop = False
        Me.fraDataInput.Text = "数据录入"
        '
        'txtBVtyp
        '
        Me.txtBVtyp.AcceptsReturn = True
        Me.txtBVtyp.AutoSize = False
        Me.txtBVtyp.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVtyp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVtyp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVtyp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVtyp.Location = New System.Drawing.Point(256, 308)
        Me.txtBVtyp.MaxLength = 0
        Me.txtBVtyp.Name = "txtBVtyp"
        Me.txtBVtyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVtyp.Size = New System.Drawing.Size(88, 20)
        Me.txtBVtyp.TabIndex = 8
        Me.txtBVtyp.Text = "6"
        Me.txtBVtyp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVupv
        '
        Me.txtBVupv.AcceptsReturn = True
        Me.txtBVupv.AutoSize = False
        Me.txtBVupv.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVupv.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVupv.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVupv.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVupv.Location = New System.Drawing.Point(256, 268)
        Me.txtBVupv.MaxLength = 0
        Me.txtBVupv.Name = "txtBVupv"
        Me.txtBVupv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVupv.Size = New System.Drawing.Size(88, 20)
        Me.txtBVupv.TabIndex = 7
        Me.txtBVupv.Text = "12"
        Me.txtBVupv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVq
        '
        Me.txtBVq.AcceptsReturn = True
        Me.txtBVq.AutoSize = False
        Me.txtBVq.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVq.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVq.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVq.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVq.Location = New System.Drawing.Point(256, 228)
        Me.txtBVq.MaxLength = 0
        Me.txtBVq.Name = "txtBVq"
        Me.txtBVq.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVq.Size = New System.Drawing.Size(88, 20)
        Me.txtBVq.TabIndex = 6
        Me.txtBVq.Text = "45"
        Me.txtBVq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVmw
        '
        Me.txtBVmw.AcceptsReturn = True
        Me.txtBVmw.AutoSize = False
        Me.txtBVmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVmw.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVmw.Location = New System.Drawing.Point(256, 188)
        Me.txtBVmw.MaxLength = 0
        Me.txtBVmw.Name = "txtBVmw"
        Me.txtBVmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVmw.Size = New System.Drawing.Size(88, 20)
        Me.txtBVmw.TabIndex = 5
        Me.txtBVmw.Text = "1.16"
        Me.txtBVmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVprc
        '
        Me.txtBVprc.AcceptsReturn = True
        Me.txtBVprc.AutoSize = False
        Me.txtBVprc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVprc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVprc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVprc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVprc.Location = New System.Drawing.Point(256, 148)
        Me.txtBVprc.MaxLength = 0
        Me.txtBVprc.Name = "txtBVprc"
        Me.txtBVprc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVprc.Size = New System.Drawing.Size(88, 20)
        Me.txtBVprc.TabIndex = 4
        Me.txtBVprc.Text = "2.52"
        Me.txtBVprc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdrc
        '
        Me.txtBVdrc.AcceptsReturn = True
        Me.txtBVdrc.AutoSize = False
        Me.txtBVdrc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdrc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdrc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdrc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdrc.Location = New System.Drawing.Point(256, 108)
        Me.txtBVdrc.MaxLength = 0
        Me.txtBVdrc.Name = "txtBVdrc"
        Me.txtBVdrc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdrc.Size = New System.Drawing.Size(88, 20)
        Me.txtBVdrc.TabIndex = 3
        Me.txtBVdrc.Text = "6"
        Me.txtBVdrc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdp
        '
        Me.txtBVdp.AcceptsReturn = True
        Me.txtBVdp.AutoSize = False
        Me.txtBVdp.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdp.Location = New System.Drawing.Point(256, 68)
        Me.txtBVdp.MaxLength = 0
        Me.txtBVdp.Name = "txtBVdp"
        Me.txtBVdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdp.Size = New System.Drawing.Size(88, 20)
        Me.txtBVdp.TabIndex = 2
        Me.txtBVdp.Text = "127"
        Me.txtBVdp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdh
        '
        Me.txtBVdh.AcceptsReturn = True
        Me.txtBVdh.AutoSize = False
        Me.txtBVdh.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdh.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdh.Location = New System.Drawing.Point(256, 28)
        Me.txtBVdh.MaxLength = 0
        Me.txtBVdh.Name = "txtBVdh"
        Me.txtBVdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdh.Size = New System.Drawing.Size(88, 20)
        Me.txtBVdh.TabIndex = 1
        Me.txtBVdh.Text = "311.15"
        Me.txtBVdh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBVtyp
        '
        Me.lblBVtyp.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVtyp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVtyp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVtyp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVtyp.Location = New System.Drawing.Point(10, 308)
        Me.lblBVtyp.Name = "lblBVtyp"
        Me.lblBVtyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVtyp.Size = New System.Drawing.Size(231, 20)
        Me.lblBVtyp.TabIndex = 25
        Me.lblBVtyp.Text = "钻井液动切力(Pa)"
        '
        'lblBVupv
        '
        Me.lblBVupv.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVupv.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVupv.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVupv.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVupv.Location = New System.Drawing.Point(10, 268)
        Me.lblBVupv.Name = "lblBVupv"
        Me.lblBVupv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVupv.Size = New System.Drawing.Size(231, 20)
        Me.lblBVupv.TabIndex = 24
        Me.lblBVupv.Text = "钻井液塑性粘度(mPa.s)"
        '
        'lblBVdh
        '
        Me.lblBVdh.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdh.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdh.Location = New System.Drawing.Point(10, 28)
        Me.lblBVdh.Name = "lblBVdh"
        Me.lblBVdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdh.Size = New System.Drawing.Size(231, 20)
        Me.lblBVdh.TabIndex = 23
        Me.lblBVdh.Text = "井眼直径(mm)"
        '
        'lblBVmw
        '
        Me.lblBVmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVmw.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVmw.Location = New System.Drawing.Point(10, 188)
        Me.lblBVmw.Name = "lblBVmw"
        Me.lblBVmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVmw.Size = New System.Drawing.Size(231, 20)
        Me.lblBVmw.TabIndex = 22
        Me.lblBVmw.Text = "钻井液密度(g/cm^3)"
        '
        'lblBVprc
        '
        Me.lblBVprc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVprc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVprc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVprc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVprc.Location = New System.Drawing.Point(10, 148)
        Me.lblBVprc.Name = "lblBVprc"
        Me.lblBVprc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVprc.Size = New System.Drawing.Size(231, 20)
        Me.lblBVprc.TabIndex = 21
        Me.lblBVprc.Text = "岩屑密度(g/cm^3)"
        '
        'lblBVdrc
        '
        Me.lblBVdrc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdrc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdrc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdrc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdrc.Location = New System.Drawing.Point(10, 108)
        Me.lblBVdrc.Name = "lblBVdrc"
        Me.lblBVdrc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdrc.Size = New System.Drawing.Size(231, 20)
        Me.lblBVdrc.TabIndex = 20
        Me.lblBVdrc.Text = "岩屑颗粒直径(mm)"
        '
        'lblBVdp
        '
        Me.lblBVdp.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdp.Location = New System.Drawing.Point(10, 68)
        Me.lblBVdp.Name = "lblBVdp"
        Me.lblBVdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdp.Size = New System.Drawing.Size(231, 20)
        Me.lblBVdp.TabIndex = 19
        Me.lblBVdp.Text = "钻柱外径(mm)"
        '
        'lblBVq
        '
        Me.lblBVq.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVq.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVq.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVq.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVq.Location = New System.Drawing.Point(10, 228)
        Me.lblBVq.Name = "lblBVq"
        Me.lblBVq.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVq.Size = New System.Drawing.Size(231, 20)
        Me.lblBVq.TabIndex = 18
        Me.lblBVq.Text = "排量(l/s)"
        '
        'fraProgram
        '
        Me.fraProgram.BackColor = System.Drawing.SystemColors.Control
        Me.fraProgram.Controls.Add(Me.lblBVprogram)
        Me.fraProgram.ForeColor = System.Drawing.Color.Blue
        Me.fraProgram.Location = New System.Drawing.Point(8, 8)
        Me.fraProgram.Name = "fraProgram"
        Me.fraProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraProgram.Size = New System.Drawing.Size(702, 113)
        Me.fraProgram.TabIndex = 0
        Me.fraProgram.TabStop = False
        Me.fraProgram.Text = "程序说明"
        '
        'lblBVprogram
        '
        Me.lblBVprogram.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVprogram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblBVprogram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVprogram.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVprogram.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblBVprogram.Location = New System.Drawing.Point(10, 17)
        Me.lblBVprogram.Name = "lblBVprogram"
        Me.lblBVprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVprogram.Size = New System.Drawing.Size(682, 87)
        Me.lblBVprogram.TabIndex = 30
        Me.lblBVprogram.Text = "    本程序用来计算井眼的净化问题，即岩屑的下滑速度、升举效率的计算，同时给出了对环空净化能力的评价。环空携岩能力LC要大于等于0.5。请注意数据单位。"
        '
        'frmBVel
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 588)
        Me.Controls.Add(Me.picCuttingCarrying)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.fraDataOuput)
        Me.Controls.Add(Me.fraDataInput)
        Me.Controls.Add(Me.fraProgram)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmBVel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "井眼净化能力评价(LC)"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraDataOuput.ResumeLayout(False)
        Me.fraDataInput.ResumeLayout(False)
        Me.fraProgram.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmBVel
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmBVel
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmBVel()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月5日17:51于青海省海西洲大柴旦镇红山参2井。加入纠错功能。
	'环空净化能力计算评价
	'模块的英文化修改，时间：2012年4月15日
	
	Dim dh As Single
	Dim dp As Single
	Dim Q As Single
	Dim upv As Single
	Dim typ As Single
	Dim drc As Single
	Dim prc As Single
	Dim mw As Single
	
	Dim Vs As Single
	Dim uf As Single
	Dim Va As Single
	Dim gvs As Single
	Dim lc As Single
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		dh = Val(txtBVdh.Text)
		If txtBVdh.Text = "" Or dh = 0 Then
            MsgBox("请检查你输入的‘井眼直径’数据是否有效！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
			txtBVdh.Text = ""
			Exit Sub '纠错功能解决。2006年8月7日王朝
		End If
		
		dp = Val(txtBVdp.Text)
		If dp >= dh Then
            MsgBox("请检查你输入的‘钻杆直径’数据是否有效！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
			txtBVdp.Text = ""
			Exit Sub
		End If
		
		drc = Val(txtBVdrc.Text)
		If drc <= 0 Then
            MsgBox("请检查你输入的‘岩屑颗粒直径’数据是否有效！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
			txtBVdrc.Text = ""
			Exit Sub
		End If
		
		mw = Val(txtBVmw.Text)
		If mw <= 1# Then
            MsgBox("请检查你输入的‘钻井液密度’数据是否有效！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
			txtBVmw.Text = ""
			Exit Sub
		End If
		
		prc = Val(txtBVprc.Text)
		If prc <= 1# Then
            MsgBox("请检查你输入的‘岩屑密度’数据是否有效！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
			txtBVprc.Text = ""
			Exit Sub
		End If
		
		Q = Val(txtBVq.Text)
		upv = Val(txtBVupv.Text)
		typ = Val(txtBVtyp.Text)
		
		Va = (1273 * Q) / (dh ^ 2 - dp ^ 2)
		uf = upv + 0.112 * ((typ * (dh - dp)) / Va)
		Vs = (0.071 * drc * ((prc - mw) ^ 0.667)) / ((mw * uf) ^ 0.333)
		gvs = Va - Vs
		lc = 1 - Vs / Va
		
		txtBVhvs.Text = VB6.Format(Va, "0.00")
		txtBVvs.Text = VB6.Format(Vs, "0.00")
		txtBVgvs.Text = VB6.Format(gvs, "0.00")
		txtBVlc.Text = VB6.Format(lc, "0.00")
		
		If lc >= 0.5 Then
            MsgBox("井眼净化合格！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
		Else
            MsgBox("井眼净化不合格，请改变施工参数！", MsgBoxStyle.Information, "井眼净化能力评价(LC)")
		End If
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		txtBVdh.Text = ""
		txtBVdp.Text = ""
		txtBVdrc.Text = ""
		txtBVmw.Text = ""
		txtBVprc.Text = ""
		txtBVq.Text = ""
		txtBVupv.Text = ""
		txtBVtyp.Text = ""
		txtBVhvs.Text = ""
		txtBVvs.Text = ""
		txtBVgvs.Text = ""
		txtBVlc.Text = "" '使用该方法清除数据，可能存在bug隐患。
		
		
	End Sub
	
	Private Sub frmBVel_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdclear.Visible = False '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MCutCarry.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MCutCarry.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("MLeakoff.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdh_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdh.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dh = Val(txtBVdh.Text)
		
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdp_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdp.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dp = Val(txtBVdp.Text)
		
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdrc_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdrc.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdrc_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdrc.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		drc = Val(txtBVdrc.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVmw_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVmw.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVmw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVmw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mw = Val(txtBVmw.Text)
		If mw <= 0.99 Then
			MsgBox("Please check that you have entered the data!", MsgBoxStyle.Information)
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVprc_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVprc.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVprc_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVprc.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		prc = Val(txtBVprc.Text)
		If prc < 2 Or prc > 3 Then
			MsgBox("Please check that you have entered the data!", MsgBoxStyle.Information)
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVq_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVq.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVq_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVq.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Q = Val(txtBVq.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVtyp_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVtyp.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVupv_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVupv.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVupv_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVupv.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		upv = Val(txtBVupv.Text)
	End Sub
	
	Private Sub txtBVtyp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVtyp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		typ = Val(txtBVtyp.Text)
	End Sub

    
End Class