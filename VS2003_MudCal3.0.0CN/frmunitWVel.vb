Option Strict Off
Option Explicit On
Friend Class frmunitWVel
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtWVolumekgh As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumekgs As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumeth As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumetd As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumeta As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumelbs As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumelbh As System.Windows.Forms.TextBox
	Public WithEvents txtWVolumelbd As System.Windows.Forms.TextBox
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents lblWVolumekgh As System.Windows.Forms.Label
	Public WithEvents lblWVolumekgs As System.Windows.Forms.Label
	Public WithEvents lblWVolumeth As System.Windows.Forms.Label
	Public WithEvents lblWVolumetd As System.Windows.Forms.Label
	Public WithEvents lblWVolumeinput As System.Windows.Forms.Label
	Public WithEvents lblWVolumeta As System.Windows.Forms.Label
	Public WithEvents lblWVolumelbs As System.Windows.Forms.Label
	Public WithEvents lblWVolumelbh As System.Windows.Forms.Label
	Public WithEvents lblWVolumelbd As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblWVolumekgh = New System.Windows.Forms.Label
        Me.lblWVolumekgs = New System.Windows.Forms.Label
        Me.lblWVolumeth = New System.Windows.Forms.Label
        Me.lblWVolumetd = New System.Windows.Forms.Label
        Me.lblWVolumeta = New System.Windows.Forms.Label
        Me.lblWVolumelbs = New System.Windows.Forms.Label
        Me.lblWVolumelbh = New System.Windows.Forms.Label
        Me.lblWVolumelbd = New System.Windows.Forms.Label
        Me.txtWVolumekgh = New System.Windows.Forms.TextBox
        Me.txtWVolumekgs = New System.Windows.Forms.TextBox
        Me.txtWVolumeth = New System.Windows.Forms.TextBox
        Me.txtWVolumetd = New System.Windows.Forms.TextBox
        Me.txtWVolumeta = New System.Windows.Forms.TextBox
        Me.txtWVolumelbs = New System.Windows.Forms.TextBox
        Me.txtWVolumelbh = New System.Windows.Forms.TextBox
        Me.txtWVolumelbd = New System.Windows.Forms.TextBox
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.lblWVolumeinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblWVolumekgh
        '
        Me.lblWVolumekgh.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumekgh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumekgh.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumekgh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumekgh.Location = New System.Drawing.Point(125, 43)
        Me.lblWVolumekgh.Name = "lblWVolumekgh"
        Me.lblWVolumekgh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumekgh.Size = New System.Drawing.Size(49, 18)
        Me.lblWVolumekgh.TabIndex = 18
        Me.lblWVolumekgh.Text = "kg/h"
        Me.ToolTip1.SetToolTip(Me.lblWVolumekgh, "千克每时")
        '
        'lblWVolumekgs
        '
        Me.lblWVolumekgs.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumekgs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumekgs.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumekgs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumekgs.Location = New System.Drawing.Point(125, 86)
        Me.lblWVolumekgs.Name = "lblWVolumekgs"
        Me.lblWVolumekgs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumekgs.Size = New System.Drawing.Size(49, 18)
        Me.lblWVolumekgs.TabIndex = 17
        Me.lblWVolumekgs.Text = "kg/s"
        Me.ToolTip1.SetToolTip(Me.lblWVolumekgs, "千克每秒")
        '
        'lblWVolumeth
        '
        Me.lblWVolumeth.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumeth.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumeth.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumeth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumeth.Location = New System.Drawing.Point(125, 129)
        Me.lblWVolumeth.Name = "lblWVolumeth"
        Me.lblWVolumeth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumeth.Size = New System.Drawing.Size(49, 19)
        Me.lblWVolumeth.TabIndex = 16
        Me.lblWVolumeth.Text = "t/h"
        Me.ToolTip1.SetToolTip(Me.lblWVolumeth, "吨每时")
        '
        'lblWVolumetd
        '
        Me.lblWVolumetd.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumetd.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumetd.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumetd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumetd.Location = New System.Drawing.Point(125, 172)
        Me.lblWVolumetd.Name = "lblWVolumetd"
        Me.lblWVolumetd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumetd.Size = New System.Drawing.Size(49, 19)
        Me.lblWVolumetd.TabIndex = 15
        Me.lblWVolumetd.Text = "t/d"
        Me.ToolTip1.SetToolTip(Me.lblWVolumetd, "吨每日")
        '
        'lblWVolumeta
        '
        Me.lblWVolumeta.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumeta.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumeta.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumeta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumeta.Location = New System.Drawing.Point(307, 43)
        Me.lblWVolumeta.Name = "lblWVolumeta"
        Me.lblWVolumeta.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumeta.Size = New System.Drawing.Size(40, 18)
        Me.lblWVolumeta.TabIndex = 9
        Me.lblWVolumeta.Text = "t/a"
        Me.ToolTip1.SetToolTip(Me.lblWVolumeta, "吨每年(按8000小时)")
        '
        'lblWVolumelbs
        '
        Me.lblWVolumelbs.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumelbs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumelbs.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumelbs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumelbs.Location = New System.Drawing.Point(307, 86)
        Me.lblWVolumelbs.Name = "lblWVolumelbs"
        Me.lblWVolumelbs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumelbs.Size = New System.Drawing.Size(49, 18)
        Me.lblWVolumelbs.TabIndex = 8
        Me.lblWVolumelbs.Text = "lb/s"
        Me.ToolTip1.SetToolTip(Me.lblWVolumelbs, "磅每秒")
        '
        'lblWVolumelbh
        '
        Me.lblWVolumelbh.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumelbh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumelbh.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumelbh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumelbh.Location = New System.Drawing.Point(307, 129)
        Me.lblWVolumelbh.Name = "lblWVolumelbh"
        Me.lblWVolumelbh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumelbh.Size = New System.Drawing.Size(49, 19)
        Me.lblWVolumelbh.TabIndex = 7
        Me.lblWVolumelbh.Text = "lb/h"
        Me.ToolTip1.SetToolTip(Me.lblWVolumelbh, "磅每时")
        '
        'lblWVolumelbd
        '
        Me.lblWVolumelbd.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumelbd.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumelbd.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumelbd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumelbd.Location = New System.Drawing.Point(307, 172)
        Me.lblWVolumelbd.Name = "lblWVolumelbd"
        Me.lblWVolumelbd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumelbd.Size = New System.Drawing.Size(59, 19)
        Me.lblWVolumelbd.TabIndex = 6
        Me.lblWVolumelbd.Text = "lb/d"
        Me.ToolTip1.SetToolTip(Me.lblWVolumelbd, "磅每日")
        '
        'txtWVolumekgh
        '
        Me.txtWVolumekgh.AcceptsReturn = True
        Me.txtWVolumekgh.AutoSize = False
        Me.txtWVolumekgh.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumekgh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumekgh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumekgh.Location = New System.Drawing.Point(10, 43)
        Me.txtWVolumekgh.MaxLength = 0
        Me.txtWVolumekgh.Name = "txtWVolumekgh"
        Me.txtWVolumekgh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumekgh.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumekgh.TabIndex = 14
        Me.txtWVolumekgh.Text = ""
        '
        'txtWVolumekgs
        '
        Me.txtWVolumekgs.AcceptsReturn = True
        Me.txtWVolumekgs.AutoSize = False
        Me.txtWVolumekgs.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumekgs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumekgs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumekgs.Location = New System.Drawing.Point(10, 86)
        Me.txtWVolumekgs.MaxLength = 0
        Me.txtWVolumekgs.Name = "txtWVolumekgs"
        Me.txtWVolumekgs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumekgs.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumekgs.TabIndex = 13
        Me.txtWVolumekgs.Text = ""
        '
        'txtWVolumeth
        '
        Me.txtWVolumeth.AcceptsReturn = True
        Me.txtWVolumeth.AutoSize = False
        Me.txtWVolumeth.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumeth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumeth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumeth.Location = New System.Drawing.Point(10, 129)
        Me.txtWVolumeth.MaxLength = 0
        Me.txtWVolumeth.Name = "txtWVolumeth"
        Me.txtWVolumeth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumeth.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumeth.TabIndex = 12
        Me.txtWVolumeth.Text = ""
        '
        'txtWVolumetd
        '
        Me.txtWVolumetd.AcceptsReturn = True
        Me.txtWVolumetd.AutoSize = False
        Me.txtWVolumetd.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumetd.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumetd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumetd.Location = New System.Drawing.Point(10, 172)
        Me.txtWVolumetd.MaxLength = 0
        Me.txtWVolumetd.Name = "txtWVolumetd"
        Me.txtWVolumetd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumetd.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumetd.TabIndex = 11
        Me.txtWVolumetd.Text = ""
        '
        'txtWVolumeta
        '
        Me.txtWVolumeta.AcceptsReturn = True
        Me.txtWVolumeta.AutoSize = False
        Me.txtWVolumeta.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumeta.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumeta.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumeta.Location = New System.Drawing.Point(192, 43)
        Me.txtWVolumeta.MaxLength = 0
        Me.txtWVolumeta.Name = "txtWVolumeta"
        Me.txtWVolumeta.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumeta.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumeta.TabIndex = 5
        Me.txtWVolumeta.Text = ""
        '
        'txtWVolumelbs
        '
        Me.txtWVolumelbs.AcceptsReturn = True
        Me.txtWVolumelbs.AutoSize = False
        Me.txtWVolumelbs.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumelbs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumelbs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumelbs.Location = New System.Drawing.Point(192, 86)
        Me.txtWVolumelbs.MaxLength = 0
        Me.txtWVolumelbs.Name = "txtWVolumelbs"
        Me.txtWVolumelbs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumelbs.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumelbs.TabIndex = 4
        Me.txtWVolumelbs.Text = ""
        '
        'txtWVolumelbh
        '
        Me.txtWVolumelbh.AcceptsReturn = True
        Me.txtWVolumelbh.AutoSize = False
        Me.txtWVolumelbh.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumelbh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumelbh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumelbh.Location = New System.Drawing.Point(192, 129)
        Me.txtWVolumelbh.MaxLength = 0
        Me.txtWVolumelbh.Name = "txtWVolumelbh"
        Me.txtWVolumelbh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumelbh.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumelbh.TabIndex = 3
        Me.txtWVolumelbh.Text = ""
        '
        'txtWVolumelbd
        '
        Me.txtWVolumelbd.AcceptsReturn = True
        Me.txtWVolumelbd.AutoSize = False
        Me.txtWVolumelbd.BackColor = System.Drawing.SystemColors.Window
        Me.txtWVolumelbd.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWVolumelbd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWVolumelbd.Location = New System.Drawing.Point(192, 172)
        Me.txtWVolumelbd.MaxLength = 0
        Me.txtWVolumelbd.Name = "txtWVolumelbd"
        Me.txtWVolumelbd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWVolumelbd.Size = New System.Drawing.Size(97, 27)
        Me.txtWVolumelbd.TabIndex = 2
        Me.txtWVolumelbd.Text = ""
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(230, 215)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 1
        Me.cmdclear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(48, 215)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 0
        Me.cmdquit.Text = "退出"
        '
        'lblWVolumeinput
        '
        Me.lblWVolumeinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblWVolumeinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWVolumeinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblWVolumeinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWVolumeinput.Location = New System.Drawing.Point(10, 9)
        Me.lblWVolumeinput.Name = "lblWVolumeinput"
        Me.lblWVolumeinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWVolumeinput.Size = New System.Drawing.Size(260, 18)
        Me.lblWVolumeinput.TabIndex = 10
        Me.lblWVolumeinput.Text = "输入质量流率单位换算数据："
        '
        'frmunitWVel
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(374, 254)
        Me.Controls.Add(Me.txtWVolumekgh)
        Me.Controls.Add(Me.txtWVolumekgs)
        Me.Controls.Add(Me.txtWVolumeth)
        Me.Controls.Add(Me.txtWVolumetd)
        Me.Controls.Add(Me.txtWVolumeta)
        Me.Controls.Add(Me.txtWVolumelbs)
        Me.Controls.Add(Me.txtWVolumelbh)
        Me.Controls.Add(Me.txtWVolumelbd)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.lblWVolumekgh)
        Me.Controls.Add(Me.lblWVolumekgs)
        Me.Controls.Add(Me.lblWVolumeth)
        Me.Controls.Add(Me.lblWVolumetd)
        Me.Controls.Add(Me.lblWVolumeinput)
        Me.Controls.Add(Me.lblWVolumeta)
        Me.Controls.Add(Me.lblWVolumelbs)
        Me.Controls.Add(Me.lblWVolumelbh)
        Me.Controls.Add(Me.lblWVolumelbd)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitWVel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "质量流率单位制换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitWVel
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitWVel
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitWVel()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月1日12:40于青海省海西洲大柴旦红山参2井
	'质量速率单位换算
	
	
	Dim kgh As Single
	Dim kgs As Single
	Dim th As Single
	Dim td As Single
	Dim ta As Single
	Dim lbs As Single
	Dim lbh As Single
	Dim lbd As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitWVel.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtWVolumekgh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumekgh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgh = Val(txtWVolumekgh.Text)
		
		kgs = kgh * 0.00027778
		th = kgh * 0.001
		td = kgh * 0.024
		ta = kgh * 8#
		lbs = kgh * 0.0006124
		lbh = kgh * 2.2046
		lbd = kgh * 52.9109
		
		'txtWVolumekgh.Text = kgh
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
		
	End Sub
	
	Private Sub txtWVolumekgs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumekgs.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgs = Val(txtWVolumekgs.Text)
		
		kgh = kgs * 3600#
		th = kgs * 3.6
		td = kgs * 86.4
		ta = kgs * 28800#
		lbs = kgs * 2.2046
		lbh = kgs * 7936.6414
		lbd = kgs * 190480#
		
		txtWVolumekgh.Text = CStr(kgh)
		'txtWVolumekgs.Text = kgs
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
	
	Private Sub txtWVolumelbd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumelbd.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbd = Val(txtWVolumelbd.Text)
		
		kgh = lbd * 0.0189
		kgs = lbd * 0.0000052499
		th = lbd * 0.0000189
		td = lbd * 0.00045359
		ta = lbd * 0.1512
		lbs = lbd * 0.000011574
		lbh = lbd * 0.0417
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		'txtWVolumelbd.Text = lbd
	End Sub
	
	Private Sub txtWVolumelbh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumelbh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbh = Val(txtWVolumelbh.Text)
		
		kgh = lbh * 0.4536
		kgs = lbh * 0.000126
		th = lbh * 0.00045359
		td = lbh * 0.0109
		ta = lbh * 3.6287
		lbs = lbh * 0.00027778
		lbd = lbh * 24#
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		'txtWVolumelbh.Text = lbh
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
	
	Private Sub txtWVolumelbs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumelbs.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbs = Val(txtWVolumelbs.Text)
		
		kgh = lbs * 1632.9325
		kgs = lbs * 0.4536
		th = lbs * 1.6329
		td = lbs * 39.1904
		ta = lbs * 13063#
		lbh = lbs * 3600#
		lbd = lbs * 86400#
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		'txtWVolumelbs.Text = lbs
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
	
	Private Sub txtWVolumeta_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumeta.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ta = Val(txtWVolumeta.Text)
		
		kgh = ta * 0.125
		kgs = ta * 0.000034722
		th = ta * 0.000125
		td = ta * 0.003
		lbs = ta * 0.000076549
		lbh = ta * 0.2756
		lbd = ta * 6.6139
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		txtWVolumetd.Text = CStr(td)
		'txtWVolumeta.Text = ta
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
	
	Private Sub txtWVolumetd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumetd.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		td = Val(txtWVolumetd.Text)
		
		kgh = td * 41.6667
		kgs = td * 0.0116
		th = td * 0.0417
		ta = td * 333.3333
		lbs = td * 0.0255
		lbh = td * 91.8593
		lbd = td * 2204.6226
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		txtWVolumeth.Text = CStr(th)
		'txtWVolumetd.Text = td
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
	
	Private Sub txtWVolumeth_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWVolumeth.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		th = Val(txtWVolumeth.Text)
		
		kgh = th * 1000#
		kgs = th * 0.2778
		td = th * 24#
		ta = th * 8000#
		lbs = th * 0.6124
		lbh = th * 2204.6226
		lbd = th * 52911#
		
		txtWVolumekgh.Text = CStr(kgh)
		txtWVolumekgs.Text = CStr(kgs)
		'txtWVolumeth.Text = th
		txtWVolumetd.Text = CStr(td)
		txtWVolumeta.Text = CStr(ta)
		txtWVolumelbs.Text = CStr(lbs)
		txtWVolumelbh.Text = CStr(lbh)
		txtWVolumelbd.Text = CStr(lbd)
	End Sub
End Class