Option Strict Off
Option Explicit On
Friend Class frmunitWeight
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdhelp As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdWeightozgr2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightozgr1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlboz2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlboz1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightls2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightls1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightshortt2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightshortt1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlongt2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlongt1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightgrmg2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightgrmg1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightozg2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightozg1 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlbkg2 As System.Windows.Forms.Button
	Public WithEvents cmdWeightlbkg1 As System.Windows.Forms.Button
	Public WithEvents txtWeightgr2 As System.Windows.Forms.TextBox
	Public WithEvents txtWeightoz2 As System.Windows.Forms.TextBox
	Public WithEvents txtWeightshort As System.Windows.Forms.TextBox
	Public WithEvents txtWeightt2 As System.Windows.Forms.TextBox
	Public WithEvents txtWeightt As System.Windows.Forms.TextBox
	Public WithEvents txtWeightmg As System.Windows.Forms.TextBox
	Public WithEvents txtWeightg As System.Windows.Forms.TextBox
	Public WithEvents txtWeightkg As System.Windows.Forms.TextBox
	Public WithEvents txtWeightoz3 As System.Windows.Forms.TextBox
	Public WithEvents txtWeightlb2 As System.Windows.Forms.TextBox
	Public WithEvents txtWeightlong As System.Windows.Forms.TextBox
	Public WithEvents txtWeightshortt As System.Windows.Forms.TextBox
	Public WithEvents txtWeightlongt As System.Windows.Forms.TextBox
	Public WithEvents txtWeightgr As System.Windows.Forms.TextBox
	Public WithEvents txtWeightoz As System.Windows.Forms.TextBox
	Public WithEvents txtWeightlb As System.Windows.Forms.TextBox
	Public WithEvents labWeightgr2 As System.Windows.Forms.Label
	Public WithEvents labWeightoz2 As System.Windows.Forms.Label
	Public WithEvents labWeightshort As System.Windows.Forms.Label
	Public WithEvents labWeightt2 As System.Windows.Forms.Label
	Public WithEvents labWeightt As System.Windows.Forms.Label
	Public WithEvents labWeightmg As System.Windows.Forms.Label
	Public WithEvents labWeightg As System.Windows.Forms.Label
	Public WithEvents labWeightkg As System.Windows.Forms.Label
	Public WithEvents labWeightoz3 As System.Windows.Forms.Label
	Public WithEvents labWeightlb2 As System.Windows.Forms.Label
	Public WithEvents labWeightlong As System.Windows.Forms.Label
	Public WithEvents labWeightshortt As System.Windows.Forms.Label
	Public WithEvents labWeightlongt As System.Windows.Forms.Label
	Public WithEvents labWeightgr As System.Windows.Forms.Label
	Public WithEvents labWeightoz As System.Windows.Forms.Label
	Public WithEvents labWeightlb As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdWeightozgr2 = New System.Windows.Forms.Button
        Me.cmdWeightozgr1 = New System.Windows.Forms.Button
        Me.cmdWeightlboz2 = New System.Windows.Forms.Button
        Me.cmdWeightlboz1 = New System.Windows.Forms.Button
        Me.cmdWeightls2 = New System.Windows.Forms.Button
        Me.cmdWeightls1 = New System.Windows.Forms.Button
        Me.cmdWeightshortt2 = New System.Windows.Forms.Button
        Me.cmdWeightshortt1 = New System.Windows.Forms.Button
        Me.cmdWeightlongt2 = New System.Windows.Forms.Button
        Me.cmdWeightlongt1 = New System.Windows.Forms.Button
        Me.cmdWeightgrmg2 = New System.Windows.Forms.Button
        Me.cmdWeightgrmg1 = New System.Windows.Forms.Button
        Me.cmdWeightozg2 = New System.Windows.Forms.Button
        Me.cmdWeightozg1 = New System.Windows.Forms.Button
        Me.cmdWeightlbkg2 = New System.Windows.Forms.Button
        Me.cmdWeightlbkg1 = New System.Windows.Forms.Button
        Me.labWeightgr2 = New System.Windows.Forms.Label
        Me.labWeightoz2 = New System.Windows.Forms.Label
        Me.labWeightshort = New System.Windows.Forms.Label
        Me.labWeightt2 = New System.Windows.Forms.Label
        Me.labWeightt = New System.Windows.Forms.Label
        Me.labWeightmg = New System.Windows.Forms.Label
        Me.labWeightg = New System.Windows.Forms.Label
        Me.labWeightkg = New System.Windows.Forms.Label
        Me.labWeightoz3 = New System.Windows.Forms.Label
        Me.labWeightlb2 = New System.Windows.Forms.Label
        Me.labWeightlong = New System.Windows.Forms.Label
        Me.labWeightshortt = New System.Windows.Forms.Label
        Me.labWeightlongt = New System.Windows.Forms.Label
        Me.labWeightgr = New System.Windows.Forms.Label
        Me.labWeightoz = New System.Windows.Forms.Label
        Me.labWeightlb = New System.Windows.Forms.Label
        Me.cmdhelp = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtWeightgr2 = New System.Windows.Forms.TextBox
        Me.txtWeightoz2 = New System.Windows.Forms.TextBox
        Me.txtWeightshort = New System.Windows.Forms.TextBox
        Me.txtWeightt2 = New System.Windows.Forms.TextBox
        Me.txtWeightt = New System.Windows.Forms.TextBox
        Me.txtWeightmg = New System.Windows.Forms.TextBox
        Me.txtWeightg = New System.Windows.Forms.TextBox
        Me.txtWeightkg = New System.Windows.Forms.TextBox
        Me.txtWeightoz3 = New System.Windows.Forms.TextBox
        Me.txtWeightlb2 = New System.Windows.Forms.TextBox
        Me.txtWeightlong = New System.Windows.Forms.TextBox
        Me.txtWeightshortt = New System.Windows.Forms.TextBox
        Me.txtWeightlongt = New System.Windows.Forms.TextBox
        Me.txtWeightgr = New System.Windows.Forms.TextBox
        Me.txtWeightoz = New System.Windows.Forms.TextBox
        Me.txtWeightlb = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'cmdWeightozgr2
        '
        Me.cmdWeightozgr2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightozgr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightozgr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightozgr2.Location = New System.Drawing.Point(269, 267)
        Me.cmdWeightozgr2.Name = "cmdWeightozgr2"
        Me.cmdWeightozgr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightozgr2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightozgr2.TabIndex = 47
        Me.cmdWeightozgr2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightozgr2, "1oz = 437.5000gr")
        '
        'cmdWeightozgr1
        '
        Me.cmdWeightozgr1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightozgr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightozgr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightozgr1.Location = New System.Drawing.Point(182, 267)
        Me.cmdWeightozgr1.Name = "cmdWeightozgr1"
        Me.cmdWeightozgr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightozgr1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightozgr1.TabIndex = 46
        Me.cmdWeightozgr1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightozgr1, "1gr = 0.0023oz")
        '
        'cmdWeightlboz2
        '
        Me.cmdWeightlboz2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlboz2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlboz2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlboz2.Location = New System.Drawing.Point(269, 233)
        Me.cmdWeightlboz2.Name = "cmdWeightlboz2"
        Me.cmdWeightlboz2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlboz2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightlboz2.TabIndex = 45
        Me.cmdWeightlboz2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightlboz2, "1lb = 16.0000oz")
        '
        'cmdWeightlboz1
        '
        Me.cmdWeightlboz1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlboz1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlboz1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlboz1.Location = New System.Drawing.Point(182, 233)
        Me.cmdWeightlboz1.Name = "cmdWeightlboz1"
        Me.cmdWeightlboz1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlboz1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightlboz1.TabIndex = 44
        Me.cmdWeightlboz1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightlboz1, "1oz = 0.0625lb")
        '
        'cmdWeightls2
        '
        Me.cmdWeightls2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightls2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightls2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightls2.Location = New System.Drawing.Point(269, 198)
        Me.cmdWeightls2.Name = "cmdWeightls2"
        Me.cmdWeightls2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightls2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightls2.TabIndex = 43
        Me.cmdWeightls2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightls2, "1long t = 1.1200short t")
        '
        'cmdWeightls1
        '
        Me.cmdWeightls1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightls1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightls1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightls1.Location = New System.Drawing.Point(182, 198)
        Me.cmdWeightls1.Name = "cmdWeightls1"
        Me.cmdWeightls1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightls1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightls1.TabIndex = 42
        Me.cmdWeightls1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightls1, "1short t = 0.8929long t")
        '
        'cmdWeightshortt2
        '
        Me.cmdWeightshortt2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightshortt2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightshortt2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightshortt2.Location = New System.Drawing.Point(269, 164)
        Me.cmdWeightshortt2.Name = "cmdWeightshortt2"
        Me.cmdWeightshortt2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightshortt2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightshortt2.TabIndex = 41
        Me.cmdWeightshortt2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightshortt2, "1short t = 0.9072t")
        '
        'cmdWeightshortt1
        '
        Me.cmdWeightshortt1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightshortt1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightshortt1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightshortt1.Location = New System.Drawing.Point(182, 164)
        Me.cmdWeightshortt1.Name = "cmdWeightshortt1"
        Me.cmdWeightshortt1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightshortt1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightshortt1.TabIndex = 40
        Me.cmdWeightshortt1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightshortt1, "1t = 1.1023short t")
        '
        'cmdWeightlongt2
        '
        Me.cmdWeightlongt2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlongt2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlongt2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlongt2.Location = New System.Drawing.Point(269, 129)
        Me.cmdWeightlongt2.Name = "cmdWeightlongt2"
        Me.cmdWeightlongt2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlongt2.Size = New System.Drawing.Size(49, 19)
        Me.cmdWeightlongt2.TabIndex = 39
        Me.cmdWeightlongt2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightlongt2, "1long t = 1.0160t")
        '
        'cmdWeightlongt1
        '
        Me.cmdWeightlongt1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlongt1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlongt1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlongt1.Location = New System.Drawing.Point(182, 129)
        Me.cmdWeightlongt1.Name = "cmdWeightlongt1"
        Me.cmdWeightlongt1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlongt1.Size = New System.Drawing.Size(50, 19)
        Me.cmdWeightlongt1.TabIndex = 38
        Me.cmdWeightlongt1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightlongt1, "1t = 0.9842long t")
        '
        'cmdWeightgrmg2
        '
        Me.cmdWeightgrmg2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightgrmg2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightgrmg2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightgrmg2.Location = New System.Drawing.Point(269, 95)
        Me.cmdWeightgrmg2.Name = "cmdWeightgrmg2"
        Me.cmdWeightgrmg2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightgrmg2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightgrmg2.TabIndex = 37
        Me.cmdWeightgrmg2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightgrmg2, "1gr = 64.7989mg")
        '
        'cmdWeightgrmg1
        '
        Me.cmdWeightgrmg1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightgrmg1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightgrmg1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightgrmg1.Location = New System.Drawing.Point(182, 95)
        Me.cmdWeightgrmg1.Name = "cmdWeightgrmg1"
        Me.cmdWeightgrmg1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightgrmg1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightgrmg1.TabIndex = 36
        Me.cmdWeightgrmg1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightgrmg1, "1mg = 0.0154gr")
        '
        'cmdWeightozg2
        '
        Me.cmdWeightozg2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightozg2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightozg2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightozg2.Location = New System.Drawing.Point(269, 60)
        Me.cmdWeightozg2.Name = "cmdWeightozg2"
        Me.cmdWeightozg2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightozg2.Size = New System.Drawing.Size(49, 19)
        Me.cmdWeightozg2.TabIndex = 35
        Me.cmdWeightozg2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightozg2, "1oz = 28.35g")
        '
        'cmdWeightozg1
        '
        Me.cmdWeightozg1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightozg1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightozg1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightozg1.Location = New System.Drawing.Point(182, 60)
        Me.cmdWeightozg1.Name = "cmdWeightozg1"
        Me.cmdWeightozg1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightozg1.Size = New System.Drawing.Size(50, 19)
        Me.cmdWeightozg1.TabIndex = 34
        Me.cmdWeightozg1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightozg1, "1g = 0.0353oz")
        '
        'cmdWeightlbkg2
        '
        Me.cmdWeightlbkg2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlbkg2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlbkg2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlbkg2.Location = New System.Drawing.Point(269, 26)
        Me.cmdWeightlbkg2.Name = "cmdWeightlbkg2"
        Me.cmdWeightlbkg2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlbkg2.Size = New System.Drawing.Size(49, 18)
        Me.cmdWeightlbkg2.TabIndex = 33
        Me.cmdWeightlbkg2.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdWeightlbkg2, "1lb = 0.4536kg")
        '
        'cmdWeightlbkg1
        '
        Me.cmdWeightlbkg1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWeightlbkg1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWeightlbkg1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWeightlbkg1.Location = New System.Drawing.Point(182, 26)
        Me.cmdWeightlbkg1.Name = "cmdWeightlbkg1"
        Me.cmdWeightlbkg1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWeightlbkg1.Size = New System.Drawing.Size(50, 18)
        Me.cmdWeightlbkg1.TabIndex = 32
        Me.cmdWeightlbkg1.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdWeightlbkg1, "1kg = 2.2075lb")
        '
        'labWeightgr2
        '
        Me.labWeightgr2.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightgr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightgr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightgr2.Location = New System.Drawing.Point(413, 267)
        Me.labWeightgr2.Name = "labWeightgr2"
        Me.labWeightgr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightgr2.Size = New System.Drawing.Size(68, 18)
        Me.labWeightgr2.TabIndex = 31
        Me.labWeightgr2.Text = "gr"
        Me.ToolTip1.SetToolTip(Me.labWeightgr2, "格令")
        '
        'labWeightoz2
        '
        Me.labWeightoz2.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightoz2.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightoz2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightoz2.Location = New System.Drawing.Point(413, 233)
        Me.labWeightoz2.Name = "labWeightoz2"
        Me.labWeightoz2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightoz2.Size = New System.Drawing.Size(68, 18)
        Me.labWeightoz2.TabIndex = 30
        Me.labWeightoz2.Text = "oz"
        Me.ToolTip1.SetToolTip(Me.labWeightoz2, "盎司")
        '
        'labWeightshort
        '
        Me.labWeightshort.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightshort.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightshort.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightshort.Location = New System.Drawing.Point(413, 198)
        Me.labWeightshort.Name = "labWeightshort"
        Me.labWeightshort.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightshort.Size = New System.Drawing.Size(68, 18)
        Me.labWeightshort.TabIndex = 29
        Me.labWeightshort.Text = "short t"
        Me.ToolTip1.SetToolTip(Me.labWeightshort, "美吨")
        '
        'labWeightt2
        '
        Me.labWeightt2.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightt2.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightt2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightt2.Location = New System.Drawing.Point(413, 164)
        Me.labWeightt2.Name = "labWeightt2"
        Me.labWeightt2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightt2.Size = New System.Drawing.Size(68, 18)
        Me.labWeightt2.TabIndex = 28
        Me.labWeightt2.Text = "t"
        Me.ToolTip1.SetToolTip(Me.labWeightt2, "吨")
        '
        'labWeightt
        '
        Me.labWeightt.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightt.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightt.Location = New System.Drawing.Point(413, 129)
        Me.labWeightt.Name = "labWeightt"
        Me.labWeightt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightt.Size = New System.Drawing.Size(68, 19)
        Me.labWeightt.TabIndex = 27
        Me.labWeightt.Text = "t"
        Me.ToolTip1.SetToolTip(Me.labWeightt, "吨")
        '
        'labWeightmg
        '
        Me.labWeightmg.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightmg.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightmg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightmg.Location = New System.Drawing.Point(413, 95)
        Me.labWeightmg.Name = "labWeightmg"
        Me.labWeightmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightmg.Size = New System.Drawing.Size(68, 18)
        Me.labWeightmg.TabIndex = 26
        Me.labWeightmg.Text = "mg"
        Me.ToolTip1.SetToolTip(Me.labWeightmg, "毫克")
        '
        'labWeightg
        '
        Me.labWeightg.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightg.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightg.Location = New System.Drawing.Point(413, 60)
        Me.labWeightg.Name = "labWeightg"
        Me.labWeightg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightg.Size = New System.Drawing.Size(68, 19)
        Me.labWeightg.TabIndex = 25
        Me.labWeightg.Text = "g"
        Me.ToolTip1.SetToolTip(Me.labWeightg, "克")
        '
        'labWeightkg
        '
        Me.labWeightkg.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightkg.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightkg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightkg.Location = New System.Drawing.Point(413, 26)
        Me.labWeightkg.Name = "labWeightkg"
        Me.labWeightkg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightkg.Size = New System.Drawing.Size(68, 18)
        Me.labWeightkg.TabIndex = 24
        Me.labWeightkg.Text = "kg"
        Me.ToolTip1.SetToolTip(Me.labWeightkg, "公斤")
        '
        'labWeightoz3
        '
        Me.labWeightoz3.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightoz3.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightoz3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightoz3.Location = New System.Drawing.Point(96, 267)
        Me.labWeightoz3.Name = "labWeightoz3"
        Me.labWeightoz3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightoz3.Size = New System.Drawing.Size(68, 18)
        Me.labWeightoz3.TabIndex = 15
        Me.labWeightoz3.Text = "oz"
        Me.ToolTip1.SetToolTip(Me.labWeightoz3, "盎司")
        '
        'labWeightlb2
        '
        Me.labWeightlb2.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightlb2.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightlb2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightlb2.Location = New System.Drawing.Point(96, 233)
        Me.labWeightlb2.Name = "labWeightlb2"
        Me.labWeightlb2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightlb2.Size = New System.Drawing.Size(68, 18)
        Me.labWeightlb2.TabIndex = 14
        Me.labWeightlb2.Text = "lb"
        Me.ToolTip1.SetToolTip(Me.labWeightlb2, "磅")
        '
        'labWeightlong
        '
        Me.labWeightlong.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightlong.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightlong.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightlong.Location = New System.Drawing.Point(96, 198)
        Me.labWeightlong.Name = "labWeightlong"
        Me.labWeightlong.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightlong.Size = New System.Drawing.Size(68, 18)
        Me.labWeightlong.TabIndex = 13
        Me.labWeightlong.Text = "long t"
        Me.ToolTip1.SetToolTip(Me.labWeightlong, "英吨")
        '
        'labWeightshortt
        '
        Me.labWeightshortt.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightshortt.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightshortt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightshortt.Location = New System.Drawing.Point(96, 164)
        Me.labWeightshortt.Name = "labWeightshortt"
        Me.labWeightshortt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightshortt.Size = New System.Drawing.Size(88, 18)
        Me.labWeightshortt.TabIndex = 12
        Me.labWeightshortt.Text = "short t(US)"
        Me.ToolTip1.SetToolTip(Me.labWeightshortt, "美吨")
        '
        'labWeightlongt
        '
        Me.labWeightlongt.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightlongt.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightlongt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightlongt.Location = New System.Drawing.Point(96, 129)
        Me.labWeightlongt.Name = "labWeightlongt"
        Me.labWeightlongt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightlongt.Size = New System.Drawing.Size(78, 19)
        Me.labWeightlongt.TabIndex = 11
        Me.labWeightlongt.Text = "long t(UK)"
        Me.ToolTip1.SetToolTip(Me.labWeightlongt, "英吨")
        '
        'labWeightgr
        '
        Me.labWeightgr.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightgr.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightgr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightgr.Location = New System.Drawing.Point(96, 95)
        Me.labWeightgr.Name = "labWeightgr"
        Me.labWeightgr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightgr.Size = New System.Drawing.Size(68, 18)
        Me.labWeightgr.TabIndex = 10
        Me.labWeightgr.Text = "gr"
        Me.ToolTip1.SetToolTip(Me.labWeightgr, "格令")
        '
        'labWeightoz
        '
        Me.labWeightoz.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightoz.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightoz.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightoz.Location = New System.Drawing.Point(96, 60)
        Me.labWeightoz.Name = "labWeightoz"
        Me.labWeightoz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightoz.Size = New System.Drawing.Size(68, 19)
        Me.labWeightoz.TabIndex = 9
        Me.labWeightoz.Text = "oz"
        Me.ToolTip1.SetToolTip(Me.labWeightoz, "盎司")
        '
        'labWeightlb
        '
        Me.labWeightlb.BackColor = System.Drawing.SystemColors.Control
        Me.labWeightlb.Cursor = System.Windows.Forms.Cursors.Default
        Me.labWeightlb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labWeightlb.Location = New System.Drawing.Point(96, 26)
        Me.labWeightlb.Name = "labWeightlb"
        Me.labWeightlb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labWeightlb.Size = New System.Drawing.Size(68, 18)
        Me.labWeightlb.TabIndex = 8
        Me.labWeightlb.Text = "lb"
        Me.ToolTip1.SetToolTip(Me.labWeightlb, "磅")
        '
        'cmdhelp
        '
        Me.cmdhelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdhelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdhelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdhelp.Location = New System.Drawing.Point(298, 310)
        Me.cmdhelp.Name = "cmdhelp"
        Me.cmdhelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdhelp.Size = New System.Drawing.Size(87, 27)
        Me.cmdhelp.TabIndex = 49
        Me.cmdhelp.Text = "帮助"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(125, 310)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 48
        Me.cmdquit.Text = "退出"
        '
        'txtWeightgr2
        '
        Me.txtWeightgr2.AcceptsReturn = True
        Me.txtWeightgr2.AutoSize = False
        Me.txtWeightgr2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightgr2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightgr2.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightgr2.Location = New System.Drawing.Point(336, 267)
        Me.txtWeightgr2.MaxLength = 0
        Me.txtWeightgr2.Name = "txtWeightgr2"
        Me.txtWeightgr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightgr2.Size = New System.Drawing.Size(68, 19)
        Me.txtWeightgr2.TabIndex = 23
        Me.txtWeightgr2.Text = "437.5000"
        '
        'txtWeightoz2
        '
        Me.txtWeightoz2.AcceptsReturn = True
        Me.txtWeightoz2.AutoSize = False
        Me.txtWeightoz2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightoz2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightoz2.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightoz2.Location = New System.Drawing.Point(336, 233)
        Me.txtWeightoz2.MaxLength = 0
        Me.txtWeightoz2.Name = "txtWeightoz2"
        Me.txtWeightoz2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightoz2.Size = New System.Drawing.Size(68, 19)
        Me.txtWeightoz2.TabIndex = 22
        Me.txtWeightoz2.Text = "16.0000"
        '
        'txtWeightshort
        '
        Me.txtWeightshort.AcceptsReturn = True
        Me.txtWeightshort.AutoSize = False
        Me.txtWeightshort.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightshort.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightshort.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightshort.Location = New System.Drawing.Point(336, 198)
        Me.txtWeightshort.MaxLength = 0
        Me.txtWeightshort.Name = "txtWeightshort"
        Me.txtWeightshort.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightshort.Size = New System.Drawing.Size(68, 20)
        Me.txtWeightshort.TabIndex = 21
        Me.txtWeightshort.Text = "1.1200"
        '
        'txtWeightt2
        '
        Me.txtWeightt2.AcceptsReturn = True
        Me.txtWeightt2.AutoSize = False
        Me.txtWeightt2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightt2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightt2.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightt2.Location = New System.Drawing.Point(336, 164)
        Me.txtWeightt2.MaxLength = 0
        Me.txtWeightt2.Name = "txtWeightt2"
        Me.txtWeightt2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightt2.Size = New System.Drawing.Size(68, 19)
        Me.txtWeightt2.TabIndex = 20
        Me.txtWeightt2.Text = "0.9072"
        '
        'txtWeightt
        '
        Me.txtWeightt.AcceptsReturn = True
        Me.txtWeightt.AutoSize = False
        Me.txtWeightt.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightt.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightt.Location = New System.Drawing.Point(336, 129)
        Me.txtWeightt.MaxLength = 0
        Me.txtWeightt.Name = "txtWeightt"
        Me.txtWeightt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightt.Size = New System.Drawing.Size(68, 20)
        Me.txtWeightt.TabIndex = 19
        Me.txtWeightt.Text = "1.0160"
        '
        'txtWeightmg
        '
        Me.txtWeightmg.AcceptsReturn = True
        Me.txtWeightmg.AutoSize = False
        Me.txtWeightmg.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightmg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightmg.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightmg.Location = New System.Drawing.Point(336, 95)
        Me.txtWeightmg.MaxLength = 0
        Me.txtWeightmg.Name = "txtWeightmg"
        Me.txtWeightmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightmg.Size = New System.Drawing.Size(68, 19)
        Me.txtWeightmg.TabIndex = 18
        Me.txtWeightmg.Text = "64.7989"
        '
        'txtWeightg
        '
        Me.txtWeightg.AcceptsReturn = True
        Me.txtWeightg.AutoSize = False
        Me.txtWeightg.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightg.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightg.Location = New System.Drawing.Point(336, 60)
        Me.txtWeightg.MaxLength = 0
        Me.txtWeightg.Name = "txtWeightg"
        Me.txtWeightg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightg.Size = New System.Drawing.Size(68, 20)
        Me.txtWeightg.TabIndex = 17
        Me.txtWeightg.Text = "28.35"
        '
        'txtWeightkg
        '
        Me.txtWeightkg.AcceptsReturn = True
        Me.txtWeightkg.AutoSize = False
        Me.txtWeightkg.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightkg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightkg.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightkg.Location = New System.Drawing.Point(336, 26)
        Me.txtWeightkg.MaxLength = 0
        Me.txtWeightkg.Name = "txtWeightkg"
        Me.txtWeightkg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightkg.Size = New System.Drawing.Size(68, 19)
        Me.txtWeightkg.TabIndex = 16
        Me.txtWeightkg.Text = "0.4536"
        '
        'txtWeightoz3
        '
        Me.txtWeightoz3.AcceptsReturn = True
        Me.txtWeightoz3.AutoSize = False
        Me.txtWeightoz3.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightoz3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightoz3.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightoz3.Location = New System.Drawing.Point(19, 267)
        Me.txtWeightoz3.MaxLength = 0
        Me.txtWeightoz3.Name = "txtWeightoz3"
        Me.txtWeightoz3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightoz3.Size = New System.Drawing.Size(69, 19)
        Me.txtWeightoz3.TabIndex = 7
        Me.txtWeightoz3.Text = "1"
        '
        'txtWeightlb2
        '
        Me.txtWeightlb2.AcceptsReturn = True
        Me.txtWeightlb2.AutoSize = False
        Me.txtWeightlb2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightlb2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightlb2.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightlb2.Location = New System.Drawing.Point(19, 233)
        Me.txtWeightlb2.MaxLength = 0
        Me.txtWeightlb2.Name = "txtWeightlb2"
        Me.txtWeightlb2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightlb2.Size = New System.Drawing.Size(69, 19)
        Me.txtWeightlb2.TabIndex = 6
        Me.txtWeightlb2.Text = "1"
        '
        'txtWeightlong
        '
        Me.txtWeightlong.AcceptsReturn = True
        Me.txtWeightlong.AutoSize = False
        Me.txtWeightlong.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightlong.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightlong.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightlong.Location = New System.Drawing.Point(19, 198)
        Me.txtWeightlong.MaxLength = 0
        Me.txtWeightlong.Name = "txtWeightlong"
        Me.txtWeightlong.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightlong.Size = New System.Drawing.Size(69, 20)
        Me.txtWeightlong.TabIndex = 5
        Me.txtWeightlong.Text = "1"
        '
        'txtWeightshortt
        '
        Me.txtWeightshortt.AcceptsReturn = True
        Me.txtWeightshortt.AutoSize = False
        Me.txtWeightshortt.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightshortt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightshortt.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightshortt.Location = New System.Drawing.Point(19, 164)
        Me.txtWeightshortt.MaxLength = 0
        Me.txtWeightshortt.Name = "txtWeightshortt"
        Me.txtWeightshortt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightshortt.Size = New System.Drawing.Size(69, 19)
        Me.txtWeightshortt.TabIndex = 4
        Me.txtWeightshortt.Text = "1"
        '
        'txtWeightlongt
        '
        Me.txtWeightlongt.AcceptsReturn = True
        Me.txtWeightlongt.AutoSize = False
        Me.txtWeightlongt.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightlongt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightlongt.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightlongt.Location = New System.Drawing.Point(19, 129)
        Me.txtWeightlongt.MaxLength = 0
        Me.txtWeightlongt.Name = "txtWeightlongt"
        Me.txtWeightlongt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightlongt.Size = New System.Drawing.Size(69, 20)
        Me.txtWeightlongt.TabIndex = 3
        Me.txtWeightlongt.Text = "1"
        '
        'txtWeightgr
        '
        Me.txtWeightgr.AcceptsReturn = True
        Me.txtWeightgr.AutoSize = False
        Me.txtWeightgr.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightgr.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightgr.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightgr.Location = New System.Drawing.Point(19, 95)
        Me.txtWeightgr.MaxLength = 0
        Me.txtWeightgr.Name = "txtWeightgr"
        Me.txtWeightgr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightgr.Size = New System.Drawing.Size(69, 19)
        Me.txtWeightgr.TabIndex = 2
        Me.txtWeightgr.Text = "1"
        '
        'txtWeightoz
        '
        Me.txtWeightoz.AcceptsReturn = True
        Me.txtWeightoz.AutoSize = False
        Me.txtWeightoz.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightoz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightoz.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightoz.Location = New System.Drawing.Point(19, 60)
        Me.txtWeightoz.MaxLength = 0
        Me.txtWeightoz.Name = "txtWeightoz"
        Me.txtWeightoz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightoz.Size = New System.Drawing.Size(69, 20)
        Me.txtWeightoz.TabIndex = 1
        Me.txtWeightoz.Text = "1"
        '
        'txtWeightlb
        '
        Me.txtWeightlb.AcceptsReturn = True
        Me.txtWeightlb.AutoSize = False
        Me.txtWeightlb.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtWeightlb.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWeightlb.ForeColor = System.Drawing.Color.Blue
        Me.txtWeightlb.Location = New System.Drawing.Point(19, 26)
        Me.txtWeightlb.MaxLength = 0
        Me.txtWeightlb.Name = "txtWeightlb"
        Me.txtWeightlb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWeightlb.Size = New System.Drawing.Size(69, 19)
        Me.txtWeightlb.TabIndex = 0
        Me.txtWeightlb.Text = "1"
        '
        'frmunitWeight
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(493, 353)
        Me.Controls.Add(Me.cmdhelp)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdWeightozgr2)
        Me.Controls.Add(Me.cmdWeightozgr1)
        Me.Controls.Add(Me.cmdWeightlboz2)
        Me.Controls.Add(Me.cmdWeightlboz1)
        Me.Controls.Add(Me.cmdWeightls2)
        Me.Controls.Add(Me.cmdWeightls1)
        Me.Controls.Add(Me.cmdWeightshortt2)
        Me.Controls.Add(Me.cmdWeightshortt1)
        Me.Controls.Add(Me.cmdWeightlongt2)
        Me.Controls.Add(Me.cmdWeightlongt1)
        Me.Controls.Add(Me.cmdWeightgrmg2)
        Me.Controls.Add(Me.cmdWeightgrmg1)
        Me.Controls.Add(Me.cmdWeightozg2)
        Me.Controls.Add(Me.cmdWeightozg1)
        Me.Controls.Add(Me.cmdWeightlbkg2)
        Me.Controls.Add(Me.cmdWeightlbkg1)
        Me.Controls.Add(Me.txtWeightgr2)
        Me.Controls.Add(Me.txtWeightoz2)
        Me.Controls.Add(Me.txtWeightshort)
        Me.Controls.Add(Me.txtWeightt2)
        Me.Controls.Add(Me.txtWeightt)
        Me.Controls.Add(Me.txtWeightmg)
        Me.Controls.Add(Me.txtWeightg)
        Me.Controls.Add(Me.txtWeightkg)
        Me.Controls.Add(Me.txtWeightoz3)
        Me.Controls.Add(Me.txtWeightlb2)
        Me.Controls.Add(Me.txtWeightlong)
        Me.Controls.Add(Me.txtWeightshortt)
        Me.Controls.Add(Me.txtWeightlongt)
        Me.Controls.Add(Me.txtWeightgr)
        Me.Controls.Add(Me.txtWeightoz)
        Me.Controls.Add(Me.txtWeightlb)
        Me.Controls.Add(Me.labWeightgr2)
        Me.Controls.Add(Me.labWeightoz2)
        Me.Controls.Add(Me.labWeightshort)
        Me.Controls.Add(Me.labWeightt2)
        Me.Controls.Add(Me.labWeightt)
        Me.Controls.Add(Me.labWeightmg)
        Me.Controls.Add(Me.labWeightg)
        Me.Controls.Add(Me.labWeightkg)
        Me.Controls.Add(Me.labWeightoz3)
        Me.Controls.Add(Me.labWeightlb2)
        Me.Controls.Add(Me.labWeightlong)
        Me.Controls.Add(Me.labWeightshortt)
        Me.Controls.Add(Me.labWeightlongt)
        Me.Controls.Add(Me.labWeightgr)
        Me.Controls.Add(Me.labWeightoz)
        Me.Controls.Add(Me.labWeightlb)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitWeight"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "质量(Weight)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitWeight
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitWeight
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitWeight()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月2日17:51于青海省海西洲青海油田南八仙油区仙104井。8月1日加入纠错功能。
	'质量单位换算
	
	
	Dim lb As Single
	Dim gr As Single
	Dim oz As Single
	Dim longt As Single
	Dim shortt As Single
	
	Dim t As Single
	Dim kg As Single
	Dim g As Single
	Dim mg As Single
	
	Dim t2 As Single
	Dim lb2 As Single
	Dim gr2 As Single
	Dim oz2 As Single
	Dim oz3 As Single
	Dim lt As Single
	Dim st As Single
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdWeightgrmg1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightgrmg1.Click '格令与毫克的转换－－－－－－编号3－1
		mg = Val(txtWeightmg.Text)
		If txtWeightmg.Text = "" Or mg <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			gr = mg * 0.0154
		End If
		
		txtWeightgr.Text = CStr(gr)
	End Sub
	
	Private Sub cmdWeightgrmg2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightgrmg2.Click '格令与毫克的转换－－－－－－编号3－2
		gr = Val(txtWeightgr.Text)
		If txtWeightgr.Text = "" Or gr <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			mg = gr * 64.7989
		End If
		
		txtWeightmg.Text = CStr(mg)
	End Sub
	
	Private Sub cmdWeightlbkg1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlbkg1.Click '磅与公斤的转换－－－－－－编号1－1
		kg = Val(txtWeightkg.Text)
		If txtWeightkg.Text = "" Or kg <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			lb = kg * 2.2075
		End If
		
		txtWeightlb.Text = CStr(lb)
	End Sub
	
	Private Sub cmdWeightlbkg2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlbkg2.Click '磅与公斤的转换－－－－－－编号1－2
		
		lb = Val(txtWeightlb.Text)
		If txtWeightlb.Text = "" Or lb <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			kg = lb * 0.4536
		End If
		
		txtWeightkg.Text = CStr(kg)
	End Sub
	
	Private Sub cmdWeightlboz1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlboz1.Click '磅与盎司的转换－－－－－－－－编号7－1        '
		oz2 = Val(txtWeightoz2.Text)
		If txtWeightoz2.Text = "" Or oz2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			lb2 = oz2 * 0.0625
		End If
		
		txtWeightlb2.Text = CStr(lb2)
	End Sub
	
	Private Sub cmdWeightlboz2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlboz2.Click '磅与盎司的转换－－－－－－－－编号7－2
		lb2 = Val(txtWeightlb2.Text)
		If txtWeightlb2.Text = "" Or lb2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			oz2 = lb2 * 16#
		End If
		
		txtWeightoz2.Text = CStr(oz2)
	End Sub
	
	Private Sub cmdWeightlongt1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlongt1.Click '英吨与吨的转换－－－－－－编号4－1
		t = Val(txtWeightt.Text)
		If txtWeightt.Text = "" Or t <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			longt = t * 0.9842
		End If
		
		txtWeightlongt.Text = CStr(longt)
	End Sub
	
	Private Sub cmdWeightlongt2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightlongt2.Click '英吨与吨的转换－－－－－－编号4－2
		longt = Val(txtWeightlongt.Text)
		If txtWeightlongt.Text = "" Or longt <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			t = longt * 1.016
		End If
		
		txtWeightt.Text = CStr(t)
	End Sub
	
	Private Sub cmdWeightls1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightls1.Click '英吨与美吨的转换－－－－－－编号6－1
		st = Val(txtWeightshort.Text)
		If txtWeightshort.Text = "" Or st <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			lt = st * 0.8929
		End If
		
		txtWeightlong.Text = CStr(lt)
	End Sub
	
	Private Sub cmdWeightls2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightls2.Click '英吨与美吨的转换－－－－－－编号6－2
		lt = Val(txtWeightlong.Text)
		If txtWeightlong.Text = "" Or lt <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			st = lt * 1.12
		End If
		
		txtWeightshort.Text = CStr(st)
	End Sub
	
	Private Sub cmdWeightozg1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightozg1.Click '盎司与克的转换－－－－－－编号2－1
		g = Val(txtWeightg.Text)
		If txtWeightg.Text = "" Or g <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			oz = g * 0.0353
		End If
		
		txtWeightoz.Text = CStr(oz)
	End Sub
	
	Private Sub cmdWeightozg2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightozg2.Click '盎司与克的转换－－－－－－编号2－2
		oz = Val(txtWeightoz.Text)
		If txtWeightoz.Text = "" Or oz <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			g = oz * 28.35
		End If
		
		txtWeightg.Text = CStr(g)
	End Sub
	
	Private Sub cmdWeightozgr1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightozgr1.Click '盎司与格令的转换－－－－－－编号8－1
		gr2 = Val(txtWeightgr2.Text)
		If txtWeightgr2.Text = "" Or gr2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			oz3 = gr2 * 0.0023
		End If
		
		txtWeightoz3.Text = CStr(oz3)
	End Sub
	
	Private Sub cmdWeightozgr2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightozgr2.Click '盎司与格令的转换－－－－－－编号8－2
		oz3 = Val(txtWeightoz3.Text)
		If txtWeightoz3.Text = "" Or oz3 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			gr2 = oz3 * 437.5
		End If
		
		txtWeightgr2.Text = CStr(gr2)
	End Sub
	
	Private Sub cmdWeightshortt1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightshortt1.Click '美吨与吨的转换－－－－－－编号5－1
		t2 = Val(txtWeightt2.Text)
		If txtWeightt2.Text = "" Or t2 <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			shortt = t2 * 1.1023
		End If
		
		txtWeightshortt.Text = CStr(shortt)
	End Sub
	
	Private Sub cmdWeightshortt2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWeightshortt2.Click '美吨与吨的转换－－－－－－编号5－2
		shortt = Val(txtWeightshortt.Text)
		If txtWeightshortt.Text = "" Or shortt <= 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		Else
			
			t2 = shortt * 0.9072
		End If
		
		txtWeightt2.Text = CStr(t2)
	End Sub
	
	Private Sub txtWeightg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		g = Val(txtWeightg.Text)
	End Sub
	
	Private Sub txtWeightgr_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightgr.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gr = Val(txtWeightgr.Text)
	End Sub
	
	Private Sub txtWeightgr2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightgr2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gr2 = Val(txtWeightgr2.Text)
	End Sub
	
	Private Sub txtWeightkg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightkg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kg = Val(txtWeightkg.Text)
	End Sub
	
	Private Sub txtWeightlb_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightlb.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lb = Val(txtWeightlb.Text)
	End Sub
	
	Private Sub txtWeightlb2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightlb2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lb2 = Val(txtWeightlb2.Text)
	End Sub
	
	Private Sub txtWeightlong_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightlong.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lt = Val(txtWeightlong.Text)
	End Sub
	
	Private Sub txtWeightlongt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightlongt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		longt = Val(txtWeightlongt.Text)
	End Sub
	
	Private Sub txtWeightmg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightmg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mg = Val(txtWeightmg.Text)
	End Sub
	
	Private Sub txtWeightoz_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightoz.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		oz = Val(txtWeightoz.Text)
	End Sub
	
	Private Sub txtWeightoz2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightoz2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		oz2 = Val(txtWeightoz2.Text)
	End Sub
	
	Private Sub txtWeightoz3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightoz3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		oz3 = Val(txtWeightoz3.Text)
	End Sub
	
	Private Sub txtWeightshort_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightshort.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		st = Val(txtWeightshort.Text)
	End Sub
	
	Private Sub txtWeightshortt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightshortt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		shortt = Val(txtWeightshortt.Text)
	End Sub
	Private Sub txtWeightt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		t = Val(txtWeightt.Text)
	End Sub
	
	Private Sub txtWeightt2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWeightt2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		t2 = Val(txtWeightt2.Text)
	End Sub
End Class