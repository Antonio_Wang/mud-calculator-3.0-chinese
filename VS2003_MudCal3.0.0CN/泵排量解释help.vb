Option Strict Off
Option Explicit On
Friend Class frmHelpPumpVol
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmHelpPumpVol))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdHelp.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(203, 320)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(80, 20)
        Me.CmdHelp.TabIndex = 1
        Me.CmdHelp.Text = "确 定"
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtHelp.Location = New System.Drawing.Point(0, 0)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.Size = New System.Drawing.Size(473, 307)
        Me.txtHelp.TabIndex = 0
        Me.txtHelp.Text = ""
        '
        'frmHelpPumpVol
        '
        Me.AcceptButton = Me.CmdHelp
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 16)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.CmdHelp
        Me.ClientSize = New System.Drawing.Size(475, 359)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.txtHelp)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(3, 29)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHelpPumpVol"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "由泵参数计算排量模块使用说明"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmHelpPumpVol
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmHelpPumpVol
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmHelpPumpVol()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Private Sub frmHelpPumpVol_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		Dim CRLF As String
		CRLF = Chr(13) & Chr(10)
		
		Dim Msg As String
		Msg = CRLF & "                 根据泥浆泵的基本参数来计算当前排量大小" & CRLF & CRLF
        Msg = Msg & "1.泥浆泵的缸套个数可以达到对'单缸'、'双缸'、'三缸'等泥浆泵类型的选择； " & CRLF & CRLF
        Msg = Msg & "2.通过对泥浆泵个数的数值输入，可以选择现场配备的泥浆泵是'单泵'还是'双泵'； " & CRLF & CRLF
        Msg = Msg & "3.通过对'泵型1'和'泵型2'的参数输入变化，可以达到现场配备'不同型号泵'和'相同型号泵'的实际要求。" & CRLF & CRLF
        Msg = Msg & "4.通过泵'上水效率'的不同取值，保证计算出的排量是实际得到的排量。" & CRLF & CRLF
        Msg = Msg & "5.通过同时选择'泵型1'和'泵型2'但变换各泵'缸套直径'、'缸套数量'的取值，可计算单泵三缸两种缸套直径的排量计算。" & CRLF & CRLF
		Msg = Msg & "    本模块的数据输入窗口模仿胜利油田钻井工艺研究院钻井信息中心《钻井工程计算软件包 V1.2》形式设计，特此声明。" & CRLF
		
		
		txtHelp.Text = Msg
		
	End Sub
	
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		Me.Close()
	End Sub
End Class