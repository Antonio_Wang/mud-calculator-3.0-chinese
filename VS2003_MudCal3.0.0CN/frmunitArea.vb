Option Strict Off
Option Explicit On
Friend Class frmunitArea
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtAreakm2 As System.Windows.Forms.TextBox
	Public WithEvents txtAream2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreadm2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreacm2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreamm2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreaha As System.Windows.Forms.TextBox
	Public WithEvents txtAreain2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreaft2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreayd2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreamile2 As System.Windows.Forms.TextBox
	Public WithEvents txtAreaacre As System.Windows.Forms.TextBox
	Public WithEvents txtAreaChinaac As System.Windows.Forms.TextBox
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents lblAreakm2 As System.Windows.Forms.Label
	Public WithEvents lblAream2 As System.Windows.Forms.Label
	Public WithEvents lblAreadm2 As System.Windows.Forms.Label
	Public WithEvents lblAreacm2 As System.Windows.Forms.Label
	Public WithEvents lblAreamm2 As System.Windows.Forms.Label
	Public WithEvents lblAreaha As System.Windows.Forms.Label
	Public WithEvents lblAreain2 As System.Windows.Forms.Label
	Public WithEvents lblAreaft2 As System.Windows.Forms.Label
	Public WithEvents lblAreayd2 As System.Windows.Forms.Label
	Public WithEvents lblAreaMile2 As System.Windows.Forms.Label
	Public WithEvents lblAreaacre As System.Windows.Forms.Label
	Public WithEvents lblAreaChinaac As System.Windows.Forms.Label
	Public WithEvents lblArea As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmunitArea))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtAreakm2 = New System.Windows.Forms.TextBox
        Me.txtAream2 = New System.Windows.Forms.TextBox
        Me.txtAreadm2 = New System.Windows.Forms.TextBox
        Me.txtAreacm2 = New System.Windows.Forms.TextBox
        Me.txtAreamm2 = New System.Windows.Forms.TextBox
        Me.txtAreaha = New System.Windows.Forms.TextBox
        Me.txtAreain2 = New System.Windows.Forms.TextBox
        Me.txtAreaft2 = New System.Windows.Forms.TextBox
        Me.txtAreayd2 = New System.Windows.Forms.TextBox
        Me.txtAreamile2 = New System.Windows.Forms.TextBox
        Me.txtAreaacre = New System.Windows.Forms.TextBox
        Me.txtAreaChinaac = New System.Windows.Forms.TextBox
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.lblAreakm2 = New System.Windows.Forms.Label
        Me.lblAream2 = New System.Windows.Forms.Label
        Me.lblAreadm2 = New System.Windows.Forms.Label
        Me.lblAreacm2 = New System.Windows.Forms.Label
        Me.lblAreamm2 = New System.Windows.Forms.Label
        Me.lblAreaha = New System.Windows.Forms.Label
        Me.lblAreain2 = New System.Windows.Forms.Label
        Me.lblAreaft2 = New System.Windows.Forms.Label
        Me.lblAreayd2 = New System.Windows.Forms.Label
        Me.lblAreaMile2 = New System.Windows.Forms.Label
        Me.lblAreaacre = New System.Windows.Forms.Label
        Me.lblAreaChinaac = New System.Windows.Forms.Label
        Me.lblArea = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtAreakm2
        '
        Me.txtAreakm2.AcceptsReturn = True
        Me.txtAreakm2.AutoSize = False
        Me.txtAreakm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreakm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreakm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreakm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreakm2.Location = New System.Drawing.Point(10, 52)
        Me.txtAreakm2.MaxLength = 0
        Me.txtAreakm2.Name = "txtAreakm2"
        Me.txtAreakm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreakm2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreakm2.TabIndex = 13
        Me.txtAreakm2.Text = ""
        '
        'txtAream2
        '
        Me.txtAream2.AcceptsReturn = True
        Me.txtAream2.AutoSize = False
        Me.txtAream2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAream2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAream2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAream2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAream2.Location = New System.Drawing.Point(10, 121)
        Me.txtAream2.MaxLength = 0
        Me.txtAream2.Name = "txtAream2"
        Me.txtAream2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAream2.Size = New System.Drawing.Size(126, 19)
        Me.txtAream2.TabIndex = 12
        Me.txtAream2.Text = ""
        '
        'txtAreadm2
        '
        Me.txtAreadm2.AcceptsReturn = True
        Me.txtAreadm2.AutoSize = False
        Me.txtAreadm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreadm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreadm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreadm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreadm2.Location = New System.Drawing.Point(10, 155)
        Me.txtAreadm2.MaxLength = 0
        Me.txtAreadm2.Name = "txtAreadm2"
        Me.txtAreadm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreadm2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreadm2.TabIndex = 11
        Me.txtAreadm2.Text = ""
        '
        'txtAreacm2
        '
        Me.txtAreacm2.AcceptsReturn = True
        Me.txtAreacm2.AutoSize = False
        Me.txtAreacm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreacm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreacm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreacm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreacm2.Location = New System.Drawing.Point(10, 190)
        Me.txtAreacm2.MaxLength = 0
        Me.txtAreacm2.Name = "txtAreacm2"
        Me.txtAreacm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreacm2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreacm2.TabIndex = 10
        Me.txtAreacm2.Text = ""
        '
        'txtAreamm2
        '
        Me.txtAreamm2.AcceptsReturn = True
        Me.txtAreamm2.AutoSize = False
        Me.txtAreamm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreamm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreamm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreamm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreamm2.Location = New System.Drawing.Point(10, 224)
        Me.txtAreamm2.MaxLength = 0
        Me.txtAreamm2.Name = "txtAreamm2"
        Me.txtAreamm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreamm2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreamm2.TabIndex = 9
        Me.txtAreamm2.Text = ""
        '
        'txtAreaha
        '
        Me.txtAreaha.AcceptsReturn = True
        Me.txtAreaha.AutoSize = False
        Me.txtAreaha.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreaha.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreaha.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreaha.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreaha.Location = New System.Drawing.Point(10, 86)
        Me.txtAreaha.MaxLength = 0
        Me.txtAreaha.Name = "txtAreaha"
        Me.txtAreaha.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreaha.Size = New System.Drawing.Size(126, 20)
        Me.txtAreaha.TabIndex = 8
        Me.txtAreaha.Text = ""
        '
        'txtAreain2
        '
        Me.txtAreain2.AcceptsReturn = True
        Me.txtAreain2.AutoSize = False
        Me.txtAreain2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreain2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreain2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreain2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreain2.Location = New System.Drawing.Point(278, 52)
        Me.txtAreain2.MaxLength = 0
        Me.txtAreain2.Name = "txtAreain2"
        Me.txtAreain2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreain2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreain2.TabIndex = 7
        Me.txtAreain2.Text = ""
        '
        'txtAreaft2
        '
        Me.txtAreaft2.AcceptsReturn = True
        Me.txtAreaft2.AutoSize = False
        Me.txtAreaft2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreaft2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreaft2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreaft2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreaft2.Location = New System.Drawing.Point(278, 86)
        Me.txtAreaft2.MaxLength = 0
        Me.txtAreaft2.Name = "txtAreaft2"
        Me.txtAreaft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreaft2.Size = New System.Drawing.Size(126, 20)
        Me.txtAreaft2.TabIndex = 6
        Me.txtAreaft2.Text = ""
        '
        'txtAreayd2
        '
        Me.txtAreayd2.AcceptsReturn = True
        Me.txtAreayd2.AutoSize = False
        Me.txtAreayd2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreayd2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreayd2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreayd2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreayd2.Location = New System.Drawing.Point(278, 121)
        Me.txtAreayd2.MaxLength = 0
        Me.txtAreayd2.Name = "txtAreayd2"
        Me.txtAreayd2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreayd2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreayd2.TabIndex = 5
        Me.txtAreayd2.Text = ""
        '
        'txtAreamile2
        '
        Me.txtAreamile2.AcceptsReturn = True
        Me.txtAreamile2.AutoSize = False
        Me.txtAreamile2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreamile2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreamile2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreamile2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreamile2.Location = New System.Drawing.Point(278, 155)
        Me.txtAreamile2.MaxLength = 0
        Me.txtAreamile2.Name = "txtAreamile2"
        Me.txtAreamile2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreamile2.Size = New System.Drawing.Size(126, 19)
        Me.txtAreamile2.TabIndex = 4
        Me.txtAreamile2.Text = ""
        '
        'txtAreaacre
        '
        Me.txtAreaacre.AcceptsReturn = True
        Me.txtAreaacre.AutoSize = False
        Me.txtAreaacre.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreaacre.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreaacre.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreaacre.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreaacre.Location = New System.Drawing.Point(278, 190)
        Me.txtAreaacre.MaxLength = 0
        Me.txtAreaacre.Name = "txtAreaacre"
        Me.txtAreaacre.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreaacre.Size = New System.Drawing.Size(126, 19)
        Me.txtAreaacre.TabIndex = 3
        Me.txtAreaacre.Text = ""
        '
        'txtAreaChinaac
        '
        Me.txtAreaChinaac.AcceptsReturn = True
        Me.txtAreaChinaac.AutoSize = False
        Me.txtAreaChinaac.BackColor = System.Drawing.SystemColors.Window
        Me.txtAreaChinaac.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAreaChinaac.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtAreaChinaac.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAreaChinaac.Location = New System.Drawing.Point(278, 224)
        Me.txtAreaChinaac.MaxLength = 0
        Me.txtAreaChinaac.Name = "txtAreaChinaac"
        Me.txtAreaChinaac.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAreaChinaac.Size = New System.Drawing.Size(126, 19)
        Me.txtAreaChinaac.TabIndex = 2
        Me.txtAreaChinaac.Text = ""
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(106, 267)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 1
        Me.cmdquit.Text = "退出"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(355, 267)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 0
        Me.cmdclear.Text = "清除"
        '
        'lblAreakm2
        '
        Me.lblAreakm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreakm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreakm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreakm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreakm2.Location = New System.Drawing.Point(154, 52)
        Me.lblAreakm2.Name = "lblAreakm2"
        Me.lblAreakm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreakm2.Size = New System.Drawing.Size(106, 18)
        Me.lblAreakm2.TabIndex = 26
        Me.lblAreakm2.Text = "Km2(平方千米)"
        '
        'lblAream2
        '
        Me.lblAream2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAream2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAream2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAream2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAream2.Location = New System.Drawing.Point(154, 121)
        Me.lblAream2.Name = "lblAream2"
        Me.lblAream2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAream2.Size = New System.Drawing.Size(106, 18)
        Me.lblAream2.TabIndex = 25
        Me.lblAream2.Text = "m2(平方米)"
        '
        'lblAreadm2
        '
        Me.lblAreadm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreadm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreadm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreadm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreadm2.Location = New System.Drawing.Point(154, 155)
        Me.lblAreadm2.Name = "lblAreadm2"
        Me.lblAreadm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreadm2.Size = New System.Drawing.Size(106, 18)
        Me.lblAreadm2.TabIndex = 24
        Me.lblAreadm2.Text = "dm2(平方分米)"
        '
        'lblAreacm2
        '
        Me.lblAreacm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreacm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreacm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreacm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreacm2.Location = New System.Drawing.Point(154, 190)
        Me.lblAreacm2.Name = "lblAreacm2"
        Me.lblAreacm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreacm2.Size = New System.Drawing.Size(106, 18)
        Me.lblAreacm2.TabIndex = 23
        Me.lblAreacm2.Text = "cm2(平方厘米)"
        '
        'lblAreamm2
        '
        Me.lblAreamm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreamm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreamm2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreamm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreamm2.Location = New System.Drawing.Point(154, 224)
        Me.lblAreamm2.Name = "lblAreamm2"
        Me.lblAreamm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreamm2.Size = New System.Drawing.Size(106, 18)
        Me.lblAreamm2.TabIndex = 22
        Me.lblAreamm2.Text = "mm2(平方毫米)"
        '
        'lblAreaha
        '
        Me.lblAreaha.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreaha.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreaha.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreaha.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreaha.Location = New System.Drawing.Point(154, 86)
        Me.lblAreaha.Name = "lblAreaha"
        Me.lblAreaha.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreaha.Size = New System.Drawing.Size(106, 18)
        Me.lblAreaha.TabIndex = 21
        Me.lblAreaha.Text = "ha(公顷)"
        '
        'lblAreain2
        '
        Me.lblAreain2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreain2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreain2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreain2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreain2.Location = New System.Drawing.Point(422, 52)
        Me.lblAreain2.Name = "lblAreain2"
        Me.lblAreain2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreain2.Size = New System.Drawing.Size(107, 18)
        Me.lblAreain2.TabIndex = 20
        Me.lblAreain2.Text = "in2(平方英寸)"
        '
        'lblAreaft2
        '
        Me.lblAreaft2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreaft2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreaft2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreaft2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreaft2.Location = New System.Drawing.Point(422, 86)
        Me.lblAreaft2.Name = "lblAreaft2"
        Me.lblAreaft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreaft2.Size = New System.Drawing.Size(107, 18)
        Me.lblAreaft2.TabIndex = 19
        Me.lblAreaft2.Text = "ft2(平方英尺)"
        '
        'lblAreayd2
        '
        Me.lblAreayd2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreayd2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreayd2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreayd2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreayd2.Location = New System.Drawing.Point(422, 121)
        Me.lblAreayd2.Name = "lblAreayd2"
        Me.lblAreayd2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreayd2.Size = New System.Drawing.Size(107, 18)
        Me.lblAreayd2.TabIndex = 18
        Me.lblAreayd2.Text = "yd2(平方码)"
        '
        'lblAreaMile2
        '
        Me.lblAreaMile2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreaMile2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreaMile2.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreaMile2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreaMile2.Location = New System.Drawing.Point(422, 155)
        Me.lblAreaMile2.Name = "lblAreaMile2"
        Me.lblAreaMile2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreaMile2.Size = New System.Drawing.Size(126, 18)
        Me.lblAreaMile2.TabIndex = 17
        Me.lblAreaMile2.Text = "mile2(平方英哩)"
        '
        'lblAreaacre
        '
        Me.lblAreaacre.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreaacre.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreaacre.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreaacre.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreaacre.Location = New System.Drawing.Point(422, 190)
        Me.lblAreaacre.Name = "lblAreaacre"
        Me.lblAreaacre.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreaacre.Size = New System.Drawing.Size(107, 18)
        Me.lblAreaacre.TabIndex = 16
        Me.lblAreaacre.Text = "acre(英亩)"
        '
        'lblAreaChinaac
        '
        Me.lblAreaChinaac.BackColor = System.Drawing.SystemColors.Control
        Me.lblAreaChinaac.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAreaChinaac.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAreaChinaac.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAreaChinaac.Location = New System.Drawing.Point(422, 224)
        Me.lblAreaChinaac.Name = "lblAreaChinaac"
        Me.lblAreaChinaac.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAreaChinaac.Size = New System.Drawing.Size(107, 18)
        Me.lblAreaChinaac.TabIndex = 15
        Me.lblAreaChinaac.Text = "(市亩)"
        '
        'lblArea
        '
        Me.lblArea.BackColor = System.Drawing.SystemColors.Control
        Me.lblArea.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblArea.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblArea.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblArea.Location = New System.Drawing.Point(10, 17)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblArea.Size = New System.Drawing.Size(250, 19)
        Me.lblArea.TabIndex = 14
        Me.lblArea.Text = "输入面积单位换算数据："
        '
        'frmunitArea
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(555, 320)
        Me.Controls.Add(Me.txtAreakm2)
        Me.Controls.Add(Me.txtAream2)
        Me.Controls.Add(Me.txtAreadm2)
        Me.Controls.Add(Me.txtAreacm2)
        Me.Controls.Add(Me.txtAreamm2)
        Me.Controls.Add(Me.txtAreaha)
        Me.Controls.Add(Me.txtAreain2)
        Me.Controls.Add(Me.txtAreaft2)
        Me.Controls.Add(Me.txtAreayd2)
        Me.Controls.Add(Me.txtAreamile2)
        Me.Controls.Add(Me.txtAreaacre)
        Me.Controls.Add(Me.txtAreaChinaac)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.lblAreakm2)
        Me.Controls.Add(Me.lblAream2)
        Me.Controls.Add(Me.lblAreadm2)
        Me.Controls.Add(Me.lblAreacm2)
        Me.Controls.Add(Me.lblAreamm2)
        Me.Controls.Add(Me.lblAreaha)
        Me.Controls.Add(Me.lblAreain2)
        Me.Controls.Add(Me.lblAreaft2)
        Me.Controls.Add(Me.lblAreayd2)
        Me.Controls.Add(Me.lblAreaMile2)
        Me.Controls.Add(Me.lblAreaacre)
        Me.Controls.Add(Me.lblAreaChinaac)
        Me.Controls.Add(Me.lblArea)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitArea"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "面积(Area)单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitArea
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitArea
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitArea()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月10日23:43于青海省海西洲马海马北油田,,,,备注：界面英语化：秘鲁 Talara  2012 April 17
	'面积单位换算
	
	Dim km2 As Single
	Dim m2 As Single
	Dim dm2 As Single
	Dim cm2 As Single
	Dim mm2 As Single
	Dim ha As Single
	Dim Inch2 As Single
	Dim ft2 As Single
	Dim yd2 As Single
	Dim mile2 As Single
	Dim acre As Single
	Dim ChinaAc As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitArea.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtAreacm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreacm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方厘米square centimetre－－输入数据的计算过程
		cm2 = Val(txtAreacm2.Text)
		
		km2 = cm2 / 10000000000#
		dm2 = cm2 / 100
		m2 = cm2 / 10000
		mm2 = cm2 * 100
		ha = cm2 / 100000000#
		Inch2 = cm2 / 6.45 '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = cm2 / 929 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = cm2 / 10000 / 0.836 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = cm2 / 10000000000# / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = cm2 / 10000 / 4046.856 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = cm2 / 10000 / 666.6667 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreadm2.Text = CStr(dm2)
		txtAream2.Text = CStr(m2)
		txtAreamm2.Text = CStr(mm2)
		txtAreaha.Text = CStr(ha)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreadm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreadm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方分米－－输入数据的计算过程
		dm2 = Val(txtAreadm2.Text)
		km2 = dm2 / 100000000#
		m2 = dm2 / 100
		cm2 = dm2 * 100
		mm2 = dm2 * 10000
		ha = dm2 / 1000000
		Inch2 = dm2 / 0.064516 '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = dm2 / 9.2903 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = dm2 / 83.6127 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = dm2 / 100000000# / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = dm2 / 404685.6 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = dm2 / 66666.67 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreacm2.Text = CStr(cm2)
		txtAream2.Text = CStr(m2)
		txtAreamm2.Text = CStr(mm2)
		txtAreaha.Text = CStr(ha)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreaft2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreaft2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方英尺－－输入数据的计算过程
		ft2 = Val(txtAreaft2.Text)
		
		km2 = ft2 / 10763910#
		m2 = ft2 / 10.7639
		dm2 = ft2 / 0.1076
		cm2 = ft2 / 0.00107639
		mm2 = ft2 / 0.000010764
		ha = ft2 / 107639#
		Inch2 = ft2 / 0.0069 '1 in2=6.45cm2     1cm2=0.155in2
		'1 ft2=929cm2      1m2=10.8ft2
		yd2 = ft2 / 9 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = ft2 / 10763910# / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = ft2 / 43560 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = ft2 / 7175.9403 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreaha.Text = CStr(ha)
		txtAreain2.Text = CStr(Inch2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreaChinaac_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreaChinaac.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '市亩－－输入数据的计算过程
		ChinaAc = Val(txtAreaChinaac.Text)
		
		km2 = ChinaAc / 1500
		ha = ChinaAc / 15
		m2 = ChinaAc / 0.0015
		dm2 = ChinaAc / 0.000015
		cm2 = ChinaAc / 0.00000015
		mm2 = ChinaAc / 0.0000000015
		Inch2 = ChinaAc / 0.00000096774 '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = ChinaAc / 0.00013935 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = ChinaAc / 0.0013 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = ChinaAc / 1500 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = ChinaAc / 6.0703 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		'1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		
	End Sub
	
	Private Sub txtAreain2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreain2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方英寸－－输入数据的计算过程
		Inch2 = Val(txtAreain2.Text)
		
		km2 = Inch2 / 1550000000#
		ha = Inch2 / 15500000#
		m2 = Inch2 / 1550.0031
		dm2 = Inch2 / 15.5
		cm2 = Inch2 / 0.155
		mm2 = Inch2 / 0.00155
		'1 in2=6.45cm2     1cm2=0.155in2
		ft2 = Inch2 / 144 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = Inch2 / 1296 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = Inch2 / 1550000000# / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = Inch2 / 6272600# '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = Inch2 / 1033300# '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreakm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreakm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方千米－－输入数据的计算过程
		km2 = Val(txtAreakm2.Text)
		
		ha = km2 * 100
		m2 = km2 * 1000000
		dm2 = km2 * 100000000#
		cm2 = km2 * 10000000000#
		mm2 = km2 * 1000000000000#
		
		Inch2 = km2 * 1550000000# '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = km2 * 10764000# '1 ft2=929cm2      1m2=10.8ft2
		yd2 = km2 * 1196000# '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = km2 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = km2 * 247.1054 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = km2 * 1500 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	
	Private Sub txtAream2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAream2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方米－－输入数据的计算过程
		m2 = Val(txtAream2.Text)
		
		km2 = m2 * 0.000001
		ha = m2 * 0.0001
		dm2 = m2 * 100
		cm2 = m2 * 10000
		mm2 = m2 * 1000000
		
		Inch2 = m2 * 1550.0031 '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = m2 * 10.7639 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = m2 * 1.196 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = m2 * 0.000001 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = m2 * 0.0002471054 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = m2 * 0.0015 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreamile2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreamile2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方英里－－输入数据的计算过程
		mile2 = Val(txtAreamile2.Text)
		
		km2 = mile2 * 2.59
		ha = mile2 * 2.59 * 100
		m2 = mile2 * 2.59 * 1000000#
		dm2 = mile2 * 2.59 * 100000000#
		cm2 = mile2 * 2.59 * 10000000000#
		mm2 = mile2 * 2.59 * 1000000000000#
		
		Inch2 = mile2 * 2.59 * 1550000000# '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = mile2 * 2.59 * 10764000# '1 ft2=929cm2      1m2=10.8ft2
		yd2 = mile2 * 2.59 * 1196000# '1 yd2=0.836m2     1m2=1.20yd2
		'1 mile2=2.59km2   1km2=0.38square mile
		acre = mile2 * 2.59 * 247.1054 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = mile2 * 2.59 * 1500 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreamm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreamm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '平方毫米－－输入数据的计算过程
		mm2 = Val(txtAreamm2.Text)
		
		km2 = mm2 * 0.000000000001
		ha = mm2 * 0.0000000001
		m2 = mm2 * 0.000001
		dm2 = mm2 * 0.0001
		cm2 = mm2 * 0.01
		
		Inch2 = mm2 * 0.0016 '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = mm2 * 0.000010764 '1 ft2=929cm2      1m2=10.8ft2
		yd2 = mm2 * 0.000001196 '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = mm2 * 0.000000000001 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = mm2 * 0.00000000024711 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = mm2 * 0.0000000015 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreaha_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreaha.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '公顷－－输入数据的计算过程
		ha = Val(txtAreaha.Text)
		
		km2 = ha * 0.01
		m2 = ha * 10000#
		dm2 = ha * 1000000#
		cm2 = ha * 100000000#
		mm2 = ha * 1000000000000#
		
		Inch2 = ha * 15500000# '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = ha * 107640# '1 ft2=929cm2      1m2=10.8ft2
		yd2 = ha * 11960# '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = ha * 0.01 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = ha * 2.4711 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = ha * 15 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		txtAreaacre.Text = CStr(acre)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreaacre_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreaacre.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英亩－－－输入数据的计算过程
		acre = Val(txtAreaacre.Text)
		
		km2 = acre * 0.004
		ha = acre * 0.4047
		m2 = acre * 4046.856
		dm2 = acre * 404690#
		cm2 = acre * 40468560#
		mm2 = acre * 4046900000#
		
		Inch2 = acre * 6272600# '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = acre * 43560# '1 ft2=929cm2      1m2=10.8ft2
		yd2 = acre * 4840# '1 yd2=0.836m2     1m2=1.20yd2
		mile2 = acre * 0.004 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		'1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = acre * 6.0703 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreayd2.Text = CStr(yd2)
		txtAreamile2.Text = CStr(mile2)
		
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
	
	Private Sub txtAreayd2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAreayd2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制平方码－－输入数据的计算过程
		yd2 = Val(txtAreayd2.Text)
		
		km2 = yd2 * 0.00000083613
		ha = yd2 * 0.000083613
		m2 = yd2 * 0.8361
		dm2 = yd2 * 83.6127
		cm2 = yd2 * 8361.2736
		mm2 = yd2 * 836130#
		
		Inch2 = yd2 * 1296# '1 in2=6.45cm2     1cm2=0.155in2
		ft2 = yd2 * 9# '1 ft2=929cm2      1m2=10.8ft2
		'1 yd2=0.836m2     1m2=1.20yd2
		mile2 = yd2 * 0.00000083613 / 2.59 '1 mile2=2.59km2   1km2=0.38square mile
		acre = yd2 * 0.00020661 '1ha=2.47ac        1ac=0.405ha          1ac=4046.8560m2
		ChinaAc = yd2 * 0.0013 '1 市亩=0.1647ac   1 市亩=666.6667m2    1ac=6.0703市亩
		
		txtAreakm2.Text = CStr(km2)
		txtAreaha.Text = CStr(ha)
		txtAream2.Text = CStr(m2)
		txtAreadm2.Text = CStr(dm2)
		txtAreacm2.Text = CStr(cm2)
		txtAreamm2.Text = CStr(mm2)
		txtAreain2.Text = CStr(Inch2)
		txtAreaft2.Text = CStr(ft2)
		txtAreaacre.Text = CStr(acre)
		txtAreamile2.Text = CStr(mile2)
		txtAreaChinaac.Text = CStr(ChinaAc)
	End Sub
End Class