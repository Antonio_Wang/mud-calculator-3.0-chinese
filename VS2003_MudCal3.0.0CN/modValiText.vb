Option Strict Off
Option Explicit On
Module modValiText
	'在某些应用程序中，我们需要限制在文本框或其它一些控件中只能输入数字或一些特定的字符，现在我们可以通过下面的一个函数来实现此功能：
	'方法：在需要限制输入的控件的 KeyPress 加入以下代码：
	
	'KeyAscii = ValiText(KeyAscii, "0123456789/-", True)
	
	'现在你就可以过虑掉你不希望的字符了。在此例中，我们只接受第二个参数提供的字符，即："0123456789/-"
	
	'而此函数的第三个参数就决定了能否使用 [Backspace] 键。最后值得一提的是此函数对大小写是不敏感的。
	Public Function ValiText(ByRef KeyIn As Short, ByRef ValidateString As String, ByRef Editable As Boolean) As Short
		
		Dim ValidateList As String
		
		Dim KeyOut As Short
		
		If Editable = True Then
			
			ValidateList = UCase(ValidateString) & Chr(8)
			
		Else
			
			ValidateList = UCase(ValidateString)
			
		End If
		
		If InStr(1, ValidateList, UCase(Chr(KeyIn)), 1) > 0 Then
			
			KeyOut = KeyIn
			
		Else
			
			KeyOut = 0
			
			Beep()
			
		End If
		
		ValiText = KeyOut
		
	End Function
End Module