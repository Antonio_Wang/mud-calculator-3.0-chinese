Option Strict Off
Option Explicit On
Friend Class frmMudweight
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents Text19 As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents Label29 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents _Label19_1 As System.Windows.Forms.Label
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents Command1 As System.Windows.Forms.Button
	Public WithEvents Command5 As System.Windows.Forms.Button
	Public WithEvents cmdHelp As System.Windows.Forms.Button
	Public WithEvents Label27 As System.Windows.Forms.Label
	Public WithEvents Frame9 As System.Windows.Forms.GroupBox
	Public WithEvents _SSTab1_TabPage0 As System.Windows.Forms.TabPage
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Text20 As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents Label28 As System.Windows.Forms.Label
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Frame6 As System.Windows.Forms.GroupBox
	Public WithEvents Command2 As System.Windows.Forms.Button
	Public WithEvents Label26 As System.Windows.Forms.Label
	Public WithEvents Frame10 As System.Windows.Forms.GroupBox
	Public WithEvents Command6 As System.Windows.Forms.Button
	Public WithEvents _SSTab1_TabPage1 As System.Windows.Forms.TabPage
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents Text17 As System.Windows.Forms.TextBox
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Frame7 As System.Windows.Forms.GroupBox
	Public WithEvents Command3 As System.Windows.Forms.Button
	Public WithEvents Command7 As System.Windows.Forms.Button
	Public WithEvents Label25 As System.Windows.Forms.Label
	Public WithEvents Frame11 As System.Windows.Forms.GroupBox
	Public WithEvents _SSTab1_TabPage2 As System.Windows.Forms.TabPage
	Public WithEvents Text12 As System.Windows.Forms.TextBox
	Public WithEvents Text13 As System.Windows.Forms.TextBox
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents Label23 As System.Windows.Forms.Label
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Frame8 As System.Windows.Forms.GroupBox
	Public WithEvents Command4 As System.Windows.Forms.Button
	Public WithEvents Command8 As System.Windows.Forms.Button
	Public WithEvents Label24 As System.Windows.Forms.Label
	Public WithEvents Frame12 As System.Windows.Forms.GroupBox
	Public WithEvents _SSTab1_TabPage3 As System.Windows.Forms.TabPage
	Public WithEvents SSTab1 As System.Windows.Forms.TabControl
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents _Label19_0 As System.Windows.Forms.Label
	Public WithEvents Label19 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picMixBarite As System.Windows.Forms.PictureBox
    Public WithEvents picMudMW4 As System.Windows.Forms.PictureBox
    Public WithEvents picMudMW3 As System.Windows.Forms.PictureBox
    Public WithEvents picCutBackMW As System.Windows.Forms.PictureBox
    Public WithEvents TextBox1 As System.Windows.Forms.TextBox
    Public WithEvents Label30 As System.Windows.Forms.Label
    Public WithEvents TextBox2 As System.Windows.Forms.TextBox
    Public WithEvents Label31 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMudweight))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text8 = New System.Windows.Forms.TextBox
        Me.picMixBarite = New System.Windows.Forms.PictureBox
        Me.picMudMW4 = New System.Windows.Forms.PictureBox
        Me.picCutBackMW = New System.Windows.Forms.PictureBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.SSTab1 = New System.Windows.Forms.TabControl
        Me._SSTab1_TabPage0 = New System.Windows.Forms.TabPage
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Frame5 = New System.Windows.Forms.GroupBox
        Me.Text19 = New System.Windows.Forms.TextBox
        Me.Text15 = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me._Label19_1 = New System.Windows.Forms.Label
        Me.Command1 = New System.Windows.Forms.Button
        Me.Command5 = New System.Windows.Forms.Button
        Me.cmdHelp = New System.Windows.Forms.Button
        Me.Frame9 = New System.Windows.Forms.GroupBox
        Me.Label27 = New System.Windows.Forms.Label
        Me._SSTab1_TabPage1 = New System.Windows.Forms.TabPage
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Frame6 = New System.Windows.Forms.GroupBox
        Me.Text20 = New System.Windows.Forms.TextBox
        Me.Text16 = New System.Windows.Forms.TextBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Command2 = New System.Windows.Forms.Button
        Me.Frame10 = New System.Windows.Forms.GroupBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Command6 = New System.Windows.Forms.Button
        Me._SSTab1_TabPage2 = New System.Windows.Forms.TabPage
        Me.picMudMW3 = New System.Windows.Forms.PictureBox
        Me.Frame3 = New System.Windows.Forms.GroupBox
        Me.Text9 = New System.Windows.Forms.TextBox
        Me.Text10 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Frame7 = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Text17 = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Command3 = New System.Windows.Forms.Button
        Me.Command7 = New System.Windows.Forms.Button
        Me.Frame11 = New System.Windows.Forms.GroupBox
        Me.Label25 = New System.Windows.Forms.Label
        Me._SSTab1_TabPage3 = New System.Windows.Forms.TabPage
        Me.Frame4 = New System.Windows.Forms.GroupBox
        Me.Text12 = New System.Windows.Forms.TextBox
        Me.Text13 = New System.Windows.Forms.TextBox
        Me.Text14 = New System.Windows.Forms.TextBox
        Me.Text11 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Frame8 = New System.Windows.Forms.GroupBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Text18 = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Command4 = New System.Windows.Forms.Button
        Me.Command8 = New System.Windows.Forms.Button
        Me.Frame12 = New System.Windows.Forms.GroupBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me._Label19_0 = New System.Windows.Forms.Label
        Me.Label19 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        Me.SSTab1.SuspendLayout()
        Me._SSTab1_TabPage0.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.Frame5.SuspendLayout()
        Me.Frame9.SuspendLayout()
        Me._SSTab1_TabPage1.SuspendLayout()
        Me.Frame2.SuspendLayout()
        Me.Frame6.SuspendLayout()
        Me.Frame10.SuspendLayout()
        Me._SSTab1_TabPage2.SuspendLayout()
        Me.Frame3.SuspendLayout()
        Me.Frame7.SuspendLayout()
        Me.Frame11.SuspendLayout()
        Me._SSTab1_TabPage3.SuspendLayout()
        Me.Frame4.SuspendLayout()
        Me.Frame8.SuspendLayout()
        Me.Frame12.SuspendLayout()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(296, 152)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(106, 20)
        Me.Text4.TabIndex = 4
        Me.Text4.Text = ""
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text4, "重晶石一般为4.2g/cm^3,石灰石一般为2.7g/cm^3。")
        '
        'Text8
        '
        Me.Text8.AcceptsReturn = True
        Me.Text8.AutoSize = False
        Me.Text8.BackColor = System.Drawing.SystemColors.Window
        Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text8.Location = New System.Drawing.Point(296, 152)
        Me.Text8.MaxLength = 0
        Me.Text8.Name = "Text8"
        Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text8.Size = New System.Drawing.Size(106, 20)
        Me.Text8.TabIndex = 11
        Me.Text8.Text = ""
        Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text8, "重晶石一般为4.2g/cm^3,石灰石一般为2.7g/cm^3。")
        '
        'picMixBarite
        '
        Me.picMixBarite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picMixBarite.Image = CType(resources.GetObject("picMixBarite.Image"), System.Drawing.Image)
        Me.picMixBarite.Location = New System.Drawing.Point(552, 104)
        Me.picMixBarite.Name = "picMixBarite"
        Me.picMixBarite.Size = New System.Drawing.Size(224, 424)
        Me.picMixBarite.TabIndex = 73
        Me.picMixBarite.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picMixBarite, "GMP10RIG,TALARA PERU")
        '
        'picMudMW4
        '
        Me.picMudMW4.BackColor = System.Drawing.SystemColors.Window
        Me.picMudMW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picMudMW4.Cursor = System.Windows.Forms.Cursors.Default
        Me.picMudMW4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.picMudMW4.Image = CType(resources.GetObject("picMudMW4.Image"), System.Drawing.Image)
        Me.picMudMW4.Location = New System.Drawing.Point(480, 96)
        Me.picMudMW4.Name = "picMudMW4"
        Me.picMudMW4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picMudMW4.Size = New System.Drawing.Size(366, 428)
        Me.picMudMW4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMudMW4.TabIndex = 62
        Me.picMudMW4.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picMudMW4, "CPTS ,A Branch GWDC CNPC.  Drilling fliuds engineer at Peru.")
        '
        'picCutBackMW
        '
        Me.picCutBackMW.BackColor = System.Drawing.SystemColors.Window
        Me.picCutBackMW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picCutBackMW.Cursor = System.Windows.Forms.Cursors.Default
        Me.picCutBackMW.ForeColor = System.Drawing.SystemColors.WindowText
        Me.picCutBackMW.Image = CType(resources.GetObject("picCutBackMW.Image"), System.Drawing.Image)
        Me.picCutBackMW.Location = New System.Drawing.Point(512, 104)
        Me.picCutBackMW.Name = "picCutBackMW"
        Me.picCutBackMW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picCutBackMW.Size = New System.Drawing.Size(309, 424)
        Me.picCutBackMW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picCutBackMW.TabIndex = 64
        Me.picCutBackMW.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picCutBackMW, "SAPET ,A Branch CNODC CNPC. Talara Peru.")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(328, 456)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 72
        Me.PicLogo.TabStop = False
        '
        'SSTab1
        '
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage0)
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage1)
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage2)
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage3)
        Me.SSTab1.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.SSTab1.ItemSize = New System.Drawing.Size(42, 27)
        Me.SSTab1.Location = New System.Drawing.Point(8, 8)
        Me.SSTab1.Name = "SSTab1"
        Me.SSTab1.SelectedIndex = 3
        Me.SSTab1.Size = New System.Drawing.Size(878, 575)
        Me.SSTab1.TabIndex = 0
        '
        '_SSTab1_TabPage0
        '
        Me._SSTab1_TabPage0.Controls.Add(Me.picMixBarite)
        Me._SSTab1_TabPage0.Controls.Add(Me.Frame1)
        Me._SSTab1_TabPage0.Controls.Add(Me.Frame5)
        Me._SSTab1_TabPage0.Controls.Add(Me.Command1)
        Me._SSTab1_TabPage0.Controls.Add(Me.Command5)
        Me._SSTab1_TabPage0.Controls.Add(Me.cmdHelp)
        Me._SSTab1_TabPage0.Controls.Add(Me.Frame9)
        Me._SSTab1_TabPage0.Controls.Add(Me.PicLogo)
        Me._SSTab1_TabPage0.Location = New System.Drawing.Point(4, 31)
        Me._SSTab1_TabPage0.Name = "_SSTab1_TabPage0"
        Me._SSTab1_TabPage0.Size = New System.Drawing.Size(870, 540)
        Me._SSTab1_TabPage0.TabIndex = 0
        Me._SSTab1_TabPage0.Text = "对现有体积的钻井液加重所需加重剂的重量"
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Text4)
        Me.Frame1.Controls.Add(Me.Label1)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Red
        Me.Frame1.Location = New System.Drawing.Point(8, 95)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(452, 200)
        Me.Frame1.TabIndex = 24
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "数据录入"
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(296, 32)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(106, 20)
        Me.Text1.TabIndex = 1
        Me.Text1.Text = ""
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(296, 72)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(106, 20)
        Me.Text2.TabIndex = 2
        Me.Text2.Text = ""
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(296, 112)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(106, 20)
        Me.Text3.TabIndex = 3
        Me.Text3.Text = ""
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(16, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(253, 20)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "原有钻井液的体积(m^3)"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(16, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(253, 20)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "原有钻井液的密度(g/cm^3)"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(16, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(253, 20)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "钻井液欲加重的密度(g/cm^3)"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(16, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(253, 20)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "加重剂的密度(g/cm^3)"
        '
        'Frame5
        '
        Me.Frame5.BackColor = System.Drawing.SystemColors.Control
        Me.Frame5.Controls.Add(Me.Text19)
        Me.Frame5.Controls.Add(Me.Text15)
        Me.Frame5.Controls.Add(Me.Label29)
        Me.Frame5.Controls.Add(Me.Label16)
        Me.Frame5.Controls.Add(Me._Label19_1)
        Me.Frame5.ForeColor = System.Drawing.Color.Red
        Me.Frame5.Location = New System.Drawing.Point(8, 303)
        Me.Frame5.Name = "Frame5"
        Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame5.Size = New System.Drawing.Size(452, 140)
        Me.Frame5.TabIndex = 44
        Me.Frame5.TabStop = False
        Me.Frame5.Text = "计算结果"
        '
        'Text19
        '
        Me.Text19.AcceptsReturn = True
        Me.Text19.AutoSize = False
        Me.Text19.BackColor = System.Drawing.SystemColors.Window
        Me.Text19.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text19.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text19.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text19.Location = New System.Drawing.Point(259, 95)
        Me.Text19.MaxLength = 0
        Me.Text19.Name = "Text19"
        Me.Text19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text19.Size = New System.Drawing.Size(80, 20)
        Me.Text19.TabIndex = 69
        Me.Text19.Text = ""
        '
        'Text15
        '
        Me.Text15.AcceptsReturn = True
        Me.Text15.AutoSize = False
        Me.Text15.BackColor = System.Drawing.SystemColors.Window
        Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text15.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text15.Location = New System.Drawing.Point(259, 43)
        Me.Text15.MaxLength = 0
        Me.Text15.Name = "Text15"
        Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text15.Size = New System.Drawing.Size(80, 20)
        Me.Text15.TabIndex = 48
        Me.Text15.Text = ""
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.Control
        Me.Label29.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label29.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label29.Location = New System.Drawing.Point(16, 95)
        Me.Label29.Name = "Label29"
        Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label29.Size = New System.Drawing.Size(213, 20)
        Me.Label29.TabIndex = 68
        Me.Label29.Text = "所需加重剂换算袋数(100lb/sx)"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(341, 43)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(80, 20)
        Me.Label16.TabIndex = 50
        Me.Label16.Text = "×1000kg"
        '
        '_Label19_1
        '
        Me._Label19_1.BackColor = System.Drawing.SystemColors.Control
        Me._Label19_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label19_1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me._Label19_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.SetIndex(Me._Label19_1, CType(1, Short))
        Me._Label19_1.Location = New System.Drawing.Point(16, 43)
        Me._Label19_1.Name = "_Label19_1"
        Me._Label19_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label19_1.Size = New System.Drawing.Size(213, 20)
        Me._Label19_1.TabIndex = 49
        Me._Label19_1.Text = "加重剂所需用量"
        Me._Label19_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Command1
        '
        Me.Command1.BackColor = System.Drawing.SystemColors.Control
        Me.Command1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command1.Location = New System.Drawing.Point(32, 480)
        Me.Command1.Name = "Command1"
        Me.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command1.Size = New System.Drawing.Size(80, 20)
        Me.Command1.TabIndex = 5
        Me.Command1.Text = "Run"
        '
        'Command5
        '
        Me.Command5.BackColor = System.Drawing.SystemColors.Control
        Me.Command5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command5.Location = New System.Drawing.Point(120, 480)
        Me.Command5.Name = "Command5"
        Me.Command5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command5.Size = New System.Drawing.Size(80, 20)
        Me.Command5.TabIndex = 6
        Me.Command5.Text = "Exit"
        '
        'cmdHelp
        '
        Me.cmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelp.Location = New System.Drawing.Point(208, 480)
        Me.cmdHelp.Name = "cmdHelp"
        Me.cmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelp.Size = New System.Drawing.Size(80, 20)
        Me.cmdHelp.TabIndex = 7
        Me.cmdHelp.Text = "Help"
        '
        'Frame9
        '
        Me.Frame9.BackColor = System.Drawing.SystemColors.Control
        Me.Frame9.Controls.Add(Me.Label27)
        Me.Frame9.ForeColor = System.Drawing.Color.Blue
        Me.Frame9.Location = New System.Drawing.Point(8, 8)
        Me.Frame9.Name = "Frame9"
        Me.Frame9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame9.Size = New System.Drawing.Size(854, 79)
        Me.Frame9.TabIndex = 60
        Me.Frame9.TabStop = False
        Me.Frame9.Text = "程序说明"
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.SystemColors.Control
        Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label27.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label27.ForeColor = System.Drawing.Color.Blue
        Me.Label27.Location = New System.Drawing.Point(10, 17)
        Me.Label27.Name = "Label27"
        Me.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label27.Size = New System.Drawing.Size(832, 53)
        Me.Label27.TabIndex = 67
        Me.Label27.Text = "本程序用于对现有体积的钻井液加重所需加重剂重量的计算。通过输入不同的加重剂密度数据，可用于石灰石(2.7-2.9 g/cm^3)、重晶石(4.0-4.5g/cm^" & _
        "3)、钛铁矿(4.7g/cm^3)、方铅矿(7.0-7.5g/cm^3)、赤铁矿(5.1g/cm^3)和黄铁矿(5.0 g/cm^3)等加重剂的计算。"
        '
        '_SSTab1_TabPage1
        '
        Me._SSTab1_TabPage1.Controls.Add(Me.picMudMW4)
        Me._SSTab1_TabPage1.Controls.Add(Me.Frame2)
        Me._SSTab1_TabPage1.Controls.Add(Me.Frame6)
        Me._SSTab1_TabPage1.Controls.Add(Me.Command2)
        Me._SSTab1_TabPage1.Controls.Add(Me.Frame10)
        Me._SSTab1_TabPage1.Controls.Add(Me.Command6)
        Me._SSTab1_TabPage1.Location = New System.Drawing.Point(4, 31)
        Me._SSTab1_TabPage1.Name = "_SSTab1_TabPage1"
        Me._SSTab1_TabPage1.Size = New System.Drawing.Size(870, 540)
        Me._SSTab1_TabPage1.TabIndex = 1
        Me._SSTab1_TabPage1.Text = "配制预定体积的加重钻井液所需加重剂的重量"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text5)
        Me.Frame2.Controls.Add(Me.Text6)
        Me.Frame2.Controls.Add(Me.Text7)
        Me.Frame2.Controls.Add(Me.Text8)
        Me.Frame2.Controls.Add(Me.Label5)
        Me.Frame2.Controls.Add(Me.Label6)
        Me.Frame2.Controls.Add(Me.Label7)
        Me.Frame2.Controls.Add(Me.Label8)
        Me.Frame2.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(8, 95)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(452, 200)
        Me.Frame2.TabIndex = 31
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "数据录入"
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(296, 32)
        Me.Text5.MaxLength = 0
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(106, 20)
        Me.Text5.TabIndex = 8
        Me.Text5.Text = ""
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(296, 72)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(106, 20)
        Me.Text6.TabIndex = 9
        Me.Text6.Text = ""
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.SystemColors.Window
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(296, 112)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(106, 20)
        Me.Text7.TabIndex = 10
        Me.Text7.Text = ""
        Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(16, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(252, 20)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "加重后钻井液的体积(m^3)"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(16, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(252, 20)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "钻井液欲加重的密度(g/cm^3)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(16, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(252, 20)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "原有钻井液的密度(g/cm^3)"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(16, 152)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(252, 20)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "加重剂的密度(g/cm^3)"
        '
        'Frame6
        '
        Me.Frame6.BackColor = System.Drawing.SystemColors.Control
        Me.Frame6.Controls.Add(Me.Text20)
        Me.Frame6.Controls.Add(Me.Text16)
        Me.Frame6.Controls.Add(Me.Label28)
        Me.Frame6.Controls.Add(Me.Label17)
        Me.Frame6.Controls.Add(Me.Label20)
        Me.Frame6.ForeColor = System.Drawing.Color.Red
        Me.Frame6.Location = New System.Drawing.Point(8, 303)
        Me.Frame6.Name = "Frame6"
        Me.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame6.Size = New System.Drawing.Size(452, 140)
        Me.Frame6.TabIndex = 45
        Me.Frame6.TabStop = False
        Me.Frame6.Text = "计算结果"
        '
        'Text20
        '
        Me.Text20.AcceptsReturn = True
        Me.Text20.AutoSize = False
        Me.Text20.BackColor = System.Drawing.SystemColors.Window
        Me.Text20.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text20.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text20.Location = New System.Drawing.Point(259, 94)
        Me.Text20.MaxLength = 0
        Me.Text20.Name = "Text20"
        Me.Text20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text20.Size = New System.Drawing.Size(80, 20)
        Me.Text20.TabIndex = 70
        Me.Text20.Text = ""
        Me.Text20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text16
        '
        Me.Text16.AcceptsReturn = True
        Me.Text16.AutoSize = False
        Me.Text16.BackColor = System.Drawing.SystemColors.Window
        Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text16.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text16.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text16.Location = New System.Drawing.Point(259, 43)
        Me.Text16.MaxLength = 0
        Me.Text16.Name = "Text16"
        Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text16.Size = New System.Drawing.Size(80, 20)
        Me.Text16.TabIndex = 51
        Me.Text16.Text = ""
        Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.SystemColors.Control
        Me.Label28.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label28.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(16, 95)
        Me.Label28.Name = "Label28"
        Me.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label28.Size = New System.Drawing.Size(213, 20)
        Me.Label28.TabIndex = 71
        Me.Label28.Text = "所需加重剂换算袋数(100lb/sx)"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(341, 43)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(80, 20)
        Me.Label17.TabIndex = 53
        Me.Label17.Text = "×1000kg"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label20.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(16, 43)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label20.Size = New System.Drawing.Size(213, 20)
        Me.Label20.TabIndex = 52
        Me.Label20.Text = "加重剂所需用量"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Command2
        '
        Me.Command2.BackColor = System.Drawing.SystemColors.Control
        Me.Command2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command2.Location = New System.Drawing.Point(194, 472)
        Me.Command2.Name = "Command2"
        Me.Command2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command2.Size = New System.Drawing.Size(80, 20)
        Me.Command2.TabIndex = 12
        Me.Command2.Text = "Run"
        '
        'Frame10
        '
        Me.Frame10.BackColor = System.Drawing.SystemColors.Control
        Me.Frame10.Controls.Add(Me.Label26)
        Me.Frame10.ForeColor = System.Drawing.Color.Blue
        Me.Frame10.Location = New System.Drawing.Point(8, 8)
        Me.Frame10.Name = "Frame10"
        Me.Frame10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame10.Size = New System.Drawing.Size(854, 79)
        Me.Frame10.TabIndex = 61
        Me.Frame10.TabStop = False
        Me.Frame10.Text = "程序说明"
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.Control
        Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label26.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label26.ForeColor = System.Drawing.Color.Blue
        Me.Label26.Location = New System.Drawing.Point(10, 17)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label26.Size = New System.Drawing.Size(832, 53)
        Me.Label26.TabIndex = 66
        Me.Label26.Text = "本程序用于配制预定体积的加重钻井液所需加重剂的重量。通过输入不同的加重剂密度数据，可用于石灰石(2.7-2.9 g/cm^3)、重晶石(4.0-4.5g/cm^3" & _
        ")、钛铁矿(4.7g/cm^3)、方铅矿(7.0-7.5g/cm^3)、赤铁矿(5.1g/cm^3)和黄铁矿(5.0g/cm^3)等加重剂的计算。"
        '
        'Command6
        '
        Me.Command6.BackColor = System.Drawing.SystemColors.Control
        Me.Command6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command6.Location = New System.Drawing.Point(194, 496)
        Me.Command6.Name = "Command6"
        Me.Command6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command6.Size = New System.Drawing.Size(80, 20)
        Me.Command6.TabIndex = 13
        Me.Command6.Text = "Exit"
        '
        '_SSTab1_TabPage2
        '
        Me._SSTab1_TabPage2.Controls.Add(Me.picMudMW3)
        Me._SSTab1_TabPage2.Controls.Add(Me.Frame3)
        Me._SSTab1_TabPage2.Controls.Add(Me.Frame7)
        Me._SSTab1_TabPage2.Controls.Add(Me.Command3)
        Me._SSTab1_TabPage2.Controls.Add(Me.Command7)
        Me._SSTab1_TabPage2.Controls.Add(Me.Frame11)
        Me._SSTab1_TabPage2.Location = New System.Drawing.Point(4, 31)
        Me._SSTab1_TabPage2.Name = "_SSTab1_TabPage2"
        Me._SSTab1_TabPage2.Size = New System.Drawing.Size(870, 540)
        Me._SSTab1_TabPage2.TabIndex = 2
        Me._SSTab1_TabPage2.Text = "用重晶石加重100m^3钻井液时体积增量"
        '
        'picMudMW3
        '
        Me.picMudMW3.BackColor = System.Drawing.SystemColors.Window
        Me.picMudMW3.Cursor = System.Windows.Forms.Cursors.Default
        Me.picMudMW3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.picMudMW3.Image = CType(resources.GetObject("picMudMW3.Image"), System.Drawing.Image)
        Me.picMudMW3.Location = New System.Drawing.Point(480, 103)
        Me.picMudMW3.Name = "picMudMW3"
        Me.picMudMW3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picMudMW3.Size = New System.Drawing.Size(375, 424)
        Me.picMudMW3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picMudMW3.TabIndex = 63
        Me.picMudMW3.TabStop = False
        '
        'Frame3
        '
        Me.Frame3.BackColor = System.Drawing.SystemColors.Control
        Me.Frame3.Controls.Add(Me.Text9)
        Me.Frame3.Controls.Add(Me.Text10)
        Me.Frame3.Controls.Add(Me.Label9)
        Me.Frame3.Controls.Add(Me.Label10)
        Me.Frame3.ForeColor = System.Drawing.Color.Red
        Me.Frame3.Location = New System.Drawing.Point(8, 95)
        Me.Frame3.Name = "Frame3"
        Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame3.Size = New System.Drawing.Size(452, 120)
        Me.Frame3.TabIndex = 41
        Me.Frame3.TabStop = False
        Me.Frame3.Text = "数据录入"
        '
        'Text9
        '
        Me.Text9.AcceptsReturn = True
        Me.Text9.AutoSize = False
        Me.Text9.BackColor = System.Drawing.SystemColors.Window
        Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text9.Location = New System.Drawing.Point(296, 32)
        Me.Text9.MaxLength = 0
        Me.Text9.Name = "Text9"
        Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text9.Size = New System.Drawing.Size(80, 20)
        Me.Text9.TabIndex = 14
        Me.Text9.Text = ""
        Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text10
        '
        Me.Text10.AcceptsReturn = True
        Me.Text10.AutoSize = False
        Me.Text10.BackColor = System.Drawing.SystemColors.Window
        Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text10.Location = New System.Drawing.Point(296, 72)
        Me.Text10.MaxLength = 0
        Me.Text10.Name = "Text10"
        Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text10.Size = New System.Drawing.Size(80, 20)
        Me.Text10.TabIndex = 15
        Me.Text10.Text = ""
        Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(16, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(253, 20)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "加重前钻井液的密度(g/cm^3)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(16, 72)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(253, 20)
        Me.Label10.TabIndex = 42
        Me.Label10.Text = "加重后钻井液达到的密度(g/cm^3)"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Frame7
        '
        Me.Frame7.BackColor = System.Drawing.SystemColors.Control
        Me.Frame7.Controls.Add(Me.TextBox2)
        Me.Frame7.Controls.Add(Me.Label31)
        Me.Frame7.Controls.Add(Me.Text17)
        Me.Frame7.Controls.Add(Me.Label18)
        Me.Frame7.Controls.Add(Me.Label21)
        Me.Frame7.ForeColor = System.Drawing.Color.Red
        Me.Frame7.Location = New System.Drawing.Point(8, 223)
        Me.Frame7.Name = "Frame7"
        Me.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame7.Size = New System.Drawing.Size(452, 120)
        Me.Frame7.TabIndex = 46
        Me.Frame7.TabStop = False
        Me.Frame7.Text = "计算结果"
        '
        'TextBox2
        '
        Me.TextBox2.AcceptsReturn = True
        Me.TextBox2.AutoSize = False
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextBox2.Location = New System.Drawing.Point(259, 83)
        Me.TextBox2.MaxLength = 0
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox2.Size = New System.Drawing.Size(68, 24)
        Me.TextBox2.TabIndex = 57
        Me.TextBox2.Text = ""
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.SystemColors.Control
        Me.Label31.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label31.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label31.Location = New System.Drawing.Point(341, 83)
        Me.Label31.Name = "Label31"
        Me.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label31.Size = New System.Drawing.Size(80, 20)
        Me.Label31.TabIndex = 58
        Me.Label31.Text = "bbls"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Text17
        '
        Me.Text17.AcceptsReturn = True
        Me.Text17.AutoSize = False
        Me.Text17.BackColor = System.Drawing.SystemColors.Window
        Me.Text17.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text17.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text17.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text17.Location = New System.Drawing.Point(259, 43)
        Me.Text17.MaxLength = 0
        Me.Text17.Name = "Text17"
        Me.Text17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text17.Size = New System.Drawing.Size(68, 24)
        Me.Text17.TabIndex = 54
        Me.Text17.Text = ""
        Me.Text17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(341, 43)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(80, 20)
        Me.Label18.TabIndex = 56
        Me.Label18.Text = "m^3"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(16, 43)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label21.Size = New System.Drawing.Size(213, 20)
        Me.Label21.TabIndex = 55
        Me.Label21.Text = "钻井液体积增量"
        '
        'Command3
        '
        Me.Command3.BackColor = System.Drawing.SystemColors.Control
        Me.Command3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command3.Location = New System.Drawing.Point(194, 384)
        Me.Command3.Name = "Command3"
        Me.Command3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command3.Size = New System.Drawing.Size(80, 20)
        Me.Command3.TabIndex = 16
        Me.Command3.Text = "Run"
        '
        'Command7
        '
        Me.Command7.BackColor = System.Drawing.SystemColors.Control
        Me.Command7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command7.Location = New System.Drawing.Point(194, 416)
        Me.Command7.Name = "Command7"
        Me.Command7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command7.Size = New System.Drawing.Size(80, 20)
        Me.Command7.TabIndex = 17
        Me.Command7.Text = "Exit"
        '
        'Frame11
        '
        Me.Frame11.BackColor = System.Drawing.SystemColors.Control
        Me.Frame11.Controls.Add(Me.Label25)
        Me.Frame11.ForeColor = System.Drawing.Color.Blue
        Me.Frame11.Location = New System.Drawing.Point(8, 8)
        Me.Frame11.Name = "Frame11"
        Me.Frame11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame11.Size = New System.Drawing.Size(854, 79)
        Me.Frame11.TabIndex = 62
        Me.Frame11.TabStop = False
        Me.Frame11.Text = "程序说明"
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.Control
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label25.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label25.ForeColor = System.Drawing.Color.Blue
        Me.Label25.Location = New System.Drawing.Point(10, 17)
        Me.Label25.Name = "Label25"
        Me.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label25.Size = New System.Drawing.Size(832, 53)
        Me.Label25.TabIndex = 65
        Me.Label25.Text = "本程序用于计算用重晶石加重100立方米钻井液时，钻井液体积的增加量。"
        '
        '_SSTab1_TabPage3
        '
        Me._SSTab1_TabPage3.Controls.Add(Me.picCutBackMW)
        Me._SSTab1_TabPage3.Controls.Add(Me.Frame4)
        Me._SSTab1_TabPage3.Controls.Add(Me.Frame8)
        Me._SSTab1_TabPage3.Controls.Add(Me.Command4)
        Me._SSTab1_TabPage3.Controls.Add(Me.Command8)
        Me._SSTab1_TabPage3.Controls.Add(Me.Frame12)
        Me._SSTab1_TabPage3.Location = New System.Drawing.Point(4, 31)
        Me._SSTab1_TabPage3.Name = "_SSTab1_TabPage3"
        Me._SSTab1_TabPage3.Size = New System.Drawing.Size(870, 540)
        Me._SSTab1_TabPage3.TabIndex = 3
        Me._SSTab1_TabPage3.Text = "降低钻井液密度所需液量"
        '
        'Frame4
        '
        Me.Frame4.BackColor = System.Drawing.SystemColors.Control
        Me.Frame4.Controls.Add(Me.Text12)
        Me.Frame4.Controls.Add(Me.Text13)
        Me.Frame4.Controls.Add(Me.Text14)
        Me.Frame4.Controls.Add(Me.Text11)
        Me.Frame4.Controls.Add(Me.Label11)
        Me.Frame4.Controls.Add(Me.Label12)
        Me.Frame4.Controls.Add(Me.Label13)
        Me.Frame4.Controls.Add(Me.Label14)
        Me.Frame4.ForeColor = System.Drawing.Color.Red
        Me.Frame4.Location = New System.Drawing.Point(8, 95)
        Me.Frame4.Name = "Frame4"
        Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame4.Size = New System.Drawing.Size(452, 200)
        Me.Frame4.TabIndex = 36
        Me.Frame4.TabStop = False
        Me.Frame4.Text = "数据录入"
        '
        'Text12
        '
        Me.Text12.AcceptsReturn = True
        Me.Text12.AutoSize = False
        Me.Text12.BackColor = System.Drawing.SystemColors.Window
        Me.Text12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text12.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text12.Location = New System.Drawing.Point(309, 72)
        Me.Text12.MaxLength = 0
        Me.Text12.Name = "Text12"
        Me.Text12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text12.Size = New System.Drawing.Size(106, 20)
        Me.Text12.TabIndex = 19
        Me.Text12.Text = ""
        Me.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text13
        '
        Me.Text13.AcceptsReturn = True
        Me.Text13.AutoSize = False
        Me.Text13.BackColor = System.Drawing.SystemColors.Window
        Me.Text13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text13.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text13.Location = New System.Drawing.Point(309, 112)
        Me.Text13.MaxLength = 0
        Me.Text13.Name = "Text13"
        Me.Text13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text13.Size = New System.Drawing.Size(106, 20)
        Me.Text13.TabIndex = 20
        Me.Text13.Text = ""
        Me.Text13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text14
        '
        Me.Text14.AcceptsReturn = True
        Me.Text14.AutoSize = False
        Me.Text14.BackColor = System.Drawing.SystemColors.Window
        Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text14.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text14.Location = New System.Drawing.Point(309, 152)
        Me.Text14.MaxLength = 0
        Me.Text14.Name = "Text14"
        Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text14.Size = New System.Drawing.Size(106, 20)
        Me.Text14.TabIndex = 21
        Me.Text14.Text = ""
        Me.Text14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text11
        '
        Me.Text11.AcceptsReturn = True
        Me.Text11.AutoSize = False
        Me.Text11.BackColor = System.Drawing.SystemColors.Window
        Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text11.Location = New System.Drawing.Point(309, 32)
        Me.Text11.MaxLength = 0
        Me.Text11.Name = "Text11"
        Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text11.Size = New System.Drawing.Size(106, 20)
        Me.Text11.TabIndex = 18
        Me.Text11.Text = ""
        Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(16, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(273, 20)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "原有钻井液的密度(g/cm^3)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(16, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(273, 20)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "低密度液体的密度(g/cm^3)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(16, 112)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(273, 20)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = "混入低密度液体后钻井液的密度(g/cm^3)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(16, 152)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(273, 20)
        Me.Label14.TabIndex = 37
        Me.Label14.Text = "原有钻井液的体积(m^3)"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Frame8
        '
        Me.Frame8.BackColor = System.Drawing.SystemColors.Control
        Me.Frame8.Controls.Add(Me.TextBox1)
        Me.Frame8.Controls.Add(Me.Label30)
        Me.Frame8.Controls.Add(Me.Text18)
        Me.Frame8.Controls.Add(Me.Label23)
        Me.Frame8.Controls.Add(Me.Label22)
        Me.Frame8.ForeColor = System.Drawing.Color.Red
        Me.Frame8.Location = New System.Drawing.Point(8, 303)
        Me.Frame8.Name = "Frame8"
        Me.Frame8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame8.Size = New System.Drawing.Size(452, 120)
        Me.Frame8.TabIndex = 47
        Me.Frame8.TabStop = False
        Me.Frame8.Text = "计算结果"
        '
        'TextBox1
        '
        Me.TextBox1.AcceptsReturn = True
        Me.TextBox1.AutoSize = False
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TextBox1.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextBox1.Location = New System.Drawing.Point(254, 88)
        Me.TextBox1.MaxLength = 0
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox1.Size = New System.Drawing.Size(80, 20)
        Me.TextBox1.TabIndex = 60
        Me.TextBox1.Text = ""
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.Control
        Me.Label30.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label30.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(344, 88)
        Me.Label30.Name = "Label30"
        Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label30.Size = New System.Drawing.Size(49, 20)
        Me.Label30.TabIndex = 61
        Me.Label30.Text = "bbls"
        '
        'Text18
        '
        Me.Text18.AcceptsReturn = True
        Me.Text18.AutoSize = False
        Me.Text18.BackColor = System.Drawing.SystemColors.Window
        Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text18.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text18.Location = New System.Drawing.Point(254, 43)
        Me.Text18.MaxLength = 0
        Me.Text18.Name = "Text18"
        Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text18.Size = New System.Drawing.Size(80, 20)
        Me.Text18.TabIndex = 57
        Me.Text18.Text = ""
        Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label23.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label23.Location = New System.Drawing.Point(344, 43)
        Me.Label23.Name = "Label23"
        Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label23.Size = New System.Drawing.Size(49, 20)
        Me.Label23.TabIndex = 59
        Me.Label23.Text = "m^3"
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(38, 43)
        Me.Label22.Name = "Label22"
        Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label22.Size = New System.Drawing.Size(197, 18)
        Me.Label22.TabIndex = 58
        Me.Label22.Text = "降低密度所需低密度液体体积"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Command4
        '
        Me.Command4.BackColor = System.Drawing.SystemColors.Control
        Me.Command4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command4.Location = New System.Drawing.Point(194, 447)
        Me.Command4.Name = "Command4"
        Me.Command4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command4.Size = New System.Drawing.Size(80, 20)
        Me.Command4.TabIndex = 22
        Me.Command4.Text = "Run"
        '
        'Command8
        '
        Me.Command8.BackColor = System.Drawing.SystemColors.Control
        Me.Command8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Command8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command8.Location = New System.Drawing.Point(194, 475)
        Me.Command8.Name = "Command8"
        Me.Command8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command8.Size = New System.Drawing.Size(80, 20)
        Me.Command8.TabIndex = 23
        Me.Command8.Text = "Exit"
        '
        'Frame12
        '
        Me.Frame12.BackColor = System.Drawing.SystemColors.Control
        Me.Frame12.Controls.Add(Me.Label24)
        Me.Frame12.ForeColor = System.Drawing.Color.Blue
        Me.Frame12.Location = New System.Drawing.Point(8, 8)
        Me.Frame12.Name = "Frame12"
        Me.Frame12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame12.Size = New System.Drawing.Size(854, 79)
        Me.Frame12.TabIndex = 63
        Me.Frame12.TabStop = False
        Me.Frame12.Text = "程序说明"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.Control
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label24.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label24.ForeColor = System.Drawing.Color.Blue
        Me.Label24.Location = New System.Drawing.Point(10, 17)
        Me.Label24.Name = "Label24"
        Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label24.Size = New System.Drawing.Size(832, 53)
        Me.Label24.TabIndex = 64
        Me.Label24.Text = "本程序用于计算降低钻井液密度所需液量。"
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(259, 310)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(78, 18)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "×1000kg"
        '
        '_Label19_0
        '
        Me._Label19_0.BackColor = System.Drawing.SystemColors.Control
        Me._Label19_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label19_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.SetIndex(Me._Label19_0, CType(0, Short))
        Me._Label19_0.Location = New System.Drawing.Point(77, 310)
        Me._Label19_0.Name = "_Label19_0"
        Me._Label19_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label19_0.Size = New System.Drawing.Size(116, 18)
        Me._Label19_0.TabIndex = 29
        Me._Label19_0.Text = "表1计算需加重剂"
        '
        'frmMudweight
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.SSTab1)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me._Label19_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmMudweight"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液加重及降低密度"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SSTab1.ResumeLayout(False)
        Me._SSTab1_TabPage0.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.Frame5.ResumeLayout(False)
        Me.Frame9.ResumeLayout(False)
        Me._SSTab1_TabPage1.ResumeLayout(False)
        Me.Frame2.ResumeLayout(False)
        Me.Frame6.ResumeLayout(False)
        Me.Frame10.ResumeLayout(False)
        Me._SSTab1_TabPage2.ResumeLayout(False)
        Me.Frame3.ResumeLayout(False)
        Me.Frame7.ResumeLayout(False)
        Me.Frame11.ResumeLayout(False)
        Me._SSTab1_TabPage3.ResumeLayout(False)
        Me.Frame4.ResumeLayout(False)
        Me.Frame8.ResumeLayout(False)
        Me.Frame12.ResumeLayout(False)
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmMudweight
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmMudweight
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmMudweight()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim Vyuantiji1 As Single '原有钻井液的体积－表1
	Dim Myuanmidu1 As Single '原钻井液密度－表1
	Dim Myujiamidu1 As Single '加重后钻井液密度－表1
	Dim Mjimidu1 As Single '加重剂密度－表1
	Dim Vhoutiji2 As Single '加重后钻井液体积－表2
	Dim Myuanmidu2 As Single '原钻井液密度－表2
	Dim Myujiamidu2 As Single '加重后钻井液密度－表2
	Dim Mjimidu2 As Single '加重剂密度－表2
	Dim Jiazhongji1 As Single '加重剂用量1
	Dim Jiazhongji2 As Single '加重剂用量1
	Dim M前密度3 As Single
	Dim M后密度3 As Single
	Dim Vadd3 As Single
	Dim M原密度4 As Single
	Dim Mwater4 As Single
	Dim M稀释后密度4 As Single
	Dim V原体积4 As Single
	Dim Vwater4 As Single
	
	
	Dim buttons As MsgBoxStyle
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	Sub Quit() '编写的退出请示函数
		buttons = MsgBox("Are you real want to quit？", 65, "Warning")
		
		If buttons = 1 Then
			Me.Close()
		Else
			If buttons = 2 Then
				Exit Sub
			End If
		End If
	End Sub
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		frmHelpMudWeight.DefInstance.ShowDialog()
	End Sub
	
	Private Sub Command1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command1.Click '对现有体积的钻井液加重所需加重剂的重量公式
		Vyuantiji1 = Val(Text1.Text) '原有钻井液的体积’数据
		If Vyuantiji1 <= 0 Then
			MsgBox("Please check that you have entered the 'Original Volume' data!", MsgBoxStyle.Information, "Weight Up(Change Pit volume)")
			Text1.Text = ""
			Exit Sub
		End If
		
		Myuanmidu1 = Val(Text2.Text) '原有钻井液密度’数据
		If Myuanmidu1 <= 0 Then
			MsgBox("Please check that you have entered the 'Original Mud Weight'data!", MsgBoxStyle.Information, "Weight Up(Change Pit volume)")
			Text2.Text = ""
			Exit Sub
		End If
		
		Myujiamidu1 = Val(Text3.Text) '钻井液欲加重的密度’数据
		If Myujiamidu1 <= 0 Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight'data!", MsgBoxStyle.Information, "Weight Up(Change Pit volume)")
			Text3.Text = ""
			Exit Sub
		End If
		
		Mjimidu1 = Val(Text4.Text) '钻井液加重剂密度’数据
		If Mjimidu1 <= 0 Then
			MsgBox("Please check that you have entered the 'SG of Weighting Additive' data!", MsgBoxStyle.Information, "Weight Up(Change Pit volume)")
			Text4.Text = ""
			Exit Sub
		End If
		
		If Myujiamidu1 <= Myuanmidu1 Then '数据大小初步比较
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Weight Up(Change Pit volume)")
			Text3.Text = ""
			Exit Sub
		End If
		
		Jiazhongji1 = Vyuantiji1 * Mjimidu1 * (Myujiamidu1 - Myuanmidu1) / (Mjimidu1 - Myujiamidu1)
		Text15.Text = VB6.Format(Jiazhongji1, "0.00")
		Text19.Text = VB6.Format(Jiazhongji1 / 0.04536, "000")
	End Sub
	
	Private Sub Command2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command2.Click '配制预定体积的加重钻井液所需加重剂的重量
		Vhoutiji2 = Val(Text5.Text) '加重后钻井液体积－表2
		If Vhoutiji2 <= 0 Then
			MsgBox("Please check that you have entered the 'Desired Volume'  data!", MsgBoxStyle.Information, "Weight Up(Constant Pit Volume)")
			Text5.Text = ""
			Exit Sub
		End If
		
		Myujiamidu2 = Val(Text6.Text) '加重后钻井液密度－表2
		If Myujiamidu2 <= 0 Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Weight Up(Constant Pit Volume)")
			Text6.Text = ""
			Exit Sub
		End If
		
		Myuanmidu2 = Val(Text7.Text) '原钻井液密度－表2
		If Myuanmidu2 <= 0 Then
			MsgBox("Please check that you have entered the 'Original Mud Weight' data!", MsgBoxStyle.Information, "Weight Up(Constant Pit Volume)")
			Text7.Text = ""
			Exit Sub
		End If
		
		Mjimidu2 = Val(Text8.Text) '加重剂密度－表2
		If Mjimidu2 <= 1# Then
			MsgBox("Please check that you have entered the 'SG of Weighting Additive' data!", MsgBoxStyle.Information, "Weight Up(Constant Pit Volume)")
			Text8.Text = ""
			Exit Sub
		End If
		
		If Myujiamidu2 <= Myuanmidu2 Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Weight Up(Constant Pit Volume)")
			Text6.Text = ""
			Exit Sub
		End If
		
		Jiazhongji2 = Vhoutiji2 * Mjimidu2 * (Myujiamidu2 - Myuanmidu2) / (Mjimidu2 - Myujiamidu2)
		Text16.Text = VB6.Format(Jiazhongji2, "0.00")
		Text20.Text = VB6.Format(Jiazhongji2 / 0.04536, "000")
	End Sub
	
	Private Sub Command3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command3.Click '用重晶石加重100方钻井液时体积增量
		
		M前密度3 = Val(Text9.Text) '重晶石加重前钻井液密度输入－－3
		If M前密度3 <= 0 Then
			MsgBox("Please check that you have entered the 'Original Mud Weight' data!", MsgBoxStyle.Information, "Weight up 100m^3 mud volume to Add")
			Text9.Text = ""
			Exit Sub
		End If
		
		M后密度3 = Val(Text10.Text) '重晶石加重后钻井液密度输入－－3
		If M后密度3 <= 0 Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Weight up 100m^3 mud volume to Add")
			Text10.Text = ""
			Exit Sub
		End If
		
		
		If M后密度3 <= M前密度3 Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Weight up 100m^3 mud volume to Add")
			Text9.Text = ""
			Text10.Text = ""
			Exit Sub
		End If
		
		
		Vadd3 = (100 * (M后密度3 - M前密度3)) / (4.2 - M后密度3)
        Text17.Text = VB6.Format(Vadd3, "0.00")
        TextBox2.Text = Format(Vadd3 * 6.28, "0")         '美制体积桶换算显示   2022/11/20添加    Chade
	End Sub
	
	Private Sub Command4_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command4.Click '降低钻井液密度所需加水量
		
        M原密度4 = Val(Text11.Text) '表4加水或低密度液体稀释前钻井液密度输入
		If M原密度4 <= 0 Then
			MsgBox("Please check that you have entered the  'Original Mud Weight' data!", MsgBoxStyle.Information, "Cut Back(Change Pit Volume)")
			Text11.Text = ""
			Exit Sub
		End If
		
        Mwater4 = Val(Text12.Text) '表4所用水密度或低密度液体输入
		If Mwater4 < 1# Then
			MsgBox("Please check that you have entered the'Cut-Back Fluid Weight'data!", MsgBoxStyle.Information, "Cut Back(Change Pit Volume)")
			Text12.Text = ""
			Exit Sub
		End If
		
        M稀释后密度4 = Val(Text13.Text) '表4水密度或低密度液体后钻井液密度输入
		If M稀释后密度4 <= 1# Then
			MsgBox("Please check that you have entered the 'Desired Mud Weight' data!", MsgBoxStyle.Information, "Cut Back(Change Pit Volume)")
			Text13.Text = ""
			Exit Sub
		End If
		
        V原体积4 = Val(Text14.Text) '表4水密度或低密度液体稀释前钻井液体积
		If V原体积4 <= 0 Then
			MsgBox("Please check that you have entered the 'Original Volume' data!", MsgBoxStyle.Information, "Cut Back(Change Pit Volume)")
			Text13.Text = ""
			Exit Sub
		End If
		
		If M原密度4 <= M稀释后密度4 Then
			MsgBox("Please check that you have entered the ‘Mud weight’ data!", MsgBoxStyle.Information, "Cut Back(Change Pit Volume)")
			Text11.Text = ""
			Text13.Text = ""
			Exit Sub
		End If
		
		Vwater4 = (V原体积4 * (M原密度4 - M稀释后密度4)) / (M稀释后密度4 - Mwater4)
        Text18.Text = VB6.Format(Vwater4, "0.00")              '公制立方米
        TextBox1.Text = Format(Vwater4 * 6.28, "0")         '美制体积桶换算显示   2022/11/20添加    Chade
	End Sub
	
	Private Sub Command5_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command5.Click
		Quit()
	End Sub
	
	Private Sub Command6_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command6.Click
		Quit()
	End Sub
	
	Private Sub Command7_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command7.Click
		Quit()
	End Sub
	Private Sub Command8_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command8.Click
		Quit()
	End Sub
	
	
	
	Private Sub frmMudweight_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************


        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdAnnularMwquit.Visible = False    '双击logo 图案开始截图，设置命令按钮隐藏'
		'cmdAnnularMwok.Visible = False
		'cmdAnnularMwclear.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MDensity.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MDensity.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        'cmdPicSave.Visible = True '作废 的控件

        'cmdAnnularMwquit.Visible = True
        'cmdAnnularMwok.Visible = True
        'cmdAnnularMwclear.Visible = True
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vyuantiji1 = Val(Text1.Text) '原有钻井液的体积－表1
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text10_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text10.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M后密度3 = Val(Text10.Text) '重晶石加重后钻井液密度输入－－3
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text11_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text11.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text11_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text11.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M原密度4 = Val(Text11.Text) '表4加水稀释前钻井液密度输入
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text12_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text12.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text12_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text12.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mwater4 = Val(Text12.Text) '表4所用水密度输入
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text13_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text13.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text13_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text13.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M稀释后密度4 = Val(Text13.Text) '表4加水稀释后钻井液密度输入
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text14_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text14.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text14_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text14.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		V原体积4 = Val(Text14.Text) '表4加水稀释前钻井液体积
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myuanmidu1 = Val(Text2.Text) '原钻井液密度－表1
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text3_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text3.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myujiamidu1 = Val(Text3.Text) '加重后钻井液密度－表1
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text4_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text4.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mjimidu1 = Val(Text4.Text) '加重剂密度－表1
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text5_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text5.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vhoutiji2 = Val(Text5.Text) '加重后钻井液体积－表2
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text6_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text6.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myujiamidu2 = Val(Text6.Text) '加重后钻井液密度－表2
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text7_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text7.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myuanmidu2 = Val(Text7.Text) '原钻井液密度－表2
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text8_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text8.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mjimidu2 = Val(Text8.Text) '加重剂密度－表2
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text9_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text9.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M前密度3 = Val(Text9.Text) '重晶石加重前钻井液密度输入－－3
	End Sub

    
    Private Sub Label21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label21.Click

    End Sub

    Private Sub SSTab1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SSTab1.SelectedIndexChanged

    End Sub
End Class