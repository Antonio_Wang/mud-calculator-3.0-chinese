Option Strict Off
Option Explicit On
Friend Class frmKillWellChange
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents Picture1 As System.Windows.Forms.PictureBox
	Public WithEvents txtKillingWellstyle As System.Windows.Forms.TextBox
	Public WithEvents lblKillingWellStyleDensity As System.Windows.Forms.Label
	Public WithEvents fraKillingWellOutput As System.Windows.Forms.GroupBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents lblKillWellProgram As System.Windows.Forms.Label
	Public WithEvents fraKillWellProgram As System.Windows.Forms.GroupBox
	Public WithEvents txtKillWellShutinpressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellMudDensity As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellOpenHoleExpands As System.Windows.Forms.TextBox
	Public WithEvents txtKillWelldrillstemdiameter As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellBitdiameter As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellKickVol As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCasingpressure As System.Windows.Forms.TextBox
	Public WithEvents lblKillWellShutinpressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellMudDensity As System.Windows.Forms.Label
	Public WithEvents lblKillWellOpenHoleExpands As System.Windows.Forms.Label
	Public WithEvents lblKillWelldrillstemdiameter As System.Windows.Forms.Label
	Public WithEvents lblKillWellBitdiameter As System.Windows.Forms.Label
	Public WithEvents lblKillWellKickVol As System.Windows.Forms.Label
	Public WithEvents lblKillWellCasingpressure As System.Windows.Forms.Label
	Public WithEvents fraKillWellInputdata As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmKillWellChange))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtKillingWellstyle = New System.Windows.Forms.TextBox
        Me.Picture1 = New System.Windows.Forms.PictureBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.fraKillingWellOutput = New System.Windows.Forms.GroupBox
        Me.lblKillingWellStyleDensity = New System.Windows.Forms.Label
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.fraKillWellProgram = New System.Windows.Forms.GroupBox
        Me.lblKillWellProgram = New System.Windows.Forms.Label
        Me.fraKillWellInputdata = New System.Windows.Forms.GroupBox
        Me.txtKillWellShutinpressure = New System.Windows.Forms.TextBox
        Me.txtKillWellMudDensity = New System.Windows.Forms.TextBox
        Me.txtKillWellOpenHoleExpands = New System.Windows.Forms.TextBox
        Me.txtKillWelldrillstemdiameter = New System.Windows.Forms.TextBox
        Me.txtKillWellBitdiameter = New System.Windows.Forms.TextBox
        Me.txtKillWellKickVol = New System.Windows.Forms.TextBox
        Me.txtKillWellCasingpressure = New System.Windows.Forms.TextBox
        Me.lblKillWellShutinpressure = New System.Windows.Forms.Label
        Me.lblKillWellMudDensity = New System.Windows.Forms.Label
        Me.lblKillWellOpenHoleExpands = New System.Windows.Forms.Label
        Me.lblKillWelldrillstemdiameter = New System.Windows.Forms.Label
        Me.lblKillWellBitdiameter = New System.Windows.Forms.Label
        Me.lblKillWellKickVol = New System.Windows.Forms.Label
        Me.lblKillWellCasingpressure = New System.Windows.Forms.Label
        Me.fraKillingWellOutput.SuspendLayout()
        Me.fraKillWellProgram.SuspendLayout()
        Me.fraKillWellInputdata.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtKillingWellstyle
        '
        Me.txtKillingWellstyle.AcceptsReturn = True
        Me.txtKillingWellstyle.AutoSize = False
        Me.txtKillingWellstyle.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillingWellstyle.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillingWellstyle.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillingWellstyle.ForeColor = System.Drawing.Color.Red
        Me.txtKillingWellstyle.Location = New System.Drawing.Point(274, 26)
        Me.txtKillingWellstyle.MaxLength = 0
        Me.txtKillingWellstyle.Name = "txtKillingWellstyle"
        Me.txtKillingWellstyle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillingWellstyle.Size = New System.Drawing.Size(78, 20)
        Me.txtKillingWellstyle.TabIndex = 21
        Me.txtKillingWellstyle.Text = ""
        Me.txtKillingWellstyle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtKillingWellstyle, "溢流流体的种类判别：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.12～0.36g/cm3  为天然气溢流；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.37～0.60g/cm3  为油；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.61～0.84g/cm3  为油水混合；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.85～1.08g/cm3  为水。")
        '
        'Picture1
        '
        Me.Picture1.BackColor = System.Drawing.SystemColors.Window
        Me.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Picture1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Picture1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Picture1.Image = CType(resources.GetObject("Picture1.Image"), System.Drawing.Image)
        Me.Picture1.Location = New System.Drawing.Point(526, 152)
        Me.Picture1.Name = "Picture1"
        Me.Picture1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Picture1.Size = New System.Drawing.Size(342, 403)
        Me.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.Picture1.TabIndex = 23
        Me.Picture1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.Picture1, "I can't tell you!^-^")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(385, 457)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 24
        Me.PicLogo.TabStop = False
        '
        'fraKillingWellOutput
        '
        Me.fraKillingWellOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillingWellstyle)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillingWellStyleDensity)
        Me.fraKillingWellOutput.ForeColor = System.Drawing.Color.Red
        Me.fraKillingWellOutput.Location = New System.Drawing.Point(8, 452)
        Me.fraKillingWellOutput.Name = "fraKillingWellOutput"
        Me.fraKillingWellOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillingWellOutput.Size = New System.Drawing.Size(371, 66)
        Me.fraKillingWellOutput.TabIndex = 20
        Me.fraKillingWellOutput.TabStop = False
        Me.fraKillingWellOutput.Text = "计算结果"
        '
        'lblKillingWellStyleDensity
        '
        Me.lblKillingWellStyleDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillingWellStyleDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillingWellStyleDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillingWellStyleDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillingWellStyleDensity.Location = New System.Drawing.Point(10, 26)
        Me.lblKillingWellStyleDensity.Name = "lblKillingWellStyleDensity"
        Me.lblKillingWellStyleDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillingWellStyleDensity.Size = New System.Drawing.Size(227, 20)
        Me.lblKillingWellStyleDensity.TabIndex = 22
        Me.lblKillingWellStyleDensity.Text = "溢流流体密度 (g/cm^3)"
        Me.lblKillingWellStyleDensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(395, 414)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 19
        Me.cmdExit.Text = "Exit"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(395, 390)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 18
        Me.cmdclear.Text = "Clean"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(395, 358)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 17
        Me.cmdok.Text = "Run"
        '
        'fraKillWellProgram
        '
        Me.fraKillWellProgram.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillWellProgram.Controls.Add(Me.lblKillWellProgram)
        Me.fraKillWellProgram.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraKillWellProgram.Location = New System.Drawing.Point(8, 8)
        Me.fraKillWellProgram.Name = "fraKillWellProgram"
        Me.fraKillWellProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillWellProgram.Size = New System.Drawing.Size(877, 123)
        Me.fraKillWellProgram.TabIndex = 15
        Me.fraKillWellProgram.TabStop = False
        Me.fraKillWellProgram.Text = "程序说明"
        '
        'lblKillWellProgram
        '
        Me.lblKillWellProgram.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellProgram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblKillWellProgram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellProgram.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellProgram.ForeColor = System.Drawing.Color.Blue
        Me.lblKillWellProgram.Location = New System.Drawing.Point(10, 17)
        Me.lblKillWellProgram.Name = "lblKillWellProgram"
        Me.lblKillWellProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellProgram.Size = New System.Drawing.Size(857, 96)
        Me.lblKillWellProgram.TabIndex = 16
        Me.lblKillWellProgram.Text = "当现场发生溢流时，及时和准确的判断是什么流体类型导致的溢流，对安全生产显得十分重要。本程序就是用来解决该问题。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "溢流流体的种类判别：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.12～0.36g/c" & _
        "m3  为天然气溢流(Gas)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.37～0.60g/cm3  为油(Oil)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.61～0.84g/cm3  为油水混合(Oil and water)" & _
        "；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.85～1.08g/cm3  为水(Water)。"
        '
        'fraKillWellInputdata
        '
        Me.fraKillWellInputdata.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellShutinpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellMudDensity)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellOpenHoleExpands)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWelldrillstemdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellBitdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellKickVol)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCasingpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellShutinpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellMudDensity)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellOpenHoleExpands)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWelldrillstemdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellBitdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellKickVol)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCasingpressure)
        Me.fraKillWellInputdata.ForeColor = System.Drawing.Color.Blue
        Me.fraKillWellInputdata.Location = New System.Drawing.Point(8, 139)
        Me.fraKillWellInputdata.Name = "fraKillWellInputdata"
        Me.fraKillWellInputdata.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillWellInputdata.Size = New System.Drawing.Size(371, 305)
        Me.fraKillWellInputdata.TabIndex = 0
        Me.fraKillWellInputdata.TabStop = False
        Me.fraKillWellInputdata.Text = "数据输入"
        '
        'txtKillWellShutinpressure
        '
        Me.txtKillWellShutinpressure.AcceptsReturn = True
        Me.txtKillWellShutinpressure.AutoSize = False
        Me.txtKillWellShutinpressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellShutinpressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellShutinpressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellShutinpressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellShutinpressure.Location = New System.Drawing.Point(274, 26)
        Me.txtKillWellShutinpressure.MaxLength = 0
        Me.txtKillWellShutinpressure.Name = "txtKillWellShutinpressure"
        Me.txtKillWellShutinpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellShutinpressure.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellShutinpressure.TabIndex = 7
        Me.txtKillWellShutinpressure.Text = "3.5"
        Me.txtKillWellShutinpressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellMudDensity
        '
        Me.txtKillWellMudDensity.AcceptsReturn = True
        Me.txtKillWellMudDensity.AutoSize = False
        Me.txtKillWellMudDensity.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellMudDensity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellMudDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellMudDensity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellMudDensity.Location = New System.Drawing.Point(274, 266)
        Me.txtKillWellMudDensity.MaxLength = 0
        Me.txtKillWellMudDensity.Name = "txtKillWellMudDensity"
        Me.txtKillWellMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellMudDensity.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellMudDensity.TabIndex = 6
        Me.txtKillWellMudDensity.Text = "1.66"
        Me.txtKillWellMudDensity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellOpenHoleExpands
        '
        Me.txtKillWellOpenHoleExpands.AcceptsReturn = True
        Me.txtKillWellOpenHoleExpands.AutoSize = False
        Me.txtKillWellOpenHoleExpands.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellOpenHoleExpands.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellOpenHoleExpands.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellOpenHoleExpands.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellOpenHoleExpands.Location = New System.Drawing.Point(274, 226)
        Me.txtKillWellOpenHoleExpands.MaxLength = 0
        Me.txtKillWellOpenHoleExpands.Name = "txtKillWellOpenHoleExpands"
        Me.txtKillWellOpenHoleExpands.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellOpenHoleExpands.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellOpenHoleExpands.TabIndex = 5
        Me.txtKillWellOpenHoleExpands.Text = "1.05"
        Me.txtKillWellOpenHoleExpands.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtKillWellOpenHoleExpands, "举例：裸眼井径扩大率5%，录入:1.05")
        '
        'txtKillWelldrillstemdiameter
        '
        Me.txtKillWelldrillstemdiameter.AcceptsReturn = True
        Me.txtKillWelldrillstemdiameter.AutoSize = False
        Me.txtKillWelldrillstemdiameter.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWelldrillstemdiameter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWelldrillstemdiameter.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWelldrillstemdiameter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWelldrillstemdiameter.Location = New System.Drawing.Point(274, 186)
        Me.txtKillWelldrillstemdiameter.MaxLength = 0
        Me.txtKillWelldrillstemdiameter.Name = "txtKillWelldrillstemdiameter"
        Me.txtKillWelldrillstemdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWelldrillstemdiameter.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWelldrillstemdiameter.TabIndex = 4
        Me.txtKillWelldrillstemdiameter.Text = "127"
        Me.txtKillWelldrillstemdiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellBitdiameter
        '
        Me.txtKillWellBitdiameter.AcceptsReturn = True
        Me.txtKillWellBitdiameter.AutoSize = False
        Me.txtKillWellBitdiameter.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellBitdiameter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellBitdiameter.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellBitdiameter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellBitdiameter.Location = New System.Drawing.Point(274, 146)
        Me.txtKillWellBitdiameter.MaxLength = 0
        Me.txtKillWellBitdiameter.Name = "txtKillWellBitdiameter"
        Me.txtKillWellBitdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellBitdiameter.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellBitdiameter.TabIndex = 3
        Me.txtKillWellBitdiameter.Text = "215.9"
        Me.txtKillWellBitdiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellKickVol
        '
        Me.txtKillWellKickVol.AcceptsReturn = True
        Me.txtKillWellKickVol.AutoSize = False
        Me.txtKillWellKickVol.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellKickVol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellKickVol.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellKickVol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellKickVol.Location = New System.Drawing.Point(274, 106)
        Me.txtKillWellKickVol.MaxLength = 0
        Me.txtKillWellKickVol.Name = "txtKillWellKickVol"
        Me.txtKillWellKickVol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellKickVol.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellKickVol.TabIndex = 2
        Me.txtKillWellKickVol.Text = "11"
        Me.txtKillWellKickVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCasingpressure
        '
        Me.txtKillWellCasingpressure.AcceptsReturn = True
        Me.txtKillWellCasingpressure.AutoSize = False
        Me.txtKillWellCasingpressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCasingpressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCasingpressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCasingpressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCasingpressure.Location = New System.Drawing.Point(274, 66)
        Me.txtKillWellCasingpressure.MaxLength = 0
        Me.txtKillWellCasingpressure.Name = "txtKillWellCasingpressure"
        Me.txtKillWellCasingpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCasingpressure.Size = New System.Drawing.Size(78, 20)
        Me.txtKillWellCasingpressure.TabIndex = 1
        Me.txtKillWellCasingpressure.Text = "8.0"
        Me.txtKillWellCasingpressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblKillWellShutinpressure
        '
        Me.lblKillWellShutinpressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellShutinpressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellShutinpressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellShutinpressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellShutinpressure.Location = New System.Drawing.Point(16, 26)
        Me.lblKillWellShutinpressure.Name = "lblKillWellShutinpressure"
        Me.lblKillWellShutinpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellShutinpressure.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellShutinpressure.TabIndex = 14
        Me.lblKillWellShutinpressure.Text = "关井立管压力SIDPP (MPa)"
        Me.lblKillWellShutinpressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellMudDensity
        '
        Me.lblKillWellMudDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellMudDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellMudDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellMudDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellMudDensity.Location = New System.Drawing.Point(16, 266)
        Me.lblKillWellMudDensity.Name = "lblKillWellMudDensity"
        Me.lblKillWellMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellMudDensity.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellMudDensity.TabIndex = 13
        Me.lblKillWellMudDensity.Text = "原钻井液密度 (g/cm^3)"
        Me.lblKillWellMudDensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellOpenHoleExpands
        '
        Me.lblKillWellOpenHoleExpands.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellOpenHoleExpands.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellOpenHoleExpands.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellOpenHoleExpands.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellOpenHoleExpands.Location = New System.Drawing.Point(16, 226)
        Me.lblKillWellOpenHoleExpands.Name = "lblKillWellOpenHoleExpands"
        Me.lblKillWellOpenHoleExpands.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellOpenHoleExpands.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellOpenHoleExpands.TabIndex = 12
        Me.lblKillWellOpenHoleExpands.Text = "裸眼井径扩大率 "
        Me.lblKillWellOpenHoleExpands.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWelldrillstemdiameter
        '
        Me.lblKillWelldrillstemdiameter.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWelldrillstemdiameter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWelldrillstemdiameter.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWelldrillstemdiameter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWelldrillstemdiameter.Location = New System.Drawing.Point(16, 186)
        Me.lblKillWelldrillstemdiameter.Name = "lblKillWelldrillstemdiameter"
        Me.lblKillWelldrillstemdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWelldrillstemdiameter.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWelldrillstemdiameter.TabIndex = 11
        Me.lblKillWelldrillstemdiameter.Text = "钻柱外径 (mm)"
        Me.lblKillWelldrillstemdiameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellBitdiameter
        '
        Me.lblKillWellBitdiameter.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellBitdiameter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellBitdiameter.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellBitdiameter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellBitdiameter.Location = New System.Drawing.Point(16, 146)
        Me.lblKillWellBitdiameter.Name = "lblKillWellBitdiameter"
        Me.lblKillWellBitdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellBitdiameter.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellBitdiameter.TabIndex = 10
        Me.lblKillWellBitdiameter.Text = "钻头直径 (mm)"
        Me.lblKillWellBitdiameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellKickVol
        '
        Me.lblKillWellKickVol.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellKickVol.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellKickVol.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellKickVol.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellKickVol.Location = New System.Drawing.Point(16, 106)
        Me.lblKillWellKickVol.Name = "lblKillWellKickVol"
        Me.lblKillWellKickVol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellKickVol.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellKickVol.TabIndex = 9
        Me.lblKillWellKickVol.Text = "井底溢流体积 (m^3)"
        Me.lblKillWellKickVol.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellCasingpressure
        '
        Me.lblKillWellCasingpressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCasingpressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCasingpressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCasingpressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCasingpressure.Location = New System.Drawing.Point(16, 66)
        Me.lblKillWellCasingpressure.Name = "lblKillWellCasingpressure"
        Me.lblKillWellCasingpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCasingpressure.Size = New System.Drawing.Size(227, 20)
        Me.lblKillWellCasingpressure.TabIndex = 8
        Me.lblKillWellCasingpressure.Text = "关井套管压力SICP (MPa)"
        Me.lblKillWellCasingpressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmKillWellChange
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 588)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.Picture1)
        Me.Controls.Add(Me.fraKillingWellOutput)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.fraKillWellProgram)
        Me.Controls.Add(Me.fraKillWellInputdata)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmKillWellChange"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "溢流类型判断"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraKillingWellOutput.ResumeLayout(False)
        Me.fraKillWellProgram.ResumeLayout(False)
        Me.fraKillWellInputdata.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmKillWellChange
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmKillWellChange
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmKillWellChange()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'溢流类型判断计算，2006年10月29日1：00青海省涩北台东2井。
	
	
	Const pi As Double = 3.1415927 '数学计算所需圆周率3.14159265358973846
	Const g As Double = 0.0098 '计算所需的物理常量:重力加速度g=0.0098
	
	
	
	Dim drillpipeP As Single '关井立管压力
	Dim casingP As Single '关井套管压力
	Dim KickVol As Single '溢流体积
	Dim BitDiameter As Single '钻头外径
	Dim DrillstemDiameter As Single '钻具外径
	
	Dim OpenHoleExpands As Single '井径扩大率
	Dim MudDensity As Single '井涌时钻井液密度
	
	'-----------------------------------------------------------------
	'模块计算结果输入变量定义
	Dim KickStyle As Single '溢流类型
	
	'-----------------------------------------------------------------
	'计算中定义的中间变量
	Dim VolOfAnnulus As Single '环空钻井液体积单位长度
	
	Dim OpenHoleDiameter As Single '扩大井径
	'--------------------------------------------------------------------
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		BitDiameter = Val(txtKillWellBitdiameter.Text)
		casingP = Val(txtKillWellCasingpressure.Text)
		DrillstemDiameter = Val(txtKillWelldrillstemdiameter.Text)
		
		KickVol = Val(txtKillWellKickVol.Text)
		MudDensity = Val(txtKillWellMudDensity.Text)
		OpenHoleExpands = Val(txtKillWellOpenHoleExpands.Text)
		
		
		drillpipeP = Val(txtKillWellShutinpressure.Text)
		If txtKillWellShutinpressure.Text = "" Or drillpipeP < 0 Then
            'MsgBox "Please check that you have entered 'SIDPP'data!", vbInformation, "溢流类型判断"
            MsgBox("请检查你输入的'关井立管压力'数据是否有效！", MsgBoxStyle.Information, "溢流类型判断")
			Exit Sub '纠错功能解决。2006年10月24日王朝
		End If
		
		casingP = Val(txtKillWellCasingpressure.Text)
		If txtKillWellCasingpressure.Text = "" Or casingP < 0 Then
            'MsgBox "Please check that you have entered 'SICP'data!", vbInformation, "溢流类型判断"
            MsgBox("请检查你输入的'关井套管压力'数据是否有效！", MsgBoxStyle.Information, "溢流类型判断")
			Exit Sub '纠错功能解决。2006年10月24日王朝
		End If
		
		'11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
		OpenHoleDiameter = OpenHoleExpands * BitDiameter
		VolOfAnnulus = pi / 40 * ((OpenHoleDiameter / 10) ^ 2 - (DrillstemDiameter / 10) ^ 2)
		KickStyle = MudDensity - (casingP - drillpipeP) / (g * KickVol * 1000 / VolOfAnnulus)
		
		txtKillingWellstyle.Text = VB6.Format(KickStyle, "0.0000")
		
		
		'溢流流体的种类判别和确定钻井液附加值数据
		'0.12～0.36g/cm3  为天然气溢流；
		'0.37～0.60g/cm3  为油；
		'0.61～0.84g/cm3  为油水混合；
		'0.85～1.08g/cm3  为水。
		
		'************************************************
		'消息框英语化时间：2012年4月15日秘鲁Talara  GMP－Negritos－i-33-12233井
		'**************************************************
		If KickStyle >= 0.12 And KickStyle <= 0.36 Then
            'MsgBox("Overflow type of fluid for gas ! Killing additional drilling fluid density :0.07--0.15g/cm^3。", MsgBoxStyle.Information, "Overflow type of fluid")
            MsgBox("溢流流体为天然气! 压井钻井液密度附加量:0.07--0.15g/cm^3。", MsgBoxStyle.Information, "溢流类型判断")
			
		Else
			If KickStyle >= 0.37 And KickStyle <= 0.6 Then
                'MsgBox("Overflow type of fluid for petroleum ! Killing additional drilling fluid density :0.05--0.10g/cm^3。", MsgBoxStyle.Information, "Overflow type of fluid")
                MsgBox("溢流流体为油! 压井钻井液密度附加量:0.05--0.10g/cm^3。", MsgBoxStyle.Information, "溢流类型判断")
				
			Else
				If KickStyle >= 0.61 And KickStyle <= 0.84 Then
                    'MsgBox("Overflow type for oil-water mixed !Killing additional drilling fluid density :0.05--0.10g/cm^3.", MsgBoxStyle.Information, "Overflow type of fluid")
                    MsgBox("溢流流体为油水混合! 压井钻井液密度附加量:0.05--0.10g/cm^3.", MsgBoxStyle.Information, "溢流类型判断")

					
				Else
					If KickStyle >= 0.85 And KickStyle <= 1.08 Then
                        'MsgBox("Overflow type for water!Killing additional drilling fluid density :0.05--0.10g/cm^3", MsgBoxStyle.Information, "Overflow type of fluid")
                        MsgBox("溢流流体为水! 压井钻井液密度附加量:0.05--0.10g/cm^3", MsgBoxStyle.Information, "溢流类型判断")
					Else
						If KickStyle <= 0 Then
                            MsgBox("可能由于程序未考虑钻铤数据的原因，也可能你采集的溢流量、关井立管压力和关井套压数据的错误，溢流流体的类型判断出现错误！但并不影响压井计算，程序假设附加钻井液密度为:0.05g/cm^3。", MsgBoxStyle.Information, "溢流类型判断")
							
						End If
					End If
				End If
			End If
		End If
		
		
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		
		txtKillWellShutinpressure.Text = ""
		txtKillWellCasingpressure.Text = ""
		txtKillWellKickVol.Text = ""
		txtKillWellBitdiameter.Text = ""
		txtKillWelldrillstemdiameter.Text = ""
		
		txtKillWellOpenHoleExpands.Text = ""
		txtKillWellMudDensity.Text = ""
		
		
		txtKillingWellstyle.Text = ""
		
		
	End Sub
	
	Private Sub frmKillWellChange_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************


        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
		Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdclear.Visible = False '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False

		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MkickUp.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MkickUp.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        cmdclear.Visible = True '

        cmdExit.Visible = True
        cmdok.Visible = True

	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellBitdiameter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellBitdiameter.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellBitdiameter_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellBitdiameter.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'bit's diameter
		BitDiameter = Val(txtKillWellBitdiameter.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCasingpressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCasingpressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCasingpressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCasingpressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'After kick control,casing pressure
		casingP = Val(txtKillWellCasingpressure.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWelldrillstemdiameter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWelldrillstemdiameter.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWelldrillstemdiameter_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWelldrillstemdiameter.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'drill stem's diameter
		DrillstemDiameter = Val(txtKillWelldrillstemdiameter.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellKickVol_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellKickVol.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellKickVol_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellKickVol.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'kick volume
		KickVol = Val(txtKillWellKickVol.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellMudDensity_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellMudDensity.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellMudDensity_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellMudDensity.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'Before kick, use mud density data.
		MudDensity = Val(txtKillWellMudDensity.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellOpenHoleExpands_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellOpenHoleExpands.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellOpenHoleExpands_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellOpenHoleExpands.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 ' After drilling hole,hole expands percent data.
		OpenHoleExpands = Val(txtKillWellOpenHoleExpands.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellShutinpressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellShutinpressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellShutinpressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellShutinpressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'After Shut in well,drill pipe  inside  pressure data.
		drillpipeP = Val(txtKillWellShutinpressure.Text)
	End Sub

   
   
End Class