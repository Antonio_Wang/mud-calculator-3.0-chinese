Option Strict Off
Option Explicit On
Friend Class frmunitVFluids
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtVFluidsUSbbl As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsUKbbl As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsm3 As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsLitre As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsml As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsfloz As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsUKpt As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsUSpt As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsUKgal As System.Windows.Forms.TextBox
	Public WithEvents txtVFluidsUSgal As System.Windows.Forms.TextBox
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents lblVFluidsUSbbl As System.Windows.Forms.Label
	Public WithEvents lblVFluidsUKbbl As System.Windows.Forms.Label
	Public WithEvents lblVFluids As System.Windows.Forms.Label
	Public WithEvents lblVFluidsm3 As System.Windows.Forms.Label
	Public WithEvents lblVFluidsLitre As System.Windows.Forms.Label
	Public WithEvents lblVFluidsml As System.Windows.Forms.Label
	Public WithEvents lblVFluidsfloz As System.Windows.Forms.Label
	Public WithEvents lblVFluidsUKpt As System.Windows.Forms.Label
	Public WithEvents lblVFluidsUSpt As System.Windows.Forms.Label
	Public WithEvents lblVFluidsUKgal As System.Windows.Forms.Label
	Public WithEvents lblVFluidsUSgal As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtVFluidsUSbbl = New System.Windows.Forms.TextBox
        Me.txtVFluidsUKbbl = New System.Windows.Forms.TextBox
        Me.txtVFluidsm3 = New System.Windows.Forms.TextBox
        Me.txtVFluidsLitre = New System.Windows.Forms.TextBox
        Me.txtVFluidsml = New System.Windows.Forms.TextBox
        Me.txtVFluidsfloz = New System.Windows.Forms.TextBox
        Me.txtVFluidsUKpt = New System.Windows.Forms.TextBox
        Me.txtVFluidsUSpt = New System.Windows.Forms.TextBox
        Me.txtVFluidsUKgal = New System.Windows.Forms.TextBox
        Me.txtVFluidsUSgal = New System.Windows.Forms.TextBox
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.lblVFluidsUSbbl = New System.Windows.Forms.Label
        Me.lblVFluidsUKbbl = New System.Windows.Forms.Label
        Me.lblVFluids = New System.Windows.Forms.Label
        Me.lblVFluidsm3 = New System.Windows.Forms.Label
        Me.lblVFluidsLitre = New System.Windows.Forms.Label
        Me.lblVFluidsml = New System.Windows.Forms.Label
        Me.lblVFluidsfloz = New System.Windows.Forms.Label
        Me.lblVFluidsUKpt = New System.Windows.Forms.Label
        Me.lblVFluidsUSpt = New System.Windows.Forms.Label
        Me.lblVFluidsUKgal = New System.Windows.Forms.Label
        Me.lblVFluidsUSgal = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtVFluidsUSbbl
        '
        Me.txtVFluidsUSbbl.AcceptsReturn = True
        Me.txtVFluidsUSbbl.AutoSize = False
        Me.txtVFluidsUSbbl.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUSbbl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUSbbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUSbbl.Location = New System.Drawing.Point(267, 190)
        Me.txtVFluidsUSbbl.MaxLength = 0
        Me.txtVFluidsUSbbl.Name = "txtVFluidsUSbbl"
        Me.txtVFluidsUSbbl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUSbbl.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsUSbbl.TabIndex = 20
        Me.txtVFluidsUSbbl.Text = ""
        '
        'txtVFluidsUKbbl
        '
        Me.txtVFluidsUKbbl.AcceptsReturn = True
        Me.txtVFluidsUKbbl.AutoSize = False
        Me.txtVFluidsUKbbl.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUKbbl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUKbbl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUKbbl.Location = New System.Drawing.Point(19, 190)
        Me.txtVFluidsUKbbl.MaxLength = 0
        Me.txtVFluidsUKbbl.Name = "txtVFluidsUKbbl"
        Me.txtVFluidsUKbbl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUKbbl.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsUKbbl.TabIndex = 19
        Me.txtVFluidsUKbbl.Text = ""
        '
        'txtVFluidsm3
        '
        Me.txtVFluidsm3.AcceptsReturn = True
        Me.txtVFluidsm3.AutoSize = False
        Me.txtVFluidsm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsm3.Location = New System.Drawing.Point(19, 52)
        Me.txtVFluidsm3.MaxLength = 0
        Me.txtVFluidsm3.Name = "txtVFluidsm3"
        Me.txtVFluidsm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsm3.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsm3.TabIndex = 9
        Me.txtVFluidsm3.Text = ""
        '
        'txtVFluidsLitre
        '
        Me.txtVFluidsLitre.AcceptsReturn = True
        Me.txtVFluidsLitre.AutoSize = False
        Me.txtVFluidsLitre.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsLitre.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsLitre.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsLitre.Location = New System.Drawing.Point(19, 86)
        Me.txtVFluidsLitre.MaxLength = 0
        Me.txtVFluidsLitre.Name = "txtVFluidsLitre"
        Me.txtVFluidsLitre.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsLitre.Size = New System.Drawing.Size(107, 20)
        Me.txtVFluidsLitre.TabIndex = 8
        Me.txtVFluidsLitre.Text = ""
        '
        'txtVFluidsml
        '
        Me.txtVFluidsml.AcceptsReturn = True
        Me.txtVFluidsml.AutoSize = False
        Me.txtVFluidsml.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsml.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsml.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsml.Location = New System.Drawing.Point(19, 121)
        Me.txtVFluidsml.MaxLength = 0
        Me.txtVFluidsml.Name = "txtVFluidsml"
        Me.txtVFluidsml.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsml.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsml.TabIndex = 7
        Me.txtVFluidsml.Text = ""
        '
        'txtVFluidsfloz
        '
        Me.txtVFluidsfloz.AcceptsReturn = True
        Me.txtVFluidsfloz.AutoSize = False
        Me.txtVFluidsfloz.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsfloz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsfloz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsfloz.Location = New System.Drawing.Point(19, 155)
        Me.txtVFluidsfloz.MaxLength = 0
        Me.txtVFluidsfloz.Name = "txtVFluidsfloz"
        Me.txtVFluidsfloz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsfloz.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsfloz.TabIndex = 6
        Me.txtVFluidsfloz.Text = ""
        '
        'txtVFluidsUKpt
        '
        Me.txtVFluidsUKpt.AcceptsReturn = True
        Me.txtVFluidsUKpt.AutoSize = False
        Me.txtVFluidsUKpt.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUKpt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUKpt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUKpt.Location = New System.Drawing.Point(267, 52)
        Me.txtVFluidsUKpt.MaxLength = 0
        Me.txtVFluidsUKpt.Name = "txtVFluidsUKpt"
        Me.txtVFluidsUKpt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUKpt.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsUKpt.TabIndex = 5
        Me.txtVFluidsUKpt.Text = ""
        '
        'txtVFluidsUSpt
        '
        Me.txtVFluidsUSpt.AcceptsReturn = True
        Me.txtVFluidsUSpt.AutoSize = False
        Me.txtVFluidsUSpt.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUSpt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUSpt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUSpt.Location = New System.Drawing.Point(267, 86)
        Me.txtVFluidsUSpt.MaxLength = 0
        Me.txtVFluidsUSpt.Name = "txtVFluidsUSpt"
        Me.txtVFluidsUSpt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUSpt.Size = New System.Drawing.Size(107, 20)
        Me.txtVFluidsUSpt.TabIndex = 4
        Me.txtVFluidsUSpt.Text = ""
        '
        'txtVFluidsUKgal
        '
        Me.txtVFluidsUKgal.AcceptsReturn = True
        Me.txtVFluidsUKgal.AutoSize = False
        Me.txtVFluidsUKgal.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUKgal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUKgal.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUKgal.Location = New System.Drawing.Point(267, 121)
        Me.txtVFluidsUKgal.MaxLength = 0
        Me.txtVFluidsUKgal.Name = "txtVFluidsUKgal"
        Me.txtVFluidsUKgal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUKgal.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsUKgal.TabIndex = 3
        Me.txtVFluidsUKgal.Text = ""
        '
        'txtVFluidsUSgal
        '
        Me.txtVFluidsUSgal.AcceptsReturn = True
        Me.txtVFluidsUSgal.AutoSize = False
        Me.txtVFluidsUSgal.BackColor = System.Drawing.SystemColors.Window
        Me.txtVFluidsUSgal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVFluidsUSgal.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVFluidsUSgal.Location = New System.Drawing.Point(267, 155)
        Me.txtVFluidsUSgal.MaxLength = 0
        Me.txtVFluidsUSgal.Name = "txtVFluidsUSgal"
        Me.txtVFluidsUSgal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVFluidsUSgal.Size = New System.Drawing.Size(107, 19)
        Me.txtVFluidsUSgal.TabIndex = 2
        Me.txtVFluidsUSgal.Text = ""
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(278, 233)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 1
        Me.cmdclear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(86, 233)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 0
        Me.cmdquit.Text = "退出"
        '
        'lblVFluidsUSbbl
        '
        Me.lblVFluidsUSbbl.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUSbbl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUSbbl.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUSbbl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUSbbl.Location = New System.Drawing.Point(392, 190)
        Me.lblVFluidsUSbbl.Name = "lblVFluidsUSbbl"
        Me.lblVFluidsUSbbl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUSbbl.Size = New System.Drawing.Size(97, 18)
        Me.lblVFluidsUSbbl.TabIndex = 22
        Me.lblVFluidsUSbbl.Text = "bbl(美制桶)"
        '
        'lblVFluidsUKbbl
        '
        Me.lblVFluidsUKbbl.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUKbbl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUKbbl.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUKbbl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUKbbl.Location = New System.Drawing.Point(144, 190)
        Me.lblVFluidsUKbbl.Name = "lblVFluidsUKbbl"
        Me.lblVFluidsUKbbl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUKbbl.Size = New System.Drawing.Size(107, 18)
        Me.lblVFluidsUKbbl.TabIndex = 21
        Me.lblVFluidsUKbbl.Text = "bbl(英制桶)"
        '
        'lblVFluids
        '
        Me.lblVFluids.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluids.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluids.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluids.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluids.Location = New System.Drawing.Point(10, 9)
        Me.lblVFluids.Name = "lblVFluids"
        Me.lblVFluids.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluids.Size = New System.Drawing.Size(250, 18)
        Me.lblVFluids.TabIndex = 18
        Me.lblVFluids.Text = "输入容积单位换算数据："
        '
        'lblVFluidsm3
        '
        Me.lblVFluidsm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsm3.Location = New System.Drawing.Point(144, 52)
        Me.lblVFluidsm3.Name = "lblVFluidsm3"
        Me.lblVFluidsm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsm3.Size = New System.Drawing.Size(107, 18)
        Me.lblVFluidsm3.TabIndex = 17
        Me.lblVFluidsm3.Text = "m3(立方米)"
        '
        'lblVFluidsLitre
        '
        Me.lblVFluidsLitre.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsLitre.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsLitre.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsLitre.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsLitre.Location = New System.Drawing.Point(144, 86)
        Me.lblVFluidsLitre.Name = "lblVFluidsLitre"
        Me.lblVFluidsLitre.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsLitre.Size = New System.Drawing.Size(107, 18)
        Me.lblVFluidsLitre.TabIndex = 16
        Me.lblVFluidsLitre.Text = "l(升)"
        '
        'lblVFluidsml
        '
        Me.lblVFluidsml.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsml.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsml.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsml.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsml.Location = New System.Drawing.Point(144, 121)
        Me.lblVFluidsml.Name = "lblVFluidsml"
        Me.lblVFluidsml.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsml.Size = New System.Drawing.Size(107, 18)
        Me.lblVFluidsml.TabIndex = 15
        Me.lblVFluidsml.Text = "ml(毫升)"
        '
        'lblVFluidsfloz
        '
        Me.lblVFluidsfloz.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsfloz.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsfloz.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsfloz.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsfloz.Location = New System.Drawing.Point(144, 155)
        Me.lblVFluidsfloz.Name = "lblVFluidsfloz"
        Me.lblVFluidsfloz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsfloz.Size = New System.Drawing.Size(107, 18)
        Me.lblVFluidsfloz.TabIndex = 14
        Me.lblVFluidsfloz.Text = "fl oz(液体盎司)"
        '
        'lblVFluidsUKpt
        '
        Me.lblVFluidsUKpt.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUKpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUKpt.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUKpt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUKpt.Location = New System.Drawing.Point(392, 52)
        Me.lblVFluidsUKpt.Name = "lblVFluidsUKpt"
        Me.lblVFluidsUKpt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUKpt.Size = New System.Drawing.Size(97, 18)
        Me.lblVFluidsUKpt.TabIndex = 13
        Me.lblVFluidsUKpt.Text = "pt(英品脱)"
        '
        'lblVFluidsUSpt
        '
        Me.lblVFluidsUSpt.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUSpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUSpt.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUSpt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUSpt.Location = New System.Drawing.Point(392, 86)
        Me.lblVFluidsUSpt.Name = "lblVFluidsUSpt"
        Me.lblVFluidsUSpt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUSpt.Size = New System.Drawing.Size(97, 18)
        Me.lblVFluidsUSpt.TabIndex = 12
        Me.lblVFluidsUSpt.Text = "pt(美品脱)"
        '
        'lblVFluidsUKgal
        '
        Me.lblVFluidsUKgal.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUKgal.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUKgal.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUKgal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUKgal.Location = New System.Drawing.Point(392, 121)
        Me.lblVFluidsUKgal.Name = "lblVFluidsUKgal"
        Me.lblVFluidsUKgal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUKgal.Size = New System.Drawing.Size(97, 18)
        Me.lblVFluidsUKgal.TabIndex = 11
        Me.lblVFluidsUKgal.Text = "gal(英加仑)"
        '
        'lblVFluidsUSgal
        '
        Me.lblVFluidsUSgal.BackColor = System.Drawing.SystemColors.Control
        Me.lblVFluidsUSgal.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVFluidsUSgal.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVFluidsUSgal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVFluidsUSgal.Location = New System.Drawing.Point(392, 155)
        Me.lblVFluidsUSgal.Name = "lblVFluidsUSgal"
        Me.lblVFluidsUSgal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVFluidsUSgal.Size = New System.Drawing.Size(97, 18)
        Me.lblVFluidsUSgal.TabIndex = 10
        Me.lblVFluidsUSgal.Text = "gal(美加仑)"
        '
        'frmunitVFluids
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(502, 277)
        Me.Controls.Add(Me.txtVFluidsUSbbl)
        Me.Controls.Add(Me.txtVFluidsUKbbl)
        Me.Controls.Add(Me.txtVFluidsm3)
        Me.Controls.Add(Me.txtVFluidsLitre)
        Me.Controls.Add(Me.txtVFluidsml)
        Me.Controls.Add(Me.txtVFluidsfloz)
        Me.Controls.Add(Me.txtVFluidsUKpt)
        Me.Controls.Add(Me.txtVFluidsUSpt)
        Me.Controls.Add(Me.txtVFluidsUKgal)
        Me.Controls.Add(Me.txtVFluidsUSgal)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.lblVFluidsUSbbl)
        Me.Controls.Add(Me.lblVFluidsUKbbl)
        Me.Controls.Add(Me.lblVFluids)
        Me.Controls.Add(Me.lblVFluidsm3)
        Me.Controls.Add(Me.lblVFluidsLitre)
        Me.Controls.Add(Me.lblVFluidsml)
        Me.Controls.Add(Me.lblVFluidsfloz)
        Me.Controls.Add(Me.lblVFluidsUKpt)
        Me.Controls.Add(Me.lblVFluidsUSpt)
        Me.Controls.Add(Me.lblVFluidsUKgal)
        Me.Controls.Add(Me.lblVFluidsUSgal)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitVFluids"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "容积(液体)Volume(Fluids)单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitVFluids
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitVFluids
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitVFluids()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月12日18:43于青海省海西洲马海马北油田
	'液体体积单位换算
	
	
	Dim m3 As Single
	Dim Litre As Single
	Dim ml As Single
	Dim UKgal As Single
	Dim USgal As Single
	Dim floz As Single
	Dim UKbbl As Single
	Dim USbbl As Single
	Dim UKpt As Single
	Dim USpt As Single
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitVFluids.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtVFluidsfloz_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsfloz.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '液体盎司fluid ounce(fl oz)－－单位换算
		floz = Val(txtVFluidsfloz.Text)
		
		m3 = floz * 28.4 / 1000000
		Litre = floz * 28.4 / 1000
		ml = floz * 28.4
		UKgal = floz * 28.4 / 0.2199 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = floz * 28.4 / 0.2642 '1gal(US)=3.785l    1ml=0.2642gal(US)
		'1floz=28.4ml       1ml=0.0352floz
		UKbbl = floz * 28.4 / 1000 / 159.11 '1bbl(UK)=159.110l
		USbbl = floz * 28.4 / 1000 / 158.984 '1bbl(US)=158.984l
		UKpt = floz * 28.4 / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = floz * 28.4 / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		'txtVFluidsfloz.Text = floz
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsLitre_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsLitre.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '升－－单位换算
		Litre = Val(txtVFluidsLitre.Text)
		
		m3 = Litre / 1000
		ml = Litre * 1000
		UKgal = Litre / 4.546 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = Litre / 3.785 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = Litre * 1000 / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = Litre / 159.11 '1bbl(UK)=159.110l
		USbbl = Litre / 158.984 '1bbl(US)=158.984l
		UKpt = Litre * 1.7606 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = Litre * 2.1141 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		'txtVFluidsLitre.Text = Litre
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方米 cubic metre--单位换算
		m3 = Val(txtVFluidsm3.Text)
		
		Litre = m3 * 1000
		ml = m3 * 1000000
		UKgal = m3 * 219.97 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = m3 * 264.172 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = m3 * 1000000 / 28.4 '1floz=28.4ml       1ml=0.0352floz
		UKbbl = m3 * 6.2848 '1bbl(UK)=159.110l
		USbbl = m3 * 6.2898 '1bbl(US)=158.984l
		UKpt = m3 * 1000 * 1.7606 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = m3 * 1000 * 2.1141 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsml_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsml.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ml = Val(txtVFluidsml.Text)
		
		m3 = ml / 1000000
		Litre = ml / 1000
		UKgal = ml / 1000 / 4.546 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = ml / 1000 / 3.785 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = ml / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = ml / 1000 / 159.11 '1bbl(UK)=159.110l
		USbbl = ml / 1000 / 158.984 '1bbl(US)=158.984l
		UKpt = ml / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = ml / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		'txtVFluidsml.Text = ml
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsUKbbl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUKbbl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制桶－－－单位转换
		UKbbl = Val(txtVFluidsUKbbl.Text)
		
		m3 = UKbbl * 0.1591
		Litre = UKbbl * 159.11
		ml = UKbbl * 159110#
		UKgal = UKbbl * 35.001 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = UKbbl * 42.0337 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = UKbbl * 159110# / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		'1bbl(UK)=159.110l
		USbbl = UKbbl * 1.0008 '1bbl(US)=158.984l
		UKpt = UKbbl * 159110# / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = UKbbl * 159110# / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		'txtVFluidsUKbbl.Text = UKbbl
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsUKgal_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUKgal.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制加仑－－单位转换
		UKgal = Val(txtVFluidsUKgal.Text)
		
		m3 = UKgal * 0.0045
		Litre = UKgal * 4.546
		ml = UKgal * 4546#
		'UKgal = UKgal                                          '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = UKgal * 1.201 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = UKgal * 4546 / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = UKgal * 0.0286 '1bbl(UK)=159.110l
		USbbl = UKgal * 0.0286 '1bbl(US)=158.984l
		UKpt = UKgal * 4546 / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = UKgal * 4546 / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		'txtVFluidsUKgal.Text = UKgal
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsUKpt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUKpt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制品脱  －单位转换
		UKpt = Val(txtVFluidsUKpt.Text)
		
		m3 = UKpt * 0.568 / 1000
		Litre = UKpt * 0.568
		ml = UKpt * 568
		UKgal = UKpt * 0.568 / 4.546 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = UKpt * 0.568 / 3.785 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = UKpt * 568 / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = UKpt * 0.568 / 159.11 '1bbl(UK)=159.110l
		USbbl = UKpt * 0.568 / 158.984 '1bbl(US)=158.984l
		'UKpt = UKpt                                           '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = UKpt * 0.568 / 0.473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		'txtVFluidsUKpt.Text = UKpt
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	
	Private Sub txtVFluidsUSbbl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUSbbl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '美制桶－－－单位转换
		USbbl = Val(txtVFluidsUSbbl.Text)
		
		m3 = USbbl * 0.159
		Litre = USbbl * 158.9873
		ml = USbbl * 158990#
		UKgal = USbbl * 34.9723 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = USbbl * 42# '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = USbbl * 158990# / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = USbbl * 0.9992 '1bbl(UK)=159.110l
		'USbbl = USbbl                                          '1bbl(US)=158.984l
		UKpt = USbbl * 158990# / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = USbbl * 158990# / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		'txtVFluidsUSbbl.Text = USbbl
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsUSgal_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUSgal.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '美制加仑－－单位转换
		USgal = Val(txtVFluidsUSgal.Text)
		
		m3 = USgal * 0.0038
		Litre = USgal * 3.7854
		ml = USgal * 3785.412
		UKgal = USgal * 0.8327 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		'USgal = USgal                                                  '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = USgal * 3785.412 / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = USgal * 0.0238 '1bbl(UK)=159.110l
		USbbl = USgal * 0.0238 '1bbl(US)=158.984l
		UKpt = USgal * 3785.412 / 568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		USpt = USgal * 3785.412 / 473 '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		'txtVFluidsUSgal.Text = USgal
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		txtVFluidsUSpt.Text = CStr(USpt)
	End Sub
	
	Private Sub txtVFluidsUSpt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVFluidsUSpt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '美制品脱－－单位转换
		USpt = Val(txtVFluidsUSpt.Text)
		
		m3 = USpt * 0.473 / 1000
		Litre = USpt * 0.473
		ml = USpt * 473
		UKgal = USpt * 0.473 / 4.546 '1gal(UK)=4.546l    1ml=0.2199gal(UK)   1m^3=220gal(UK)
		USgal = USpt * 0.473 / 3.785 '1gal(US)=3.785l    1ml=0.2642gal(US)
		floz = USpt * 473 / 28.4 '1fl oz=28.4ml       1ml=0.0352floz
		UKbbl = USpt * 0.473 / 159.11 '1bbl(UK)=159.110l
		USbbl = USpt * 0.473 / 158.984 '1bbl(US)=158.984l
		UKpt = USpt * 0.473 / 0.568 '1l=1.7606pt(UK)    1pt(UK)=0.568l=568ml
		'USpt = USpt'                                          '1l=2.1141pt(US)    1pt(US)=0.473l
		
		txtVFluidsm3.Text = CStr(m3)
		txtVFluidsLitre.Text = CStr(Litre)
		txtVFluidsml.Text = CStr(ml)
		txtVFluidsUKgal.Text = CStr(UKgal)
		txtVFluidsUSgal.Text = CStr(USgal)
		txtVFluidsfloz.Text = CStr(floz)
		txtVFluidsUKbbl.Text = CStr(UKbbl)
		txtVFluidsUSbbl.Text = CStr(USbbl)
		txtVFluidsUKpt.Text = CStr(UKpt)
		'txtVFluidsUSpt.Text = USpt
	End Sub
End Class