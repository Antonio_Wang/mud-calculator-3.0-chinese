Option Strict Off
Option Explicit On
Friend Class frmSolid
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdHelpSolid As System.Windows.Forms.Button
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents Text28 As System.Windows.Forms.TextBox
	Public WithEvents Text27 As System.Windows.Forms.TextBox
	Public WithEvents Text26 As System.Windows.Forms.TextBox
	Public WithEvents Text25 As System.Windows.Forms.TextBox
	Public WithEvents Text24 As System.Windows.Forms.TextBox
	Public WithEvents Text23 As System.Windows.Forms.TextBox
	Public WithEvents Text22 As System.Windows.Forms.TextBox
	Public WithEvents Text21 As System.Windows.Forms.TextBox
	Public WithEvents Text20 As System.Windows.Forms.TextBox
	Public WithEvents Text19 As System.Windows.Forms.TextBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents Text17 As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents Label28 As System.Windows.Forms.Label
	Public WithEvents Label27 As System.Windows.Forms.Label
	Public WithEvents Label26 As System.Windows.Forms.Label
	Public WithEvents Label25 As System.Windows.Forms.Label
	Public WithEvents Label24 As System.Windows.Forms.Label
	Public WithEvents Label23 As System.Windows.Forms.Label
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents Text29 As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents Text13 As System.Windows.Forms.TextBox
	Public WithEvents Label30 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Text12 As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents Label29 As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picMudSolidAnaysis As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolid))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text29 = New System.Windows.Forms.TextBox
        Me.Text14 = New System.Windows.Forms.TextBox
        Me.Text13 = New System.Windows.Forms.TextBox
        Me.Text12 = New System.Windows.Forms.TextBox
        Me.Text11 = New System.Windows.Forms.TextBox
        Me.Text9 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.picMudSolidAnaysis = New System.Windows.Forms.PictureBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdHelpSolid = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.Frame3 = New System.Windows.Forms.GroupBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.Text28 = New System.Windows.Forms.TextBox
        Me.Text27 = New System.Windows.Forms.TextBox
        Me.Text26 = New System.Windows.Forms.TextBox
        Me.Text25 = New System.Windows.Forms.TextBox
        Me.Text24 = New System.Windows.Forms.TextBox
        Me.Text23 = New System.Windows.Forms.TextBox
        Me.Text22 = New System.Windows.Forms.TextBox
        Me.Text21 = New System.Windows.Forms.TextBox
        Me.Text20 = New System.Windows.Forms.TextBox
        Me.Text19 = New System.Windows.Forms.TextBox
        Me.Text18 = New System.Windows.Forms.TextBox
        Me.Text17 = New System.Windows.Forms.TextBox
        Me.Text16 = New System.Windows.Forms.TextBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text15 = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Text10 = New System.Windows.Forms.TextBox
        Me.Text8 = New System.Windows.Forms.TextBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Frame3.SuspendLayout()
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Text29
        '
        Me.Text29.AcceptsReturn = True
        Me.Text29.AutoSize = False
        Me.Text29.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text29.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text29.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text29.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text29.Location = New System.Drawing.Point(192, 112)
        Me.Text29.MaxLength = 0
        Me.Text29.Name = "Text29"
        Me.Text29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text29.Size = New System.Drawing.Size(78, 18)
        Me.Text29.TabIndex = 64
        Me.Text29.Text = ""
        Me.ToolTip1.SetToolTip(Me.Text29, "固相中溶解盐体积百分比含量")
        '
        'Text14
        '
        Me.Text14.AcceptsReturn = True
        Me.Text14.AutoSize = False
        Me.Text14.BackColor = System.Drawing.SystemColors.Window
        Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text14.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text14.Location = New System.Drawing.Point(192, 55)
        Me.Text14.MaxLength = 0
        Me.Text14.Name = "Text14"
        Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text14.Size = New System.Drawing.Size(78, 19)
        Me.Text14.TabIndex = 30
        Me.Text14.Text = ""
        Me.ToolTip1.SetToolTip(Me.Text14, "不含溶解盐的固相体积百分比")
        '
        'Text13
        '
        Me.Text13.AcceptsReturn = True
        Me.Text13.AutoSize = False
        Me.Text13.BackColor = System.Drawing.SystemColors.Window
        Me.Text13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text13.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text13.Location = New System.Drawing.Point(192, 25)
        Me.Text13.MaxLength = 0
        Me.Text13.Name = "Text13"
        Me.Text13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text13.Size = New System.Drawing.Size(78, 19)
        Me.Text13.TabIndex = 29
        Me.Text13.Text = ""
        Me.ToolTip1.SetToolTip(Me.Text13, "含溶解盐的水相体积百分比")
        '
        'Text12
        '
        Me.Text12.AcceptsReturn = True
        Me.Text12.AutoSize = False
        Me.Text12.BackColor = System.Drawing.SystemColors.Window
        Me.Text12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text12.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text12.Location = New System.Drawing.Point(317, 376)
        Me.Text12.MaxLength = 0
        Me.Text12.Name = "Text12"
        Me.Text12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text12.Size = New System.Drawing.Size(97, 19)
        Me.Text12.TabIndex = 12
        Me.Text12.Text = "60"
        Me.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text12, "若未知,一般可为60")
        '
        'Text11
        '
        Me.Text11.AcceptsReturn = True
        Me.Text11.AutoSize = False
        Me.Text11.BackColor = System.Drawing.Color.White
        Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text11.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text11.Location = New System.Drawing.Point(317, 345)
        Me.Text11.MaxLength = 0
        Me.Text11.Name = "Text11"
        Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text11.Size = New System.Drawing.Size(97, 19)
        Me.Text11.TabIndex = 11
        Me.Text11.Text = "4.19"
        Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text11, "该值可实测取得，也可根据劣质土造浆率估计取值。水云母10－40，高岭石3－15。")
        '
        'Text9
        '
        Me.Text9.AcceptsReturn = True
        Me.Text9.AutoSize = False
        Me.Text9.BackColor = System.Drawing.SystemColors.Window
        Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text9.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text9.Location = New System.Drawing.Point(317, 283)
        Me.Text9.MaxLength = 0
        Me.Text9.Name = "Text9"
        Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text9.Size = New System.Drawing.Size(97, 20)
        Me.Text9.TabIndex = 9
        Me.Text9.Text = ""
        Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text9, "数据未知，可空缺。")
        Me.Text9.Visible = False
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(317, 159)
        Me.Text5.MaxLength = 0
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(97, 20)
        Me.Text5.TabIndex = 5
        Me.Text5.Text = "2.6"
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text5, "2.1～2.9,平均2.6")
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(317, 129)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(97, 20)
        Me.Text4.TabIndex = 4
        Me.Text4.Text = "0.84"
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text4, "若未知则用0.84")
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(317, 98)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(97, 19)
        Me.Text3.TabIndex = 3
        Me.Text3.Text = "4.2"
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text3, "重晶石粉密度为4.0～4.5g/cm^3，一般取4.2。")
        '
        'picMudSolidAnaysis
        '
        Me.picMudSolidAnaysis.Image = CType(resources.GetObject("picMudSolidAnaysis.Image"), System.Drawing.Image)
        Me.picMudSolidAnaysis.Location = New System.Drawing.Point(788, 24)
        Me.picMudSolidAnaysis.Name = "picMudSolidAnaysis"
        Me.picMudSolidAnaysis.Size = New System.Drawing.Size(96, 504)
        Me.picMudSolidAnaysis.TabIndex = 63
        Me.picMudSolidAnaysis.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picMudSolidAnaysis, "DAQING32 Rig,Qinghai China")
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(329, 452)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 20)
        Me.cmdOK.TabIndex = 58
        Me.cmdOK.Text = "Run"
        '
        'cmdHelpSolid
        '
        Me.cmdHelpSolid.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelpSolid.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelpSolid.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdHelpSolid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelpSolid.Location = New System.Drawing.Point(329, 508)
        Me.cmdHelpSolid.Name = "cmdHelpSolid"
        Me.cmdHelpSolid.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelpSolid.Size = New System.Drawing.Size(80, 20)
        Me.cmdHelpSolid.TabIndex = 57
        Me.cmdHelpSolid.Text = "Help"
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(329, 480)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 56
        Me.cmdExit.Text = "Exit"
        '
        'Frame3
        '
        Me.Frame3.BackColor = System.Drawing.SystemColors.Control
        Me.Frame3.Controls.Add(Me.PicLogo)
        Me.Frame3.Controls.Add(Me.Text28)
        Me.Frame3.Controls.Add(Me.Text27)
        Me.Frame3.Controls.Add(Me.Text26)
        Me.Frame3.Controls.Add(Me.Text25)
        Me.Frame3.Controls.Add(Me.Text24)
        Me.Frame3.Controls.Add(Me.Text23)
        Me.Frame3.Controls.Add(Me.Text22)
        Me.Frame3.Controls.Add(Me.Text21)
        Me.Frame3.Controls.Add(Me.Text20)
        Me.Frame3.Controls.Add(Me.Text19)
        Me.Frame3.Controls.Add(Me.Text18)
        Me.Frame3.Controls.Add(Me.Text17)
        Me.Frame3.Controls.Add(Me.Text16)
        Me.Frame3.Controls.Add(Me.Label28)
        Me.Frame3.Controls.Add(Me.Label27)
        Me.Frame3.Controls.Add(Me.Label26)
        Me.Frame3.Controls.Add(Me.Label25)
        Me.Frame3.Controls.Add(Me.Label24)
        Me.Frame3.Controls.Add(Me.Label23)
        Me.Frame3.Controls.Add(Me.Label22)
        Me.Frame3.Controls.Add(Me.Label21)
        Me.Frame3.Controls.Add(Me.Label20)
        Me.Frame3.Controls.Add(Me.Label19)
        Me.Frame3.Controls.Add(Me.Label18)
        Me.Frame3.Controls.Add(Me.Label17)
        Me.Frame3.Controls.Add(Me.Label16)
        Me.Frame3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame3.ForeColor = System.Drawing.Color.Red
        Me.Frame3.Location = New System.Drawing.Point(440, 8)
        Me.Frame3.Name = "Frame3"
        Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame3.Size = New System.Drawing.Size(340, 546)
        Me.Frame3.TabIndex = 32
        Me.Frame3.TabStop = False
        Me.Frame3.Text = "计算结果"
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(77, 465)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 65
        Me.PicLogo.TabStop = False
        '
        'Text28
        '
        Me.Text28.AcceptsReturn = True
        Me.Text28.AutoSize = False
        Me.Text28.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text28.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text28.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text28.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text28.Location = New System.Drawing.Point(251, 384)
        Me.Text28.MaxLength = 0
        Me.Text28.Name = "Text28"
        Me.Text28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text28.Size = New System.Drawing.Size(78, 24)
        Me.Text28.TabIndex = 61
        Me.Text28.Text = ""
        '
        'Text27
        '
        Me.Text27.AcceptsReturn = True
        Me.Text27.AutoSize = False
        Me.Text27.BackColor = System.Drawing.Color.White
        Me.Text27.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text27.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text27.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text27.Location = New System.Drawing.Point(251, 346)
        Me.Text27.MaxLength = 0
        Me.Text27.Name = "Text27"
        Me.Text27.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text27.Size = New System.Drawing.Size(78, 23)
        Me.Text27.TabIndex = 60
        Me.Text27.Text = ""
        '
        'Text26
        '
        Me.Text26.AcceptsReturn = True
        Me.Text26.AutoSize = False
        Me.Text26.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text26.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text26.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text26.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text26.Location = New System.Drawing.Point(251, 510)
        Me.Text26.MaxLength = 0
        Me.Text26.Name = "Text26"
        Me.Text26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text26.Size = New System.Drawing.Size(78, 24)
        Me.Text26.TabIndex = 54
        Me.Text26.Text = ""
        Me.Text26.Visible = False
        '
        'Text25
        '
        Me.Text25.AcceptsReturn = True
        Me.Text25.AutoSize = False
        Me.Text25.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text25.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text25.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text25.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text25.Location = New System.Drawing.Point(251, 463)
        Me.Text25.MaxLength = 0
        Me.Text25.Name = "Text25"
        Me.Text25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text25.Size = New System.Drawing.Size(78, 24)
        Me.Text25.TabIndex = 53
        Me.Text25.Text = ""
        Me.Text25.Visible = False
        '
        'Text24
        '
        Me.Text24.AcceptsReturn = True
        Me.Text24.AutoSize = False
        Me.Text24.BackColor = System.Drawing.SystemColors.Window
        Me.Text24.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text24.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text24.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text24.Location = New System.Drawing.Point(251, 424)
        Me.Text24.MaxLength = 0
        Me.Text24.Name = "Text24"
        Me.Text24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text24.Size = New System.Drawing.Size(78, 24)
        Me.Text24.TabIndex = 52
        Me.Text24.Text = ""
        '
        'Text23
        '
        Me.Text23.AcceptsReturn = True
        Me.Text23.AutoSize = False
        Me.Text23.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text23.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text23.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text23.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text23.Location = New System.Drawing.Point(251, 307)
        Me.Text23.MaxLength = 0
        Me.Text23.Name = "Text23"
        Me.Text23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text23.Size = New System.Drawing.Size(78, 24)
        Me.Text23.TabIndex = 51
        Me.Text23.Text = ""
        '
        'Text22
        '
        Me.Text22.AcceptsReturn = True
        Me.Text22.AutoSize = False
        Me.Text22.BackColor = System.Drawing.Color.White
        Me.Text22.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text22.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text22.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text22.Location = New System.Drawing.Point(251, 267)
        Me.Text22.MaxLength = 0
        Me.Text22.Name = "Text22"
        Me.Text22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text22.Size = New System.Drawing.Size(78, 24)
        Me.Text22.TabIndex = 50
        Me.Text22.Text = ""
        '
        'Text21
        '
        Me.Text21.AcceptsReturn = True
        Me.Text21.AutoSize = False
        Me.Text21.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(128, Byte))
        Me.Text21.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text21.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text21.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text21.Location = New System.Drawing.Point(251, 228)
        Me.Text21.MaxLength = 0
        Me.Text21.Name = "Text21"
        Me.Text21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text21.Size = New System.Drawing.Size(78, 24)
        Me.Text21.TabIndex = 49
        Me.Text21.Text = ""
        '
        'Text20
        '
        Me.Text20.AcceptsReturn = True
        Me.Text20.AutoSize = False
        Me.Text20.BackColor = System.Drawing.Color.White
        Me.Text20.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text20.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text20.Location = New System.Drawing.Point(251, 190)
        Me.Text20.MaxLength = 0
        Me.Text20.Name = "Text20"
        Me.Text20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text20.Size = New System.Drawing.Size(78, 23)
        Me.Text20.TabIndex = 48
        Me.Text20.Text = ""
        '
        'Text19
        '
        Me.Text19.AcceptsReturn = True
        Me.Text19.AutoSize = False
        Me.Text19.BackColor = System.Drawing.Color.White
        Me.Text19.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text19.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text19.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text19.Location = New System.Drawing.Point(251, 150)
        Me.Text19.MaxLength = 0
        Me.Text19.Name = "Text19"
        Me.Text19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text19.Size = New System.Drawing.Size(78, 23)
        Me.Text19.TabIndex = 47
        Me.Text19.Text = ""
        '
        'Text18
        '
        Me.Text18.AcceptsReturn = True
        Me.Text18.AutoSize = False
        Me.Text18.BackColor = System.Drawing.SystemColors.Window
        Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text18.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text18.Location = New System.Drawing.Point(251, 111)
        Me.Text18.MaxLength = 0
        Me.Text18.Name = "Text18"
        Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text18.Size = New System.Drawing.Size(78, 24)
        Me.Text18.TabIndex = 46
        Me.Text18.Text = ""
        '
        'Text17
        '
        Me.Text17.AcceptsReturn = True
        Me.Text17.AutoSize = False
        Me.Text17.BackColor = System.Drawing.SystemColors.Window
        Me.Text17.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text17.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text17.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text17.Location = New System.Drawing.Point(251, 72)
        Me.Text17.MaxLength = 0
        Me.Text17.Name = "Text17"
        Me.Text17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text17.Size = New System.Drawing.Size(78, 24)
        Me.Text17.TabIndex = 45
        Me.Text17.Text = ""
        '
        'Text16
        '
        Me.Text16.AcceptsReturn = True
        Me.Text16.AutoSize = False
        Me.Text16.BackColor = System.Drawing.SystemColors.Window
        Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text16.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text16.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text16.Location = New System.Drawing.Point(251, 32)
        Me.Text16.MaxLength = 0
        Me.Text16.Name = "Text16"
        Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text16.Size = New System.Drawing.Size(78, 24)
        Me.Text16.TabIndex = 44
        Me.Text16.Text = ""
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.SystemColors.Control
        Me.Label28.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(10, 390)
        Me.Label28.Name = "Label28"
        Me.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label28.Size = New System.Drawing.Size(231, 18)
        Me.Label28.TabIndex = 59
        Me.Label28.Text = "按NaCl计算含盐量浓度(kg/m^3)"
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.SystemColors.Control
        Me.Label27.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(12, 351)
        Me.Label27.Name = "Label27"
        Me.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label27.Size = New System.Drawing.Size(232, 18)
        Me.Label27.TabIndex = 55
        Me.Label27.Text = "NaCl含盐量质量百分比浓度(质量%)"
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.Control
        Me.Label26.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label26.Location = New System.Drawing.Point(12, 515)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label26.Size = New System.Drawing.Size(232, 18)
        Me.Label26.TabIndex = 43
        Me.Label26.Text = "钻井液蒸馏器固相浓度量(kg/m^3)"
        Me.Label26.Visible = False
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.Control
        Me.Label25.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label25.Location = New System.Drawing.Point(12, 467)
        Me.Label25.Name = "Label25"
        Me.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label25.Size = New System.Drawing.Size(232, 27)
        Me.Label25.TabIndex = 42
        Me.Label25.Text = "加重后钻井液的最高固相体积百分比(经验公式)"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label25.Visible = False
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.Control
        Me.Label24.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label24.Location = New System.Drawing.Point(10, 431)
        Me.Label24.Name = "Label24"
        Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label24.Size = New System.Drawing.Size(231, 18)
        Me.Label24.TabIndex = 41
        Me.Label24.Text = "钻屑/膨润土体积比值"
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label23.Location = New System.Drawing.Point(10, 311)
        Me.Label23.Name = "Label23"
        Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label23.Size = New System.Drawing.Size(231, 19)
        Me.Label23.TabIndex = 40
        Me.Label23.Text = "钻井液中钻屑浓度近似值(kg/m^3)"
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(10, 274)
        Me.Label22.Name = "Label22"
        Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label22.Size = New System.Drawing.Size(232, 18)
        Me.Label22.TabIndex = 39
        Me.Label22.Text = "钻井液中钻屑(V%)"
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(10, 234)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label21.Size = New System.Drawing.Size(236, 18)
        Me.Label21.TabIndex = 38
        Me.Label21.Text = "钻井液中膨润土浓度近似值(kg/m^3)"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(10, 195)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label20.Size = New System.Drawing.Size(231, 18)
        Me.Label20.TabIndex = 37
        Me.Label20.Text = "钻井液中校正膨润土(V%)"
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(10, 156)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label19.Size = New System.Drawing.Size(231, 18)
        Me.Label19.TabIndex = 36
        Me.Label19.Text = "钻井液高密度固相浓度(kg/m^3)"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(10, 117)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(231, 19)
        Me.Label18.TabIndex = 35
        Me.Label18.Text = "钻井液高密度固相(V%)"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(10, 79)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(231, 18)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "钻井液低密度固相浓度(kg/m^3)"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(10, 40)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(231, 18)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "钻井液低密度固相(V%)"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text29)
        Me.Frame2.Controls.Add(Me.Text15)
        Me.Frame2.Controls.Add(Me.Text14)
        Me.Frame2.Controls.Add(Me.Text13)
        Me.Frame2.Controls.Add(Me.Label30)
        Me.Frame2.Controls.Add(Me.Label15)
        Me.Frame2.Controls.Add(Me.Label14)
        Me.Frame2.Controls.Add(Me.Label13)
        Me.Frame2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(8, 419)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(280, 135)
        Me.Frame2.TabIndex = 13
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "校正固相结果"
        '
        'Text15
        '
        Me.Text15.AcceptsReturn = True
        Me.Text15.AutoSize = False
        Me.Text15.BackColor = System.Drawing.SystemColors.Window
        Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text15.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text15.Location = New System.Drawing.Point(192, 85)
        Me.Text15.MaxLength = 0
        Me.Text15.Name = "Text15"
        Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text15.Size = New System.Drawing.Size(78, 19)
        Me.Text15.TabIndex = 31
        Me.Text15.Text = ""
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.Control
        Me.Label30.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label30.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(10, 112)
        Me.Label30.Name = "Label30"
        Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label30.Size = New System.Drawing.Size(174, 18)
        Me.Label30.TabIndex = 63
        Me.Label30.Text = "溶解盐含量(V%)"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(10, 86)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(174, 18)
        Me.Label15.TabIndex = 28
        Me.Label15.Text = "油相含量(V%)"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(10, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(174, 18)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "校正固相含量(V%)"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(10, 26)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(174, 18)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "校正水相含量(V%)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Text12)
        Me.Frame1.Controls.Add(Me.Text11)
        Me.Frame1.Controls.Add(Me.Text10)
        Me.Frame1.Controls.Add(Me.Text9)
        Me.Frame1.Controls.Add(Me.Text8)
        Me.Frame1.Controls.Add(Me.Text7)
        Me.Frame1.Controls.Add(Me.Text6)
        Me.Frame1.Controls.Add(Me.Text5)
        Me.Frame1.Controls.Add(Me.Text4)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Label12)
        Me.Frame1.Controls.Add(Me.Label11)
        Me.Frame1.Controls.Add(Me.Label10)
        Me.Frame1.Controls.Add(Me.Label9)
        Me.Frame1.Controls.Add(Me.Label8)
        Me.Frame1.Controls.Add(Me.Label7)
        Me.Frame1.Controls.Add(Me.Label6)
        Me.Frame1.Controls.Add(Me.Label5)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Controls.Add(Me.Label1)
        Me.Frame1.Font = New System.Drawing.Font("Times New Roman", 10.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Red
        Me.Frame1.Location = New System.Drawing.Point(8, 8)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(424, 403)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "已知数据录入"
        '
        'Text10
        '
        Me.Text10.AcceptsReturn = True
        Me.Text10.AutoSize = False
        Me.Text10.BackColor = System.Drawing.SystemColors.Window
        Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text10.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text10.Location = New System.Drawing.Point(317, 313)
        Me.Text10.MaxLength = 0
        Me.Text10.Name = "Text10"
        Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text10.Size = New System.Drawing.Size(97, 20)
        Me.Text10.TabIndex = 10
        Me.Text10.Text = "38.61"
        Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text8
        '
        Me.Text8.AcceptsReturn = True
        Me.Text8.AutoSize = False
        Me.Text8.BackColor = System.Drawing.SystemColors.Window
        Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text8.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text8.Location = New System.Drawing.Point(317, 252)
        Me.Text8.MaxLength = 0
        Me.Text8.Name = "Text8"
        Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text8.Size = New System.Drawing.Size(97, 19)
        Me.Text8.TabIndex = 8
        Me.Text8.Text = "16"
        Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.SystemColors.Window
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(317, 222)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(97, 19)
        Me.Text7.TabIndex = 7
        Me.Text7.Text = "0"
        Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(317, 191)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(97, 19)
        Me.Text6.TabIndex = 6
        Me.Text6.Text = "84"
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(317, 68)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(97, 19)
        Me.Text2.TabIndex = 2
        Me.Text2.Text = "2530"
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(317, 37)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(97, 19)
        Me.Text1.TabIndex = 1
        Me.Text1.Text = "1.380"
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(10, 377)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(286, 18)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "膨润土阳离子交换容量(毫克当量/100g)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(10, 348)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(277, 18)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "钻屑阳离子交换容量(毫克当量/100g)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(10, 317)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(277, 18)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "亚甲基蓝膨润土含量 (g/L)"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(10, 286)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(277, 18)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "蒸馏器固相质量(g)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label9.Visible = False
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(10, 255)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(277, 18)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "固相体积比(%)"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(10, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(277, 18)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "油相体积比(%)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(10, 194)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(277, 18)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "水相体积比(%)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(10, 163)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(277, 18)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "低密度固相的密度 (g/cm^3)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(10, 132)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(277, 18)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "油的密度 (g/cm^3)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(10, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(277, 18)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "所用加重材料密度 (g/cm^3)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(10, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(277, 18)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "测样钻井液滤液氯离子浓度(Salt)(mg/L)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(10, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(277, 18)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "测样钻井液密度 (g/cm^3)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.Control
        Me.Label29.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label29.Location = New System.Drawing.Point(29, 558)
        Me.Label29.Name = "Label29"
        Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label29.Size = New System.Drawing.Size(779, 18)
        Me.Label29.TabIndex = 62
        Me.Label29.Text = "程序计算结果中棕褐色背景数据结果仅供参考。请大家甄别使用。为保证计算结果的准确性，请正确采集实验数据。"
        '
        'frmSolid
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picMudSolidAnaysis)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdHelpSolid)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.Frame3)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.Label29)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmSolid"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液固相数据分析"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame3.ResumeLayout(False)
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmSolid
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmSolid
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmSolid()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim Mmud As Single '钻井液密度
	Dim P氯 As Single '滤液氯化物浓度
	Dim Pnacl As Single 'NaCl浓度  单位mg/l
	Dim Pnaclppm As Single 'NaCl浓度  单位ppm
	Dim Mlym As Single '滤液密度
	Dim Mb As Single '加重剂密度
	Dim Moil As Single '油密度
	Dim Mlg As Single '低密度固相密度
	Dim Vw As Single '含水百分比
	Dim Vo As Single '含油百分比
	Dim Vs As Single '固含百分比
	Dim zlg As Single '蒸馏器固相质量
	Dim MBT As Single '亚甲基篮般含
	Dim CECzx As Single '钻屑阳离子交换容量
	Dim CECtu As Single '般土阳离子交换容量
	Dim CECpj As Single '低密度固相阳离子交换容量
	Dim Vww As Single '校正含水百分比
	Dim Vss As Single '校正固含百分比
	Dim Vlg As Single '低密度固相含量
	Dim Vb As Single '高密度固相含量
	Dim Vtu As Single '般土含量百分比
	Dim Vzx As Single '钻屑含量百分比
	Dim LGS As Single '低密度固相浓度
	Dim Dzx As Single '钻井液中钻屑的浓度近似值
	Dim DMbi As Single '钻屑与当量搬土含量的比值
	Dim Vnacl As Single '计算水溶物为氯化钠重量体积百分比
	Dim VVnacl As Single '溶解盐体积百分比
	Dim Mnacl As Single '计算水溶物按氯化钠计算的单位体积钻井液中Nacl质量 单位：Kg/m3
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		Mmud = Val(Text1.Text)
		P氯 = Val(Text2.Text)
		Mb = Val(Text3.Text)
		Moil = Val(Text4.Text)
		Mlg = Val(Text5.Text)
		Vw = Val(Text6.Text)
		Vo = Val(Text7.Text)
		Vs = Val(Text8.Text)
		zlg = Val(Text9.Text)
		MBT = Val(Text10.Text)
		CECzx = Val(Text11.Text)
		CECtu = Val(Text12.Text)
		
		If Vw + Vo + Vs <> 100 Then
			'MsgBox "请检查你输入的“钻井液样品含油、固含、含水百分比”数据是否有效！程序要求三项数据合计等于100％。", vbInformation, "钻井液固相分析"
            MsgBox("Please checkthat you have entered the 'Mud Voil%、Vs%、Vw% 'data! Because Voil%+Vs%+Vw%=100％.", MsgBoxStyle.Information, "Mud Solid Analysis")
			Exit Sub
		End If
		
		
		'计算滤液密度
		
		Mlym = 1# + 0.00000109 * P氯
		
		'Nacl含量浓度计算  单位mg/l（暂时不显示）
		
		Pnacl = ((23 + 35.5) / 35.5) * P氯
		
		'Nacl含量浓度计算  单位ppm（暂时不显示）
		
		Pnaclppm = Pnacl / Mlym
		
		'计算水溶物为氯化钠质量体积百分比
		
		Vnacl = ((Pnacl / Mlym) / 1000000) * 100
		
		
		'校正悬浮固相体积百分比
		
		Vss = Vs - (Vw * P氯) / (1680000 - 1.21 * P氯)
		VVnacl = (Vw * P氯) / (1680000 - 1.21 * P氯)
		
		'计算含氯化物水样体积百分比(公式废弃)
		
		Vww = 100 - Vss - Vo
		
		'确定显示格式:校正含水量
		
		Text13.Text = VB6.Format(Vww, "0.00")
		
		'显示校正悬浮固相和油相体积百分比
		
		Text14.Text = VB6.Format(Vss, "0.00")
		Text15.Text = VB6.Format(Vo, "0.00")
		
		'计算水溶物按氯化钠计算的单位体积钻井液中Nacl质量 单位：Kg/m3
		'公式中以1方钻井液1000升的校正含水体积数乘以Nacl含量浓度得出毫克重量换算为千克单位。
		
		Mnacl = (1000 * Vww / 100) * Pnacl / 1000000
		
		'-----------------------------------------------------------------------------
		
		'计算低密度固相体积百分比
		'公式为南海泥浆服务公司手册公式
		'Vlg = ((Vww * Mlym + Vss * Mb + Vo * Moil) - 100 * Mmud) / (Mb - Mlg)               停用
		
		'API13C标准公式中钻井液密度Mmud的计量单位为：磅/加仑，换算为单位g/cm3应乘8.3454
		
		'Vlg = (100 * Mlym + (Mb - Mlym) * Vss - 12 * 8.3454 * Mmud - (Mlym - Moil) * Vo) / (Mb - Mlg)     停用
		
		'钻井手册（甲方）上册P670页中公式错误，缺失减号。中海油泥浆服务公司手册公式如下。
		'---------------------------------------------------------------------------------
		Vlg = (100 * Mlym + (Mb - Mlym) * Vss - 100 * Mmud - (Mlym - Moil) * Vo) / (Mb - Mlg)
		
		'计算高密度固相体积百分比
		
		Vb = Vss - Vlg
		
		'计算校正后坂土/钻屑体积比，公式来源中海油泥浆服务公司手册
		
		CECpj = 2.695345 * MBT / Vlg
		Vtu = Vlg * (CECpj - CECzx) / (CECtu - CECzx)
		Vzx = Vlg - Vtu
		
		'由低密度固相体积百分比计算低密度固相浓度
		
		'LGS = Vlg * 25.9623           根据中海油钻井液手册中修正公式
		LGS = Vlg * Mlg * 3.49 * 2.853
		
		'钻井液中钻屑浓度近似值
		
		'Dzx = (LGS - MBT) / 0.85 (停用)               '公式来源自中海油泥浆服务公司手册，可能存在错误
		
		Dzx = Vzx * Mlg * 3.49 * 2.853
		
		
		'估算钻井液的钻屑与当量搬土含量的比值
		
		'DMbi = (LGS - MBT) / MBT(停用)
		
		'钻井液钻屑与膨润土体积比
		
		DMbi = Vzx / Vtu
		
		'分析结果显示
		Text16.Text = VB6.Format(Vlg, "0.00")
		Text17.Text = VB6.Format(LGS, "0.00")
		Text18.Text = VB6.Format(Vb, "0.00")
		Text19.Text = VB6.Format(Vb * Mb * 3.49 * 2.853, "0.00") '根据钻井液手册中修正公式高密度固相浓度计算
		Text20.Text = VB6.Format(Vtu, "0.00")
		Text21.Text = VB6.Format(LGS - Dzx, "0.00")
		Text22.Text = VB6.Format(Vzx, "0.00")
		Text23.Text = VB6.Format(Dzx, "0.00")
		Text24.Text = VB6.Format(DMbi, "0.00")
		Text25.Text = VB6.Format(24.47 * Mmud - 14.23, "0.00")
		Text26.Text = VB6.Format(zlg * 50, "0.00") '蒸馏器内固相质量折算固相浓度
		Text27.Text = VB6.Format(Vnacl, "0.00")
		Text28.Text = VB6.Format(Mnacl, "0.00")
		Text29.Text = VB6.Format(VVnacl, "0.00")
	End Sub
	
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdHelpSolid_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelpSolid.Click
		frmHelpSolid.DefInstance.ShowDialog()
	End Sub
	
	Private Sub frmSolid_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
		
		'*******************************************************************************
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdExit.Visible = False '双击logo 图案开始截图，设置命令按钮隐藏'
		cmdok.Visible = False
		cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MSolidAn.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MSolidAn.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        'cmdPicSave.Visible = True '作废 的控件

        cmdExit.Visible = True
        cmdOK.Visible = True
        cmdHelpSolid.Visible = True
	End Sub
	
	'钻井液密度数据输入限制，仅支持.0123456789及删除键
	Private Sub Text1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mmud = Val(Text1.Text)
	End Sub
	
	'钻井液MBT数据输入限制，仅支持.0123456789及删除键
	Private Sub Text10_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text10.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		MBT = Val(Text10.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456789及删除键
	Private Sub Text11_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text11.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text11_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text11.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		CECzx = Val(Text11.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456789及删除键
	Private Sub Text12_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text12.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text12_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text12.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		CECtu = Val(Text12.Text)
	End Sub
	
	'P氯数据输入限制，仅支持.0123456789及删除键
	Private Sub Text2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		P氯 = Val(Text2.Text)
	End Sub
	
	'加重剂密度数据输入限制，仅支持.0123456789及删除键
	Private Sub Text3_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text3.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mb = Val(Text3.Text)
	End Sub
	
	'油密度数据输入限制，仅支持.0123456789及删除键
	Private Sub Text4_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text4.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Moil = Val(Text4.Text)
	End Sub
	
	'低密度固相密度数据输入限制，仅支持.0123456789及删除键
	Private Sub Text5_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text5.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	Private Sub Text5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mlg = Val(Text5.Text)
	End Sub
	
	'水相体积百分比数据输入限制，仅支持.0123456789及删除键
	Private Sub Text6_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text6.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vw = Val(Text6.Text)
	End Sub
	
	'油相体积百分比数据输入限制，仅支持.0123456789及删除键
	Private Sub Text7_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text7.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vo = Val(Text7.Text)
	End Sub
	
	'固相体积百分比数据输入限制，仅支持.0123456789及删除键
	Private Sub Text8_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text8.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vs = Val(Text8.Text)
	End Sub
	
	'蒸馏器内固相质量数据输入限制，仅支持.0123456789及删除键
	Private Sub Text9_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text9.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		zlg = Val(Text9.Text)
	End Sub
End Class