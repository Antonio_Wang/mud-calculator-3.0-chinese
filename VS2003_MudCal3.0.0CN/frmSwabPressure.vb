Option Strict Off
Option Explicit On
Friend Class frmSwabPressure
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents txtSPoutputat As System.Windows.Forms.TextBox
	Public WithEvents txtSPoutputmpa As System.Windows.Forms.TextBox
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents fraSPoutput As System.Windows.Forms.GroupBox
	Public WithEvents _optSpWork_2 As System.Windows.Forms.RadioButton
	Public WithEvents _optSpWork_1 As System.Windows.Forms.RadioButton
	Public WithEvents _optSpWork_0 As System.Windows.Forms.RadioButton
	Public WithEvents fraSPworkchang As System.Windows.Forms.GroupBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtSPmw As System.Windows.Forms.TextBox
	Public WithEvents txtSPs6 As System.Windows.Forms.TextBox
	Public WithEvents txtSPs3 As System.Windows.Forms.TextBox
	Public WithEvents txtSPs0 As System.Windows.Forms.TextBox
	Public WithEvents txtSPqp As System.Windows.Forms.TextBox
	Public WithEvents txtSPvp As System.Windows.Forms.TextBox
	Public WithEvents txtSPl As System.Windows.Forms.TextBox
	Public WithEvents txtSPdpi As System.Windows.Forms.TextBox
	Public WithEvents txtSPdp As System.Windows.Forms.TextBox
	Public WithEvents txtSPdh As System.Windows.Forms.TextBox
	Public WithEvents txtSPwl As System.Windows.Forms.TextBox
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents fraSPinputdata As System.Windows.Forms.GroupBox
	Public WithEvents optSpWork As Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.fraSPoutput = New System.Windows.Forms.GroupBox
        Me.txtSPoutputat = New System.Windows.Forms.TextBox
        Me.txtSPoutputmpa = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.fraSPworkchang = New System.Windows.Forms.GroupBox
        Me._optSpWork_2 = New System.Windows.Forms.RadioButton
        Me._optSpWork_1 = New System.Windows.Forms.RadioButton
        Me._optSpWork_0 = New System.Windows.Forms.RadioButton
        Me.fraSPinputdata = New System.Windows.Forms.GroupBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.txtSPmw = New System.Windows.Forms.TextBox
        Me.txtSPs6 = New System.Windows.Forms.TextBox
        Me.txtSPs3 = New System.Windows.Forms.TextBox
        Me.txtSPs0 = New System.Windows.Forms.TextBox
        Me.txtSPqp = New System.Windows.Forms.TextBox
        Me.txtSPvp = New System.Windows.Forms.TextBox
        Me.txtSPl = New System.Windows.Forms.TextBox
        Me.txtSPdpi = New System.Windows.Forms.TextBox
        Me.txtSPdp = New System.Windows.Forms.TextBox
        Me.txtSPdh = New System.Windows.Forms.TextBox
        Me.txtSPwl = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.optSpWork = New Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray(Me.components)
        Me.fraSPoutput.SuspendLayout()
        Me.fraSPworkchang.SuspendLayout()
        Me.fraSPinputdata.SuspendLayout()
        CType(Me.optSpWork, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(527, 351)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(80, 20)
        Me.cmdquit.TabIndex = 33
        Me.cmdquit.Text = "返  回"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(402, 351)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 32
        Me.cmdok.Text = "计  算"
        '
        'fraSPoutput
        '
        Me.fraSPoutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraSPoutput.Controls.Add(Me.txtSPoutputat)
        Me.fraSPoutput.Controls.Add(Me.txtSPoutputmpa)
        Me.fraSPoutput.Controls.Add(Me.Label13)
        Me.fraSPoutput.Controls.Add(Me.Label12)
        Me.fraSPoutput.ForeColor = System.Drawing.Color.Red
        Me.fraSPoutput.Location = New System.Drawing.Point(362, 232)
        Me.fraSPoutput.Name = "fraSPoutput"
        Me.fraSPoutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSPoutput.Size = New System.Drawing.Size(270, 95)
        Me.fraSPoutput.TabIndex = 2
        Me.fraSPoutput.TabStop = False
        Me.fraSPoutput.Text = "计算结果"
        '
        'txtSPoutputat
        '
        Me.txtSPoutputat.AcceptsReturn = True
        Me.txtSPoutputat.AutoSize = False
        Me.txtSPoutputat.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPoutputat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPoutputat.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPoutputat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPoutputat.Location = New System.Drawing.Point(38, 60)
        Me.txtSPoutputat.MaxLength = 0
        Me.txtSPoutputat.Name = "txtSPoutputat"
        Me.txtSPoutputat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPoutputat.Size = New System.Drawing.Size(78, 20)
        Me.txtSPoutputat.TabIndex = 29
        Me.txtSPoutputat.Text = ""
        Me.txtSPoutputat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPoutputmpa
        '
        Me.txtSPoutputmpa.AcceptsReturn = True
        Me.txtSPoutputmpa.AutoSize = False
        Me.txtSPoutputmpa.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPoutputmpa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPoutputmpa.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPoutputmpa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPoutputmpa.Location = New System.Drawing.Point(38, 26)
        Me.txtSPoutputmpa.MaxLength = 0
        Me.txtSPoutputmpa.Name = "txtSPoutputmpa"
        Me.txtSPoutputmpa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPoutputmpa.Size = New System.Drawing.Size(78, 19)
        Me.txtSPoutputmpa.TabIndex = 28
        Me.txtSPoutputmpa.Text = ""
        Me.txtSPoutputmpa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(135, 61)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(68, 19)
        Me.Label13.TabIndex = 31
        Me.Label13.Text = "(at)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(135, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(59, 18)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "(MPa)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraSPworkchang
        '
        Me.fraSPworkchang.BackColor = System.Drawing.SystemColors.Control
        Me.fraSPworkchang.Controls.Add(Me._optSpWork_2)
        Me.fraSPworkchang.Controls.Add(Me._optSpWork_1)
        Me.fraSPworkchang.Controls.Add(Me._optSpWork_0)
        Me.fraSPworkchang.ForeColor = System.Drawing.Color.Blue
        Me.fraSPworkchang.Location = New System.Drawing.Point(8, 232)
        Me.fraSPworkchang.Name = "fraSPworkchang"
        Me.fraSPworkchang.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSPworkchang.Size = New System.Drawing.Size(346, 139)
        Me.fraSPworkchang.TabIndex = 1
        Me.fraSPworkchang.TabStop = False
        Me.fraSPworkchang.Text = "作业状态"
        '
        '_optSpWork_2
        '
        Me._optSpWork_2.BackColor = System.Drawing.SystemColors.Control
        Me._optSpWork_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._optSpWork_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSpWork.SetIndex(Me._optSpWork_2, CType(2, Short))
        Me._optSpWork_2.Location = New System.Drawing.Point(19, 95)
        Me._optSpWork_2.Name = "_optSpWork_2"
        Me._optSpWork_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optSpWork_2.Size = New System.Drawing.Size(309, 18)
        Me._optSpWork_2.TabIndex = 22
        Me._optSpWork_2.TabStop = True
        Me._optSpWork_2.Text = "开口--停泵状态的一般起下钻作业"
        '
        '_optSpWork_1
        '
        Me._optSpWork_1.BackColor = System.Drawing.SystemColors.Control
        Me._optSpWork_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._optSpWork_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSpWork.SetIndex(Me._optSpWork_1, CType(1, Short))
        Me._optSpWork_1.Location = New System.Drawing.Point(19, 60)
        Me._optSpWork_1.Name = "_optSpWork_1"
        Me._optSpWork_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optSpWork_1.Size = New System.Drawing.Size(309, 19)
        Me._optSpWork_1.TabIndex = 21
        Me._optSpWork_1.TabStop = True
        Me._optSpWork_1.Text = "封闭、开口--开泵状态划眼或活动套管"
        '
        '_optSpWork_0
        '
        Me._optSpWork_0.BackColor = System.Drawing.SystemColors.Control
        Me._optSpWork_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._optSpWork_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSpWork.SetIndex(Me._optSpWork_0, CType(0, Short))
        Me._optSpWork_0.Location = New System.Drawing.Point(19, 26)
        Me._optSpWork_0.Name = "_optSpWork_0"
        Me._optSpWork_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optSpWork_0.Size = New System.Drawing.Size(309, 18)
        Me._optSpWork_0.TabIndex = 20
        Me._optSpWork_0.TabStop = True
        Me._optSpWork_0.Text = "封闭--停泵状态下钻或下套管"
        '
        'fraSPinputdata
        '
        Me.fraSPinputdata.BackColor = System.Drawing.SystemColors.Control
        Me.fraSPinputdata.Controls.Add(Me.Text7)
        Me.fraSPinputdata.Controls.Add(Me.Text6)
        Me.fraSPinputdata.Controls.Add(Me.Text5)
        Me.fraSPinputdata.Controls.Add(Me.Text4)
        Me.fraSPinputdata.Controls.Add(Me.Text3)
        Me.fraSPinputdata.Controls.Add(Me.Text2)
        Me.fraSPinputdata.Controls.Add(Me.Text1)
        Me.fraSPinputdata.Controls.Add(Me.txtSPmw)
        Me.fraSPinputdata.Controls.Add(Me.txtSPs6)
        Me.fraSPinputdata.Controls.Add(Me.txtSPs3)
        Me.fraSPinputdata.Controls.Add(Me.txtSPs0)
        Me.fraSPinputdata.Controls.Add(Me.txtSPqp)
        Me.fraSPinputdata.Controls.Add(Me.txtSPvp)
        Me.fraSPinputdata.Controls.Add(Me.txtSPl)
        Me.fraSPinputdata.Controls.Add(Me.txtSPdpi)
        Me.fraSPinputdata.Controls.Add(Me.txtSPdp)
        Me.fraSPinputdata.Controls.Add(Me.txtSPdh)
        Me.fraSPinputdata.Controls.Add(Me.txtSPwl)
        Me.fraSPinputdata.Controls.Add(Me.Label11)
        Me.fraSPinputdata.Controls.Add(Me.Label10)
        Me.fraSPinputdata.Controls.Add(Me.Label9)
        Me.fraSPinputdata.Controls.Add(Me.Label8)
        Me.fraSPinputdata.Controls.Add(Me.Label7)
        Me.fraSPinputdata.Controls.Add(Me.Label6)
        Me.fraSPinputdata.Controls.Add(Me.Label5)
        Me.fraSPinputdata.Controls.Add(Me.Label4)
        Me.fraSPinputdata.Controls.Add(Me.Label3)
        Me.fraSPinputdata.Controls.Add(Me.Label2)
        Me.fraSPinputdata.Controls.Add(Me.Label1)
        Me.fraSPinputdata.ForeColor = System.Drawing.Color.Blue
        Me.fraSPinputdata.Location = New System.Drawing.Point(8, 8)
        Me.fraSPinputdata.Name = "fraSPinputdata"
        Me.fraSPinputdata.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSPinputdata.Size = New System.Drawing.Size(640, 216)
        Me.fraSPinputdata.TabIndex = 0
        Me.fraSPinputdata.TabStop = False
        Me.fraSPinputdata.Text = "已知数据输入"
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(576, 147)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(40, 20)
        Me.Text7.TabIndex = 40
        Me.Text7.Text = ""
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(259, 180)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(59, 19)
        Me.Text6.TabIndex = 39
        Me.Text6.Text = ""
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(259, 149)
        Me.Text5.MaxLength = 0
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(59, 19)
        Me.Text5.TabIndex = 38
        Me.Text5.Text = ""
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(259, 115)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(59, 20)
        Me.Text4.TabIndex = 37
        Me.Text4.Text = ""
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(259, 84)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(59, 19)
        Me.Text3.TabIndex = 36
        Me.Text3.Text = ""
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(259, 51)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(59, 20)
        Me.Text2.TabIndex = 35
        Me.Text2.Text = ""
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(259, 21)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(59, 19)
        Me.Text1.TabIndex = 34
        Me.Text1.Text = ""
        '
        'txtSPmw
        '
        Me.txtSPmw.AcceptsReturn = True
        Me.txtSPmw.AutoSize = False
        Me.txtSPmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPmw.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPmw.Location = New System.Drawing.Point(470, 147)
        Me.txtSPmw.MaxLength = 0
        Me.txtSPmw.Name = "txtSPmw"
        Me.txtSPmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPmw.Size = New System.Drawing.Size(88, 20)
        Me.txtSPmw.TabIndex = 19
        Me.txtSPmw.Text = "1.150"
        Me.txtSPmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPs6
        '
        Me.txtSPs6.AcceptsReturn = True
        Me.txtSPs6.AutoSize = False
        Me.txtSPs6.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPs6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPs6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPs6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPs6.Location = New System.Drawing.Point(470, 115)
        Me.txtSPs6.MaxLength = 0
        Me.txtSPs6.Name = "txtSPs6"
        Me.txtSPs6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPs6.Size = New System.Drawing.Size(88, 19)
        Me.txtSPs6.TabIndex = 18
        Me.txtSPs6.Text = "60"
        Me.txtSPs6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPs3
        '
        Me.txtSPs3.AcceptsReturn = True
        Me.txtSPs3.AutoSize = False
        Me.txtSPs3.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPs3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPs3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPs3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPs3.Location = New System.Drawing.Point(470, 83)
        Me.txtSPs3.MaxLength = 0
        Me.txtSPs3.Name = "txtSPs3"
        Me.txtSPs3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPs3.Size = New System.Drawing.Size(88, 19)
        Me.txtSPs3.TabIndex = 17
        Me.txtSPs3.Text = "38"
        Me.txtSPs3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPs0
        '
        Me.txtSPs0.AcceptsReturn = True
        Me.txtSPs0.AutoSize = False
        Me.txtSPs0.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPs0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPs0.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPs0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPs0.Location = New System.Drawing.Point(470, 50)
        Me.txtSPs0.MaxLength = 0
        Me.txtSPs0.Name = "txtSPs0"
        Me.txtSPs0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPs0.Size = New System.Drawing.Size(88, 20)
        Me.txtSPs0.TabIndex = 16
        Me.txtSPs0.Text = "0.5"
        Me.txtSPs0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPqp
        '
        Me.txtSPqp.AcceptsReturn = True
        Me.txtSPqp.AutoSize = False
        Me.txtSPqp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPqp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPqp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPqp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPqp.Location = New System.Drawing.Point(470, 20)
        Me.txtSPqp.MaxLength = 0
        Me.txtSPqp.Name = "txtSPqp"
        Me.txtSPqp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPqp.Size = New System.Drawing.Size(88, 19)
        Me.txtSPqp.TabIndex = 15
        Me.txtSPqp.Text = "30"
        Me.txtSPqp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPvp
        '
        Me.txtSPvp.AcceptsReturn = True
        Me.txtSPvp.AutoSize = False
        Me.txtSPvp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPvp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPvp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPvp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPvp.Location = New System.Drawing.Point(144, 180)
        Me.txtSPvp.MaxLength = 0
        Me.txtSPvp.Name = "txtSPvp"
        Me.txtSPvp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPvp.Size = New System.Drawing.Size(88, 19)
        Me.txtSPvp.TabIndex = 14
        Me.txtSPvp.Text = "1.05"
        Me.txtSPvp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPl
        '
        Me.txtSPl.AcceptsReturn = True
        Me.txtSPl.AutoSize = False
        Me.txtSPl.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPl.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPl.Location = New System.Drawing.Point(144, 148)
        Me.txtSPl.MaxLength = 0
        Me.txtSPl.Name = "txtSPl"
        Me.txtSPl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPl.Size = New System.Drawing.Size(88, 20)
        Me.txtSPl.TabIndex = 13
        Me.txtSPl.Text = "1500.32"
        Me.txtSPl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPdpi
        '
        Me.txtSPdpi.AcceptsReturn = True
        Me.txtSPdpi.AutoSize = False
        Me.txtSPdpi.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPdpi.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPdpi.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPdpi.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPdpi.Location = New System.Drawing.Point(144, 116)
        Me.txtSPdpi.MaxLength = 0
        Me.txtSPdpi.Name = "txtSPdpi"
        Me.txtSPdpi.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPdpi.Size = New System.Drawing.Size(88, 19)
        Me.txtSPdpi.TabIndex = 12
        Me.txtSPdpi.Text = "108.6"
        Me.txtSPdpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPdp
        '
        Me.txtSPdp.AcceptsReturn = True
        Me.txtSPdp.AutoSize = False
        Me.txtSPdp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPdp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPdp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPdp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPdp.Location = New System.Drawing.Point(144, 84)
        Me.txtSPdp.MaxLength = 0
        Me.txtSPdp.Name = "txtSPdp"
        Me.txtSPdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPdp.Size = New System.Drawing.Size(88, 19)
        Me.txtSPdp.TabIndex = 11
        Me.txtSPdp.Text = "127"
        Me.txtSPdp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPdh
        '
        Me.txtSPdh.AcceptsReturn = True
        Me.txtSPdh.AutoSize = False
        Me.txtSPdh.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPdh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPdh.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPdh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPdh.Location = New System.Drawing.Point(144, 52)
        Me.txtSPdh.MaxLength = 0
        Me.txtSPdh.Name = "txtSPdh"
        Me.txtSPdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPdh.Size = New System.Drawing.Size(88, 19)
        Me.txtSPdh.TabIndex = 10
        Me.txtSPdh.Text = "311"
        Me.txtSPdh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSPwl
        '
        Me.txtSPwl.AcceptsReturn = True
        Me.txtSPwl.AutoSize = False
        Me.txtSPwl.BackColor = System.Drawing.SystemColors.Window
        Me.txtSPwl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSPwl.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtSPwl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSPwl.Location = New System.Drawing.Point(144, 20)
        Me.txtSPwl.MaxLength = 0
        Me.txtSPwl.Name = "txtSPwl"
        Me.txtSPwl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSPwl.Size = New System.Drawing.Size(88, 20)
        Me.txtSPwl.TabIndex = 9
        Me.txtSPwl.Text = "2000"
        Me.txtSPwl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(336, 148)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(125, 19)
        Me.Label11.TabIndex = 27
        Me.Label11.Text = "钻井液密度(g/cm^3)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(336, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(125, 19)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "钻井液600转读数"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(336, 83)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(125, 19)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "钻井液300转读数"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(336, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(125, 19)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "钻井液3转读数"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(336, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(125, 19)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "排量(l/s)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(10, 180)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(126, 18)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "钻具起下速度(m/s)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(10, 149)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(126, 18)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "钻具长度(m)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(10, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(126, 18)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "钻具内径(mm)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(10, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(126, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "钻具外径(mm)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(10, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(126, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "井径(mm)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(10, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(126, 19)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "井深(m)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'optSpWork
        '
        '
        'frmSwabPressure
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(655, 385)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.fraSPoutput)
        Me.Controls.Add(Me.fraSPworkchang)
        Me.Controls.Add(Me.fraSPinputdata)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmSwabPressure"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "起下钻具(或套管)、活动钻具(或套管)以及扩划眼时的激动压力计算"
        Me.fraSPoutput.ResumeLayout(False)
        Me.fraSPworkchang.ResumeLayout(False)
        Me.fraSPinputdata.ResumeLayout(False)
        CType(Me.optSpWork, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmSwabPressure
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmSwabPressure
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmSwabPressure()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月11日23:51于青海省海西洲大柴旦镇红山参2井。加入纠错功能。
	'激动压力计算
	
	
	Const pi As Double = 3.14159
	
	Dim Wl As Single
	Dim dh As Single
	Dim dp As Single
	Dim Dpi As Single
	Dim L As Single
	Dim Qp As Single
	Dim Qi As Single
	Dim H As Short
	
	Dim mw As Single
	Dim s0 As Single
	Dim s3 As Single
	Dim s6 As Single
	Dim av As Single
	Dim Vp As Single
	Dim vcr As Single
	
	Dim N As Single
	Dim K As Single
	Dim f As Single
	Dim a As Single
	Dim b As Single
	Dim Re As Single
	
	Dim Psw As Single
	Dim Ppi As Single
	Dim Ph As Single
	
	Dim Qx As Single
	Dim df As Single
	Dim i As Single
	
	Dim tpi As Short
	Dim th As Short
	
	Dim WorkC As Short
	
	
	
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		'********************************************************************
		'已知数据默认值取值，对关键数据纠错提醒输入。
		'
		dh = Val(txtSPdh.Text)
		If txtSPdh.Text = "" Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information, "激动压力计算")
			Exit Sub '纠错功能解决。2006年8月8日王朝
		End If
		
		If dh = 0 Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
			
		End If
		dp = Val(txtSPdp.Text)
		If dp >= dh Then
			MsgBox("请检查你输入的数据是否有效！", MsgBoxStyle.Information)
		End If
		
		Wl = Val(txtSPwl.Text)
		L = Val(txtSPl.Text)
		mw = Val(txtSPmw.Text)
		Dpi = Val(txtSPdpi.Text)
		Qp = Val(txtSPqp.Text)
		s0 = Val(txtSPs0.Text)
		s3 = Val(txtSPs3.Text)
		s6 = Val(txtSPs6.Text)
		Vp = Val(txtSPvp.Text)
		
		
		'**************************************************************************
		'第一步：根据已知的3、300、600转读数计算钻井液的幂律模式的n  、k值
		
		
		'UPGRADE_WARNING: 未能解析对象 Log10() 的默认属性。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"”
        N = 3.32 * Math.Log10((s6 - s0) / (s3 - s0))
		K = (s3 - s0) / (511 ^ N)
		
		Text3.Text = VB6.Format(N, "0.00")
		Text4.Text = VB6.Format(K, "0.000")
		
		'**************************************************************************
		'第二步：根据已知的钻井液的幂律模式的n  、k值、钻井液密度mw、井径Dh、钻具外径Dp计算环空临界流速Vcr值
		'公式选择为：胜利油田【钻井技术手册】第624页上部关于幂律流体的环空临界流速计算
		
		vcr = 0.00508 * ((2.04 * 10 ^ 4 * N ^ 0.387 * K / mw) * (25.5 / (dh - dp)) ^ N) ^ (1 / (2 - N)) '『钻井工程技术手册』P95公式
		Text1.Text = VB6.Format(vcr, "0.00")
		
		'**************************************************************************
		'第三步：根据已知的作业工况通过条件语句分别计算选定工况的环空流速并与临界流速对比得出环空层流或紊流情况
		
		
		Select Case WorkC
			Case 0
				av = 1.5 * Vp * (0.5 + ((dp / 1000) ^ 2 / ((dh / 1000) ^ 2 - (dp / 1000) ^ 2))) '井径、钻具外径的单位公式中为m，需换算除1000。
				
				Text2.Text = VB6.Format(av, "0.00")
				If av <= vcr Then
					Psw = (av * 100 / (dh / 10 - dp / 10) * (4 * (2 * N + 1) / N)) ^ N * ((4 * K * L * 100) / (dh / 10 - dp / 10)) '换算环空流速单位为厘米/秒，必须乘以100
				Else

                    a = (Math.Log10(N) + 3.93) / 50

                    b = (1.75 - Math.Log10(N)) / 7
					Re = (mw * (dh / 10 - dp / 10) ^ N * (av * 100) ^ (2 - N)) / (12 ^ (N - 1) * K * ((2 * N + 1) / (3 * N)))
					f = a / Re ^ b
					Psw = (2 * f * mw * L * 100 * (av * 100) ^ 2 / (dh / 10 - dp / 10)) '除9800000换算为kg/cm^3单位
				End If '自编函数的调用可能错误
				txtSPoutputmpa.Text = VB6.Format(Psw / 980000 / 10, "0.0000")
				txtSPoutputat.Text = VB6.Format(Psw / 980000, "0.0000")
			Case 1
				av = 1.5 * Vp * (0.5 + ((dp / 1000) ^ 2 - (Dpi / 1000) ^ 2) / ((dh / 1000) ^ 2 - (dp / 1000) ^ 2)) + ((Qp / 1000) / (pi / 4 * ((dh / 1000) ^ 2 - (dp / 1000) ^ 2)))
				
				Text2.Text = VB6.Format(av, "0.00")
				If av <= vcr Then
					Psw = (av * 100 / (dh / 10 - dp / 10) * (4 * (2 * N + 1) / N)) ^ N * ((4 * K * L * 100) / (dh / 10 - dp / 10))
				Else

                    a = (Math.Log10(N) + 3.93) / 50

                    b = (1.75 - Math.Log10(N)) / 7
					Re = (mw * (dh / 10 - dp / 10) ^ N * (av * 100) ^ (2 - N)) / (12 ^ (N - 1) * K * ((2 * N + 1) / (3 * N)))
					f = a / Re ^ b
					Psw = (2 * f * mw * L * 100 * (av * 100) ^ 2 / (dh / 10 - dp / 10)) '除9800000换算为kg/cm^3单位
				End If '自编函数的调用可能错误
				txtSPoutputmpa.Text = VB6.Format(Psw / 980000 / 10, "0.0000")
				txtSPoutputat.Text = VB6.Format(Psw / 980000, "0.0000")
			Case 2
				Qx = pi / 4 * ((dp / 100) ^ 2 - (Dpi / 100) ^ 2) * Vp * 10 + 1
				H = Int(Qx)
				Text5.Text = VB6.Format(H, "0.00")
				For i = 1 To H Step 0.1
					Qi = i
					av = 1.5 * Vp * (0.5 + ((dp / 1000) ^ 2 - (Dpi / 1000) ^ 2) / ((dh / 1000) ^ 2 - (dp / 1000) ^ 2)) + ((Qi / 1000) / (pi / 4 * ((dh / 1000) ^ 2 - (dp / 1000) ^ 2)))
					Ppi = ((800 * av / Dpi / 10) * ((3 * N + 1) / 4 * N)) ^ N * (L * K / 2450 * Dpi / 10)
					Ph = (1200 * av / (dh / 10 - dp / 10) * ((2 * N + 1) / 3 * N)) ^ N * (L * K / 2450 * (dh / 10 - dp / 10))
					tpi = Int(Ppi)
					th = Int(Ph)
					Text2.Text = VB6.Format(av, "0.00")
					Text6.Text = VB6.Format(Qi, "0.00")
					Text7.Text = CStr(i)
					
					If tpi = th Then
						
						Exit For
					End If
					
					
					
				Next i
				If av <= vcr Then
					Psw = (av * 100 / (dh / 10 - dp / 10) * (4 * (2 * N + 1) / N)) ^ N * ((4 * K * L * 100) / (dh / 10 - dp / 10))
				Else

                    a = (Math.Log10(N) + 3.93) / 50

                    b = (1.75 - Math.Log10(N)) / 7
					Re = (mw * (dh / 10 - dp / 10) ^ N * (av * 100) ^ (2 - N)) / (12 ^ (N - 1) * K * ((2 * N + 1) / (3 * N)))
					f = a / Re ^ b
					Psw = (2 * f * mw * L * 100 * (av * 100) ^ 2 / (dh / 10 - dp / 10)) '除9800000换算为kg/cm^3单位
				End If '自编函数的调用可能错误
				txtSPoutputmpa.Text = VB6.Format(Psw / 980000 / 10, "0.0000")
				txtSPoutputat.Text = VB6.Format(Psw / 980000, "0.0000")
				
				
		End Select
		
		
		
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 optSpWork.CheckedChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub optSpWork_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSpWork.CheckedChanged
		If eventSender.Checked Then
			Dim Index As Short = optSpWork.GetIndex(eventSender)
			Select Case Index
				Case 0
					WorkC = 0
				Case 1
					WorkC = 1
				Case 2
					WorkC = 2
			End Select
		End If
	End Sub
	
	Private Sub txtSPdh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPdh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dh = Val(txtSPdh.Text)
	End Sub
	
	Private Sub txtSPdp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPdp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dp = Val(txtSPdp.Text)
	End Sub
	
	Private Sub txtSPdpi_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPdpi.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Dpi = Val(txtSPdpi.Text)
	End Sub
	
	Private Sub txtSPl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		L = Val(txtSPl.Text)
	End Sub
	
	Private Sub txtSPmw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPmw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mw = Val(txtSPmw.Text)
	End Sub
	
	Private Sub txtSPqp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPqp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Qp = Val(txtSPqp.Text)
	End Sub
	
	Private Sub txtSPs0_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPs0.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s0 = Val(txtSPs0.Text)
	End Sub
	
	Private Sub txtSPs3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPs3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s3 = Val(txtSPs3.Text)
	End Sub
	
	Private Sub txtSPs6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPs6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s6 = Val(txtSPs6.Text)
	End Sub
	
	Private Sub txtSPvp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPvp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vp = Val(txtSPvp.Text)
	End Sub
	
	Private Sub txtSPwl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSPwl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Wl = Val(txtSPwl.Text)
	End Sub
End Class