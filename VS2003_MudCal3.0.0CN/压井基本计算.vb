Option Strict Off
Option Explicit On
Friend Class frmKillWellbasecal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents txtKillWellCalTotalDepth As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCaldrillsteminsidediameter As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalKWPumppressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalKWDOP As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalCSLMW As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalCasingshoedepth As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCasingpressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellKickVol As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellBitdiameter As System.Windows.Forms.TextBox
	Public WithEvents txtKillWelldrillstemdiameter As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellOpenHoleExpands As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellMudDensity As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellShutinpressure As System.Windows.Forms.TextBox
	Public WithEvents lblKillWellCalTotalDepth As System.Windows.Forms.Label
	Public WithEvents lblKillWelldrillsteminsidediameter As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalKWPumppressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalKWDOP As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalCSLMW As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalCasingshoedepth As System.Windows.Forms.Label
	Public WithEvents lblKillWellCasingpressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellKickVol As System.Windows.Forms.Label
	Public WithEvents lblKillWellBitdiameter As System.Windows.Forms.Label
	Public WithEvents lblKillWelldrillstemdiameter As System.Windows.Forms.Label
	Public WithEvents lblKillWellOpenHoleExpands As System.Windows.Forms.Label
	Public WithEvents lblKillWellMudDensity As System.Windows.Forms.Label
	Public WithEvents lblKillWellShutinpressure As System.Windows.Forms.Label
	Public WithEvents fraKillWellInputdata As System.Windows.Forms.GroupBox
	Public WithEvents lblKillWellProgram As System.Windows.Forms.Label
	Public WithEvents fraKillWellProgram As System.Windows.Forms.GroupBox
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents txtKillWellCalTimeInsidePipe As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalTimeAnnulus As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalEndCirculatingPressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalStarCirculatingPressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalKTWMaxCasingPressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalMaxShutInCasingPressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalDrillingMudDensity As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalStratumPressureMudDensity As System.Windows.Forms.TextBox
	Public WithEvents txtKillWellCalStratumPressure As System.Windows.Forms.TextBox
	Public WithEvents txtKillingWellstyle As System.Windows.Forms.TextBox
	Public WithEvents lblKillWellCalTimeInsidePipe As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalTimeAnnulus As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalEndCirculatingPressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalStarCirculatingPressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalKTWMaxCasingPressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalMaxShutInCasingPressure As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalDrillingMudDensity As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalStratumPressureMudDensity As System.Windows.Forms.Label
	Public WithEvents lblKillWellCalStratumPressure As System.Windows.Forms.Label
	Public WithEvents lblKillingWellStyleDensity As System.Windows.Forms.Label
	Public WithEvents fraKillingWellOutput As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmKillWellbasecal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtKillingWellstyle = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.fraKillWellInputdata = New System.Windows.Forms.GroupBox
        Me.txtKillWellCalTotalDepth = New System.Windows.Forms.TextBox
        Me.txtKillWellCaldrillsteminsidediameter = New System.Windows.Forms.TextBox
        Me.txtKillWellCalKWPumppressure = New System.Windows.Forms.TextBox
        Me.txtKillWellCalKWDOP = New System.Windows.Forms.TextBox
        Me.txtKillWellCalCSLMW = New System.Windows.Forms.TextBox
        Me.txtKillWellCalCasingshoedepth = New System.Windows.Forms.TextBox
        Me.txtKillWellCasingpressure = New System.Windows.Forms.TextBox
        Me.txtKillWellKickVol = New System.Windows.Forms.TextBox
        Me.txtKillWellBitdiameter = New System.Windows.Forms.TextBox
        Me.txtKillWelldrillstemdiameter = New System.Windows.Forms.TextBox
        Me.txtKillWellOpenHoleExpands = New System.Windows.Forms.TextBox
        Me.txtKillWellMudDensity = New System.Windows.Forms.TextBox
        Me.txtKillWellShutinpressure = New System.Windows.Forms.TextBox
        Me.lblKillWellCalTotalDepth = New System.Windows.Forms.Label
        Me.lblKillWelldrillsteminsidediameter = New System.Windows.Forms.Label
        Me.lblKillWellCalKWPumppressure = New System.Windows.Forms.Label
        Me.lblKillWellCalKWDOP = New System.Windows.Forms.Label
        Me.lblKillWellCalCSLMW = New System.Windows.Forms.Label
        Me.lblKillWellCalCasingshoedepth = New System.Windows.Forms.Label
        Me.lblKillWellCasingpressure = New System.Windows.Forms.Label
        Me.lblKillWellKickVol = New System.Windows.Forms.Label
        Me.lblKillWellBitdiameter = New System.Windows.Forms.Label
        Me.lblKillWelldrillstemdiameter = New System.Windows.Forms.Label
        Me.lblKillWellOpenHoleExpands = New System.Windows.Forms.Label
        Me.lblKillWellMudDensity = New System.Windows.Forms.Label
        Me.lblKillWellShutinpressure = New System.Windows.Forms.Label
        Me.fraKillWellProgram = New System.Windows.Forms.GroupBox
        Me.lblKillWellProgram = New System.Windows.Forms.Label
        Me.cmdok = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.fraKillingWellOutput = New System.Windows.Forms.GroupBox
        Me.txtKillWellCalTimeInsidePipe = New System.Windows.Forms.TextBox
        Me.txtKillWellCalTimeAnnulus = New System.Windows.Forms.TextBox
        Me.txtKillWellCalEndCirculatingPressure = New System.Windows.Forms.TextBox
        Me.txtKillWellCalStarCirculatingPressure = New System.Windows.Forms.TextBox
        Me.txtKillWellCalKTWMaxCasingPressure = New System.Windows.Forms.TextBox
        Me.txtKillWellCalMaxShutInCasingPressure = New System.Windows.Forms.TextBox
        Me.txtKillWellCalDrillingMudDensity = New System.Windows.Forms.TextBox
        Me.txtKillWellCalStratumPressureMudDensity = New System.Windows.Forms.TextBox
        Me.txtKillWellCalStratumPressure = New System.Windows.Forms.TextBox
        Me.lblKillWellCalTimeInsidePipe = New System.Windows.Forms.Label
        Me.lblKillWellCalTimeAnnulus = New System.Windows.Forms.Label
        Me.lblKillWellCalEndCirculatingPressure = New System.Windows.Forms.Label
        Me.lblKillWellCalStarCirculatingPressure = New System.Windows.Forms.Label
        Me.lblKillWellCalKTWMaxCasingPressure = New System.Windows.Forms.Label
        Me.lblKillWellCalMaxShutInCasingPressure = New System.Windows.Forms.Label
        Me.lblKillWellCalDrillingMudDensity = New System.Windows.Forms.Label
        Me.lblKillWellCalStratumPressureMudDensity = New System.Windows.Forms.Label
        Me.lblKillWellCalStratumPressure = New System.Windows.Forms.Label
        Me.lblKillingWellStyleDensity = New System.Windows.Forms.Label
        Me.fraKillWellInputdata.SuspendLayout()
        Me.fraKillWellProgram.SuspendLayout()
        Me.fraKillingWellOutput.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtKillingWellstyle
        '
        Me.txtKillingWellstyle.AcceptsReturn = True
        Me.txtKillingWellstyle.AutoSize = False
        Me.txtKillingWellstyle.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillingWellstyle.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillingWellstyle.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillingWellstyle.ForeColor = System.Drawing.Color.Red
        Me.txtKillingWellstyle.Location = New System.Drawing.Point(304, 24)
        Me.txtKillingWellstyle.MaxLength = 0
        Me.txtKillingWellstyle.Name = "txtKillingWellstyle"
        Me.txtKillingWellstyle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillingWellstyle.Size = New System.Drawing.Size(80, 20)
        Me.txtKillingWellstyle.TabIndex = 1
        Me.txtKillingWellstyle.Text = ""
        Me.txtKillingWellstyle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtKillingWellstyle, "溢流流体的种类判别：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.12～0.36g/cm3  为天然气溢流；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.37～0.60g/cm3  为油；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.61～0.84g/cm3  为油水混合；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.85～1.08g/cm3  为水。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(696, 470)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 53
        Me.PicLogo.TabStop = False
        '
        'fraKillWellInputdata
        '
        Me.fraKillWellInputdata.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCalTotalDepth)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCaldrillsteminsidediameter)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCalKWPumppressure)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCalKWDOP)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCalCSLMW)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCalCasingshoedepth)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellCasingpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellKickVol)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellBitdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWelldrillstemdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellOpenHoleExpands)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellMudDensity)
        Me.fraKillWellInputdata.Controls.Add(Me.txtKillWellShutinpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCalTotalDepth)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWelldrillsteminsidediameter)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCalKWPumppressure)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCalKWDOP)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCalCSLMW)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCalCasingshoedepth)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellCasingpressure)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellKickVol)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellBitdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWelldrillstemdiameter)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellOpenHoleExpands)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellMudDensity)
        Me.fraKillWellInputdata.Controls.Add(Me.lblKillWellShutinpressure)
        Me.fraKillWellInputdata.ForeColor = System.Drawing.Color.Blue
        Me.fraKillWellInputdata.Location = New System.Drawing.Point(8, 99)
        Me.fraKillWellInputdata.Name = "fraKillWellInputdata"
        Me.fraKillWellInputdata.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillWellInputdata.Size = New System.Drawing.Size(424, 470)
        Me.fraKillWellInputdata.TabIndex = 8
        Me.fraKillWellInputdata.TabStop = False
        Me.fraKillWellInputdata.Text = "数据录入"
        '
        'txtKillWellCalTotalDepth
        '
        Me.txtKillWellCalTotalDepth.AcceptsReturn = True
        Me.txtKillWellCalTotalDepth.AutoSize = False
        Me.txtKillWellCalTotalDepth.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalTotalDepth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalTotalDepth.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalTotalDepth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCalTotalDepth.Location = New System.Drawing.Point(293, 22)
        Me.txtKillWellCalTotalDepth.MaxLength = 0
        Me.txtKillWellCalTotalDepth.Name = "txtKillWellCalTotalDepth"
        Me.txtKillWellCalTotalDepth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalTotalDepth.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalTotalDepth.TabIndex = 34
        Me.txtKillWellCalTotalDepth.Text = "2000"
        Me.txtKillWellCalTotalDepth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCaldrillsteminsidediameter
        '
        Me.txtKillWellCaldrillsteminsidediameter.AcceptsReturn = True
        Me.txtKillWellCaldrillsteminsidediameter.AutoSize = False
        Me.txtKillWellCaldrillsteminsidediameter.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCaldrillsteminsidediameter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCaldrillsteminsidediameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCaldrillsteminsidediameter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCaldrillsteminsidediameter.Location = New System.Drawing.Point(293, 232)
        Me.txtKillWellCaldrillsteminsidediameter.MaxLength = 0
        Me.txtKillWellCaldrillsteminsidediameter.Name = "txtKillWellCaldrillsteminsidediameter"
        Me.txtKillWellCaldrillsteminsidediameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCaldrillsteminsidediameter.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCaldrillsteminsidediameter.TabIndex = 32
        Me.txtKillWellCaldrillsteminsidediameter.Text = "108.6"
        Me.txtKillWellCaldrillsteminsidediameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalKWPumppressure
        '
        Me.txtKillWellCalKWPumppressure.AcceptsReturn = True
        Me.txtKillWellCalKWPumppressure.AutoSize = False
        Me.txtKillWellCalKWPumppressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalKWPumppressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalKWPumppressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalKWPumppressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCalKWPumppressure.Location = New System.Drawing.Point(293, 442)
        Me.txtKillWellCalKWPumppressure.MaxLength = 0
        Me.txtKillWellCalKWPumppressure.Name = "txtKillWellCalKWPumppressure"
        Me.txtKillWellCalKWPumppressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalKWPumppressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalKWPumppressure.TabIndex = 30
        Me.txtKillWellCalKWPumppressure.Text = "7.65"
        Me.txtKillWellCalKWPumppressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalKWDOP
        '
        Me.txtKillWellCalKWDOP.AcceptsReturn = True
        Me.txtKillWellCalKWDOP.AutoSize = False
        Me.txtKillWellCalKWDOP.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalKWDOP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalKWDOP.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalKWDOP.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCalKWDOP.Location = New System.Drawing.Point(293, 407)
        Me.txtKillWellCalKWDOP.MaxLength = 0
        Me.txtKillWellCalKWDOP.Name = "txtKillWellCalKWDOP"
        Me.txtKillWellCalKWDOP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalKWDOP.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalKWDOP.TabIndex = 29
        Me.txtKillWellCalKWDOP.Text = "15.78"
        Me.txtKillWellCalKWDOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalCSLMW
        '
        Me.txtKillWellCalCSLMW.AcceptsReturn = True
        Me.txtKillWellCalCSLMW.AutoSize = False
        Me.txtKillWellCalCSLMW.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalCSLMW.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalCSLMW.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalCSLMW.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCalCSLMW.Location = New System.Drawing.Point(293, 372)
        Me.txtKillWellCalCSLMW.MaxLength = 0
        Me.txtKillWellCalCSLMW.Name = "txtKillWellCalCSLMW"
        Me.txtKillWellCalCSLMW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalCSLMW.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalCSLMW.TabIndex = 28
        Me.txtKillWellCalCSLMW.Text = "2.40"
        Me.txtKillWellCalCSLMW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalCasingshoedepth
        '
        Me.txtKillWellCalCasingshoedepth.AcceptsReturn = True
        Me.txtKillWellCalCasingshoedepth.AutoSize = False
        Me.txtKillWellCalCasingshoedepth.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalCasingshoedepth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalCasingshoedepth.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalCasingshoedepth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCalCasingshoedepth.Location = New System.Drawing.Point(293, 337)
        Me.txtKillWellCalCasingshoedepth.MaxLength = 0
        Me.txtKillWellCalCasingshoedepth.Name = "txtKillWellCalCasingshoedepth"
        Me.txtKillWellCalCasingshoedepth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalCasingshoedepth.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalCasingshoedepth.TabIndex = 27
        Me.txtKillWellCalCasingshoedepth.Text = "1800"
        Me.txtKillWellCalCasingshoedepth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCasingpressure
        '
        Me.txtKillWellCasingpressure.AcceptsReturn = True
        Me.txtKillWellCasingpressure.AutoSize = False
        Me.txtKillWellCasingpressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCasingpressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCasingpressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCasingpressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellCasingpressure.Location = New System.Drawing.Point(293, 92)
        Me.txtKillWellCasingpressure.MaxLength = 0
        Me.txtKillWellCasingpressure.Name = "txtKillWellCasingpressure"
        Me.txtKillWellCasingpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCasingpressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCasingpressure.TabIndex = 15
        Me.txtKillWellCasingpressure.Text = "8.0"
        Me.txtKillWellCasingpressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellKickVol
        '
        Me.txtKillWellKickVol.AcceptsReturn = True
        Me.txtKillWellKickVol.AutoSize = False
        Me.txtKillWellKickVol.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellKickVol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellKickVol.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellKickVol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellKickVol.Location = New System.Drawing.Point(293, 127)
        Me.txtKillWellKickVol.MaxLength = 0
        Me.txtKillWellKickVol.Name = "txtKillWellKickVol"
        Me.txtKillWellKickVol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellKickVol.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellKickVol.TabIndex = 14
        Me.txtKillWellKickVol.Text = "11"
        Me.txtKillWellKickVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellBitdiameter
        '
        Me.txtKillWellBitdiameter.AcceptsReturn = True
        Me.txtKillWellBitdiameter.AutoSize = False
        Me.txtKillWellBitdiameter.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellBitdiameter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellBitdiameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellBitdiameter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellBitdiameter.Location = New System.Drawing.Point(293, 162)
        Me.txtKillWellBitdiameter.MaxLength = 0
        Me.txtKillWellBitdiameter.Name = "txtKillWellBitdiameter"
        Me.txtKillWellBitdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellBitdiameter.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellBitdiameter.TabIndex = 13
        Me.txtKillWellBitdiameter.Text = "215.9"
        Me.txtKillWellBitdiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWelldrillstemdiameter
        '
        Me.txtKillWelldrillstemdiameter.AcceptsReturn = True
        Me.txtKillWelldrillstemdiameter.AutoSize = False
        Me.txtKillWelldrillstemdiameter.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWelldrillstemdiameter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWelldrillstemdiameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWelldrillstemdiameter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWelldrillstemdiameter.Location = New System.Drawing.Point(293, 197)
        Me.txtKillWelldrillstemdiameter.MaxLength = 0
        Me.txtKillWelldrillstemdiameter.Name = "txtKillWelldrillstemdiameter"
        Me.txtKillWelldrillstemdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWelldrillstemdiameter.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWelldrillstemdiameter.TabIndex = 12
        Me.txtKillWelldrillstemdiameter.Text = "127"
        Me.txtKillWelldrillstemdiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellOpenHoleExpands
        '
        Me.txtKillWellOpenHoleExpands.AcceptsReturn = True
        Me.txtKillWellOpenHoleExpands.AutoSize = False
        Me.txtKillWellOpenHoleExpands.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellOpenHoleExpands.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellOpenHoleExpands.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellOpenHoleExpands.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellOpenHoleExpands.Location = New System.Drawing.Point(293, 267)
        Me.txtKillWellOpenHoleExpands.MaxLength = 0
        Me.txtKillWellOpenHoleExpands.Name = "txtKillWellOpenHoleExpands"
        Me.txtKillWellOpenHoleExpands.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellOpenHoleExpands.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellOpenHoleExpands.TabIndex = 11
        Me.txtKillWellOpenHoleExpands.Text = "1.05"
        Me.txtKillWellOpenHoleExpands.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellMudDensity
        '
        Me.txtKillWellMudDensity.AcceptsReturn = True
        Me.txtKillWellMudDensity.AutoSize = False
        Me.txtKillWellMudDensity.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellMudDensity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellMudDensity.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellMudDensity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellMudDensity.Location = New System.Drawing.Point(293, 302)
        Me.txtKillWellMudDensity.MaxLength = 0
        Me.txtKillWellMudDensity.Name = "txtKillWellMudDensity"
        Me.txtKillWellMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellMudDensity.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellMudDensity.TabIndex = 10
        Me.txtKillWellMudDensity.Text = "1.66"
        Me.txtKillWellMudDensity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellShutinpressure
        '
        Me.txtKillWellShutinpressure.AcceptsReturn = True
        Me.txtKillWellShutinpressure.AutoSize = False
        Me.txtKillWellShutinpressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellShutinpressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellShutinpressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellShutinpressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKillWellShutinpressure.Location = New System.Drawing.Point(293, 57)
        Me.txtKillWellShutinpressure.MaxLength = 0
        Me.txtKillWellShutinpressure.Name = "txtKillWellShutinpressure"
        Me.txtKillWellShutinpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellShutinpressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellShutinpressure.TabIndex = 9
        Me.txtKillWellShutinpressure.Text = "3.5"
        Me.txtKillWellShutinpressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblKillWellCalTotalDepth
        '
        Me.lblKillWellCalTotalDepth.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalTotalDepth.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalTotalDepth.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalTotalDepth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalTotalDepth.Location = New System.Drawing.Point(8, 22)
        Me.lblKillWellCalTotalDepth.Name = "lblKillWellCalTotalDepth"
        Me.lblKillWellCalTotalDepth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalTotalDepth.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCalTotalDepth.TabIndex = 33
        Me.lblKillWellCalTotalDepth.Text = "井深(TVD) (m)"
        Me.lblKillWellCalTotalDepth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWelldrillsteminsidediameter
        '
        Me.lblKillWelldrillsteminsidediameter.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWelldrillsteminsidediameter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWelldrillsteminsidediameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWelldrillsteminsidediameter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWelldrillsteminsidediameter.Location = New System.Drawing.Point(8, 232)
        Me.lblKillWelldrillsteminsidediameter.Name = "lblKillWelldrillsteminsidediameter"
        Me.lblKillWelldrillsteminsidediameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWelldrillsteminsidediameter.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWelldrillsteminsidediameter.TabIndex = 31
        Me.lblKillWelldrillsteminsidediameter.Text = "钻具内径 (mm)"
        Me.lblKillWelldrillsteminsidediameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellCalKWPumppressure
        '
        Me.lblKillWellCalKWPumppressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalKWPumppressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalKWPumppressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalKWPumppressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalKWPumppressure.Location = New System.Drawing.Point(8, 442)
        Me.lblKillWellCalKWPumppressure.Name = "lblKillWellCalKWPumppressure"
        Me.lblKillWellCalKWPumppressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalKWPumppressure.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCalKWPumppressure.TabIndex = 26
        Me.lblKillWellCalKWPumppressure.Text = "压井排量下的循环压力 (MPa)"
        Me.lblKillWellCalKWPumppressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.lblKillWellCalKWPumppressure, "日常低泵冲试验计量循环压力")
        '
        'lblKillWellCalKWDOP
        '
        Me.lblKillWellCalKWDOP.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalKWDOP.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalKWDOP.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalKWDOP.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalKWDOP.Location = New System.Drawing.Point(8, 407)
        Me.lblKillWellCalKWDOP.Name = "lblKillWellCalKWDOP"
        Me.lblKillWellCalKWDOP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalKWDOP.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCalKWDOP.TabIndex = 25
        Me.lblKillWellCalKWDOP.Text = "压井排量  (l/s)"
        Me.lblKillWellCalKWDOP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellCalCSLMW
        '
        Me.lblKillWellCalCSLMW.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalCSLMW.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalCSLMW.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalCSLMW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalCSLMW.Location = New System.Drawing.Point(8, 372)
        Me.lblKillWellCalCSLMW.Name = "lblKillWellCalCSLMW"
        Me.lblKillWellCalCSLMW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalCSLMW.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCalCSLMW.TabIndex = 24
        Me.lblKillWellCalCSLMW.Text = "套管鞋地层破裂压力当量密度 (g/cm^3)"
        Me.lblKillWellCalCSLMW.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellCalCasingshoedepth
        '
        Me.lblKillWellCalCasingshoedepth.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalCasingshoedepth.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalCasingshoedepth.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalCasingshoedepth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalCasingshoedepth.Location = New System.Drawing.Point(8, 337)
        Me.lblKillWellCalCasingshoedepth.Name = "lblKillWellCalCasingshoedepth"
        Me.lblKillWellCalCasingshoedepth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalCasingshoedepth.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCalCasingshoedepth.TabIndex = 23
        Me.lblKillWellCalCasingshoedepth.Text = "套管鞋下深 TVD (m)"
        Me.lblKillWellCalCasingshoedepth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellCasingpressure
        '
        Me.lblKillWellCasingpressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCasingpressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCasingpressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCasingpressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCasingpressure.Location = New System.Drawing.Point(8, 92)
        Me.lblKillWellCasingpressure.Name = "lblKillWellCasingpressure"
        Me.lblKillWellCasingpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCasingpressure.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellCasingpressure.TabIndex = 22
        Me.lblKillWellCasingpressure.Text = "关井套管压力SICP(MPa)"
        Me.lblKillWellCasingpressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellKickVol
        '
        Me.lblKillWellKickVol.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellKickVol.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellKickVol.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellKickVol.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellKickVol.Location = New System.Drawing.Point(8, 127)
        Me.lblKillWellKickVol.Name = "lblKillWellKickVol"
        Me.lblKillWellKickVol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellKickVol.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellKickVol.TabIndex = 21
        Me.lblKillWellKickVol.Text = "井底溢流体积 (m^3)"
        Me.lblKillWellKickVol.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellBitdiameter
        '
        Me.lblKillWellBitdiameter.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellBitdiameter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellBitdiameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellBitdiameter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellBitdiameter.Location = New System.Drawing.Point(8, 162)
        Me.lblKillWellBitdiameter.Name = "lblKillWellBitdiameter"
        Me.lblKillWellBitdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellBitdiameter.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellBitdiameter.TabIndex = 20
        Me.lblKillWellBitdiameter.Text = "钻头直径 (mm)"
        Me.lblKillWellBitdiameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWelldrillstemdiameter
        '
        Me.lblKillWelldrillstemdiameter.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWelldrillstemdiameter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWelldrillstemdiameter.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWelldrillstemdiameter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWelldrillstemdiameter.Location = New System.Drawing.Point(8, 197)
        Me.lblKillWelldrillstemdiameter.Name = "lblKillWelldrillstemdiameter"
        Me.lblKillWelldrillstemdiameter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWelldrillstemdiameter.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWelldrillstemdiameter.TabIndex = 19
        Me.lblKillWelldrillstemdiameter.Text = "钻具外径 (mm)"
        Me.lblKillWelldrillstemdiameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellOpenHoleExpands
        '
        Me.lblKillWellOpenHoleExpands.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellOpenHoleExpands.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellOpenHoleExpands.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellOpenHoleExpands.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellOpenHoleExpands.Location = New System.Drawing.Point(8, 267)
        Me.lblKillWellOpenHoleExpands.Name = "lblKillWellOpenHoleExpands"
        Me.lblKillWellOpenHoleExpands.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellOpenHoleExpands.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellOpenHoleExpands.TabIndex = 18
        Me.lblKillWellOpenHoleExpands.Text = "裸眼井径扩大率"
        Me.lblKillWellOpenHoleExpands.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellMudDensity
        '
        Me.lblKillWellMudDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellMudDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellMudDensity.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellMudDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellMudDensity.Location = New System.Drawing.Point(8, 302)
        Me.lblKillWellMudDensity.Name = "lblKillWellMudDensity"
        Me.lblKillWellMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellMudDensity.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellMudDensity.TabIndex = 17
        Me.lblKillWellMudDensity.Text = "原钻井液密度 (g/cm^3)"
        Me.lblKillWellMudDensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblKillWellShutinpressure
        '
        Me.lblKillWellShutinpressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellShutinpressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellShutinpressure.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellShutinpressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellShutinpressure.Location = New System.Drawing.Point(8, 57)
        Me.lblKillWellShutinpressure.Name = "lblKillWellShutinpressure"
        Me.lblKillWellShutinpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellShutinpressure.Size = New System.Drawing.Size(261, 20)
        Me.lblKillWellShutinpressure.TabIndex = 16
        Me.lblKillWellShutinpressure.Text = "关井立管压力SIDPP(MPa)"
        Me.lblKillWellShutinpressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraKillWellProgram
        '
        Me.fraKillWellProgram.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillWellProgram.Controls.Add(Me.lblKillWellProgram)
        Me.fraKillWellProgram.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraKillWellProgram.Location = New System.Drawing.Point(8, 8)
        Me.fraKillWellProgram.Name = "fraKillWellProgram"
        Me.fraKillWellProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillWellProgram.Size = New System.Drawing.Size(424, 83)
        Me.fraKillWellProgram.TabIndex = 6
        Me.fraKillWellProgram.TabStop = False
        Me.fraKillWellProgram.Text = "程序帮助"
        '
        'lblKillWellProgram
        '
        Me.lblKillWellProgram.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellProgram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblKillWellProgram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellProgram.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellProgram.ForeColor = System.Drawing.Color.Blue
        Me.lblKillWellProgram.Location = New System.Drawing.Point(10, 17)
        Me.lblKillWellProgram.Name = "lblKillWellProgram"
        Me.lblKillWellProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellProgram.Size = New System.Drawing.Size(403, 56)
        Me.lblKillWellProgram.TabIndex = 7
        Me.lblKillWellProgram.Text = "    本程序用来计算现场施工中，压井前所需要计算的一些基本数据，以供压井作业的需要。为了保证计算的准确性，请大家正确采集输入数据。"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(510, 455)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 5
        Me.cmdok.Text = "Run"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(510, 482)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 4
        Me.cmdclear.Text = "Clean"
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(510, 510)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 3
        Me.cmdExit.Text = "Exit"
        '
        'fraKillingWellOutput
        '
        Me.fraKillingWellOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalTimeInsidePipe)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalTimeAnnulus)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalEndCirculatingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalStarCirculatingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalKTWMaxCasingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalMaxShutInCasingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalDrillingMudDensity)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalStratumPressureMudDensity)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillWellCalStratumPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.txtKillingWellstyle)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalTimeInsidePipe)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalTimeAnnulus)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalEndCirculatingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalStarCirculatingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalKTWMaxCasingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalMaxShutInCasingPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalDrillingMudDensity)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalStratumPressureMudDensity)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillWellCalStratumPressure)
        Me.fraKillingWellOutput.Controls.Add(Me.lblKillingWellStyleDensity)
        Me.fraKillingWellOutput.ForeColor = System.Drawing.Color.Red
        Me.fraKillingWellOutput.Location = New System.Drawing.Point(440, 8)
        Me.fraKillingWellOutput.Name = "fraKillingWellOutput"
        Me.fraKillingWellOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraKillingWellOutput.Size = New System.Drawing.Size(448, 416)
        Me.fraKillingWellOutput.TabIndex = 0
        Me.fraKillingWellOutput.TabStop = False
        Me.fraKillingWellOutput.Text = "结果"
        '
        'txtKillWellCalTimeInsidePipe
        '
        Me.txtKillWellCalTimeInsidePipe.AcceptsReturn = True
        Me.txtKillWellCalTimeInsidePipe.AutoSize = False
        Me.txtKillWellCalTimeInsidePipe.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalTimeInsidePipe.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalTimeInsidePipe.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalTimeInsidePipe.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalTimeInsidePipe.Location = New System.Drawing.Point(304, 384)
        Me.txtKillWellCalTimeInsidePipe.MaxLength = 0
        Me.txtKillWellCalTimeInsidePipe.Name = "txtKillWellCalTimeInsidePipe"
        Me.txtKillWellCalTimeInsidePipe.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalTimeInsidePipe.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalTimeInsidePipe.TabIndex = 52
        Me.txtKillWellCalTimeInsidePipe.Text = ""
        Me.txtKillWellCalTimeInsidePipe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalTimeAnnulus
        '
        Me.txtKillWellCalTimeAnnulus.AcceptsReturn = True
        Me.txtKillWellCalTimeAnnulus.AutoSize = False
        Me.txtKillWellCalTimeAnnulus.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalTimeAnnulus.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalTimeAnnulus.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalTimeAnnulus.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalTimeAnnulus.Location = New System.Drawing.Point(304, 344)
        Me.txtKillWellCalTimeAnnulus.MaxLength = 0
        Me.txtKillWellCalTimeAnnulus.Name = "txtKillWellCalTimeAnnulus"
        Me.txtKillWellCalTimeAnnulus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalTimeAnnulus.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalTimeAnnulus.TabIndex = 51
        Me.txtKillWellCalTimeAnnulus.Text = ""
        Me.txtKillWellCalTimeAnnulus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalEndCirculatingPressure
        '
        Me.txtKillWellCalEndCirculatingPressure.AcceptsReturn = True
        Me.txtKillWellCalEndCirculatingPressure.AutoSize = False
        Me.txtKillWellCalEndCirculatingPressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalEndCirculatingPressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalEndCirculatingPressure.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalEndCirculatingPressure.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalEndCirculatingPressure.Location = New System.Drawing.Point(304, 304)
        Me.txtKillWellCalEndCirculatingPressure.MaxLength = 0
        Me.txtKillWellCalEndCirculatingPressure.Name = "txtKillWellCalEndCirculatingPressure"
        Me.txtKillWellCalEndCirculatingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalEndCirculatingPressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalEndCirculatingPressure.TabIndex = 50
        Me.txtKillWellCalEndCirculatingPressure.Text = ""
        Me.txtKillWellCalEndCirculatingPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalStarCirculatingPressure
        '
        Me.txtKillWellCalStarCirculatingPressure.AcceptsReturn = True
        Me.txtKillWellCalStarCirculatingPressure.AutoSize = False
        Me.txtKillWellCalStarCirculatingPressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalStarCirculatingPressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalStarCirculatingPressure.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalStarCirculatingPressure.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalStarCirculatingPressure.Location = New System.Drawing.Point(304, 264)
        Me.txtKillWellCalStarCirculatingPressure.MaxLength = 0
        Me.txtKillWellCalStarCirculatingPressure.Name = "txtKillWellCalStarCirculatingPressure"
        Me.txtKillWellCalStarCirculatingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalStarCirculatingPressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalStarCirculatingPressure.TabIndex = 49
        Me.txtKillWellCalStarCirculatingPressure.Text = ""
        Me.txtKillWellCalStarCirculatingPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalKTWMaxCasingPressure
        '
        Me.txtKillWellCalKTWMaxCasingPressure.AcceptsReturn = True
        Me.txtKillWellCalKTWMaxCasingPressure.AutoSize = False
        Me.txtKillWellCalKTWMaxCasingPressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalKTWMaxCasingPressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalKTWMaxCasingPressure.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalKTWMaxCasingPressure.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalKTWMaxCasingPressure.Location = New System.Drawing.Point(304, 224)
        Me.txtKillWellCalKTWMaxCasingPressure.MaxLength = 0
        Me.txtKillWellCalKTWMaxCasingPressure.Name = "txtKillWellCalKTWMaxCasingPressure"
        Me.txtKillWellCalKTWMaxCasingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalKTWMaxCasingPressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalKTWMaxCasingPressure.TabIndex = 48
        Me.txtKillWellCalKTWMaxCasingPressure.Text = ""
        Me.txtKillWellCalKTWMaxCasingPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalMaxShutInCasingPressure
        '
        Me.txtKillWellCalMaxShutInCasingPressure.AcceptsReturn = True
        Me.txtKillWellCalMaxShutInCasingPressure.AutoSize = False
        Me.txtKillWellCalMaxShutInCasingPressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalMaxShutInCasingPressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalMaxShutInCasingPressure.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalMaxShutInCasingPressure.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalMaxShutInCasingPressure.Location = New System.Drawing.Point(304, 184)
        Me.txtKillWellCalMaxShutInCasingPressure.MaxLength = 0
        Me.txtKillWellCalMaxShutInCasingPressure.Name = "txtKillWellCalMaxShutInCasingPressure"
        Me.txtKillWellCalMaxShutInCasingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalMaxShutInCasingPressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalMaxShutInCasingPressure.TabIndex = 47
        Me.txtKillWellCalMaxShutInCasingPressure.Text = ""
        Me.txtKillWellCalMaxShutInCasingPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalDrillingMudDensity
        '
        Me.txtKillWellCalDrillingMudDensity.AcceptsReturn = True
        Me.txtKillWellCalDrillingMudDensity.AutoSize = False
        Me.txtKillWellCalDrillingMudDensity.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalDrillingMudDensity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalDrillingMudDensity.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalDrillingMudDensity.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalDrillingMudDensity.Location = New System.Drawing.Point(304, 144)
        Me.txtKillWellCalDrillingMudDensity.MaxLength = 0
        Me.txtKillWellCalDrillingMudDensity.Name = "txtKillWellCalDrillingMudDensity"
        Me.txtKillWellCalDrillingMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalDrillingMudDensity.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalDrillingMudDensity.TabIndex = 46
        Me.txtKillWellCalDrillingMudDensity.Text = ""
        Me.txtKillWellCalDrillingMudDensity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalStratumPressureMudDensity
        '
        Me.txtKillWellCalStratumPressureMudDensity.AcceptsReturn = True
        Me.txtKillWellCalStratumPressureMudDensity.AutoSize = False
        Me.txtKillWellCalStratumPressureMudDensity.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalStratumPressureMudDensity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalStratumPressureMudDensity.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalStratumPressureMudDensity.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalStratumPressureMudDensity.Location = New System.Drawing.Point(304, 104)
        Me.txtKillWellCalStratumPressureMudDensity.MaxLength = 0
        Me.txtKillWellCalStratumPressureMudDensity.Name = "txtKillWellCalStratumPressureMudDensity"
        Me.txtKillWellCalStratumPressureMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalStratumPressureMudDensity.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalStratumPressureMudDensity.TabIndex = 45
        Me.txtKillWellCalStratumPressureMudDensity.Text = ""
        Me.txtKillWellCalStratumPressureMudDensity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKillWellCalStratumPressure
        '
        Me.txtKillWellCalStratumPressure.AcceptsReturn = True
        Me.txtKillWellCalStratumPressure.AutoSize = False
        Me.txtKillWellCalStratumPressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtKillWellCalStratumPressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKillWellCalStratumPressure.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtKillWellCalStratumPressure.ForeColor = System.Drawing.Color.Red
        Me.txtKillWellCalStratumPressure.Location = New System.Drawing.Point(304, 64)
        Me.txtKillWellCalStratumPressure.MaxLength = 0
        Me.txtKillWellCalStratumPressure.Name = "txtKillWellCalStratumPressure"
        Me.txtKillWellCalStratumPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKillWellCalStratumPressure.Size = New System.Drawing.Size(80, 20)
        Me.txtKillWellCalStratumPressure.TabIndex = 44
        Me.txtKillWellCalStratumPressure.Text = ""
        Me.txtKillWellCalStratumPressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblKillWellCalTimeInsidePipe
        '
        Me.lblKillWellCalTimeInsidePipe.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalTimeInsidePipe.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalTimeInsidePipe.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalTimeInsidePipe.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalTimeInsidePipe.Location = New System.Drawing.Point(8, 384)
        Me.lblKillWellCalTimeInsidePipe.Name = "lblKillWellCalTimeInsidePipe"
        Me.lblKillWellCalTimeInsidePipe.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalTimeInsidePipe.Size = New System.Drawing.Size(288, 20)
        Me.lblKillWellCalTimeInsidePipe.TabIndex = 43
        Me.lblKillWellCalTimeInsidePipe.Text = "压井钻井液从地面到达钻头所需时间 (min)"
        '
        'lblKillWellCalTimeAnnulus
        '
        Me.lblKillWellCalTimeAnnulus.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalTimeAnnulus.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalTimeAnnulus.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalTimeAnnulus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalTimeAnnulus.Location = New System.Drawing.Point(8, 344)
        Me.lblKillWellCalTimeAnnulus.Name = "lblKillWellCalTimeAnnulus"
        Me.lblKillWellCalTimeAnnulus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalTimeAnnulus.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalTimeAnnulus.TabIndex = 42
        Me.lblKillWellCalTimeAnnulus.Text = "压井钻井液充满环空所需时间 (min)"
        '
        'lblKillWellCalEndCirculatingPressure
        '
        Me.lblKillWellCalEndCirculatingPressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalEndCirculatingPressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalEndCirculatingPressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalEndCirculatingPressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalEndCirculatingPressure.Location = New System.Drawing.Point(8, 304)
        Me.lblKillWellCalEndCirculatingPressure.Name = "lblKillWellCalEndCirculatingPressure"
        Me.lblKillWellCalEndCirculatingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalEndCirculatingPressure.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalEndCirculatingPressure.TabIndex = 41
        Me.lblKillWellCalEndCirculatingPressure.Text = "终了循环时立管压力 (MPa)"
        '
        'lblKillWellCalStarCirculatingPressure
        '
        Me.lblKillWellCalStarCirculatingPressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalStarCirculatingPressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalStarCirculatingPressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalStarCirculatingPressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalStarCirculatingPressure.Location = New System.Drawing.Point(8, 264)
        Me.lblKillWellCalStarCirculatingPressure.Name = "lblKillWellCalStarCirculatingPressure"
        Me.lblKillWellCalStarCirculatingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalStarCirculatingPressure.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalStarCirculatingPressure.TabIndex = 40
        Me.lblKillWellCalStarCirculatingPressure.Text = "初始循环时立管压力 (MPa)"
        '
        'lblKillWellCalKTWMaxCasingPressure
        '
        Me.lblKillWellCalKTWMaxCasingPressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalKTWMaxCasingPressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalKTWMaxCasingPressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalKTWMaxCasingPressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalKTWMaxCasingPressure.Location = New System.Drawing.Point(8, 224)
        Me.lblKillWellCalKTWMaxCasingPressure.Name = "lblKillWellCalKTWMaxCasingPressure"
        Me.lblKillWellCalKTWMaxCasingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalKTWMaxCasingPressure.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalKTWMaxCasingPressure.TabIndex = 39
        Me.lblKillWellCalKTWMaxCasingPressure.Text = "压井最大允许套管压力 (MPa)"
        '
        'lblKillWellCalMaxShutInCasingPressure
        '
        Me.lblKillWellCalMaxShutInCasingPressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalMaxShutInCasingPressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalMaxShutInCasingPressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalMaxShutInCasingPressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalMaxShutInCasingPressure.Location = New System.Drawing.Point(8, 184)
        Me.lblKillWellCalMaxShutInCasingPressure.Name = "lblKillWellCalMaxShutInCasingPressure"
        Me.lblKillWellCalMaxShutInCasingPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalMaxShutInCasingPressure.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalMaxShutInCasingPressure.TabIndex = 38
        Me.lblKillWellCalMaxShutInCasingPressure.Text = "最大允许关井套管压力SICP (MPa)"
        '
        'lblKillWellCalDrillingMudDensity
        '
        Me.lblKillWellCalDrillingMudDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalDrillingMudDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalDrillingMudDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalDrillingMudDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalDrillingMudDensity.Location = New System.Drawing.Point(8, 144)
        Me.lblKillWellCalDrillingMudDensity.Name = "lblKillWellCalDrillingMudDensity"
        Me.lblKillWellCalDrillingMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalDrillingMudDensity.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalDrillingMudDensity.TabIndex = 37
        Me.lblKillWellCalDrillingMudDensity.Text = "压井和钻井是钻井液密度(g/cm^3)"
        '
        'lblKillWellCalStratumPressureMudDensity
        '
        Me.lblKillWellCalStratumPressureMudDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalStratumPressureMudDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalStratumPressureMudDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalStratumPressureMudDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalStratumPressureMudDensity.Location = New System.Drawing.Point(8, 104)
        Me.lblKillWellCalStratumPressureMudDensity.Name = "lblKillWellCalStratumPressureMudDensity"
        Me.lblKillWellCalStratumPressureMudDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalStratumPressureMudDensity.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalStratumPressureMudDensity.TabIndex = 36
        Me.lblKillWellCalStratumPressureMudDensity.Text = "地层流体压力当量钻井液密度 (g/cm^3)"
        '
        'lblKillWellCalStratumPressure
        '
        Me.lblKillWellCalStratumPressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillWellCalStratumPressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillWellCalStratumPressure.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillWellCalStratumPressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillWellCalStratumPressure.Location = New System.Drawing.Point(8, 64)
        Me.lblKillWellCalStratumPressure.Name = "lblKillWellCalStratumPressure"
        Me.lblKillWellCalStratumPressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillWellCalStratumPressure.Size = New System.Drawing.Size(265, 20)
        Me.lblKillWellCalStratumPressure.TabIndex = 35
        Me.lblKillWellCalStratumPressure.Text = "地层流体压力 (MPa)"
        '
        'lblKillingWellStyleDensity
        '
        Me.lblKillingWellStyleDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblKillingWellStyleDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKillingWellStyleDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblKillingWellStyleDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKillingWellStyleDensity.Location = New System.Drawing.Point(8, 24)
        Me.lblKillingWellStyleDensity.Name = "lblKillingWellStyleDensity"
        Me.lblKillingWellStyleDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKillingWellStyleDensity.Size = New System.Drawing.Size(265, 20)
        Me.lblKillingWellStyleDensity.TabIndex = 2
        Me.lblKillingWellStyleDensity.Text = "溢流流体密度 (g/cm^3)"
        '
        'frmKillWellbasecal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.fraKillWellInputdata)
        Me.Controls.Add(Me.fraKillWellProgram)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.fraKillingWellOutput)
        Me.Controls.Add(Me.PicLogo)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmKillWellbasecal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "压井基础计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraKillWellInputdata.ResumeLayout(False)
        Me.fraKillWellProgram.ResumeLayout(False)
        Me.fraKillingWellOutput.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmKillWellbasecal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmKillWellbasecal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmKillWellbasecal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'压井基本数据计算，2006年10月29日1：00青海省涩北台东2井。
	
	
	Const pi As Double = 3.1415927 '数学计算所需圆周率3.14159265358973846
	Const g As Double = 0.0098 '计算所需的物理常量:重力加速度g=0.0098
	
	
	Dim WellDepth As Single '井深
	Dim drillpipeP As Single '关井立管压力
	Dim casingP As Single '关井套管压力
	Dim KickVol As Single '溢流体积
	Dim BitDiameter As Single '钻头外径
	Dim DrillstemDiameter As Single '钻具外径
	Dim drillstemInsideDiameter As Single '钻具内径
	Dim OpenHoleExpands As Single '井径扩大率
	Dim MudDensity As Single '井涌时钻井液密度
	Dim CasingShoeDepth As Single '套管鞋深度
	Dim CasingShoeLostMudDensity As Single '套管鞋破裂当量钻井液密度
	Dim KWDOP As Single '压井排量
	Dim KWPumppressure As Single '压井排量循环压力
	'-----------------------------------------------------------------
	'模块计算结果输入变量定义
	Dim KickStyle As Single '溢流类型
	Dim StratumPressure As Single '地层压力
	Dim StratumPressureMudDensity As Single '地层压力当量钻井液密度
	Dim DrillingMudDensity As Single '钻井钻井液密度＝地层压力当量钻井液密度＋附加钻井液密度
	Dim MaxShutInCasingPressure As Single '最大关井套管压力
	Dim KTWMaxCasingPressure As Single '最大压井套管压力
	Dim StarCirculatingPressure As Single '初始循环压力
	Dim EndCirculatingPressure As Single '终了循环压力
	Dim TimeAnnulus As Single '压井钻井液充满环空所需时间
	Dim TimeInsidePipe As Single '压井钻井液从地面到达钻头所需时间
	'-----------------------------------------------------------------
	'计算中定义的中间变量
	Dim VolOfAnnulus As Single '环空钻井液体积单位长度
	Dim VolInsidePipe As Single '钻具内容积单位长度
	Dim OpenHoleDiameter As Single '扩大井径
	Dim saftyDensity As Single '确定附加的安全钻井液密度值
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		WellDepth = Val(txtKillWellCalTotalDepth.Text)
		BitDiameter = Val(txtKillWellBitdiameter.Text)
		casingP = Val(txtKillWellCasingpressure.Text)
		DrillstemDiameter = Val(txtKillWelldrillstemdiameter.Text)
		drillstemInsideDiameter = Val(txtKillWellCaldrillsteminsidediameter.Text)
		KickVol = Val(txtKillWellKickVol.Text)
		MudDensity = Val(txtKillWellMudDensity.Text)
		OpenHoleExpands = Val(txtKillWellOpenHoleExpands.Text)
		CasingShoeDepth = Val(txtKillWellCalCasingshoedepth.Text)
		CasingShoeLostMudDensity = Val(txtKillWellCalCSLMW.Text)
		KWDOP = Val(txtKillWellCalKWDOP.Text)
		KWPumppressure = Val(txtKillWellCalKWPumppressure.Text)
		
		drillpipeP = Val(txtKillWellShutinpressure.Text)
		If txtKillWellShutinpressure.Text = "" Or drillpipeP < 0 Then
			MsgBox("Please check that you have entered 'SIDPP'data!", MsgBoxStyle.Information, "Overflow type of fluid")
			Exit Sub '纠错功能解决。2006年10月24日王朝
		End If
		
		casingP = Val(txtKillWellCasingpressure.Text)
		If txtKillWellCasingpressure.Text = "" Or casingP < 0 Then
			MsgBox("Please check that you have entered 'SICP'data!", MsgBoxStyle.Information, "Overflow type of fluid")
			Exit Sub '纠错功能解决。2006年10月24日王朝
		End If
		
		'11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
		OpenHoleDiameter = OpenHoleExpands * BitDiameter
		VolOfAnnulus = pi / 40 * ((OpenHoleDiameter / 10) ^ 2 - (DrillstemDiameter / 10) ^ 2)
		KickStyle = MudDensity - (casingP - drillpipeP) / (g * KickVol * 1000 / VolOfAnnulus)
		
		txtKillingWellstyle.Text = VB6.Format(KickStyle, "0.0000")
		
		
		'溢流流体的种类判别和确定钻井液附加值数据
		'0.12～0.36g/cm3  为天然气溢流；
		'0.37～0.60g/cm3  为油；
		'0.61～0.84g/cm3  为油水混合；
		'0.85～1.08g/cm3  为水。
		If KickStyle >= 0.12 And KickStyle <= 0.36 Then
			MsgBox("Overflow type of fluid for gas ! Killing additional drilling fluid density :0.07--0.15g/cm^3.", MsgBoxStyle.Information, "Overflow type of fluid")
			saftyDensity = 0.07
			
		Else
			If KickStyle >= 0.37 And KickStyle <= 0.6 Then
				MsgBox("Overflow type of fluid for petroleum ! Killing additional drilling fluid density :0.05--0.10g/cm^3.", MsgBoxStyle.Information, "Overflow type of fluid")
				saftyDensity = 0.05
				
			Else
				If KickStyle >= 0.61 And KickStyle <= 0.84 Then
					MsgBox("Overflow type for oil-water mixed !Killing additional drilling fluid density :0.05--0.10g/cm^3.", MsgBoxStyle.Information, "Overflow type of fluid")
					saftyDensity = 0.05
					
				Else
					If KickStyle >= 0.85 And KickStyle <= 1.08 Then
						MsgBox("Overflow type for water!Killing additional drilling fluid density :0.05--0.10g/cm^3.", MsgBoxStyle.Information, "Overflow type of fluid")
						saftyDensity = 0.05
					Else
						If KickStyle <= 0 Then
							MsgBox("可能由于程序未考虑钻铤数据的原因，也可能你采集的溢流量、关井立管压力和关井套压数据的错误，溢流流体的类型判断出现错误！但并不影响压井计算，程序假设附加钻井液密度为:0.05g/cm^3。", MsgBoxStyle.Information, "Overflow type of fluid")
							saftyDensity = 0.05
						End If
					End If
				End If
			End If
		End If
		
		'22222222222222222222222222222222222222222222222222222222222222222222222
		'Stratum Pressure地层压力计算
		StratumPressure = g * WellDepth * MudDensity + drillpipeP
		txtKillWellCalStratumPressure.Text = VB6.Format(StratumPressure, "0.00")
		
		'3333333333333333333333333333333333333333333333333333333333333333333333
		'地层压力当量钻井液密度
		StratumPressureMudDensity = StratumPressure / (g * WellDepth)
		txtKillWellCalStratumPressureMudDensity.Text = VB6.Format(StratumPressureMudDensity, "0.000")
		
		'4444444444444444444444444444444444444444444444444444444444444444444444
		'压井或钻井钻井液密度计算
		DrillingMudDensity = StratumPressureMudDensity + saftyDensity
		txtKillWellCalDrillingMudDensity.Text = VB6.Format(DrillingMudDensity, "0.000")
		
		'55555555555555555555555555555555555555555555555555555555555555555555555
		'确定关井最大套管压力计算
		MaxShutInCasingPressure = g * CasingShoeDepth * (CasingShoeLostMudDensity - MudDensity)
		txtKillWellCalMaxShutInCasingPressure.Text = VB6.Format(MaxShutInCasingPressure, "0.00")
		
		'66666666666666666666666666666666666666666666666666666666666666666666666
		'确定压井过程中最大套管压力
		KTWMaxCasingPressure = g * CasingShoeDepth * (CasingShoeLostMudDensity - DrillingMudDensity)
		If KTWMaxCasingPressure <= 0 Then
			'MsgBox "你选用的压井钻井液密度将导致套管鞋井漏，请在满足压井的条件下减少压井钻井液附加安全密度值。", vbInformation, "Kill Base Calculations"
			MsgBox("Your choice of killing M.W will leak off at casing shoe.Please reduce the kill of mud additional security density .", MsgBoxStyle.Information, "Kill Base Calculations")
			txtKillWellCalKTWMaxCasingPressure.Text = ""
			txtKillWellCalStarCirculatingPressure.Text = ""
			txtKillWellCalEndCirculatingPressure.Text = ""
			txtKillWellCalTimeAnnulus.Text = ""
			txtKillWellCalTimeInsidePipe.Text = ""
			Exit Sub
		End If
		
		txtKillWellCalKTWMaxCasingPressure.Text = VB6.Format(KTWMaxCasingPressure, "0.00")
		
		'77777777777777777777777777777777777777777777777777777777777777777777777777
		'计算压井钻井液初始循环立管压力
		StarCirculatingPressure = drillpipeP + KWPumppressure
		txtKillWellCalStarCirculatingPressure.Text = VB6.Format(StarCirculatingPressure, "0.0")
		
		'88888888888888888888888888888888888888888888888888888888888888888888888
		'计算压井钻井液终了循环立管压力
		EndCirculatingPressure = (DrillingMudDensity / MudDensity) * KWPumppressure
		txtKillWellCalEndCirculatingPressure.Text = VB6.Format(EndCirculatingPressure, "0.0")
		
		'999999999999999999999999999999999999999999999999999999999999999999999999
		'计算压井钻井液充满环空和从地面到达钻头的时间
		TimeAnnulus = ((VolOfAnnulus * WellDepth) / KWDOP) / 60
		VolInsidePipe = pi / 40 * (drillstemInsideDiameter / 10) ^ 2
		TimeInsidePipe = ((VolInsidePipe * WellDepth) / KWDOP) / 60
		
		txtKillWellCalTimeAnnulus.Text = VB6.Format(TimeAnnulus, "0")
		txtKillWellCalTimeInsidePipe.Text = VB6.Format(TimeInsidePipe, "0")
		
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		txtKillWellCalTotalDepth.Text = ""
		txtKillWellShutinpressure.Text = ""
		txtKillWellCasingpressure.Text = ""
		txtKillWellKickVol.Text = ""
		txtKillWellBitdiameter.Text = ""
		txtKillWelldrillstemdiameter.Text = ""
		txtKillWellCaldrillsteminsidediameter.Text = ""
		txtKillWellOpenHoleExpands.Text = ""
		txtKillWellMudDensity.Text = ""
		txtKillWellCalCasingshoedepth.Text = ""
		txtKillWellCalCSLMW.Text = ""
		txtKillWellCalKWDOP.Text = ""
		txtKillWellCalKWPumppressure.Text = ""
		
		txtKillingWellstyle.Text = ""
		txtKillWellCalStratumPressure.Text = ""
		txtKillWellCalStratumPressureMudDensity.Text = ""
		txtKillWellCalDrillingMudDensity.Text = ""
		txtKillWellCalMaxShutInCasingPressure.Text = ""
		txtKillWellCalKTWMaxCasingPressure.Text = ""
		txtKillWellCalStarCirculatingPressure.Text = ""
		txtKillWellCalEndCirculatingPressure.Text = ""
		txtKillWellCalTimeAnnulus.Text = ""
		txtKillWellCalTimeInsidePipe.Text = ""
		saftyDensity = 0
	End Sub
	
	Private Sub frmKillWellbasecal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

		'************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdclear.Visible = False '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MKillWell.bmp")


        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MKillWell.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellBitdiameter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellBitdiameter.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellBitdiameter_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellBitdiameter.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'bit's diameter
		BitDiameter = Val(txtKillWellBitdiameter.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCalCasingshoedepth_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCalCasingshoedepth.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCalCasingshoedepth_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCalCasingshoedepth.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'Casing shoe depth
		CasingShoeDepth = Val(txtKillWellCalCasingshoedepth.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCalCSLMW_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCalCSLMW.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCalCSLMW_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCalCSLMW.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'casing shoe lost mud weight(density).
		CasingShoeLostMudDensity = Val(txtKillWellCalCSLMW.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCaldrillsteminsidediameter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCaldrillsteminsidediameter.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCaldrillsteminsidediameter_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCaldrillsteminsidediameter.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 ' drill pipe inside diameter data.
		drillstemInsideDiameter = Val(txtKillWellCaldrillsteminsidediameter.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCalKWDOP_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCalKWDOP.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCalKWDOP_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCalKWDOP.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'kill the well  ,discharge  of  pump
		KWDOP = Val(txtKillWellCalKWDOP.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCalKWPumppressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCalKWPumppressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCalKWPumppressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCalKWPumppressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'kill the well ,pump pressure
		KWPumppressure = Val(txtKillWellCalKWPumppressure.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCalTotalDepth_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCalTotalDepth.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCalTotalDepth_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCalTotalDepth.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'Well 's total depth.
		WellDepth = Val(txtKillWellCalTotalDepth.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellCasingpressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellCasingpressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellCasingpressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellCasingpressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'After kick control,casing pressure
		casingP = Val(txtKillWellCasingpressure.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWelldrillstemdiameter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWelldrillstemdiameter.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWelldrillstemdiameter_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWelldrillstemdiameter.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'drill stem's diameter
		DrillstemDiameter = Val(txtKillWelldrillstemdiameter.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellKickVol_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellKickVol.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellKickVol_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellKickVol.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'kick volume
		KickVol = Val(txtKillWellKickVol.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellMudDensity_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellMudDensity.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellMudDensity_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellMudDensity.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'Before kick, use mud density data.
		MudDensity = Val(txtKillWellMudDensity.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellOpenHoleExpands_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellOpenHoleExpands.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellOpenHoleExpands_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellOpenHoleExpands.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 ' After drilling hole,hole expands percent data.
		OpenHoleExpands = Val(txtKillWellOpenHoleExpands.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtKillWellShutinpressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtKillWellShutinpressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtKillWellShutinpressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKillWellShutinpressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 'After Shut in well,drill pipe  inside  pressure data.
		drillpipeP = Val(txtKillWellShutinpressure.Text)
	End Sub


    Private Sub lblKillWellShutinpressure_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblKillWellShutinpressure.Click

    End Sub
End Class