Option Strict Off
Option Explicit On
Module modFrmSplash
	Dim WinVersion As Short
	
	
	Structure MYVERSION
		Dim lMajorVersion As Integer
		Dim lMinorVersion As Integer
		Dim lExtraInfo As Integer
		Dim lBuildNumber As Integer
	End Structure
	
	Structure OSVERSIONINFO
		Dim dwOSVersionInfoSize As Integer
		Dim dwMajorVersion As Integer
		Dim dwMinorVersion As Integer
		Dim dwBuildNumber As Integer
		Dim dwPlatformId As Integer
		<VBFixedString(128),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=128)> Public szCSDVersion As String '  为了 PSS 的用途而维护串
	End Structure
	
	Public Structure SystemInfo
		Dim dwOemId As Integer
		Dim dwPageSize As Integer
		Dim lpMinimumApplicationAddress As Integer
		Dim lpMaximumApplicationAddress As Integer
		Dim dwActiveProcessorMask As Integer
		Dim dwNumberOfProcessors As Integer
		Dim dwProcessorType As Integer
		Dim dwAllocationGranularity As Integer
		Dim dwReserved As Integer
	End Structure
	
	Declare Function GetWindowsDirectory Lib "kernel32"  Alias "GetWindowsDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	'UPGRADE_WARNING: 结构 SystemInfo 可能要求封送处理属性作为此声明语句中的参数传递。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"”
	Declare Sub GetSystemInfo Lib "kernel32" (ByRef lpSystemInfo As SystemInfo)
	'UPGRADE_WARNING: 结构 OSVERSIONINFO 可能要求封送处理属性作为此声明语句中的参数传递。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"”
	Declare Function GetVersionEx Lib "kernel32"  Alias "GetVersionExA"(ByRef lpVersionInformation As OSVERSIONINFO) As Integer
	Declare Function GetSystemDirectory Lib "kernel32"  Alias "GetSystemDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	Declare Function GetUserName Lib "advapi32.dll"  Alias "GetUserNameA"(ByVal lpBuffer As String, ByRef nSize As Integer) As Integer
	Declare Function GetComputerName Lib "kernel32"  Alias "GetComputerNameA"(ByVal lpBuffer As String, ByRef nSize As Integer) As Integer
	
	Public Const VER_PLATFORM_WIN32s As Short = 0
	Public Const VER_PLATFORM_WIN32_WINDOWS As Short = 1
	Public Const VER_PLATFORM_WIN32_NT As Short = 2
	
	Public Const WF_CPU286 As Integer = &H2
	Public Const WF_CPU386 As Integer = &H4
	Public Const WF_CPU486 As Integer = &H8
	Public Const WF_STANDARD As Integer = &H10
	Public Const WF_ENHANCED As Integer = &H20
	Public Const WF_80x87 As Integer = &H400
	
	Function GetUserNameWC() As String
		Dim UserName As String
		Dim size As Integer
		UserName = New String(Chr(0), 64)
		size = 64
		GetUserNameWC = CStr(GetUserName(UserName, size))
	End Function
	
	Function GetComputerNameWC() As String
		Dim ComputerName As String
		Dim size As Integer
		Dim rc As Short
		ComputerName = New String(Chr(0), 32)
		size = 32
		GetComputerNameWC = CStr(GetComputerName(ComputerName, size))
	End Function
	
	Function SystemDirectory() As String '该函数目前作为学习保留
		Dim WinPath As String
		WinPath = New String(Chr(0), 145)
		SystemDirectory = Left(WinPath, GetSystemDirectory(WinPath, 145))
	End Function
	
	Function WindowsDirectory() As String '该函数目前作为学习保留
		Dim WinPath As String
		WinPath = New String(Chr(0), 145)
		WindowsDirectory = Left(WinPath, GetWindowsDirectory(WinPath, 145))
	End Function
	
	
	Function WindowsVersion() As MYVERSION
		Dim myOS As OSVERSIONINFO
		Dim WinVer As MYVERSION
		Dim lResult As Integer
		
		myOS.dwOSVersionInfoSize = Len(myOS) '应该是 148
		
		lResult = GetVersionEx(myOS)
		
		'用相关信息填充用户类型
		WinVer.lMajorVersion = myOS.dwMajorVersion
		WinVer.lMinorVersion = myOS.dwMinorVersion
		WinVer.lExtraInfo = myOS.dwPlatformId
		WinVer.lBuildNumber = myOS.dwBuildNumber
		
		'UPGRADE_WARNING: 未能解析对象 WindowsVersion 的默认属性。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"”
		WindowsVersion = WinVer
		
	End Function
End Module