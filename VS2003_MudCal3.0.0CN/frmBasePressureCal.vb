Option Strict Off
Option Explicit On
Friend Class frmBasePressureCal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lblAnnularMw As System.Windows.Forms.Label
	Public WithEvents fraAnnularMw As System.Windows.Forms.GroupBox
	Public WithEvents txtAnnularMwhwell As System.Windows.Forms.TextBox
	Public WithEvents txtAnnularMwpressure As System.Windows.Forms.TextBox
	Public WithEvents txtAnnularMwmw As System.Windows.Forms.TextBox
	Public WithEvents lblAnnularMwhwell As System.Windows.Forms.Label
	Public WithEvents lblAnnularMwpressure As System.Windows.Forms.Label
	Public WithEvents lblAnnularMwmw As System.Windows.Forms.Label
	Public WithEvents fraAnnularMwInput As System.Windows.Forms.GroupBox
	Public WithEvents txtAnnularMwoutmwpm As System.Windows.Forms.TextBox
	Public WithEvents txtAnnularMwoutmw As System.Windows.Forms.TextBox
	Public WithEvents lblAnnularMwoutmwpm As System.Windows.Forms.Label
	Public WithEvents lblAnnularMwoutmw As System.Windows.Forms.Label
	Public WithEvents fraAnnularMwOutput As System.Windows.Forms.GroupBox
	Public WithEvents cmdAnnularMwquit As System.Windows.Forms.Button
	Public WithEvents cmdAnnularMwclear As System.Windows.Forms.Button
	Public WithEvents cmdAnnularMwok As System.Windows.Forms.Button
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents _SSTab1_TabPage0 As System.Windows.Forms.TabPage
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents fraMw As System.Windows.Forms.GroupBox
	Public WithEvents txtMudcasePressure As System.Windows.Forms.TextBox
	Public WithEvents txtMudcasehwell As System.Windows.Forms.TextBox
	Public WithEvents txtMudmw As System.Windows.Forms.TextBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents fraMwInput As System.Windows.Forms.GroupBox
	Public WithEvents txtMudoutputmw As System.Windows.Forms.TextBox
	Public WithEvents txtMudoutputa As System.Windows.Forms.TextBox
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents fraMwOutput As System.Windows.Forms.GroupBox
	Public WithEvents cmdMwQuit As System.Windows.Forms.Button
	Public WithEvents cmdMwClear As System.Windows.Forms.Button
	Public WithEvents cmdMwOk As System.Windows.Forms.Button
	Public WithEvents PicLogoECD As System.Windows.Forms.PictureBox
	Public WithEvents _SSTab1_TabPage1 As System.Windows.Forms.TabPage
	Public WithEvents lblYPprogram As System.Windows.Forms.Label
	Public WithEvents fraYPprogram As System.Windows.Forms.GroupBox
	Public WithEvents txtYPh As System.Windows.Forms.TextBox
	Public WithEvents txtYPmwh As System.Windows.Forms.TextBox
	Public WithEvents txtYPmwb As System.Windows.Forms.TextBox
	Public WithEvents txtYPdp As System.Windows.Forms.TextBox
	Public WithEvents txtYPdh As System.Windows.Forms.TextBox
	Public WithEvents txtYPyp As System.Windows.Forms.TextBox
	Public WithEvents Line3 As System.Windows.Forms.Label
	Public WithEvents Line2 As System.Windows.Forms.Label
	Public WithEvents Line1 As System.Windows.Forms.Label
	Public WithEvents lblYPh As System.Windows.Forms.Label
	Public WithEvents lblYPmwh As System.Windows.Forms.Label
	Public WithEvents lblYPmwb As System.Windows.Forms.Label
	Public WithEvents lblYPdp As System.Windows.Forms.Label
	Public WithEvents lblYPdh As System.Windows.Forms.Label
	Public WithEvents lblYPyp As System.Windows.Forms.Label
	Public WithEvents fraYPinputdata As System.Windows.Forms.GroupBox
	Public WithEvents txtYPmw As System.Windows.Forms.TextBox
	Public WithEvents lblYPmw As System.Windows.Forms.Label
	Public WithEvents fraYPmw As System.Windows.Forms.GroupBox
	Public WithEvents txtYPECD As System.Windows.Forms.TextBox
	Public WithEvents lblYPECD As System.Windows.Forms.Label
	Public WithEvents fraYPECD As System.Windows.Forms.GroupBox
	Public WithEvents txtYPPann As System.Windows.Forms.TextBox
	Public WithEvents lblYPPann As System.Windows.Forms.Label
	Public WithEvents fraYPPann As System.Windows.Forms.GroupBox
	Public WithEvents cmdYPOk As System.Windows.Forms.Button
	Public WithEvents cmdYPClear As System.Windows.Forms.Button
	Public WithEvents cmdYPQuit As System.Windows.Forms.Button
	Public WithEvents _SSTab1_TabPage2 As System.Windows.Forms.TabPage
	Public WithEvents SSTab1 As System.Windows.Forms.TabControl
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picBasePressureLeakoff As System.Windows.Forms.PictureBox
    Friend WithEvents picBasePressureEMW As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBasePressureCal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picBasePressureLeakoff = New System.Windows.Forms.PictureBox
        Me.picBasePressureEMW = New System.Windows.Forms.PictureBox
        Me.SSTab1 = New System.Windows.Forms.TabControl
        Me._SSTab1_TabPage0 = New System.Windows.Forms.TabPage
        Me.fraAnnularMw = New System.Windows.Forms.GroupBox
        Me.lblAnnularMw = New System.Windows.Forms.Label
        Me.fraAnnularMwInput = New System.Windows.Forms.GroupBox
        Me.txtAnnularMwhwell = New System.Windows.Forms.TextBox
        Me.txtAnnularMwpressure = New System.Windows.Forms.TextBox
        Me.txtAnnularMwmw = New System.Windows.Forms.TextBox
        Me.lblAnnularMwhwell = New System.Windows.Forms.Label
        Me.lblAnnularMwpressure = New System.Windows.Forms.Label
        Me.lblAnnularMwmw = New System.Windows.Forms.Label
        Me.fraAnnularMwOutput = New System.Windows.Forms.GroupBox
        Me.txtAnnularMwoutmwpm = New System.Windows.Forms.TextBox
        Me.txtAnnularMwoutmw = New System.Windows.Forms.TextBox
        Me.lblAnnularMwoutmwpm = New System.Windows.Forms.Label
        Me.lblAnnularMwoutmw = New System.Windows.Forms.Label
        Me.cmdAnnularMwquit = New System.Windows.Forms.Button
        Me.cmdAnnularMwclear = New System.Windows.Forms.Button
        Me.cmdAnnularMwok = New System.Windows.Forms.Button
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me._SSTab1_TabPage1 = New System.Windows.Forms.TabPage
        Me.fraMw = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.fraMwInput = New System.Windows.Forms.GroupBox
        Me.txtMudcasePressure = New System.Windows.Forms.TextBox
        Me.txtMudcasehwell = New System.Windows.Forms.TextBox
        Me.txtMudmw = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.fraMwOutput = New System.Windows.Forms.GroupBox
        Me.txtMudoutputmw = New System.Windows.Forms.TextBox
        Me.txtMudoutputa = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmdMwQuit = New System.Windows.Forms.Button
        Me.cmdMwClear = New System.Windows.Forms.Button
        Me.cmdMwOk = New System.Windows.Forms.Button
        Me.PicLogoECD = New System.Windows.Forms.PictureBox
        Me._SSTab1_TabPage2 = New System.Windows.Forms.TabPage
        Me.fraYPprogram = New System.Windows.Forms.GroupBox
        Me.lblYPprogram = New System.Windows.Forms.Label
        Me.fraYPinputdata = New System.Windows.Forms.GroupBox
        Me.txtYPh = New System.Windows.Forms.TextBox
        Me.txtYPmwh = New System.Windows.Forms.TextBox
        Me.txtYPmwb = New System.Windows.Forms.TextBox
        Me.txtYPdp = New System.Windows.Forms.TextBox
        Me.txtYPdh = New System.Windows.Forms.TextBox
        Me.txtYPyp = New System.Windows.Forms.TextBox
        Me.Line3 = New System.Windows.Forms.Label
        Me.Line2 = New System.Windows.Forms.Label
        Me.Line1 = New System.Windows.Forms.Label
        Me.lblYPh = New System.Windows.Forms.Label
        Me.lblYPmwh = New System.Windows.Forms.Label
        Me.lblYPmwb = New System.Windows.Forms.Label
        Me.lblYPdp = New System.Windows.Forms.Label
        Me.lblYPdh = New System.Windows.Forms.Label
        Me.lblYPyp = New System.Windows.Forms.Label
        Me.fraYPmw = New System.Windows.Forms.GroupBox
        Me.txtYPmw = New System.Windows.Forms.TextBox
        Me.lblYPmw = New System.Windows.Forms.Label
        Me.fraYPECD = New System.Windows.Forms.GroupBox
        Me.txtYPECD = New System.Windows.Forms.TextBox
        Me.lblYPECD = New System.Windows.Forms.Label
        Me.fraYPPann = New System.Windows.Forms.GroupBox
        Me.txtYPPann = New System.Windows.Forms.TextBox
        Me.lblYPPann = New System.Windows.Forms.Label
        Me.cmdYPOk = New System.Windows.Forms.Button
        Me.cmdYPClear = New System.Windows.Forms.Button
        Me.cmdYPQuit = New System.Windows.Forms.Button
        Me.SSTab1.SuspendLayout()
        Me._SSTab1_TabPage0.SuspendLayout()
        Me.fraAnnularMw.SuspendLayout()
        Me.fraAnnularMwInput.SuspendLayout()
        Me.fraAnnularMwOutput.SuspendLayout()
        Me._SSTab1_TabPage1.SuspendLayout()
        Me.fraMw.SuspendLayout()
        Me.fraMwInput.SuspendLayout()
        Me.fraMwOutput.SuspendLayout()
        Me._SSTab1_TabPage2.SuspendLayout()
        Me.fraYPprogram.SuspendLayout()
        Me.fraYPinputdata.SuspendLayout()
        Me.fraYPmw.SuspendLayout()
        Me.fraYPECD.SuspendLayout()
        Me.fraYPPann.SuspendLayout()
        Me.SuspendLayout()
        '
        'picBasePressureLeakoff
        '
        Me.picBasePressureLeakoff.Image = CType(resources.GetObject("picBasePressureLeakoff.Image"), System.Drawing.Image)
        Me.picBasePressureLeakoff.Location = New System.Drawing.Point(648, 120)
        Me.picBasePressureLeakoff.Name = "picBasePressureLeakoff"
        Me.picBasePressureLeakoff.Size = New System.Drawing.Size(180, 400)
        Me.picBasePressureLeakoff.TabIndex = 63
        Me.picBasePressureLeakoff.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picBasePressureLeakoff, "GMP10RIG,Talara Peru")
        '
        'picBasePressureEMW
        '
        Me.picBasePressureEMW.Image = CType(resources.GetObject("picBasePressureEMW.Image"), System.Drawing.Image)
        Me.picBasePressureEMW.Location = New System.Drawing.Point(648, 120)
        Me.picBasePressureEMW.Name = "picBasePressureEMW"
        Me.picBasePressureEMW.Size = New System.Drawing.Size(180, 400)
        Me.picBasePressureEMW.TabIndex = 64
        Me.picBasePressureEMW.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picBasePressureEMW, "GMP8RIG,TALARA PERU")
        '
        'SSTab1
        '
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage0)
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage1)
        Me.SSTab1.Controls.Add(Me._SSTab1_TabPage2)
        Me.SSTab1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SSTab1.ItemSize = New System.Drawing.Size(42, 30)
        Me.SSTab1.Location = New System.Drawing.Point(8, 8)
        Me.SSTab1.Name = "SSTab1"
        Me.SSTab1.SelectedIndex = 0
        Me.SSTab1.Size = New System.Drawing.Size(894, 570)
        Me.SSTab1.TabIndex = 0
        '
        '_SSTab1_TabPage0
        '
        Me._SSTab1_TabPage0.Controls.Add(Me.picBasePressureLeakoff)
        Me._SSTab1_TabPage0.Controls.Add(Me.fraAnnularMw)
        Me._SSTab1_TabPage0.Controls.Add(Me.fraAnnularMwInput)
        Me._SSTab1_TabPage0.Controls.Add(Me.fraAnnularMwOutput)
        Me._SSTab1_TabPage0.Controls.Add(Me.cmdAnnularMwquit)
        Me._SSTab1_TabPage0.Controls.Add(Me.cmdAnnularMwclear)
        Me._SSTab1_TabPage0.Controls.Add(Me.cmdAnnularMwok)
        Me._SSTab1_TabPage0.Controls.Add(Me.PicLogo)
        Me._SSTab1_TabPage0.Location = New System.Drawing.Point(4, 34)
        Me._SSTab1_TabPage0.Name = "_SSTab1_TabPage0"
        Me._SSTab1_TabPage0.Size = New System.Drawing.Size(886, 532)
        Me._SSTab1_TabPage0.TabIndex = 0
        Me._SSTab1_TabPage0.Text = "漏失试验"
        '
        'fraAnnularMw
        '
        Me.fraAnnularMw.BackColor = System.Drawing.SystemColors.Control
        Me.fraAnnularMw.Controls.Add(Me.lblAnnularMw)
        Me.fraAnnularMw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraAnnularMw.Location = New System.Drawing.Point(8, 8)
        Me.fraAnnularMw.Name = "fraAnnularMw"
        Me.fraAnnularMw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraAnnularMw.Size = New System.Drawing.Size(868, 100)
        Me.fraAnnularMw.TabIndex = 13
        Me.fraAnnularMw.TabStop = False
        Me.fraAnnularMw.Text = "程序说明"
        '
        'lblAnnularMw
        '
        Me.lblAnnularMw.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMw.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAnnularMw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMw.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAnnularMw.ForeColor = System.Drawing.Color.Blue
        Me.lblAnnularMw.Location = New System.Drawing.Point(10, 17)
        Me.lblAnnularMw.Name = "lblAnnularMw"
        Me.lblAnnularMw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMw.Size = New System.Drawing.Size(848, 74)
        Me.lblAnnularMw.TabIndex = 16
        Me.lblAnnularMw.Text = "    本程序用于漏失试验计算地层破裂压力。现场要获得实际的地层破裂压力，最好的方法是进行漏失试验。这也是现场最常用的方法，通常在套管鞋以下钻出一段裸眼井后进行。" & _
        "首先通过液压试验检验注水泥作业，只有在注水泥作业完好的情况下才能成功地对套管鞋下的砂层进行破裂压力试验。在取得第一个砂层的破裂压力梯度值后，一般在开钻后所采用的" & _
        "钻井液密度不应超过此破裂压力梯度值。"
        '
        'fraAnnularMwInput
        '
        Me.fraAnnularMwInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraAnnularMwInput.Controls.Add(Me.txtAnnularMwhwell)
        Me.fraAnnularMwInput.Controls.Add(Me.txtAnnularMwpressure)
        Me.fraAnnularMwInput.Controls.Add(Me.txtAnnularMwmw)
        Me.fraAnnularMwInput.Controls.Add(Me.lblAnnularMwhwell)
        Me.fraAnnularMwInput.Controls.Add(Me.lblAnnularMwpressure)
        Me.fraAnnularMwInput.Controls.Add(Me.lblAnnularMwmw)
        Me.fraAnnularMwInput.ForeColor = System.Drawing.Color.Blue
        Me.fraAnnularMwInput.Location = New System.Drawing.Point(8, 116)
        Me.fraAnnularMwInput.Name = "fraAnnularMwInput"
        Me.fraAnnularMwInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraAnnularMwInput.Size = New System.Drawing.Size(582, 140)
        Me.fraAnnularMwInput.TabIndex = 14
        Me.fraAnnularMwInput.TabStop = False
        Me.fraAnnularMwInput.Text = "数据输入"
        '
        'txtAnnularMwhwell
        '
        Me.txtAnnularMwhwell.AcceptsReturn = True
        Me.txtAnnularMwhwell.AutoSize = False
        Me.txtAnnularMwhwell.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnnularMwhwell.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnnularMwhwell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAnnularMwhwell.Location = New System.Drawing.Point(490, 26)
        Me.txtAnnularMwhwell.MaxLength = 0
        Me.txtAnnularMwhwell.Name = "txtAnnularMwhwell"
        Me.txtAnnularMwhwell.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnnularMwhwell.Size = New System.Drawing.Size(73, 19)
        Me.txtAnnularMwhwell.TabIndex = 2
        Me.txtAnnularMwhwell.Text = "2781"
        Me.txtAnnularMwhwell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAnnularMwpressure
        '
        Me.txtAnnularMwpressure.AcceptsReturn = True
        Me.txtAnnularMwpressure.AutoSize = False
        Me.txtAnnularMwpressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnnularMwpressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnnularMwpressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAnnularMwpressure.Location = New System.Drawing.Point(202, 86)
        Me.txtAnnularMwpressure.MaxLength = 0
        Me.txtAnnularMwpressure.Name = "txtAnnularMwpressure"
        Me.txtAnnularMwpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnnularMwpressure.Size = New System.Drawing.Size(73, 19)
        Me.txtAnnularMwpressure.TabIndex = 3
        Me.txtAnnularMwpressure.Text = "14.5"
        Me.txtAnnularMwpressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAnnularMwmw
        '
        Me.txtAnnularMwmw.AcceptsReturn = True
        Me.txtAnnularMwmw.AutoSize = False
        Me.txtAnnularMwmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnnularMwmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnnularMwmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAnnularMwmw.Location = New System.Drawing.Point(202, 26)
        Me.txtAnnularMwmw.MaxLength = 0
        Me.txtAnnularMwmw.Name = "txtAnnularMwmw"
        Me.txtAnnularMwmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnnularMwmw.Size = New System.Drawing.Size(73, 19)
        Me.txtAnnularMwmw.TabIndex = 1
        Me.txtAnnularMwmw.Text = "1.46"
        Me.txtAnnularMwmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAnnularMwhwell
        '
        Me.lblAnnularMwhwell.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMwhwell.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMwhwell.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnnularMwhwell.Location = New System.Drawing.Point(346, 24)
        Me.lblAnnularMwhwell.Name = "lblAnnularMwhwell"
        Me.lblAnnularMwhwell.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMwhwell.Size = New System.Drawing.Size(130, 22)
        Me.lblAnnularMwhwell.TabIndex = 23
        Me.lblAnnularMwhwell.Text = "实验井深m(TVD)  m"
        '
        'lblAnnularMwpressure
        '
        Me.lblAnnularMwpressure.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMwpressure.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMwpressure.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnnularMwpressure.Location = New System.Drawing.Point(8, 86)
        Me.lblAnnularMwpressure.Name = "lblAnnularMwpressure"
        Me.lblAnnularMwpressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMwpressure.Size = New System.Drawing.Size(159, 18)
        Me.lblAnnularMwpressure.TabIndex = 22
        Me.lblAnnularMwpressure.Text = "破裂压力 MPa"
        '
        'lblAnnularMwmw
        '
        Me.lblAnnularMwmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMwmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMwmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnnularMwmw.Location = New System.Drawing.Point(8, 26)
        Me.lblAnnularMwmw.Name = "lblAnnularMwmw"
        Me.lblAnnularMwmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMwmw.Size = New System.Drawing.Size(159, 18)
        Me.lblAnnularMwmw.TabIndex = 21
        Me.lblAnnularMwmw.Text = "试验用钻井液密度 g/cm^3"
        '
        'fraAnnularMwOutput
        '
        Me.fraAnnularMwOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraAnnularMwOutput.Controls.Add(Me.txtAnnularMwoutmwpm)
        Me.fraAnnularMwOutput.Controls.Add(Me.txtAnnularMwoutmw)
        Me.fraAnnularMwOutput.Controls.Add(Me.lblAnnularMwoutmwpm)
        Me.fraAnnularMwOutput.Controls.Add(Me.lblAnnularMwoutmw)
        Me.fraAnnularMwOutput.ForeColor = System.Drawing.Color.Red
        Me.fraAnnularMwOutput.Location = New System.Drawing.Point(8, 264)
        Me.fraAnnularMwOutput.Name = "fraAnnularMwOutput"
        Me.fraAnnularMwOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraAnnularMwOutput.Size = New System.Drawing.Size(390, 109)
        Me.fraAnnularMwOutput.TabIndex = 15
        Me.fraAnnularMwOutput.TabStop = False
        Me.fraAnnularMwOutput.Text = "计算结果"
        '
        'txtAnnularMwoutmwpm
        '
        Me.txtAnnularMwoutmwpm.AcceptsReturn = True
        Me.txtAnnularMwoutmwpm.AutoSize = False
        Me.txtAnnularMwoutmwpm.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnnularMwoutmwpm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnnularMwoutmwpm.ForeColor = System.Drawing.Color.Red
        Me.txtAnnularMwoutmwpm.Location = New System.Drawing.Point(293, 71)
        Me.txtAnnularMwoutmwpm.MaxLength = 0
        Me.txtAnnularMwoutmwpm.Name = "txtAnnularMwoutmwpm"
        Me.txtAnnularMwoutmwpm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnnularMwoutmwpm.Size = New System.Drawing.Size(73, 19)
        Me.txtAnnularMwoutmwpm.TabIndex = 27
        Me.txtAnnularMwoutmwpm.Text = ""
        Me.txtAnnularMwoutmwpm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAnnularMwoutmw
        '
        Me.txtAnnularMwoutmw.AcceptsReturn = True
        Me.txtAnnularMwoutmw.AutoSize = False
        Me.txtAnnularMwoutmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnnularMwoutmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAnnularMwoutmw.ForeColor = System.Drawing.Color.Red
        Me.txtAnnularMwoutmw.Location = New System.Drawing.Point(293, 40)
        Me.txtAnnularMwoutmw.MaxLength = 0
        Me.txtAnnularMwoutmw.Name = "txtAnnularMwoutmw"
        Me.txtAnnularMwoutmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAnnularMwoutmw.Size = New System.Drawing.Size(73, 20)
        Me.txtAnnularMwoutmw.TabIndex = 25
        Me.txtAnnularMwoutmw.Text = ""
        Me.txtAnnularMwoutmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAnnularMwoutmwpm
        '
        Me.lblAnnularMwoutmwpm.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMwoutmwpm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMwoutmwpm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnnularMwoutmwpm.Location = New System.Drawing.Point(8, 69)
        Me.lblAnnularMwoutmwpm.Name = "lblAnnularMwoutmwpm"
        Me.lblAnnularMwoutmwpm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMwoutmwpm.Size = New System.Drawing.Size(216, 22)
        Me.lblAnnularMwoutmwpm.TabIndex = 26
        Me.lblAnnularMwoutmwpm.Text = "破裂压力梯度 MPa/m"
        Me.lblAnnularMwoutmwpm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAnnularMwoutmw
        '
        Me.lblAnnularMwoutmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblAnnularMwoutmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAnnularMwoutmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAnnularMwoutmw.Location = New System.Drawing.Point(8, 39)
        Me.lblAnnularMwoutmw.Name = "lblAnnularMwoutmw"
        Me.lblAnnularMwoutmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAnnularMwoutmw.Size = New System.Drawing.Size(216, 22)
        Me.lblAnnularMwoutmw.TabIndex = 24
        Me.lblAnnularMwoutmw.Text = "破裂压力当量密度 g/cm^3"
        Me.lblAnnularMwoutmw.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdAnnularMwquit
        '
        Me.cmdAnnularMwquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAnnularMwquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAnnularMwquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAnnularMwquit.Location = New System.Drawing.Point(437, 336)
        Me.cmdAnnularMwquit.Name = "cmdAnnularMwquit"
        Me.cmdAnnularMwquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAnnularMwquit.Size = New System.Drawing.Size(80, 20)
        Me.cmdAnnularMwquit.TabIndex = 6
        Me.cmdAnnularMwquit.Text = "Exit"
        '
        'cmdAnnularMwclear
        '
        Me.cmdAnnularMwclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAnnularMwclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAnnularMwclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAnnularMwclear.Location = New System.Drawing.Point(437, 308)
        Me.cmdAnnularMwclear.Name = "cmdAnnularMwclear"
        Me.cmdAnnularMwclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAnnularMwclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdAnnularMwclear.TabIndex = 5
        Me.cmdAnnularMwclear.Text = "Clean"
        '
        'cmdAnnularMwok
        '
        Me.cmdAnnularMwok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAnnularMwok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAnnularMwok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAnnularMwok.Location = New System.Drawing.Point(437, 280)
        Me.cmdAnnularMwok.Name = "cmdAnnularMwok"
        Me.cmdAnnularMwok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAnnularMwok.Size = New System.Drawing.Size(80, 20)
        Me.cmdAnnularMwok.TabIndex = 4
        Me.cmdAnnularMwok.Text = "Run"
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(136, 408)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(174, 70)
        Me.PicLogo.TabIndex = 62
        Me.PicLogo.TabStop = False
        '
        '_SSTab1_TabPage1
        '
        Me._SSTab1_TabPage1.Controls.Add(Me.picBasePressureEMW)
        Me._SSTab1_TabPage1.Controls.Add(Me.fraMw)
        Me._SSTab1_TabPage1.Controls.Add(Me.fraMwInput)
        Me._SSTab1_TabPage1.Controls.Add(Me.fraMwOutput)
        Me._SSTab1_TabPage1.Controls.Add(Me.cmdMwQuit)
        Me._SSTab1_TabPage1.Controls.Add(Me.cmdMwClear)
        Me._SSTab1_TabPage1.Controls.Add(Me.cmdMwOk)
        Me._SSTab1_TabPage1.Controls.Add(Me.PicLogoECD)
        Me._SSTab1_TabPage1.Location = New System.Drawing.Point(4, 34)
        Me._SSTab1_TabPage1.Name = "_SSTab1_TabPage1"
        Me._SSTab1_TabPage1.Size = New System.Drawing.Size(886, 532)
        Me._SSTab1_TabPage1.TabIndex = 1
        Me._SSTab1_TabPage1.Text = "钻井液当量密度"
        '
        'fraMw
        '
        Me.fraMw.BackColor = System.Drawing.SystemColors.Control
        Me.fraMw.Controls.Add(Me.Label2)
        Me.fraMw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraMw.Location = New System.Drawing.Point(8, 8)
        Me.fraMw.Name = "fraMw"
        Me.fraMw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMw.Size = New System.Drawing.Size(868, 100)
        Me.fraMw.TabIndex = 17
        Me.fraMw.TabStop = False
        Me.fraMw.Text = "程序说明"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(10, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(848, 74)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "    本程序主要进行钻井液当量密度的计算。当量钻井液密度等于环空钻井液密度加上钻井液密度增量，而钻井液密度增量是由附加压力引起的，通常情况下，附加压力包括环空压" & _
        "力损失与井涌控制压力（举例：关井立管压力）。"
        '
        'fraMwInput
        '
        Me.fraMwInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraMwInput.Controls.Add(Me.txtMudcasePressure)
        Me.fraMwInput.Controls.Add(Me.txtMudcasehwell)
        Me.fraMwInput.Controls.Add(Me.txtMudmw)
        Me.fraMwInput.Controls.Add(Me.Label4)
        Me.fraMwInput.Controls.Add(Me.Label3)
        Me.fraMwInput.Controls.Add(Me.Label1)
        Me.fraMwInput.ForeColor = System.Drawing.Color.Blue
        Me.fraMwInput.Location = New System.Drawing.Point(8, 116)
        Me.fraMwInput.Name = "fraMwInput"
        Me.fraMwInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMwInput.Size = New System.Drawing.Size(582, 140)
        Me.fraMwInput.TabIndex = 19
        Me.fraMwInput.TabStop = False
        Me.fraMwInput.Text = "数据输入"
        '
        'txtMudcasePressure
        '
        Me.txtMudcasePressure.AcceptsReturn = True
        Me.txtMudcasePressure.AutoSize = False
        Me.txtMudcasePressure.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudcasePressure.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudcasePressure.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudcasePressure.Location = New System.Drawing.Point(166, 96)
        Me.txtMudcasePressure.MaxLength = 0
        Me.txtMudcasePressure.Name = "txtMudcasePressure"
        Me.txtMudcasePressure.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudcasePressure.Size = New System.Drawing.Size(73, 20)
        Me.txtMudcasePressure.TabIndex = 8
        Me.txtMudcasePressure.Text = "1.2"
        Me.txtMudcasePressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMudcasehwell
        '
        Me.txtMudcasehwell.AcceptsReturn = True
        Me.txtMudcasehwell.AutoSize = False
        Me.txtMudcasehwell.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudcasehwell.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudcasehwell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudcasehwell.Location = New System.Drawing.Point(464, 39)
        Me.txtMudcasehwell.MaxLength = 0
        Me.txtMudcasehwell.Name = "txtMudcasehwell"
        Me.txtMudcasehwell.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudcasehwell.Size = New System.Drawing.Size(73, 20)
        Me.txtMudcasehwell.TabIndex = 9
        Me.txtMudcasehwell.Text = "2000"
        Me.txtMudcasehwell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMudmw
        '
        Me.txtMudmw.AcceptsReturn = True
        Me.txtMudmw.AutoSize = False
        Me.txtMudmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudmw.Location = New System.Drawing.Point(166, 39)
        Me.txtMudmw.MaxLength = 0
        Me.txtMudmw.Name = "txtMudmw"
        Me.txtMudmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudmw.Size = New System.Drawing.Size(73, 20)
        Me.txtMudmw.TabIndex = 7
        Me.txtMudmw.Text = "1.20"
        Me.txtMudmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(13, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(136, 20)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "关井套压(MPa)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(288, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(166, 20)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "套管深度TVD(m)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(13, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(136, 20)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "钻井液密度(g/cm^3)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraMwOutput
        '
        Me.fraMwOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraMwOutput.Controls.Add(Me.txtMudoutputmw)
        Me.fraMwOutput.Controls.Add(Me.txtMudoutputa)
        Me.fraMwOutput.Controls.Add(Me.Label6)
        Me.fraMwOutput.Controls.Add(Me.Label5)
        Me.fraMwOutput.ForeColor = System.Drawing.Color.Red
        Me.fraMwOutput.Location = New System.Drawing.Point(8, 264)
        Me.fraMwOutput.Name = "fraMwOutput"
        Me.fraMwOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMwOutput.Size = New System.Drawing.Size(390, 109)
        Me.fraMwOutput.TabIndex = 20
        Me.fraMwOutput.TabStop = False
        Me.fraMwOutput.Text = "计算结果"
        '
        'txtMudoutputmw
        '
        Me.txtMudoutputmw.AcceptsReturn = True
        Me.txtMudoutputmw.AutoSize = False
        Me.txtMudoutputmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudoutputmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudoutputmw.ForeColor = System.Drawing.Color.Red
        Me.txtMudoutputmw.Location = New System.Drawing.Point(302, 65)
        Me.txtMudoutputmw.MaxLength = 0
        Me.txtMudoutputmw.Name = "txtMudoutputmw"
        Me.txtMudoutputmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudoutputmw.Size = New System.Drawing.Size(73, 20)
        Me.txtMudoutputmw.TabIndex = 34
        Me.txtMudoutputmw.Text = ""
        '
        'txtMudoutputa
        '
        Me.txtMudoutputa.AcceptsReturn = True
        Me.txtMudoutputa.AutoSize = False
        Me.txtMudoutputa.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudoutputa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudoutputa.ForeColor = System.Drawing.Color.Red
        Me.txtMudoutputa.Location = New System.Drawing.Point(302, 26)
        Me.txtMudoutputa.MaxLength = 0
        Me.txtMudoutputa.Name = "txtMudoutputa"
        Me.txtMudoutputa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudoutputa.Size = New System.Drawing.Size(73, 20)
        Me.txtMudoutputa.TabIndex = 33
        Me.txtMudoutputa.Text = ""
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(10, 65)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(279, 20)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "套管鞋处钻井液当量密度(g/cm^3)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(10, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(279, 20)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "套管鞋处钻井液密度增量(g/cm^3)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdMwQuit
        '
        Me.cmdMwQuit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMwQuit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMwQuit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMwQuit.Location = New System.Drawing.Point(437, 338)
        Me.cmdMwQuit.Name = "cmdMwQuit"
        Me.cmdMwQuit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMwQuit.Size = New System.Drawing.Size(80, 20)
        Me.cmdMwQuit.TabIndex = 12
        Me.cmdMwQuit.Text = "Exit"
        '
        'cmdMwClear
        '
        Me.cmdMwClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMwClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMwClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMwClear.Location = New System.Drawing.Point(437, 310)
        Me.cmdMwClear.Name = "cmdMwClear"
        Me.cmdMwClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMwClear.Size = New System.Drawing.Size(80, 20)
        Me.cmdMwClear.TabIndex = 11
        Me.cmdMwClear.Text = "Clean"
        '
        'cmdMwOk
        '
        Me.cmdMwOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMwOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMwOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMwOk.Location = New System.Drawing.Point(437, 282)
        Me.cmdMwOk.Name = "cmdMwOk"
        Me.cmdMwOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMwOk.Size = New System.Drawing.Size(80, 20)
        Me.cmdMwOk.TabIndex = 10
        Me.cmdMwOk.Text = "Run"
        '
        'PicLogoECD
        '
        Me.PicLogoECD.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogoECD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogoECD.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogoECD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogoECD.Location = New System.Drawing.Point(136, 408)
        Me.PicLogoECD.Name = "PicLogoECD"
        Me.PicLogoECD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogoECD.Size = New System.Drawing.Size(126, 70)
        Me.PicLogoECD.TabIndex = 63
        Me.PicLogoECD.TabStop = False
        '
        '_SSTab1_TabPage2
        '
        Me._SSTab1_TabPage2.Controls.Add(Me.fraYPprogram)
        Me._SSTab1_TabPage2.Controls.Add(Me.fraYPinputdata)
        Me._SSTab1_TabPage2.Controls.Add(Me.fraYPmw)
        Me._SSTab1_TabPage2.Controls.Add(Me.fraYPECD)
        Me._SSTab1_TabPage2.Controls.Add(Me.fraYPPann)
        Me._SSTab1_TabPage2.Controls.Add(Me.cmdYPOk)
        Me._SSTab1_TabPage2.Controls.Add(Me.cmdYPClear)
        Me._SSTab1_TabPage2.Controls.Add(Me.cmdYPQuit)
        Me._SSTab1_TabPage2.Location = New System.Drawing.Point(4, 34)
        Me._SSTab1_TabPage2.Name = "_SSTab1_TabPage2"
        Me._SSTab1_TabPage2.Size = New System.Drawing.Size(886, 532)
        Me._SSTab1_TabPage2.TabIndex = 2
        Me._SSTab1_TabPage2.Text = "利用屈服值进行的经验计算"
        '
        'fraYPprogram
        '
        Me.fraYPprogram.BackColor = System.Drawing.SystemColors.Control
        Me.fraYPprogram.Controls.Add(Me.lblYPprogram)
        Me.fraYPprogram.ForeColor = System.Drawing.Color.Blue
        Me.fraYPprogram.Location = New System.Drawing.Point(10, 43)
        Me.fraYPprogram.Name = "fraYPprogram"
        Me.fraYPprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraYPprogram.Size = New System.Drawing.Size(826, 79)
        Me.fraYPprogram.TabIndex = 35
        Me.fraYPprogram.TabStop = False
        Me.fraYPprogram.Text = "程序说明"
        '
        'lblYPprogram
        '
        Me.lblYPprogram.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPprogram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblYPprogram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPprogram.ForeColor = System.Drawing.Color.Blue
        Me.lblYPprogram.Location = New System.Drawing.Point(10, 17)
        Me.lblYPprogram.Name = "lblYPprogram"
        Me.lblYPprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPprogram.Size = New System.Drawing.Size(807, 53)
        Me.lblYPprogram.TabIndex = 61
        Me.lblYPprogram.Text = "本程序利用钻井液屈服值可进行起钻时克服抽汲作用的安全钻井液密度、估算当量循环密度、估算环形空间压力损失（层流时）的经验计算。同颜色已知数据输入内容对应同颜色的计算" & _
        "结果，请输入数据时注意区别。经验公式来源《工程师手册》，计算结果仅供参考，请勿用于现场指导生产。"
        '
        'fraYPinputdata
        '
        Me.fraYPinputdata.BackColor = System.Drawing.SystemColors.Control
        Me.fraYPinputdata.Controls.Add(Me.txtYPh)
        Me.fraYPinputdata.Controls.Add(Me.txtYPmwh)
        Me.fraYPinputdata.Controls.Add(Me.txtYPmwb)
        Me.fraYPinputdata.Controls.Add(Me.txtYPdp)
        Me.fraYPinputdata.Controls.Add(Me.txtYPdh)
        Me.fraYPinputdata.Controls.Add(Me.txtYPyp)
        Me.fraYPinputdata.Controls.Add(Me.Line3)
        Me.fraYPinputdata.Controls.Add(Me.Line2)
        Me.fraYPinputdata.Controls.Add(Me.Line1)
        Me.fraYPinputdata.Controls.Add(Me.lblYPh)
        Me.fraYPinputdata.Controls.Add(Me.lblYPmwh)
        Me.fraYPinputdata.Controls.Add(Me.lblYPmwb)
        Me.fraYPinputdata.Controls.Add(Me.lblYPdp)
        Me.fraYPinputdata.Controls.Add(Me.lblYPdh)
        Me.fraYPinputdata.Controls.Add(Me.lblYPyp)
        Me.fraYPinputdata.ForeColor = System.Drawing.Color.Blue
        Me.fraYPinputdata.Location = New System.Drawing.Point(10, 125)
        Me.fraYPinputdata.Name = "fraYPinputdata"
        Me.fraYPinputdata.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraYPinputdata.Size = New System.Drawing.Size(414, 242)
        Me.fraYPinputdata.TabIndex = 36
        Me.fraYPinputdata.TabStop = False
        Me.fraYPinputdata.Text = "已知数据输入"
        '
        'txtYPh
        '
        Me.txtYPh.AcceptsReturn = True
        Me.txtYPh.AutoSize = False
        Me.txtYPh.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(192, Byte), CType(255, Byte))
        Me.txtYPh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPh.Location = New System.Drawing.Point(259, 211)
        Me.txtYPh.MaxLength = 0
        Me.txtYPh.Name = "txtYPh"
        Me.txtYPh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPh.Size = New System.Drawing.Size(97, 23)
        Me.txtYPh.TabIndex = 54
        Me.txtYPh.Text = "3100"
        '
        'txtYPmwh
        '
        Me.txtYPmwh.AcceptsReturn = True
        Me.txtYPmwh.AutoSize = False
        Me.txtYPmwh.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtYPmwh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPmwh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPmwh.Location = New System.Drawing.Point(259, 168)
        Me.txtYPmwh.MaxLength = 0
        Me.txtYPmwh.Name = "txtYPmwh"
        Me.txtYPmwh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPmwh.Size = New System.Drawing.Size(97, 23)
        Me.txtYPmwh.TabIndex = 53
        Me.txtYPmwh.Text = "1.20"
        '
        'txtYPmwb
        '
        Me.txtYPmwb.AcceptsReturn = True
        Me.txtYPmwb.AutoSize = False
        Me.txtYPmwb.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(192, Byte))
        Me.txtYPmwb.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPmwb.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPmwb.Location = New System.Drawing.Point(259, 129)
        Me.txtYPmwb.MaxLength = 0
        Me.txtYPmwb.Name = "txtYPmwb"
        Me.txtYPmwb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPmwb.Size = New System.Drawing.Size(97, 23)
        Me.txtYPmwb.TabIndex = 52
        Me.txtYPmwb.Text = "1.18"
        '
        'txtYPdp
        '
        Me.txtYPdp.AcceptsReturn = True
        Me.txtYPdp.AutoSize = False
        Me.txtYPdp.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPdp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPdp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPdp.Location = New System.Drawing.Point(259, 86)
        Me.txtYPdp.MaxLength = 0
        Me.txtYPdp.Name = "txtYPdp"
        Me.txtYPdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPdp.Size = New System.Drawing.Size(97, 23)
        Me.txtYPdp.TabIndex = 51
        Me.txtYPdp.Text = "127.0"
        '
        'txtYPdh
        '
        Me.txtYPdh.AcceptsReturn = True
        Me.txtYPdh.AutoSize = False
        Me.txtYPdh.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPdh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPdh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPdh.Location = New System.Drawing.Point(259, 52)
        Me.txtYPdh.MaxLength = 0
        Me.txtYPdh.Name = "txtYPdh"
        Me.txtYPdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPdh.Size = New System.Drawing.Size(97, 22)
        Me.txtYPdh.TabIndex = 50
        Me.txtYPdh.Text = "215.9"
        '
        'txtYPyp
        '
        Me.txtYPyp.AcceptsReturn = True
        Me.txtYPyp.AutoSize = False
        Me.txtYPyp.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPyp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPyp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPyp.Location = New System.Drawing.Point(259, 17)
        Me.txtYPyp.MaxLength = 0
        Me.txtYPyp.Name = "txtYPyp"
        Me.txtYPyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPyp.Size = New System.Drawing.Size(97, 23)
        Me.txtYPyp.TabIndex = 49
        Me.txtYPyp.Text = "12"
        '
        'Line3
        '
        Me.Line3.BackColor = System.Drawing.Color.Blue
        Me.Line3.Location = New System.Drawing.Point(5, 202)
        Me.Line3.Name = "Line3"
        Me.Line3.Size = New System.Drawing.Size(403, 2)
        Me.Line3.TabIndex = 55
        '
        'Line2
        '
        Me.Line2.BackColor = System.Drawing.Color.Blue
        Me.Line2.Location = New System.Drawing.Point(5, 159)
        Me.Line2.Name = "Line2"
        Me.Line2.Size = New System.Drawing.Size(403, 1)
        Me.Line2.TabIndex = 56
        '
        'Line1
        '
        Me.Line1.BackColor = System.Drawing.Color.Blue
        Me.Line1.Location = New System.Drawing.Point(5, 116)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(408, 1)
        Me.Line1.TabIndex = 57
        '
        'lblYPh
        '
        Me.lblYPh.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPh.Location = New System.Drawing.Point(43, 215)
        Me.lblYPh.Name = "lblYPh"
        Me.lblYPh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPh.Size = New System.Drawing.Size(131, 19)
        Me.lblYPh.TabIndex = 48
        Me.lblYPh.Text = "井段长度(m)"
        '
        'lblYPmwh
        '
        Me.lblYPmwh.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPmwh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPmwh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPmwh.Location = New System.Drawing.Point(38, 172)
        Me.lblYPmwh.Name = "lblYPmwh"
        Me.lblYPmwh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPmwh.Size = New System.Drawing.Size(170, 23)
        Me.lblYPmwh.TabIndex = 47
        Me.lblYPmwh.Text = "环空钻井液密度(g/cm^3)"
        '
        'lblYPmwb
        '
        Me.lblYPmwb.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPmwb.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPmwb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPmwb.Location = New System.Drawing.Point(38, 127)
        Me.lblYPmwb.Name = "lblYPmwb"
        Me.lblYPmwb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPmwb.Size = New System.Drawing.Size(141, 31)
        Me.lblYPmwb.TabIndex = 46
        Me.lblYPmwb.Text = "平衡地层压力所需的钻井液密度(g/cm^3)"
        '
        'lblYPdp
        '
        Me.lblYPdp.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPdp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPdp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPdp.Location = New System.Drawing.Point(38, 88)
        Me.lblYPdp.Name = "lblYPdp"
        Me.lblYPdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPdp.Size = New System.Drawing.Size(131, 23)
        Me.lblYPdp.TabIndex = 45
        Me.lblYPdp.Text = "钻杆直径(mm)"
        '
        'lblYPdh
        '
        Me.lblYPdh.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPdh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPdh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPdh.Location = New System.Drawing.Point(38, 55)
        Me.lblYPdh.Name = "lblYPdh"
        Me.lblYPdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPdh.Size = New System.Drawing.Size(131, 23)
        Me.lblYPdh.TabIndex = 44
        Me.lblYPdh.Text = "井眼直径(mm)"
        '
        'lblYPyp
        '
        Me.lblYPyp.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPyp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPyp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPyp.Location = New System.Drawing.Point(38, 22)
        Me.lblYPyp.Name = "lblYPyp"
        Me.lblYPyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPyp.Size = New System.Drawing.Size(131, 22)
        Me.lblYPyp.TabIndex = 43
        Me.lblYPyp.Text = "钻井液屈服值(Pa)"
        '
        'fraYPmw
        '
        Me.fraYPmw.BackColor = System.Drawing.SystemColors.Control
        Me.fraYPmw.Controls.Add(Me.txtYPmw)
        Me.fraYPmw.Controls.Add(Me.lblYPmw)
        Me.fraYPmw.ForeColor = System.Drawing.Color.Red
        Me.fraYPmw.Location = New System.Drawing.Point(446, 125)
        Me.fraYPmw.Name = "fraYPmw"
        Me.fraYPmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraYPmw.Size = New System.Drawing.Size(386, 87)
        Me.fraYPmw.TabIndex = 37
        Me.fraYPmw.TabStop = False
        Me.fraYPmw.Text = "经验计算1"
        '
        'txtYPmw
        '
        Me.txtYPmw.AcceptsReturn = True
        Me.txtYPmw.AutoSize = False
        Me.txtYPmw.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(192, Byte), CType(192, Byte))
        Me.txtYPmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPmw.Location = New System.Drawing.Point(154, 56)
        Me.txtYPmw.MaxLength = 0
        Me.txtYPmw.Name = "txtYPmw"
        Me.txtYPmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPmw.Size = New System.Drawing.Size(102, 24)
        Me.txtYPmw.TabIndex = 56
        Me.txtYPmw.Text = ""
        '
        'lblYPmw
        '
        Me.lblYPmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPmw.Location = New System.Drawing.Point(67, 19)
        Me.lblYPmw.Name = "lblYPmw"
        Me.lblYPmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPmw.Size = New System.Drawing.Size(275, 30)
        Me.lblYPmw.TabIndex = 55
        Me.lblYPmw.Text = "起钻时克服抽汲作用的安全钻井液密度(g/cm^3)"
        Me.lblYPmw.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'fraYPECD
        '
        Me.fraYPECD.BackColor = System.Drawing.SystemColors.Control
        Me.fraYPECD.Controls.Add(Me.txtYPECD)
        Me.fraYPECD.Controls.Add(Me.lblYPECD)
        Me.fraYPECD.ForeColor = System.Drawing.Color.Red
        Me.fraYPECD.Location = New System.Drawing.Point(446, 220)
        Me.fraYPECD.Name = "fraYPECD"
        Me.fraYPECD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraYPECD.Size = New System.Drawing.Size(386, 96)
        Me.fraYPECD.TabIndex = 38
        Me.fraYPECD.TabStop = False
        Me.fraYPECD.Text = "经验计算2"
        '
        'txtYPECD
        '
        Me.txtYPECD.AcceptsReturn = True
        Me.txtYPECD.AutoSize = False
        Me.txtYPECD.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtYPECD.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPECD.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPECD.Location = New System.Drawing.Point(158, 60)
        Me.txtYPECD.MaxLength = 0
        Me.txtYPECD.Name = "txtYPECD"
        Me.txtYPECD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPECD.Size = New System.Drawing.Size(98, 23)
        Me.txtYPECD.TabIndex = 58
        Me.txtYPECD.Text = ""
        '
        'lblYPECD
        '
        Me.lblYPECD.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPECD.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPECD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPECD.Location = New System.Drawing.Point(82, 26)
        Me.lblYPECD.Name = "lblYPECD"
        Me.lblYPECD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPECD.Size = New System.Drawing.Size(246, 31)
        Me.lblYPECD.TabIndex = 57
        Me.lblYPECD.Text = "由屈服值估算当量循环密度ECD (g/cm^3)"
        Me.lblYPECD.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'fraYPPann
        '
        Me.fraYPPann.BackColor = System.Drawing.SystemColors.Control
        Me.fraYPPann.Controls.Add(Me.txtYPPann)
        Me.fraYPPann.Controls.Add(Me.lblYPPann)
        Me.fraYPPann.ForeColor = System.Drawing.Color.Red
        Me.fraYPPann.Location = New System.Drawing.Point(446, 323)
        Me.fraYPPann.Name = "fraYPPann"
        Me.fraYPPann.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraYPPann.Size = New System.Drawing.Size(386, 96)
        Me.fraYPPann.TabIndex = 39
        Me.fraYPPann.TabStop = False
        Me.fraYPPann.Text = "经验计算3"
        '
        'txtYPPann
        '
        Me.txtYPPann.AcceptsReturn = True
        Me.txtYPPann.AutoSize = False
        Me.txtYPPann.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(192, Byte), CType(255, Byte))
        Me.txtYPPann.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPPann.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPPann.Location = New System.Drawing.Point(158, 56)
        Me.txtYPPann.MaxLength = 0
        Me.txtYPPann.Name = "txtYPPann"
        Me.txtYPPann.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPPann.Size = New System.Drawing.Size(102, 24)
        Me.txtYPPann.TabIndex = 60
        Me.txtYPPann.Text = ""
        '
        'lblYPPann
        '
        Me.lblYPPann.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPPann.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPPann.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPPann.Location = New System.Drawing.Point(72, 17)
        Me.lblYPPann.Name = "lblYPPann"
        Me.lblYPPann.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPPann.Size = New System.Drawing.Size(275, 30)
        Me.lblYPPann.TabIndex = 59
        Me.lblYPPann.Text = "由屈服值估算环形空间压力损失(层流时) (MPa)"
        Me.lblYPPann.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdYPOk
        '
        Me.cmdYPOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdYPOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdYPOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdYPOk.Location = New System.Drawing.Point(86, 388)
        Me.cmdYPOk.Name = "cmdYPOk"
        Me.cmdYPOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdYPOk.Size = New System.Drawing.Size(69, 22)
        Me.cmdYPOk.TabIndex = 40
        Me.cmdYPOk.Text = "计  算"
        '
        'cmdYPClear
        '
        Me.cmdYPClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdYPClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdYPClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdYPClear.Location = New System.Drawing.Point(187, 388)
        Me.cmdYPClear.Name = "cmdYPClear"
        Me.cmdYPClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdYPClear.Size = New System.Drawing.Size(69, 22)
        Me.cmdYPClear.TabIndex = 41
        Me.cmdYPClear.Text = "清  除"
        '
        'cmdYPQuit
        '
        Me.cmdYPQuit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdYPQuit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdYPQuit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdYPQuit.Location = New System.Drawing.Point(288, 388)
        Me.cmdYPQuit.Name = "cmdYPQuit"
        Me.cmdYPQuit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdYPQuit.Size = New System.Drawing.Size(68, 22)
        Me.cmdYPQuit.TabIndex = 42
        Me.cmdYPQuit.Text = "返  回"
        '
        'frmBasePressureCal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 588)
        Me.Controls.Add(Me.SSTab1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmBasePressureCal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Base Pressure Calculation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SSTab1.ResumeLayout(False)
        Me._SSTab1_TabPage0.ResumeLayout(False)
        Me.fraAnnularMw.ResumeLayout(False)
        Me.fraAnnularMwInput.ResumeLayout(False)
        Me.fraAnnularMwOutput.ResumeLayout(False)
        Me._SSTab1_TabPage1.ResumeLayout(False)
        Me.fraMw.ResumeLayout(False)
        Me.fraMwInput.ResumeLayout(False)
        Me.fraMwOutput.ResumeLayout(False)
        Me._SSTab1_TabPage2.ResumeLayout(False)
        Me.fraYPprogram.ResumeLayout(False)
        Me.fraYPinputdata.ResumeLayout(False)
        Me.fraYPmw.ResumeLayout(False)
        Me.fraYPECD.ResumeLayout(False)
        Me.fraYPPann.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmBasePressureCal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmBasePressureCal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmBasePressureCal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
    '程序界面英文化，时间：2012年4月15日，第三个屈服值经验计算未完成，不保证计算结果有正确。
	
	
	
	Const g As Double = 9.81
	Const C6 As Double = 1.475
	Const C7 As Double = 0.275
	
	Dim lostmw As Single
	Dim losthwell As Single
	Dim lostpressure As Single
	Dim Plm As Single
	Dim openmw As Single
	Dim openpm As Single
	
	Dim casemw As Single
	Dim casehwell As Single
	Dim casepressure As Single
	Dim Pcm As Single
	Dim outputa As Single
	Dim outputmw As Single
	
	Dim yp As Single
	Dim dh As Single
	Dim dp As Single
	Dim MWb As Single
	Dim MWh As Single
	Dim H As Single
	Dim MWp As Single
	Dim ECD As Single
	Dim Pann As Single
	
	Dim buttons As MsgBoxStyle
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	
	Sub Quit() '编写的退出请示函数
        buttons = MsgBox("你确实要退出吗？", 65, "警告 ")
		
		If buttons = 1 Then
			Me.Close()
		Else
			If buttons = 2 Then
				Exit Sub
			End If
		End If
	End Sub
	
	
	Private Sub cmdAnnularMwclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAnnularMwclear.Click
		txtAnnularMwoutmw.Text = ""
		txtAnnularMwoutmwpm.Text = ""
		txtAnnularMwhwell.Text = ""
		txtAnnularMwmw.Text = ""
		txtAnnularMwpressure.Text = ""
	End Sub
	
	Private Sub cmdAnnularMwok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAnnularMwok.Click
		lostmw = Val(txtAnnularMwmw.Text)
		If lostmw < 1# Then
            MsgBox("请检查你输入的‘试验用钻井液密度’数据是否正确有效！", MsgBoxStyle.Information, "漏失试验")
			txtAnnularMwmw.Text = ""
			Exit Sub
		End If
		
		losthwell = Val(txtAnnularMwhwell.Text)
		If lostmw <= 0 Then
            MsgBox("请检查你输入的‘实验井深’数据是否正确有效！", MsgBoxStyle.Information, "漏失试验")
			txtAnnularMwhwell.Text = ""
			Exit Sub
		End If
		lostpressure = Val(txtAnnularMwpressure.Text)
		If lostmw <= 0 Then
            MsgBox("请检查你输入的‘漏失压力’数据是否正确有效！", MsgBoxStyle.Information, "漏失试验")
			txtAnnularMwpressure.Text = ""
			Exit Sub
		End If
		
		Plm = 10 ^ -3 * lostmw * g * losthwell + lostpressure
		openmw = (10 ^ 3 * Plm) / (g * losthwell)
		openpm = Plm / losthwell
		
		txtAnnularMwoutmw.Text = VB6.Format(openmw, "0.00")
		txtAnnularMwoutmwpm.Text = VB6.Format(openpm, "0.00000")
	End Sub
	
	Private Sub cmdAnnularMwquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAnnularMwquit.Click
		Quit()
	End Sub
	
	Private Sub cmdMwClear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMwClear.Click
		txtMudmw.Text = ""
		txtMudcasePressure.Text = ""
		txtMudcasehwell.Text = ""
		txtMudoutputa.Text = ""
		txtMudoutputmw.Text = ""
		
	End Sub
	
	Private Sub cmdMwOk_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMwOk.Click
		casemw = Val(txtMudmw.Text)
		If casemw <= 0 Then
            MsgBox("请检查你输入的‘钻井液密度’数据是否正确有效！", MsgBoxStyle.Information, "钻井液当量密度")
			txtMudmw.Text = ""
			Exit Sub
		End If
		
		casepressure = Val(txtMudcasePressure.Text)
        If casepressure < 0 Then
            MsgBox("请检查你输入的‘关井套压’数据是否正确有效！", MsgBoxStyle.Information, "钻井液当量密度")
            txtMudcasePressure.Text = ""
            Exit Sub
        End If

        casehwell = Val(txtMudcasehwell.Text)
        If casehwell <= 0 Then
            MsgBox("请检查你输入的‘套管深度’数据是否正确有效！", MsgBoxStyle.Information, "钻井液当量密度")
            txtMudcasehwell.Text = ""
            Exit Sub
        End If

        Pcm = 10 ^ -3 * casemw * g * casehwell + casepressure
        outputmw = (10 ^ 3 * Pcm) / (g * casehwell)
        outputa = (10 ^ 3 * casepressure) / (g * casehwell)

        txtMudoutputa.Text = VB6.Format(outputa, "0.000")
        txtMudoutputmw.Text = VB6.Format(outputmw, "0.000")
	End Sub
	
	Private Sub cmdMwQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMwQuit.Click
		Quit()
	End Sub
	
	Private Sub cmdYPClear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdYPClear.Click
		txtYPyp.Text = ""
		txtYPdh.Text = ""
		txtYPdp.Text = ""
		txtYPmwb.Text = ""
		txtYPmwh.Text = ""
		txtYPh.Text = ""
		
		txtYPmw.Text = ""
		txtYPECD.Text = ""
		txtYPPann.Text = ""
	End Sub
	
	Private Sub cmdYPOk_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdYPOk.Click
		yp = Val(txtYPyp.Text)
		If yp <= 0 Then
			MsgBox("请检查你输入的‘钻井液屈服值’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPyp.Text = ""
			Exit Sub
		End If
		
		dh = Val(txtYPdh.Text)
		If dh <= 0 Then
			MsgBox("请检查你输入的‘井眼直径’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPdh.Text = ""
			Exit Sub
		End If
		dp = Val(txtYPdp.Text)
		If dp <= 0 Or dp >= dh Then
			MsgBox("请检查你输入的‘钻杆直径’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPdp.Text = ""
			Exit Sub
		End If
		
		MWb = Val(txtYPmwb.Text)
		If MWb <= 1# Then
			MsgBox("请检查你输入的‘平衡地层压力所需的钻井液密度’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPmwb.Text = ""
			Exit Sub
		End If
		
		MWh = Val(txtYPmwh.Text)
		If MWh <= 1# Then
			MsgBox("请检查你输入的‘井眼环空钻井液密度’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPmwh.Text = ""
			Exit Sub
		End If
		
		H = Val(txtYPh.Text)
		If H <= 0 Then
			MsgBox("请检查你输入的‘井段长度’数据是否正确有效！", MsgBoxStyle.Information, "利用屈服值进行的经验计算")
			txtYPh.Text = ""
			Exit Sub
		End If
		
		MWp = (yp * C6) / (11.7 * (dh - dp)) + MWb
		ECD = (yp * C6) / (10 * (dh - dp)) + MWh
		Pann = (H * yp * C7) / (255 * (dh - dp))
		
		txtYPmw.Text = VB6.Format(MWp, "0.000")
		txtYPECD.Text = VB6.Format(ECD, "0.000")
		txtYPPann.Text = VB6.Format(Pann, "0.000")
		
	End Sub
	
	Private Sub cmdYPQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdYPQuit.Click
		Quit()
	End Sub
	
	Private Sub frmBasePressureCal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
		Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		Call PicGWDC(PicLogoECD)
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdAnnularMwquit.Visible = False '双击logo 图案开始截图，设置命令按钮隐藏'
		cmdAnnularMwok.Visible = False
		cmdAnnularMwclear.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)

        'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”

		
		'cmdPicSave.Visible = True '作废 的控件

        'https://oomake.com/question/4484292  来源编码   王朝 20210930
        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MLeakoff.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("MLeakoff.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        cmdAnnularMwquit.Visible = True
        cmdAnnularMwok.Visible = True
        cmdAnnularMwclear.Visible = True
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogoECD_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogoECD.DoubleClick
		cmdMwQuit.Visible = False '双击logo 图案开始截图，设置命令按钮隐藏'
		cmdMwOk.Visible = False
		cmdMwClear.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)

        'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”



        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MEMW.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("MLeakoff.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If



        'cmdPicSave.Visible = True '作废 的控件

        cmdMwQuit.Visible = True
        cmdMwOk.Visible = True
        cmdMwClear.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtAnnularMwhwell_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtAnnularMwhwell.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtAnnularMwhwell_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAnnularMwhwell.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		losthwell = Val(txtAnnularMwhwell.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtAnnularMwmw_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtAnnularMwmw.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtAnnularMwmw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAnnularMwmw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lostmw = Val(txtAnnularMwmw.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtAnnularMwpressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtAnnularMwpressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtAnnularMwpressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAnnularMwpressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lostpressure = Val(txtAnnularMwpressure.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMudcasehwell_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMudcasehwell.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMudcasehwell_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMudcasehwell.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		casehwell = Val(txtMudcasehwell.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMudcasePressure_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMudcasePressure.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMudcasePressure_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMudcasePressure.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		casepressure = Val(txtMudcasePressure.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMudmw_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMudmw.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMudmw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMudmw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		casemw = Val(txtMudmw.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPdh_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPdh.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPdh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPdh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dh = Val(txtYPdh.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPdp_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPdp.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPdp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPdp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dp = Val(txtYPdp.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPh_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPh.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		H = Val(txtYPh.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPmwb_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPmwb.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPmwb_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPmwb.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		MWb = Val(txtYPmwb.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPmwh_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPmwh.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPmwh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPmwh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		MWh = Val(txtYPmwh.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtYPyp_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtYPyp.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtYPyp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPyp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		yp = Val(txtYPyp.Text)
	End Sub

    
End Class