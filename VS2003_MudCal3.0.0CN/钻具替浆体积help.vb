Option Strict Off
Option Explicit On
Friend Class frmHelpDisplace
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmHelpDisplace))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdHelp.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(203, 336)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(80, 20)
        Me.CmdHelp.TabIndex = 1
        Me.CmdHelp.Text = "确 定"
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtHelp.Location = New System.Drawing.Point(0, 0)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.Size = New System.Drawing.Size(473, 315)
        Me.txtHelp.TabIndex = 0
        Me.txtHelp.Text = ""
        '
        'frmHelpDisplace
        '
        Me.AcceptButton = Me.CmdHelp
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 16)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.CmdHelp
        Me.ClientSize = New System.Drawing.Size(475, 375)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.txtHelp)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(3, 29)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHelpDisplace"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "钻具替浆体积模块使用说明"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmHelpDisplace
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmHelpDisplace
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmHelpDisplace()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Private Sub frmHelpDisplace_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		Dim CRLF As String
		CRLF = Chr(13) & Chr(10)
		
		Dim Msg As String
		Msg = CRLF & "                  钻具替浆体积计算模块使用说明" & CRLF & CRLF
		Msg = Msg & "1.本模块可实现：钻具每100米替浆体积、整套钻具替浆体积总量计算。 " & CRLF & CRLF
		Msg = Msg & "2.当钻具数据控件中只有外径有数值而内径无数值时，计算的体积大小为一个直径大小等于钻具外径的实心圆柱体积；当外径和内径均有数值时，计算出的体积大小是一个环空圆环柱体（即厚壁环圆柱体）的体积；当对应的钻具长度有数值时，此时计算出的体积为该套钻具组合的总替浆体积。" & CRLF & CRLF
		Msg = Msg & "3.浅黄色文本框为计算出的对应钻具的每100米替浆体积量。" & CRLF & CRLF
		Msg = Msg & "4.浅绿色文本框为可选择输入的钻具长度项，若要计算整套钻具的替浆体积量，则浅绿色长度文本框为必须输入的内容，否则计算结果中显示的替换体积量大小为零。 " & CRLF & CRLF
		Msg = Msg & "    本模块的数据输入窗口模仿胜利油田钻井工艺研究院钻井信息中心《钻井工程计算软件包 V1.2》形式设计，特此声明。" & CRLF
		
		
		txtHelp.Text = Msg
		
	End Sub
	
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		Me.Close()
	End Sub
End Class