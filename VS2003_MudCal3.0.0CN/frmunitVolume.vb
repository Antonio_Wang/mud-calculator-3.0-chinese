Option Strict Off
Option Explicit On
Friend Class frmunitVolume
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents txtVolumebushel As System.Windows.Forms.TextBox
	Public WithEvents txtVolumeyd3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumeft3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumeinch3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumemm3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumecm3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumedm3 As System.Windows.Forms.TextBox
	Public WithEvents txtVolumem3 As System.Windows.Forms.TextBox
	Public WithEvents lblVolumebush As System.Windows.Forms.Label
	Public WithEvents lblVolumeyd3 As System.Windows.Forms.Label
	Public WithEvents lblVolumeft3 As System.Windows.Forms.Label
	Public WithEvents lblVolumein3 As System.Windows.Forms.Label
	Public WithEvents lblVolumemm3 As System.Windows.Forms.Label
	Public WithEvents lblVolumecm3 As System.Windows.Forms.Label
	Public WithEvents lblVolumedm3 As System.Windows.Forms.Label
	Public WithEvents lblVolumem3 As System.Windows.Forms.Label
	Public WithEvents lblVolume As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.txtVolumebushel = New System.Windows.Forms.TextBox
        Me.txtVolumeyd3 = New System.Windows.Forms.TextBox
        Me.txtVolumeft3 = New System.Windows.Forms.TextBox
        Me.txtVolumeinch3 = New System.Windows.Forms.TextBox
        Me.txtVolumemm3 = New System.Windows.Forms.TextBox
        Me.txtVolumecm3 = New System.Windows.Forms.TextBox
        Me.txtVolumedm3 = New System.Windows.Forms.TextBox
        Me.txtVolumem3 = New System.Windows.Forms.TextBox
        Me.lblVolumebush = New System.Windows.Forms.Label
        Me.lblVolumeyd3 = New System.Windows.Forms.Label
        Me.lblVolumeft3 = New System.Windows.Forms.Label
        Me.lblVolumein3 = New System.Windows.Forms.Label
        Me.lblVolumemm3 = New System.Windows.Forms.Label
        Me.lblVolumecm3 = New System.Windows.Forms.Label
        Me.lblVolumedm3 = New System.Windows.Forms.Label
        Me.lblVolumem3 = New System.Windows.Forms.Label
        Me.lblVolume = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(106, 190)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 26)
        Me.cmdquit.TabIndex = 18
        Me.cmdquit.Text = "退出"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(288, 190)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 26)
        Me.cmdclear.TabIndex = 17
        Me.cmdclear.Text = "清除"
        '
        'txtVolumebushel
        '
        Me.txtVolumebushel.AcceptsReturn = True
        Me.txtVolumebushel.AutoSize = False
        Me.txtVolumebushel.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumebushel.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumebushel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumebushel.Location = New System.Drawing.Point(240, 155)
        Me.txtVolumebushel.MaxLength = 0
        Me.txtVolumebushel.Name = "txtVolumebushel"
        Me.txtVolumebushel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumebushel.Size = New System.Drawing.Size(97, 19)
        Me.txtVolumebushel.TabIndex = 12
        Me.txtVolumebushel.Text = ""
        '
        'txtVolumeyd3
        '
        Me.txtVolumeyd3.AcceptsReturn = True
        Me.txtVolumeyd3.AutoSize = False
        Me.txtVolumeyd3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumeyd3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumeyd3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumeyd3.Location = New System.Drawing.Point(240, 121)
        Me.txtVolumeyd3.MaxLength = 0
        Me.txtVolumeyd3.Name = "txtVolumeyd3"
        Me.txtVolumeyd3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumeyd3.Size = New System.Drawing.Size(97, 19)
        Me.txtVolumeyd3.TabIndex = 11
        Me.txtVolumeyd3.Text = ""
        '
        'txtVolumeft3
        '
        Me.txtVolumeft3.AcceptsReturn = True
        Me.txtVolumeft3.AutoSize = False
        Me.txtVolumeft3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumeft3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumeft3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumeft3.Location = New System.Drawing.Point(240, 86)
        Me.txtVolumeft3.MaxLength = 0
        Me.txtVolumeft3.Name = "txtVolumeft3"
        Me.txtVolumeft3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumeft3.Size = New System.Drawing.Size(97, 20)
        Me.txtVolumeft3.TabIndex = 10
        Me.txtVolumeft3.Text = ""
        '
        'txtVolumeinch3
        '
        Me.txtVolumeinch3.AcceptsReturn = True
        Me.txtVolumeinch3.AutoSize = False
        Me.txtVolumeinch3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumeinch3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumeinch3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumeinch3.Location = New System.Drawing.Point(240, 52)
        Me.txtVolumeinch3.MaxLength = 0
        Me.txtVolumeinch3.Name = "txtVolumeinch3"
        Me.txtVolumeinch3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumeinch3.Size = New System.Drawing.Size(97, 19)
        Me.txtVolumeinch3.TabIndex = 9
        Me.txtVolumeinch3.Text = ""
        '
        'txtVolumemm3
        '
        Me.txtVolumemm3.AcceptsReturn = True
        Me.txtVolumemm3.AutoSize = False
        Me.txtVolumemm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumemm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumemm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumemm3.Location = New System.Drawing.Point(10, 155)
        Me.txtVolumemm3.MaxLength = 0
        Me.txtVolumemm3.Name = "txtVolumemm3"
        Me.txtVolumemm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumemm3.Size = New System.Drawing.Size(87, 19)
        Me.txtVolumemm3.TabIndex = 4
        Me.txtVolumemm3.Text = ""
        '
        'txtVolumecm3
        '
        Me.txtVolumecm3.AcceptsReturn = True
        Me.txtVolumecm3.AutoSize = False
        Me.txtVolumecm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumecm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumecm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumecm3.Location = New System.Drawing.Point(10, 121)
        Me.txtVolumecm3.MaxLength = 0
        Me.txtVolumecm3.Name = "txtVolumecm3"
        Me.txtVolumecm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumecm3.Size = New System.Drawing.Size(87, 19)
        Me.txtVolumecm3.TabIndex = 3
        Me.txtVolumecm3.Text = ""
        '
        'txtVolumedm3
        '
        Me.txtVolumedm3.AcceptsReturn = True
        Me.txtVolumedm3.AutoSize = False
        Me.txtVolumedm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumedm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumedm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumedm3.Location = New System.Drawing.Point(10, 86)
        Me.txtVolumedm3.MaxLength = 0
        Me.txtVolumedm3.Name = "txtVolumedm3"
        Me.txtVolumedm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumedm3.Size = New System.Drawing.Size(87, 20)
        Me.txtVolumedm3.TabIndex = 2
        Me.txtVolumedm3.Text = ""
        '
        'txtVolumem3
        '
        Me.txtVolumem3.AcceptsReturn = True
        Me.txtVolumem3.AutoSize = False
        Me.txtVolumem3.BackColor = System.Drawing.SystemColors.Window
        Me.txtVolumem3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVolumem3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVolumem3.Location = New System.Drawing.Point(10, 52)
        Me.txtVolumem3.MaxLength = 0
        Me.txtVolumem3.Name = "txtVolumem3"
        Me.txtVolumem3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVolumem3.Size = New System.Drawing.Size(87, 19)
        Me.txtVolumem3.TabIndex = 1
        Me.txtVolumem3.Text = ""
        '
        'lblVolumebush
        '
        Me.lblVolumebush.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumebush.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumebush.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumebush.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumebush.Location = New System.Drawing.Point(355, 155)
        Me.lblVolumebush.Name = "lblVolumebush"
        Me.lblVolumebush.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumebush.Size = New System.Drawing.Size(117, 18)
        Me.lblVolumebush.TabIndex = 16
        Me.lblVolumebush.Text = "bushel(浦式耳)"
        '
        'lblVolumeyd3
        '
        Me.lblVolumeyd3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumeyd3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumeyd3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumeyd3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumeyd3.Location = New System.Drawing.Point(355, 121)
        Me.lblVolumeyd3.Name = "lblVolumeyd3"
        Me.lblVolumeyd3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumeyd3.Size = New System.Drawing.Size(117, 18)
        Me.lblVolumeyd3.TabIndex = 15
        Me.lblVolumeyd3.Text = "yd3(立方码)"
        '
        'lblVolumeft3
        '
        Me.lblVolumeft3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumeft3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumeft3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumeft3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumeft3.Location = New System.Drawing.Point(355, 86)
        Me.lblVolumeft3.Name = "lblVolumeft3"
        Me.lblVolumeft3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumeft3.Size = New System.Drawing.Size(117, 18)
        Me.lblVolumeft3.TabIndex = 14
        Me.lblVolumeft3.Text = "ft3(立方英尺)"
        '
        'lblVolumein3
        '
        Me.lblVolumein3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumein3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumein3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumein3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumein3.Location = New System.Drawing.Point(355, 52)
        Me.lblVolumein3.Name = "lblVolumein3"
        Me.lblVolumein3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumein3.Size = New System.Drawing.Size(117, 18)
        Me.lblVolumein3.TabIndex = 13
        Me.lblVolumein3.Text = "in3(立方英寸)"
        '
        'lblVolumemm3
        '
        Me.lblVolumemm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumemm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumemm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumemm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumemm3.Location = New System.Drawing.Point(115, 155)
        Me.lblVolumemm3.Name = "lblVolumemm3"
        Me.lblVolumemm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumemm3.Size = New System.Drawing.Size(107, 18)
        Me.lblVolumemm3.TabIndex = 8
        Me.lblVolumemm3.Text = "mm3(立方毫米)"
        '
        'lblVolumecm3
        '
        Me.lblVolumecm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumecm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumecm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumecm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumecm3.Location = New System.Drawing.Point(115, 121)
        Me.lblVolumecm3.Name = "lblVolumecm3"
        Me.lblVolumecm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumecm3.Size = New System.Drawing.Size(107, 18)
        Me.lblVolumecm3.TabIndex = 7
        Me.lblVolumecm3.Text = "cm3(立方厘米)"
        '
        'lblVolumedm3
        '
        Me.lblVolumedm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumedm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumedm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumedm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumedm3.Location = New System.Drawing.Point(115, 86)
        Me.lblVolumedm3.Name = "lblVolumedm3"
        Me.lblVolumedm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumedm3.Size = New System.Drawing.Size(107, 18)
        Me.lblVolumedm3.TabIndex = 6
        Me.lblVolumedm3.Text = "dm3(立方分米)"
        '
        'lblVolumem3
        '
        Me.lblVolumem3.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolumem3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolumem3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolumem3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolumem3.Location = New System.Drawing.Point(115, 52)
        Me.lblVolumem3.Name = "lblVolumem3"
        Me.lblVolumem3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolumem3.Size = New System.Drawing.Size(107, 18)
        Me.lblVolumem3.TabIndex = 5
        Me.lblVolumem3.Text = "m3(立方米)"
        '
        'lblVolume
        '
        Me.lblVolume.BackColor = System.Drawing.SystemColors.Control
        Me.lblVolume.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVolume.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblVolume.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVolume.Location = New System.Drawing.Point(10, 9)
        Me.lblVolume.Name = "lblVolume"
        Me.lblVolume.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVolume.Size = New System.Drawing.Size(279, 18)
        Me.lblVolume.TabIndex = 0
        Me.lblVolume.Text = "输入体积单位换算数据："
        '
        'frmunitVolume
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(484, 229)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.txtVolumebushel)
        Me.Controls.Add(Me.txtVolumeyd3)
        Me.Controls.Add(Me.txtVolumeft3)
        Me.Controls.Add(Me.txtVolumeinch3)
        Me.Controls.Add(Me.txtVolumemm3)
        Me.Controls.Add(Me.txtVolumecm3)
        Me.Controls.Add(Me.txtVolumedm3)
        Me.Controls.Add(Me.txtVolumem3)
        Me.Controls.Add(Me.lblVolumebush)
        Me.Controls.Add(Me.lblVolumeyd3)
        Me.Controls.Add(Me.lblVolumeft3)
        Me.Controls.Add(Me.lblVolumein3)
        Me.Controls.Add(Me.lblVolumemm3)
        Me.Controls.Add(Me.lblVolumecm3)
        Me.Controls.Add(Me.lblVolumedm3)
        Me.Controls.Add(Me.lblVolumem3)
        Me.Controls.Add(Me.lblVolume)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitVolume"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "体积(Volume)单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitVolume
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitVolume
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitVolume()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月11日13:43于青海省海西洲马海马北油田
	'体积单位换算
	
	
	Dim m3 As Single
	Dim dm3 As Single
	Dim cm3 As Single
	Dim mm3 As Single
	
	Dim Inch3 As Single
	Dim ft3 As Single
	Dim yd3 As Single
	Dim bushel As Single
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitVolume.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtVolumecm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumecm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方厘米cublic centimetre－－输入数据的计算过程
		cm3 = Val(txtVolumecm3.Text)
		
		
		m3 = cm3 * 0.000001
		dm3 = cm3 * 0.001
		mm3 = cm3 * 1000#
		
		Inch3 = cm3 * 0.061 '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = cm3 * 0.000035315 '1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = cm3 * 0.000001308 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = cm3 * 0.000001 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumemm3.Text = CStr(mm3)
		
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	Private Sub txtVolumedm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumedm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方分米－－输入数据的计算过程
		dm3 = Val(txtVolumedm3.Text)
		
		m3 = dm3 * 0.001
		cm3 = dm3 * 1000#
		mm3 = dm3 * 1000000#
		
		Inch3 = dm3 * 61.0237 '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = dm3 * 0.0353 '1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = dm3 * 0.0013 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = dm3 * 0.001 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		
		
		txtVolumem3.Text = CStr(m3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	Private Sub txtVolumeft3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumeft3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方英尺－－输入数据的计算过程
		ft3 = Val(txtVolumeft3.Text)
		
		m3 = ft3 * 0.0283
		dm3 = ft3 * 28.3169
		cm3 = ft3 * 28316#
		mm3 = ft3 * 28316000#
		
		Inch3 = ft3 * 1728# '1 in3=16.4cm3     1cm3=0.610in3
		'1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = ft3 * 0.037 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = ft3 * 0.0283 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	
	Private Sub txtVolumeinch3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumeinch3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方英寸－－输入数据的计算过程
		Inch3 = Val(txtVolumeinch3.Text)
		
		m3 = Inch3 * 0.000016387
		dm3 = Inch3 * 0.0164
		cm3 = Inch3 * 16.3871
		mm3 = Inch3 * 16387#
		ft3 = Inch3 * 0.0005787 '1 in3=16.4cm3     1cm3=0.610in3
		'1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = Inch3 * 0.000021433 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = Inch3 * 0.000016387 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	Private Sub txtVolumem3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumem3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方米－－输入数据的计算过程
		m3 = Val(txtVolumem3.Text)
		
		dm3 = m3 * 1000#
		cm3 = m3 * 1000000#
		mm3 = m3 * 1000000000#
		
		Inch3 = m3 * 61024# '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = m3 * 35.3147 '1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = m3 * 1.308 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = m3 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	Private Sub txtVolumemm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumemm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '立方毫米－－输入数据的计算过程
		mm3 = Val(txtVolumemm3.Text)
		
		m3 = mm3 * 0.000000001
		dm3 = mm3 * 0.000001
		cm3 = mm3 * 0.001
		
		Inch3 = mm3 * 0.000061024 '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = mm3 * 0.000000035315 '1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = mm3 * 0.000000001308 '1 yd3=0.765m3     1m3=1.309yd3
		bushel = mm3 * 0.000000001 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
	
	Private Sub txtVolumebushel_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumebushel.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '浦式耳－－－－输入数据的计算过程
		bushel = Val(txtVolumebushel.Text)
		
		m3 = bushel * 0.0346
		dm3 = bushel * 0.0346 * 1000
		cm3 = bushel * 0.0346 * 1000000#
		mm3 = bushel * 0.0346 * 1000000000#
		
		Inch3 = bushel * 0.0346 * 61024# '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = bushel * 0.0346 * 35.3147 '1 ft3=0.0283m3    1m3=35.3147ft3
		yd3 = bushel * 0.0346 * 1.308 '1 yd3=0.765m3     1m3=1.309yd3
		'1 bus=0.0346m3    1m3=27.5bus
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumeyd3.Text = CStr(yd3)
		
	End Sub
	
	Private Sub txtVolumeyd3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVolumeyd3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制立方码－－输入数据的计算过程
		yd3 = Val(txtVolumeyd3.Text)
		
		m3 = yd3 * 0.7646
		dm3 = yd3 * 764.55
		cm3 = yd3 * 764550#
		mm3 = yd3 * 764550000#
		
		Inch3 = yd3 * 46656# '1 in3=16.4cm3     1cm3=0.610in3
		ft3 = yd3 * 27# '1 ft3=0.0283m3    1m3=35.3147ft3
		'1 yd3=0.765m3     1m3=1.309yd3
		bushel = yd3 * 0.7646 * 27.5 '1 bus=0.0346m3    1m3=27.5bus
		
		txtVolumem3.Text = CStr(m3)
		txtVolumedm3.Text = CStr(dm3)
		txtVolumecm3.Text = CStr(cm3)
		txtVolumemm3.Text = CStr(mm3)
		txtVolumeinch3.Text = CStr(Inch3)
		txtVolumeft3.Text = CStr(ft3)
		txtVolumebushel.Text = CStr(bushel)
	End Sub
End Class