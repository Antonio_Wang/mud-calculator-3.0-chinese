Option Strict Off
Option Explicit On
Module modPicGWDC

	Public Sub PicGWDC(ByRef PicLogo As System.Windows.Forms.PictureBox)

		'************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		'PicLogo.GotFocus
        'PicLogo.Height = VB6.TwipsToPixelsY(VB6.FromPixelsUserHeight(frmAbout.DefInstance.picIcon.Height, 3033.51, 0))
        'PicLogo.Width = VB6.TwipsToPixelsX(VB6.FromPixelsUserWidth(frmAbout.DefInstance.picIcon.Width, 5380.77, 0))

        PicLogo.Height = frmAbout.DefInstance.picIcon.Height
        PicLogo.Width = frmAbout.DefInstance.picIcon.Width
		'UPGRADE_ISSUE: PictureBox 属性 picIcon.Appearance 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2064"”
		'UPGRADE_ISSUE: PictureBox 属性 PicLogo.Appearance 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2064"”

        'PicLogo.Appearance = frmAbout.DefInstance.picIcon.Appearance

        'UPGRADE_WARNING: PictureBox 属性 PicLogo.AutoSize 具有新的行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2065"”


        PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
		
		PicLogo.Image = frmAbout.DefInstance.picIcon.Image
		
        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        'Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
		
		
	End Sub
End Module