Option Strict Off
Option Explicit On
Friend Class FrmHelpHydraulic
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	Public WithEvents cmdHelp As System.Windows.Forms.Button
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmHelpHydraulic))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.cmdHelp = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHelp.Location = New System.Drawing.Point(0, 0)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtHelp.Size = New System.Drawing.Size(534, 328)
        Me.txtHelp.TabIndex = 1
        Me.txtHelp.Text = "水力参数计算" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     1、地面管汇压耗：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Psur=C×MW×(Q/100)^1.86×C1" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Psur---地面管汇压耗，Mpa（ps" & _
        "i）；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C----地面管汇的摩阻系数；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     MW----井内钻井液密度，g/cm^3(ppg);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Q----排量，l/s(gal" & _
        "/min);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C1----与单位有关的系数，当采用法定法量单位时，C1＝9.818；当采用英制单位时，C1＝1；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     地面管汇类型与C值：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "管汇类型" & Microsoft.VisualBasic.ChrW(13) & "      立管" & Microsoft.VisualBasic.ChrW(13) & "         水龙带" & Microsoft.VisualBasic.ChrW(13) & "       水龙头" & Microsoft.VisualBasic.ChrW(13) & "       方钻杆      " & Microsoft.VisualBasic.ChrW(13) & "C值" & Microsoft.VisualBasic.ChrW(13) & "        " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "          长" & _
        "度m" & Microsoft.VisualBasic.ChrW(13) & "  内径mm" & Microsoft.VisualBasic.ChrW(13) & "  长度m " & Microsoft.VisualBasic.ChrW(13) & "内径mm" & Microsoft.VisualBasic.ChrW(13) & " 长度m " & Microsoft.VisualBasic.ChrW(13) & "内径mm" & Microsoft.VisualBasic.ChrW(13) & " 长度m " & Microsoft.VisualBasic.ChrW(13) & "内径mm" & Microsoft.VisualBasic.ChrW(13) & "   " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   1" & Microsoft.VisualBasic.ChrW(13) & "      12.2" & Microsoft.VisualBasic.ChrW(13) & "    76.2" & Microsoft.VisualBasic.ChrW(13) & "   13." & _
        "7" & Microsoft.VisualBasic.ChrW(13) & "  50.8" & Microsoft.VisualBasic.ChrW(13) & "   1.2" & Microsoft.VisualBasic.ChrW(13) & "   50.8   " & Microsoft.VisualBasic.ChrW(13) & "12.2" & Microsoft.VisualBasic.ChrW(13) & "   57.2" & Microsoft.VisualBasic.ChrW(13) & "  1.0" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   " & Microsoft.VisualBasic.ChrW(13) & "2      " & Microsoft.VisualBasic.ChrW(13) & "12.2    " & Microsoft.VisualBasic.ChrW(13) & "88.9   " & Microsoft.VisualBasic.ChrW(13) & "16.8" & _
        "" & Microsoft.VisualBasic.ChrW(13) & "  63.5   " & Microsoft.VisualBasic.ChrW(13) & "1.5   " & Microsoft.VisualBasic.ChrW(13) & "57.2" & Microsoft.VisualBasic.ChrW(13) & "   12.2" & Microsoft.VisualBasic.ChrW(13) & "   82.6  " & Microsoft.VisualBasic.ChrW(13) & "0.36" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   3      " & Microsoft.VisualBasic.ChrW(13) & "13.7   " & Microsoft.VisualBasic.ChrW(13) & "101.6   " & Microsoft.VisualBasic.ChrW(13) & "16.8" & Microsoft.VisualBasic.ChrW(13) & _
        "  76.2" & Microsoft.VisualBasic.ChrW(13) & "   1.5" & Microsoft.VisualBasic.ChrW(13) & "   57.2   " & Microsoft.VisualBasic.ChrW(13) & "12.2   " & Microsoft.VisualBasic.ChrW(13) & "82.6" & Microsoft.VisualBasic.ChrW(13) & "  0.22" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   4      " & Microsoft.VisualBasic.ChrW(13) & "13.7   " & Microsoft.VisualBasic.ChrW(13) & "101.6" & Microsoft.VisualBasic.ChrW(13) & "   16.8" & Microsoft.VisualBasic.ChrW(13) & " " & _
        " 76.2" & Microsoft.VisualBasic.ChrW(13) & "   1.8   " & Microsoft.VisualBasic.ChrW(13) & "76.2   " & Microsoft.VisualBasic.ChrW(13) & "12.2  " & Microsoft.VisualBasic.ChrW(13) & "101.6  " & Microsoft.VisualBasic.ChrW(13) & "0.15" & Microsoft.VisualBasic.ChrW(13) & "     " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    2、确定钻具内的钻井液流态及计算压耗：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    " & _
        " ① 钻具内钻井液的平均流速：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     V1=(C2×Q)/(2.448×d^2)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     V1-------钻具内钻井液的平均流速，m/s(ft/s)" & _
        "；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Q-------排量，l/s(gal/min);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     d-------钻具内径，mm(in)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C2------与单位有关的" & _
        "系数。当采用法定计量单位时，C2＝3117，采用英制单位时，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "             C2＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ② 钻具内钻井液的临界流速" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     V1c" & _
        "=(1.08×PV＋1.08×(PV^2＋12.34×d^2×YP×MW×C3)^0.5)/(MW×d×C4)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     V1c -------钻具内钻井液的" & _
        "临界流速，m/s(ft/s)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     PV----钻井液的塑性粘度，mPa.s(cps);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     d------钻具内径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     " & _
        "MW----钻井液密度，g/cm^3(ppg);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C3 、C4------与单位有关的系数。采用法定计量单位时，C3＝0.006193，C4＝1.0" & _
        "78；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(9) & "  采用英制单位时，C3＝1、C4＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ③ 如果V1≤V1c，则流态为层流，钻具内的循环压耗为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "　" & Microsoft.VisualBasic.ChrW(9) & "Pp=(C5×L×YP)/(2" & _
        "25×d)＋(C6×V1×L×PV)/(1500×d^2)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ④ 如果V1>V1c，则流态为紊流，钻具内的循环压耗为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     " & Microsoft.VisualBasic.ChrW(9) & "Pp=(0.000" & _
        "0765×PV^0.18×MW^0.82×Q^1.82×L×C7)/d^4.82" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pp---钻具内的循环压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     L--" & _
        "--某一相同内径的钻具的长度，m(ft)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     V1-------钻具内钻井液的平均流速，m/s(ft/s)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     d------钻具内径，mm" & _
        "(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     MW----钻井液密度，g/cm^3(ppg);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Q-------排量，l/s(gal/min);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C3 、C6-" & _
        "-----与单位有关的系数。采用法定计量单位时，C5＝0.2750，C6＝47.86；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                  当采用英制单位时，C5＝1、C6＝" & _
        "1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C7 ------与单位有关的系数。采用法定计量单位时，C7＝1.162×108；当采用英制单位时，C7＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     3、环空的钻井液流" & _
        "态确定及压耗计算" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ① 环空内钻井液的平均流速" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Va = Q×C2/ ((Dh^2-Dp^2)×2.448)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Va------" & _
        "----环空内钻井液的平均流速m/s(ft/s)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Q-------排量，l/s(gal/min);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dh ------井眼直径或套管内" & _
        "径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dp ------钻具外径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C2 ------与单位有关的系数。采用法定计量单位时，C2＝3117；当采" & _
        "用英制单位时，C2＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ②环空内钻井液的临界流速" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "　　　Vac =(1.08×PV+1.08×(PV^2+9.26×(Dh- Dp )^2×YP" & _
        "×MW×C3)^0.5)/(MW×(Dh- Dp )×C4)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Vac-------环空内钻井液的临界流速m/s(ft/s)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     YP－－钻" & _
        "井液的屈服值，Pa(lbs/100ft2)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     PV------钻井液塑性粘度mPa.s(cps)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     MW----钻井液密度，g/cm^3" & _
        "(ppg);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dh ------井眼直径或套管内径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dp ------钻具外径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C3 、C4--" & _
        "----与单位有关的系数。采用法定计量单位时，C3＝0.006193，C4＝1.078；采用英制" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                  单位时，C3＝1、C4＝" & _
        "1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ③如果Va≤Vac，则环空流态为层流，环空压耗为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pa= C3×L×YP/(200×(Dh- Dp))+ (C6×Va×L×C7)" & _
        "/(1000×(Dh- Dp )^2)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     ④如果Va>Vac，则环空流态为紊流，环空压耗为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pa=(0.0000765×PV^0.18×M" & _
        "W^0.82×Q^1.82×L×C7)/((Dh- Dp )^3×(Dh＋ Dp )^1.82)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pa---循环压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    " & _
        " L----某一相同外径和井眼直径段的长度，m(ft)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     YP－－钻井液的屈服值，Pa(lbs/100ft2)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     PV------钻井液" & _
        "塑性粘度mPa.s(cps)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Va----------环空内钻井液的平均流速m/s(ft/s)；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dh ------井眼直径或套管内径" & _
        "，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Dp ------钻具外径，mm(in)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     MW----钻井液密度，g/cm^3(ppg);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Q-------" & _
        "排量，l/s(gal/min);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C5 、C6------与单位有关的系数。采用法定计量单位时，C5＝0.2750，C6＝47.86；当采用英制" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "                  单位时，C5＝1、C6＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     C7 ------与单位有关的系数。采用法定计量单位时，C7＝1.162×108；" & _
        "当采用英制单位时，C7＝1。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     4、根据前面求出的地面压耗和钻具内外各段的循环压耗，便可求出总的循环压耗：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pt= Psur＋Pc＋Pp " & _
        "＋Pca＋Ppa" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pa---总的循环压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Psur---地面管汇压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pc---钻铤" & _
        "段的内压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pp---钻杆段的内压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     Pca---钻铤段的环空压耗，Mpa(psi);" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   " & _
        "  Ppa---钻杆段的环空压耗，Mpa(psi);"
        Me.txtHelp.WordWrap = False
        '
        'cmdHelp
        '
        Me.cmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelp.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelp.Location = New System.Drawing.Point(216, 351)
        Me.cmdHelp.Name = "cmdHelp"
        Me.cmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelp.Size = New System.Drawing.Size(80, 20)
        Me.cmdHelp.TabIndex = 0
        Me.cmdHelp.Text = "确 定"
        '
        'FrmHelpHydraulic
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(529, 390)
        Me.Controls.Add(Me.txtHelp)
        Me.Controls.Add(Me.cmdHelp)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.Name = "FrmHelpHydraulic"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "钻井液水力计算公式说明"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As FrmHelpHydraulic
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As FrmHelpHydraulic
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New FrmHelpHydraulic()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		Me.Close()
	End Sub
End Class