Option Strict Off
Option Explicit On
Friend Class Form6
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Combo1 As System.Windows.Forms.ComboBox
	Public WithEvents Combo2 As System.Windows.Forms.ComboBox
	Public WithEvents Combo3 As System.Windows.Forms.ComboBox
	Public WithEvents Combo4 As System.Windows.Forms.ComboBox
	Public WithEvents Combo5 As System.Windows.Forms.ComboBox
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picMudAnnularVel As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form6))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.picMudAnnularVel = New System.Windows.Forms.PictureBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text10 = New System.Windows.Forms.TextBox
        Me.Text9 = New System.Windows.Forms.TextBox
        Me.Text8 = New System.Windows.Forms.TextBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Combo1 = New System.Windows.Forms.ComboBox
        Me.Combo2 = New System.Windows.Forms.ComboBox
        Me.Combo3 = New System.Windows.Forms.ComboBox
        Me.Combo4 = New System.Windows.Forms.ComboBox
        Me.Combo5 = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(240, 421)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(68, 20)
        Me.Text3.TabIndex = 8
        Me.Text3.Text = ""
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text3, "Example：1.03。")
        '
        'picMudAnnularVel
        '
        Me.picMudAnnularVel.Image = CType(resources.GetObject("picMudAnnularVel.Image"), System.Drawing.Image)
        Me.picMudAnnularVel.Location = New System.Drawing.Point(696, 48)
        Me.picMudAnnularVel.Name = "picMudAnnularVel"
        Me.picMudAnnularVel.Size = New System.Drawing.Size(180, 502)
        Me.picMudAnnularVel.TabIndex = 42
        Me.picMudAnnularVel.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picMudAnnularVel, "70D Rig  Haiyuan Ningxia China")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(497, 413)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(174, 70)
        Me.PicLogo.TabIndex = 41
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(401, 463)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 10
        Me.cmdExit.Text = "Exit"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(401, 432)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 9
        Me.cmdok.Text = "Run"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text10)
        Me.Frame2.Controls.Add(Me.Text9)
        Me.Frame2.Controls.Add(Me.Text8)
        Me.Frame2.Controls.Add(Me.Text7)
        Me.Frame2.Controls.Add(Me.Text6)
        Me.Frame2.Controls.Add(Me.Text5)
        Me.Frame2.Controls.Add(Me.Text4)
        Me.Frame2.Controls.Add(Me.Label22)
        Me.Frame2.Controls.Add(Me.Label21)
        Me.Frame2.Controls.Add(Me.Label20)
        Me.Frame2.Controls.Add(Me.Label19)
        Me.Frame2.Controls.Add(Me.Label18)
        Me.Frame2.Controls.Add(Me.Label17)
        Me.Frame2.Controls.Add(Me.Label16)
        Me.Frame2.Controls.Add(Me.Label15)
        Me.Frame2.Controls.Add(Me.Label14)
        Me.Frame2.Controls.Add(Me.Label13)
        Me.Frame2.Controls.Add(Me.Label12)
        Me.Frame2.Controls.Add(Me.Label11)
        Me.Frame2.Controls.Add(Me.Label10)
        Me.Frame2.Controls.Add(Me.Label9)
        Me.Frame2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(401, 8)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(270, 362)
        Me.Frame2.TabIndex = 11
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "结果"
        '
        'Text10
        '
        Me.Text10.AcceptsReturn = True
        Me.Text10.AutoSize = False
        Me.Text10.BackColor = System.Drawing.SystemColors.Window
        Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text10.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text10.Location = New System.Drawing.Point(115, 327)
        Me.Text10.MaxLength = 0
        Me.Text10.Name = "Text10"
        Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text10.Size = New System.Drawing.Size(69, 20)
        Me.Text10.TabIndex = 33
        Me.Text10.Text = ""
        Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text9
        '
        Me.Text9.AcceptsReturn = True
        Me.Text9.AutoSize = False
        Me.Text9.BackColor = System.Drawing.SystemColors.Window
        Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text9.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text9.Location = New System.Drawing.Point(115, 276)
        Me.Text9.MaxLength = 0
        Me.Text9.Name = "Text9"
        Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text9.Size = New System.Drawing.Size(69, 19)
        Me.Text9.TabIndex = 32
        Me.Text9.Text = ""
        Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text8
        '
        Me.Text8.AcceptsReturn = True
        Me.Text8.AutoSize = False
        Me.Text8.BackColor = System.Drawing.SystemColors.Window
        Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text8.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text8.Location = New System.Drawing.Point(115, 224)
        Me.Text8.MaxLength = 0
        Me.Text8.Name = "Text8"
        Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text8.Size = New System.Drawing.Size(69, 19)
        Me.Text8.TabIndex = 31
        Me.Text8.Text = ""
        Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.SystemColors.Window
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(115, 181)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(69, 19)
        Me.Text7.TabIndex = 30
        Me.Text7.Text = ""
        Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(115, 129)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(69, 20)
        Me.Text6.TabIndex = 29
        Me.Text6.Text = ""
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(115, 78)
        Me.Text5.MaxLength = 0
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(69, 19)
        Me.Text5.TabIndex = 28
        Me.Text5.Text = ""
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(115, 34)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(69, 20)
        Me.Text4.TabIndex = 27
        Me.Text4.Text = ""
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(202, 327)
        Me.Label22.Name = "Label22"
        Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label22.Size = New System.Drawing.Size(58, 19)
        Me.Label22.TabIndex = 40
        Me.Label22.Text = "m/s"
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(202, 276)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label21.Size = New System.Drawing.Size(58, 18)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "m/s"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label20.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(202, 224)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label20.Size = New System.Drawing.Size(58, 18)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "m/s"
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label19.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(202, 181)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label19.Size = New System.Drawing.Size(58, 18)
        Me.Label19.TabIndex = 37
        Me.Label19.Text = "m/s"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(202, 129)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(58, 19)
        Me.Label18.TabIndex = 36
        Me.Label18.Text = "m/s"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(202, 78)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(58, 18)
        Me.Label17.TabIndex = 35
        Me.Label17.Text = "m/s"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(202, 34)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(58, 19)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "m/s"
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(19, 327)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(93, 19)
        Me.Label15.TabIndex = 26
        Me.Label15.Text = "最高环空返速"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(0, Byte), CType(255, Byte))
        Me.Label14.Location = New System.Drawing.Point(19, 279)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(93, 19)
        Me.Label14.TabIndex = 25
        Me.Label14.Text = "最低环空返速"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(19, 233)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(69, 18)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "2#钻杆"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(19, 181)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(69, 18)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "1#钻杆"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(19, 132)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(69, 19)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "3#钻铤"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(19, 83)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(69, 18)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "2#钻铤"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(19, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(97, 19)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "1#钻铤"
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Combo1)
        Me.Frame1.Controls.Add(Me.Combo2)
        Me.Frame1.Controls.Add(Me.Combo3)
        Me.Frame1.Controls.Add(Me.Combo4)
        Me.Frame1.Controls.Add(Me.Combo5)
        Me.Frame1.Controls.Add(Me.Label6)
        Me.Frame1.Controls.Add(Me.Label7)
        Me.Frame1.Controls.Add(Me.Label8)
        Me.Frame1.Controls.Add(Me.Label1)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Controls.Add(Me.Label5)
        Me.Frame1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Blue
        Me.Frame1.Location = New System.Drawing.Point(8, 8)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(385, 475)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "数据录入"
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(240, 314)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(68, 20)
        Me.Text1.TabIndex = 6
        Me.Text1.Text = ""
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(240, 368)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(68, 19)
        Me.Text2.TabIndex = 7
        Me.Text2.Text = ""
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Combo1
        '
        Me.Combo1.BackColor = System.Drawing.SystemColors.Window
        Me.Combo1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo1.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo1.Location = New System.Drawing.Point(240, 34)
        Me.Combo1.Name = "Combo1"
        Me.Combo1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo1.Size = New System.Drawing.Size(68, 22)
        Me.Combo1.TabIndex = 1
        '
        'Combo2
        '
        Me.Combo2.BackColor = System.Drawing.SystemColors.Window
        Me.Combo2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo2.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo2.Location = New System.Drawing.Point(240, 90)
        Me.Combo2.Name = "Combo2"
        Me.Combo2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo2.Size = New System.Drawing.Size(68, 22)
        Me.Combo2.TabIndex = 2
        '
        'Combo3
        '
        Me.Combo3.BackColor = System.Drawing.SystemColors.Window
        Me.Combo3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo3.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo3.Location = New System.Drawing.Point(240, 146)
        Me.Combo3.Name = "Combo3"
        Me.Combo3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo3.Size = New System.Drawing.Size(68, 22)
        Me.Combo3.TabIndex = 3
        '
        'Combo4
        '
        Me.Combo4.BackColor = System.Drawing.SystemColors.Window
        Me.Combo4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo4.Items.AddRange(New Object() {"60.324", "73.024", "88.90", "101.60", "114.30", "118.60", "127.00", "139.70", "168.27"})
        Me.Combo4.Location = New System.Drawing.Point(240, 202)
        Me.Combo4.Name = "Combo4"
        Me.Combo4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo4.Size = New System.Drawing.Size(68, 22)
        Me.Combo4.TabIndex = 4
        '
        'Combo5
        '
        Me.Combo5.BackColor = System.Drawing.SystemColors.Window
        Me.Combo5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo5.Items.AddRange(New Object() {"60.324", "73.024", "88.90", "101.60", "114.30", "118.60", "127.00", "139.70", "168.27"})
        Me.Combo5.Location = New System.Drawing.Point(240, 258)
        Me.Combo5.Name = "Combo5"
        Me.Combo5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo5.Size = New System.Drawing.Size(68, 22)
        Me.Combo5.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(10, 314)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(222, 19)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "泥浆泵排量(L/S)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(10, 368)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(222, 19)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "钻头直径/套管内径(mm)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(10, 421)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(222, 19)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "裸眼井径扩大率"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(10, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(222, 22)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "1#钻铤外径(mm)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(10, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(222, 19)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "2#钻铤外径(mm)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(10, 146)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(222, 19)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "3#钻铤外径(mm)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(10, 202)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(222, 19)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "1#钻杆外径(mm)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(10, 258)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(222, 19)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "2#钻杆外径(mm)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Form6
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picMudAnnularVel)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "Form6"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液环空返速"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As Form6
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form6
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form6()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim Tw1 As Single 'Tw钻铤外径
	Dim Tw2 As Single
	Dim Tw3 As Single
	Dim Gw1 As Single ' Gw钻杆外径
	Dim Gw2 As Single
	Dim PumpQ As Single '泵排量
	Dim Bitd As Single '钻头直径
	Dim Kwell As Single '井径扩大率
	Dim Vtw1 As Single '钻铤1段环空返速
	Dim Vtw2 As Single '钻铤2段环空返速
	Dim Vtw3 As Single '钻铤3段环空返速
	Dim Vgw1 As Single '钻杆1段环空返速
	Dim Vgw2 As Single '钻杆2段环空返速
	Dim VH As Single '最高环空返速
	Dim VL As Single '最低环空返速
	Dim Vm As Single
	Const pi As Double = 3.1415926
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo1.SelectedIndexChanged
		Tw1 = Val(Combo1.Text)
	End Sub
	
	Private Sub Combo1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw1 = Val(Combo1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo2.SelectedIndexChanged
		Tw2 = Val(Combo2.Text)
	End Sub
	
	Private Sub Combo2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw2 = Val(Combo2.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo3.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo3.SelectedIndexChanged
		Tw3 = Val(Combo3.Text)
	End Sub
	
	Private Sub Combo3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw3 = Val(Combo3.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo4.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo4.SelectedIndexChanged
		Gw1 = Val(Combo4.Text)
	End Sub
	
	Private Sub Combo4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw1 = Val(Combo4.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo5.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo5_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo5.SelectedIndexChanged
		Gw2 = Val(Combo5.Text)
	End Sub
	
	Private Sub Combo5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw2 = Val(Combo5.Text)
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Gw1 = Val(Combo4.Text)
		
		
		PumpQ = Val(Text1.Text)
		If PumpQ <= 0 Then
			MsgBox("Please check that you have entered the 'Pump flow rate'data!", MsgBoxStyle.Information, "Mud Annular velocity")
			Text1.Text = ""
			Exit Sub
		End If
		
		Bitd = Val(Text2.Text)
		If Bitd <= 0 Or Bitd <= Gw1 Then
			MsgBox("Please check that you have entered the 'Bit diameter'data!", MsgBoxStyle.Information, "Mud Annular velocity")
			Text2.Text = ""
			Exit Sub
		End If
		
		Kwell = Val(Text3.Text)
		If Kwell >= 2 Or Kwell < 1 Then
			MsgBox("Please check that you have entered the 'Hole diameter enlargement rate' data!", MsgBoxStyle.Information, "Mud Annular velocity")
			Text3.Text = ""
			Exit Sub
		End If
		
		Vtw1 = Vtw2 = Vtw3 = Vgw1 = Vgw2 = 0
		If Tw1 > 0 Then
			Vtw1 = (1274 * PumpQ) / ((Kwell * Bitd) ^ 2 - (Tw1) ^ 2)
			Text4.Text = VB6.Format(Vtw1, "0.00")
		Else
			Text4.Text = ""
		End If
		If Tw2 > 0 Then
			Vtw2 = (1274 * PumpQ) / ((Kwell * Bitd) ^ 2 - (Tw2) ^ 2)
			Text5.Text = VB6.Format(Vtw2, "0.00")
		Else
			Text5.Text = ""
		End If
		If Tw3 > 0 Then
			Vtw3 = (1274 * PumpQ) / ((Kwell * Bitd) ^ 2 - (Tw3) ^ 2)
			Text6.Text = VB6.Format(Vtw3, "0.00")
		Else
			Text6.Text = ""
		End If
		If Gw1 > 0 Then
			Vgw1 = (1274 * PumpQ) / ((Kwell * Bitd) ^ 2 - (Gw1) ^ 2)
			Text7.Text = VB6.Format(Vgw1, "0.00")
		Else
			Text7.Text = ""
		End If
		If Gw2 > 0 Then
			Vgw2 = (1274 * PumpQ) / ((Kwell * Bitd) ^ 2 - (Gw2) ^ 2)
			Text8.Text = VB6.Format(Vgw2, "0.00")
			If Vgw1 > Vgw2 Then '环空最小返速一般是钻杆井段，钻杆段返速对比即可得出结果。
				VL = Vgw2
				Text9.Text = VB6.Format(VL, "0.00")
			Else
				VL = Vgw1
				Text9.Text = VB6.Format(VL, "0.00")
			End If
		Else
			Text8.Text = ""
			Text9.Text = VB6.Format(Vgw1, "0.00")
		End If
		
		'************************
		If Vtw1 > Vtw2 Then
			Vm = Vtw1
		Else
			Vm = Vtw2
		End If
		If Vm > Vtw3 Then
			Text10.Text = VB6.Format(Vm, "0.00")
		Else
			Text10.Text = VB6.Format(Vtw3, "0.00")
		End If
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub Form6_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************



		'************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		
		'*********************************************************************
		
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
		'--------------------------------------------------------------------------------------
        '*********************************************
        '加载数据代码在vb.net中取消直接图形界面设置录入
        '举例如下代码

        'Combo1.Items.Add("79.4") '1#钻铤外径
        'Combo1.Items.Add("88.9")
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MAVelocity.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MAVelocity.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpQ = Val(Text1.Text)
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Bitd = Val(Text2.Text)
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Kwell = Val(Text3.Text)
		If Kwell >= 2 Then
			MsgBox("Please check that you have entered the 'Hole diameter enlargement rate' data!", MsgBoxStyle.Information, "Mud Annular velocity")
			Text3.Text = ""
		End If
	End Sub

    
End Class