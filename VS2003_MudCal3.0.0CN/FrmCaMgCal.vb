Option Strict Off
Option Explicit On
Friend Class FrmCaMgCal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdOk As System.Windows.Forms.Button
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	Public WithEvents FraHelp As System.Windows.Forms.GroupBox
	Public WithEvents txtCausticSoda As System.Windows.Forms.TextBox
	Public WithEvents txtSoda As System.Windows.Forms.TextBox
	Public WithEvents txtMg As System.Windows.Forms.TextBox
	Public WithEvents txtCa As System.Windows.Forms.TextBox
	Public WithEvents lblCausticSodaQuantity As System.Windows.Forms.Label
	Public WithEvents lblSodaQuantity As System.Windows.Forms.Label
	Public WithEvents lblMg As System.Windows.Forms.Label
	Public WithEvents lblCa As System.Windows.Forms.Label
	Public WithEvents fraDataOutput As System.Windows.Forms.GroupBox
	Public WithEvents txtCEDTA As System.Windows.Forms.TextBox
	Public WithEvents txtCaDosage As System.Windows.Forms.TextBox
	Public WithEvents txtFiltrateSample As System.Windows.Forms.TextBox
	Public WithEvents txtMgDosage As System.Windows.Forms.TextBox
	Public WithEvents LblCEDTA As System.Windows.Forms.Label
	Public WithEvents LblCaDosage As System.Windows.Forms.Label
	Public WithEvents LblFiltrateSample As System.Windows.Forms.Label
	Public WithEvents lblMgDosage As System.Windows.Forms.Label
	Public WithEvents fraDataInput As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmCaMgCal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtSoda = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdOk = New System.Windows.Forms.Button
        Me.FraHelp = New System.Windows.Forms.GroupBox
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.fraDataOutput = New System.Windows.Forms.GroupBox
        Me.txtCausticSoda = New System.Windows.Forms.TextBox
        Me.txtMg = New System.Windows.Forms.TextBox
        Me.txtCa = New System.Windows.Forms.TextBox
        Me.lblCausticSodaQuantity = New System.Windows.Forms.Label
        Me.lblSodaQuantity = New System.Windows.Forms.Label
        Me.lblMg = New System.Windows.Forms.Label
        Me.lblCa = New System.Windows.Forms.Label
        Me.fraDataInput = New System.Windows.Forms.GroupBox
        Me.txtCEDTA = New System.Windows.Forms.TextBox
        Me.txtCaDosage = New System.Windows.Forms.TextBox
        Me.txtFiltrateSample = New System.Windows.Forms.TextBox
        Me.txtMgDosage = New System.Windows.Forms.TextBox
        Me.LblCEDTA = New System.Windows.Forms.Label
        Me.LblCaDosage = New System.Windows.Forms.Label
        Me.LblFiltrateSample = New System.Windows.Forms.Label
        Me.lblMgDosage = New System.Windows.Forms.Label
        Me.FraHelp.SuspendLayout()
        Me.fraDataOutput.SuspendLayout()
        Me.fraDataInput.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtSoda
        '
        Me.txtSoda.AcceptsReturn = True
        Me.txtSoda.AutoSize = False
        Me.txtSoda.BackColor = System.Drawing.SystemColors.Window
        Me.txtSoda.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSoda.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSoda.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSoda.Location = New System.Drawing.Point(280, 148)
        Me.txtSoda.MaxLength = 0
        Me.txtSoda.Name = "txtSoda"
        Me.txtSoda.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSoda.Size = New System.Drawing.Size(116, 27)
        Me.txtSoda.TabIndex = 8
        Me.txtSoda.Text = ""
        Me.txtSoda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtSoda, "实际生产中，钻井液中一般保留<200mg/l的Ca++含量，以利于钻井液性能的稳定。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(224, 496)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 22
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(88, 528)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 5
        Me.cmdExit.Text = "Exit"
        '
        'cmdOk
        '
        Me.cmdOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOk.Location = New System.Drawing.Point(88, 500)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOk.Size = New System.Drawing.Size(80, 20)
        Me.cmdOk.TabIndex = 4
        Me.cmdOk.Text = "Run"
        '
        'FraHelp
        '
        Me.FraHelp.BackColor = System.Drawing.SystemColors.Control
        Me.FraHelp.Controls.Add(Me.txtHelp)
        Me.FraHelp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FraHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FraHelp.Location = New System.Drawing.Point(432, 8)
        Me.FraHelp.Name = "FraHelp"
        Me.FraHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FraHelp.Size = New System.Drawing.Size(453, 568)
        Me.FraHelp.TabIndex = 16
        Me.FraHelp.TabStop = False
        Me.FraHelp.Text = "测试程序及污染处理方法"
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtHelp.Location = New System.Drawing.Point(10, 26)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtHelp.Size = New System.Drawing.Size(433, 531)
        Me.txtHelp.TabIndex = 17
        Me.txtHelp.Text = "1  仪器和试剂" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  a.  硝酸银: 浓度0.02820N(4.7910g/L)(相当于0.001 g 氯离" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "子每毫升), preferably store" & _
        "d in an amber bottle.；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  铬酸钾溶液:  5g/100cm3水；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  c.  硫酸或硝酸溶液:  0.02N 标准溶液；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "  d.  酚酞指示剂：将1g酚酞溶于100cm3 浓度为50%的酒精水溶" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "液中配制而成；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  e.  沉淀碳酸钙：化学纯；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  f.  蒸馏水；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "g." & _
        "  带刻度的移液管:  1cm3和10cm3 的各一支；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  h.  滴定瓶:  100~150cm3，白色。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  i.  搅拌棒。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2  测定步骤" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & " " & _
        "a.  取1cm3或几cm3滤液于滴定瓶中，加2～3滴酚酞溶液。如" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "果显示粉红色，则边搅拌边用移液管逐滴加入酸，直至粉红色消" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "失。如果滤液的颜色较深，则先加" & _
        "入2cm3 0.2N硫酸或硝酸并搅" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "拌，然后再加入1g碳酸钙并搅拌。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  加入25～50cm3蒸馏水和5～10滴铬酸钾指示剂。在不断搅" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "拌下，用移" & _
        "液管逐滴加入硝酸银标准溶液，直至颜色由黄色变为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "橙红色并能保持30秒为止。记录达到终点所消耗的硝酸银的cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "数。如果硝酸银溶液用量超过10cm3，则取少一" & _
        "些滤液进行重复" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "测定。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    如果滤液中的氯离子浓度超过1000mg/l，应使用1cm3=0.01g" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "氯离子的硝酸银溶液（即0.2820N的浓度）。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "3  计算" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        1000(硝酸银溶液用量，cm3)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  氯离子浓度Cl－(mg/l) = -----------" & _
        "------------" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        滤液样品体积，cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   如果所用的硝酸银溶液1cm3=0.01Cl－（即0" & _
        ".2820N的浓度）" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        10000(硝酸银溶液用量，cm3)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  氯离子浓度Cl－(mg/l) = ---" & _
        "------------------" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        滤液样品体积，cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  滤液NaCl含量(mg/l) = 1.6" & _
        "5(氯离子浓度，mg/l)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  若用ppm为单位表示氯离子浓度，则：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                     (氯离子浓度，mg/l)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "氯离子浓" & _
        "度Cl－(ppm) =---------------------------         " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     (滤液密度g/cm3)  " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   式中，滤" & _
        "液密度可由Cl－(mg/l)浓度通过查表而得到，如果" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "滤液的盐较复杂，不是单纯的NaCl，CaCl2或KCl等，则滤液密度" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "难于从表中查到，只能实际测定。对" & _
        "于淡水钻井液，由于滤液密" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "度近似1g/cm3，因此，用mg/l表示的氯离子浓度近似等于用ppm" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "表示的数值。"
        Me.txtHelp.WordWrap = False
        '
        'fraDataOutput
        '
        Me.fraDataOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataOutput.Controls.Add(Me.txtCausticSoda)
        Me.fraDataOutput.Controls.Add(Me.txtSoda)
        Me.fraDataOutput.Controls.Add(Me.txtMg)
        Me.fraDataOutput.Controls.Add(Me.txtCa)
        Me.fraDataOutput.Controls.Add(Me.lblCausticSodaQuantity)
        Me.fraDataOutput.Controls.Add(Me.lblSodaQuantity)
        Me.fraDataOutput.Controls.Add(Me.lblMg)
        Me.fraDataOutput.Controls.Add(Me.lblCa)
        Me.fraDataOutput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataOutput.ForeColor = System.Drawing.Color.Red
        Me.fraDataOutput.Location = New System.Drawing.Point(8, 215)
        Me.fraDataOutput.Name = "fraDataOutput"
        Me.fraDataOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataOutput.Size = New System.Drawing.Size(416, 268)
        Me.fraDataOutput.TabIndex = 11
        Me.fraDataOutput.TabStop = False
        Me.fraDataOutput.Text = "计算结果"
        '
        'txtCausticSoda
        '
        Me.txtCausticSoda.AcceptsReturn = True
        Me.txtCausticSoda.AutoSize = False
        Me.txtCausticSoda.BackColor = System.Drawing.SystemColors.Window
        Me.txtCausticSoda.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCausticSoda.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCausticSoda.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCausticSoda.Location = New System.Drawing.Point(280, 204)
        Me.txtCausticSoda.MaxLength = 0
        Me.txtCausticSoda.Name = "txtCausticSoda"
        Me.txtCausticSoda.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCausticSoda.Size = New System.Drawing.Size(116, 27)
        Me.txtCausticSoda.TabIndex = 10
        Me.txtCausticSoda.Text = ""
        Me.txtCausticSoda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMg
        '
        Me.txtMg.AcceptsReturn = True
        Me.txtMg.AutoSize = False
        Me.txtMg.BackColor = System.Drawing.SystemColors.Window
        Me.txtMg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMg.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMg.Location = New System.Drawing.Point(280, 76)
        Me.txtMg.MaxLength = 0
        Me.txtMg.Name = "txtMg"
        Me.txtMg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMg.Size = New System.Drawing.Size(116, 27)
        Me.txtMg.TabIndex = 7
        Me.txtMg.Text = ""
        Me.txtMg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCa
        '
        Me.txtCa.AcceptsReturn = True
        Me.txtCa.AutoSize = False
        Me.txtCa.BackColor = System.Drawing.SystemColors.Window
        Me.txtCa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCa.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCa.Location = New System.Drawing.Point(280, 31)
        Me.txtCa.MaxLength = 0
        Me.txtCa.Name = "txtCa"
        Me.txtCa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCa.Size = New System.Drawing.Size(116, 27)
        Me.txtCa.TabIndex = 6
        Me.txtCa.Text = ""
        Me.txtCa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCausticSodaQuantity
        '
        Me.lblCausticSodaQuantity.BackColor = System.Drawing.SystemColors.Control
        Me.lblCausticSodaQuantity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCausticSodaQuantity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCausticSodaQuantity.Location = New System.Drawing.Point(8, 200)
        Me.lblCausticSodaQuantity.Name = "lblCausticSodaQuantity"
        Me.lblCausticSodaQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCausticSodaQuantity.Size = New System.Drawing.Size(246, 35)
        Me.lblCausticSodaQuantity.TabIndex = 15
        Me.lblCausticSodaQuantity.Text = "清除硬水全部Mg++所需烧碱的用量(Kg.m-3/mg.l-1)"
        Me.lblCausticSodaQuantity.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblSodaQuantity
        '
        Me.lblSodaQuantity.BackColor = System.Drawing.SystemColors.Control
        Me.lblSodaQuantity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSodaQuantity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSodaQuantity.Location = New System.Drawing.Point(8, 144)
        Me.lblSodaQuantity.Name = "lblSodaQuantity"
        Me.lblSodaQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSodaQuantity.Size = New System.Drawing.Size(246, 35)
        Me.lblSodaQuantity.TabIndex = 14
        Me.lblSodaQuantity.Text = "清除硬水全部Ca++所需纯碱的用量(Kg.m-3/mg.l-1)"
        Me.lblSodaQuantity.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMg
        '
        Me.lblMg.BackColor = System.Drawing.SystemColors.Control
        Me.lblMg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMg.Location = New System.Drawing.Point(10, 80)
        Me.lblMg.Name = "lblMg"
        Me.lblMg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMg.Size = New System.Drawing.Size(174, 18)
        Me.lblMg.TabIndex = 13
        Me.lblMg.Text = "Mg++含量(mg/l) "
        '
        'lblCa
        '
        Me.lblCa.BackColor = System.Drawing.SystemColors.Control
        Me.lblCa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCa.Location = New System.Drawing.Point(10, 34)
        Me.lblCa.Name = "lblCa"
        Me.lblCa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCa.Size = New System.Drawing.Size(174, 20)
        Me.lblCa.TabIndex = 12
        Me.lblCa.Text = "Ca++含量(mg/l)"
        '
        'fraDataInput
        '
        Me.fraDataInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataInput.Controls.Add(Me.txtCEDTA)
        Me.fraDataInput.Controls.Add(Me.txtCaDosage)
        Me.fraDataInput.Controls.Add(Me.txtFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.txtMgDosage)
        Me.fraDataInput.Controls.Add(Me.LblCEDTA)
        Me.fraDataInput.Controls.Add(Me.LblCaDosage)
        Me.fraDataInput.Controls.Add(Me.LblFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.lblMgDosage)
        Me.fraDataInput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataInput.ForeColor = System.Drawing.Color.Blue
        Me.fraDataInput.Location = New System.Drawing.Point(8, 8)
        Me.fraDataInput.Name = "fraDataInput"
        Me.fraDataInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataInput.Size = New System.Drawing.Size(416, 199)
        Me.fraDataInput.TabIndex = 9
        Me.fraDataInput.TabStop = False
        Me.fraDataInput.Text = "数据录入"
        '
        'txtCEDTA
        '
        Me.txtCEDTA.AcceptsReturn = True
        Me.txtCEDTA.AutoSize = False
        Me.txtCEDTA.BackColor = System.Drawing.SystemColors.Window
        Me.txtCEDTA.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCEDTA.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCEDTA.Location = New System.Drawing.Point(280, 26)
        Me.txtCEDTA.MaxLength = 0
        Me.txtCEDTA.Name = "txtCEDTA"
        Me.txtCEDTA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCEDTA.Size = New System.Drawing.Size(98, 27)
        Me.txtCEDTA.TabIndex = 0
        Me.txtCEDTA.Text = ""
        Me.txtCEDTA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCaDosage
        '
        Me.txtCaDosage.AcceptsReturn = True
        Me.txtCaDosage.AutoSize = False
        Me.txtCaDosage.BackColor = System.Drawing.SystemColors.Window
        Me.txtCaDosage.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCaDosage.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCaDosage.Location = New System.Drawing.Point(280, 66)
        Me.txtCaDosage.MaxLength = 0
        Me.txtCaDosage.Name = "txtCaDosage"
        Me.txtCaDosage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCaDosage.Size = New System.Drawing.Size(98, 27)
        Me.txtCaDosage.TabIndex = 1
        Me.txtCaDosage.Text = ""
        Me.txtCaDosage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFiltrateSample
        '
        Me.txtFiltrateSample.AcceptsReturn = True
        Me.txtFiltrateSample.AutoSize = False
        Me.txtFiltrateSample.BackColor = System.Drawing.SystemColors.Window
        Me.txtFiltrateSample.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFiltrateSample.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFiltrateSample.Location = New System.Drawing.Point(280, 146)
        Me.txtFiltrateSample.MaxLength = 0
        Me.txtFiltrateSample.Name = "txtFiltrateSample"
        Me.txtFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFiltrateSample.Size = New System.Drawing.Size(98, 27)
        Me.txtFiltrateSample.TabIndex = 3
        Me.txtFiltrateSample.Text = ""
        Me.txtFiltrateSample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMgDosage
        '
        Me.txtMgDosage.AcceptsReturn = True
        Me.txtMgDosage.AutoSize = False
        Me.txtMgDosage.BackColor = System.Drawing.SystemColors.Window
        Me.txtMgDosage.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMgDosage.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMgDosage.Location = New System.Drawing.Point(280, 107)
        Me.txtMgDosage.MaxLength = 0
        Me.txtMgDosage.Name = "txtMgDosage"
        Me.txtMgDosage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMgDosage.Size = New System.Drawing.Size(98, 27)
        Me.txtMgDosage.TabIndex = 2
        Me.txtMgDosage.Text = ""
        Me.txtMgDosage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblCEDTA
        '
        Me.LblCEDTA.BackColor = System.Drawing.SystemColors.Control
        Me.LblCEDTA.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblCEDTA.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblCEDTA.Location = New System.Drawing.Point(10, 34)
        Me.LblCEDTA.Name = "LblCEDTA"
        Me.LblCEDTA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblCEDTA.Size = New System.Drawing.Size(174, 19)
        Me.LblCEDTA.TabIndex = 21
        Me.LblCEDTA.Text = "EDTA标准溶液浓度，mol/l"
        '
        'LblCaDosage
        '
        Me.LblCaDosage.BackColor = System.Drawing.SystemColors.Control
        Me.LblCaDosage.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblCaDosage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblCaDosage.Location = New System.Drawing.Point(10, 74)
        Me.LblCaDosage.Name = "LblCaDosage"
        Me.LblCaDosage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblCaDosage.Size = New System.Drawing.Size(174, 19)
        Me.LblCaDosage.TabIndex = 20
        Me.LblCaDosage.Text = "Ca++滴定EDTA用量，ml"
        '
        'LblFiltrateSample
        '
        Me.LblFiltrateSample.BackColor = System.Drawing.SystemColors.Control
        Me.LblFiltrateSample.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblFiltrateSample.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblFiltrateSample.Location = New System.Drawing.Point(10, 155)
        Me.LblFiltrateSample.Name = "LblFiltrateSample"
        Me.LblFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblFiltrateSample.Size = New System.Drawing.Size(174, 18)
        Me.LblFiltrateSample.TabIndex = 19
        Me.LblFiltrateSample.Text = "滤液样品体积，ml"
        '
        'lblMgDosage
        '
        Me.lblMgDosage.BackColor = System.Drawing.SystemColors.Control
        Me.lblMgDosage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMgDosage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMgDosage.Location = New System.Drawing.Point(10, 115)
        Me.lblMgDosage.Name = "lblMgDosage"
        Me.lblMgDosage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMgDosage.Size = New System.Drawing.Size(174, 19)
        Me.lblMgDosage.TabIndex = 18
        Me.lblMgDosage.Text = "Mg++滴定EDTA用量，ml"
        '
        'FrmCaMgCal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 588)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.FraHelp)
        Me.Controls.Add(Me.fraDataOutput)
        Me.Controls.Add(Me.fraDataInput)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "FrmCaMgCal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Ca++/Mg++离子测定"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FraHelp.ResumeLayout(False)
        Me.fraDataOutput.ResumeLayout(False)
        Me.fraDataInput.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As FrmCaMgCal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As FrmCaMgCal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New FrmCaMgCal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim CEDTA As Single '硝酸银标准溶液的当量浓度
	Dim V1 As Single '滴定Ca＋＋中消耗乙二胺四乙酸二钠标准溶液的体积，ml;
	Dim V2 As Single '滴定Mg＋＋中消耗乙二胺四乙酸二钠标准溶液的体积，ml;
	Dim V0 As Single '滤液样品的体积，ml
	Dim PCa As Single 'Ca++含量，单位mg/l
	Dim PMg As Single 'Mg++含量，单位mg/l
	Dim NNa2CO3 As Single '清除全部Ca++所需纯碱的用量(Kg.m-3/mg.l-1)
	Dim NNaOH As Single '清除全部Mg++所需烧碱的用量(Kg.m-3/mg.l-1)
	
	'--------------------------------------------------------------------
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		'*********************************************************
		'输入数据的纠错功能，避免出现除数为零的错误！
		
		CEDTA = Val(txtCEDTA.Text)
		If CEDTA = 0 Then
			MsgBox("请检查你输入的'EDTA标准溶液浓度'数据是否有效！", MsgBoxStyle.Information, "Ca++/Mg++离子测定")
			txtCEDTA.Text = ""
			Exit Sub
		End If
		
		V1 = Val(txtCaDosage.Text)
		If V1 = 0 Then
			MsgBox("请检查你输入的'Ca++滴定EDTA用量'数据是否有效！", MsgBoxStyle.Information, "Ca++/Mg++离子测定")
			txtCaDosage.Text = ""
			Exit Sub
		End If
		
		V2 = Val(txtMgDosage.Text)
		If V2 = 0 Then
			MsgBox("请检查你输入的'Mg++滴定EDTA用量'数据是否有效！", MsgBoxStyle.Information, "Ca++/Mg++离子测定")
			txtMgDosage.Text = ""
			Exit Sub
		End If
		
		V0 = Val(txtFiltrateSample.Text)
		If V0 = 0 Then
			MsgBox("请检查你输入的'滤液样品体积'数据是否有效！", MsgBoxStyle.Information, "Ca++/Mg++离子测定")
			txtFiltrateSample.Text = ""
			Exit Sub
		End If
		
		'计算公式
		PCa = ((CEDTA * V1) / V0) * 1000 * 40.08 'Ca++含量计算公式，单位mg/l
		PMg = (CEDTA * (V2 - V1) / V0) * 1000 * 24.32 'Mg++含量计算公式，单位mg/l
		NNa2CO3 = PCa * 0.00265 '清除全部Ca++所需纯碱的用量(Kg.m-3/mg.l-1)
		NNaOH = PMg * 0.00331 '清除全部Mg++所需烧碱的用量(Kg.m-3/mg.l-1)
		
		'数据结果显示
		
		txtCa.Text = VB6.Format(PCa, "0.00")
		txtMg.Text = VB6.Format(PMg, "0.00")
		txtSoda.Text = VB6.Format(NNa2CO3, "0.00000")
		txtCausticSoda.Text = VB6.Format(NNaOH, "0.00000")
		
	End Sub
	
	
	Private Sub FrmCaMgCal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************


        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MCaMagnesium.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MCaMagnesium.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdOk.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtCaDosage_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtCaDosage.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCaDosage_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCaDosage.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		V1 = Val(txtCaDosage.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtCEDTA_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtCEDTA.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCEDTA_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCEDTA.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		CEDTA = Val(txtCEDTA.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtFiltrateSample_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtFiltrateSample.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtFiltrateSample_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFiltrateSample.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		V0 = Val(txtFiltrateSample.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMgDosage_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMgDosage.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMgDosage_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMgDosage.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		V2 = Val(txtMgDosage.Text)
	End Sub
End Class