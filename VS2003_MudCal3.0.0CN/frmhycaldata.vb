Option Strict Off
Option Explicit On
Module modFrmMudCal
	'在标准模块中创建自定义数据类型－环空水力计算（MilPark公司模式），目前自定义数据内数据41项
	Public Structure hycaldatas
		Dim Q As Single
		Dim mw As Single
		Dim s0 As Single
		Dim s3 As Single
		Dim s6 As Single
		Dim C嘴 As Single '喷嘴流量系数
		Dim Hwell As Single '井深
		Dim Tgn As Single '套管内径
		Dim Tgh As Single '套管下深
		Dim Dz1 As Single
		Dim Dz2 As Single
		Dim Dz3 As Single
		Dim Dz4 As Single
		Dim Dz5 As Single
		Dim Dz6 As Single
		Dim Dz7 As Single '钻头喷嘴直径mm
		Dim Dz8 As Single
		Dim Tw1 As Single '第一段钻铤数据
		Dim Tn1 As Single
		Dim Tc1 As Single
		Dim Dt1 As Single
		Dim Tw2 As Single '第二段钻铤数据
		Dim Tn2 As Single
		Dim Tc2 As Single
		Dim Dt2 As Single
		Dim Tw3 As Single '第三段钻铤数据
		Dim Tn3 As Single
		Dim Tc3 As Single
		Dim Dt3 As Single
		Dim Gw1 As Single ' Gw钻杆外径 Gn钻杆内径 Gc钻杆长度
		Dim Gw2 As Single
		Dim Gw3 As Single
		Dim Gn1 As Single
		Dim Gn2 As Single
		Dim Gn3 As Single
		Dim Gc1 As Single
		Dim Gc2 As Single
		Dim Gc3 As Single
		Dim Dg1 As Single '钻杆段井眼直径
		Dim Dg2 As Single
		Dim Dg3 As Single
	End Structure
End Module