Option Strict Off
Option Explicit On
Friend Class frmunitBiteye
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtBiteyeUK As System.Windows.Forms.TextBox
	Public WithEvents txtBiteyemm As System.Windows.Forms.TextBox
	Public WithEvents cmdBiteyemmuk As System.Windows.Forms.Button
	Public WithEvents cmdBiteyeukmm As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents lblBiteyeUK As System.Windows.Forms.Label
	Public WithEvents lblVelocitykn As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmunitBiteye))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdBiteyemmuk = New System.Windows.Forms.Button
        Me.cmdBiteyeukmm = New System.Windows.Forms.Button
        Me.lblBiteyeUK = New System.Windows.Forms.Label
        Me.lblVelocitykn = New System.Windows.Forms.Label
        Me.txtBiteyeUK = New System.Windows.Forms.TextBox
        Me.txtBiteyemm = New System.Windows.Forms.TextBox
        Me.cmdquit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdBiteyemmuk
        '
        Me.cmdBiteyemmuk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBiteyemmuk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdBiteyemmuk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBiteyemmuk.Location = New System.Drawing.Point(182, 26)
        Me.cmdBiteyemmuk.Name = "cmdBiteyemmuk"
        Me.cmdBiteyemmuk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdBiteyemmuk.Size = New System.Drawing.Size(49, 18)
        Me.cmdBiteyemmuk.TabIndex = 2
        Me.cmdBiteyemmuk.Text = "<="
        Me.ToolTip1.SetToolTip(Me.cmdBiteyemmuk, "英制水眼号＝(国内水眼号*32)/25.4")
        '
        'cmdBiteyeukmm
        '
        Me.cmdBiteyeukmm.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBiteyeukmm.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdBiteyeukmm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBiteyeukmm.Location = New System.Drawing.Point(247, 26)
        Me.cmdBiteyeukmm.Name = "cmdBiteyeukmm"
        Me.cmdBiteyeukmm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdBiteyeukmm.Size = New System.Drawing.Size(49, 18)
        Me.cmdBiteyeukmm.TabIndex = 1
        Me.cmdBiteyeukmm.Text = "=>"
        Me.ToolTip1.SetToolTip(Me.cmdBiteyeukmm, "国内毫米水眼号 =(英制水眼号*25.4)/32")
        '
        'lblBiteyeUK
        '
        Me.lblBiteyeUK.BackColor = System.Drawing.SystemColors.Control
        Me.lblBiteyeUK.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBiteyeUK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBiteyeUK.Location = New System.Drawing.Point(64, 27)
        Me.lblBiteyeUK.Name = "lblBiteyeUK"
        Me.lblBiteyeUK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBiteyeUK.Size = New System.Drawing.Size(120, 16)
        Me.lblBiteyeUK.TabIndex = 6
        Me.lblBiteyeUK.Text = "1/32(UK)英制水眼号"
        Me.ToolTip1.SetToolTip(Me.lblBiteyeUK, "千米/小时")
        '
        'lblVelocitykn
        '
        Me.lblVelocitykn.BackColor = System.Drawing.SystemColors.Control
        Me.lblVelocitykn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVelocitykn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVelocitykn.Location = New System.Drawing.Point(378, 27)
        Me.lblVelocitykn.Name = "lblVelocitykn"
        Me.lblVelocitykn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVelocitykn.Size = New System.Drawing.Size(120, 16)
        Me.lblVelocitykn.TabIndex = 5
        Me.lblVelocitykn.Text = "mm(SI)毫米水眼号"
        Me.ToolTip1.SetToolTip(Me.lblVelocitykn, "节")
        '
        'txtBiteyeUK
        '
        Me.txtBiteyeUK.AcceptsReturn = True
        Me.txtBiteyeUK.AutoSize = False
        Me.txtBiteyeUK.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtBiteyeUK.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBiteyeUK.ForeColor = System.Drawing.Color.Blue
        Me.txtBiteyeUK.Location = New System.Drawing.Point(14, 26)
        Me.txtBiteyeUK.MaxLength = 0
        Me.txtBiteyeUK.Name = "txtBiteyeUK"
        Me.txtBiteyeUK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBiteyeUK.Size = New System.Drawing.Size(50, 19)
        Me.txtBiteyeUK.TabIndex = 4
        Me.txtBiteyeUK.Text = "1"
        '
        'txtBiteyemm
        '
        Me.txtBiteyemm.AcceptsReturn = True
        Me.txtBiteyemm.AutoSize = False
        Me.txtBiteyemm.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.txtBiteyemm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBiteyemm.ForeColor = System.Drawing.Color.Blue
        Me.txtBiteyemm.Location = New System.Drawing.Point(317, 26)
        Me.txtBiteyemm.MaxLength = 0
        Me.txtBiteyemm.Name = "txtBiteyemm"
        Me.txtBiteyemm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBiteyemm.Size = New System.Drawing.Size(50, 19)
        Me.txtBiteyemm.TabIndex = 3
        Me.txtBiteyemm.Text = "0.79375"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(202, 69)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 0
        Me.cmdquit.Text = "退出"
        '
        'frmunitBiteye
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(493, 117)
        Me.Controls.Add(Me.txtBiteyeUK)
        Me.Controls.Add(Me.txtBiteyemm)
        Me.Controls.Add(Me.cmdBiteyemmuk)
        Me.Controls.Add(Me.cmdBiteyeukmm)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.lblBiteyeUK)
        Me.Controls.Add(Me.lblVelocitykn)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.Name = "frmunitBiteye"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "钻头水眼号公英制转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitBiteye
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitBiteye
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitBiteye()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim Bituk As Single
	Dim Bitmm As Single
	
	Private Sub cmdBiteyemmuk_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBiteyemmuk.Click
		Bitmm = Val(txtBiteyemm.Text)
		If txtBiteyemm.Text = "" Or Bitmm < 0 Then
			MsgBox("Please check that you have entered 'mm(SI)'data!", MsgBoxStyle.Information, "Bit Jet Size convert")
			txtBiteyemm.Text = ""
			Exit Sub
		Else
			
			Bituk = (Bitmm * 32) / 25.4
		End If
		
		txtBiteyeUK.Text = VB6.Format(Bituk, "0.00")
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdBiteyeukmm_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBiteyeukmm.Click
		
		
		Bituk = Val(txtBiteyeUK.Text)
		If txtBiteyeUK.Text = "" Or Bituk < 1 Then
			MsgBox("Please check that you have entered '1/32(UK)'data!", MsgBoxStyle.Information, "Bit Jet Size convert")
			txtBiteyeUK.Text = ""
			Exit Sub
		Else
			
			Bitmm = (Bituk * 25.4) / 32
		End If
		
		txtBiteyemm.Text = VB6.Format(Bitmm, "0.0000")
	End Sub
	
	Private Sub txtBiteyemm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBiteyemm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Bitmm = Val(txtBiteyemm.Text)
	End Sub
	
	Private Sub txtBiteyeUK_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBiteyeUK.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Bituk = Val(txtBiteyeUK.Text)
	End Sub

    Private Sub lblVelocitykn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblVelocitykn.Click

    End Sub
End Class