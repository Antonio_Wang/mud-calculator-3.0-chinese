Option Strict Off
Option Explicit On
Friend Class FrmClCal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdOk As System.Windows.Forms.Button
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	Public WithEvents FraHelp As System.Windows.Forms.GroupBox
	Public WithEvents txtFiltrateDensity As System.Windows.Forms.TextBox
	Public WithEvents txtNaClPercent As System.Windows.Forms.TextBox
	Public WithEvents txtNaClppm As System.Windows.Forms.TextBox
	Public WithEvents txtNaClmg As System.Windows.Forms.TextBox
	Public WithEvents txtClppm As System.Windows.Forms.TextBox
	Public WithEvents txtClmg As System.Windows.Forms.TextBox
	Public WithEvents lblFiltrateDensity As System.Windows.Forms.Label
	Public WithEvents lblNaClPercent As System.Windows.Forms.Label
	Public WithEvents lblNaClppm As System.Windows.Forms.Label
	Public WithEvents lblNaClmg As System.Windows.Forms.Label
	Public WithEvents lblClppm As System.Windows.Forms.Label
	Public WithEvents lblClmg As System.Windows.Forms.Label
	Public WithEvents fraDataOutput As System.Windows.Forms.GroupBox
	Public WithEvents txtFiltrateSample As System.Windows.Forms.TextBox
	Public WithEvents txtWastage As System.Windows.Forms.TextBox
	Public WithEvents txtliquorChroma As System.Windows.Forms.TextBox
	Public WithEvents LblFiltrateSample As System.Windows.Forms.Label
	Public WithEvents LblWastage As System.Windows.Forms.Label
	Public WithEvents LblliquorChroma As System.Windows.Forms.Label
	Public WithEvents fraDataInput As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmClCal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdOk = New System.Windows.Forms.Button
        Me.FraHelp = New System.Windows.Forms.GroupBox
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.fraDataOutput = New System.Windows.Forms.GroupBox
        Me.txtFiltrateDensity = New System.Windows.Forms.TextBox
        Me.txtNaClPercent = New System.Windows.Forms.TextBox
        Me.txtNaClppm = New System.Windows.Forms.TextBox
        Me.txtNaClmg = New System.Windows.Forms.TextBox
        Me.txtClppm = New System.Windows.Forms.TextBox
        Me.txtClmg = New System.Windows.Forms.TextBox
        Me.lblFiltrateDensity = New System.Windows.Forms.Label
        Me.lblNaClPercent = New System.Windows.Forms.Label
        Me.lblNaClppm = New System.Windows.Forms.Label
        Me.lblNaClmg = New System.Windows.Forms.Label
        Me.lblClppm = New System.Windows.Forms.Label
        Me.lblClmg = New System.Windows.Forms.Label
        Me.fraDataInput = New System.Windows.Forms.GroupBox
        Me.txtFiltrateSample = New System.Windows.Forms.TextBox
        Me.txtWastage = New System.Windows.Forms.TextBox
        Me.txtliquorChroma = New System.Windows.Forms.TextBox
        Me.LblFiltrateSample = New System.Windows.Forms.Label
        Me.LblWastage = New System.Windows.Forms.Label
        Me.LblliquorChroma = New System.Windows.Forms.Label
        Me.FraHelp.SuspendLayout()
        Me.fraDataOutput.SuspendLayout()
        Me.fraDataInput.SuspendLayout()
        Me.SuspendLayout()
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(240, 488)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 24
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(88, 528)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 11
        Me.cmdExit.Text = "Exit"
        '
        'cmdOk
        '
        Me.cmdOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOk.Location = New System.Drawing.Point(88, 500)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOk.Size = New System.Drawing.Size(80, 20)
        Me.cmdOk.TabIndex = 10
        Me.cmdOk.Text = "Run"
        '
        'FraHelp
        '
        Me.FraHelp.BackColor = System.Drawing.SystemColors.Control
        Me.FraHelp.Controls.Add(Me.txtHelp)
        Me.FraHelp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FraHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FraHelp.Location = New System.Drawing.Point(430, 8)
        Me.FraHelp.Name = "FraHelp"
        Me.FraHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FraHelp.Size = New System.Drawing.Size(453, 535)
        Me.FraHelp.TabIndex = 21
        Me.FraHelp.TabStop = False
        Me.FraHelp.Text = "测试说明"
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtHelp.Location = New System.Drawing.Point(10, 26)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtHelp.Size = New System.Drawing.Size(433, 498)
        Me.txtHelp.TabIndex = 22
        Me.txtHelp.Text = "1  仪器和试剂" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  a.  硝酸银: 浓度0.02820N(4.7910g/L)(相当于0.001 g 氯离" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "子每毫升), preferably store" & _
        "d in an amber bottle.；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  铬酸钾溶液:  5g/100cm3水；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  c.  硫酸或硝酸溶液:  0.02N 标准溶液；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "  d.  酚酞指示剂：将1g酚酞溶于100cm3 浓度为50%的酒精水溶" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "液中配制而成；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  e.  沉淀碳酸钙：化学纯；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  f.  蒸馏水；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "g." & _
        "  带刻度的移液管:  1cm3和10cm3 的各一支；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  h.  滴定瓶:  100~150cm3，白色。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  i.  搅拌棒。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2  测定步骤" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "a" & _
        ".  取1cm3或几cm3滤液于滴定瓶中，加2～3滴酚酞溶液。如" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "果显示粉红色，则边搅拌边用移液管逐滴加入酸，直至粉红色消" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "失。如果滤液的颜色较深，则先加入" & _
        "2cm3 0.2N硫酸或硝酸并搅" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "拌，然后再加入1g碳酸钙并搅拌。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  加入25～50cm3蒸馏水和5～10滴铬酸钾指示剂。在不断搅" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "拌下，用移液" & _
        "管逐滴加入硝酸银标准溶液，直至颜色由黄色变为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "橙红色并能保持30秒为止。记录达到终点所消耗的硝酸银的cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "数。如果硝酸银溶液用量超过10cm3，则取少一些" & _
        "滤液进行重复" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "测定。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    如果滤液中的氯离子浓度超过1000mg/l，应使用1cm3=0.01g" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "氯离子的硝酸银溶液（即0.2820N的浓度）。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "3" & _
        "  计算" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        1000(硝酸银溶液用量，cm3)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  氯离子浓度Cl－(mg/l) = ------------" & _
        "-----------" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        滤液样品体积，cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   如果所用的硝酸银溶液1cm3=0.01Cl－（即0." & _
        "2820N的浓度）" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        10000(硝酸银溶液用量，cm3)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  氯离子浓度Cl－(mg/l) = ----" & _
        "-----------------" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        滤液样品体积，cm3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  滤液NaCl含量(mg/l) = 1.65" & _
        "(氯离子浓度，mg/l)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  若用ppm为单位表示氯离子浓度，则：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                     (氯离子浓度，mg/l)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "氯离子浓度" & _
        "Cl－(ppm) =---------------------------         " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     (滤液密度g/cm3)  " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "   式中，滤液" & _
        "密度可由Cl－(mg/l)浓度通过查表而得到，如果" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "滤液的盐较复杂，不是单纯的NaCl，CaCl2或KCl等，则滤液密度" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "难于从表中查到，只能实际测定。对于" & _
        "淡水钻井液，由于滤液密" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "度近似1g/cm3，因此，用mg/l表示的氯离子浓度近似等于用ppm" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "表示的数值。"
        '
        'fraDataOutput
        '
        Me.fraDataOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataOutput.Controls.Add(Me.txtFiltrateDensity)
        Me.fraDataOutput.Controls.Add(Me.txtNaClPercent)
        Me.fraDataOutput.Controls.Add(Me.txtNaClppm)
        Me.fraDataOutput.Controls.Add(Me.txtNaClmg)
        Me.fraDataOutput.Controls.Add(Me.txtClppm)
        Me.fraDataOutput.Controls.Add(Me.txtClmg)
        Me.fraDataOutput.Controls.Add(Me.lblFiltrateDensity)
        Me.fraDataOutput.Controls.Add(Me.lblNaClPercent)
        Me.fraDataOutput.Controls.Add(Me.lblNaClppm)
        Me.fraDataOutput.Controls.Add(Me.lblNaClmg)
        Me.fraDataOutput.Controls.Add(Me.lblClppm)
        Me.fraDataOutput.Controls.Add(Me.lblClmg)
        Me.fraDataOutput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataOutput.ForeColor = System.Drawing.Color.Red
        Me.fraDataOutput.Location = New System.Drawing.Point(8, 172)
        Me.fraDataOutput.Name = "fraDataOutput"
        Me.fraDataOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataOutput.Size = New System.Drawing.Size(410, 294)
        Me.fraDataOutput.TabIndex = 12
        Me.fraDataOutput.TabStop = False
        Me.fraDataOutput.Text = "计算结果"
        '
        'txtFiltrateDensity
        '
        Me.txtFiltrateDensity.AcceptsReturn = True
        Me.txtFiltrateDensity.AutoSize = False
        Me.txtFiltrateDensity.BackColor = System.Drawing.SystemColors.Window
        Me.txtFiltrateDensity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFiltrateDensity.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFiltrateDensity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFiltrateDensity.Location = New System.Drawing.Point(270, 250)
        Me.txtFiltrateDensity.MaxLength = 0
        Me.txtFiltrateDensity.Name = "txtFiltrateDensity"
        Me.txtFiltrateDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFiltrateDensity.Size = New System.Drawing.Size(116, 27)
        Me.txtFiltrateDensity.TabIndex = 9
        Me.txtFiltrateDensity.Text = ""
        '
        'txtNaClPercent
        '
        Me.txtNaClPercent.AcceptsReturn = True
        Me.txtNaClPercent.AutoSize = False
        Me.txtNaClPercent.BackColor = System.Drawing.SystemColors.Window
        Me.txtNaClPercent.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNaClPercent.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNaClPercent.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNaClPercent.Location = New System.Drawing.Point(270, 207)
        Me.txtNaClPercent.MaxLength = 0
        Me.txtNaClPercent.Name = "txtNaClPercent"
        Me.txtNaClPercent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNaClPercent.Size = New System.Drawing.Size(116, 27)
        Me.txtNaClPercent.TabIndex = 8
        Me.txtNaClPercent.Text = ""
        '
        'txtNaClppm
        '
        Me.txtNaClppm.AcceptsReturn = True
        Me.txtNaClppm.AutoSize = False
        Me.txtNaClppm.BackColor = System.Drawing.SystemColors.Window
        Me.txtNaClppm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNaClppm.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNaClppm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNaClppm.Location = New System.Drawing.Point(270, 164)
        Me.txtNaClppm.MaxLength = 0
        Me.txtNaClppm.Name = "txtNaClppm"
        Me.txtNaClppm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNaClppm.Size = New System.Drawing.Size(116, 26)
        Me.txtNaClppm.TabIndex = 7
        Me.txtNaClppm.Text = ""
        '
        'txtNaClmg
        '
        Me.txtNaClmg.AcceptsReturn = True
        Me.txtNaClmg.AutoSize = False
        Me.txtNaClmg.BackColor = System.Drawing.SystemColors.Window
        Me.txtNaClmg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNaClmg.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNaClmg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNaClmg.Location = New System.Drawing.Point(270, 121)
        Me.txtNaClmg.MaxLength = 0
        Me.txtNaClmg.Name = "txtNaClmg"
        Me.txtNaClmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNaClmg.Size = New System.Drawing.Size(116, 27)
        Me.txtNaClmg.TabIndex = 6
        Me.txtNaClmg.Text = ""
        '
        'txtClppm
        '
        Me.txtClppm.AcceptsReturn = True
        Me.txtClppm.AutoSize = False
        Me.txtClppm.BackColor = System.Drawing.SystemColors.Window
        Me.txtClppm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClppm.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClppm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClppm.Location = New System.Drawing.Point(270, 78)
        Me.txtClppm.MaxLength = 0
        Me.txtClppm.Name = "txtClppm"
        Me.txtClppm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClppm.Size = New System.Drawing.Size(116, 27)
        Me.txtClppm.TabIndex = 5
        Me.txtClppm.Text = ""
        '
        'txtClmg
        '
        Me.txtClmg.AcceptsReturn = True
        Me.txtClmg.AutoSize = False
        Me.txtClmg.BackColor = System.Drawing.SystemColors.Window
        Me.txtClmg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClmg.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClmg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClmg.Location = New System.Drawing.Point(270, 34)
        Me.txtClmg.MaxLength = 0
        Me.txtClmg.Name = "txtClmg"
        Me.txtClmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClmg.Size = New System.Drawing.Size(116, 27)
        Me.txtClmg.TabIndex = 4
        Me.txtClmg.Text = ""
        '
        'lblFiltrateDensity
        '
        Me.lblFiltrateDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblFiltrateDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFiltrateDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFiltrateDensity.Location = New System.Drawing.Point(10, 250)
        Me.lblFiltrateDensity.Name = "lblFiltrateDensity"
        Me.lblFiltrateDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFiltrateDensity.Size = New System.Drawing.Size(246, 27)
        Me.lblFiltrateDensity.TabIndex = 23
        Me.lblFiltrateDensity.Text = "滤液密度  g/cm^3"
        Me.lblFiltrateDensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNaClPercent
        '
        Me.lblNaClPercent.BackColor = System.Drawing.SystemColors.Control
        Me.lblNaClPercent.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNaClPercent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNaClPercent.Location = New System.Drawing.Point(10, 207)
        Me.lblNaClPercent.Name = "lblNaClPercent"
        Me.lblNaClPercent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNaClPercent.Size = New System.Drawing.Size(246, 27)
        Me.lblNaClPercent.TabIndex = 20
        Me.lblNaClPercent.Text = "滤液NaCl含盐量重量百分比浓度(%)"
        Me.lblNaClPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNaClppm
        '
        Me.lblNaClppm.BackColor = System.Drawing.SystemColors.Control
        Me.lblNaClppm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNaClppm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNaClppm.Location = New System.Drawing.Point(10, 164)
        Me.lblNaClppm.Name = "lblNaClppm"
        Me.lblNaClppm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNaClppm.Size = New System.Drawing.Size(246, 27)
        Me.lblNaClppm.TabIndex = 19
        Me.lblNaClppm.Text = "滤液NaCl含量  (ppm)"
        Me.lblNaClppm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNaClmg
        '
        Me.lblNaClmg.BackColor = System.Drawing.SystemColors.Control
        Me.lblNaClmg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNaClmg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNaClmg.Location = New System.Drawing.Point(10, 121)
        Me.lblNaClmg.Name = "lblNaClmg"
        Me.lblNaClmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNaClmg.Size = New System.Drawing.Size(246, 27)
        Me.lblNaClmg.TabIndex = 18
        Me.lblNaClmg.Text = "滤液NaCl含量  (mg/l)"
        Me.lblNaClmg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClppm
        '
        Me.lblClppm.BackColor = System.Drawing.SystemColors.Control
        Me.lblClppm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClppm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClppm.Location = New System.Drawing.Point(10, 78)
        Me.lblClppm.Name = "lblClppm"
        Me.lblClppm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClppm.Size = New System.Drawing.Size(246, 27)
        Me.lblClppm.TabIndex = 17
        Me.lblClppm.Text = "氯离子浓度Cl－(ppm) "
        Me.lblClppm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClmg
        '
        Me.lblClmg.BackColor = System.Drawing.SystemColors.Control
        Me.lblClmg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClmg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClmg.Location = New System.Drawing.Point(10, 34)
        Me.lblClmg.Name = "lblClmg"
        Me.lblClmg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClmg.Size = New System.Drawing.Size(246, 27)
        Me.lblClmg.TabIndex = 16
        Me.lblClmg.Text = "氯离子浓度Cl－(mg/l)"
        Me.lblClmg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraDataInput
        '
        Me.fraDataInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataInput.Controls.Add(Me.txtFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.txtWastage)
        Me.fraDataInput.Controls.Add(Me.txtliquorChroma)
        Me.fraDataInput.Controls.Add(Me.LblFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.LblWastage)
        Me.fraDataInput.Controls.Add(Me.LblliquorChroma)
        Me.fraDataInput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataInput.ForeColor = System.Drawing.Color.Blue
        Me.fraDataInput.Location = New System.Drawing.Point(8, 8)
        Me.fraDataInput.Name = "fraDataInput"
        Me.fraDataInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataInput.Size = New System.Drawing.Size(410, 156)
        Me.fraDataInput.TabIndex = 0
        Me.fraDataInput.TabStop = False
        Me.fraDataInput.Text = "数据录入"
        '
        'txtFiltrateSample
        '
        Me.txtFiltrateSample.AcceptsReturn = True
        Me.txtFiltrateSample.AutoSize = False
        Me.txtFiltrateSample.BackColor = System.Drawing.SystemColors.Window
        Me.txtFiltrateSample.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFiltrateSample.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtFiltrateSample.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFiltrateSample.Location = New System.Drawing.Point(288, 112)
        Me.txtFiltrateSample.MaxLength = 0
        Me.txtFiltrateSample.Name = "txtFiltrateSample"
        Me.txtFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFiltrateSample.Size = New System.Drawing.Size(98, 27)
        Me.txtFiltrateSample.TabIndex = 3
        Me.txtFiltrateSample.Text = ""
        '
        'txtWastage
        '
        Me.txtWastage.AcceptsReturn = True
        Me.txtWastage.AutoSize = False
        Me.txtWastage.BackColor = System.Drawing.SystemColors.Window
        Me.txtWastage.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWastage.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtWastage.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWastage.Location = New System.Drawing.Point(288, 69)
        Me.txtWastage.MaxLength = 0
        Me.txtWastage.Name = "txtWastage"
        Me.txtWastage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWastage.Size = New System.Drawing.Size(98, 27)
        Me.txtWastage.TabIndex = 2
        Me.txtWastage.Text = ""
        '
        'txtliquorChroma
        '
        Me.txtliquorChroma.AcceptsReturn = True
        Me.txtliquorChroma.AutoSize = False
        Me.txtliquorChroma.BackColor = System.Drawing.SystemColors.Window
        Me.txtliquorChroma.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtliquorChroma.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtliquorChroma.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtliquorChroma.Location = New System.Drawing.Point(288, 26)
        Me.txtliquorChroma.MaxLength = 0
        Me.txtliquorChroma.Name = "txtliquorChroma"
        Me.txtliquorChroma.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtliquorChroma.Size = New System.Drawing.Size(98, 27)
        Me.txtliquorChroma.TabIndex = 1
        Me.txtliquorChroma.Text = ""
        '
        'LblFiltrateSample
        '
        Me.LblFiltrateSample.BackColor = System.Drawing.SystemColors.Control
        Me.LblFiltrateSample.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblFiltrateSample.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblFiltrateSample.Location = New System.Drawing.Point(10, 121)
        Me.LblFiltrateSample.Name = "LblFiltrateSample"
        Me.LblFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblFiltrateSample.Size = New System.Drawing.Size(174, 18)
        Me.LblFiltrateSample.TabIndex = 15
        Me.LblFiltrateSample.Text = "滤液样品体积，cm3"
        '
        'LblWastage
        '
        Me.LblWastage.BackColor = System.Drawing.SystemColors.Control
        Me.LblWastage.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblWastage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblWastage.Location = New System.Drawing.Point(10, 78)
        Me.LblWastage.Name = "LblWastage"
        Me.LblWastage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblWastage.Size = New System.Drawing.Size(174, 18)
        Me.LblWastage.TabIndex = 14
        Me.LblWastage.Text = "硝酸银溶液用量，cm3"
        '
        'LblliquorChroma
        '
        Me.LblliquorChroma.BackColor = System.Drawing.SystemColors.Control
        Me.LblliquorChroma.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblliquorChroma.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblliquorChroma.Location = New System.Drawing.Point(10, 34)
        Me.LblliquorChroma.Name = "LblliquorChroma"
        Me.LblliquorChroma.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblliquorChroma.Size = New System.Drawing.Size(174, 19)
        Me.LblliquorChroma.TabIndex = 13
        Me.LblliquorChroma.Text = "硝酸银标准溶液浓度,mol/l"
        '
        'FrmClCal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 598)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.FraHelp)
        Me.Controls.Add(Me.fraDataOutput)
        Me.Controls.Add(Me.fraDataInput)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "FrmClCal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "氯离子计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FraHelp.ResumeLayout(False)
        Me.fraDataOutput.ResumeLayout(False)
        Me.fraDataInput.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As FrmClCal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As FrmClCal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New FrmClCal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim NAgNO3 As Single '硝酸银标准溶液的当量浓度
	Dim VAgNO3 As Single '消耗硝酸银标准溶液的体积，ml
	Dim VFilSam As Single '滤液样品的体积，ml
	Dim NClmg As Single 'Cl-含量，单位mg/l
	Dim NClppm As Single 'Cl-含量，单位ppm
	Dim NNaClmg As Single 'NaCl含量，单位mg/l
	Dim NNaClppm As Single 'NaCl含量，单位ppm
	Dim NaClPercent As Single 'NaCl含盐量重量百分比，%
	Dim Pf As Single '滤液密度，单位g/cm^3
	
	'--------------------------------------------------------------------
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		NAgNO3 = Val(txtliquorChroma.Text)
		If NAgNO3 = 0 Then
			MsgBox("请检查你输入的'AgNO3A标准溶液浓度'数据是否有效！", MsgBoxStyle.Information, "氯离子计算")
			txtliquorChroma.Text = ""
			Exit Sub
		End If
		
		VAgNO3 = Val(txtWastage.Text)
		If VAgNO3 = 0 Then
			MsgBox("请检查你输入的'硝酸银溶液用量'数据是否有效！", MsgBoxStyle.Information, "氯离子计算")
			txtWastage.Text = ""
			Exit Sub
		End If
		
		VFilSam = Val(txtFiltrateSample.Text)
		If VFilSam = 0 Then
			MsgBox("请检查你输入的'滤液样品体积'数据是否有效！", MsgBoxStyle.Information, "氯离子计算")
			txtFiltrateSample.Text = ""
			Exit Sub
		End If
		
		NClmg = ((NAgNO3 * VAgNO3) / VFilSam) * 1000 * 35.45 'Cl-含量计算公式，单位mg/l
		Pf = 1 + 0.00000109 * NClmg '由氯离子含量计算滤液密度公式，单位g/cm^3
		NClppm = NClmg / Pf 'Cl-含量计算公式，单位ppm
		NNaClmg = 1.65 * NClmg 'NaCl含量计算公式，单位mg/l
		NNaClppm = NNaClmg / Pf 'NaCl含量计算公式，单位ppm
		NaClPercent = ((NNaClmg / Pf) / 1000000) * 100 'NaCl含盐量重量百分比，%
		
		'数据结果显示
		
		txtClmg.Text = VB6.Format(NClmg, "0")
		txtClppm.Text = VB6.Format(NClppm, "0")
		txtNaClmg.Text = VB6.Format(NNaClmg, "0")
		txtNaClppm.Text = VB6.Format(NNaClppm, "0")
		txtNaClPercent.Text = VB6.Format(NaClPercent, "0.00")
		txtFiltrateDensity.Text = VB6.Format(Pf, "0.0000")
	End Sub
	
	Private Sub FrmClCal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("Mchloride.bmp")
		
        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("Mchloride.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdOk.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtFiltrateSample_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtFiltrateSample.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtFiltrateSample_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFiltrateSample.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		VFilSam = Val(txtFiltrateSample.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtliquorChroma_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtliquorChroma.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtliquorChroma_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtliquorChroma.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		NAgNO3 = Val(txtliquorChroma.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtWastage_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtWastage.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtWastage_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWastage.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		VAgNO3 = Val(txtWastage.Text)
	End Sub


End Class