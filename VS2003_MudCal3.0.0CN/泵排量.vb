Option Strict Off
Option Explicit On
Friend Class FrmPump
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdHelpPumpVol As System.Windows.Forms.Button
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents txtMudflowUS As System.Windows.Forms.TextBox
	Public WithEvents txtMudflow As System.Windows.Forms.TextBox
	Public WithEvents lblMudflowUS As System.Windows.Forms.Label
	Public WithEvents lblMudflow As System.Windows.Forms.Label
	Public WithEvents fraMudflow As System.Windows.Forms.GroupBox
	Public WithEvents txtPumpliang2 As System.Windows.Forms.TextBox
	Public WithEvents txtXiaolu2 As System.Windows.Forms.TextBox
	Public WithEvents txtPumpgantao2 As System.Windows.Forms.TextBox
	Public WithEvents txtPumpStorke2 As System.Windows.Forms.TextBox
	Public WithEvents cboPumplength2 As System.Windows.Forms.ComboBox
	Public WithEvents cboPumpDia2 As System.Windows.Forms.ComboBox
	Public WithEvents lblPumpliang2 As System.Windows.Forms.Label
	Public WithEvents lblXiaolu2 As System.Windows.Forms.Label
	Public WithEvents lblPumpgantao2 As System.Windows.Forms.Label
	Public WithEvents lblPumpStorke2 As System.Windows.Forms.Label
	Public WithEvents lblPumpLength2 As System.Windows.Forms.Label
	Public WithEvents lblPumpDiameter2 As System.Windows.Forms.Label
	Public WithEvents fraPump2 As System.Windows.Forms.GroupBox
	Public WithEvents txtPumpliang1 As System.Windows.Forms.TextBox
	Public WithEvents txtXiaolu1 As System.Windows.Forms.TextBox
	Public WithEvents txtPumpgantao1 As System.Windows.Forms.TextBox
	Public WithEvents txtPumpStorke1 As System.Windows.Forms.TextBox
	Public WithEvents cboPumplength1 As System.Windows.Forms.ComboBox
	Public WithEvents cboPumpDia1 As System.Windows.Forms.ComboBox
	Public WithEvents lblPumpDiameter1 As System.Windows.Forms.Label
	Public WithEvents lblPumpLength1 As System.Windows.Forms.Label
	Public WithEvents lblPumpStorke1 As System.Windows.Forms.Label
	Public WithEvents lblPumpgantao1 As System.Windows.Forms.Label
	Public WithEvents lblXiaolu1 As System.Windows.Forms.Label
	Public WithEvents lblPumpliang1 As System.Windows.Forms.Label
	Public WithEvents fraPump1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmPump))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtXiaolu2 = New System.Windows.Forms.TextBox
        Me.txtXiaolu1 = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdHelpPumpVol = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.fraMudflow = New System.Windows.Forms.GroupBox
        Me.txtMudflowUS = New System.Windows.Forms.TextBox
        Me.txtMudflow = New System.Windows.Forms.TextBox
        Me.lblMudflowUS = New System.Windows.Forms.Label
        Me.lblMudflow = New System.Windows.Forms.Label
        Me.fraPump2 = New System.Windows.Forms.GroupBox
        Me.txtPumpliang2 = New System.Windows.Forms.TextBox
        Me.txtPumpgantao2 = New System.Windows.Forms.TextBox
        Me.txtPumpStorke2 = New System.Windows.Forms.TextBox
        Me.cboPumplength2 = New System.Windows.Forms.ComboBox
        Me.cboPumpDia2 = New System.Windows.Forms.ComboBox
        Me.lblPumpliang2 = New System.Windows.Forms.Label
        Me.lblXiaolu2 = New System.Windows.Forms.Label
        Me.lblPumpgantao2 = New System.Windows.Forms.Label
        Me.lblPumpStorke2 = New System.Windows.Forms.Label
        Me.lblPumpLength2 = New System.Windows.Forms.Label
        Me.lblPumpDiameter2 = New System.Windows.Forms.Label
        Me.fraPump1 = New System.Windows.Forms.GroupBox
        Me.txtPumpliang1 = New System.Windows.Forms.TextBox
        Me.txtPumpgantao1 = New System.Windows.Forms.TextBox
        Me.txtPumpStorke1 = New System.Windows.Forms.TextBox
        Me.cboPumplength1 = New System.Windows.Forms.ComboBox
        Me.cboPumpDia1 = New System.Windows.Forms.ComboBox
        Me.lblPumpDiameter1 = New System.Windows.Forms.Label
        Me.lblPumpLength1 = New System.Windows.Forms.Label
        Me.lblPumpStorke1 = New System.Windows.Forms.Label
        Me.lblPumpgantao1 = New System.Windows.Forms.Label
        Me.lblXiaolu1 = New System.Windows.Forms.Label
        Me.lblPumpliang1 = New System.Windows.Forms.Label
        Me.fraMudflow.SuspendLayout()
        Me.fraPump2.SuspendLayout()
        Me.fraPump1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtXiaolu2
        '
        Me.txtXiaolu2.AcceptsReturn = True
        Me.txtXiaolu2.AutoSize = False
        Me.txtXiaolu2.BackColor = System.Drawing.SystemColors.Window
        Me.txtXiaolu2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtXiaolu2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtXiaolu2.Location = New System.Drawing.Point(194, 216)
        Me.txtXiaolu2.MaxLength = 0
        Me.txtXiaolu2.Name = "txtXiaolu2"
        Me.txtXiaolu2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtXiaolu2.Size = New System.Drawing.Size(69, 24)
        Me.txtXiaolu2.TabIndex = 11
        Me.txtXiaolu2.Text = ""
        Me.txtXiaolu2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtXiaolu2, "一般泥浆泵上水效率为0.92～0.96。")
        '
        'txtXiaolu1
        '
        Me.txtXiaolu1.AcceptsReturn = True
        Me.txtXiaolu1.AutoSize = False
        Me.txtXiaolu1.BackColor = System.Drawing.SystemColors.Window
        Me.txtXiaolu1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtXiaolu1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtXiaolu1.Location = New System.Drawing.Point(206, 220)
        Me.txtXiaolu1.MaxLength = 0
        Me.txtXiaolu1.Name = "txtXiaolu1"
        Me.txtXiaolu1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtXiaolu1.Size = New System.Drawing.Size(69, 25)
        Me.txtXiaolu1.TabIndex = 5
        Me.txtXiaolu1.Text = ""
        Me.txtXiaolu1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtXiaolu1, "一般泥浆泵上水效率为92～96%。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(499, 415)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 32
        Me.PicLogo.TabStop = False
        '
        'cmdHelpPumpVol
        '
        Me.cmdHelpPumpVol.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelpPumpVol.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelpPumpVol.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdHelpPumpVol.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelpPumpVol.Location = New System.Drawing.Point(374, 452)
        Me.cmdHelpPumpVol.Name = "cmdHelpPumpVol"
        Me.cmdHelpPumpVol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelpPumpVol.Size = New System.Drawing.Size(80, 20)
        Me.cmdHelpPumpVol.TabIndex = 25
        Me.cmdHelpPumpVol.Text = "Help"
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(374, 424)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 14
        Me.cmdExit.Text = "Exit"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(374, 396)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 13
        Me.cmdok.Text = "Run"
        '
        'fraMudflow
        '
        Me.fraMudflow.BackColor = System.Drawing.SystemColors.Control
        Me.fraMudflow.Controls.Add(Me.txtMudflowUS)
        Me.fraMudflow.Controls.Add(Me.txtMudflow)
        Me.fraMudflow.Controls.Add(Me.lblMudflowUS)
        Me.fraMudflow.Controls.Add(Me.lblMudflow)
        Me.fraMudflow.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraMudflow.ForeColor = System.Drawing.Color.Red
        Me.fraMudflow.Location = New System.Drawing.Point(8, 360)
        Me.fraMudflow.Name = "fraMudflow"
        Me.fraMudflow.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMudflow.Size = New System.Drawing.Size(320, 115)
        Me.fraMudflow.TabIndex = 22
        Me.fraMudflow.TabStop = False
        Me.fraMudflow.Text = "计算结果"
        '
        'txtMudflowUS
        '
        Me.txtMudflowUS.AcceptsReturn = True
        Me.txtMudflowUS.AutoSize = False
        Me.txtMudflowUS.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudflowUS.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudflowUS.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMudflowUS.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudflowUS.Location = New System.Drawing.Point(182, 69)
        Me.txtMudflowUS.MaxLength = 0
        Me.txtMudflowUS.Name = "txtMudflowUS"
        Me.txtMudflowUS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudflowUS.Size = New System.Drawing.Size(105, 23)
        Me.txtMudflowUS.TabIndex = 33
        Me.txtMudflowUS.Text = ""
        Me.txtMudflowUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMudflow
        '
        Me.txtMudflow.AcceptsReturn = True
        Me.txtMudflow.AutoSize = False
        Me.txtMudflow.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudflow.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudflow.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMudflow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudflow.Location = New System.Drawing.Point(182, 28)
        Me.txtMudflow.MaxLength = 0
        Me.txtMudflow.Name = "txtMudflow"
        Me.txtMudflow.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudflow.Size = New System.Drawing.Size(105, 24)
        Me.txtMudflow.TabIndex = 24
        Me.txtMudflow.Text = ""
        Me.txtMudflow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMudflowUS
        '
        Me.lblMudflowUS.BackColor = System.Drawing.SystemColors.Control
        Me.lblMudflowUS.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMudflowUS.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMudflowUS.Location = New System.Drawing.Point(29, 73)
        Me.lblMudflowUS.Name = "lblMudflowUS"
        Me.lblMudflowUS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMudflowUS.Size = New System.Drawing.Size(143, 21)
        Me.lblMudflowUS.TabIndex = 34
        Me.lblMudflowUS.Text = "泥浆泵排量(gal/min)"
        '
        'lblMudflow
        '
        Me.lblMudflow.BackColor = System.Drawing.SystemColors.Control
        Me.lblMudflow.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMudflow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMudflow.Location = New System.Drawing.Point(29, 32)
        Me.lblMudflow.Name = "lblMudflow"
        Me.lblMudflow.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMudflow.Size = New System.Drawing.Size(143, 21)
        Me.lblMudflow.TabIndex = 23
        Me.lblMudflow.Text = "泥浆泵排量(L/S)"
        '
        'fraPump2
        '
        Me.fraPump2.BackColor = System.Drawing.SystemColors.Control
        Me.fraPump2.Controls.Add(Me.txtPumpliang2)
        Me.fraPump2.Controls.Add(Me.txtXiaolu2)
        Me.fraPump2.Controls.Add(Me.txtPumpgantao2)
        Me.fraPump2.Controls.Add(Me.txtPumpStorke2)
        Me.fraPump2.Controls.Add(Me.cboPumplength2)
        Me.fraPump2.Controls.Add(Me.cboPumpDia2)
        Me.fraPump2.Controls.Add(Me.lblPumpliang2)
        Me.fraPump2.Controls.Add(Me.lblXiaolu2)
        Me.fraPump2.Controls.Add(Me.lblPumpgantao2)
        Me.fraPump2.Controls.Add(Me.lblPumpStorke2)
        Me.fraPump2.Controls.Add(Me.lblPumpLength2)
        Me.fraPump2.Controls.Add(Me.lblPumpDiameter2)
        Me.fraPump2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraPump2.ForeColor = System.Drawing.Color.Red
        Me.fraPump2.Location = New System.Drawing.Point(336, 28)
        Me.fraPump2.Name = "fraPump2"
        Me.fraPump2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraPump2.Size = New System.Drawing.Size(320, 324)
        Me.fraPump2.TabIndex = 15
        Me.fraPump2.TabStop = False
        Me.fraPump2.Text = "2# 泥浆泵参数"
        '
        'txtPumpliang2
        '
        Me.txtPumpliang2.AcceptsReturn = True
        Me.txtPumpliang2.AutoSize = False
        Me.txtPumpliang2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpliang2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpliang2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpliang2.Location = New System.Drawing.Point(194, 264)
        Me.txtPumpliang2.MaxLength = 0
        Me.txtPumpliang2.Name = "txtPumpliang2"
        Me.txtPumpliang2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpliang2.Size = New System.Drawing.Size(69, 25)
        Me.txtPumpliang2.TabIndex = 12
        Me.txtPumpliang2.Text = ""
        Me.txtPumpliang2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPumpgantao2
        '
        Me.txtPumpgantao2.AcceptsReturn = True
        Me.txtPumpgantao2.AutoSize = False
        Me.txtPumpgantao2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpgantao2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpgantao2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpgantao2.Location = New System.Drawing.Point(194, 167)
        Me.txtPumpgantao2.MaxLength = 0
        Me.txtPumpgantao2.Name = "txtPumpgantao2"
        Me.txtPumpgantao2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpgantao2.Size = New System.Drawing.Size(69, 25)
        Me.txtPumpgantao2.TabIndex = 10
        Me.txtPumpgantao2.Text = ""
        Me.txtPumpgantao2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPumpStorke2
        '
        Me.txtPumpStorke2.AcceptsReturn = True
        Me.txtPumpStorke2.AutoSize = False
        Me.txtPumpStorke2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpStorke2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpStorke2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpStorke2.Location = New System.Drawing.Point(194, 118)
        Me.txtPumpStorke2.MaxLength = 0
        Me.txtPumpStorke2.Name = "txtPumpStorke2"
        Me.txtPumpStorke2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpStorke2.Size = New System.Drawing.Size(69, 25)
        Me.txtPumpStorke2.TabIndex = 9
        Me.txtPumpStorke2.Text = ""
        Me.txtPumpStorke2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPumplength2
        '
        Me.cboPumplength2.BackColor = System.Drawing.SystemColors.Window
        Me.cboPumplength2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboPumplength2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cboPumplength2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboPumplength2.Items.AddRange(New Object() {"216", "220", "235", "254", "304.8", "320", "400"})
        Me.cboPumplength2.Location = New System.Drawing.Point(194, 72)
        Me.cboPumplength2.Name = "cboPumplength2"
        Me.cboPumplength2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboPumplength2.Size = New System.Drawing.Size(93, 22)
        Me.cboPumplength2.TabIndex = 8
        Me.cboPumplength2.Text = "Stroke Length"
        '
        'cboPumpDia2
        '
        Me.cboPumpDia2.BackColor = System.Drawing.SystemColors.Window
        Me.cboPumpDia2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboPumpDia2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cboPumpDia2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboPumpDia2.Items.AddRange(New Object() {"90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "190"})
        Me.cboPumpDia2.Location = New System.Drawing.Point(194, 26)
        Me.cboPumpDia2.Name = "cboPumpDia2"
        Me.cboPumpDia2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboPumpDia2.Size = New System.Drawing.Size(93, 22)
        Me.cboPumpDia2.TabIndex = 7
        Me.cboPumpDia2.Text = "Liner Diameter"
        '
        'lblPumpliang2
        '
        Me.lblPumpliang2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpliang2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpliang2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpliang2.Location = New System.Drawing.Point(12, 264)
        Me.lblPumpliang2.Name = "lblPumpliang2"
        Me.lblPumpliang2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpliang2.Size = New System.Drawing.Size(172, 25)
        Me.lblPumpliang2.TabIndex = 21
        Me.lblPumpliang2.Text = "泥浆泵个数 (个）"
        Me.lblPumpliang2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lblXiaolu2
        '
        Me.lblXiaolu2.BackColor = System.Drawing.SystemColors.Control
        Me.lblXiaolu2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblXiaolu2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblXiaolu2.Location = New System.Drawing.Point(12, 216)
        Me.lblXiaolu2.Name = "lblXiaolu2"
        Me.lblXiaolu2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblXiaolu2.Size = New System.Drawing.Size(172, 25)
        Me.lblXiaolu2.TabIndex = 20
        Me.lblXiaolu2.Text = "上水效率 %"
        Me.lblXiaolu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpgantao2
        '
        Me.lblPumpgantao2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpgantao2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpgantao2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpgantao2.Location = New System.Drawing.Point(12, 167)
        Me.lblPumpgantao2.Name = "lblPumpgantao2"
        Me.lblPumpgantao2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpgantao2.Size = New System.Drawing.Size(172, 25)
        Me.lblPumpgantao2.TabIndex = 19
        Me.lblPumpgantao2.Text = "缸套数量 （个）"
        Me.lblPumpgantao2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lblPumpStorke2
        '
        Me.lblPumpStorke2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpStorke2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpStorke2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpStorke2.Location = New System.Drawing.Point(12, 118)
        Me.lblPumpStorke2.Name = "lblPumpStorke2"
        Me.lblPumpStorke2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpStorke2.Size = New System.Drawing.Size(172, 25)
        Me.lblPumpStorke2.TabIndex = 18
        Me.lblPumpStorke2.Text = "每分钟冲数 (stk/min)"
        Me.lblPumpStorke2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpLength2
        '
        Me.lblPumpLength2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpLength2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpLength2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpLength2.Location = New System.Drawing.Point(12, 71)
        Me.lblPumpLength2.Name = "lblPumpLength2"
        Me.lblPumpLength2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpLength2.Size = New System.Drawing.Size(172, 25)
        Me.lblPumpLength2.TabIndex = 17
        Me.lblPumpLength2.Text = "活塞冲程 (mm)"
        Me.lblPumpLength2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpDiameter2
        '
        Me.lblPumpDiameter2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpDiameter2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpDiameter2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpDiameter2.Location = New System.Drawing.Point(12, 25)
        Me.lblPumpDiameter2.Name = "lblPumpDiameter2"
        Me.lblPumpDiameter2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpDiameter2.Size = New System.Drawing.Size(172, 25)
        Me.lblPumpDiameter2.TabIndex = 16
        Me.lblPumpDiameter2.Text = "泥浆泵缸套直径 (mm)"
        Me.lblPumpDiameter2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraPump1
        '
        Me.fraPump1.BackColor = System.Drawing.SystemColors.Control
        Me.fraPump1.Controls.Add(Me.txtPumpliang1)
        Me.fraPump1.Controls.Add(Me.txtXiaolu1)
        Me.fraPump1.Controls.Add(Me.txtPumpgantao1)
        Me.fraPump1.Controls.Add(Me.txtPumpStorke1)
        Me.fraPump1.Controls.Add(Me.cboPumplength1)
        Me.fraPump1.Controls.Add(Me.cboPumpDia1)
        Me.fraPump1.Controls.Add(Me.lblPumpDiameter1)
        Me.fraPump1.Controls.Add(Me.lblPumpLength1)
        Me.fraPump1.Controls.Add(Me.lblPumpStorke1)
        Me.fraPump1.Controls.Add(Me.lblPumpgantao1)
        Me.fraPump1.Controls.Add(Me.lblXiaolu1)
        Me.fraPump1.Controls.Add(Me.lblPumpliang1)
        Me.fraPump1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraPump1.ForeColor = System.Drawing.Color.Red
        Me.fraPump1.Location = New System.Drawing.Point(8, 28)
        Me.fraPump1.Name = "fraPump1"
        Me.fraPump1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraPump1.Size = New System.Drawing.Size(320, 324)
        Me.fraPump1.TabIndex = 0
        Me.fraPump1.TabStop = False
        Me.fraPump1.Text = "1# 泥浆泵参数"
        '
        'txtPumpliang1
        '
        Me.txtPumpliang1.AcceptsReturn = True
        Me.txtPumpliang1.AutoSize = False
        Me.txtPumpliang1.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpliang1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpliang1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpliang1.Location = New System.Drawing.Point(206, 270)
        Me.txtPumpliang1.MaxLength = 0
        Me.txtPumpliang1.Name = "txtPumpliang1"
        Me.txtPumpliang1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpliang1.Size = New System.Drawing.Size(69, 24)
        Me.txtPumpliang1.TabIndex = 6
        Me.txtPumpliang1.Text = ""
        Me.txtPumpliang1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPumpgantao1
        '
        Me.txtPumpgantao1.AcceptsReturn = True
        Me.txtPumpgantao1.AutoSize = False
        Me.txtPumpgantao1.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpgantao1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpgantao1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpgantao1.Location = New System.Drawing.Point(206, 170)
        Me.txtPumpgantao1.MaxLength = 0
        Me.txtPumpgantao1.Name = "txtPumpgantao1"
        Me.txtPumpgantao1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpgantao1.Size = New System.Drawing.Size(69, 25)
        Me.txtPumpgantao1.TabIndex = 4
        Me.txtPumpgantao1.Text = ""
        Me.txtPumpgantao1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPumpStorke1
        '
        Me.txtPumpStorke1.AcceptsReturn = True
        Me.txtPumpStorke1.AutoSize = False
        Me.txtPumpStorke1.BackColor = System.Drawing.SystemColors.Window
        Me.txtPumpStorke1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPumpStorke1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPumpStorke1.Location = New System.Drawing.Point(206, 120)
        Me.txtPumpStorke1.MaxLength = 0
        Me.txtPumpStorke1.Name = "txtPumpStorke1"
        Me.txtPumpStorke1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPumpStorke1.Size = New System.Drawing.Size(69, 25)
        Me.txtPumpStorke1.TabIndex = 3
        Me.txtPumpStorke1.Text = ""
        Me.txtPumpStorke1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPumplength1
        '
        Me.cboPumplength1.BackColor = System.Drawing.SystemColors.Window
        Me.cboPumplength1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboPumplength1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cboPumplength1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboPumplength1.Items.AddRange(New Object() {"216", "220", "235", "254", "304.8", "320", "400"})
        Me.cboPumplength1.Location = New System.Drawing.Point(204, 73)
        Me.cboPumplength1.Name = "cboPumplength1"
        Me.cboPumplength1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboPumplength1.Size = New System.Drawing.Size(92, 22)
        Me.cboPumplength1.TabIndex = 2
        Me.cboPumplength1.Text = "Stroke Length"
        '
        'cboPumpDia1
        '
        Me.cboPumpDia1.BackColor = System.Drawing.SystemColors.Window
        Me.cboPumpDia1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboPumpDia1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cboPumpDia1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboPumpDia1.Items.AddRange(New Object() {"90", "100", "110", "120", "130", "140", "150", "160", "170", "180", "190"})
        Me.cboPumpDia1.Location = New System.Drawing.Point(204, 26)
        Me.cboPumpDia1.Name = "cboPumpDia1"
        Me.cboPumpDia1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboPumpDia1.Size = New System.Drawing.Size(92, 22)
        Me.cboPumpDia1.TabIndex = 1
        Me.cboPumpDia1.Text = "Liner Diameter"
        '
        'lblPumpDiameter1
        '
        Me.lblPumpDiameter1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpDiameter1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpDiameter1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpDiameter1.Location = New System.Drawing.Point(19, 25)
        Me.lblPumpDiameter1.Name = "lblPumpDiameter1"
        Me.lblPumpDiameter1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpDiameter1.Size = New System.Drawing.Size(160, 25)
        Me.lblPumpDiameter1.TabIndex = 31
        Me.lblPumpDiameter1.Text = "泥浆泵缸套直径 (mm)"
        Me.lblPumpDiameter1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpLength1
        '
        Me.lblPumpLength1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpLength1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpLength1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpLength1.Location = New System.Drawing.Point(19, 72)
        Me.lblPumpLength1.Name = "lblPumpLength1"
        Me.lblPumpLength1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpLength1.Size = New System.Drawing.Size(160, 25)
        Me.lblPumpLength1.TabIndex = 30
        Me.lblPumpLength1.Text = "活塞冲程 (mm)"
        Me.lblPumpLength1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpStorke1
        '
        Me.lblPumpStorke1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpStorke1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpStorke1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpStorke1.Location = New System.Drawing.Point(19, 120)
        Me.lblPumpStorke1.Name = "lblPumpStorke1"
        Me.lblPumpStorke1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpStorke1.Size = New System.Drawing.Size(160, 25)
        Me.lblPumpStorke1.TabIndex = 29
        Me.lblPumpStorke1.Text = "每分钟冲数(stk/min)"
        Me.lblPumpStorke1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpgantao1
        '
        Me.lblPumpgantao1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpgantao1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpgantao1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpgantao1.Location = New System.Drawing.Point(19, 170)
        Me.lblPumpgantao1.Name = "lblPumpgantao1"
        Me.lblPumpgantao1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpgantao1.Size = New System.Drawing.Size(160, 25)
        Me.lblPumpgantao1.TabIndex = 28
        Me.lblPumpgantao1.Text = "缸套数量 (个）"
        Me.lblPumpgantao1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lblXiaolu1
        '
        Me.lblXiaolu1.BackColor = System.Drawing.SystemColors.Control
        Me.lblXiaolu1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblXiaolu1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblXiaolu1.Location = New System.Drawing.Point(19, 220)
        Me.lblXiaolu1.Name = "lblXiaolu1"
        Me.lblXiaolu1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblXiaolu1.Size = New System.Drawing.Size(173, 25)
        Me.lblXiaolu1.TabIndex = 27
        Me.lblXiaolu1.Text = "上水效率 %"
        Me.lblXiaolu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPumpliang1
        '
        Me.lblPumpliang1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPumpliang1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPumpliang1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPumpliang1.Location = New System.Drawing.Point(19, 270)
        Me.lblPumpliang1.Name = "lblPumpliang1"
        Me.lblPumpliang1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPumpliang1.Size = New System.Drawing.Size(160, 25)
        Me.lblPumpliang1.TabIndex = 26
        Me.lblPumpliang1.Text = "泥浆泵个数 （个）"
        Me.lblPumpliang1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'FrmPump
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(662, 482)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdHelpPumpVol)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.fraMudflow)
        Me.Controls.Add(Me.fraPump2)
        Me.Controls.Add(Me.fraPump1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(117, 115)
        Me.MaximizeBox = False
        Me.Name = "FrmPump"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "由泥浆泵基本参数求排量"
        Me.fraMudflow.ResumeLayout(False)
        Me.fraPump2.ResumeLayout(False)
        Me.fraPump1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As FrmPump
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As FrmPump
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New FrmPump()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim PumpDia1, PumpDia2 As Short
	Dim Pumplength1, Pumplength2 As Single
	Dim Pumpgantao1, PumpStorke1, PumpStorke2, Pumpgantao2 As Short
	Dim Xiaolu1, Xiaolu2 As Single
	Dim Pumpliang1, Pumpliang2 As Short
	Dim Vp1, Mudflow, Vp2 As Single
	Const pi As Double = 3.1415926
	
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 cboPumpDia1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub cboPumpDia1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cboPumpDia1.SelectedIndexChanged
		PumpDia1 = Val(cboPumpDia1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub cboPumpDia1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles cboPumpDia1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cboPumpDia1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cboPumpDia1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpDia1 = Val(cboPumpDia1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 cboPumpDia2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub cboPumpDia2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cboPumpDia2.SelectedIndexChanged
		PumpDia2 = Val(cboPumpDia2.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub cboPumpDia2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles cboPumpDia2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cboPumpDia2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cboPumpDia2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpDia2 = Val(cboPumpDia2.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 cboPumplength1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub cboPumplength1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cboPumplength1.SelectedIndexChanged
		Pumplength1 = Val(cboPumplength1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub cboPumplength1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles cboPumplength1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cboPumplength1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cboPumplength1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumplength1 = Val(cboPumplength1.Text)
	End Sub
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 cboPumplength2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub cboPumplength2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cboPumplength2.SelectedIndexChanged
		Pumplength2 = Val(cboPumplength2.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub cboPumplength2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles cboPumplength2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cboPumplength2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cboPumplength2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumplength2 = Val(cboPumplength2.Text)
	End Sub
	
	Private Sub cmdHelpPumpVol_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelpPumpVol.Click
		frmHelpPumpVol.DefInstance.ShowDialog()
	End Sub
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		Vp1 = pi * (PumpDia1 / 200) ^ 2 * (Pumplength1 / 100) * PumpStorke1 * (Xiaolu1 / 100) * Pumpliang1 * Pumpgantao1
		Vp2 = pi * (PumpDia2 / 200) ^ 2 * (Pumplength2 / 100) * PumpStorke2 * (Xiaolu2 / 100) * Pumpliang2 * Pumpgantao2
		Mudflow = (Vp1 + Vp2) / 60
		txtMudflow.Text = VB6.Format(Mudflow, "0.0")
		txtMudflowUS.Text = VB6.Format(Mudflow / 3.785 * 60, "000")
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub FrmPump_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

		'************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
		
		'*******************************************************************************
		
		
        '加载数据
        '2022年7月27日发现赋值失败，需要在vb.net中直接填写数值
		
		FrmPump.DefInstance.Left = VB6.TwipsToPixelsX((VB6.PixelsToTwipsX(MDIFrmMud.DefInstance.Width) - VB6.PixelsToTwipsX(FrmPump.DefInstance.Width)) / 2) '泵排量计算窗口位置居中计算
		FrmPump.DefInstance.Top = VB6.TwipsToPixelsY((VB6.PixelsToTwipsY(MDIFrmMud.DefInstance.Height) - VB6.PixelsToTwipsY(FrmPump.DefInstance.Height)) / 2) '泵排量计算窗口位置居中计算
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdExit.Visible = False '双击logo 图案开始截图，设置命令按钮隐藏'
		cmdok.Visible = False
		cmdHelpPumpVol.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MPumpFlow.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MPumpFlow.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        'cmdPicSave.Visible = True '作废 的控件

        cmdExit.Visible = True
        cmdok.Visible = True
        cmdHelpPumpVol.Visible = True
	End Sub
	
	
	Private Sub txtPumpgantao1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPumpgantao1.Enter
		Pumpgantao1 = Val(txtPumpgantao1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpgantao1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpgantao1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpgantao1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpgantao1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumpgantao1 = Val(txtPumpgantao1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpgantao2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpgantao2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpgantao2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpgantao2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumpgantao2 = Val(txtPumpgantao2.Text)
	End Sub
	
	Private Sub txtPumpliang1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPumpliang1.Enter
		Pumpliang1 = Val(txtPumpliang1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpliang1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpliang1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpliang1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpliang1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumpliang1 = Val(txtPumpliang1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 txtPumpliang2.TextChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub txtPumpliang2_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPumpliang2.TextChanged
		Pumpliang2 = Val(txtPumpliang2.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpliang2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpliang2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpliang2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpliang2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Pumpliang2 = Val(txtPumpliang2.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpStorke1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpStorke1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpStorke1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpStorke1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpStorke1 = Val(txtPumpStorke1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtPumpStorke2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtPumpStorke2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtPumpStorke2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPumpStorke2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpStorke2 = Val(txtPumpStorke2.Text)
	End Sub
	
	Private Sub txtXiaolu1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXiaolu1.Enter
		Xiaolu1 = Val(txtXiaolu1.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtXiaolu1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtXiaolu1.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtXiaolu1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtXiaolu1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Xiaolu1 = Val(txtXiaolu1.Text)
	End Sub
	
	Private Sub txtXiaolu2_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtXiaolu2.Enter
		Xiaolu2 = Val(txtXiaolu2.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtXiaolu2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtXiaolu2.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtXiaolu2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtXiaolu2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Xiaolu2 = Val(txtXiaolu2.Text)
	End Sub

    
    Private Sub lblMudflow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMudflow.Click

    End Sub
End Class