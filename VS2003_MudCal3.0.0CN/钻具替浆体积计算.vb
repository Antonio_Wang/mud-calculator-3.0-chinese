Option Strict Off
Option Explicit On
Friend Class Form8
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents PicLogo As System.Windows.Forms.PictureBox

	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdHelp As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents Text12 As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Combo1 As System.Windows.Forms.ComboBox
	Public WithEvents Combo2 As System.Windows.Forms.ComboBox
	Public WithEvents Combo3 As System.Windows.Forms.ComboBox
	Public WithEvents Combo4 As System.Windows.Forms.ComboBox
	Public WithEvents Combo5 As System.Windows.Forms.ComboBox
	Public WithEvents Combo6 As System.Windows.Forms.ComboBox
	Public WithEvents Combo7 As System.Windows.Forms.ComboBox
	Public WithEvents Combo8 As System.Windows.Forms.ComboBox
	Public WithEvents Combo9 As System.Windows.Forms.ComboBox
	Public WithEvents Combo10 As System.Windows.Forms.ComboBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picDrillstringDisplacement As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form8))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picDrillstringDisplacement = New System.Windows.Forms.PictureBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdHelp = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text12 = New System.Windows.Forms.TextBox
        Me.Text11 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Text10 = New System.Windows.Forms.TextBox
        Me.Text9 = New System.Windows.Forms.TextBox
        Me.Text8 = New System.Windows.Forms.TextBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Combo1 = New System.Windows.Forms.ComboBox
        Me.Combo2 = New System.Windows.Forms.ComboBox
        Me.Combo3 = New System.Windows.Forms.ComboBox
        Me.Combo4 = New System.Windows.Forms.ComboBox
        Me.Combo5 = New System.Windows.Forms.ComboBox
        Me.Combo6 = New System.Windows.Forms.ComboBox
        Me.Combo7 = New System.Windows.Forms.ComboBox
        Me.Combo8 = New System.Windows.Forms.ComboBox
        Me.Combo9 = New System.Windows.Forms.ComboBox
        Me.Combo10 = New System.Windows.Forms.ComboBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picDrillstringDisplacement
        '
        Me.picDrillstringDisplacement.Image = CType(resources.GetObject("picDrillstringDisplacement.Image"), System.Drawing.Image)
        Me.picDrillstringDisplacement.Location = New System.Drawing.Point(664, 16)
        Me.picDrillstringDisplacement.Name = "picDrillstringDisplacement"
        Me.picDrillstringDisplacement.Size = New System.Drawing.Size(216, 552)
        Me.picDrillstringDisplacement.TabIndex = 39
        Me.picDrillstringDisplacement.TabStop = False
        Me.ToolTip1.SetToolTip(Me.picDrillstringDisplacement, "45Rig,Dacaidan Qinghai China")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(472, 488)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(174, 62)
        Me.PicLogo.TabIndex = 38
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(530, 428)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 18
        Me.cmdExit.Text = "Exit"
        '
        'cmdHelp
        '
        Me.cmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdHelp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdHelp.Location = New System.Drawing.Point(530, 456)
        Me.cmdHelp.Name = "cmdHelp"
        Me.cmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdHelp.Size = New System.Drawing.Size(80, 20)
        Me.cmdHelp.TabIndex = 17
        Me.cmdHelp.Text = "Help"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(530, 400)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 16
        Me.cmdok.Text = "Run"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text12)
        Me.Frame2.Controls.Add(Me.Text11)
        Me.Frame2.Controls.Add(Me.Label11)
        Me.Frame2.Controls.Add(Me.Label10)
        Me.Frame2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(8, 368)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(424, 109)
        Me.Frame2.TabIndex = 35
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "计算结果(整套钻具替浆体积)"
        '
        'Text12
        '
        Me.Text12.AcceptsReturn = True
        Me.Text12.AutoSize = False
        Me.Text12.BackColor = System.Drawing.SystemColors.Window
        Me.Text12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text12.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text12.Location = New System.Drawing.Point(240, 52)
        Me.Text12.MaxLength = 0
        Me.Text12.Name = "Text12"
        Me.Text12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text12.Size = New System.Drawing.Size(126, 19)
        Me.Text12.TabIndex = 20
        Me.Text12.Text = ""
        '
        'Text11
        '
        Me.Text11.AcceptsReturn = True
        Me.Text11.AutoSize = False
        Me.Text11.BackColor = System.Drawing.SystemColors.Window
        Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text11.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text11.Location = New System.Drawing.Point(38, 52)
        Me.Text11.MaxLength = 0
        Me.Text11.Name = "Text11"
        Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text11.Size = New System.Drawing.Size(98, 19)
        Me.Text11.TabIndex = 19
        Me.Text11.Text = ""
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(259, 26)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(78, 18)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "(BBl)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(48, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(78, 18)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "(m^3)"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Text10)
        Me.Frame1.Controls.Add(Me.Text9)
        Me.Frame1.Controls.Add(Me.Text8)
        Me.Frame1.Controls.Add(Me.Text7)
        Me.Frame1.Controls.Add(Me.Text6)
        Me.Frame1.Controls.Add(Me.Combo1)
        Me.Frame1.Controls.Add(Me.Combo2)
        Me.Frame1.Controls.Add(Me.Combo3)
        Me.Frame1.Controls.Add(Me.Combo4)
        Me.Frame1.Controls.Add(Me.Combo5)
        Me.Frame1.Controls.Add(Me.Combo6)
        Me.Frame1.Controls.Add(Me.Combo7)
        Me.Frame1.Controls.Add(Me.Combo8)
        Me.Frame1.Controls.Add(Me.Combo9)
        Me.Frame1.Controls.Add(Me.Combo10)
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Text4)
        Me.Frame1.Controls.Add(Me.Text5)
        Me.Frame1.Controls.Add(Me.Label9)
        Me.Frame1.Controls.Add(Me.Label1)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Controls.Add(Me.Label5)
        Me.Frame1.Controls.Add(Me.Label6)
        Me.Frame1.Controls.Add(Me.Label7)
        Me.Frame1.Controls.Add(Me.Label8)
        Me.Frame1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Blue
        Me.Frame1.Location = New System.Drawing.Point(8, 8)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(640, 352)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "Data"
        '
        'Text10
        '
        Me.Text10.AcceptsReturn = True
        Me.Text10.AutoSize = False
        Me.Text10.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text10.Location = New System.Drawing.Point(538, 293)
        Me.Text10.MaxLength = 0
        Me.Text10.Name = "Text10"
        Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text10.Size = New System.Drawing.Size(68, 19)
        Me.Text10.TabIndex = 15
        Me.Text10.Text = ""
        '
        'Text9
        '
        Me.Text9.AcceptsReturn = True
        Me.Text9.AutoSize = False
        Me.Text9.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text9.Location = New System.Drawing.Point(538, 233)
        Me.Text9.MaxLength = 0
        Me.Text9.Name = "Text9"
        Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text9.Size = New System.Drawing.Size(68, 19)
        Me.Text9.TabIndex = 12
        Me.Text9.Text = ""
        '
        'Text8
        '
        Me.Text8.AcceptsReturn = True
        Me.Text8.AutoSize = False
        Me.Text8.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text8.Location = New System.Drawing.Point(538, 181)
        Me.Text8.MaxLength = 0
        Me.Text8.Name = "Text8"
        Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text8.Size = New System.Drawing.Size(68, 19)
        Me.Text8.TabIndex = 9
        Me.Text8.Text = ""
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(538, 121)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(68, 19)
        Me.Text7.TabIndex = 6
        Me.Text7.Text = ""
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(538, 69)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(68, 19)
        Me.Text6.TabIndex = 3
        Me.Text6.Text = ""
        '
        'Combo1
        '
        Me.Combo1.BackColor = System.Drawing.SystemColors.Window
        Me.Combo1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo1.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo1.Location = New System.Drawing.Point(96, 69)
        Me.Combo1.Name = "Combo1"
        Me.Combo1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo1.Size = New System.Drawing.Size(68, 22)
        Me.Combo1.TabIndex = 1
        '
        'Combo2
        '
        Me.Combo2.BackColor = System.Drawing.SystemColors.Window
        Me.Combo2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo2.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo2.Location = New System.Drawing.Point(96, 121)
        Me.Combo2.Name = "Combo2"
        Me.Combo2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo2.Size = New System.Drawing.Size(68, 22)
        Me.Combo2.TabIndex = 4
        '
        'Combo3
        '
        Me.Combo3.BackColor = System.Drawing.SystemColors.Window
        Me.Combo3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo3.Items.AddRange(New Object() {"79.4", "88.9", "104.78", "120.65", "127.00", "146.05", "152.40", "158.75", "165.10", "171.45", "177.80", "184.15", "196.85", "203.20", "209.6", "228.60", "241.3", "247.7", "254.0", "279.4"})
        Me.Combo3.Location = New System.Drawing.Point(96, 181)
        Me.Combo3.Name = "Combo3"
        Me.Combo3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo3.Size = New System.Drawing.Size(68, 22)
        Me.Combo3.TabIndex = 7
        '
        'Combo4
        '
        Me.Combo4.BackColor = System.Drawing.SystemColors.Window
        Me.Combo4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo4.Items.AddRange(New Object() {"60.324", "73.024", "88.90", "101.60", "114.30", "118.60", "127.00", "139.70", "168.27"})
        Me.Combo4.Location = New System.Drawing.Point(96, 233)
        Me.Combo4.Name = "Combo4"
        Me.Combo4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo4.Size = New System.Drawing.Size(68, 22)
        Me.Combo4.TabIndex = 10
        '
        'Combo5
        '
        Me.Combo5.BackColor = System.Drawing.SystemColors.Window
        Me.Combo5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo5.Items.AddRange(New Object() {"60.324", "73.024", "88.90", "101.60", "114.30", "118.60", "127.00", "139.70", "168.27"})
        Me.Combo5.Location = New System.Drawing.Point(96, 293)
        Me.Combo5.Name = "Combo5"
        Me.Combo5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo5.Size = New System.Drawing.Size(68, 22)
        Me.Combo5.TabIndex = 13
        '
        'Combo6
        '
        Me.Combo6.BackColor = System.Drawing.SystemColors.Window
        Me.Combo6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo6.Items.AddRange(New Object() {"31.8", "38.1", "50.8", "57.15", "71.44", "76.20"})
        Me.Combo6.Location = New System.Drawing.Point(192, 69)
        Me.Combo6.Name = "Combo6"
        Me.Combo6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo6.Size = New System.Drawing.Size(68, 22)
        Me.Combo6.TabIndex = 2
        '
        'Combo7
        '
        Me.Combo7.BackColor = System.Drawing.SystemColors.Window
        Me.Combo7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo7.Items.AddRange(New Object() {"31.8", "38.1", "50.8", "57.15", "71.44", "76.20"})
        Me.Combo7.Location = New System.Drawing.Point(192, 121)
        Me.Combo7.Name = "Combo7"
        Me.Combo7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo7.Size = New System.Drawing.Size(68, 22)
        Me.Combo7.TabIndex = 5
        '
        'Combo8
        '
        Me.Combo8.BackColor = System.Drawing.SystemColors.Window
        Me.Combo8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo8.Items.AddRange(New Object() {"31.8", "38.1", "50.8", "57.15", "71.44", "76.20"})
        Me.Combo8.Location = New System.Drawing.Point(192, 181)
        Me.Combo8.Name = "Combo8"
        Me.Combo8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo8.Size = New System.Drawing.Size(68, 22)
        Me.Combo8.TabIndex = 8
        '
        'Combo9
        '
        Me.Combo9.BackColor = System.Drawing.SystemColors.Window
        Me.Combo9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo9.Items.AddRange(New Object() {"46.10", "50.7", "50.8", "54.64", "62.00", "63.50", "66.09", "70.21", "71.40", "76.00", "76.20", "84.84", "88.29", "92.46", "95.18", "95.35", "100.53", "108.61", "111.96", "118.62", "121.36", "151.51"})
        Me.Combo9.Location = New System.Drawing.Point(192, 233)
        Me.Combo9.Name = "Combo9"
        Me.Combo9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo9.Size = New System.Drawing.Size(68, 22)
        Me.Combo9.TabIndex = 11
        '
        'Combo10
        '
        Me.Combo10.BackColor = System.Drawing.SystemColors.Window
        Me.Combo10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo10.Items.AddRange(New Object() {"46.10", "50.7", "50.8", "54.64", "62.00", "63.50", "66.09", "70.21", "71.40", "76.00", "76.20", "84.84", "88.29", "92.46", "95.18", "95.35", "100.53", "108.61", "111.96", "118.62", "121.36", "151.51"})
        Me.Combo10.Location = New System.Drawing.Point(192, 293)
        Me.Combo10.Name = "Combo10"
        Me.Combo10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo10.Size = New System.Drawing.Size(68, 22)
        Me.Combo10.TabIndex = 14
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(336, 69)
        Me.Text1.MaxLength = 3
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(68, 19)
        Me.Text1.TabIndex = 21
        Me.Text1.Text = ""
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(336, 121)
        Me.Text2.MaxLength = 3
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(68, 19)
        Me.Text2.TabIndex = 22
        Me.Text2.Text = ""
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(336, 181)
        Me.Text3.MaxLength = 3
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(68, 19)
        Me.Text3.TabIndex = 23
        Me.Text3.Text = ""
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(336, 233)
        Me.Text4.MaxLength = 3
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(68, 19)
        Me.Text4.TabIndex = 24
        Me.Text4.Text = ""
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(336, 293)
        Me.Text5.MaxLength = 3
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(68, 19)
        Me.Text5.TabIndex = 25
        Me.Text5.Text = ""
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(192, Byte), CType(255, Byte), CType(192, Byte))
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(538, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(68, 19)
        Me.Label9.TabIndex = 34
        Me.Label9.Text = "长度(m)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(19, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(59, 18)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "1#钻铤"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(19, 126)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(59, 18)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "2#钻铤"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(19, 183)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(59, 18)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "3#钻铤"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(19, 239)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(59, 18)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "1#钻杆"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(19, 296)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(59, 18)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "2#钻杆"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(106, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(68, 19)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "外径(mm)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(202, 34)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(68, 19)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "内径(mm)"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(305, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(130, 27)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "替浆体积 m^3/100m"
        '
        'Form8
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.cmdExit
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picDrillstringDisplacement)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdHelp)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "Form8"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻具替浆体积计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As Form8
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form8
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form8()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim Tw1 As Single 'Tw钻铤外径 Tn钻铤内径 Tc钻铤长度
	Dim Tw2 As Single
	Dim Tw3 As Single
	Dim Gw1 As Single ' Gw钻杆外径 Gn钻杆内径 Gc钻杆长度
	Dim Gw2 As Single
	Dim Tn1 As Single
	Dim Tn2 As Single
	Dim Tn3 As Single
	Dim Gn1 As Single
	Dim Gn2 As Single
	Dim Tc1 As Single
	Dim Tc2 As Single
	Dim Tc3 As Single
	Dim Gc1 As Single
	Dim Gc2 As Single
	Dim Vt1 As Single '钻具截面积
	Dim Vt2 As Single
	Dim Vt3 As Single
	Dim Vg1 As Single
	Dim Vg2 As Single
	Dim Vzhong As Single
	Const pi As Double = 3.1415926
	Dim Vzt1 As Single
	Dim Vzt2 As Single
	Dim Vzt3 As Single
	Dim Vzg1 As Single
	Dim Vzg2 As Single
	
	'2012年3月29日填加的屏幕截图----------
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	'*******************************************
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo1.SelectedIndexChanged
		Tw1 = Val(Combo1.Text)
	End Sub
	
	Private Sub Combo1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw1 = Val(Combo1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo10.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo10_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo10.SelectedIndexChanged
		Gn2 = Val(Combo10.Text)
	End Sub
	
	Private Sub Combo10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gn2 = Val(Combo10.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo2.SelectedIndexChanged
		Tw2 = Val(Combo2.Text)
	End Sub
	
	Private Sub Combo2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw2 = Val(Combo2.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo3.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo3.SelectedIndexChanged
		Tw3 = Val(Combo3.Text)
	End Sub
	
	Private Sub Combo3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw3 = Val(Combo3.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo4.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo4.SelectedIndexChanged
		Gw1 = Val(Combo4.Text)
	End Sub
	
	Private Sub Combo4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw1 = Val(Combo4.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo5.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo5_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo5.SelectedIndexChanged
		Gw2 = Val(Combo5.Text)
	End Sub
	
	Private Sub Combo5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw2 = Val(Combo5.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo6.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo6_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo6.SelectedIndexChanged
		Tn1 = Val(Combo6.Text)
	End Sub
	
	Private Sub Combo6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn1 = Val(Combo6.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo7.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo7_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo7.SelectedIndexChanged
		Tn2 = Val(Combo7.Text)
	End Sub
	
	Private Sub Combo7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn2 = Val(Combo7.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo8.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo8_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo8.SelectedIndexChanged
		Tn3 = Val(Combo8.Text)
	End Sub
	
	Private Sub Combo8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn3 = Val(Combo8.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo9.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo9_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo9.SelectedIndexChanged
		Gn1 = Val(Combo9.Text)
	End Sub
	
	Private Sub Combo9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gn1 = Val(Combo9.Text)
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Vt1 = 0.25 * pi * ((Tw1 / 1000) ^ 2 - (Tn1 / 1000) ^ 2)
		Vt2 = 0.25 * pi * ((Tw2 / 1000) ^ 2 - (Tn2 / 1000) ^ 2)
		Vt3 = 0.25 * pi * ((Tw3 / 1000) ^ 2 - (Tn3 / 1000) ^ 2)
		Vg1 = 0.25 * pi * ((Gw1 / 1000) ^ 2 - (Gn1 / 1000) ^ 2)
		Vg2 = 0.25 * pi * ((Gw2 / 1000) ^ 2 - (Gn2 / 1000) ^ 2)
		Vzt1 = Vt1 * 100
		Vzt2 = Vt2 * 100
		Vzt3 = Vt3 * 100
		Vzg1 = Vg1 * 100
		Vzg2 = Vg2 * 100
		If Vzt1 > 0 Then
			Text1.Text = VB6.Format(Vzt1, "0.000")
		End If
		If Vzt2 > 0 Then
			Text2.Text = VB6.Format(Vzt2, "0.000")
		End If
		If Vzt3 > 0 Then
			Text3.Text = VB6.Format(Vzt3, "0.000")
		End If
		If Vzg1 > 0 Then
			Text4.Text = VB6.Format(Vzg1, "0.000")
		End If
		If Vzg2 > 0 Then
			Text5.Text = VB6.Format(Vzg2, "0.000")
		End If
		Vzhong = Vt1 * Tc1 + Vt2 * Tc2 + Vt3 * Tc3 + Vg1 * Gc1 + Vg2 * Gc2
		Text11.Text = VB6.Format(Vzhong, "0.00")
		Text12.Text = VB6.Format(Vzhong * 6.289, "0.0")
	End Sub
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
        frmHelpDisplace.DefInstance.ShowDialog()
        'frmHelpDisplace.DefInstance.Show()
        'frmHelpDisplace.DefInstance.Activate()
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub Form8_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************






        '-------------------------------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块中的过程
		'--------------------------------------------------

		

		
		
		
	End Sub
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		cmdHelp.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MDisplacement.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MDisplacement.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        cmdHelp.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text6_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text6.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc1 = Val(Text6.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text7_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text7.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc2 = Val(Text7.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text8_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text8.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc3 = Val(Text8.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text9_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text9.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gc1 = Val(Text9.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub Text10_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text10.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub Text10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gc2 = Val(Text10.Text)
	End Sub

    
    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click

    End Sub
End Class