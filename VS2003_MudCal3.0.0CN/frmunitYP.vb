Option Strict Off
Option Explicit On
Friend Class frmunitYP
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtYPlbf100ft2 As System.Windows.Forms.TextBox
	Public WithEvents txtYPdyncm2 As System.Windows.Forms.TextBox
	Public WithEvents txtYPpa As System.Windows.Forms.TextBox
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents lblYPlbf100ft As System.Windows.Forms.Label
	Public WithEvents lblYPdyncm2 As System.Windows.Forms.Label
	Public WithEvents lblYPpa As System.Windows.Forms.Label
	Public WithEvents lblYP As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblYPlbf100ft = New System.Windows.Forms.Label
        Me.lblYPdyncm2 = New System.Windows.Forms.Label
        Me.lblYPpa = New System.Windows.Forms.Label
        Me.txtYPlbf100ft2 = New System.Windows.Forms.TextBox
        Me.txtYPdyncm2 = New System.Windows.Forms.TextBox
        Me.txtYPpa = New System.Windows.Forms.TextBox
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.lblYP = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblYPlbf100ft
        '
        Me.lblYPlbf100ft.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPlbf100ft.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPlbf100ft.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPlbf100ft.Location = New System.Drawing.Point(182, 52)
        Me.lblYPlbf100ft.Name = "lblYPlbf100ft"
        Me.lblYPlbf100ft.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPlbf100ft.Size = New System.Drawing.Size(88, 18)
        Me.lblYPlbf100ft.TabIndex = 8
        Me.lblYPlbf100ft.Text = "lbf/100ft^2"
        Me.ToolTip1.SetToolTip(Me.lblYPlbf100ft, "磅力/100平方英尺")
        '
        'lblYPdyncm2
        '
        Me.lblYPdyncm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPdyncm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPdyncm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPdyncm2.Location = New System.Drawing.Point(182, 103)
        Me.lblYPdyncm2.Name = "lblYPdyncm2"
        Me.lblYPdyncm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPdyncm2.Size = New System.Drawing.Size(88, 19)
        Me.lblYPdyncm2.TabIndex = 7
        Me.lblYPdyncm2.Text = "dynes/cm^2"
        Me.ToolTip1.SetToolTip(Me.lblYPdyncm2, "达因/平方厘米")
        '
        'lblYPpa
        '
        Me.lblYPpa.BackColor = System.Drawing.SystemColors.Control
        Me.lblYPpa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYPpa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYPpa.Location = New System.Drawing.Point(182, 164)
        Me.lblYPpa.Name = "lblYPpa"
        Me.lblYPpa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYPpa.Size = New System.Drawing.Size(69, 18)
        Me.lblYPpa.TabIndex = 6
        Me.lblYPpa.Text = "Pa"
        Me.ToolTip1.SetToolTip(Me.lblYPpa, "帕[斯卡]")
        '
        'txtYPlbf100ft2
        '
        Me.txtYPlbf100ft2.AcceptsReturn = True
        Me.txtYPlbf100ft2.AutoSize = False
        Me.txtYPlbf100ft2.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPlbf100ft2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPlbf100ft2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPlbf100ft2.Location = New System.Drawing.Point(29, 43)
        Me.txtYPlbf100ft2.MaxLength = 0
        Me.txtYPlbf100ft2.Name = "txtYPlbf100ft2"
        Me.txtYPlbf100ft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPlbf100ft2.Size = New System.Drawing.Size(145, 27)
        Me.txtYPlbf100ft2.TabIndex = 4
        Me.txtYPlbf100ft2.Text = ""
        '
        'txtYPdyncm2
        '
        Me.txtYPdyncm2.AcceptsReturn = True
        Me.txtYPdyncm2.AutoSize = False
        Me.txtYPdyncm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPdyncm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPdyncm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPdyncm2.Location = New System.Drawing.Point(29, 95)
        Me.txtYPdyncm2.MaxLength = 0
        Me.txtYPdyncm2.Name = "txtYPdyncm2"
        Me.txtYPdyncm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPdyncm2.Size = New System.Drawing.Size(145, 27)
        Me.txtYPdyncm2.TabIndex = 3
        Me.txtYPdyncm2.Text = ""
        '
        'txtYPpa
        '
        Me.txtYPpa.AcceptsReturn = True
        Me.txtYPpa.AutoSize = False
        Me.txtYPpa.BackColor = System.Drawing.SystemColors.Window
        Me.txtYPpa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYPpa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtYPpa.Location = New System.Drawing.Point(29, 155)
        Me.txtYPpa.MaxLength = 0
        Me.txtYPpa.Name = "txtYPpa"
        Me.txtYPpa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtYPpa.Size = New System.Drawing.Size(145, 27)
        Me.txtYPpa.TabIndex = 2
        Me.txtYPpa.Text = ""
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(163, 198)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 1
        Me.cmdclear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(38, 198)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 0
        Me.cmdquit.Text = "退出"
        '
        'lblYP
        '
        Me.lblYP.BackColor = System.Drawing.SystemColors.Control
        Me.lblYP.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblYP.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblYP.Location = New System.Drawing.Point(19, 17)
        Me.lblYP.Name = "lblYP"
        Me.lblYP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblYP.Size = New System.Drawing.Size(232, 19)
        Me.lblYP.TabIndex = 5
        Me.lblYP.Text = "输入剪切应力单位换算数据："
        '
        'frmunitYP
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(290, 246)
        Me.Controls.Add(Me.txtYPlbf100ft2)
        Me.Controls.Add(Me.txtYPdyncm2)
        Me.Controls.Add(Me.txtYPpa)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.lblYPlbf100ft)
        Me.Controls.Add(Me.lblYPdyncm2)
        Me.Controls.Add(Me.lblYPpa)
        Me.Controls.Add(Me.lblYP)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmunitYP"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "钻井液剪切应力换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitYP
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitYP
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitYP()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月9日17:43于青海省海西洲马海马北油田
	'钻井液剪切应力单位换算
	
	
	Dim lbf100ft2 As Single
	Dim dyncm2 As Single
	Dim pa As Single
	
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitYP.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtYPlbf100ft2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPlbf100ft2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbf100ft2 = Val(txtYPlbf100ft2.Text)
		
		dyncm2 = lbf100ft2 * 4.78964
		pa = lbf100ft2 * 0.478964
		
		txtYPdyncm2.Text = VB6.Format(dyncm2, "##0.000")
		txtYPpa.Text = VB6.Format(pa, "##0.000")
		
	End Sub
	
	Private Sub txtYPdyncm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPdyncm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dyncm2 = Val(txtYPdyncm2.Text)
		
		lbf100ft2 = dyncm2 / 4.78964
		pa = dyncm2 / 10
		
		txtYPlbf100ft2.Text = VB6.Format(lbf100ft2, "##0.000")
		txtYPpa.Text = VB6.Format(pa, "##0.000")
	End Sub
	
	Private Sub txtYPpa_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtYPpa.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		pa = Val(txtYPpa.Text)
		
		lbf100ft2 = pa * 2.08784
		dyncm2 = pa * 10
		
		txtYPlbf100ft2.Text = VB6.Format(lbf100ft2, "##0.000")
		txtYPdyncm2.Text = VB6.Format(dyncm2, "##0.000")
	End Sub
End Class