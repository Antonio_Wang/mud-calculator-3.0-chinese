Option Strict Off
Option Explicit On
Friend Class frmHhydraulicCal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents picIcon As System.Windows.Forms.PictureBox
    Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents txtHDmlk As System.Windows.Forms.TextBox
	Public WithEvents txtHDmln As System.Windows.Forms.TextBox
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents txtHDksjx As System.Windows.Forms.TextBox
	Public WithEvents txtHDksup As System.Windows.Forms.TextBox
	Public WithEvents txtHDksyp As System.Windows.Forms.TextBox
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents txtHDbhypup As System.Windows.Forms.TextBox
	Public WithEvents txtHDbhupv As System.Windows.Forms.TextBox
	Public WithEvents txtHDbhyp As System.Windows.Forms.TextBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents txtHDs3 As System.Windows.Forms.TextBox
	Public WithEvents txtHDs6 As System.Windows.Forms.TextBox
	Public WithEvents lblHDs3 As System.Windows.Forms.Label
	Public WithEvents lblHDs6 As System.Windows.Forms.Label
	Public WithEvents fraHDinput As System.Windows.Forms.GroupBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents fraHDprogram As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picBaseMudRhelogy As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmHhydraulicCal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtHDksjx = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdok = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.Frame5 = New System.Windows.Forms.GroupBox
        Me.txtHDmlk = New System.Windows.Forms.TextBox
        Me.txtHDmln = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Frame4 = New System.Windows.Forms.GroupBox
        Me.txtHDksup = New System.Windows.Forms.TextBox
        Me.txtHDksyp = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Frame3 = New System.Windows.Forms.GroupBox
        Me.txtHDbhypup = New System.Windows.Forms.TextBox
        Me.txtHDbhupv = New System.Windows.Forms.TextBox
        Me.txtHDbhyp = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.fraHDinput = New System.Windows.Forms.GroupBox
        Me.txtHDs3 = New System.Windows.Forms.TextBox
        Me.txtHDs6 = New System.Windows.Forms.TextBox
        Me.lblHDs3 = New System.Windows.Forms.Label
        Me.lblHDs6 = New System.Windows.Forms.Label
        Me.fraHDprogram = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.picBaseMudRhelogy = New System.Windows.Forms.PictureBox
        Me.Frame5.SuspendLayout()
        Me.Frame4.SuspendLayout()
        Me.Frame3.SuspendLayout()
        Me.fraHDinput.SuspendLayout()
        Me.fraHDprogram.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtHDksjx
        '
        Me.txtHDksjx.AcceptsReturn = True
        Me.txtHDksjx.AutoSize = False
        Me.txtHDksjx.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDksjx.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDksjx.ForeColor = System.Drawing.Color.Red
        Me.txtHDksjx.Location = New System.Drawing.Point(220, 94)
        Me.txtHDksjx.MaxLength = 0
        Me.txtHDksjx.Name = "txtHDksjx"
        Me.txtHDksjx.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDksjx.Size = New System.Drawing.Size(88, 20)
        Me.txtHDksjx.TabIndex = 24
        Me.txtHDksjx.Text = ""
        Me.txtHDksjx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtHDksjx, "分散钻井液的Im一般小于200，不分散聚合物钻井液和适度絮凝的抑制性钻井液的Im值常在300～600之间，高者可达800以上。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(8, 391)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(160, 86)
        Me.PicLogo.TabIndex = 29
        Me.PicLogo.TabStop = False
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(208, 375)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 7
        Me.cmdok.Text = "Run"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(208, 403)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 6
        Me.cmdclear.Text = "Clean"
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(208, 431)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 5
        Me.cmdExit.Text = "Exit"
        '
        'Frame5
        '
        Me.Frame5.BackColor = System.Drawing.SystemColors.Control
        Me.Frame5.Controls.Add(Me.txtHDmlk)
        Me.Frame5.Controls.Add(Me.txtHDmln)
        Me.Frame5.Controls.Add(Me.Label9)
        Me.Frame5.Controls.Add(Me.Label8)
        Me.Frame5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame5.ForeColor = System.Drawing.Color.Red
        Me.Frame5.Location = New System.Drawing.Point(382, 280)
        Me.Frame5.Name = "Frame5"
        Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame5.Size = New System.Drawing.Size(327, 134)
        Me.Frame5.TabIndex = 4
        Me.Frame5.TabStop = False
        Me.Frame5.Text = "幂律模式计算结果"
        '
        'txtHDmlk
        '
        Me.txtHDmlk.AcceptsReturn = True
        Me.txtHDmlk.AutoSize = False
        Me.txtHDmlk.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDmlk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDmlk.ForeColor = System.Drawing.Color.Red
        Me.txtHDmlk.Location = New System.Drawing.Point(220, 90)
        Me.txtHDmlk.MaxLength = 0
        Me.txtHDmlk.Name = "txtHDmlk"
        Me.txtHDmlk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDmlk.Size = New System.Drawing.Size(88, 23)
        Me.txtHDmlk.TabIndex = 28
        Me.txtHDmlk.Text = ""
        Me.txtHDmlk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHDmln
        '
        Me.txtHDmln.AcceptsReturn = True
        Me.txtHDmln.AutoSize = False
        Me.txtHDmln.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDmln.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDmln.ForeColor = System.Drawing.Color.Red
        Me.txtHDmln.Location = New System.Drawing.Point(220, 34)
        Me.txtHDmln.MaxLength = 0
        Me.txtHDmln.Name = "txtHDmln"
        Me.txtHDmln.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDmln.Size = New System.Drawing.Size(88, 23)
        Me.txtHDmln.TabIndex = 27
        Me.txtHDmln.Text = ""
        Me.txtHDmln.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(24, 90)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(136, 23)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "稠度系数K(Pa.s^n)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(24, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(136, 23)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "流型指数n"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Frame4
        '
        Me.Frame4.BackColor = System.Drawing.SystemColors.Control
        Me.Frame4.Controls.Add(Me.txtHDksjx)
        Me.Frame4.Controls.Add(Me.txtHDksup)
        Me.Frame4.Controls.Add(Me.txtHDksyp)
        Me.Frame4.Controls.Add(Me.Label7)
        Me.Frame4.Controls.Add(Me.Label6)
        Me.Frame4.Controls.Add(Me.Label5)
        Me.Frame4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame4.ForeColor = System.Drawing.Color.Red
        Me.Frame4.Location = New System.Drawing.Point(382, 146)
        Me.Frame4.Name = "Frame4"
        Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame4.Size = New System.Drawing.Size(327, 126)
        Me.Frame4.TabIndex = 3
        Me.Frame4.TabStop = False
        Me.Frame4.Text = "卡森模式计算结果"
        '
        'txtHDksup
        '
        Me.txtHDksup.AcceptsReturn = True
        Me.txtHDksup.AutoSize = False
        Me.txtHDksup.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDksup.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDksup.ForeColor = System.Drawing.Color.Red
        Me.txtHDksup.Location = New System.Drawing.Point(220, 60)
        Me.txtHDksup.MaxLength = 0
        Me.txtHDksup.Name = "txtHDksup"
        Me.txtHDksup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDksup.Size = New System.Drawing.Size(88, 19)
        Me.txtHDksup.TabIndex = 23
        Me.txtHDksup.Text = ""
        Me.txtHDksup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHDksyp
        '
        Me.txtHDksyp.AcceptsReturn = True
        Me.txtHDksyp.AutoSize = False
        Me.txtHDksyp.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDksyp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDksyp.ForeColor = System.Drawing.Color.Red
        Me.txtHDksyp.Location = New System.Drawing.Point(220, 26)
        Me.txtHDksyp.MaxLength = 0
        Me.txtHDksyp.Name = "txtHDksyp"
        Me.txtHDksyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDksyp.Size = New System.Drawing.Size(88, 19)
        Me.txtHDksyp.TabIndex = 22
        Me.txtHDksyp.Text = ""
        Me.txtHDksyp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(24, 95)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(121, 18)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "剪切稀释常数Im"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(24, 60)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(121, 19)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "塑性粘度η∞(mPa.s)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(24, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(121, 18)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "动切力τc(Pa)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Frame3
        '
        Me.Frame3.BackColor = System.Drawing.SystemColors.Control
        Me.Frame3.Controls.Add(Me.txtHDbhypup)
        Me.Frame3.Controls.Add(Me.txtHDbhupv)
        Me.Frame3.Controls.Add(Me.txtHDbhyp)
        Me.Frame3.Controls.Add(Me.Label4)
        Me.Frame3.Controls.Add(Me.Label3)
        Me.Frame3.Controls.Add(Me.Label2)
        Me.Frame3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame3.ForeColor = System.Drawing.Color.Red
        Me.Frame3.Location = New System.Drawing.Point(382, 8)
        Me.Frame3.Name = "Frame3"
        Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame3.Size = New System.Drawing.Size(327, 130)
        Me.Frame3.TabIndex = 2
        Me.Frame3.TabStop = False
        Me.Frame3.Text = "宾汉模式计算结果"
        '
        'txtHDbhypup
        '
        Me.txtHDbhypup.AcceptsReturn = True
        Me.txtHDbhypup.AutoSize = False
        Me.txtHDbhypup.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDbhypup.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDbhypup.ForeColor = System.Drawing.Color.Red
        Me.txtHDbhypup.Location = New System.Drawing.Point(221, 101)
        Me.txtHDbhypup.MaxLength = 0
        Me.txtHDbhypup.Name = "txtHDbhypup"
        Me.txtHDbhypup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDbhypup.Size = New System.Drawing.Size(87, 19)
        Me.txtHDbhypup.TabIndex = 18
        Me.txtHDbhypup.Text = ""
        Me.txtHDbhypup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHDbhupv
        '
        Me.txtHDbhupv.AcceptsReturn = True
        Me.txtHDbhupv.AutoSize = False
        Me.txtHDbhupv.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDbhupv.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDbhupv.ForeColor = System.Drawing.Color.Red
        Me.txtHDbhupv.Location = New System.Drawing.Point(221, 61)
        Me.txtHDbhupv.MaxLength = 0
        Me.txtHDbhupv.Name = "txtHDbhupv"
        Me.txtHDbhupv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDbhupv.Size = New System.Drawing.Size(87, 20)
        Me.txtHDbhupv.TabIndex = 17
        Me.txtHDbhupv.Text = ""
        Me.txtHDbhupv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHDbhyp
        '
        Me.txtHDbhyp.AcceptsReturn = True
        Me.txtHDbhyp.AutoSize = False
        Me.txtHDbhyp.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDbhyp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDbhyp.ForeColor = System.Drawing.Color.Red
        Me.txtHDbhyp.Location = New System.Drawing.Point(221, 24)
        Me.txtHDbhyp.MaxLength = 0
        Me.txtHDbhyp.Name = "txtHDbhyp"
        Me.txtHDbhyp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDbhyp.Size = New System.Drawing.Size(87, 19)
        Me.txtHDbhyp.TabIndex = 16
        Me.txtHDbhyp.Text = ""
        Me.txtHDbhyp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(24, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(179, 23)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "动塑比 Pa·(mPa·s)－1"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(24, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(179, 23)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "塑性粘度PV(mPa.s)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(24, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(179, 23)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "动切力YP (Pa)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraHDinput
        '
        Me.fraHDinput.BackColor = System.Drawing.SystemColors.Control
        Me.fraHDinput.Controls.Add(Me.txtHDs3)
        Me.fraHDinput.Controls.Add(Me.txtHDs6)
        Me.fraHDinput.Controls.Add(Me.lblHDs3)
        Me.fraHDinput.Controls.Add(Me.lblHDs6)
        Me.fraHDinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraHDinput.ForeColor = System.Drawing.Color.Red
        Me.fraHDinput.Location = New System.Drawing.Point(8, 198)
        Me.fraHDinput.Name = "fraHDinput"
        Me.fraHDinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraHDinput.Size = New System.Drawing.Size(366, 169)
        Me.fraHDinput.TabIndex = 1
        Me.fraHDinput.TabStop = False
        Me.fraHDinput.Text = "参数录入"
        '
        'txtHDs3
        '
        Me.txtHDs3.AcceptsReturn = True
        Me.txtHDs3.AutoSize = False
        Me.txtHDs3.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDs3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDs3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHDs3.Location = New System.Drawing.Point(197, 99)
        Me.txtHDs3.MaxLength = 0
        Me.txtHDs3.Name = "txtHDs3"
        Me.txtHDs3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDs3.Size = New System.Drawing.Size(87, 23)
        Me.txtHDs3.TabIndex = 12
        Me.txtHDs3.Text = "34"
        Me.txtHDs3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHDs6
        '
        Me.txtHDs6.AcceptsReturn = True
        Me.txtHDs6.AutoSize = False
        Me.txtHDs6.BackColor = System.Drawing.SystemColors.Window
        Me.txtHDs6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHDs6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHDs6.Location = New System.Drawing.Point(197, 34)
        Me.txtHDs6.MaxLength = 0
        Me.txtHDs6.Name = "txtHDs6"
        Me.txtHDs6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHDs6.Size = New System.Drawing.Size(87, 23)
        Me.txtHDs6.TabIndex = 11
        Me.txtHDs6.Text = "53"
        Me.txtHDs6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHDs3
        '
        Me.lblHDs3.BackColor = System.Drawing.SystemColors.Control
        Me.lblHDs3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHDs3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHDs3.Location = New System.Drawing.Point(24, 103)
        Me.lblHDs3.Name = "lblHDs3"
        Me.lblHDs3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHDs3.Size = New System.Drawing.Size(169, 27)
        Me.lblHDs3.TabIndex = 10
        Me.lblHDs3.Text = "粘度计300转读数(Fann)"
        '
        'lblHDs6
        '
        Me.lblHDs6.BackColor = System.Drawing.SystemColors.Control
        Me.lblHDs6.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHDs6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHDs6.Location = New System.Drawing.Point(24, 39)
        Me.lblHDs6.Name = "lblHDs6"
        Me.lblHDs6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHDs6.Size = New System.Drawing.Size(169, 27)
        Me.lblHDs6.TabIndex = 9
        Me.lblHDs6.Text = "粘度计600转读数(Fann)"
        '
        'fraHDprogram
        '
        Me.fraHDprogram.BackColor = System.Drawing.SystemColors.Control
        Me.fraHDprogram.Controls.Add(Me.Label1)
        Me.fraHDprogram.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraHDprogram.ForeColor = System.Drawing.Color.Blue
        Me.fraHDprogram.Location = New System.Drawing.Point(8, 8)
        Me.fraHDprogram.Name = "fraHDprogram"
        Me.fraHDprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraHDprogram.Size = New System.Drawing.Size(366, 182)
        Me.fraHDprogram.TabIndex = 0
        Me.fraHDprogram.TabStop = False
        Me.fraHDprogram.Text = "程序说明"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(10, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(346, 152)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "                                              此程序对于我国现场常用流变仪(仪器常数为1.078,r ＝n*1.70" & _
        "3，τ=θ*0.511(n为转/分，θ为应力读数))测定结果进行流变参数计算。此程序将计算宾汉、幂律、卡森流体的流变参数，可在计算宾汉、卡森流体时，同时给出动塑" & _
        "比、剪切稀释常数，供钻井液工作者考察钻井液的携屑能力。"
        '
        'picBaseMudRhelogy
        '
        Me.picBaseMudRhelogy.Image = CType(resources.GetObject("picBaseMudRhelogy.Image"), System.Drawing.Image)
        Me.picBaseMudRhelogy.Location = New System.Drawing.Point(728, 16)
        Me.picBaseMudRhelogy.Name = "picBaseMudRhelogy"
        Me.picBaseMudRhelogy.Size = New System.Drawing.Size(144, 550)
        Me.picBaseMudRhelogy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBaseMudRhelogy.TabIndex = 30
        Me.picBaseMudRhelogy.TabStop = False
        '
        'frmHhydraulicCal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picBaseMudRhelogy)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.Frame5)
        Me.Controls.Add(Me.Frame4)
        Me.Controls.Add(Me.Frame3)
        Me.Controls.Add(Me.fraHDinput)
        Me.Controls.Add(Me.fraHDprogram)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "frmHhydraulicCal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "流变参数计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame5.ResumeLayout(False)
        Me.Frame4.ResumeLayout(False)
        Me.Frame3.ResumeLayout(False)
        Me.fraHDinput.ResumeLayout(False)
        Me.fraHDprogram.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmHhydraulicCal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmHhydraulicCal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmHhydraulicCal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim s3 As Single
	Dim s6 As Single
	
	Dim yp As Single
	Dim upv As Single
	Dim ypup As Single
	
	Dim ksyp As Single
	Dim ksup As Single
	Dim ksjx As Single
	
	Dim N As Single
	Dim K As Single
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		
		s6 = Val(txtHDs6.Text)
		If txtHDs6.Text = "" Then
			MsgBox("Please check that you have entered the '600-rpm Reading'data!", MsgBoxStyle.Information, "Base Mud Rhelogy")
			Exit Sub '纠错功能解决。2006年8月8日王朝
		End If
		
		s3 = Val(txtHDs3.Text)
		If txtHDs3.Text = "" Then
			MsgBox("Please check that you have entered the '300-rpm Reading'data!", MsgBoxStyle.Information, "Base Mud Rhelogy")
			Exit Sub '纠错功能解决。2006年8月8日王朝
		End If
		
		upv = s6 - s3

        'yp = 0.511 * (2 * s3 - s6)


        '******************************
        'Mi Swaco手册
        yp = 0.4788 * (2 * s3 - s6)
		ypup = yp / upv
        '*****************************



        '********************************************
        '水眼动切力（Tc）卡森动切力是卡森模式的一个参数，代号为Tc。表示钻井液内可供拆散的网架结构强度。
		ksyp = (1.671 * ((2 * s3) ^ 0.5 - s6 ^ 0.5)) ^ 2
        '********************************************
        '水眼粘度是指钻头水眼处的极限剪切粘度。η∞的大小直接影响钻井的速度，在低固相聚合物钻井液中η∞的数值一般控制在3～6mPa·s
        '
        ksup = (2.414 * (s6 ^ 0.5 - s3 ^ 0.5)) ^ 2
        '***************************
        '剪切稀释常数是卡森模式的一个参数，又称剪切稀释指数，代号为Im
        'Im越大，则剪切稀释性越强。分散钻井液的Im一般小于200，不分散聚合物钻井液和适度絮凝的抑制性钻井液的Im值常在300～600之间，高者可达800以上。但Im值过大会使泵压升高，造成开泵困难。
        ksjx = (1 + (1000 * (ksyp / ksup)) ^ 0.5) ^ 2



        '***************************
        '中国国内常用稠度系数计算
        N = 3.322 * Math.Log10(s6 / s3)

        K = 0.511 * s3 / (511 ^ N)     '单位Pa.s^n


        '**********************
        'Kp  钻具内稠度系数  MI SWACO  BAKE  

        'K = 5.11 * s3 / (511 ^ N)
        'K = 5.11 * s6 / (1022 ^ N)      
		
		txtHDbhupv.Text = VB6.Format(upv, "0.0")
		txtHDbhyp.Text = VB6.Format(yp, "0.0")
		txtHDbhypup.Text = VB6.Format(ypup, "0.00")
		
		txtHDksyp.Text = VB6.Format(ksyp, "0.0")
		txtHDksup.Text = VB6.Format(ksup, "0.0")
		txtHDksjx.Text = VB6.Format(ksjx, "0.00")
		
		txtHDmln.Text = VB6.Format(N, "0.00")
		txtHDmlk.Text = VB6.Format(K, "0.00")
		
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		txtHDs3.Text = ""
		txtHDs6.Text = ""
		
		txtHDbhupv.Text = ""
		txtHDbhyp.Text = ""
		txtHDbhypup.Text = ""
		
		txtHDksyp.Text = ""
		txtHDksup.Text = ""
		txtHDksjx.Text = ""
		
		txtHDmln.Text = ""
		txtHDmlk.Text = ""
		
		
	End Sub
	
	Private Sub frmHhydraulicCal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        'Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************


        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdExit.Visible = False '双击logo 图案开始截图，设置命令按钮隐藏'
		cmdok.Visible = False
		cmdclear.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MRhelogy.bmp")


        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MRhelogy.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdPicSave.Visible = True '作废 的控件

        cmdExit.Visible = True
        cmdok.Visible = True
        cmdclear.Visible = True
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtHDs3_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtHDs3.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtHDs3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtHDs3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s3 = Val(txtHDs3.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtHDs6_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtHDs6.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtHDs6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtHDs6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s6 = Val(txtHDs6.Text)
	End Sub

    
End Class