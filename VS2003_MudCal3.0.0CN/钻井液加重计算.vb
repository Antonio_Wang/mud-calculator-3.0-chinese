Option Strict Off
Option Explicit On
Friend Class Form3
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdHelp As System.Windows.Forms.Button
	Public WithEvents Command5 As System.Windows.Forms.Button
	Public WithEvents Command4 As System.Windows.Forms.Button
	Public WithEvents Command3 As System.Windows.Forms.Button
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents Text17 As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents Text13 As System.Windows.Forms.TextBox
	Public WithEvents Text12 As System.Windows.Forms.TextBox
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents Command2 As System.Windows.Forms.Button
	Public WithEvents Command1 As System.Windows.Forms.Button
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form3))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ToolTip1.Active = True
		Me.cmdHelp = New System.Windows.Forms.Button
		Me.Command5 = New System.Windows.Forms.Button
		Me.Command4 = New System.Windows.Forms.Button
		Me.Command3 = New System.Windows.Forms.Button
		Me.Frame5 = New System.Windows.Forms.GroupBox
		Me.Text18 = New System.Windows.Forms.TextBox
		Me.Text17 = New System.Windows.Forms.TextBox
		Me.Text16 = New System.Windows.Forms.TextBox
		Me.Text15 = New System.Windows.Forms.TextBox
		Me.Label22 = New System.Windows.Forms.Label
		Me.Label21 = New System.Windows.Forms.Label
		Me.Label20 = New System.Windows.Forms.Label
		Me.Label19 = New System.Windows.Forms.Label
		Me.Label18 = New System.Windows.Forms.Label
		Me.Label17 = New System.Windows.Forms.Label
		Me.Label16 = New System.Windows.Forms.Label
		Me.Label15 = New System.Windows.Forms.Label
		Me.Frame4 = New System.Windows.Forms.GroupBox
		Me.Text11 = New System.Windows.Forms.TextBox
		Me.Text14 = New System.Windows.Forms.TextBox
		Me.Text13 = New System.Windows.Forms.TextBox
		Me.Text12 = New System.Windows.Forms.TextBox
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label13 = New System.Windows.Forms.Label
		Me.Label12 = New System.Windows.Forms.Label
		Me.Label11 = New System.Windows.Forms.Label
		Me.Frame3 = New System.Windows.Forms.GroupBox
		Me.Text10 = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.Label10 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Command2 = New System.Windows.Forms.Button
		Me.Command1 = New System.Windows.Forms.Button
		Me.Frame2 = New System.Windows.Forms.GroupBox
		Me.Text8 = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Text = "钻井液加重计算"
		Me.ClientSize = New System.Drawing.Size(649, 537)
		Me.Location = New System.Drawing.Point(26, 94)
		Me.Icon = CType(resources.GetObject("Form3.Icon"), System.Drawing.Icon)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.Name = "Form3"
		Me.cmdHelp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdHelp.Text = "帮 助"
		Me.cmdHelp.Size = New System.Drawing.Size(73, 25)
		Me.cmdHelp.Location = New System.Drawing.Point(488, 500)
		Me.cmdHelp.TabIndex = 50
		Me.cmdHelp.BackColor = System.Drawing.SystemColors.Control
		Me.cmdHelp.CausesValidation = True
		Me.cmdHelp.Enabled = True
		Me.cmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdHelp.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdHelp.TabStop = True
		Me.cmdHelp.Name = "cmdHelp"
		Me.Command5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command5.Text = "退 出"
		Me.Command5.Font = New System.Drawing.Font("宋体", 9!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
		Me.Command5.Size = New System.Drawing.Size(73, 25)
		Me.Command5.Location = New System.Drawing.Point(488, 453)
		Me.Command5.TabIndex = 19
		Me.Command5.BackColor = System.Drawing.SystemColors.Control
		Me.Command5.CausesValidation = True
		Me.Command5.Enabled = True
		Me.Command5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command5.TabStop = True
		Me.Command5.Name = "Command5"
		Me.Command4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command4.Text = "计算4"
		Me.Command4.Size = New System.Drawing.Size(73, 25)
		Me.Command4.Location = New System.Drawing.Point(488, 406)
		Me.Command4.TabIndex = 18
		Me.Command4.BackColor = System.Drawing.SystemColors.Control
		Me.Command4.CausesValidation = True
		Me.Command4.Enabled = True
		Me.Command4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command4.TabStop = True
		Me.Command4.Name = "Command4"
		Me.Command3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command3.Text = "计算3"
		Me.Command3.Size = New System.Drawing.Size(73, 25)
		Me.Command3.Location = New System.Drawing.Point(488, 358)
		Me.Command3.TabIndex = 17
		Me.Command3.BackColor = System.Drawing.SystemColors.Control
		Me.Command3.CausesValidation = True
		Me.Command3.Enabled = True
		Me.Command3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command3.TabStop = True
		Me.Command3.Name = "Command3"
		Me.Frame5.Text = "计算结果"
		Me.Frame5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
		Me.Frame5.ForeColor = System.Drawing.Color.Red
		Me.Frame5.Size = New System.Drawing.Size(241, 225)
		Me.Frame5.Location = New System.Drawing.Point(400, 16)
		Me.Frame5.TabIndex = 33
		Me.Frame5.BackColor = System.Drawing.SystemColors.Control
		Me.Frame5.Enabled = True
		Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame5.Visible = True
		Me.Frame5.Name = "Frame5"
		Me.Text18.AutoSize = False
		Me.Text18.Font = New System.Drawing.Font("Times New Roman", 9!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text18.Size = New System.Drawing.Size(41, 18)
		Me.Text18.Location = New System.Drawing.Point(120, 184)
		Me.Text18.TabIndex = 41
		Me.Text18.AcceptsReturn = True
		Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text18.BackColor = System.Drawing.SystemColors.Window
		Me.Text18.CausesValidation = True
		Me.Text18.Enabled = True
		Me.Text18.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text18.HideSelection = True
		Me.Text18.ReadOnly = False
		Me.Text18.Maxlength = 0
		Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text18.MultiLine = False
		Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text18.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text18.TabStop = True
		Me.Text18.Visible = True
		Me.Text18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text18.Name = "Text18"
		Me.Text17.AutoSize = False
		Me.Text17.Font = New System.Drawing.Font("Times New Roman", 9!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text17.Size = New System.Drawing.Size(41, 18)
		Me.Text17.Location = New System.Drawing.Point(120, 134)
		Me.Text17.TabIndex = 40
		Me.Text17.AcceptsReturn = True
		Me.Text17.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text17.BackColor = System.Drawing.SystemColors.Window
		Me.Text17.CausesValidation = True
		Me.Text17.Enabled = True
		Me.Text17.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text17.HideSelection = True
		Me.Text17.ReadOnly = False
		Me.Text17.Maxlength = 0
		Me.Text17.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text17.MultiLine = False
		Me.Text17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text17.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text17.TabStop = True
		Me.Text17.Visible = True
		Me.Text17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text17.Name = "Text17"
		Me.Text16.AutoSize = False
		Me.Text16.Font = New System.Drawing.Font("Times New Roman", 9!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text16.Size = New System.Drawing.Size(41, 18)
		Me.Text16.Location = New System.Drawing.Point(120, 83)
		Me.Text16.TabIndex = 39
		Me.Text16.AcceptsReturn = True
		Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text16.BackColor = System.Drawing.SystemColors.Window
		Me.Text16.CausesValidation = True
		Me.Text16.Enabled = True
		Me.Text16.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text16.HideSelection = True
		Me.Text16.ReadOnly = False
		Me.Text16.Maxlength = 0
		Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text16.MultiLine = False
		Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text16.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text16.TabStop = True
		Me.Text16.Visible = True
		Me.Text16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text16.Name = "Text16"
		Me.Text15.AutoSize = False
		Me.Text15.Font = New System.Drawing.Font("Times New Roman", 9!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text15.Size = New System.Drawing.Size(41, 18)
		Me.Text15.Location = New System.Drawing.Point(120, 32)
		Me.Text15.TabIndex = 38
		Me.Text15.AcceptsReturn = True
		Me.Text15.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text15.BackColor = System.Drawing.SystemColors.Window
		Me.Text15.CausesValidation = True
		Me.Text15.Enabled = True
		Me.Text15.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text15.HideSelection = True
		Me.Text15.ReadOnly = False
		Me.Text15.Maxlength = 0
		Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text15.MultiLine = False
		Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text15.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text15.TabStop = True
		Me.Text15.Visible = True
		Me.Text15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text15.Name = "Text15"
		Me.Label22.Text = "表4计算所需水量"
		Me.Label22.Size = New System.Drawing.Size(97, 17)
		Me.Label22.Location = New System.Drawing.Point(16, 184)
		Me.Label22.TabIndex = 49
		Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label22.BackColor = System.Drawing.SystemColors.Control
		Me.Label22.Enabled = True
		Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label22.UseMnemonic = True
		Me.Label22.Visible = True
		Me.Label22.AutoSize = False
		Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label22.Name = "Label22"
		Me.Label21.Text = "表3钻井液体积增量"
		Me.Label21.Size = New System.Drawing.Size(105, 17)
		Me.Label21.Location = New System.Drawing.Point(16, 134)
		Me.Label21.TabIndex = 48
		Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label21.BackColor = System.Drawing.SystemColors.Control
		Me.Label21.Enabled = True
		Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label21.UseMnemonic = True
		Me.Label21.Visible = True
		Me.Label21.AutoSize = False
		Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label21.Name = "Label21"
		Me.Label20.Text = "表2计算需加重剂"
		Me.Label20.Size = New System.Drawing.Size(97, 17)
		Me.Label20.Location = New System.Drawing.Point(16, 83)
		Me.Label20.TabIndex = 47
		Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label20.BackColor = System.Drawing.SystemColors.Control
		Me.Label20.Enabled = True
		Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label20.UseMnemonic = True
		Me.Label20.Visible = True
		Me.Label20.AutoSize = False
		Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label20.Name = "Label20"
		Me.Label19.Text = "表1计算需加重剂"
		Me.Label19.Size = New System.Drawing.Size(97, 17)
		Me.Label19.Location = New System.Drawing.Point(16, 32)
		Me.Label19.TabIndex = 46
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label19.BackColor = System.Drawing.SystemColors.Control
		Me.Label19.Enabled = True
		Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label19.UseMnemonic = True
		Me.Label19.Visible = True
		Me.Label19.AutoSize = False
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label19.Name = "Label19"
		Me.Label18.Text = "m^3"
		Me.Label18.Size = New System.Drawing.Size(41, 17)
		Me.Label18.Location = New System.Drawing.Point(192, 184)
		Me.Label18.TabIndex = 45
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label18.BackColor = System.Drawing.SystemColors.Control
		Me.Label18.Enabled = True
		Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.UseMnemonic = True
		Me.Label18.Visible = True
		Me.Label18.AutoSize = False
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Name = "Label18"
		Me.Label17.Text = "m^3"
		Me.Label17.Size = New System.Drawing.Size(41, 17)
		Me.Label17.Location = New System.Drawing.Point(192, 136)
		Me.Label17.TabIndex = 44
		Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label17.BackColor = System.Drawing.SystemColors.Control
		Me.Label17.Enabled = True
		Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label17.UseMnemonic = True
		Me.Label17.Visible = True
		Me.Label17.AutoSize = False
		Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label17.Name = "Label17"
		Me.Label16.Text = "×1000kg"
		Me.Label16.Size = New System.Drawing.Size(65, 17)
		Me.Label16.Location = New System.Drawing.Point(168, 88)
		Me.Label16.TabIndex = 43
		Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label16.BackColor = System.Drawing.SystemColors.Control
		Me.Label16.Enabled = True
		Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label16.UseMnemonic = True
		Me.Label16.Visible = True
		Me.Label16.AutoSize = False
		Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label16.Name = "Label16"
		Me.Label15.Text = "×1000kg"
		Me.Label15.Size = New System.Drawing.Size(65, 17)
		Me.Label15.Location = New System.Drawing.Point(168, 32)
		Me.Label15.TabIndex = 42
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label15.BackColor = System.Drawing.SystemColors.Control
		Me.Label15.Enabled = True
		Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.UseMnemonic = True
		Me.Label15.Visible = True
		Me.Label15.AutoSize = False
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Name = "Label15"
		Me.Frame4.Text = "降低钻井液密度所需加水量(计算4)"
		Me.Frame4.ForeColor = System.Drawing.Color.Blue
		Me.Frame4.Size = New System.Drawing.Size(369, 125)
		Me.Frame4.Location = New System.Drawing.Point(16, 406)
		Me.Frame4.TabIndex = 22
		Me.Frame4.BackColor = System.Drawing.SystemColors.Control
		Me.Frame4.Enabled = True
		Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame4.Visible = True
		Me.Frame4.Name = "Frame4"
		Me.Text11.AutoSize = False
		Me.Text11.Size = New System.Drawing.Size(81, 18)
		Me.Text11.Location = New System.Drawing.Point(256, 16)
		Me.Text11.TabIndex = 11
		Me.Text11.AcceptsReturn = True
		Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text11.BackColor = System.Drawing.SystemColors.Window
		Me.Text11.CausesValidation = True
		Me.Text11.Enabled = True
		Me.Text11.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text11.HideSelection = True
		Me.Text11.ReadOnly = False
		Me.Text11.Maxlength = 0
		Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text11.MultiLine = False
		Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text11.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text11.TabStop = True
		Me.Text11.Visible = True
		Me.Text11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text11.Name = "Text11"
		Me.Text14.AutoSize = False
		Me.Text14.Size = New System.Drawing.Size(81, 18)
		Me.Text14.Location = New System.Drawing.Point(256, 104)
		Me.Text14.TabIndex = 14
		Me.Text14.AcceptsReturn = True
		Me.Text14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text14.BackColor = System.Drawing.SystemColors.Window
		Me.Text14.CausesValidation = True
		Me.Text14.Enabled = True
		Me.Text14.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text14.HideSelection = True
		Me.Text14.ReadOnly = False
		Me.Text14.Maxlength = 0
		Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text14.MultiLine = False
		Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text14.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text14.TabStop = True
		Me.Text14.Visible = True
		Me.Text14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text14.Name = "Text14"
		Me.Text13.AutoSize = False
		Me.Text13.Size = New System.Drawing.Size(81, 18)
		Me.Text13.Location = New System.Drawing.Point(256, 75)
		Me.Text13.TabIndex = 13
		Me.Text13.AcceptsReturn = True
		Me.Text13.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text13.BackColor = System.Drawing.SystemColors.Window
		Me.Text13.CausesValidation = True
		Me.Text13.Enabled = True
		Me.Text13.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text13.HideSelection = True
		Me.Text13.ReadOnly = False
		Me.Text13.Maxlength = 0
		Me.Text13.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text13.MultiLine = False
		Me.Text13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text13.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text13.TabStop = True
		Me.Text13.Visible = True
		Me.Text13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text13.Name = "Text13"
		Me.Text12.AutoSize = False
		Me.Text12.Size = New System.Drawing.Size(81, 18)
		Me.Text12.Location = New System.Drawing.Point(256, 46)
		Me.Text12.TabIndex = 12
		Me.Text12.AcceptsReturn = True
		Me.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text12.BackColor = System.Drawing.SystemColors.Window
		Me.Text12.CausesValidation = True
		Me.Text12.Enabled = True
		Me.Text12.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text12.HideSelection = True
		Me.Text12.ReadOnly = False
		Me.Text12.Maxlength = 0
		Me.Text12.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text12.MultiLine = False
		Me.Text12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text12.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text12.TabStop = True
		Me.Text12.Visible = True
		Me.Text12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text12.Name = "Text12"
		Me.Label14.Text = "原有钻井液的体积(m^3)"
		Me.Label14.Size = New System.Drawing.Size(217, 17)
		Me.Label14.Location = New System.Drawing.Point(16, 102)
		Me.Label14.TabIndex = 37
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label14.BackColor = System.Drawing.SystemColors.Control
		Me.Label14.Enabled = True
		Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.UseMnemonic = True
		Me.Label14.Visible = True
		Me.Label14.AutoSize = False
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Name = "Label14"
		Me.Label13.Text = "加水后钻井液的密度(g/cm^3)"
		Me.Label13.Size = New System.Drawing.Size(217, 17)
		Me.Label13.Location = New System.Drawing.Point(16, 75)
		Me.Label13.TabIndex = 36
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label13.BackColor = System.Drawing.SystemColors.Control
		Me.Label13.Enabled = True
		Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.UseMnemonic = True
		Me.Label13.Visible = True
		Me.Label13.AutoSize = False
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Name = "Label13"
		Me.Label12.Text = "水的密度(g/cm^3)"
		Me.Label12.Size = New System.Drawing.Size(217, 17)
		Me.Label12.Location = New System.Drawing.Point(16, 49)
		Me.Label12.TabIndex = 35
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"
		Me.Label11.Text = "原有钻井液的密度(g/cm^3)"
		Me.Label11.Size = New System.Drawing.Size(217, 17)
		Me.Label11.Location = New System.Drawing.Point(16, 22)
		Me.Label11.TabIndex = 34
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label11.BackColor = System.Drawing.SystemColors.Control
		Me.Label11.Enabled = True
		Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.UseMnemonic = True
		Me.Label11.Visible = True
		Me.Label11.AutoSize = False
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Name = "Label11"
		Me.Frame3.Text = "用重晶石加重100m^3钻井液时体积增量(计算3)"
		Me.Frame3.ForeColor = System.Drawing.Color.Blue
		Me.Frame3.Size = New System.Drawing.Size(369, 81)
		Me.Frame3.Location = New System.Drawing.Point(16, 312)
		Me.Frame3.TabIndex = 21
		Me.Frame3.BackColor = System.Drawing.SystemColors.Control
		Me.Frame3.Enabled = True
		Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame3.Visible = True
		Me.Frame3.Name = "Frame3"
		Me.Text10.AutoSize = False
		Me.Text10.Size = New System.Drawing.Size(81, 18)
		Me.Text10.Location = New System.Drawing.Point(256, 48)
		Me.Text10.TabIndex = 10
		Me.Text10.AcceptsReturn = True
		Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text10.BackColor = System.Drawing.SystemColors.Window
		Me.Text10.CausesValidation = True
		Me.Text10.Enabled = True
		Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text10.HideSelection = True
		Me.Text10.ReadOnly = False
		Me.Text10.Maxlength = 0
		Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text10.MultiLine = False
		Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text10.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text10.TabStop = True
		Me.Text10.Visible = True
		Me.Text10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text10.Name = "Text10"
		Me.Text9.AutoSize = False
		Me.Text9.Size = New System.Drawing.Size(81, 18)
		Me.Text9.Location = New System.Drawing.Point(256, 24)
		Me.Text9.TabIndex = 9
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.BackColor = System.Drawing.SystemColors.Window
		Me.Text9.CausesValidation = True
		Me.Text9.Enabled = True
		Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text9.Name = "Text9"
		Me.Label10.Text = "加重后钻井液达到的密度(g/cm^3)"
		Me.Label10.Size = New System.Drawing.Size(217, 17)
		Me.Label10.Location = New System.Drawing.Point(16, 48)
		Me.Label10.TabIndex = 32
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.Label9.Text = "加重前钻井液的密度(g/cm^3)"
		Me.Label9.Size = New System.Drawing.Size(201, 17)
		Me.Label9.Location = New System.Drawing.Point(16, 24)
		Me.Label9.TabIndex = 31
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Command2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command2.Text = "计算2"
		Me.Command2.Size = New System.Drawing.Size(73, 25)
		Me.Command2.Location = New System.Drawing.Point(488, 311)
		Me.Command2.TabIndex = 16
		Me.Command2.BackColor = System.Drawing.SystemColors.Control
		Me.Command2.CausesValidation = True
		Me.Command2.Enabled = True
		Me.Command2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command2.TabStop = True
		Me.Command2.Name = "Command2"
		Me.Command1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Command1.Text = "计算1"
		Me.Command1.Size = New System.Drawing.Size(73, 25)
		Me.Command1.Location = New System.Drawing.Point(488, 264)
		Me.Command1.TabIndex = 15
		Me.Command1.BackColor = System.Drawing.SystemColors.Control
		Me.Command1.CausesValidation = True
		Me.Command1.Enabled = True
		Me.Command1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command1.TabStop = True
		Me.Command1.Name = "Command1"
		Me.Frame2.Text = "配制预定体积的加重钻井液所需加重剂的重量(计算2)"
		Me.Frame2.ForeColor = System.Drawing.Color.Blue
		Me.Frame2.Size = New System.Drawing.Size(369, 137)
		Me.Frame2.Location = New System.Drawing.Point(16, 164)
		Me.Frame2.TabIndex = 20
		Me.Frame2.BackColor = System.Drawing.SystemColors.Control
		Me.Frame2.Enabled = True
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Visible = True
		Me.Frame2.Name = "Frame2"
		Me.Text8.AutoSize = False
		Me.Text8.Size = New System.Drawing.Size(89, 18)
		Me.Text8.Location = New System.Drawing.Point(248, 104)
		Me.Text8.TabIndex = 8
		Me.Text8.AcceptsReturn = True
		Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text8.BackColor = System.Drawing.SystemColors.Window
		Me.Text8.CausesValidation = True
		Me.Text8.Enabled = True
		Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text8.HideSelection = True
		Me.Text8.ReadOnly = False
		Me.Text8.Maxlength = 0
		Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text8.MultiLine = False
		Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text8.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text8.TabStop = True
		Me.Text8.Visible = True
		Me.Text8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text8.Name = "Text8"
		Me.Text7.AutoSize = False
		Me.Text7.Size = New System.Drawing.Size(89, 18)
		Me.Text7.Location = New System.Drawing.Point(248, 77)
		Me.Text7.TabIndex = 7
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.BackColor = System.Drawing.SystemColors.Window
		Me.Text7.CausesValidation = True
		Me.Text7.Enabled = True
		Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text7.Name = "Text7"
		Me.Text6.AutoSize = False
		Me.Text6.Size = New System.Drawing.Size(89, 18)
		Me.Text6.Location = New System.Drawing.Point(248, 51)
		Me.Text6.TabIndex = 6
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.BackColor = System.Drawing.SystemColors.Window
		Me.Text6.CausesValidation = True
		Me.Text6.Enabled = True
		Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text6.Name = "Text6"
		Me.Text5.AutoSize = False
		Me.Text5.Size = New System.Drawing.Size(89, 18)
		Me.Text5.Location = New System.Drawing.Point(248, 24)
		Me.Text5.TabIndex = 5
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.BackColor = System.Drawing.SystemColors.Window
		Me.Text5.CausesValidation = True
		Me.Text5.Enabled = True
		Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text5.Name = "Text5"
		Me.Label8.Text = "加重剂的密度(g/cm^3)"
		Me.Label8.Size = New System.Drawing.Size(193, 17)
		Me.Label8.Location = New System.Drawing.Point(16, 112)
		Me.Label8.TabIndex = 30
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.Text = "原有钻井液的密度(g/cm^3)"
		Me.Label7.Size = New System.Drawing.Size(193, 17)
		Me.Label7.Location = New System.Drawing.Point(16, 83)
		Me.Label7.TabIndex = 29
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "钻井液欲加重的密度(g/cm^3)"
		Me.Label6.Size = New System.Drawing.Size(193, 17)
		Me.Label6.Location = New System.Drawing.Point(16, 54)
		Me.Label6.TabIndex = 28
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.Text = "加重后钻井液的体积(m^3)"
		Me.Label5.Size = New System.Drawing.Size(193, 17)
		Me.Label5.Location = New System.Drawing.Point(16, 24)
		Me.Label5.TabIndex = 27
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Frame1.Text = "对现有体积的钻井液加重所需加重剂的重量(计算1)"
		Me.Frame1.ForeColor = System.Drawing.Color.Blue
		Me.Frame1.Size = New System.Drawing.Size(369, 137)
		Me.Frame1.Location = New System.Drawing.Point(16, 16)
		Me.Frame1.TabIndex = 0
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Name = "Frame1"
		Me.Text4.AutoSize = False
		Me.Text4.Size = New System.Drawing.Size(89, 18)
		Me.Text4.Location = New System.Drawing.Point(248, 104)
		Me.Text4.TabIndex = 4
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.BackColor = System.Drawing.SystemColors.Window
		Me.Text4.CausesValidation = True
		Me.Text4.Enabled = True
		Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text4.Name = "Text4"
		Me.Text3.AutoSize = False
		Me.Text3.Size = New System.Drawing.Size(89, 18)
		Me.Text3.Location = New System.Drawing.Point(248, 76)
		Me.Text3.TabIndex = 3
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.BackColor = System.Drawing.SystemColors.Window
		Me.Text3.CausesValidation = True
		Me.Text3.Enabled = True
		Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text3.Name = "Text3"
		Me.Text2.AutoSize = False
		Me.Text2.Size = New System.Drawing.Size(89, 18)
		Me.Text2.Location = New System.Drawing.Point(248, 48)
		Me.Text2.TabIndex = 2
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.BackColor = System.Drawing.SystemColors.Window
		Me.Text2.CausesValidation = True
		Me.Text2.Enabled = True
		Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text2.Name = "Text2"
		Me.Text1.AutoSize = False
		Me.Text1.Size = New System.Drawing.Size(89, 18)
		Me.Text1.Location = New System.Drawing.Point(248, 24)
		Me.Text1.TabIndex = 1
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.BackColor = System.Drawing.SystemColors.Window
		Me.Text1.CausesValidation = True
		Me.Text1.Enabled = True
		Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text1.Name = "Text1"
		Me.Label4.Text = "加重剂的密度(g/cm^3)"
		Me.Label4.Size = New System.Drawing.Size(193, 17)
		Me.Label4.Location = New System.Drawing.Point(16, 104)
		Me.Label4.TabIndex = 26
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "钻井液欲加重的密度(g/cm^3)"
		Me.Label3.Size = New System.Drawing.Size(193, 17)
		Me.Label3.Location = New System.Drawing.Point(16, 80)
		Me.Label3.TabIndex = 25
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "原有钻井液的密度(g/cm^3)"
		Me.Label2.Size = New System.Drawing.Size(193, 17)
		Me.Label2.Location = New System.Drawing.Point(16, 56)
		Me.Label2.TabIndex = 24
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "原有钻井液的体积(m^3)"
		Me.Label1.Size = New System.Drawing.Size(185, 17)
		Me.Label1.Location = New System.Drawing.Point(16, 32)
		Me.Label1.TabIndex = 23
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(cmdHelp)
		Me.Controls.Add(Command5)
		Me.Controls.Add(Command4)
		Me.Controls.Add(Command3)
		Me.Controls.Add(Frame5)
		Me.Controls.Add(Frame4)
		Me.Controls.Add(Frame3)
		Me.Controls.Add(Command2)
		Me.Controls.Add(Command1)
		Me.Controls.Add(Frame2)
		Me.Controls.Add(Frame1)
		Me.Frame5.Controls.Add(Text18)
		Me.Frame5.Controls.Add(Text17)
		Me.Frame5.Controls.Add(Text16)
		Me.Frame5.Controls.Add(Text15)
		Me.Frame5.Controls.Add(Label22)
		Me.Frame5.Controls.Add(Label21)
		Me.Frame5.Controls.Add(Label20)
		Me.Frame5.Controls.Add(Label19)
		Me.Frame5.Controls.Add(Label18)
		Me.Frame5.Controls.Add(Label17)
		Me.Frame5.Controls.Add(Label16)
		Me.Frame5.Controls.Add(Label15)
		Me.Frame4.Controls.Add(Text11)
		Me.Frame4.Controls.Add(Text14)
		Me.Frame4.Controls.Add(Text13)
		Me.Frame4.Controls.Add(Text12)
		Me.Frame4.Controls.Add(Label14)
		Me.Frame4.Controls.Add(Label13)
		Me.Frame4.Controls.Add(Label12)
		Me.Frame4.Controls.Add(Label11)
		Me.Frame3.Controls.Add(Text10)
		Me.Frame3.Controls.Add(Text9)
		Me.Frame3.Controls.Add(Label10)
		Me.Frame3.Controls.Add(Label9)
		Me.Frame2.Controls.Add(Text8)
		Me.Frame2.Controls.Add(Text7)
		Me.Frame2.Controls.Add(Text6)
		Me.Frame2.Controls.Add(Text5)
		Me.Frame2.Controls.Add(Label8)
		Me.Frame2.Controls.Add(Label7)
		Me.Frame2.Controls.Add(Label6)
		Me.Frame2.Controls.Add(Label5)
		Me.Frame1.Controls.Add(Text4)
		Me.Frame1.Controls.Add(Text3)
		Me.Frame1.Controls.Add(Text2)
		Me.Frame1.Controls.Add(Text1)
		Me.Frame1.Controls.Add(Label4)
		Me.Frame1.Controls.Add(Label3)
		Me.Frame1.Controls.Add(Label2)
		Me.Frame1.Controls.Add(Label1)
	End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As Form3
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form3
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form3()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim Vyuantiji1 As Single '原有钻井液的体积－表1
	Dim Myuanmidu1 As Single '原钻井液密度－表1
	Dim Myujiamidu1 As Single '加重后钻井液密度－表1
	Dim Mjimidu1 As Single '加重剂密度－表1
	Dim Vhoutiji2 As Single '加重后钻井液体积－表2
	Dim Myuanmidu2 As Single '原钻井液密度－表2
	Dim Myujiamidu2 As Single '加重后钻井液密度－表2
	Dim Mjimidu2 As Single '加重剂密度－表2
	Dim Jiazhongji1 As Single '加重剂用量1
	Dim Jiazhongji2 As Single '加重剂用量1
	Dim M前密度3 As Single
	Dim M后密度3 As Single
	Dim Vadd3 As Single
	Dim M原密度4 As Single
	Dim Mwater4 As Single
	Dim M稀释后密度4 As Single
	Dim V原体积4 As Single
	Dim Vwater4 As Single
	
	
	
	Private Sub cmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdHelp.Click
		frmHelpMudWeight.DefInstance.ShowDialog()
	End Sub
	
	Private Sub Command1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command1.Click '对现有体积的钻井液加重所需加重剂的重量公式
		
		
		
		
		
		Jiazhongji1 = Vyuantiji1 * Mjimidu1 * (Myujiamidu1 - Myuanmidu1) / (Mjimidu1 - Myujiamidu1)
		Text15.Text = VB6.Format(Jiazhongji1, "0.00")
	End Sub
	
	Private Sub Command2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command2.Click '配制预定体积的加重钻井液所需加重剂的重量
		Jiazhongji2 = Vhoutiji2 * Mjimidu2 * (Myujiamidu2 - Myuanmidu2) / (Mjimidu2 - Myujiamidu2)
		Text16.Text = VB6.Format(Jiazhongji2, "0.00")
	End Sub
	
	Private Sub Command3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command3.Click '用重晶石加重100方钻井液时体积增量
		Vadd3 = (100 * (M后密度3 - M前密度3)) / (4.2 - M后密度3)
		Text17.Text = VB6.Format(Vadd3, "0.00")
	End Sub
	
	Private Sub Command4_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command4.Click '降低钻井液密度所需加水量
		Vwater4 = (V原体积4 * (M原密度4 - M稀释后密度4)) / (M稀释后密度4 - Mwater4)
		Text18.Text = VB6.Format(Vwater4, "0.00")
	End Sub
	
	Private Sub Command5_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command5.Click
		Me.Close()
	End Sub
	
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vyuantiji1 = Val(Text1.Text) '原有钻井液的体积－表1
	End Sub
	
	Private Sub Text10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M后密度3 = Val(Text10.Text) '重晶石加重后钻井液密度输入－－3
	End Sub
	
	Private Sub Text11_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text11.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M原密度4 = Val(Text11.Text) '表4加水稀释前钻井液密度输入
	End Sub
	
	Private Sub Text12_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text12.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mwater4 = Val(Text12.Text) '表4所用水密度输入
	End Sub
	
	Private Sub Text13_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text13.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M稀释后密度4 = Val(Text13.Text) '表4加水稀释后钻井液密度输入
	End Sub
	
	Private Sub Text14_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text14.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		V原体积4 = Val(Text14.Text) '表4加水稀释前钻井液体积
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myuanmidu1 = Val(Text2.Text) '原钻井液密度－表1
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myujiamidu1 = Val(Text3.Text) '加重后钻井液密度－表1
	End Sub
	
	Private Sub Text4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mjimidu1 = Val(Text4.Text) '加重剂密度－表1
	End Sub
	
	Private Sub Text5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vhoutiji2 = Val(Text5.Text) '加重后钻井液体积－表2
	End Sub
	
	Private Sub Text6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myujiamidu2 = Val(Text6.Text) '加重后钻井液密度－表2
	End Sub
	
	Private Sub Text7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Myuanmidu2 = Val(Text7.Text) '原钻井液密度－表2
	End Sub
	
	Private Sub Text8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Mjimidu2 = Val(Text8.Text) '加重剂密度－表2
	End Sub
	
	Private Sub Text9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		M前密度3 = Val(Text9.Text) '重晶石加重前钻井液密度输入－－3
	End Sub
End Class