Option Strict Off
Option Explicit On
Friend Class frmunitLenght
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents txtLenghtfur As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtnmile As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtmile As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtyd As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtft As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtin As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtum As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtmm As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtcm As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtdm As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtm As System.Windows.Forms.TextBox
	Public WithEvents txtLenghtkm As System.Windows.Forms.TextBox
	Public WithEvents lbllenght As System.Windows.Forms.Label
	Public WithEvents lblLenghtfur As System.Windows.Forms.Label
	Public WithEvents lblLenghtnmile As System.Windows.Forms.Label
	Public WithEvents lblLenghtMile As System.Windows.Forms.Label
	Public WithEvents lblLenghtyd As System.Windows.Forms.Label
	Public WithEvents lblLenghtft As System.Windows.Forms.Label
	Public WithEvents lblLenghtin As System.Windows.Forms.Label
	Public WithEvents lblLenghtUM As System.Windows.Forms.Label
	Public WithEvents lblLenghtMM As System.Windows.Forms.Label
	Public WithEvents lblLenghtCM As System.Windows.Forms.Label
	Public WithEvents lblLenghtDM As System.Windows.Forms.Label
	Public WithEvents lblLenghtM As System.Windows.Forms.Label
	Public WithEvents lblLenghtkm As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtLenghtfur = New System.Windows.Forms.TextBox
        Me.txtLenghtnmile = New System.Windows.Forms.TextBox
        Me.txtLenghtmile = New System.Windows.Forms.TextBox
        Me.txtLenghtyd = New System.Windows.Forms.TextBox
        Me.txtLenghtft = New System.Windows.Forms.TextBox
        Me.txtLenghtin = New System.Windows.Forms.TextBox
        Me.txtLenghtum = New System.Windows.Forms.TextBox
        Me.txtLenghtmm = New System.Windows.Forms.TextBox
        Me.txtLenghtcm = New System.Windows.Forms.TextBox
        Me.txtLenghtdm = New System.Windows.Forms.TextBox
        Me.txtLenghtm = New System.Windows.Forms.TextBox
        Me.txtLenghtkm = New System.Windows.Forms.TextBox
        Me.lbllenght = New System.Windows.Forms.Label
        Me.lblLenghtfur = New System.Windows.Forms.Label
        Me.lblLenghtnmile = New System.Windows.Forms.Label
        Me.lblLenghtMile = New System.Windows.Forms.Label
        Me.lblLenghtyd = New System.Windows.Forms.Label
        Me.lblLenghtft = New System.Windows.Forms.Label
        Me.lblLenghtin = New System.Windows.Forms.Label
        Me.lblLenghtUM = New System.Windows.Forms.Label
        Me.lblLenghtMM = New System.Windows.Forms.Label
        Me.lblLenghtCM = New System.Windows.Forms.Label
        Me.lblLenghtDM = New System.Windows.Forms.Label
        Me.lblLenghtM = New System.Windows.Forms.Label
        Me.lblLenghtkm = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(278, 258)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 26
        Me.cmdclear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(86, 258)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 25
        Me.cmdquit.Text = "退出"
        '
        'txtLenghtfur
        '
        Me.txtLenghtfur.AcceptsReturn = True
        Me.txtLenghtfur.AutoSize = False
        Me.txtLenghtfur.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtfur.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtfur.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtfur.Location = New System.Drawing.Point(259, 215)
        Me.txtLenghtfur.MaxLength = 0
        Me.txtLenghtfur.Name = "txtLenghtfur"
        Me.txtLenghtfur.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtfur.Size = New System.Drawing.Size(97, 20)
        Me.txtLenghtfur.TabIndex = 11
        Me.txtLenghtfur.Text = ""
        '
        'txtLenghtnmile
        '
        Me.txtLenghtnmile.AcceptsReturn = True
        Me.txtLenghtnmile.AutoSize = False
        Me.txtLenghtnmile.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtnmile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtnmile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtnmile.Location = New System.Drawing.Point(259, 181)
        Me.txtLenghtnmile.MaxLength = 0
        Me.txtLenghtnmile.Name = "txtLenghtnmile"
        Me.txtLenghtnmile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtnmile.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtnmile.TabIndex = 10
        Me.txtLenghtnmile.Text = ""
        '
        'txtLenghtmile
        '
        Me.txtLenghtmile.AcceptsReturn = True
        Me.txtLenghtmile.AutoSize = False
        Me.txtLenghtmile.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtmile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtmile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtmile.Location = New System.Drawing.Point(259, 146)
        Me.txtLenghtmile.MaxLength = 0
        Me.txtLenghtmile.Name = "txtLenghtmile"
        Me.txtLenghtmile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtmile.Size = New System.Drawing.Size(97, 20)
        Me.txtLenghtmile.TabIndex = 9
        Me.txtLenghtmile.Text = ""
        '
        'txtLenghtyd
        '
        Me.txtLenghtyd.AcceptsReturn = True
        Me.txtLenghtyd.AutoSize = False
        Me.txtLenghtyd.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtyd.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtyd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtyd.Location = New System.Drawing.Point(259, 112)
        Me.txtLenghtyd.MaxLength = 0
        Me.txtLenghtyd.Name = "txtLenghtyd"
        Me.txtLenghtyd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtyd.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtyd.TabIndex = 8
        Me.txtLenghtyd.Text = ""
        '
        'txtLenghtft
        '
        Me.txtLenghtft.AcceptsReturn = True
        Me.txtLenghtft.AutoSize = False
        Me.txtLenghtft.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtft.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtft.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtft.Location = New System.Drawing.Point(259, 78)
        Me.txtLenghtft.MaxLength = 0
        Me.txtLenghtft.Name = "txtLenghtft"
        Me.txtLenghtft.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtft.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtft.TabIndex = 7
        Me.txtLenghtft.Text = ""
        '
        'txtLenghtin
        '
        Me.txtLenghtin.AcceptsReturn = True
        Me.txtLenghtin.AutoSize = False
        Me.txtLenghtin.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtin.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtin.Location = New System.Drawing.Point(259, 43)
        Me.txtLenghtin.MaxLength = 0
        Me.txtLenghtin.Name = "txtLenghtin"
        Me.txtLenghtin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtin.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtin.TabIndex = 6
        Me.txtLenghtin.Text = ""
        '
        'txtLenghtum
        '
        Me.txtLenghtum.AcceptsReturn = True
        Me.txtLenghtum.AutoSize = False
        Me.txtLenghtum.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtum.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtum.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtum.Location = New System.Drawing.Point(29, 215)
        Me.txtLenghtum.MaxLength = 0
        Me.txtLenghtum.Name = "txtLenghtum"
        Me.txtLenghtum.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtum.Size = New System.Drawing.Size(97, 20)
        Me.txtLenghtum.TabIndex = 5
        Me.txtLenghtum.Text = ""
        '
        'txtLenghtmm
        '
        Me.txtLenghtmm.AcceptsReturn = True
        Me.txtLenghtmm.AutoSize = False
        Me.txtLenghtmm.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtmm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtmm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtmm.Location = New System.Drawing.Point(29, 181)
        Me.txtLenghtmm.MaxLength = 0
        Me.txtLenghtmm.Name = "txtLenghtmm"
        Me.txtLenghtmm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtmm.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtmm.TabIndex = 2
        Me.txtLenghtmm.Text = ""
        '
        'txtLenghtcm
        '
        Me.txtLenghtcm.AcceptsReturn = True
        Me.txtLenghtcm.AutoSize = False
        Me.txtLenghtcm.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtcm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtcm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtcm.Location = New System.Drawing.Point(29, 146)
        Me.txtLenghtcm.MaxLength = 0
        Me.txtLenghtcm.Name = "txtLenghtcm"
        Me.txtLenghtcm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtcm.Size = New System.Drawing.Size(97, 20)
        Me.txtLenghtcm.TabIndex = 1
        Me.txtLenghtcm.Text = ""
        '
        'txtLenghtdm
        '
        Me.txtLenghtdm.AcceptsReturn = True
        Me.txtLenghtdm.AutoSize = False
        Me.txtLenghtdm.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtdm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtdm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtdm.Location = New System.Drawing.Point(29, 112)
        Me.txtLenghtdm.MaxLength = 0
        Me.txtLenghtdm.Name = "txtLenghtdm"
        Me.txtLenghtdm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtdm.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtdm.TabIndex = 4
        Me.txtLenghtdm.Text = ""
        '
        'txtLenghtm
        '
        Me.txtLenghtm.AcceptsReturn = True
        Me.txtLenghtm.AutoSize = False
        Me.txtLenghtm.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtm.Location = New System.Drawing.Point(29, 78)
        Me.txtLenghtm.MaxLength = 0
        Me.txtLenghtm.Name = "txtLenghtm"
        Me.txtLenghtm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtm.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtm.TabIndex = 0
        Me.txtLenghtm.Text = ""
        '
        'txtLenghtkm
        '
        Me.txtLenghtkm.AcceptsReturn = True
        Me.txtLenghtkm.AutoSize = False
        Me.txtLenghtkm.BackColor = System.Drawing.SystemColors.Window
        Me.txtLenghtkm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLenghtkm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLenghtkm.Location = New System.Drawing.Point(29, 43)
        Me.txtLenghtkm.MaxLength = 0
        Me.txtLenghtkm.Name = "txtLenghtkm"
        Me.txtLenghtkm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLenghtkm.Size = New System.Drawing.Size(97, 19)
        Me.txtLenghtkm.TabIndex = 3
        Me.txtLenghtkm.Text = ""
        '
        'lbllenght
        '
        Me.lbllenght.BackColor = System.Drawing.SystemColors.Control
        Me.lbllenght.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbllenght.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbllenght.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbllenght.Location = New System.Drawing.Point(19, 9)
        Me.lbllenght.Name = "lbllenght"
        Me.lbllenght.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbllenght.Size = New System.Drawing.Size(261, 18)
        Me.lbllenght.TabIndex = 24
        Me.lbllenght.Text = "输入长度单位换算数据："
        '
        'lblLenghtfur
        '
        Me.lblLenghtfur.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtfur.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtfur.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtfur.Location = New System.Drawing.Point(374, 215)
        Me.lblLenghtfur.Name = "lblLenghtfur"
        Me.lblLenghtfur.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtfur.Size = New System.Drawing.Size(78, 19)
        Me.lblLenghtfur.TabIndex = 23
        Me.lblLenghtfur.Text = "fur(浪)"
        '
        'lblLenghtnmile
        '
        Me.lblLenghtnmile.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtnmile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtnmile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtnmile.Location = New System.Drawing.Point(374, 181)
        Me.lblLenghtnmile.Name = "lblLenghtnmile"
        Me.lblLenghtnmile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtnmile.Size = New System.Drawing.Size(117, 18)
        Me.lblLenghtnmile.TabIndex = 22
        Me.lblLenghtnmile.Text = "nmile(国际海哩)"
        '
        'lblLenghtMile
        '
        Me.lblLenghtMile.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtMile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtMile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtMile.Location = New System.Drawing.Point(374, 146)
        Me.lblLenghtMile.Name = "lblLenghtMile"
        Me.lblLenghtMile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtMile.Size = New System.Drawing.Size(78, 19)
        Me.lblLenghtMile.TabIndex = 21
        Me.lblLenghtMile.Text = "mile(英哩)"
        '
        'lblLenghtyd
        '
        Me.lblLenghtyd.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtyd.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtyd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtyd.Location = New System.Drawing.Point(374, 112)
        Me.lblLenghtyd.Name = "lblLenghtyd"
        Me.lblLenghtyd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtyd.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtyd.TabIndex = 20
        Me.lblLenghtyd.Text = "yd(码)"
        '
        'lblLenghtft
        '
        Me.lblLenghtft.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtft.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtft.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtft.Location = New System.Drawing.Point(374, 78)
        Me.lblLenghtft.Name = "lblLenghtft"
        Me.lblLenghtft.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtft.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtft.TabIndex = 19
        Me.lblLenghtft.Text = "ft(英尺)"
        '
        'lblLenghtin
        '
        Me.lblLenghtin.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtin.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtin.Location = New System.Drawing.Point(374, 43)
        Me.lblLenghtin.Name = "lblLenghtin"
        Me.lblLenghtin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtin.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtin.TabIndex = 18
        Me.lblLenghtin.Text = "in(英寸)"
        '
        'lblLenghtUM
        '
        Me.lblLenghtUM.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtUM.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtUM.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtUM.Location = New System.Drawing.Point(154, 215)
        Me.lblLenghtUM.Name = "lblLenghtUM"
        Me.lblLenghtUM.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtUM.Size = New System.Drawing.Size(78, 19)
        Me.lblLenghtUM.TabIndex = 17
        Me.lblLenghtUM.Text = "um(微米)"
        '
        'lblLenghtMM
        '
        Me.lblLenghtMM.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtMM.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtMM.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtMM.Location = New System.Drawing.Point(154, 181)
        Me.lblLenghtMM.Name = "lblLenghtMM"
        Me.lblLenghtMM.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtMM.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtMM.TabIndex = 16
        Me.lblLenghtMM.Text = "mm(毫米)"
        '
        'lblLenghtCM
        '
        Me.lblLenghtCM.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtCM.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtCM.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtCM.Location = New System.Drawing.Point(154, 146)
        Me.lblLenghtCM.Name = "lblLenghtCM"
        Me.lblLenghtCM.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtCM.Size = New System.Drawing.Size(78, 19)
        Me.lblLenghtCM.TabIndex = 15
        Me.lblLenghtCM.Text = "cm(厘米)"
        '
        'lblLenghtDM
        '
        Me.lblLenghtDM.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtDM.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtDM.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtDM.Location = New System.Drawing.Point(154, 112)
        Me.lblLenghtDM.Name = "lblLenghtDM"
        Me.lblLenghtDM.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtDM.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtDM.TabIndex = 14
        Me.lblLenghtDM.Text = "dm(分米)"
        '
        'lblLenghtM
        '
        Me.lblLenghtM.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtM.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtM.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtM.Location = New System.Drawing.Point(154, 78)
        Me.lblLenghtM.Name = "lblLenghtM"
        Me.lblLenghtM.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtM.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtM.TabIndex = 13
        Me.lblLenghtM.Text = "m(米)"
        '
        'lblLenghtkm
        '
        Me.lblLenghtkm.BackColor = System.Drawing.SystemColors.Control
        Me.lblLenghtkm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLenghtkm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLenghtkm.Location = New System.Drawing.Point(154, 43)
        Me.lblLenghtkm.Name = "lblLenghtkm"
        Me.lblLenghtkm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLenghtkm.Size = New System.Drawing.Size(78, 18)
        Me.lblLenghtkm.TabIndex = 12
        Me.lblLenghtkm.Text = "Km(千米)"
        '
        'frmunitLenght
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(487, 309)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.txtLenghtfur)
        Me.Controls.Add(Me.txtLenghtnmile)
        Me.Controls.Add(Me.txtLenghtmile)
        Me.Controls.Add(Me.txtLenghtyd)
        Me.Controls.Add(Me.txtLenghtft)
        Me.Controls.Add(Me.txtLenghtin)
        Me.Controls.Add(Me.txtLenghtum)
        Me.Controls.Add(Me.txtLenghtmm)
        Me.Controls.Add(Me.txtLenghtcm)
        Me.Controls.Add(Me.txtLenghtdm)
        Me.Controls.Add(Me.txtLenghtm)
        Me.Controls.Add(Me.txtLenghtkm)
        Me.Controls.Add(Me.lbllenght)
        Me.Controls.Add(Me.lblLenghtfur)
        Me.Controls.Add(Me.lblLenghtnmile)
        Me.Controls.Add(Me.lblLenghtMile)
        Me.Controls.Add(Me.lblLenghtyd)
        Me.Controls.Add(Me.lblLenghtft)
        Me.Controls.Add(Me.lblLenghtin)
        Me.Controls.Add(Me.lblLenghtUM)
        Me.Controls.Add(Me.lblLenghtMM)
        Me.Controls.Add(Me.lblLenghtCM)
        Me.Controls.Add(Me.lblLenghtDM)
        Me.Controls.Add(Me.lblLenghtM)
        Me.Controls.Add(Me.lblLenghtkm)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitLenght"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "长度(Lenght)单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitLenght
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitLenght
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitLenght()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月8日于青海省海西洲马海马北油田
	'长度单位换算模块
	
	Dim km As Single
	Dim m As Single
	Dim dm As Single
	Dim cm As Single
	Dim mm As Single
	Dim um As Single
	Dim Inch As Single
	Dim ft As Single
	Dim yd As Single
	Dim mile As Single
	Dim nmile As Single
	Dim fur As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitLenght.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtLenghtcm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtcm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '厘米－－输入数据的计算过程
		cm = Val(txtLenghtcm.Text)
		km = cm / 100000
		dm = cm / 10
		m = cm / 100
		mm = cm * 10
		um = cm * 10000
		Inch = cm / 2.54 '1 in=25.4mm
		ft = cm / 30.48 '1 ft=30.5cm   1m=3.28ft
		yd = cm / 100 / 0.9144 '1 yd=0.914m   1m=1.09yd
		mile = cm / 100 / 1609.344 '1 mile=5280ft=1609.344m
		nmile = cm / 100 / 1852 '1 nmile=1852m
		fur = cm / 100 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtdm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtdm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '分米－－输入数据的计算过程
		dm = Val(txtLenghtdm.Text)
		km = dm / 10000
		m = dm / 10
		cm = dm * 10
		mm = dm * 100
		um = dm * 100000
		Inch = dm * 10 / 2.54
		ft = dm * 10 / 30.48
		yd = dm / 10 / 0.9144
		mile = dm / 10 / 1609.344
		nmile = dm / 10 / 1852
		fur = dm / 10 / 201
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtm.Text = CStr(m)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtft_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtft.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英尺－－输入数据的计算过程
		ft = Val(txtLenghtft.Text)
		km = ft / 3.28 / 1000
		dm = ft / 3.28 * 10
		m = ft / 3.28
		cm = ft * 30.48
		mm = ft * 30.48 * 10
		um = ft * 30.48 * 10000 '1 ft=30.5cm   1m=3.28ft
		Inch = ft * 12 '1 in=25.4mm
		
		yd = ft / 3 '1 yd=0.914m   1m=1.09yd
		mile = ft / 5280 '1 mile=5280ft=1609.344m
		nmile = ft * 0.3048 / 1852 '1 nmile=1852m
		fur = ft * 0.3048 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtfur_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtfur.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制浪－－输入数据的计算过程
		fur = Val(txtLenghtfur.Text)
		km = fur * 0.201
		dm = fur * 201 * 10
		m = fur * 201
		cm = fur * 201 * 100
		mm = fur * 201 * 1000
		um = fur * 201 * 1000000
		Inch = fur * 201 * 1000 / 25.4 '1 in=25.4mm
		ft = fur * 201 / 3.28 '1 ft=30.5cm   1m=3.28ft
		yd = fur * 201 / 0.914 '1 yd=0.914m   1m=1.09yd
		mile = fur * 201 / 1609.344 '1 mile=5280ft=1609.344m
		nmile = m / 1852 '1 nmile=1852m
		fur = m / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtm.Text = CStr(m)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
	End Sub
	
	Private Sub txtLenghtin_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtin.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英寸－－输入数据的计算过程
		Inch = Val(txtLenghtin.Text)
		km = Inch * 2.54 / 100000
		dm = Inch * 2.54 / 10
		m = Inch * 2.54 / 100
		cm = Inch * 2.54
		mm = Inch * 25.4
		um = Inch * 25.4 * 1000 '1 ft=30.5cm   1m=3.28ft
		ft = Inch / 12 '1 in=25.4mm
		yd = Inch / 36 '1 yd=0.914m   1m=1.09yd
		mile = Inch / 12 / 5280 '1 mile=5280ft=1609.344m
		nmile = Inch * 0.0254 / 1852 '1 nmile=1852m
		fur = Inch * 0.0254 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtkm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtkm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '千米－－输入数据的计算过程
		km = Val(txtLenghtkm.Text)
		m = km * 1000
		dm = km * 10000
		cm = km * 100000
		mm = km * 1000000
		um = km * 1000000000
		Inch = km * 100000 / 2.54
		ft = km * 1000 / 3.28
		yd = km * 1000 * 0.9144
		mile = km * 1000 / 1609.344
		nmile = km * 1000 / 1852
		fur = km * 1000 / 201
		
		txtLenghtm.Text = CStr(m)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	
	Private Sub txtLenghtm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '米－－输入数据的计算过程
		m = Val(txtLenghtm.Text)
		km = m / 1000
		dm = m * 10
		cm = m * 100
		mm = m * 1000
		um = m * 1000000
		Inch = m * 100 / 2.54 '1 in=25.4mm
		ft = m * 3.28 '1 ft=30.5cm   1m=3.28ft
		yd = m / 0.9144 '1 yd=0.914m   1m=1.09yd
		mile = m / 1609.344 '1 mile=5280ft=1609.344m
		nmile = m / 1852 '1 nmile=1852m
		fur = m / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtmile_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtmile.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英里－－输入数据的计算过程
		mile = Val(txtLenghtmile.Text)
		km = mile * 1609.344 / 1000
		dm = mile * 1609.344 * 10
		m = mile * 1609.344
		cm = mile * 1609.344 * 100
		mm = mile * 1609.344 * 1000
		um = mile * 1609.344 * 1000000 '1 ft=30.5cm   1m=3.28ft
		Inch = mile * 5280 * 12 '1 in=25.4mm
		ft = mile * 5280 '1 mile=5280ft=1609.344m
		yd = mile * 1760 '1 yd=0.914m   1m=1.09yd
		nmile = mile * 0.869 '1 nmile=1852m
		fur = mile * 1609.344 / 210 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtmm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtmm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '毫米－－输入数据的计算过程
		mm = Val(txtLenghtmm.Text)
		km = mm / 1000000
		dm = mm / 100
		m = mm / 1000
		cm = mm / 10
		um = mm * 1000
		Inch = mm / 25.4 '1 in=25.4mm
		ft = mm / 25.4 / 12 '1 ft=30.5cm   1m=3.28ft
		yd = mm / 914 '1 yd=0.914m   1m=1.09yd
		mile = mm / 1000 / 1609.344 '1 mile=5280ft=1609.344m
		nmile = mm / 1000 / 1852 '1 nmile=1852m
		fur = mm / 1000 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtnmile_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtnmile.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '国际海里－－输入数据的计算过程
		nmile = Val(txtLenghtnmile.Text)
		km = nmile * 1852 / 1000
		m = nmile * 1852
		dm = nmile * 1852 * 10
		cm = nmile * 1852 * 100
		mm = nmile * 1852 * 1000
		um = nmile * 1852 * 1000000
		Inch = nmile * 1852 * 1000 / 2.54 '1 in=25.4mm
		ft = nmile * 1852 / 3.28 '1 ft=30.5cm   1m=3.28ft
		yd = nmile * 1852 * 0.914 '1 yd=0.914m   1m=1.09yd
		mile = nmile * 1852 / 1609.344 '1 mile=5280ft=1609.344m
		'1 nmile=1852m
		fur = nmile * 1852 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtm.Text = CStr(m)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtum_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtum.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '微米－－输入数据的计算过程
		um = Val(txtLenghtum.Text)
		km = um / 1000000000
		dm = um / 100000
		m = um / 1000000
		cm = um / 10000
		mm = um / 1000
		Inch = um / 1000 / 25.4 '1 in=25.4mm
		ft = um / 1000 / 25.4 / 12 '1 ft=30.5cm   1m=3.28ft
		yd = um / 1000 / 914 '1 yd=0.914m   1m=1.09yd
		mile = um / 1000000 / 1609.344 '1 mile=5280ft=1609.344m
		nmile = um / 1000000 / 1852 '1 nmile=1852m
		fur = um / 1000000 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtm.Text = CStr(m)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtyd.Text = CStr(yd)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
	
	Private Sub txtLenghtyd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLenghtyd.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000 '英制码－－输入数据的计算过程
		yd = Val(txtLenghtyd.Text)
		km = yd * 0.914 / 1000
		m = yd * 0.914
		dm = yd * 0.914 * 10
		cm = yd * 0.914 * 100
		mm = yd * 0.914 * 1000
		um = yd * 0.914 * 1000000
		Inch = yd * 0.914 * 1000 / 2.54 '1 in=25.4mm
		ft = yd * 0.914 * 3.28 '1 ft=30.5cm   1m=3.28ft
		'1 yd=0.914m   1m=1.09yd
		mile = yd * 0.914 / 1609.344 '1 mile=5280ft=1609.344m
		nmile = yd * 0.914 / 1852 '1 nmile=1852m
		fur = yd * 0.914 / 201 '1 fur=210m   1km=4.97fur
		
		txtLenghtkm.Text = CStr(km)
		txtLenghtm.Text = CStr(m)
		txtLenghtdm.Text = CStr(dm)
		txtLenghtcm.Text = CStr(cm)
		txtLenghtmm.Text = CStr(mm)
		txtLenghtum.Text = CStr(um)
		txtLenghtin.Text = CStr(Inch)
		txtLenghtft.Text = CStr(ft)
		txtLenghtmile.Text = CStr(mile)
		txtLenghtnmile.Text = CStr(nmile)
		txtLenghtfur.Text = CStr(fur)
	End Sub
End Class