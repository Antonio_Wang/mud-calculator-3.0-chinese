Option Strict Off
Option Explicit On
Friend Class frmunit
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents picFrmunitBg As System.Windows.Forms.PictureBox
	Public WithEvents munfrmunitLenght As System.Windows.Forms.MenuItem
	Public WithEvents munfrmunitArea As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitVolume As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitVolumeFluids As System.Windows.Forms.MenuItem
	Public WithEvents FENGGE1 As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitWeight As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitTemperature As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitPressure As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitDensity As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitVelocity As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitForce As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitE As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitPower As System.Windows.Forms.MenuItem
	Public WithEvents FENGGE2 As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmuintMR As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitVR As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitPN As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitSN As System.Windows.Forms.MenuItem
	Public WithEvents FENGGE3 As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitKX As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitBZ As System.Windows.Forms.MenuItem
	Public WithEvents FENGGE4 As System.Windows.Forms.MenuItem
	Public WithEvents munfrmunitYP As System.Windows.Forms.MenuItem
	Public WithEvents munfrmunitBiteye As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitChange As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitMoudle As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunit As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitPic As System.Windows.Forms.MenuItem
	Public WithEvents mumfrmunitQuit As System.Windows.Forms.MenuItem
	Public MainMenu1 As System.Windows.Forms.MainMenu
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmunit))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picFrmunitBg = New System.Windows.Forms.PictureBox
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mumfrmunitChange = New System.Windows.Forms.MenuItem
        Me.munfrmunitLenght = New System.Windows.Forms.MenuItem
        Me.munfrmunitArea = New System.Windows.Forms.MenuItem
        Me.mumfrmunitVolume = New System.Windows.Forms.MenuItem
        Me.mumfrmunitVolumeFluids = New System.Windows.Forms.MenuItem
        Me.FENGGE1 = New System.Windows.Forms.MenuItem
        Me.mumfrmunitWeight = New System.Windows.Forms.MenuItem
        Me.mumfrmunitTemperature = New System.Windows.Forms.MenuItem
        Me.mumfrmunitPressure = New System.Windows.Forms.MenuItem
        Me.mumfrmunitDensity = New System.Windows.Forms.MenuItem
        Me.mumfrmunitVelocity = New System.Windows.Forms.MenuItem
        Me.mumfrmunitForce = New System.Windows.Forms.MenuItem
        Me.mumfrmunitE = New System.Windows.Forms.MenuItem
        Me.mumfrmunitPower = New System.Windows.Forms.MenuItem
        Me.FENGGE2 = New System.Windows.Forms.MenuItem
        Me.mumfrmuintMR = New System.Windows.Forms.MenuItem
        Me.mumfrmunitVR = New System.Windows.Forms.MenuItem
        Me.mumfrmunitPN = New System.Windows.Forms.MenuItem
        Me.mumfrmunitSN = New System.Windows.Forms.MenuItem
        Me.FENGGE3 = New System.Windows.Forms.MenuItem
        Me.mumfrmunitKX = New System.Windows.Forms.MenuItem
        Me.mumfrmunitBZ = New System.Windows.Forms.MenuItem
        Me.FENGGE4 = New System.Windows.Forms.MenuItem
        Me.munfrmunitYP = New System.Windows.Forms.MenuItem
        Me.munfrmunitBiteye = New System.Windows.Forms.MenuItem
        Me.mumfrmunitMoudle = New System.Windows.Forms.MenuItem
        Me.mumfrmunit = New System.Windows.Forms.MenuItem
        Me.mumfrmunitPic = New System.Windows.Forms.MenuItem
        Me.mumfrmunitQuit = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'picFrmunitBg
        '
        Me.picFrmunitBg.BackColor = System.Drawing.SystemColors.Control
        Me.picFrmunitBg.Cursor = System.Windows.Forms.Cursors.Default
        Me.picFrmunitBg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.picFrmunitBg.Image = CType(resources.GetObject("picFrmunitBg.Image"), System.Drawing.Image)
        Me.picFrmunitBg.Location = New System.Drawing.Point(0, -9)
        Me.picFrmunitBg.Name = "picFrmunitBg"
        Me.picFrmunitBg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picFrmunitBg.Size = New System.Drawing.Size(808, 484)
        Me.picFrmunitBg.TabIndex = 0
        Me.picFrmunitBg.TabStop = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mumfrmunitChange, Me.mumfrmunitMoudle, Me.mumfrmunit, Me.mumfrmunitPic, Me.mumfrmunitQuit})
        '
        'mumfrmunitChange
        '
        Me.mumfrmunitChange.Index = 0
        Me.mumfrmunitChange.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.munfrmunitLenght, Me.munfrmunitArea, Me.mumfrmunitVolume, Me.mumfrmunitVolumeFluids, Me.FENGGE1, Me.mumfrmunitWeight, Me.mumfrmunitTemperature, Me.mumfrmunitPressure, Me.mumfrmunitDensity, Me.mumfrmunitVelocity, Me.mumfrmunitForce, Me.mumfrmunitE, Me.mumfrmunitPower, Me.FENGGE2, Me.mumfrmuintMR, Me.mumfrmunitVR, Me.mumfrmunitPN, Me.mumfrmunitSN, Me.FENGGE3, Me.mumfrmunitKX, Me.mumfrmunitBZ, Me.FENGGE4, Me.munfrmunitYP, Me.munfrmunitBiteye})
        Me.mumfrmunitChange.Text = "&C单位转换"
        '
        'munfrmunitLenght
        '
        Me.munfrmunitLenght.Index = 0
        Me.munfrmunitLenght.Text = "&L长度"
        '
        'munfrmunitArea
        '
        Me.munfrmunitArea.Index = 1
        Me.munfrmunitArea.Text = "&A面积"
        '
        'mumfrmunitVolume
        '
        Me.mumfrmunitVolume.Index = 2
        Me.mumfrmunitVolume.Text = "&V体积"
        '
        'mumfrmunitVolumeFluids
        '
        Me.mumfrmunitVolumeFluids.Index = 3
        Me.mumfrmunitVolumeFluids.Text = "&R容积"
        '
        'FENGGE1
        '
        Me.FENGGE1.Index = 4
        Me.FENGGE1.Text = "-"
        '
        'mumfrmunitWeight
        '
        Me.mumfrmunitWeight.Index = 5
        Me.mumfrmunitWeight.Text = "&M质量"
        '
        'mumfrmunitTemperature
        '
        Me.mumfrmunitTemperature.Index = 6
        Me.mumfrmunitTemperature.Text = "&T温度"
        '
        'mumfrmunitPressure
        '
        Me.mumfrmunitPressure.Index = 7
        Me.mumfrmunitPressure.Text = "&P压力"
        '
        'mumfrmunitDensity
        '
        Me.mumfrmunitDensity.Index = 8
        Me.mumfrmunitDensity.Text = "&D密度"
        '
        'mumfrmunitVelocity
        '
        Me.mumfrmunitVelocity.Index = 9
        Me.mumfrmunitVelocity.Text = "&V速度"
        '
        'mumfrmunitForce
        '
        Me.mumfrmunitForce.Index = 10
        Me.mumfrmunitForce.Text = "&F力"
        '
        'mumfrmunitE
        '
        Me.mumfrmunitE.Index = 11
        Me.mumfrmunitE.Text = "&E能量"
        '
        'mumfrmunitPower
        '
        Me.mumfrmunitPower.Index = 12
        Me.mumfrmunitPower.Text = "&P功率"
        '
        'FENGGE2
        '
        Me.FENGGE2.Index = 13
        Me.FENGGE2.Text = "-"
        '
        'mumfrmuintMR
        '
        Me.mumfrmuintMR.Index = 14
        Me.mumfrmuintMR.Text = "质量流率"
        '
        'mumfrmunitVR
        '
        Me.mumfrmunitVR.Index = 15
        Me.mumfrmunitVR.Text = "体积流率"
        '
        'mumfrmunitPN
        '
        Me.mumfrmunitPN.Index = 16
        Me.mumfrmunitPN.Text = "【动力】粘度"
        '
        'mumfrmunitSN
        '
        Me.mumfrmunitSN.Index = 17
        Me.mumfrmunitSN.Text = "运动粘度"
        Me.mumfrmunitSN.Visible = False
        '
        'FENGGE3
        '
        Me.FENGGE3.Index = 18
        Me.FENGGE3.Text = "-"
        Me.FENGGE3.Visible = False
        '
        'mumfrmunitKX
        '
        Me.mumfrmunitKX.Index = 19
        Me.mumfrmunitKX.Text = "扩散系数"
        Me.mumfrmunitKX.Visible = False
        '
        'mumfrmunitBZ
        '
        Me.mumfrmunitBZ.Index = 20
        Me.mumfrmunitBZ.Text = "表面张力"
        Me.mumfrmunitBZ.Visible = False
        '
        'FENGGE4
        '
        Me.FENGGE4.Index = 21
        Me.FENGGE4.Text = "-"
        '
        'munfrmunitYP
        '
        Me.munfrmunitYP.Index = 22
        Me.munfrmunitYP.Text = "钻井液剪切应力"
        '
        'munfrmunitBiteye
        '
        Me.munfrmunitBiteye.Index = 23
        Me.munfrmunitBiteye.Text = "钻头水眼号"
        '
        'mumfrmunitMoudle
        '
        Me.mumfrmunitMoudle.Index = 1
        Me.mumfrmunitMoudle.Text = "基本概念&M"
        Me.mumfrmunitMoudle.Visible = False
        '
        'mumfrmunit
        '
        Me.mumfrmunit.Index = 2
        Me.mumfrmunit.Text = "量和单位&U"
        Me.mumfrmunit.Visible = False
        '
        'mumfrmunitPic
        '
        Me.mumfrmunitPic.Index = 3
        Me.mumfrmunitPic.Text = "照片说明"
        '
        'mumfrmunitQuit
        '
        Me.mumfrmunitQuit.Index = 4
        Me.mumfrmunitQuit.Text = "&Q退出"
        '
        'frmunit
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(255, Byte), CType(255, Byte), CType(192, Byte))
        Me.ClientSize = New System.Drawing.Size(672, 437)
        Me.Controls.Add(Me.picFrmunitBg)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(11, 37)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "frmunit"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "工程单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunit
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunit
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunit()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Public Sub mumfrmuintMR_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmuintMR.Popup
		mumfrmuintMR_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmuintMR_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmuintMR.Click
		frmunitWVel.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitDensity_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitDensity.Popup
		mumfrmunitDensity_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitDensity_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitDensity.Click
		frmunitDensity.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitE_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitE.Popup
		mumfrmunitE_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitE_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitE.Click
		frmunitPVel.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitForce_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitForce.Popup
		mumfrmunitForce_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitForce_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitForce.Click
		frmunitForce.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitPic_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPic.Popup
		mumfrmunitPic_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitPic_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPic.Click
		frmunitPic.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitPN_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPN.Popup
		mumfrmunitPN_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitPN_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPN.Click
		frmunitsport.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitPower_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPower.Popup
		mumfrmunitPower_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitPower_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPower.Click
		frmunitPower.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitPressure_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPressure.Popup
		mumfrmunitPressure_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitPressure_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitPressure.Click
		frmunitPressure.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitQuit_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitQuit.Popup
		mumfrmunitQuit_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitQuit.Click
		Me.Close()
	End Sub
	
	Public Sub mumfrmunitSN_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitSN.Popup
		mumfrmunitSN_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitSN_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitSN.Click
		frmunitsport.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitTemperature_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitTemperature.Popup
		mumfrmunitTemperature_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitTemperature_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitTemperature.Click
		frmunitTemp.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitVelocity_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVelocity.Popup
		mumfrmunitVelocity_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitVelocity_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVelocity.Click
		frmunitVelocity.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitVolume_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVolume.Popup
		mumfrmunitVolume_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitVolume_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVolume.Click
		frmunitVolume.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitVolumeFluids_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVolumeFluids.Popup
		mumfrmunitVolumeFluids_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitVolumeFluids_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVolumeFluids.Click
		frmunitVFluids.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitVR_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVR.Popup
		mumfrmunitVR_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitVR_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitVR.Click
		frmunitVVel.DefInstance.ShowDialog()
	End Sub
	
	Public Sub mumfrmunitWeight_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitWeight.Popup
		mumfrmunitWeight_Click(eventSender, eventArgs)
	End Sub
	Public Sub mumfrmunitWeight_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mumfrmunitWeight.Click
		frmunitWeight.DefInstance.ShowDialog()
	End Sub
	
	Public Sub munfrmunitArea_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitArea.Popup
		munfrmunitArea_Click(eventSender, eventArgs)
	End Sub
	Public Sub munfrmunitArea_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitArea.Click
		frmunitArea.DefInstance.ShowDialog()
	End Sub
	
	Public Sub munfrmunitBiteye_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitBiteye.Popup
		munfrmunitBiteye_Click(eventSender, eventArgs)
	End Sub
	Public Sub munfrmunitBiteye_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitBiteye.Click
		frmunitBiteye.DefInstance.ShowDialog()
	End Sub
	
	Public Sub munfrmunitLenght_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitLenght.Popup
		munfrmunitLenght_Click(eventSender, eventArgs)
	End Sub
	Public Sub munfrmunitLenght_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitLenght.Click
		frmunitLenght.DefInstance.ShowDialog()
	End Sub
	
	Public Sub munfrmunitYP_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitYP.Popup
		munfrmunitYP_Click(eventSender, eventArgs)
	End Sub
	Public Sub munfrmunitYP_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles munfrmunitYP.Click
		frmunitYP.DefInstance.ShowDialog()
	End Sub

   
End Class