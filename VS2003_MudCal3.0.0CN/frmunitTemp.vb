Option Strict Off
Option Explicit On
Friend Class frmunitTemp
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents txtTempK As System.Windows.Forms.TextBox
	Public WithEvents txtTempF As System.Windows.Forms.TextBox
	Public WithEvents txtTempC As System.Windows.Forms.TextBox
	Public WithEvents lblTemp As System.Windows.Forms.Label
	Public WithEvents lblTempK As System.Windows.Forms.Label
	Public WithEvents lblTempF As System.Windows.Forms.Label
	Public WithEvents lblTempCelsius As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.txtTempK = New System.Windows.Forms.TextBox
        Me.txtTempF = New System.Windows.Forms.TextBox
        Me.txtTempC = New System.Windows.Forms.TextBox
        Me.lblTemp = New System.Windows.Forms.Label
        Me.lblTempK = New System.Windows.Forms.Label
        Me.lblTempF = New System.Windows.Forms.Label
        Me.lblTempCelsius = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(29, 190)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 26)
        Me.cmdquit.TabIndex = 7
        Me.cmdquit.Text = "退出"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(154, 190)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(87, 26)
        Me.cmdclear.TabIndex = 6
        Me.cmdclear.Text = "清除"
        '
        'txtTempK
        '
        Me.txtTempK.AcceptsReturn = True
        Me.txtTempK.AutoSize = False
        Me.txtTempK.BackColor = System.Drawing.SystemColors.Window
        Me.txtTempK.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTempK.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTempK.Location = New System.Drawing.Point(19, 146)
        Me.txtTempK.MaxLength = 0
        Me.txtTempK.Name = "txtTempK"
        Me.txtTempK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTempK.Size = New System.Drawing.Size(145, 27)
        Me.txtTempK.TabIndex = 2
        Me.txtTempK.Text = ""
        '
        'txtTempF
        '
        Me.txtTempF.AcceptsReturn = True
        Me.txtTempF.AutoSize = False
        Me.txtTempF.BackColor = System.Drawing.SystemColors.Window
        Me.txtTempF.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTempF.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTempF.Location = New System.Drawing.Point(19, 86)
        Me.txtTempF.MaxLength = 0
        Me.txtTempF.Name = "txtTempF"
        Me.txtTempF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTempF.Size = New System.Drawing.Size(145, 27)
        Me.txtTempF.TabIndex = 1
        Me.txtTempF.Text = ""
        '
        'txtTempC
        '
        Me.txtTempC.AcceptsReturn = True
        Me.txtTempC.AutoSize = False
        Me.txtTempC.BackColor = System.Drawing.SystemColors.Window
        Me.txtTempC.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTempC.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTempC.Location = New System.Drawing.Point(19, 34)
        Me.txtTempC.MaxLength = 0
        Me.txtTempC.Name = "txtTempC"
        Me.txtTempC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTempC.Size = New System.Drawing.Size(145, 27)
        Me.txtTempC.TabIndex = 0
        Me.txtTempC.Text = ""
        '
        'lblTemp
        '
        Me.lblTemp.BackColor = System.Drawing.SystemColors.Control
        Me.lblTemp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTemp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTemp.Location = New System.Drawing.Point(10, 9)
        Me.lblTemp.Name = "lblTemp"
        Me.lblTemp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTemp.Size = New System.Drawing.Size(202, 18)
        Me.lblTemp.TabIndex = 8
        Me.lblTemp.Text = "输入温度单位换算数据："
        '
        'lblTempK
        '
        Me.lblTempK.BackColor = System.Drawing.SystemColors.Control
        Me.lblTempK.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTempK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTempK.Location = New System.Drawing.Point(173, 155)
        Me.lblTempK.Name = "lblTempK"
        Me.lblTempK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTempK.Size = New System.Drawing.Size(145, 18)
        Me.lblTempK.TabIndex = 5
        Me.lblTempK.Text = "K开尔文"
        '
        'lblTempF
        '
        Me.lblTempF.BackColor = System.Drawing.SystemColors.Control
        Me.lblTempF.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTempF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTempF.Location = New System.Drawing.Point(173, 95)
        Me.lblTempF.Name = "lblTempF"
        Me.lblTempF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTempF.Size = New System.Drawing.Size(145, 18)
        Me.lblTempF.TabIndex = 4
        Me.lblTempF.Text = "°F华氏度"
        '
        'lblTempCelsius
        '
        Me.lblTempCelsius.BackColor = System.Drawing.SystemColors.Control
        Me.lblTempCelsius.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTempCelsius.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTempCelsius.Location = New System.Drawing.Point(173, 43)
        Me.lblTempCelsius.Name = "lblTempCelsius"
        Me.lblTempCelsius.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTempCelsius.Size = New System.Drawing.Size(135, 18)
        Me.lblTempCelsius.TabIndex = 3
        Me.lblTempCelsius.Text = "℃摄氏度"
        '
        'frmunitTemp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(333, 239)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.txtTempK)
        Me.Controls.Add(Me.txtTempF)
        Me.Controls.Add(Me.txtTempC)
        Me.Controls.Add(Me.lblTemp)
        Me.Controls.Add(Me.lblTempK)
        Me.Controls.Add(Me.lblTempF)
        Me.Controls.Add(Me.lblTempCelsius)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitTemp"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "温度(Temperature)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitTemp
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitTemp
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitTemp()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月15日17:43于青海省海西洲马海马北油田
	'温度单位换算
	
	
	Dim c As Single
	Dim f As Single
	Dim K As Single
	
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitTemp.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtTempC_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtTempC.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		c = Val(txtTempC.Text)
		
		f = 9 * (c / 5) + 32
		K = c + 273.15
		
		txtTempF.Text = CStr(f)
		txtTempK.Text = VB6.Format(K, "##0.00")
		
	End Sub
	
	Private Sub txtTempF_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtTempF.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		f = Val(txtTempF.Text)
		
		c = 5 / 9 * (f - 32)
		K = c + 273.15
		
		txtTempC.Text = CStr(c)
		txtTempK.Text = CStr(K)
	End Sub
	
	Private Sub txtTempK_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtTempK.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		K = Val(txtTempK.Text)
		
		c = K - 273.15
		f = 9 * (c / 5) + 32
		
		txtTempC.Text = CStr(c)
		txtTempF.Text = CStr(f)
	End Sub
End Class