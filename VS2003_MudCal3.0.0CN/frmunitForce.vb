Option Strict Off
Option Explicit On
Friend Class frmunitForce
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtForceshortt As System.Windows.Forms.TextBox
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents txtForcelongt As System.Windows.Forms.TextBox
	Public WithEvents txtForcelb As System.Windows.Forms.TextBox
	Public WithEvents txtForcepdl As System.Windows.Forms.TextBox
	Public WithEvents txtForcegr As System.Windows.Forms.TextBox
	Public WithEvents txtForcedyn As System.Windows.Forms.TextBox
	Public WithEvents txtForcen As System.Windows.Forms.TextBox
	Public WithEvents txtForcekn As System.Windows.Forms.TextBox
	Public WithEvents txtForceg As System.Windows.Forms.TextBox
	Public WithEvents txtForcekgf As System.Windows.Forms.TextBox
	Public WithEvents lblForceinput As System.Windows.Forms.Label
	Public WithEvents lblForcelongt As System.Windows.Forms.Label
	Public WithEvents lblForceshortt As System.Windows.Forms.Label
	Public WithEvents lblForcelb As System.Windows.Forms.Label
	Public WithEvents lblForcepdl As System.Windows.Forms.Label
	Public WithEvents lblForcegr As System.Windows.Forms.Label
	Public WithEvents lblForcedyn As System.Windows.Forms.Label
	Public WithEvents lblForcen As System.Windows.Forms.Label
	Public WithEvents lblForcekn As System.Windows.Forms.Label
	Public WithEvents lblForceg As System.Windows.Forms.Label
	Public WithEvents lblForcekgf As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblForcelongt = New System.Windows.Forms.Label
        Me.lblForceshortt = New System.Windows.Forms.Label
        Me.lblForcelb = New System.Windows.Forms.Label
        Me.lblForcepdl = New System.Windows.Forms.Label
        Me.lblForcegr = New System.Windows.Forms.Label
        Me.lblForcedyn = New System.Windows.Forms.Label
        Me.lblForcen = New System.Windows.Forms.Label
        Me.lblForcekn = New System.Windows.Forms.Label
        Me.lblForceg = New System.Windows.Forms.Label
        Me.lblForcekgf = New System.Windows.Forms.Label
        Me.txtForceshortt = New System.Windows.Forms.TextBox
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtForcelongt = New System.Windows.Forms.TextBox
        Me.txtForcelb = New System.Windows.Forms.TextBox
        Me.txtForcepdl = New System.Windows.Forms.TextBox
        Me.txtForcegr = New System.Windows.Forms.TextBox
        Me.txtForcedyn = New System.Windows.Forms.TextBox
        Me.txtForcen = New System.Windows.Forms.TextBox
        Me.txtForcekn = New System.Windows.Forms.TextBox
        Me.txtForceg = New System.Windows.Forms.TextBox
        Me.txtForcekgf = New System.Windows.Forms.TextBox
        Me.lblForceinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblForcelongt
        '
        Me.lblForcelongt.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcelongt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcelongt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcelongt.Location = New System.Drawing.Point(269, 172)
        Me.lblForcelongt.Name = "lblForcelongt"
        Me.lblForcelongt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcelongt.Size = New System.Drawing.Size(78, 19)
        Me.lblForcelongt.TabIndex = 18
        Me.lblForcelongt.Text = "long t(uk)"
        Me.ToolTip1.SetToolTip(Me.lblForcelongt, "英吨(英国)")
        '
        'lblForceshortt
        '
        Me.lblForceshortt.BackColor = System.Drawing.SystemColors.Control
        Me.lblForceshortt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForceshortt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForceshortt.Location = New System.Drawing.Point(269, 138)
        Me.lblForceshortt.Name = "lblForceshortt"
        Me.lblForceshortt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForceshortt.Size = New System.Drawing.Size(87, 18)
        Me.lblForceshortt.TabIndex = 17
        Me.lblForceshortt.Text = "short t(us)"
        Me.ToolTip1.SetToolTip(Me.lblForceshortt, "英吨(美国)")
        '
        'lblForcelb
        '
        Me.lblForcelb.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcelb.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcelb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcelb.Location = New System.Drawing.Point(269, 103)
        Me.lblForcelb.Name = "lblForcelb"
        Me.lblForcelb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcelb.Size = New System.Drawing.Size(68, 19)
        Me.lblForcelb.TabIndex = 16
        Me.lblForcelb.Text = "lb"
        Me.ToolTip1.SetToolTip(Me.lblForcelb, "磅力")
        '
        'lblForcepdl
        '
        Me.lblForcepdl.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcepdl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcepdl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcepdl.Location = New System.Drawing.Point(269, 69)
        Me.lblForcepdl.Name = "lblForcepdl"
        Me.lblForcepdl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcepdl.Size = New System.Drawing.Size(68, 18)
        Me.lblForcepdl.TabIndex = 15
        Me.lblForcepdl.Text = "pdl"
        Me.ToolTip1.SetToolTip(Me.lblForcepdl, "磅达")
        '
        'lblForcegr
        '
        Me.lblForcegr.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcegr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcegr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcegr.Location = New System.Drawing.Point(269, 34)
        Me.lblForcegr.Name = "lblForcegr"
        Me.lblForcegr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcegr.Size = New System.Drawing.Size(68, 19)
        Me.lblForcegr.TabIndex = 14
        Me.lblForcegr.Text = "gr"
        Me.ToolTip1.SetToolTip(Me.lblForcegr, "格令")
        '
        'lblForcedyn
        '
        Me.lblForcedyn.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcedyn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcedyn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcedyn.Location = New System.Drawing.Point(96, 172)
        Me.lblForcedyn.Name = "lblForcedyn"
        Me.lblForcedyn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcedyn.Size = New System.Drawing.Size(68, 19)
        Me.lblForcedyn.TabIndex = 9
        Me.lblForcedyn.Text = "dyn"
        Me.ToolTip1.SetToolTip(Me.lblForcedyn, "达因")
        '
        'lblForcen
        '
        Me.lblForcen.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcen.Location = New System.Drawing.Point(96, 138)
        Me.lblForcen.Name = "lblForcen"
        Me.lblForcen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcen.Size = New System.Drawing.Size(68, 18)
        Me.lblForcen.TabIndex = 8
        Me.lblForcen.Text = "N"
        Me.ToolTip1.SetToolTip(Me.lblForcen, "牛【顿】")
        '
        'lblForcekn
        '
        Me.lblForcekn.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcekn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcekn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcekn.Location = New System.Drawing.Point(96, 103)
        Me.lblForcekn.Name = "lblForcekn"
        Me.lblForcekn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcekn.Size = New System.Drawing.Size(68, 19)
        Me.lblForcekn.TabIndex = 7
        Me.lblForcekn.Text = "kN"
        Me.ToolTip1.SetToolTip(Me.lblForcekn, "千牛【顿】")
        '
        'lblForceg
        '
        Me.lblForceg.BackColor = System.Drawing.SystemColors.Control
        Me.lblForceg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForceg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForceg.Location = New System.Drawing.Point(96, 69)
        Me.lblForceg.Name = "lblForceg"
        Me.lblForceg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForceg.Size = New System.Drawing.Size(68, 18)
        Me.lblForceg.TabIndex = 6
        Me.lblForceg.Text = "g"
        Me.ToolTip1.SetToolTip(Me.lblForceg, "克")
        '
        'lblForcekgf
        '
        Me.lblForcekgf.BackColor = System.Drawing.SystemColors.Control
        Me.lblForcekgf.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForcekgf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForcekgf.Location = New System.Drawing.Point(96, 34)
        Me.lblForcekgf.Name = "lblForcekgf"
        Me.lblForcekgf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForcekgf.Size = New System.Drawing.Size(68, 19)
        Me.lblForcekgf.TabIndex = 5
        Me.lblForcekgf.Text = "Kgf"
        Me.ToolTip1.SetToolTip(Me.lblForcekgf, "千克力")
        '
        'txtForceshortt
        '
        Me.txtForceshortt.AcceptsReturn = True
        Me.txtForceshortt.AutoSize = False
        Me.txtForceshortt.BackColor = System.Drawing.SystemColors.Window
        Me.txtForceshortt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForceshortt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForceshortt.Location = New System.Drawing.Point(182, 138)
        Me.txtForceshortt.MaxLength = 0
        Me.txtForceshortt.Name = "txtForceshortt"
        Me.txtForceshortt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForceshortt.Size = New System.Drawing.Size(78, 19)
        Me.txtForceshortt.TabIndex = 22
        Me.txtForceshortt.Text = ""
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(211, 215)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(88, 27)
        Me.cmdClear.TabIndex = 20
        Me.cmdClear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(67, 215)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 19
        Me.cmdquit.Text = "退出"
        '
        'txtForcelongt
        '
        Me.txtForcelongt.AcceptsReturn = True
        Me.txtForcelongt.AutoSize = False
        Me.txtForcelongt.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcelongt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcelongt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcelongt.Location = New System.Drawing.Point(182, 172)
        Me.txtForcelongt.MaxLength = 0
        Me.txtForcelongt.Name = "txtForcelongt"
        Me.txtForcelongt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcelongt.Size = New System.Drawing.Size(78, 20)
        Me.txtForcelongt.TabIndex = 13
        Me.txtForcelongt.Text = ""
        '
        'txtForcelb
        '
        Me.txtForcelb.AcceptsReturn = True
        Me.txtForcelb.AutoSize = False
        Me.txtForcelb.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcelb.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcelb.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcelb.Location = New System.Drawing.Point(182, 103)
        Me.txtForcelb.MaxLength = 0
        Me.txtForcelb.Name = "txtForcelb"
        Me.txtForcelb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcelb.Size = New System.Drawing.Size(78, 20)
        Me.txtForcelb.TabIndex = 12
        Me.txtForcelb.Text = ""
        '
        'txtForcepdl
        '
        Me.txtForcepdl.AcceptsReturn = True
        Me.txtForcepdl.AutoSize = False
        Me.txtForcepdl.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcepdl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcepdl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcepdl.Location = New System.Drawing.Point(182, 69)
        Me.txtForcepdl.MaxLength = 0
        Me.txtForcepdl.Name = "txtForcepdl"
        Me.txtForcepdl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcepdl.Size = New System.Drawing.Size(78, 19)
        Me.txtForcepdl.TabIndex = 11
        Me.txtForcepdl.Text = ""
        '
        'txtForcegr
        '
        Me.txtForcegr.AcceptsReturn = True
        Me.txtForcegr.AutoSize = False
        Me.txtForcegr.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcegr.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcegr.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcegr.Location = New System.Drawing.Point(182, 34)
        Me.txtForcegr.MaxLength = 0
        Me.txtForcegr.Name = "txtForcegr"
        Me.txtForcegr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcegr.Size = New System.Drawing.Size(78, 20)
        Me.txtForcegr.TabIndex = 10
        Me.txtForcegr.Text = ""
        '
        'txtForcedyn
        '
        Me.txtForcedyn.AcceptsReturn = True
        Me.txtForcedyn.AutoSize = False
        Me.txtForcedyn.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcedyn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcedyn.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcedyn.Location = New System.Drawing.Point(10, 172)
        Me.txtForcedyn.MaxLength = 0
        Me.txtForcedyn.Name = "txtForcedyn"
        Me.txtForcedyn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcedyn.Size = New System.Drawing.Size(78, 20)
        Me.txtForcedyn.TabIndex = 4
        Me.txtForcedyn.Text = ""
        '
        'txtForcen
        '
        Me.txtForcen.AcceptsReturn = True
        Me.txtForcen.AutoSize = False
        Me.txtForcen.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcen.Location = New System.Drawing.Point(10, 138)
        Me.txtForcen.MaxLength = 0
        Me.txtForcen.Name = "txtForcen"
        Me.txtForcen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcen.Size = New System.Drawing.Size(78, 19)
        Me.txtForcen.TabIndex = 3
        Me.txtForcen.Text = ""
        '
        'txtForcekn
        '
        Me.txtForcekn.AcceptsReturn = True
        Me.txtForcekn.AutoSize = False
        Me.txtForcekn.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcekn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcekn.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcekn.Location = New System.Drawing.Point(10, 103)
        Me.txtForcekn.MaxLength = 0
        Me.txtForcekn.Name = "txtForcekn"
        Me.txtForcekn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcekn.Size = New System.Drawing.Size(78, 20)
        Me.txtForcekn.TabIndex = 2
        Me.txtForcekn.Text = ""
        '
        'txtForceg
        '
        Me.txtForceg.AcceptsReturn = True
        Me.txtForceg.AutoSize = False
        Me.txtForceg.BackColor = System.Drawing.SystemColors.Window
        Me.txtForceg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForceg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForceg.Location = New System.Drawing.Point(10, 69)
        Me.txtForceg.MaxLength = 0
        Me.txtForceg.Name = "txtForceg"
        Me.txtForceg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForceg.Size = New System.Drawing.Size(78, 19)
        Me.txtForceg.TabIndex = 1
        Me.txtForceg.Text = ""
        '
        'txtForcekgf
        '
        Me.txtForcekgf.AcceptsReturn = True
        Me.txtForcekgf.AutoSize = False
        Me.txtForcekgf.BackColor = System.Drawing.SystemColors.Window
        Me.txtForcekgf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtForcekgf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtForcekgf.Location = New System.Drawing.Point(10, 34)
        Me.txtForcekgf.MaxLength = 0
        Me.txtForcekgf.Name = "txtForcekgf"
        Me.txtForcekgf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtForcekgf.Size = New System.Drawing.Size(78, 20)
        Me.txtForcekgf.TabIndex = 0
        Me.txtForcekgf.Text = ""
        '
        'lblForceinput
        '
        Me.lblForceinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblForceinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblForceinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblForceinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblForceinput.Location = New System.Drawing.Point(10, 9)
        Me.lblForceinput.Name = "lblForceinput"
        Me.lblForceinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblForceinput.Size = New System.Drawing.Size(241, 18)
        Me.lblForceinput.TabIndex = 21
        Me.lblForceinput.Text = "输入力单位换算数据："
        '
        'frmunitForce
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(357, 263)
        Me.Controls.Add(Me.txtForceshortt)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.txtForcelongt)
        Me.Controls.Add(Me.txtForcelb)
        Me.Controls.Add(Me.txtForcepdl)
        Me.Controls.Add(Me.txtForcegr)
        Me.Controls.Add(Me.txtForcedyn)
        Me.Controls.Add(Me.txtForcen)
        Me.Controls.Add(Me.txtForcekn)
        Me.Controls.Add(Me.txtForceg)
        Me.Controls.Add(Me.txtForcekgf)
        Me.Controls.Add(Me.lblForceinput)
        Me.Controls.Add(Me.lblForcelongt)
        Me.Controls.Add(Me.lblForceshortt)
        Me.Controls.Add(Me.lblForcelb)
        Me.Controls.Add(Me.lblForcepdl)
        Me.Controls.Add(Me.lblForcegr)
        Me.Controls.Add(Me.lblForcedyn)
        Me.Controls.Add(Me.lblForcen)
        Me.Controls.Add(Me.lblForcekn)
        Me.Controls.Add(Me.lblForceg)
        Me.Controls.Add(Me.lblForcekgf)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitForce"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "力(Force)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitForce
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitForce
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitForce()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月31日17:35于青海省海西洲大柴旦红山参2井
	'力单位换算
	
	Dim kgf As Single
	Dim g As Single
	Dim kn As Single
	Dim N As Single
	Dim dyn As Single
	Dim gr As Single
	Dim pdl As Single
	Dim lb As Single
	Dim shortt As Single
	Dim longt As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitForce.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtForcedyn_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcedyn.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dyn = Val(txtForcedyn.Text)
		
		kgf = dyn * 0.0000010197
		g = dyn * 0.001
		kn = dyn * 0.00000001
		N = dyn * 0.00001
		gr = dyn * 0.0157
		pdl = dyn * 0.00007233
		lb = dyn * 0.0000022481
		shortt = dyn * 0.000000001124
		longt = dyn * 0.0000000010036
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		'txtForcedyn.Text = dyn
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForceg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForceg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		g = Val(txtForceg.Text)
		
		kgf = g * 0.001
		kn = g * 0.0000098067
		N = g * 0.0098
		dyn = g * 980.665
		gr = g * 15.4324
		pdl = g * 0.0709
		lb = g * 0.0022
		shortt = g * 0.0000011023
		longt = g * 0.00000098421
		
		txtForcekgf.Text = CStr(kgf)
		'txtForceg.Text = g
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForcegr_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcegr.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gr = Val(txtForcegr.Text)
		
		kgf = gr * 0.0000010197
		g = gr * 0.001
		kn = gr * 0.00000001
		N = gr * 0.00001
		dyn = gr * 0.0157
		pdl = gr * 0.00007233
		lb = gr * 0.0000022481
		shortt = gr * 0.000000001124
		longt = gr * 0.0000000010036
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		'txtForcegr.Text = gr
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForcekgf_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcekgf.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgf = Val(txtForcekgf.Text)
		
		g = kgf * 1000#
		kn = kgf * 0.0098
		N = kgf * 9.8067
		dyn = kgf * 980670#
		gr = kgf * 15432#
		pdl = kgf * 70.9316
		lb = kgf * 2.2046
		shortt = kgf * 0.0011
		longt = kgf * 0.001
		
		'txtForcekgf.Text = kgf
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForcekn_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcekn.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kn = Val(txtForcekn.Text)
		
		kgf = kn * 101.9716
		g = kn * 101970#
		N = kn * 1000#
		dyn = kn * 100000000#
		gr = kn * 1573700#
		pdl = kn * 7233.0139
		lb = kn * 224.8089
		shortt = kn * 0.1124
		longt = kn * 0.1004
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		'txtForcekn.Text = kn
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForcelb_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcelb.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lb = Val(txtForcelb.Text)
		
		kgf = lb * 0.45361
		g = lb * 453.5924
		kn = lb * 0.0044
		N = lb * 4.4482
		dyn = lb * 444820#
		gr = lb * 7000#
		pdl = lb * 32.174
		shortt = lb * 0.00049999
		longt = lb * 0.0004464
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		'txtForcelb.Text = lb
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	
	
	Private Sub txtForcelongt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcelongt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		longt = Val(txtForcelongt.Text)
		
		kgf = longt * 1016.047
		g = longt * 1016000#
		kn = longt * 9.964
		N = longt * 9964.0173
		dyn = longt * 996400000#
		gr = longt * 15680000#
		pdl = longt * 72070#
		lb = longt * 2240#
		shortt = longt * 1.12
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		'txtForcelongt.Text = longt
	End Sub
	
	Private Sub txtForcen_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcen.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		N = Val(txtForcen.Text)
		
		kgf = N * 0.102
		g = N * 101.9716
		kn = N * 0.001
		dyn = N * 100000#
		gr = N * 1573.6626
		pdl = N * 7.233
		lb = N * 0.2248
		shortt = N * 0.0001124
		longt = N * 0.00010035
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		'txtForcen.Text = n
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForcepdl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForcepdl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		pdl = Val(txtForcepdl.Text)
		
		kgf = pdl * 0.0141
		g = pdl * 14.0981
		kn = pdl * 0.00013826
		N = pdl * 0.1383
		dyn = pdl * 13825#
		gr = pdl * 217.5667
		lb = pdl * 0.0311
		shortt = pdl * 0.00001554
		longt = pdl * 0.000013875
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		'txtForcepdl.Text = pdl
		txtForcelb.Text = CStr(lb)
		txtForceshortt.Text = CStr(shortt)
		txtForcelongt.Text = CStr(longt)
	End Sub
	
	Private Sub txtForceshortt_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtForceshortt.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		shortt = Val(txtForceshortt.Text)
		
		kgf = shortt * 907.1847
		g = shortt * 907180#
		kn = shortt * 8.8964
		N = shortt * 8896.4428
		dyn = shortt * 889640000#
		gr = shortt * 14000000#
		pdl = shortt * 64348#
		lb = shortt * 2000#
		longt = shortt * 0.8929
		
		txtForcekgf.Text = CStr(kgf)
		txtForceg.Text = CStr(g)
		txtForcekn.Text = CStr(kn)
		txtForcen.Text = CStr(N)
		txtForcedyn.Text = CStr(dyn)
		txtForcegr.Text = CStr(gr)
		txtForcepdl.Text = CStr(pdl)
		txtForcelb.Text = CStr(lb)
		'txtForceshortt.Text = shortt
		txtForcelongt.Text = CStr(longt)
	End Sub
End Class