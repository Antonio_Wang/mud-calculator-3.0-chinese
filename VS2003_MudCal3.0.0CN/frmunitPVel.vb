Option Strict Off
Option Explicit On
Friend Class frmunitPVel
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtPVelmj As System.Windows.Forms.TextBox
	Public WithEvents txtPVelj As System.Windows.Forms.TextBox
	Public WithEvents txtPVelkj As System.Windows.Forms.TextBox
	Public WithEvents txtPVelzj As System.Windows.Forms.TextBox
	Public WithEvents txtPVelkcal As System.Windows.Forms.TextBox
	Public WithEvents txtPVelbtu As System.Windows.Forms.TextBox
	Public WithEvents txtPVelkwh As System.Windows.Forms.TextBox
	Public WithEvents txtPVelhph As System.Windows.Forms.TextBox
	Public WithEvents txtPVelkgm As System.Windows.Forms.TextBox
	Public WithEvents txtPVelftlb As System.Windows.Forms.TextBox
	Public WithEvents txtPVellatm As System.Windows.Forms.TextBox
	Public WithEvents txtPVelftpdl As System.Windows.Forms.TextBox
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents lblPVelmj As System.Windows.Forms.Label
	Public WithEvents lblPVelj As System.Windows.Forms.Label
	Public WithEvents lblPVelkj As System.Windows.Forms.Label
	Public WithEvents lblPowerzj As System.Windows.Forms.Label
	Public WithEvents lblPVelkcal As System.Windows.Forms.Label
	Public WithEvents lblPVelbtu As System.Windows.Forms.Label
	Public WithEvents lblPVelkwh As System.Windows.Forms.Label
	Public WithEvents lblPVelhph As System.Windows.Forms.Label
	Public WithEvents lblPVelkgm As System.Windows.Forms.Label
	Public WithEvents lblPVelftlb As System.Windows.Forms.Label
	Public WithEvents lblPVellatm As System.Windows.Forms.Label
	Public WithEvents lblPVelftpdl As System.Windows.Forms.Label
	Public WithEvents lblPVelinput As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblPVelmj = New System.Windows.Forms.Label
        Me.lblPVelj = New System.Windows.Forms.Label
        Me.lblPVelkj = New System.Windows.Forms.Label
        Me.lblPowerzj = New System.Windows.Forms.Label
        Me.lblPVelkcal = New System.Windows.Forms.Label
        Me.lblPVelbtu = New System.Windows.Forms.Label
        Me.lblPVelkwh = New System.Windows.Forms.Label
        Me.lblPVelhph = New System.Windows.Forms.Label
        Me.lblPVelkgm = New System.Windows.Forms.Label
        Me.lblPVelftlb = New System.Windows.Forms.Label
        Me.lblPVellatm = New System.Windows.Forms.Label
        Me.lblPVelftpdl = New System.Windows.Forms.Label
        Me.txtPVelmj = New System.Windows.Forms.TextBox
        Me.txtPVelj = New System.Windows.Forms.TextBox
        Me.txtPVelkj = New System.Windows.Forms.TextBox
        Me.txtPVelzj = New System.Windows.Forms.TextBox
        Me.txtPVelkcal = New System.Windows.Forms.TextBox
        Me.txtPVelbtu = New System.Windows.Forms.TextBox
        Me.txtPVelkwh = New System.Windows.Forms.TextBox
        Me.txtPVelhph = New System.Windows.Forms.TextBox
        Me.txtPVelkgm = New System.Windows.Forms.TextBox
        Me.txtPVelftlb = New System.Windows.Forms.TextBox
        Me.txtPVellatm = New System.Windows.Forms.TextBox
        Me.txtPVelftpdl = New System.Windows.Forms.TextBox
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdClear = New System.Windows.Forms.Button
        Me.lblPVelinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblPVelmj
        '
        Me.lblPVelmj.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelmj.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelmj.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelmj.Location = New System.Drawing.Point(115, 34)
        Me.lblPVelmj.Name = "lblPVelmj"
        Me.lblPVelmj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelmj.Size = New System.Drawing.Size(69, 19)
        Me.lblPVelmj.TabIndex = 26
        Me.lblPVelmj.Text = "mJ"
        Me.ToolTip1.SetToolTip(Me.lblPVelmj, "毫焦")
        '
        'lblPVelj
        '
        Me.lblPVelj.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelj.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelj.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelj.Location = New System.Drawing.Point(115, 69)
        Me.lblPVelj.Name = "lblPVelj"
        Me.lblPVelj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelj.Size = New System.Drawing.Size(69, 18)
        Me.lblPVelj.TabIndex = 25
        Me.lblPVelj.Text = "J"
        Me.ToolTip1.SetToolTip(Me.lblPVelj, "焦")
        '
        'lblPVelkj
        '
        Me.lblPVelkj.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelkj.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelkj.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelkj.Location = New System.Drawing.Point(115, 103)
        Me.lblPVelkj.Name = "lblPVelkj"
        Me.lblPVelkj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelkj.Size = New System.Drawing.Size(69, 19)
        Me.lblPVelkj.TabIndex = 24
        Me.lblPVelkj.Text = "kJ"
        Me.ToolTip1.SetToolTip(Me.lblPVelkj, "千焦")
        '
        'lblPowerzj
        '
        Me.lblPowerzj.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerzj.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerzj.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerzj.Location = New System.Drawing.Point(115, 138)
        Me.lblPowerzj.Name = "lblPowerzj"
        Me.lblPowerzj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerzj.Size = New System.Drawing.Size(69, 18)
        Me.lblPowerzj.TabIndex = 23
        Me.lblPowerzj.Text = "MJ"
        Me.ToolTip1.SetToolTip(Me.lblPowerzj, "兆焦")
        '
        'lblPVelkcal
        '
        Me.lblPVelkcal.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelkcal.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelkcal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelkcal.Location = New System.Drawing.Point(115, 172)
        Me.lblPVelkcal.Name = "lblPVelkcal"
        Me.lblPVelkcal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelkcal.Size = New System.Drawing.Size(69, 19)
        Me.lblPVelkcal.TabIndex = 22
        Me.lblPVelkcal.Text = "kcal"
        Me.ToolTip1.SetToolTip(Me.lblPVelkcal, "千卡")
        '
        'lblPVelbtu
        '
        Me.lblPVelbtu.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelbtu.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelbtu.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelbtu.Location = New System.Drawing.Point(115, 207)
        Me.lblPVelbtu.Name = "lblPVelbtu"
        Me.lblPVelbtu.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelbtu.Size = New System.Drawing.Size(69, 18)
        Me.lblPVelbtu.TabIndex = 21
        Me.lblPVelbtu.Text = "BTU"
        Me.ToolTip1.SetToolTip(Me.lblPVelbtu, "英热单位")
        '
        'lblPVelkwh
        '
        Me.lblPVelkwh.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelkwh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelkwh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelkwh.Location = New System.Drawing.Point(288, 34)
        Me.lblPVelkwh.Name = "lblPVelkwh"
        Me.lblPVelkwh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelkwh.Size = New System.Drawing.Size(68, 19)
        Me.lblPVelkwh.TabIndex = 20
        Me.lblPVelkwh.Text = "kW.h"
        Me.ToolTip1.SetToolTip(Me.lblPVelkwh, "千瓦时")
        '
        'lblPVelhph
        '
        Me.lblPVelhph.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelhph.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelhph.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelhph.Location = New System.Drawing.Point(288, 69)
        Me.lblPVelhph.Name = "lblPVelhph"
        Me.lblPVelhph.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelhph.Size = New System.Drawing.Size(68, 18)
        Me.lblPVelhph.TabIndex = 19
        Me.lblPVelhph.Text = "HP.h"
        Me.ToolTip1.SetToolTip(Me.lblPVelhph, "马力时")
        '
        'lblPVelkgm
        '
        Me.lblPVelkgm.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelkgm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelkgm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelkgm.Location = New System.Drawing.Point(288, 103)
        Me.lblPVelkgm.Name = "lblPVelkgm"
        Me.lblPVelkgm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelkgm.Size = New System.Drawing.Size(68, 19)
        Me.lblPVelkgm.TabIndex = 18
        Me.lblPVelkgm.Text = "kg.m"
        Me.ToolTip1.SetToolTip(Me.lblPVelkgm, "千克米")
        '
        'lblPVelftlb
        '
        Me.lblPVelftlb.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelftlb.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelftlb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelftlb.Location = New System.Drawing.Point(288, 138)
        Me.lblPVelftlb.Name = "lblPVelftlb"
        Me.lblPVelftlb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelftlb.Size = New System.Drawing.Size(68, 18)
        Me.lblPVelftlb.TabIndex = 17
        Me.lblPVelftlb.Text = "ft.lb"
        Me.ToolTip1.SetToolTip(Me.lblPVelftlb, "英尺磅")
        '
        'lblPVellatm
        '
        Me.lblPVellatm.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVellatm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVellatm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVellatm.Location = New System.Drawing.Point(288, 172)
        Me.lblPVellatm.Name = "lblPVellatm"
        Me.lblPVellatm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVellatm.Size = New System.Drawing.Size(68, 19)
        Me.lblPVellatm.TabIndex = 16
        Me.lblPVellatm.Text = "l.atm"
        Me.ToolTip1.SetToolTip(Me.lblPVellatm, "升大气压")
        '
        'lblPVelftpdl
        '
        Me.lblPVelftpdl.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelftpdl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelftpdl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelftpdl.Location = New System.Drawing.Point(288, 207)
        Me.lblPVelftpdl.Name = "lblPVelftpdl"
        Me.lblPVelftpdl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelftpdl.Size = New System.Drawing.Size(68, 18)
        Me.lblPVelftpdl.TabIndex = 15
        Me.lblPVelftpdl.Text = "ft.pdl"
        Me.ToolTip1.SetToolTip(Me.lblPVelftpdl, "英尺磅达")
        '
        'txtPVelmj
        '
        Me.txtPVelmj.AcceptsReturn = True
        Me.txtPVelmj.AutoSize = False
        Me.txtPVelmj.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelmj.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelmj.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelmj.Location = New System.Drawing.Point(19, 34)
        Me.txtPVelmj.MaxLength = 0
        Me.txtPVelmj.Name = "txtPVelmj"
        Me.txtPVelmj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelmj.Size = New System.Drawing.Size(88, 20)
        Me.txtPVelmj.TabIndex = 13
        Me.txtPVelmj.Text = ""
        '
        'txtPVelj
        '
        Me.txtPVelj.AcceptsReturn = True
        Me.txtPVelj.AutoSize = False
        Me.txtPVelj.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelj.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelj.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelj.Location = New System.Drawing.Point(19, 69)
        Me.txtPVelj.MaxLength = 0
        Me.txtPVelj.Name = "txtPVelj"
        Me.txtPVelj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelj.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelj.TabIndex = 12
        Me.txtPVelj.Text = ""
        '
        'txtPVelkj
        '
        Me.txtPVelkj.AcceptsReturn = True
        Me.txtPVelkj.AutoSize = False
        Me.txtPVelkj.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelkj.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelkj.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelkj.Location = New System.Drawing.Point(19, 103)
        Me.txtPVelkj.MaxLength = 0
        Me.txtPVelkj.Name = "txtPVelkj"
        Me.txtPVelkj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelkj.Size = New System.Drawing.Size(88, 20)
        Me.txtPVelkj.TabIndex = 11
        Me.txtPVelkj.Text = ""
        '
        'txtPVelzj
        '
        Me.txtPVelzj.AcceptsReturn = True
        Me.txtPVelzj.AutoSize = False
        Me.txtPVelzj.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelzj.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelzj.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelzj.Location = New System.Drawing.Point(19, 138)
        Me.txtPVelzj.MaxLength = 0
        Me.txtPVelzj.Name = "txtPVelzj"
        Me.txtPVelzj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelzj.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelzj.TabIndex = 10
        Me.txtPVelzj.Text = ""
        '
        'txtPVelkcal
        '
        Me.txtPVelkcal.AcceptsReturn = True
        Me.txtPVelkcal.AutoSize = False
        Me.txtPVelkcal.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelkcal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelkcal.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelkcal.Location = New System.Drawing.Point(19, 172)
        Me.txtPVelkcal.MaxLength = 0
        Me.txtPVelkcal.Name = "txtPVelkcal"
        Me.txtPVelkcal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelkcal.Size = New System.Drawing.Size(88, 20)
        Me.txtPVelkcal.TabIndex = 9
        Me.txtPVelkcal.Text = ""
        '
        'txtPVelbtu
        '
        Me.txtPVelbtu.AcceptsReturn = True
        Me.txtPVelbtu.AutoSize = False
        Me.txtPVelbtu.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelbtu.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelbtu.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelbtu.Location = New System.Drawing.Point(19, 207)
        Me.txtPVelbtu.MaxLength = 0
        Me.txtPVelbtu.Name = "txtPVelbtu"
        Me.txtPVelbtu.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelbtu.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelbtu.TabIndex = 8
        Me.txtPVelbtu.Text = ""
        '
        'txtPVelkwh
        '
        Me.txtPVelkwh.AcceptsReturn = True
        Me.txtPVelkwh.AutoSize = False
        Me.txtPVelkwh.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelkwh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelkwh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelkwh.Location = New System.Drawing.Point(192, 34)
        Me.txtPVelkwh.MaxLength = 0
        Me.txtPVelkwh.Name = "txtPVelkwh"
        Me.txtPVelkwh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelkwh.Size = New System.Drawing.Size(88, 20)
        Me.txtPVelkwh.TabIndex = 7
        Me.txtPVelkwh.Text = ""
        '
        'txtPVelhph
        '
        Me.txtPVelhph.AcceptsReturn = True
        Me.txtPVelhph.AutoSize = False
        Me.txtPVelhph.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelhph.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelhph.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelhph.Location = New System.Drawing.Point(192, 69)
        Me.txtPVelhph.MaxLength = 0
        Me.txtPVelhph.Name = "txtPVelhph"
        Me.txtPVelhph.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelhph.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelhph.TabIndex = 6
        Me.txtPVelhph.Text = ""
        '
        'txtPVelkgm
        '
        Me.txtPVelkgm.AcceptsReturn = True
        Me.txtPVelkgm.AutoSize = False
        Me.txtPVelkgm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelkgm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelkgm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelkgm.Location = New System.Drawing.Point(192, 103)
        Me.txtPVelkgm.MaxLength = 0
        Me.txtPVelkgm.Name = "txtPVelkgm"
        Me.txtPVelkgm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelkgm.Size = New System.Drawing.Size(88, 20)
        Me.txtPVelkgm.TabIndex = 5
        Me.txtPVelkgm.Text = ""
        '
        'txtPVelftlb
        '
        Me.txtPVelftlb.AcceptsReturn = True
        Me.txtPVelftlb.AutoSize = False
        Me.txtPVelftlb.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelftlb.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelftlb.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelftlb.Location = New System.Drawing.Point(192, 138)
        Me.txtPVelftlb.MaxLength = 0
        Me.txtPVelftlb.Name = "txtPVelftlb"
        Me.txtPVelftlb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelftlb.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelftlb.TabIndex = 4
        Me.txtPVelftlb.Text = ""
        '
        'txtPVellatm
        '
        Me.txtPVellatm.AcceptsReturn = True
        Me.txtPVellatm.AutoSize = False
        Me.txtPVellatm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVellatm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVellatm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVellatm.Location = New System.Drawing.Point(192, 172)
        Me.txtPVellatm.MaxLength = 0
        Me.txtPVellatm.Name = "txtPVellatm"
        Me.txtPVellatm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVellatm.Size = New System.Drawing.Size(88, 20)
        Me.txtPVellatm.TabIndex = 3
        Me.txtPVellatm.Text = ""
        '
        'txtPVelftpdl
        '
        Me.txtPVelftpdl.AcceptsReturn = True
        Me.txtPVelftpdl.AutoSize = False
        Me.txtPVelftpdl.BackColor = System.Drawing.SystemColors.Window
        Me.txtPVelftpdl.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPVelftpdl.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPVelftpdl.Location = New System.Drawing.Point(192, 207)
        Me.txtPVelftpdl.MaxLength = 0
        Me.txtPVelftpdl.Name = "txtPVelftpdl"
        Me.txtPVelftpdl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPVelftpdl.Size = New System.Drawing.Size(88, 19)
        Me.txtPVelftpdl.TabIndex = 2
        Me.txtPVelftpdl.Text = ""
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(58, 250)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 1
        Me.cmdquit.Text = "退出"
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(240, 250)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(88, 27)
        Me.cmdClear.TabIndex = 0
        Me.cmdClear.Text = "清除"
        '
        'lblPVelinput
        '
        Me.lblPVelinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblPVelinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPVelinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblPVelinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPVelinput.Location = New System.Drawing.Point(19, 9)
        Me.lblPVelinput.Name = "lblPVelinput"
        Me.lblPVelinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPVelinput.Size = New System.Drawing.Size(280, 18)
        Me.lblPVelinput.TabIndex = 14
        Me.lblPVelinput.Text = "输入能量[功]单位换算数据："
        '
        'frmunitPVel
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(372, 300)
        Me.Controls.Add(Me.txtPVelmj)
        Me.Controls.Add(Me.txtPVelj)
        Me.Controls.Add(Me.txtPVelkj)
        Me.Controls.Add(Me.txtPVelzj)
        Me.Controls.Add(Me.txtPVelkcal)
        Me.Controls.Add(Me.txtPVelbtu)
        Me.Controls.Add(Me.txtPVelkwh)
        Me.Controls.Add(Me.txtPVelhph)
        Me.Controls.Add(Me.txtPVelkgm)
        Me.Controls.Add(Me.txtPVelftlb)
        Me.Controls.Add(Me.txtPVellatm)
        Me.Controls.Add(Me.txtPVelftpdl)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.lblPVelmj)
        Me.Controls.Add(Me.lblPVelj)
        Me.Controls.Add(Me.lblPVelkj)
        Me.Controls.Add(Me.lblPowerzj)
        Me.Controls.Add(Me.lblPVelkcal)
        Me.Controls.Add(Me.lblPVelbtu)
        Me.Controls.Add(Me.lblPVelkwh)
        Me.Controls.Add(Me.lblPVelhph)
        Me.Controls.Add(Me.lblPVelkgm)
        Me.Controls.Add(Me.lblPVelftlb)
        Me.Controls.Add(Me.lblPVellatm)
        Me.Controls.Add(Me.lblPVelftpdl)
        Me.Controls.Add(Me.lblPVelinput)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmunitPVel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "能量[功]单位转换"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitPVel
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitPVel
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitPVel()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年8月1日16:43于青海省海西洲大柴旦红山参2井
	'能量（功）单位换算
	
	Dim mj As Single
	Dim j As Single
	Dim kj As Single
	Dim zj As Single
	Dim kcal As Single
	Dim kgm As Single
	Dim hph As Single
	Dim ftlb As Single
	Dim btu As Single
	Dim kwh As Single
	Dim latm As Single
	Dim ftpdl As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitPVel.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	
	
	Private Sub txtPVelbtu_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelbtu.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		btu = Val(txtPVelbtu.Text)
		
		mj = btu * 1055100#
		j = btu * 1055.056
		kj = btu * 1.0551
		zj = btu * 0.0011
		kcal = btu * 0.252
		kwh = btu * 0.00029307
		hph = btu * 0.00039301
		kgm = btu * 107.5858
		ftlb = btu * 778.1693
		latm = btu * 10.4126
		ftpdl = btu * 25037#
		
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		'txtPVelbtu.Text = btu
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	Private Sub txtPVelftlb_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelftlb.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ftlb = Val(txtPVelftlb.Text)
		
		mj = ftlb * 1355.818
		j = ftlb * 1.3558
		kj = ftlb * 0.0014
		zj = ftlb * 0.0000013558
		kcal = ftlb * 0.00032384
		btu = ftlb * 0.0013
		kwh = ftlb * 0.00000037662
		hph = ftlb * 0.00000050505
		kgm = ftlb * 0.1383
		latm = ftlb * 0.0134
		ftpdl = ftlb * 32.1741
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		'txtPVelftlb.Text = ftlb
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	
	Private Sub txtPVelftpdl_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelftpdl.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ftpdl = Val(txtPVelftpdl.Text)
		
		mj = ftpdl * 42.1401
		j = ftpdl * 0.0421
		kj = ftpdl * 0.00004214
		zj = ftpdl * 0.00000004214
		kcal = ftpdl * 0.000010065
		btu = ftpdl * 0.000039941
		kwh = ftpdl * 0.000000011706
		hph = ftpdl * 0.000000015697
		kgm = ftpdl * 0.0043
		ftlb = ftpdl * 0.0311
		latm = ftpdl * 0.0004
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		'txtPVelftpdl.Text = ftpdl
	End Sub
	
	Private Sub txtPVelhph_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelhph.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		hph = Val(txtPVelhph.Text)
		
		mj = hph * 2684500000#
		j = hph * 2684500#
		kj = hph * 2684.5196
		zj = hph * 2.6845
		kcal = hph * 641.1865
		btu = hph * 2544.4333
		kwh = hph * 0.7457
		kgm = hph * 273740#
		ftlb = hph * 1980000#
		latm = hph * 26494#
		ftpdl = hph * 63705000#
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		'txtPVelhph.Text = hph
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	Private Sub txtPVelj_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelj.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		j = Val(txtPVelj.Text)
		
		mj = j * 1000#
		kj = j * 0.001
		zj = j * 0.000001
		kcal = j * 0.00023885
		btu = j * 0.00094783
		kwh = j * 0.00000027778
		hph = j * 0.00000037251
		kgm = j * 0.102
		ftlb = j * 0.7376
		latm = j * 0.0099
		ftpdl = j * 23.7304
		
		
		txtPVelmj.Text = CStr(mj)
		'txtPVelj.Text = j
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	
	Private Sub txtPVelkcal_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelkcal.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kcal = Val(txtPVelkcal.Text)
		
		mj = kcal * 4186800#
		j = kcal * 4186.8
		kj = kcal * 4.1868
		zj = kcal * 0.0042
		btu = kcal * 3.9683
		kwh = kcal * 0.0012
		hph = kcal * 0.0016
		kgm = kcal * 426.9348
		ftlb = kcal * 3088.0251
		latm = kcal * 41.3205
		ftpdl = kcal * 99354#
		
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		'txtPVelkcal.Text = kcal
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	
	Private Sub txtPVelkgm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelkgm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgm = Val(txtPVelkgm.Text)
		
		mj = kgm * 9806.65
		j = kgm * 9.8066
		kj = kgm * 0.0098
		zj = kgm * 0.0000098066
		kcal = kgm * 0.0023
		btu = kgm * 0.0093
		kwh = kgm * 0.0000027241
		hph = kgm * 0.000003653
		ftlb = kgm * 7.233
		latm = kgm * 0.0968
		ftpdl = kgm * 232.7153
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		'txtPVelkgm.Text = kgm
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	Private Sub txtPVelkj_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelkj.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kj = Val(txtPVelkj.Text)
		
		mj = kj * 1000000#
		j = kj * 1000#
		zj = kj * 0.001
		kcal = kj * 0.2388
		btu = kj * 0.9478
		kwh = kj * 0.00027778
		hph = kj * 0.00037251
		kgm = kj * 101.9716
		ftlb = kj * 737.5621
		latm = kj * 9.8692
		ftpdl = kj * 23730#
		
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		'txtPVelkj.Text = kj
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	Private Sub txtPVelkwh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelkwh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kwh = Val(txtPVelkwh.Text)
		
		mj = kwh * 3600000000#
		j = kwh * 3600000#
		kj = kwh * 3600#
		zj = kwh * 3.6
		kcal = kwh * 859.8452
		btu = kwh * 3412.1412
		hph = kwh * 1.341
		kgm = kwh * 367100#
		ftlb = kwh * 2655200#
		latm = kwh * 35529#
		ftpdl = kwh * 85429000#
		
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		'txtPVelkwh.Text = kwh
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	
	Private Sub txtPVellatm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVellatm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		latm = Val(txtPVellatm.Text)
		
		mj = latm * 101330#
		j = latm * 101.325
		kj = latm * 0.1013
		zj = latm * 0.00010133
		kcal = latm * 0.0242
		btu = latm * 0.096
		kwh = latm * 0.000028146
		hph = latm * 0.000037744
		kgm = latm * 10.3323
		ftlb = latm * 74.7335
		ftpdl = latm * 2404.4788
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		'txtPVellatm.Text = latm
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	Private Sub txtPVelmj_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelmj.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mj = Val(txtPVelmj.Text)
		
		j = mj * 0.001
		kj = mj * 0.000001
		zj = mj * 0.000000001
		kcal = mj * 0.00000023885
		btu = mj * 0.00000094781
		kwh = mj * 0.00000000027778
		hph = mj * 0.00000000037251
		kgm = mj * 0.00010197
		ftlb = mj * 0.00073757
		latm = mj * 0.0000098692
		ftpdl = mj * 0.0237
		
		
		'txtPVelmj.Text = mj
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		txtPVelzj.Text = CStr(zj)
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
	
	Private Sub txtPVelzj_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPVelzj.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		zj = Val(txtPVelzj.Text)
		
		mj = zj * 1000000000#
		j = zj * 1000000#
		kj = zj * 1000#
		kcal = zj * 238.8459
		btu = zj * 947.817
		kwh = zj * 0.2778
		hph = zj * 0.3725
		kgm = zj * 101970#
		ftlb = zj * 737560#
		latm = zj * 9869.2327
		ftpdl = zj * 23730000#
		
		
		txtPVelmj.Text = CStr(mj)
		txtPVelj.Text = CStr(j)
		txtPVelkj.Text = CStr(kj)
		'txtPVelzj.Text = zj
		txtPVelkgm.Text = CStr(kgm)
		txtPVelkcal.Text = CStr(kcal)
		txtPVelftlb.Text = CStr(ftlb)
		txtPVelhph.Text = CStr(hph)
		txtPVelbtu.Text = CStr(btu)
		txtPVelkwh.Text = CStr(kwh)
		txtPVellatm.Text = CStr(latm)
		txtPVelftpdl.Text = CStr(ftpdl)
	End Sub
End Class