Option Strict Off
Option Explicit On
Friend Class frmunitsport
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtSportmpas As System.Windows.Forms.TextBox
	Public WithEvents txtSportpas As System.Windows.Forms.TextBox
	Public WithEvents txtSportcp As System.Windows.Forms.TextBox
	Public WithEvents txtSportp As System.Windows.Forms.TextBox
	Public WithEvents txtSportkgms As System.Windows.Forms.TextBox
	Public WithEvents txtSportlbfts As System.Windows.Forms.TextBox
	Public WithEvents txtSportdynscm2 As System.Windows.Forms.TextBox
	Public WithEvents txtSportlbsft2 As System.Windows.Forms.TextBox
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents lblSportmpas As System.Windows.Forms.Label
	Public WithEvents lblSportpas As System.Windows.Forms.Label
	Public WithEvents lblSportcp As System.Windows.Forms.Label
	Public WithEvents lblSportp As System.Windows.Forms.Label
	Public WithEvents lblSportkgms As System.Windows.Forms.Label
	Public WithEvents lblSportlbfts As System.Windows.Forms.Label
	Public WithEvents lblSportsdynscm2 As System.Windows.Forms.Label
	Public WithEvents lblSportlbsft2 As System.Windows.Forms.Label
	Public WithEvents lblSportinput As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblSportmpas = New System.Windows.Forms.Label
        Me.lblSportpas = New System.Windows.Forms.Label
        Me.lblSportcp = New System.Windows.Forms.Label
        Me.lblSportp = New System.Windows.Forms.Label
        Me.lblSportkgms = New System.Windows.Forms.Label
        Me.lblSportlbfts = New System.Windows.Forms.Label
        Me.lblSportsdynscm2 = New System.Windows.Forms.Label
        Me.lblSportlbsft2 = New System.Windows.Forms.Label
        Me.txtSportmpas = New System.Windows.Forms.TextBox
        Me.txtSportpas = New System.Windows.Forms.TextBox
        Me.txtSportcp = New System.Windows.Forms.TextBox
        Me.txtSportp = New System.Windows.Forms.TextBox
        Me.txtSportkgms = New System.Windows.Forms.TextBox
        Me.txtSportlbfts = New System.Windows.Forms.TextBox
        Me.txtSportdynscm2 = New System.Windows.Forms.TextBox
        Me.txtSportlbsft2 = New System.Windows.Forms.TextBox
        Me.cmdquit = New System.Windows.Forms.Button
        Me.cmdClear = New System.Windows.Forms.Button
        Me.lblSportinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblSportmpas
        '
        Me.lblSportmpas.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportmpas.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportmpas.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportmpas.Location = New System.Drawing.Point(106, 34)
        Me.lblSportmpas.Name = "lblSportmpas"
        Me.lblSportmpas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportmpas.Size = New System.Drawing.Size(68, 19)
        Me.lblSportmpas.TabIndex = 18
        Me.lblSportmpas.Text = "mPa.s"
        Me.ToolTip1.SetToolTip(Me.lblSportmpas, "毫帕秒")
        '
        'lblSportpas
        '
        Me.lblSportpas.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportpas.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportpas.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportpas.Location = New System.Drawing.Point(106, 69)
        Me.lblSportpas.Name = "lblSportpas"
        Me.lblSportpas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportpas.Size = New System.Drawing.Size(68, 18)
        Me.lblSportpas.TabIndex = 17
        Me.lblSportpas.Text = "pa.s"
        Me.ToolTip1.SetToolTip(Me.lblSportpas, "帕秒")
        '
        'lblSportcp
        '
        Me.lblSportcp.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportcp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportcp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportcp.Location = New System.Drawing.Point(106, 103)
        Me.lblSportcp.Name = "lblSportcp"
        Me.lblSportcp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportcp.Size = New System.Drawing.Size(68, 19)
        Me.lblSportcp.TabIndex = 16
        Me.lblSportcp.Text = "cP"
        Me.ToolTip1.SetToolTip(Me.lblSportcp, "厘泊")
        '
        'lblSportp
        '
        Me.lblSportp.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportp.Location = New System.Drawing.Point(106, 138)
        Me.lblSportp.Name = "lblSportp"
        Me.lblSportp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportp.Size = New System.Drawing.Size(68, 18)
        Me.lblSportp.TabIndex = 15
        Me.lblSportp.Text = "P"
        Me.ToolTip1.SetToolTip(Me.lblSportp, "泊")
        '
        'lblSportkgms
        '
        Me.lblSportkgms.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportkgms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportkgms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportkgms.Location = New System.Drawing.Point(269, 34)
        Me.lblSportkgms.Name = "lblSportkgms"
        Me.lblSportkgms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportkgms.Size = New System.Drawing.Size(68, 19)
        Me.lblSportkgms.TabIndex = 14
        Me.lblSportkgms.Text = "kg/[m.s]"
        Me.ToolTip1.SetToolTip(Me.lblSportkgms, "千克每米秒")
        '
        'lblSportlbfts
        '
        Me.lblSportlbfts.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportlbfts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportlbfts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportlbfts.Location = New System.Drawing.Point(269, 69)
        Me.lblSportlbfts.Name = "lblSportlbfts"
        Me.lblSportlbfts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportlbfts.Size = New System.Drawing.Size(68, 18)
        Me.lblSportlbfts.TabIndex = 13
        Me.lblSportlbfts.Text = "lb/[ft.s]"
        Me.ToolTip1.SetToolTip(Me.lblSportlbfts, "磅每英尺秒")
        '
        'lblSportsdynscm2
        '
        Me.lblSportsdynscm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportsdynscm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportsdynscm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportsdynscm2.Location = New System.Drawing.Point(269, 103)
        Me.lblSportsdynscm2.Name = "lblSportsdynscm2"
        Me.lblSportsdynscm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportsdynscm2.Size = New System.Drawing.Size(78, 19)
        Me.lblSportsdynscm2.TabIndex = 12
        Me.lblSportsdynscm2.Text = "dyn.s/cm^2"
        Me.ToolTip1.SetToolTip(Me.lblSportsdynscm2, "达因秒每平方厘米")
        '
        'lblSportlbsft2
        '
        Me.lblSportlbsft2.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportlbsft2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportlbsft2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportlbsft2.Location = New System.Drawing.Point(269, 138)
        Me.lblSportlbsft2.Name = "lblSportlbsft2"
        Me.lblSportlbsft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportlbsft2.Size = New System.Drawing.Size(68, 18)
        Me.lblSportlbsft2.TabIndex = 11
        Me.lblSportlbsft2.Text = "lb.s/ft^2"
        Me.ToolTip1.SetToolTip(Me.lblSportlbsft2, "磅秒每平方英尺")
        '
        'txtSportmpas
        '
        Me.txtSportmpas.AcceptsReturn = True
        Me.txtSportmpas.AutoSize = False
        Me.txtSportmpas.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportmpas.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportmpas.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportmpas.Location = New System.Drawing.Point(19, 34)
        Me.txtSportmpas.MaxLength = 0
        Me.txtSportmpas.Name = "txtSportmpas"
        Me.txtSportmpas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportmpas.Size = New System.Drawing.Size(78, 20)
        Me.txtSportmpas.TabIndex = 10
        Me.txtSportmpas.Text = ""
        '
        'txtSportpas
        '
        Me.txtSportpas.AcceptsReturn = True
        Me.txtSportpas.AutoSize = False
        Me.txtSportpas.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportpas.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportpas.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportpas.Location = New System.Drawing.Point(19, 69)
        Me.txtSportpas.MaxLength = 0
        Me.txtSportpas.Name = "txtSportpas"
        Me.txtSportpas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportpas.Size = New System.Drawing.Size(78, 19)
        Me.txtSportpas.TabIndex = 9
        Me.txtSportpas.Text = ""
        '
        'txtSportcp
        '
        Me.txtSportcp.AcceptsReturn = True
        Me.txtSportcp.AutoSize = False
        Me.txtSportcp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportcp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportcp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportcp.Location = New System.Drawing.Point(19, 103)
        Me.txtSportcp.MaxLength = 0
        Me.txtSportcp.Name = "txtSportcp"
        Me.txtSportcp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportcp.Size = New System.Drawing.Size(78, 20)
        Me.txtSportcp.TabIndex = 8
        Me.txtSportcp.Text = ""
        '
        'txtSportp
        '
        Me.txtSportp.AcceptsReturn = True
        Me.txtSportp.AutoSize = False
        Me.txtSportp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportp.Location = New System.Drawing.Point(19, 138)
        Me.txtSportp.MaxLength = 0
        Me.txtSportp.Name = "txtSportp"
        Me.txtSportp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportp.Size = New System.Drawing.Size(78, 19)
        Me.txtSportp.TabIndex = 7
        Me.txtSportp.Text = ""
        '
        'txtSportkgms
        '
        Me.txtSportkgms.AcceptsReturn = True
        Me.txtSportkgms.AutoSize = False
        Me.txtSportkgms.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportkgms.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportkgms.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportkgms.Location = New System.Drawing.Point(182, 34)
        Me.txtSportkgms.MaxLength = 0
        Me.txtSportkgms.Name = "txtSportkgms"
        Me.txtSportkgms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportkgms.Size = New System.Drawing.Size(78, 20)
        Me.txtSportkgms.TabIndex = 6
        Me.txtSportkgms.Text = ""
        '
        'txtSportlbfts
        '
        Me.txtSportlbfts.AcceptsReturn = True
        Me.txtSportlbfts.AutoSize = False
        Me.txtSportlbfts.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportlbfts.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportlbfts.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportlbfts.Location = New System.Drawing.Point(182, 69)
        Me.txtSportlbfts.MaxLength = 0
        Me.txtSportlbfts.Name = "txtSportlbfts"
        Me.txtSportlbfts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportlbfts.Size = New System.Drawing.Size(78, 19)
        Me.txtSportlbfts.TabIndex = 5
        Me.txtSportlbfts.Text = ""
        '
        'txtSportdynscm2
        '
        Me.txtSportdynscm2.AcceptsReturn = True
        Me.txtSportdynscm2.AutoSize = False
        Me.txtSportdynscm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportdynscm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportdynscm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportdynscm2.Location = New System.Drawing.Point(182, 103)
        Me.txtSportdynscm2.MaxLength = 0
        Me.txtSportdynscm2.Name = "txtSportdynscm2"
        Me.txtSportdynscm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportdynscm2.Size = New System.Drawing.Size(78, 20)
        Me.txtSportdynscm2.TabIndex = 4
        Me.txtSportdynscm2.Text = ""
        '
        'txtSportlbsft2
        '
        Me.txtSportlbsft2.AcceptsReturn = True
        Me.txtSportlbsft2.AutoSize = False
        Me.txtSportlbsft2.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportlbsft2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSportlbsft2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSportlbsft2.Location = New System.Drawing.Point(182, 138)
        Me.txtSportlbsft2.MaxLength = 0
        Me.txtSportlbsft2.Name = "txtSportlbsft2"
        Me.txtSportlbsft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSportlbsft2.Size = New System.Drawing.Size(78, 19)
        Me.txtSportlbsft2.TabIndex = 3
        Me.txtSportlbsft2.Text = ""
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(58, 190)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 26)
        Me.cmdquit.TabIndex = 2
        Me.cmdquit.Text = "退出"
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(221, 190)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(87, 26)
        Me.cmdClear.TabIndex = 1
        Me.cmdClear.Text = "清除"
        '
        'lblSportinput
        '
        Me.lblSportinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblSportinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSportinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblSportinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSportinput.Location = New System.Drawing.Point(10, 9)
        Me.lblSportinput.Name = "lblSportinput"
        Me.lblSportinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSportinput.Size = New System.Drawing.Size(270, 18)
        Me.lblSportinput.TabIndex = 0
        Me.lblSportinput.Text = "请输入[动力]粘度单位换算数据："
        '
        'frmunitsport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(358, 234)
        Me.Controls.Add(Me.txtSportmpas)
        Me.Controls.Add(Me.txtSportpas)
        Me.Controls.Add(Me.txtSportcp)
        Me.Controls.Add(Me.txtSportp)
        Me.Controls.Add(Me.txtSportkgms)
        Me.Controls.Add(Me.txtSportlbfts)
        Me.Controls.Add(Me.txtSportdynscm2)
        Me.Controls.Add(Me.txtSportlbsft2)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.lblSportmpas)
        Me.Controls.Add(Me.lblSportpas)
        Me.Controls.Add(Me.lblSportcp)
        Me.Controls.Add(Me.lblSportp)
        Me.Controls.Add(Me.lblSportkgms)
        Me.Controls.Add(Me.lblSportlbfts)
        Me.Controls.Add(Me.lblSportsdynscm2)
        Me.Controls.Add(Me.lblSportlbsft2)
        Me.Controls.Add(Me.lblSportinput)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.Name = "frmunitsport"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "[动力]粘度单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitsport
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitsport
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitsport()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月31日22:29于青海省海西洲大柴旦红山参2井
	'运动粘度单位换算
	
	Dim mpas As Single
	Dim pas As Single
	Dim cp As Single
	Dim p As Single
	Dim kgms As Single
	Dim lbfts As Single
	Dim dynscm2 As Single
	Dim lbsft2 As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitsport.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtSportmpas_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportmpas.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mpas = Val(txtSportmpas.Text)
		
		pas = mpas * 0.001
		cp = mpas * 1#
		p = mpas * 0.01
		kgms = mpas * 0.001
		lbfts = mpas * 0.000672
		dynscm2 = mpas * 0.01
		lbsft2 = mpas * 0.000020885
		
		'txtSportmpas.Text = mpas
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
		
	End Sub
	
	
	Private Sub txtSportcp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportcp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		cp = Val(txtSportcp.Text)
		
		mpas = cp * 1#
		pas = cp * 0.001
		p = cp * 0.01
		kgms = cp * 0.001
		lbfts = cp * 0.000672
		dynscm2 = cp * 0.01
		lbsft2 = cp * 0.000020885
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		'txtSportcp.Text = cp
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
	
	Private Sub txtSportdynscm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportdynscm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dynscm2 = Val(txtSportdynscm2.Text)
		
		mpas = dynscm2 * 100#
		pas = dynscm2 * 0.1
		cp = dynscm2 * 100#
		p = dynscm2 * 1#
		kgms = dynscm2 * 0.1
		lbfts = dynscm2 * 0.0672
		lbsft2 = dynscm2 * 0.0021
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		'txtSportdynscm2.Text = dynscm2
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
	
	Private Sub txtSportkgms_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportkgms.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgms = Val(txtSportkgms.Text)
		
		mpas = kgms * 1000#
		pas = kgms * 1#
		cp = kgms * 1000#
		p = kgms * 10#
		lbfts = kgms * 0.672
		dynscm2 = kgms * 10#
		lbsft2 = kgms * 0.0209
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		'txtSportkgms.Text = kgms
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
	
	
	
	Private Sub txtSportlbfts_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportlbfts.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbfts = Val(txtSportlbfts.Text)
		
		mpas = lbfts * 1488.1639
		pas = lbfts * 1.4882
		cp = lbfts * 1488.1639
		p = lbfts * 14.8816
		kgms = lbfts * 1.4882
		dynscm2 = lbfts * 14.8816
		lbsft2 = lbfts * 0.0311
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		'txtSportlbfts.Text = lbfts
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
	
	
	Private Sub txtSportlbsft2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportlbsft2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbsft2 = Val(txtSportlbsft2.Text)
		
		mpas = lbsft2 * 47880#
		pas = lbsft2 * 47.8803
		cp = lbsft2 * 47880#
		p = lbsft2 * 478.8026
		kgms = lbsft2 * 47.8803
		lbfts = lbsft2 * 32.174
		dynscm2 = lbsft2 * 478.8026
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		'txtSportlbsft2.Text = lbsft2
	End Sub
	
	Private Sub txtSportp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		p = Val(txtSportp.Text)
		
		mpas = p * 100#
		pas = p * 0.1
		cp = p * 100#
		kgms = p * 0.1
		lbfts = p * 0.0672
		dynscm2 = p * 1#
		lbsft2 = p * 0.0021
		
		txtSportmpas.Text = CStr(mpas)
		txtSportpas.Text = CStr(pas)
		txtSportcp.Text = CStr(cp)
		'txtSportp.Text = p
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
	
	Private Sub txtSportpas_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSportpas.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		pas = Val(txtSportpas.Text)
		
		mpas = pas * 1000#
		cp = pas * 1000#
		p = pas * 10#
		kgms = pas * 1#
		lbfts = pas * 0.672
		dynscm2 = pas * 10#
		lbsft2 = pas * 0.0209
		
		txtSportmpas.Text = CStr(mpas)
		'txtSportpas.Text = pas
		txtSportcp.Text = CStr(cp)
		txtSportp.Text = CStr(p)
		txtSportkgms.Text = CStr(kgms)
		txtSportlbfts.Text = CStr(lbfts)
		txtSportdynscm2.Text = CStr(dynscm2)
		txtSportlbsft2.Text = CStr(lbsft2)
	End Sub
End Class