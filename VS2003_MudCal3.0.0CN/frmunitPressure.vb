Option Strict Off
Option Explicit On
Friend Class frmunitPressure
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents txtPressurelbft2 As System.Windows.Forms.TextBox
	Public WithEvents txtPressurelbin2 As System.Windows.Forms.TextBox
	Public WithEvents txtPressureinh20 As System.Windows.Forms.TextBox
	Public WithEvents txtPressureinhg As System.Windows.Forms.TextBox
	Public WithEvents txtPressuremmh20 As System.Windows.Forms.TextBox
	Public WithEvents txtPressuremmhg As System.Windows.Forms.TextBox
	Public WithEvents txtPressurebar As System.Windows.Forms.TextBox
	Public WithEvents txtPressuredyncm2 As System.Windows.Forms.TextBox
	Public WithEvents txtPressureatm As System.Windows.Forms.TextBox
	Public WithEvents txtPressureat As System.Windows.Forms.TextBox
	Public WithEvents txtPressurekgcm2 As System.Windows.Forms.TextBox
	Public WithEvents txtPressurepa As System.Windows.Forms.TextBox
	Public WithEvents txtPressurekpa As System.Windows.Forms.TextBox
	Public WithEvents txtPressurempa As System.Windows.Forms.TextBox
	Public WithEvents lblPressureinput As System.Windows.Forms.Label
	Public WithEvents lblPressurelbft2 As System.Windows.Forms.Label
	Public WithEvents lblPressurelbin2 As System.Windows.Forms.Label
	Public WithEvents lblPressureinh20 As System.Windows.Forms.Label
	Public WithEvents lblPressureinhg As System.Windows.Forms.Label
	Public WithEvents lblPressuremmh20 As System.Windows.Forms.Label
	Public WithEvents lblPressuremmhg As System.Windows.Forms.Label
	Public WithEvents lblPressurebar As System.Windows.Forms.Label
	Public WithEvents lblPressuredyncm2 As System.Windows.Forms.Label
	Public WithEvents lblPressureatm As System.Windows.Forms.Label
	Public WithEvents lblPressureat As System.Windows.Forms.Label
	Public WithEvents lblPressurekgcm2 As System.Windows.Forms.Label
	Public WithEvents lblPressurepa As System.Windows.Forms.Label
	Public WithEvents lblPressurekpa As System.Windows.Forms.Label
	Public WithEvents lblPressurempa As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblPressurelbft2 = New System.Windows.Forms.Label
        Me.lblPressurelbin2 = New System.Windows.Forms.Label
        Me.lblPressureinh20 = New System.Windows.Forms.Label
        Me.lblPressureinhg = New System.Windows.Forms.Label
        Me.lblPressuremmh20 = New System.Windows.Forms.Label
        Me.lblPressuremmhg = New System.Windows.Forms.Label
        Me.lblPressurebar = New System.Windows.Forms.Label
        Me.lblPressuredyncm2 = New System.Windows.Forms.Label
        Me.lblPressureatm = New System.Windows.Forms.Label
        Me.lblPressureat = New System.Windows.Forms.Label
        Me.lblPressurekgcm2 = New System.Windows.Forms.Label
        Me.lblPressurepa = New System.Windows.Forms.Label
        Me.lblPressurekpa = New System.Windows.Forms.Label
        Me.lblPressurempa = New System.Windows.Forms.Label
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtPressurelbft2 = New System.Windows.Forms.TextBox
        Me.txtPressurelbin2 = New System.Windows.Forms.TextBox
        Me.txtPressureinh20 = New System.Windows.Forms.TextBox
        Me.txtPressureinhg = New System.Windows.Forms.TextBox
        Me.txtPressuremmh20 = New System.Windows.Forms.TextBox
        Me.txtPressuremmhg = New System.Windows.Forms.TextBox
        Me.txtPressurebar = New System.Windows.Forms.TextBox
        Me.txtPressuredyncm2 = New System.Windows.Forms.TextBox
        Me.txtPressureatm = New System.Windows.Forms.TextBox
        Me.txtPressureat = New System.Windows.Forms.TextBox
        Me.txtPressurekgcm2 = New System.Windows.Forms.TextBox
        Me.txtPressurepa = New System.Windows.Forms.TextBox
        Me.txtPressurekpa = New System.Windows.Forms.TextBox
        Me.txtPressurempa = New System.Windows.Forms.TextBox
        Me.lblPressureinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblPressurelbft2
        '
        Me.lblPressurelbft2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurelbft2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurelbft2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurelbft2.Location = New System.Drawing.Point(307, 241)
        Me.lblPressurelbft2.Name = "lblPressurelbft2"
        Me.lblPressurelbft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurelbft2.Size = New System.Drawing.Size(69, 19)
        Me.lblPressurelbft2.TabIndex = 27
        Me.lblPressurelbft2.Text = "lb/ft^2"
        Me.ToolTip1.SetToolTip(Me.lblPressurelbft2, "磅每平方英尺")
        '
        'lblPressurelbin2
        '
        Me.lblPressurelbin2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurelbin2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurelbin2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurelbin2.Location = New System.Drawing.Point(307, 207)
        Me.lblPressurelbin2.Name = "lblPressurelbin2"
        Me.lblPressurelbin2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurelbin2.Size = New System.Drawing.Size(69, 18)
        Me.lblPressurelbin2.TabIndex = 26
        Me.lblPressurelbin2.Text = "lb/in^2"
        Me.ToolTip1.SetToolTip(Me.lblPressurelbin2, "磅每平方英寸")
        '
        'lblPressureinh20
        '
        Me.lblPressureinh20.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressureinh20.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressureinh20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressureinh20.Location = New System.Drawing.Point(307, 172)
        Me.lblPressureinh20.Name = "lblPressureinh20"
        Me.lblPressureinh20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressureinh20.Size = New System.Drawing.Size(69, 19)
        Me.lblPressureinh20.TabIndex = 25
        Me.lblPressureinh20.Text = "inH20"
        Me.ToolTip1.SetToolTip(Me.lblPressureinh20, "英寸水柱")
        '
        'lblPressureinhg
        '
        Me.lblPressureinhg.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressureinhg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressureinhg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressureinhg.Location = New System.Drawing.Point(307, 138)
        Me.lblPressureinhg.Name = "lblPressureinhg"
        Me.lblPressureinhg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressureinhg.Size = New System.Drawing.Size(69, 18)
        Me.lblPressureinhg.TabIndex = 24
        Me.lblPressureinhg.Text = "inHg"
        Me.ToolTip1.SetToolTip(Me.lblPressureinhg, "英寸汞柱")
        '
        'lblPressuremmh20
        '
        Me.lblPressuremmh20.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressuremmh20.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressuremmh20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressuremmh20.Location = New System.Drawing.Point(307, 103)
        Me.lblPressuremmh20.Name = "lblPressuremmh20"
        Me.lblPressuremmh20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressuremmh20.Size = New System.Drawing.Size(69, 19)
        Me.lblPressuremmh20.TabIndex = 23
        Me.lblPressuremmh20.Text = "mmH20"
        Me.ToolTip1.SetToolTip(Me.lblPressuremmh20, "毫米水柱")
        '
        'lblPressuremmhg
        '
        Me.lblPressuremmhg.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressuremmhg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressuremmhg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressuremmhg.Location = New System.Drawing.Point(307, 69)
        Me.lblPressuremmhg.Name = "lblPressuremmhg"
        Me.lblPressuremmhg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressuremmhg.Size = New System.Drawing.Size(69, 18)
        Me.lblPressuremmhg.TabIndex = 22
        Me.lblPressuremmhg.Text = "mmHg"
        Me.ToolTip1.SetToolTip(Me.lblPressuremmhg, "毫米汞柱")
        '
        'lblPressurebar
        '
        Me.lblPressurebar.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurebar.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurebar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurebar.Location = New System.Drawing.Point(307, 34)
        Me.lblPressurebar.Name = "lblPressurebar"
        Me.lblPressurebar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurebar.Size = New System.Drawing.Size(69, 19)
        Me.lblPressurebar.TabIndex = 21
        Me.lblPressurebar.Text = "bar"
        Me.ToolTip1.SetToolTip(Me.lblPressurebar, "巴")
        '
        'lblPressuredyncm2
        '
        Me.lblPressuredyncm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressuredyncm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressuredyncm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressuredyncm2.Location = New System.Drawing.Point(115, 241)
        Me.lblPressuredyncm2.Name = "lblPressuredyncm2"
        Me.lblPressuredyncm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressuredyncm2.Size = New System.Drawing.Size(69, 19)
        Me.lblPressuredyncm2.TabIndex = 13
        Me.lblPressuredyncm2.Text = "dyn/cm^2"
        Me.ToolTip1.SetToolTip(Me.lblPressuredyncm2, "达因每平方厘米")
        '
        'lblPressureatm
        '
        Me.lblPressureatm.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressureatm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressureatm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressureatm.Location = New System.Drawing.Point(115, 207)
        Me.lblPressureatm.Name = "lblPressureatm"
        Me.lblPressureatm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressureatm.Size = New System.Drawing.Size(69, 18)
        Me.lblPressureatm.TabIndex = 12
        Me.lblPressureatm.Text = "atm"
        Me.ToolTip1.SetToolTip(Me.lblPressureatm, "(标准)大气压")
        '
        'lblPressureat
        '
        Me.lblPressureat.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressureat.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressureat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressureat.Location = New System.Drawing.Point(115, 172)
        Me.lblPressureat.Name = "lblPressureat"
        Me.lblPressureat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressureat.Size = New System.Drawing.Size(69, 19)
        Me.lblPressureat.TabIndex = 11
        Me.lblPressureat.Text = "at"
        Me.ToolTip1.SetToolTip(Me.lblPressureat, "工程大气压")
        '
        'lblPressurekgcm2
        '
        Me.lblPressurekgcm2.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurekgcm2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurekgcm2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurekgcm2.Location = New System.Drawing.Point(115, 138)
        Me.lblPressurekgcm2.Name = "lblPressurekgcm2"
        Me.lblPressurekgcm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurekgcm2.Size = New System.Drawing.Size(69, 18)
        Me.lblPressurekgcm2.TabIndex = 10
        Me.lblPressurekgcm2.Text = "kg/cm^2"
        Me.ToolTip1.SetToolTip(Me.lblPressurekgcm2, "千克每平方厘米")
        '
        'lblPressurepa
        '
        Me.lblPressurepa.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurepa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurepa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurepa.Location = New System.Drawing.Point(115, 103)
        Me.lblPressurepa.Name = "lblPressurepa"
        Me.lblPressurepa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurepa.Size = New System.Drawing.Size(69, 19)
        Me.lblPressurepa.TabIndex = 9
        Me.lblPressurepa.Text = "Pa"
        Me.ToolTip1.SetToolTip(Me.lblPressurepa, "帕")
        '
        'lblPressurekpa
        '
        Me.lblPressurekpa.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurekpa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurekpa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurekpa.Location = New System.Drawing.Point(115, 69)
        Me.lblPressurekpa.Name = "lblPressurekpa"
        Me.lblPressurekpa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurekpa.Size = New System.Drawing.Size(69, 18)
        Me.lblPressurekpa.TabIndex = 8
        Me.lblPressurekpa.Text = "kPa"
        Me.ToolTip1.SetToolTip(Me.lblPressurekpa, "千帕")
        '
        'lblPressurempa
        '
        Me.lblPressurempa.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressurempa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressurempa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressurempa.Location = New System.Drawing.Point(115, 34)
        Me.lblPressurempa.Name = "lblPressurempa"
        Me.lblPressurempa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressurempa.Size = New System.Drawing.Size(69, 19)
        Me.lblPressurempa.TabIndex = 7
        Me.lblPressurempa.Text = "MPa"
        Me.ToolTip1.SetToolTip(Me.lblPressurempa, "兆帕")
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(240, 276)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(88, 27)
        Me.cmdClear.TabIndex = 29
        Me.cmdClear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(77, 276)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 28
        Me.cmdquit.Text = "退出"
        '
        'txtPressurelbft2
        '
        Me.txtPressurelbft2.AcceptsReturn = True
        Me.txtPressurelbft2.AutoSize = False
        Me.txtPressurelbft2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurelbft2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurelbft2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurelbft2.Location = New System.Drawing.Point(211, 241)
        Me.txtPressurelbft2.MaxLength = 0
        Me.txtPressurelbft2.Name = "txtPressurelbft2"
        Me.txtPressurelbft2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurelbft2.Size = New System.Drawing.Size(88, 20)
        Me.txtPressurelbft2.TabIndex = 20
        Me.txtPressurelbft2.Text = ""
        '
        'txtPressurelbin2
        '
        Me.txtPressurelbin2.AcceptsReturn = True
        Me.txtPressurelbin2.AutoSize = False
        Me.txtPressurelbin2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurelbin2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurelbin2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurelbin2.Location = New System.Drawing.Point(211, 207)
        Me.txtPressurelbin2.MaxLength = 0
        Me.txtPressurelbin2.Name = "txtPressurelbin2"
        Me.txtPressurelbin2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurelbin2.Size = New System.Drawing.Size(88, 19)
        Me.txtPressurelbin2.TabIndex = 19
        Me.txtPressurelbin2.Text = ""
        '
        'txtPressureinh20
        '
        Me.txtPressureinh20.AcceptsReturn = True
        Me.txtPressureinh20.AutoSize = False
        Me.txtPressureinh20.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressureinh20.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressureinh20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressureinh20.Location = New System.Drawing.Point(211, 172)
        Me.txtPressureinh20.MaxLength = 0
        Me.txtPressureinh20.Name = "txtPressureinh20"
        Me.txtPressureinh20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressureinh20.Size = New System.Drawing.Size(88, 20)
        Me.txtPressureinh20.TabIndex = 18
        Me.txtPressureinh20.Text = ""
        '
        'txtPressureinhg
        '
        Me.txtPressureinhg.AcceptsReturn = True
        Me.txtPressureinhg.AutoSize = False
        Me.txtPressureinhg.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressureinhg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressureinhg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressureinhg.Location = New System.Drawing.Point(211, 138)
        Me.txtPressureinhg.MaxLength = 0
        Me.txtPressureinhg.Name = "txtPressureinhg"
        Me.txtPressureinhg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressureinhg.Size = New System.Drawing.Size(88, 19)
        Me.txtPressureinhg.TabIndex = 17
        Me.txtPressureinhg.Text = ""
        '
        'txtPressuremmh20
        '
        Me.txtPressuremmh20.AcceptsReturn = True
        Me.txtPressuremmh20.AutoSize = False
        Me.txtPressuremmh20.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressuremmh20.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressuremmh20.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressuremmh20.Location = New System.Drawing.Point(211, 103)
        Me.txtPressuremmh20.MaxLength = 0
        Me.txtPressuremmh20.Name = "txtPressuremmh20"
        Me.txtPressuremmh20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressuremmh20.Size = New System.Drawing.Size(88, 20)
        Me.txtPressuremmh20.TabIndex = 16
        Me.txtPressuremmh20.Text = ""
        '
        'txtPressuremmhg
        '
        Me.txtPressuremmhg.AcceptsReturn = True
        Me.txtPressuremmhg.AutoSize = False
        Me.txtPressuremmhg.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressuremmhg.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressuremmhg.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressuremmhg.Location = New System.Drawing.Point(211, 69)
        Me.txtPressuremmhg.MaxLength = 0
        Me.txtPressuremmhg.Name = "txtPressuremmhg"
        Me.txtPressuremmhg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressuremmhg.Size = New System.Drawing.Size(88, 19)
        Me.txtPressuremmhg.TabIndex = 15
        Me.txtPressuremmhg.Text = ""
        '
        'txtPressurebar
        '
        Me.txtPressurebar.AcceptsReturn = True
        Me.txtPressurebar.AutoSize = False
        Me.txtPressurebar.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurebar.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurebar.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurebar.Location = New System.Drawing.Point(211, 34)
        Me.txtPressurebar.MaxLength = 0
        Me.txtPressurebar.Name = "txtPressurebar"
        Me.txtPressurebar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurebar.Size = New System.Drawing.Size(88, 20)
        Me.txtPressurebar.TabIndex = 14
        Me.txtPressurebar.Text = ""
        '
        'txtPressuredyncm2
        '
        Me.txtPressuredyncm2.AcceptsReturn = True
        Me.txtPressuredyncm2.AutoSize = False
        Me.txtPressuredyncm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressuredyncm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressuredyncm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressuredyncm2.Location = New System.Drawing.Point(19, 241)
        Me.txtPressuredyncm2.MaxLength = 0
        Me.txtPressuredyncm2.Name = "txtPressuredyncm2"
        Me.txtPressuredyncm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressuredyncm2.Size = New System.Drawing.Size(88, 20)
        Me.txtPressuredyncm2.TabIndex = 6
        Me.txtPressuredyncm2.Text = ""
        '
        'txtPressureatm
        '
        Me.txtPressureatm.AcceptsReturn = True
        Me.txtPressureatm.AutoSize = False
        Me.txtPressureatm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressureatm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressureatm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressureatm.Location = New System.Drawing.Point(19, 207)
        Me.txtPressureatm.MaxLength = 0
        Me.txtPressureatm.Name = "txtPressureatm"
        Me.txtPressureatm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressureatm.Size = New System.Drawing.Size(88, 19)
        Me.txtPressureatm.TabIndex = 5
        Me.txtPressureatm.Text = ""
        '
        'txtPressureat
        '
        Me.txtPressureat.AcceptsReturn = True
        Me.txtPressureat.AutoSize = False
        Me.txtPressureat.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressureat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressureat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressureat.Location = New System.Drawing.Point(19, 172)
        Me.txtPressureat.MaxLength = 0
        Me.txtPressureat.Name = "txtPressureat"
        Me.txtPressureat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressureat.Size = New System.Drawing.Size(88, 20)
        Me.txtPressureat.TabIndex = 4
        Me.txtPressureat.Text = ""
        '
        'txtPressurekgcm2
        '
        Me.txtPressurekgcm2.AcceptsReturn = True
        Me.txtPressurekgcm2.AutoSize = False
        Me.txtPressurekgcm2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurekgcm2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurekgcm2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurekgcm2.Location = New System.Drawing.Point(19, 138)
        Me.txtPressurekgcm2.MaxLength = 0
        Me.txtPressurekgcm2.Name = "txtPressurekgcm2"
        Me.txtPressurekgcm2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurekgcm2.Size = New System.Drawing.Size(88, 19)
        Me.txtPressurekgcm2.TabIndex = 3
        Me.txtPressurekgcm2.Text = ""
        '
        'txtPressurepa
        '
        Me.txtPressurepa.AcceptsReturn = True
        Me.txtPressurepa.AutoSize = False
        Me.txtPressurepa.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurepa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurepa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurepa.Location = New System.Drawing.Point(19, 103)
        Me.txtPressurepa.MaxLength = 0
        Me.txtPressurepa.Name = "txtPressurepa"
        Me.txtPressurepa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurepa.Size = New System.Drawing.Size(88, 20)
        Me.txtPressurepa.TabIndex = 2
        Me.txtPressurepa.Text = ""
        '
        'txtPressurekpa
        '
        Me.txtPressurekpa.AcceptsReturn = True
        Me.txtPressurekpa.AutoSize = False
        Me.txtPressurekpa.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurekpa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurekpa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurekpa.Location = New System.Drawing.Point(19, 69)
        Me.txtPressurekpa.MaxLength = 0
        Me.txtPressurekpa.Name = "txtPressurekpa"
        Me.txtPressurekpa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurekpa.Size = New System.Drawing.Size(88, 19)
        Me.txtPressurekpa.TabIndex = 1
        Me.txtPressurekpa.Text = ""
        '
        'txtPressurempa
        '
        Me.txtPressurempa.AcceptsReturn = True
        Me.txtPressurempa.AutoSize = False
        Me.txtPressurempa.BackColor = System.Drawing.SystemColors.Window
        Me.txtPressurempa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPressurempa.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPressurempa.Location = New System.Drawing.Point(19, 34)
        Me.txtPressurempa.MaxLength = 0
        Me.txtPressurempa.Name = "txtPressurempa"
        Me.txtPressurempa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPressurempa.Size = New System.Drawing.Size(88, 20)
        Me.txtPressurempa.TabIndex = 0
        Me.txtPressurempa.Text = ""
        '
        'lblPressureinput
        '
        Me.lblPressureinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblPressureinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPressureinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblPressureinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPressureinput.Location = New System.Drawing.Point(10, 9)
        Me.lblPressureinput.Name = "lblPressureinput"
        Me.lblPressureinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPressureinput.Size = New System.Drawing.Size(260, 18)
        Me.lblPressureinput.TabIndex = 30
        Me.lblPressureinput.Text = "输入压力单位换算数据："
        '
        'frmunitPressure
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(390, 327)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.txtPressurelbft2)
        Me.Controls.Add(Me.txtPressurelbin2)
        Me.Controls.Add(Me.txtPressureinh20)
        Me.Controls.Add(Me.txtPressureinhg)
        Me.Controls.Add(Me.txtPressuremmh20)
        Me.Controls.Add(Me.txtPressuremmhg)
        Me.Controls.Add(Me.txtPressurebar)
        Me.Controls.Add(Me.txtPressuredyncm2)
        Me.Controls.Add(Me.txtPressureatm)
        Me.Controls.Add(Me.txtPressureat)
        Me.Controls.Add(Me.txtPressurekgcm2)
        Me.Controls.Add(Me.txtPressurepa)
        Me.Controls.Add(Me.txtPressurekpa)
        Me.Controls.Add(Me.txtPressurempa)
        Me.Controls.Add(Me.lblPressureinput)
        Me.Controls.Add(Me.lblPressurelbft2)
        Me.Controls.Add(Me.lblPressurelbin2)
        Me.Controls.Add(Me.lblPressureinh20)
        Me.Controls.Add(Me.lblPressureinhg)
        Me.Controls.Add(Me.lblPressuremmh20)
        Me.Controls.Add(Me.lblPressuremmhg)
        Me.Controls.Add(Me.lblPressurebar)
        Me.Controls.Add(Me.lblPressuredyncm2)
        Me.Controls.Add(Me.lblPressureatm)
        Me.Controls.Add(Me.lblPressureat)
        Me.Controls.Add(Me.lblPressurekgcm2)
        Me.Controls.Add(Me.lblPressurepa)
        Me.Controls.Add(Me.lblPressurekpa)
        Me.Controls.Add(Me.lblPressurempa)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitPressure"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "压力(Pressure)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitPressure
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitPressure
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitPressure()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月31日19:35于青海省海西洲大柴旦红山参2井
	'压力单位换算
	
	Dim mpa As Single
	Dim kpa As Single
	Dim pa As Single
	Dim kgcm2 As Single
	Dim at As Single
	Dim atm As Single
	Dim dyncm2 As Single
	Dim bar As Single
	Dim mmhg As Single
	Dim mmh20 As Single
	Dim inhg As Single
	Dim inh20 As Single
	Dim lbin2 As Single
	Dim lbft2 As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitPressure.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtPressureat_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressureat.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		at = Val(txtPressureat.Text)
		
		mpa = at * 0.0981
		kpa = at * 98.0665
		pa = at * 98067#
		kgcm2 = at * 1#
		atm = at * 0.9678
		dyncm2 = at * 980670#
		bar = at * 0.9807
		mmhg = at * 735.5591
		mmh20 = at * 10000#
		inhg = at * 28.959
		inh20 = at * 393.7008
		lbin2 = at * 14.2233
		lbft2 = at * 2048.1615
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		'txtPressureat.Text = at
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	
	Private Sub txtPressureatm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressureatm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		atm = Val(txtPressureatm.Text)
		
		mpa = atm * 0.1013
		kpa = atm * 101.325
		pa = atm * 101330#
		kgcm2 = atm * 1.0332
		at = atm * 1.0332
		dyncm2 = atm * 1013300#
		bar = atm * 1.0133
		mmhg = atm * 760#
		mmh20 = atm * 10332#
		inhg = atm * 29.9213
		inh20 = atm * 406.7825
		lbin2 = atm * 14.6959
		lbft2 = atm * 2116.2167
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		'txtPressureatm.Text = atm
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressurebar_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurebar.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		bar = Val(txtPressurebar.Text)
		
		mpa = bar * 0.1
		kpa = bar * 100#
		pa = bar * 100000#
		kgcm2 = bar * 1.0197
		at = bar * 1.0197
		atm = bar * 0.9869
		dyncm2 = bar * 1000000#
		mmhg = bar * 750.0615
		mmh20 = bar * 10197#
		inhg = bar * 29.53
		inh20 = bar * 401.4631
		lbin2 = bar * 14.5038
		lbft2 = bar * 2088.5435
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		'txtPressurebar.Text = bar
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressuredyncm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressuredyncm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dyncm2 = Val(txtPressuredyncm2.Text)
		
		mpa = dyncm2 * 0.0000001
		kpa = dyncm2 * 0.0001
		pa = dyncm2 * 0.1
		kgcm2 = dyncm2 * 0.0000010197
		at = dyncm2 * 0.0000010197
		atm = dyncm2 * 0.00000098692
		bar = dyncm2 * 0.000001
		mmhg = dyncm2 * 0.00075006
		mmh20 = dyncm2 * 0.0102
		inhg = dyncm2 * 0.00002953
		inh20 = dyncm2 * 0.00040147
		lbin2 = dyncm2 * 0.000014504
		lbft2 = dyncm2 * 0.0021
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		'txtPressuredyncm2.Text = dyncm2
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	
	
	Private Sub txtPressureinh20_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressureinh20.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		inh20 = Val(txtPressureinh20.Text)
		
		mpa = inh20 * 0.00024909
		kpa = inh20 * 0.2491
		pa = inh20 * 249.0889
		kgcm2 = inh20 * 0.0025
		at = inh20 * 0.0025
		atm = inh20 * 0.0025
		dyncm2 = inh20 * 2490.8891
		bar = inh20 * 0.0025
		mmhg = inh20 * 1.8683
		mmh20 = inh20 * 25.4
		inhg = inh20 * 0.0736
		lbin2 = inh20 * 0.0361
		lbft2 = inh20 * 5.2023
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		'txtPressureinh20.Text = inh20
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressureinhg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressureinhg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		inhg = Val(txtPressureinhg.Text)
		
		mpa = inhg * 0.0034
		kpa = inhg * 3.3864
		pa = inhg * 3386.389
		kgcm2 = inhg * 0.0345
		at = inhg * 0.0345
		atm = inhg * 0.0334
		dyncm2 = inhg * 33864#
		bar = inhg * 0.0339
		mmhg = inhg * 25.4
		mmh20 = inhg * 345.3156
		inh20 = inhg * 13.5951
		lbin2 = inhg * 0.4912
		lbft2 = inhg * 70.7262
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		'txtPressureinhg.Text = inhg
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressurekgcm2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurekgcm2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgcm2 = Val(txtPressurekgcm2.Text)
		
		mpa = kgcm2 * 0.0981
		kpa = kgcm2 * 98.0665
		pa = kgcm2 * 98067#
		at = kgcm2 * 1#
		atm = kgcm2 * 0.9678
		dyncm2 = kgcm2 * 980670#
		bar = kgcm2 * 0.9807
		mmhg = kgcm2 * 735.5591
		mmh20 = kgcm2 * 10000#
		inhg = kgcm2 * 28.959
		inh20 = kgcm2 * 393.7008
		lbin2 = kgcm2 * 14.2233
		lbft2 = kgcm2 * 2048.1615
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		'txtPressurekgcm2.Text = kgcm2
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressurekpa_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurekpa.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kpa = Val(txtPressurekpa.Text)
		
		mpa = kpa * 0.001
		pa = kpa * 1000#
		kgcm2 = kpa * 0.0102
		at = kpa * 0.0102
		atm = kpa * 0.0099
		dyncm2 = kpa * 10000#
		bar = kpa * 0.01
		mmhg = kpa * 7.5006
		mmh20 = kpa * 101.97
		inhg = kpa * 0.2953
		inh20 = kpa * 4.0146
		lbin2 = kpa * 0.145
		lbft2 = kpa * 20.885
		
		txtPressurempa.Text = CStr(mpa)
		'txtPressurekpa.Text = kpa
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	Private Sub txtPressurelbft2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurelbft2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbft2 = Val(txtPressurelbft2.Text)
		
		mpa = lbft2 * 0.00004788
		kpa = lbft2 * 0.0479
		pa = lbft2 * 47.8803
		kgcm2 = lbft2 * 0.00048824
		at = lbft2 * 0.00048824
		atm = lbft2 * 0.00047254
		dyncm2 = lbft2 * 478.8026
		bar = lbft2 * 0.0005
		mmhg = lbft2 * 0.3591
		mmh20 = lbft2 * 4.8824
		inhg = lbft2 * 0.0141
		inh20 = lbft2 * 0.1922
		lbin2 = lbft2 * 0.0069
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		'txtPressurelbft2.Text = lbft2
	End Sub
	
	Private Sub txtPressurelbin2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurelbin2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbin2 = Val(txtPressurelbin2.Text)
		
		mpa = lbin2 * 0.0069
		kpa = lbin2 * 6.8948
		pa = lbin2 * 6894.757
		kgcm2 = lbin2 * 0.0703
		at = lbin2 * 0.0703
		atm = lbin2 * 0.068
		dyncm2 = lbin2 * 68948#
		bar = lbin2 * 0.0689
		mmhg = lbin2 * 51.7149
		mmh20 = lbin2 * 703.0695
		inhg = lbin2 * 2.036
		inh20 = lbin2 * 27.6799
		lbft2 = lbin2 * 144#
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		'txtPressurelbin2.Text = lbin2
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressuremmh20_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressuremmh20.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mmh20 = Val(txtPressuremmh20.Text)
		
		mpa = mmh20 * 0.0000098067
		kpa = mmh20 * 0.0098
		pa = mmh20 * 9.8067
		kgcm2 = mmh20 * 0.0001
		at = mmh20 * 0.0001
		atm = mmh20 * 0.00009678
		dyncm2 = mmh20 * 98.0665
		bar = mmh20 * 0.000098067
		mmhg = mmh20 * 0.0736
		inhg = mmh20 * 0.0029
		inh20 = mmh20 * 0.0394
		lbin2 = mmh20 * 0.0014
		lbft2 = mmh20 * 0.2048
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		'txtPressuremmh20.Text = mmh20
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressuremmhg_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressuremmhg.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mmhg = Val(txtPressuremmhg.Text)
		
		mpa = mmhg * 0.00013332
		kpa = mmhg * 0.1333
		pa = mmhg * 133.3224
		kgcm2 = mmhg * 0.0014
		at = mmhg * 0.0014
		atm = mmhg * 0.0013
		dyncm2 = mmhg * 1333.224
		bar = mmhg * 0.0013
		mmh20 = mmhg * 13.5951
		inhg = mmhg * 0.0394
		inh20 = mmhg * 0.5352
		lbin2 = mmhg * 0.0193
		lbft2 = mmhg * 2.7845
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		'txtPressuremmhg.Text = mmhg
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressurempa_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurempa.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mpa = Val(txtPressurempa.Text)
		
		kpa = mpa * 1000#
		pa = mpa * 1000000#
		kgcm2 = mpa * 10.1972
		at = mpa * 10.1972
		atm = mpa * 9.8692
		dyncm2 = mpa * 10000000#
		bar = mpa * 10#
		mmhg = mpa * 7500.6151
		mmh20 = mpa * 101970#
		inhg = mpa * 295.2998
		inh20 = mpa * 4014.6308
		lbin2 = mpa * 145.0377
		lbft2 = mpa * 20885#
		
		'txtPressurempa.Text = mpa
		txtPressurekpa.Text = CStr(kpa)
		txtPressurepa.Text = CStr(pa)
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
	
	Private Sub txtPressurepa_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPressurepa.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		pa = Val(txtPressurepa.Text)
		
		mpa = pa * 0.000001
		kpa = pa * 0.001
		kgcm2 = pa * 0.000010197
		at = pa * 0.000010197
		atm = pa * 0.0000098692
		dyncm2 = pa * 10#
		bar = pa * 0.00001
		mmhg = pa * 0.0075
		mmh20 = pa * 0.102
		inhg = pa * 0.0002953
		inh20 = pa * 0.004
		lbin2 = pa * 0.00014504
		lbft2 = pa * 0.0209
		
		txtPressurempa.Text = CStr(mpa)
		txtPressurekpa.Text = CStr(kpa)
		'txtPressurepa.Text = pa
		txtPressurekgcm2.Text = CStr(kgcm2)
		txtPressureat.Text = CStr(at)
		txtPressureatm.Text = CStr(atm)
		txtPressuredyncm2.Text = CStr(dyncm2)
		txtPressurebar.Text = CStr(bar)
		txtPressuremmhg.Text = CStr(mmhg)
		txtPressuremmh20.Text = CStr(mmh20)
		txtPressureinhg.Text = CStr(inhg)
		txtPressureinh20.Text = CStr(inh20)
		txtPressurelbin2.Text = CStr(lbin2)
		txtPressurelbft2.Text = CStr(lbft2)
	End Sub
End Class