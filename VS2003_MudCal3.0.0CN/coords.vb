Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("clsCoords_NET.clsCoords")> Public Class clsCoords
	' An object that holds the x and y co-ordinates
	' for wrongly marked squares (ones w/o mines)
	Public mintX As Short
	Public mintY As Short
End Class