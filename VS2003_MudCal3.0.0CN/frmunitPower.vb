Option Strict Off
Option Explicit On
Friend Class frmunitPower
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents txtPowerjs As System.Windows.Forms.TextBox
	Public WithEvents txtPowerergs As System.Windows.Forms.TextBox
	Public WithEvents txtPowerbtuh As System.Windows.Forms.TextBox
	Public WithEvents txtPowerbtumin As System.Windows.Forms.TextBox
	Public WithEvents txtPowerhp As System.Windows.Forms.TextBox
	Public WithEvents txtPowerftlbs As System.Windows.Forms.TextBox
	Public WithEvents txtPowerkcalh As System.Windows.Forms.TextBox
	Public WithEvents txtPowerkgms As System.Windows.Forms.TextBox
	Public WithEvents txtPowerzw As System.Windows.Forms.TextBox
	Public WithEvents txtPowerkw As System.Windows.Forms.TextBox
	Public WithEvents txtPowerw As System.Windows.Forms.TextBox
	Public WithEvents txtPowermw As System.Windows.Forms.TextBox
	Public WithEvents lblPowerinput As System.Windows.Forms.Label
	Public WithEvents lblPowerjs As System.Windows.Forms.Label
	Public WithEvents lblPowerergs As System.Windows.Forms.Label
	Public WithEvents lblPowerbtuh As System.Windows.Forms.Label
	Public WithEvents lblPowerbtumin As System.Windows.Forms.Label
	Public WithEvents lblPowerhp As System.Windows.Forms.Label
	Public WithEvents lblPowerftlbs As System.Windows.Forms.Label
	Public WithEvents lblPowerkcalh As System.Windows.Forms.Label
	Public WithEvents lblPowerkgms As System.Windows.Forms.Label
	Public WithEvents lblPowerzw As System.Windows.Forms.Label
	Public WithEvents lblPowerkw As System.Windows.Forms.Label
	Public WithEvents lblPowerw As System.Windows.Forms.Label
	Public WithEvents lblPowermw As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblPowerjs = New System.Windows.Forms.Label
        Me.lblPowerergs = New System.Windows.Forms.Label
        Me.lblPowerbtuh = New System.Windows.Forms.Label
        Me.lblPowerbtumin = New System.Windows.Forms.Label
        Me.lblPowerhp = New System.Windows.Forms.Label
        Me.lblPowerftlbs = New System.Windows.Forms.Label
        Me.lblPowerkcalh = New System.Windows.Forms.Label
        Me.lblPowerkgms = New System.Windows.Forms.Label
        Me.lblPowerzw = New System.Windows.Forms.Label
        Me.lblPowerkw = New System.Windows.Forms.Label
        Me.lblPowerw = New System.Windows.Forms.Label
        Me.lblPowermw = New System.Windows.Forms.Label
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtPowerjs = New System.Windows.Forms.TextBox
        Me.txtPowerergs = New System.Windows.Forms.TextBox
        Me.txtPowerbtuh = New System.Windows.Forms.TextBox
        Me.txtPowerbtumin = New System.Windows.Forms.TextBox
        Me.txtPowerhp = New System.Windows.Forms.TextBox
        Me.txtPowerftlbs = New System.Windows.Forms.TextBox
        Me.txtPowerkcalh = New System.Windows.Forms.TextBox
        Me.txtPowerkgms = New System.Windows.Forms.TextBox
        Me.txtPowerzw = New System.Windows.Forms.TextBox
        Me.txtPowerkw = New System.Windows.Forms.TextBox
        Me.txtPowerw = New System.Windows.Forms.TextBox
        Me.txtPowermw = New System.Windows.Forms.TextBox
        Me.lblPowerinput = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblPowerjs
        '
        Me.lblPowerjs.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerjs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerjs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerjs.Location = New System.Drawing.Point(278, 207)
        Me.lblPowerjs.Name = "lblPowerjs"
        Me.lblPowerjs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerjs.Size = New System.Drawing.Size(69, 18)
        Me.lblPowerjs.TabIndex = 23
        Me.lblPowerjs.Text = "J/s"
        Me.ToolTip1.SetToolTip(Me.lblPowerjs, "焦每秒")
        '
        'lblPowerergs
        '
        Me.lblPowerergs.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerergs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerergs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerergs.Location = New System.Drawing.Point(278, 172)
        Me.lblPowerergs.Name = "lblPowerergs"
        Me.lblPowerergs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerergs.Size = New System.Drawing.Size(69, 19)
        Me.lblPowerergs.TabIndex = 22
        Me.lblPowerergs.Text = "erg/s"
        Me.ToolTip1.SetToolTip(Me.lblPowerergs, "尔格每秒")
        '
        'lblPowerbtuh
        '
        Me.lblPowerbtuh.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerbtuh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerbtuh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerbtuh.Location = New System.Drawing.Point(278, 138)
        Me.lblPowerbtuh.Name = "lblPowerbtuh"
        Me.lblPowerbtuh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerbtuh.Size = New System.Drawing.Size(69, 18)
        Me.lblPowerbtuh.TabIndex = 21
        Me.lblPowerbtuh.Text = "BTU/h"
        Me.ToolTip1.SetToolTip(Me.lblPowerbtuh, "英热单位每时")
        '
        'lblPowerbtumin
        '
        Me.lblPowerbtumin.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerbtumin.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerbtumin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerbtumin.Location = New System.Drawing.Point(278, 103)
        Me.lblPowerbtumin.Name = "lblPowerbtumin"
        Me.lblPowerbtumin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerbtumin.Size = New System.Drawing.Size(69, 19)
        Me.lblPowerbtumin.TabIndex = 20
        Me.lblPowerbtumin.Text = "BTU/min"
        Me.ToolTip1.SetToolTip(Me.lblPowerbtumin, "英热单位每分")
        '
        'lblPowerhp
        '
        Me.lblPowerhp.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerhp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerhp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerhp.Location = New System.Drawing.Point(278, 69)
        Me.lblPowerhp.Name = "lblPowerhp"
        Me.lblPowerhp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerhp.Size = New System.Drawing.Size(69, 18)
        Me.lblPowerhp.TabIndex = 19
        Me.lblPowerhp.Text = "HP"
        Me.ToolTip1.SetToolTip(Me.lblPowerhp, "马力")
        '
        'lblPowerftlbs
        '
        Me.lblPowerftlbs.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerftlbs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerftlbs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerftlbs.Location = New System.Drawing.Point(278, 34)
        Me.lblPowerftlbs.Name = "lblPowerftlbs"
        Me.lblPowerftlbs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerftlbs.Size = New System.Drawing.Size(69, 19)
        Me.lblPowerftlbs.TabIndex = 18
        Me.lblPowerftlbs.Text = "ft.lbf/s"
        Me.ToolTip1.SetToolTip(Me.lblPowerftlbs, "英尺磅力每秒")
        '
        'lblPowerkcalh
        '
        Me.lblPowerkcalh.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerkcalh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerkcalh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerkcalh.Location = New System.Drawing.Point(96, 207)
        Me.lblPowerkcalh.Name = "lblPowerkcalh"
        Me.lblPowerkcalh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerkcalh.Size = New System.Drawing.Size(68, 18)
        Me.lblPowerkcalh.TabIndex = 11
        Me.lblPowerkcalh.Text = "kCal/h"
        Me.ToolTip1.SetToolTip(Me.lblPowerkcalh, "千卡每时")
        '
        'lblPowerkgms
        '
        Me.lblPowerkgms.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerkgms.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerkgms.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerkgms.Location = New System.Drawing.Point(96, 172)
        Me.lblPowerkgms.Name = "lblPowerkgms"
        Me.lblPowerkgms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerkgms.Size = New System.Drawing.Size(68, 19)
        Me.lblPowerkgms.TabIndex = 10
        Me.lblPowerkgms.Text = "kg.m/s"
        Me.ToolTip1.SetToolTip(Me.lblPowerkgms, "千克.米每秒")
        '
        'lblPowerzw
        '
        Me.lblPowerzw.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerzw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerzw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerzw.Location = New System.Drawing.Point(96, 138)
        Me.lblPowerzw.Name = "lblPowerzw"
        Me.lblPowerzw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerzw.Size = New System.Drawing.Size(68, 18)
        Me.lblPowerzw.TabIndex = 9
        Me.lblPowerzw.Text = "MW"
        Me.ToolTip1.SetToolTip(Me.lblPowerzw, "兆瓦")
        '
        'lblPowerkw
        '
        Me.lblPowerkw.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerkw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerkw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerkw.Location = New System.Drawing.Point(96, 103)
        Me.lblPowerkw.Name = "lblPowerkw"
        Me.lblPowerkw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerkw.Size = New System.Drawing.Size(68, 19)
        Me.lblPowerkw.TabIndex = 8
        Me.lblPowerkw.Text = "kW"
        Me.ToolTip1.SetToolTip(Me.lblPowerkw, "千瓦")
        '
        'lblPowerw
        '
        Me.lblPowerw.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerw.Location = New System.Drawing.Point(96, 69)
        Me.lblPowerw.Name = "lblPowerw"
        Me.lblPowerw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerw.Size = New System.Drawing.Size(68, 18)
        Me.lblPowerw.TabIndex = 7
        Me.lblPowerw.Text = "W"
        Me.ToolTip1.SetToolTip(Me.lblPowerw, "瓦")
        '
        'lblPowermw
        '
        Me.lblPowermw.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowermw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowermw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowermw.Location = New System.Drawing.Point(96, 34)
        Me.lblPowermw.Name = "lblPowermw"
        Me.lblPowermw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowermw.Size = New System.Drawing.Size(68, 19)
        Me.lblPowermw.TabIndex = 6
        Me.lblPowermw.Text = "mW"
        Me.ToolTip1.SetToolTip(Me.lblPowermw, "毫瓦")
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(230, 250)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(88, 27)
        Me.cmdClear.TabIndex = 25
        Me.cmdClear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(48, 250)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(88, 27)
        Me.cmdquit.TabIndex = 24
        Me.cmdquit.Text = "退出"
        '
        'txtPowerjs
        '
        Me.txtPowerjs.AcceptsReturn = True
        Me.txtPowerjs.AutoSize = False
        Me.txtPowerjs.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerjs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerjs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerjs.Location = New System.Drawing.Point(192, 207)
        Me.txtPowerjs.MaxLength = 0
        Me.txtPowerjs.Name = "txtPowerjs"
        Me.txtPowerjs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerjs.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerjs.TabIndex = 17
        Me.txtPowerjs.Text = ""
        '
        'txtPowerergs
        '
        Me.txtPowerergs.AcceptsReturn = True
        Me.txtPowerergs.AutoSize = False
        Me.txtPowerergs.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerergs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerergs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerergs.Location = New System.Drawing.Point(192, 172)
        Me.txtPowerergs.MaxLength = 0
        Me.txtPowerergs.Name = "txtPowerergs"
        Me.txtPowerergs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerergs.Size = New System.Drawing.Size(78, 20)
        Me.txtPowerergs.TabIndex = 16
        Me.txtPowerergs.Text = ""
        '
        'txtPowerbtuh
        '
        Me.txtPowerbtuh.AcceptsReturn = True
        Me.txtPowerbtuh.AutoSize = False
        Me.txtPowerbtuh.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerbtuh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerbtuh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerbtuh.Location = New System.Drawing.Point(192, 138)
        Me.txtPowerbtuh.MaxLength = 0
        Me.txtPowerbtuh.Name = "txtPowerbtuh"
        Me.txtPowerbtuh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerbtuh.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerbtuh.TabIndex = 15
        Me.txtPowerbtuh.Text = ""
        '
        'txtPowerbtumin
        '
        Me.txtPowerbtumin.AcceptsReturn = True
        Me.txtPowerbtumin.AutoSize = False
        Me.txtPowerbtumin.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerbtumin.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerbtumin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerbtumin.Location = New System.Drawing.Point(192, 103)
        Me.txtPowerbtumin.MaxLength = 0
        Me.txtPowerbtumin.Name = "txtPowerbtumin"
        Me.txtPowerbtumin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerbtumin.Size = New System.Drawing.Size(78, 20)
        Me.txtPowerbtumin.TabIndex = 14
        Me.txtPowerbtumin.Text = ""
        '
        'txtPowerhp
        '
        Me.txtPowerhp.AcceptsReturn = True
        Me.txtPowerhp.AutoSize = False
        Me.txtPowerhp.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerhp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerhp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerhp.Location = New System.Drawing.Point(192, 69)
        Me.txtPowerhp.MaxLength = 0
        Me.txtPowerhp.Name = "txtPowerhp"
        Me.txtPowerhp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerhp.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerhp.TabIndex = 13
        Me.txtPowerhp.Text = ""
        '
        'txtPowerftlbs
        '
        Me.txtPowerftlbs.AcceptsReturn = True
        Me.txtPowerftlbs.AutoSize = False
        Me.txtPowerftlbs.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerftlbs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerftlbs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerftlbs.Location = New System.Drawing.Point(192, 34)
        Me.txtPowerftlbs.MaxLength = 0
        Me.txtPowerftlbs.Name = "txtPowerftlbs"
        Me.txtPowerftlbs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerftlbs.Size = New System.Drawing.Size(78, 20)
        Me.txtPowerftlbs.TabIndex = 12
        Me.txtPowerftlbs.Text = ""
        '
        'txtPowerkcalh
        '
        Me.txtPowerkcalh.AcceptsReturn = True
        Me.txtPowerkcalh.AutoSize = False
        Me.txtPowerkcalh.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerkcalh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerkcalh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerkcalh.Location = New System.Drawing.Point(10, 207)
        Me.txtPowerkcalh.MaxLength = 0
        Me.txtPowerkcalh.Name = "txtPowerkcalh"
        Me.txtPowerkcalh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerkcalh.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerkcalh.TabIndex = 5
        Me.txtPowerkcalh.Text = ""
        '
        'txtPowerkgms
        '
        Me.txtPowerkgms.AcceptsReturn = True
        Me.txtPowerkgms.AutoSize = False
        Me.txtPowerkgms.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerkgms.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerkgms.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerkgms.Location = New System.Drawing.Point(10, 172)
        Me.txtPowerkgms.MaxLength = 0
        Me.txtPowerkgms.Name = "txtPowerkgms"
        Me.txtPowerkgms.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerkgms.Size = New System.Drawing.Size(78, 20)
        Me.txtPowerkgms.TabIndex = 4
        Me.txtPowerkgms.Text = ""
        '
        'txtPowerzw
        '
        Me.txtPowerzw.AcceptsReturn = True
        Me.txtPowerzw.AutoSize = False
        Me.txtPowerzw.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerzw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerzw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerzw.Location = New System.Drawing.Point(10, 138)
        Me.txtPowerzw.MaxLength = 0
        Me.txtPowerzw.Name = "txtPowerzw"
        Me.txtPowerzw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerzw.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerzw.TabIndex = 3
        Me.txtPowerzw.Text = ""
        '
        'txtPowerkw
        '
        Me.txtPowerkw.AcceptsReturn = True
        Me.txtPowerkw.AutoSize = False
        Me.txtPowerkw.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerkw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerkw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerkw.Location = New System.Drawing.Point(10, 103)
        Me.txtPowerkw.MaxLength = 0
        Me.txtPowerkw.Name = "txtPowerkw"
        Me.txtPowerkw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerkw.Size = New System.Drawing.Size(78, 20)
        Me.txtPowerkw.TabIndex = 2
        Me.txtPowerkw.Text = ""
        '
        'txtPowerw
        '
        Me.txtPowerw.AcceptsReturn = True
        Me.txtPowerw.AutoSize = False
        Me.txtPowerw.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowerw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowerw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowerw.Location = New System.Drawing.Point(10, 69)
        Me.txtPowerw.MaxLength = 0
        Me.txtPowerw.Name = "txtPowerw"
        Me.txtPowerw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowerw.Size = New System.Drawing.Size(78, 19)
        Me.txtPowerw.TabIndex = 1
        Me.txtPowerw.Text = ""
        '
        'txtPowermw
        '
        Me.txtPowermw.AcceptsReturn = True
        Me.txtPowermw.AutoSize = False
        Me.txtPowermw.BackColor = System.Drawing.SystemColors.Window
        Me.txtPowermw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPowermw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPowermw.Location = New System.Drawing.Point(10, 34)
        Me.txtPowermw.MaxLength = 0
        Me.txtPowermw.Name = "txtPowermw"
        Me.txtPowermw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPowermw.Size = New System.Drawing.Size(78, 20)
        Me.txtPowermw.TabIndex = 0
        Me.txtPowermw.Text = ""
        '
        'lblPowerinput
        '
        Me.lblPowerinput.BackColor = System.Drawing.SystemColors.Control
        Me.lblPowerinput.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPowerinput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblPowerinput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPowerinput.Location = New System.Drawing.Point(10, 9)
        Me.lblPowerinput.Name = "lblPowerinput"
        Me.lblPowerinput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPowerinput.Size = New System.Drawing.Size(241, 18)
        Me.lblPowerinput.TabIndex = 26
        Me.lblPowerinput.Text = "输入功率单位换算数据："
        '
        'frmunitPower
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(366, 290)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.txtPowerjs)
        Me.Controls.Add(Me.txtPowerergs)
        Me.Controls.Add(Me.txtPowerbtuh)
        Me.Controls.Add(Me.txtPowerbtumin)
        Me.Controls.Add(Me.txtPowerhp)
        Me.Controls.Add(Me.txtPowerftlbs)
        Me.Controls.Add(Me.txtPowerkcalh)
        Me.Controls.Add(Me.txtPowerkgms)
        Me.Controls.Add(Me.txtPowerzw)
        Me.Controls.Add(Me.txtPowerkw)
        Me.Controls.Add(Me.txtPowerw)
        Me.Controls.Add(Me.txtPowermw)
        Me.Controls.Add(Me.lblPowerinput)
        Me.Controls.Add(Me.lblPowerjs)
        Me.Controls.Add(Me.lblPowerergs)
        Me.Controls.Add(Me.lblPowerbtuh)
        Me.Controls.Add(Me.lblPowerbtumin)
        Me.Controls.Add(Me.lblPowerhp)
        Me.Controls.Add(Me.lblPowerftlbs)
        Me.Controls.Add(Me.lblPowerkcalh)
        Me.Controls.Add(Me.lblPowerkgms)
        Me.Controls.Add(Me.lblPowerzw)
        Me.Controls.Add(Me.lblPowerkw)
        Me.Controls.Add(Me.lblPowerw)
        Me.Controls.Add(Me.lblPowermw)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitPower"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "功率(Power)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitPower
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitPower
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitPower()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年7月31日16:43于青海省海西洲大柴旦红山参2井
	'功率单位换算
	
	Dim mw As Single
	Dim w As Single
	Dim kw As Single
	Dim zw As Single
	Dim kcalh As Single
	Dim kgms As Single
	Dim hp As Single
	Dim ftlbs As Single
	Dim btumin As Single
	Dim btuh As Single
	Dim ergs As Single
	Dim js As Single
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitPower.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtPowerbtuh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerbtuh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		btuh = Val(txtPowerbtuh.Text)
		
		mw = btuh * 293.0711
		w = btuh * 0.2931
		kw = btuh * 0.00029307
		zw = btuh * 0.00000029307
		kgms = btuh * 0.0299
		kcalh = btuh * 0.252
		ftlbs = btuh * 0.2162
		hp = btuh * 0.00039301
		btumin = btuh * 0.0167
		ergs = btuh * 2930700#
		js = btuh * 0.2931
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		'txtPowerbtuh.Text = btuh
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerbtumin_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerbtumin.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		btumin = Val(txtPowerbtumin.Text)
		
		mw = btumin * 17584#
		w = btumin * 17.5843
		kw = btumin * 0.0176
		zw = btumin * 0.000017584
		kgms = btumin * 1.7931
		kcalh = btumin * 15.1197
		ftlbs = btumin * 12.9695
		hp = btumin * 0.0236
		btuh = btumin * 60#
		ergs = btumin * 175840000#
		js = btumin * 17.5843
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		'txtPowerbtumin.Text = btumin
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerergs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerergs.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ergs = Val(txtPowerergs.Text)
		
		mw = ergs * 0.0001
		w = ergs * 0.0000001
		kw = ergs * 0.0000000001
		zw = ergs * 0.0000000000001
		kgms = ergs * 0.000000010197
		kcalh = ergs * 0.000000085985
		ftlbs = ergs * 0.000000073756
		hp = ergs * 0.0000000001341
		btumin = ergs * 0.0000000056869
		btuh = ergs * 0.00000034121
		js = ergs * 0.0000001
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		'txtPowerergs.Text = ergs
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerftlbs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerftlbs.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ftlbs = Val(txtPowerftlbs.Text)
		
		mw = ftlbs * 1355.818
		w = ftlbs * 1.3558
		kw = ftlbs * 0.0014
		zw = ftlbs * 0.0000013558
		kgms = ftlbs * 0.1383
		kcalh = ftlbs * 1.1658
		hp = ftlbs * 0.0018
		btumin = ftlbs * 0.0771
		btuh = ftlbs * 4.6262
		ergs = ftlbs * 13558000#
		js = ftlbs * 1.3558
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		'txtPowerftlbs.Text = ftlbs
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerhp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerhp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		hp = Val(txtPowerhp.Text)
		
		mw = hp * 745700#
		w = hp * 745.6999
		kw = hp * 0.7457
		zw = hp * 0.0007457
		kgms = hp * 76.0402
		kcalh = hp * 641.1865
		ftlbs = hp * 550#
		btumin = hp * 42.4072
		btuh = hp * 2544.4333
		ergs = hp * 7457000000#
		js = hp * 745.6999
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		'txtPowerhp.Text = hp
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerjs_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerjs.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		js = Val(txtPowerjs.Text)
		
		mw = js * 1000#
		w = js * 1#
		kw = js * 0.001
		zw = js * 0.000001
		kgms = js * 0.102
		kcalh = js * 0.8598
		ftlbs = js * 0.7376
		hp = js * 0.0013
		btumin = js * 0.0569
		btuh = js * 3.4121
		ergs = js * 10000000#
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		'txtPowerjs.Text = js
	End Sub
	
	Private Sub txtPowerkcalh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerkcalh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kcalh = Val(txtPowerkcalh.Text)
		
		mw = kcalh * 1163#
		w = kcalh * 1.163
		kw = kcalh * 0.0012
		zw = kcalh * 0.000001163
		kgms = kcalh * 0.1186
		ftlbs = kcalh * 0.8578
		hp = kcalh * 0.0016
		btumin = kcalh * 0.0661
		btuh = kcalh * 3.9683
		ergs = kcalh * 11630000#
		js = kcalh * 1.163
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		'txtPowerkcalh.Text = kcalh
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerkgms_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerkgms.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgms = Val(txtPowerkgms.Text)
		
		mw = kgms * 9806.65
		w = kgms * 9.8066
		kw = kgms * 0.0098
		zw = kgms * 0.0000098066
		kcalh = kgms * 8.4322
		ftlbs = kgms * 7.233
		hp = kgms * 0.0132
		btumin = kgms * 0.5577
		btuh = kgms * 33.4617
		ergs = kgms * 98067000#
		js = kgms * 9.8066
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		'txtPowerkgms.Text = kgms
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerkw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerkw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kw = Val(txtPowerkw.Text)
		
		mw = kw * 1000000#
		w = kw * 1000
		zw = kw * 0.001
		kgms = kw * 101.9716
		kcalh = kw * 859.8452
		ftlbs = kw * 737.5621
		hp = kw * 1.341
		btumin = kw * 56.869
		btuh = kw * 3412.1412
		ergs = kw * 10000000000#
		js = kw * 1000#
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		'txtPowerkw.Text = kw
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
		
	End Sub
	
	Private Sub txtPowermw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowermw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mw = Val(txtPowermw.Text)
		
		kw = mw * 0.000001
		w = mw * 0.001
		zw = mw * 0.000000001
		kgms = mw * 0.00010199
		kcalh = mw * 0.00086
		ftlbs = mw * 0.00073768
		hp = mw * 0.000001341
		btumin = mw * 0.000056869
		btuh = mw * 0.0034
		ergs = mw * 10000#
		js = mw * 0.001
		
		
		'txtPowermw.Text = mw
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	
	
	Private Sub txtPowerw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		w = Val(txtPowerw.Text)
		
		kw = w * 0.001
		mw = w * 1000#
		zw = w * 0.000001
		kgms = w * 0.102
		kcalh = w * 0.8598
		ftlbs = w * 0.7376
		hp = w * 0.0013
		btumin = w * 0.0569
		btuh = w * 3.4121
		ergs = w * 10000000#
		js = w * 1#
		
		
		txtPowermw.Text = CStr(mw)
		'txtPowerw.Text = w
		txtPowerkw.Text = CStr(kw)
		txtPowerzw.Text = CStr(zw)
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
	
	Private Sub txtPowerzw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPowerzw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		zw = Val(txtPowerzw.Text)
		
		mw = zw * 1000000000#
		w = zw * 1000000#
		kw = zw * 1000#
		kgms = zw * 101970#
		kcalh = zw * 859850#
		ftlbs = zw * 737560#
		hp = zw * 1341.022
		btumin = zw * 56869#
		btuh = zw * 3412100#
		ergs = zw * 10000000000000#
		js = zw * 1000000#
		
		
		txtPowermw.Text = CStr(mw)
		txtPowerw.Text = CStr(w)
		txtPowerkw.Text = CStr(kw)
		'txtPowerzw.Text = zw
		txtPowerkgms.Text = CStr(kgms)
		txtPowerkcalh.Text = CStr(kcalh)
		txtPowerftlbs.Text = CStr(ftlbs)
		txtPowerhp.Text = CStr(hp)
		txtPowerbtumin.Text = CStr(btumin)
		txtPowerbtuh.Text = CStr(btuh)
		txtPowerergs.Text = CStr(ergs)
		txtPowerjs.Text = CStr(js)
	End Sub
End Class