Option Strict Off
Option Explicit On
Friend Class Form7
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
    Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Combo2 As System.Windows.Forms.ComboBox
	Public WithEvents Combo1 As System.Windows.Forms.ComboBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents gboxExample As System.Windows.Forms.GroupBox
    Friend WithEvents txtHelpOilgasVel As System.Windows.Forms.TextBox
    Friend WithEvents picOilGasOutflow As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form7))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Combo2 = New System.Windows.Forms.ComboBox
        Me.Combo1 = New System.Windows.Forms.ComboBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.gboxExample = New System.Windows.Forms.GroupBox
        Me.txtHelpOilgasVel = New System.Windows.Forms.TextBox
        Me.picOilGasOutflow = New System.Windows.Forms.PictureBox
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.gboxExample.SuspendLayout()
        Me.SuspendLayout()
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(567, 411)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(174, 61)
        Me.PicLogo.TabIndex = 22
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(413, 432)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 8
        Me.cmdExit.Text = "Exit"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(413, 408)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 7
        Me.cmdok.Text = "Run"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text6)
        Me.Frame2.Controls.Add(Me.Text5)
        Me.Frame2.Controls.Add(Me.Label9)
        Me.Frame2.Controls.Add(Me.Label8)
        Me.Frame2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(8, 376)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(318, 96)
        Me.Frame2.TabIndex = 9
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "油气上窜速度"
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(202, 60)
        Me.Text6.MaxLength = 5
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(73, 20)
        Me.Text6.TabIndex = 19
        Me.Text6.Text = ""
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(29, 60)
        Me.Text5.MaxLength = 5
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(78, 20)
        Me.Text5.TabIndex = 18
        Me.Text5.Text = ""
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(202, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(78, 18)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "m/min"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(29, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(78, 18)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "m/h"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Combo2)
        Me.Frame1.Controls.Add(Me.Combo1)
        Me.Frame1.Controls.Add(Me.Text4)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Label7)
        Me.Frame1.Controls.Add(Me.Label6)
        Me.Frame1.Controls.Add(Me.Label5)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Red
        Me.Frame1.Location = New System.Drawing.Point(8, 8)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(404, 360)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "数据录入"
        '
        'Combo2
        '
        Me.Combo2.BackColor = System.Drawing.SystemColors.Window
        Me.Combo2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo2.Location = New System.Drawing.Point(287, 207)
        Me.Combo2.Name = "Combo2"
        Me.Combo2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo2.Size = New System.Drawing.Size(78, 22)
        Me.Combo2.TabIndex = 4
        Me.Combo2.Text = "114.3"
        '
        'Combo1
        '
        Me.Combo1.BackColor = System.Drawing.SystemColors.Window
        Me.Combo1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo1.Location = New System.Drawing.Point(287, 155)
        Me.Combo1.Name = "Combo1"
        Me.Combo1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo1.Size = New System.Drawing.Size(78, 22)
        Me.Combo1.TabIndex = 3
        Me.Combo1.Text = "215.9"
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(287, 310)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(78, 20)
        Me.Text4.TabIndex = 6
        Me.Text4.Text = "20"
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(287, 258)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(78, 20)
        Me.Text3.TabIndex = 5
        Me.Text3.Text = "20"
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(287, 103)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(78, 20)
        Me.Text2.TabIndex = 2
        Me.Text2.Text = "30"
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(287, 52)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(78, 19)
        Me.Text1.TabIndex = 1
        Me.Text1.Text = "2640"
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(10, 310)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(241, 19)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "钻井液静止时间(h)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(10, 258)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(241, 19)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "未气侵钻井液循环时间 (min)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(10, 207)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(241, 19)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "钻杆外径(mm)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(10, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(241, 19)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "井径 (mm)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(10, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(241, 19)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "钻井泥浆泵排量 (L/S)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(10, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(241, 19)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "油/气层深度 (m)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gboxExample
        '
        Me.gboxExample.Controls.Add(Me.txtHelpOilgasVel)
        Me.gboxExample.Location = New System.Drawing.Point(420, 8)
        Me.gboxExample.Name = "gboxExample"
        Me.gboxExample.Size = New System.Drawing.Size(468, 360)
        Me.gboxExample.TabIndex = 23
        Me.gboxExample.TabStop = False
        Me.gboxExample.Text = "举例"
        '
        'txtHelpOilgasVel
        '
        Me.txtHelpOilgasVel.AcceptsReturn = True
        Me.txtHelpOilgasVel.AutoSize = False
        Me.txtHelpOilgasVel.ForeColor = System.Drawing.Color.Blue
        Me.txtHelpOilgasVel.Location = New System.Drawing.Point(8, 17)
        Me.txtHelpOilgasVel.MaxLength = 0
        Me.txtHelpOilgasVel.Multiline = True
        Me.txtHelpOilgasVel.Name = "txtHelpOilgasVel"
        Me.txtHelpOilgasVel.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtHelpOilgasVel.Size = New System.Drawing.Size(448, 336)
        Me.txtHelpOilgasVel.TabIndex = 0
        Me.txtHelpOilgasVel.Text = "     某井在2640米，钻遇油、气层后，即循环钻井液于18:00停泵" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "起钻，次日14:00下完钻开泵，自 14:20 发现钻井液油、气侵，当" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "时泵" & _
        "排量是30升/秒，井径为215.9毫米(8.5英寸)，114mm (4.5英" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "寸)钻杆。试计算油气上窜的速度？" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "具体参数应为：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & "泥浆静止时" & _
        "间是20小时" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & "油气层深度是2640米" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & "未受气侵泥浆返出时间是20分钟" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & "泥浆泵排量是30升/秒" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(9) & "计算结果是:油气上升速度为：" & _
        "63米/小时"
        '
        'picOilGasOutflow
        '
        Me.picOilGasOutflow.Image = CType(resources.GetObject("picOilGasOutflow.Image"), System.Drawing.Image)
        Me.picOilGasOutflow.Location = New System.Drawing.Point(21, 488)
        Me.picOilGasOutflow.Name = "picOilGasOutflow"
        Me.picOilGasOutflow.Size = New System.Drawing.Size(850, 90)
        Me.picOilGasOutflow.TabIndex = 24
        Me.picOilGasOutflow.TabStop = False
        '
        'Form7
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 16)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picOilGasOutflow)
        Me.Controls.Add(Me.gboxExample)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.ForeColor = System.Drawing.Color.Blue
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "Form7"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "油气上窜速度"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.gboxExample.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As Form7
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form7
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form7()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim Hoil As Single
	Dim PumpQ As Single
	Dim Dwell As Single
	Dim Dzg As Single
	Dim Hrno As Single
	Dim Hrstop As Single
	Dim Hoursd As Single
	Dim Hminsd As Single
	Const pi As Double = 3.1415926
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo1.SelectedIndexChanged
		Dwell = Val(Combo1.Text)
	End Sub
	
	Private Sub Combo1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Dwell = Val(Combo1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo2.SelectedIndexChanged
		Dzg = Val(Combo2.Text)
	End Sub
	
	Private Sub Combo2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Dzg = Val(Combo2.Text)
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Hoil = Val(Text1.Text)
		PumpQ = Val(Text2.Text)
		Hrno = Val(Text3.Text)
		Hrstop = Val(Text4.Text)
		Dwell = Val(Combo1.Text)
		Dzg = Val(Combo2.Text)
		
		If Dwell <= Dzg Then
			'MsgBox "你采集输入了错误数据,钻具外径数据应该小于井眼外径数据,请你修改更正数据错误。", vbInformation, "油气上窜速度计算"
			'Oil and Gas Upward flow velocity
			MsgBox("Please check that you have entered the'Drillstring OD' data!", MsgBoxStyle.Information, "Oil and Gas Upward flow velocity")
			Combo1.Text = ""
			Combo2.Text = ""
			Exit Sub
		End If
		Text5.Text = ""
		Text6.Text = ""
		Hoursd = (Hoil - (PumpQ * 60 * Hrno) / (0.25 * pi * ((Dwell / 100) ^ 2 - (Dzg / 100) ^ 2) * 10)) / Hrstop
		
		If Hoursd < 0 Then
			MsgBox("Please check that you have entered  datas!", MsgBoxStyle.Information, "Oil and Gas Upward flow velocity")
			Exit Sub
		End If
		
		Text5.Text = VB6.Format(Hoursd, "0.00")
		Hminsd = Hoursd / 60
		Text6.Text = VB6.Format(Hminsd, "0.00")
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub Form7_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

		'************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		'-----------------------------------------------------------
		
		
        'Call PicGWDC(PicLogo) '调用的是模块中的过程
		'--------------------------------------------------
		
		Combo1.Items.Add("99.6") '井眼直径或套管内径输入
		Combo1.Items.Add("101.6")
		Combo1.Items.Add("105.9")
		Combo1.Items.Add("108.6")
		Combo1.Items.Add("112.0")
		Combo1.Items.Add("118.6")
		Combo1.Items.Add("121.4")
		Combo1.Items.Add("124.3")
		Combo1.Items.Add("144.1")
		Combo1.Items.Add("147.1")
		Combo1.Items.Add("150.4")
		Combo1.Items.Add("159.4")
		Combo1.Items.Add("165.1")
		Combo1.Items.Add("168.3")
		Combo1.Items.Add("171.8")
		Combo1.Items.Add("193.7")
		Combo1.Items.Add("198.8")
		Combo1.Items.Add("201.2")
		Combo1.Items.Add("215.9")
		Combo1.Items.Add("224.4")
		Combo1.Items.Add("252.7")
		Combo1.Items.Add("273.6")
		Combo1.Items.Add("311.15")
		Combo1.Items.Add("313.6")
		Combo1.Items.Add("315.3")
		Combo1.Items.Add("317.9")
		Combo1.Items.Add("320.4")
		Combo1.Items.Add("381.3")
		Combo1.Items.Add("444.5")
		Combo1.Items.Add("451.0")
		Combo1.Items.Add("482.6")
		
		Combo2.Items.Add("60.3") '钻杆外径
		Combo2.Items.Add("73.0")
		Combo2.Items.Add("88.9")
		Combo2.Items.Add("101.6")
		Combo2.Items.Add("114.3")
		Combo2.Items.Add("118.6")
		Combo2.Items.Add("127.0")
		Combo2.Items.Add("139.7")
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MoutflowS.bmp")


        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MoutflowS.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Hoil = Val(Text1.Text)
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpQ = Val(Text2.Text)
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Hrno = Val(Text3.Text)
	End Sub
	
	Private Sub Text4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Hrstop = Val(Text3.Text)
	End Sub

   
    Private Sub PicLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PicLogo.Click

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub Frame2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Frame2.Enter

    End Sub
End Class