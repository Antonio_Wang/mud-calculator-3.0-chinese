Option Strict Off
Option Explicit On
Friend Class frmHhydraulic
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents lblBVprogram As System.Windows.Forms.Label
	Public WithEvents fraProgram As System.Windows.Forms.GroupBox
	Public WithEvents txtBVvm As System.Windows.Forms.TextBox
	Public WithEvents txtBVs6 As System.Windows.Forms.TextBox
	Public WithEvents txtBVdh As System.Windows.Forms.TextBox
	Public WithEvents txtBVdp As System.Windows.Forms.TextBox
	Public WithEvents txtBVdrc As System.Windows.Forms.TextBox
	Public WithEvents txtBVprc As System.Windows.Forms.TextBox
	Public WithEvents txtBVmw As System.Windows.Forms.TextBox
	Public WithEvents txtBVq As System.Windows.Forms.TextBox
	Public WithEvents txtBVs0 As System.Windows.Forms.TextBox
	Public WithEvents txtBVs3 As System.Windows.Forms.TextBox
	Public WithEvents lblBVvm As System.Windows.Forms.Label
	Public WithEvents lblBVs6 As System.Windows.Forms.Label
	Public WithEvents lblBVq As System.Windows.Forms.Label
	Public WithEvents lblBVdp As System.Windows.Forms.Label
	Public WithEvents lblBVdrc As System.Windows.Forms.Label
	Public WithEvents lblBVprc As System.Windows.Forms.Label
	Public WithEvents lblBVmw As System.Windows.Forms.Label
	Public WithEvents lblBVdh As System.Windows.Forms.Label
	Public WithEvents lblBVs0 As System.Windows.Forms.Label
	Public WithEvents lblBVs3 As System.Windows.Forms.Label
	Public WithEvents fraDataInput As System.Windows.Forms.GroupBox
	Public WithEvents txtBVdlmw As System.Windows.Forms.TextBox
	Public WithEvents txtBVtc As System.Windows.Forms.TextBox
	Public WithEvents txtBVvcr As System.Windows.Forms.TextBox
	Public WithEvents txtBVCa As System.Windows.Forms.TextBox
	Public WithEvents txtBVQc As System.Windows.Forms.TextBox
	Public WithEvents txtBVz As System.Windows.Forms.TextBox
	Public WithEvents txtBVs As System.Windows.Forms.TextBox
	Public WithEvents txtBVr As System.Windows.Forms.TextBox
	Public WithEvents txtBVmj As System.Windows.Forms.TextBox
	Public WithEvents txtBVuf As System.Windows.Forms.TextBox
	Public WithEvents txtBVpv As System.Windows.Forms.TextBox
	Public WithEvents txtBVk As System.Windows.Forms.TextBox
	Public WithEvents txtBVn As System.Windows.Forms.TextBox
	Public WithEvents txtBVhvs As System.Windows.Forms.TextBox
	Public WithEvents txtBVvs As System.Windows.Forms.TextBox
	Public WithEvents txtBVgvs As System.Windows.Forms.TextBox
	Public WithEvents txtBVlc As System.Windows.Forms.TextBox
	Public WithEvents lblBVdlmw As System.Windows.Forms.Label
	Public WithEvents lblBVtc As System.Windows.Forms.Label
	Public WithEvents lblBVvcr As System.Windows.Forms.Label
	Public WithEvents lblBVCa As System.Windows.Forms.Label
	Public WithEvents lblBVQc As System.Windows.Forms.Label
	Public WithEvents lblBVz As System.Windows.Forms.Label
	Public WithEvents lblBVs As System.Windows.Forms.Label
	Public WithEvents lblBVr As System.Windows.Forms.Label
	Public WithEvents lblBVmj As System.Windows.Forms.Label
	Public WithEvents lblBVuf As System.Windows.Forms.Label
	Public WithEvents lblBVpv As System.Windows.Forms.Label
	Public WithEvents lblBVk As System.Windows.Forms.Label
	Public WithEvents lblBVn As System.Windows.Forms.Label
	Public WithEvents lblBVhvs As System.Windows.Forms.Label
	Public WithEvents lblBVvs As System.Windows.Forms.Label
	Public WithEvents lblBVgvs As System.Windows.Forms.Label
	Public WithEvents lblBVlc As System.Windows.Forms.Label
	Public WithEvents fraDataOuput As System.Windows.Forms.GroupBox
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdExit As System.Windows.Forms.Button
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmHhydraulic))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtBVdlmw = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.fraProgram = New System.Windows.Forms.GroupBox
        Me.lblBVprogram = New System.Windows.Forms.Label
        Me.fraDataInput = New System.Windows.Forms.GroupBox
        Me.txtBVvm = New System.Windows.Forms.TextBox
        Me.txtBVs6 = New System.Windows.Forms.TextBox
        Me.txtBVdh = New System.Windows.Forms.TextBox
        Me.txtBVdp = New System.Windows.Forms.TextBox
        Me.txtBVdrc = New System.Windows.Forms.TextBox
        Me.txtBVprc = New System.Windows.Forms.TextBox
        Me.txtBVmw = New System.Windows.Forms.TextBox
        Me.txtBVq = New System.Windows.Forms.TextBox
        Me.txtBVs0 = New System.Windows.Forms.TextBox
        Me.txtBVs3 = New System.Windows.Forms.TextBox
        Me.lblBVvm = New System.Windows.Forms.Label
        Me.lblBVs6 = New System.Windows.Forms.Label
        Me.lblBVq = New System.Windows.Forms.Label
        Me.lblBVdp = New System.Windows.Forms.Label
        Me.lblBVdrc = New System.Windows.Forms.Label
        Me.lblBVprc = New System.Windows.Forms.Label
        Me.lblBVmw = New System.Windows.Forms.Label
        Me.lblBVdh = New System.Windows.Forms.Label
        Me.lblBVs0 = New System.Windows.Forms.Label
        Me.lblBVs3 = New System.Windows.Forms.Label
        Me.fraDataOuput = New System.Windows.Forms.GroupBox
        Me.txtBVtc = New System.Windows.Forms.TextBox
        Me.txtBVvcr = New System.Windows.Forms.TextBox
        Me.txtBVCa = New System.Windows.Forms.TextBox
        Me.txtBVQc = New System.Windows.Forms.TextBox
        Me.txtBVz = New System.Windows.Forms.TextBox
        Me.txtBVs = New System.Windows.Forms.TextBox
        Me.txtBVr = New System.Windows.Forms.TextBox
        Me.txtBVmj = New System.Windows.Forms.TextBox
        Me.txtBVuf = New System.Windows.Forms.TextBox
        Me.txtBVpv = New System.Windows.Forms.TextBox
        Me.txtBVk = New System.Windows.Forms.TextBox
        Me.txtBVn = New System.Windows.Forms.TextBox
        Me.txtBVhvs = New System.Windows.Forms.TextBox
        Me.txtBVvs = New System.Windows.Forms.TextBox
        Me.txtBVgvs = New System.Windows.Forms.TextBox
        Me.txtBVlc = New System.Windows.Forms.TextBox
        Me.lblBVdlmw = New System.Windows.Forms.Label
        Me.lblBVtc = New System.Windows.Forms.Label
        Me.lblBVvcr = New System.Windows.Forms.Label
        Me.lblBVCa = New System.Windows.Forms.Label
        Me.lblBVQc = New System.Windows.Forms.Label
        Me.lblBVz = New System.Windows.Forms.Label
        Me.lblBVs = New System.Windows.Forms.Label
        Me.lblBVr = New System.Windows.Forms.Label
        Me.lblBVmj = New System.Windows.Forms.Label
        Me.lblBVuf = New System.Windows.Forms.Label
        Me.lblBVpv = New System.Windows.Forms.Label
        Me.lblBVk = New System.Windows.Forms.Label
        Me.lblBVn = New System.Windows.Forms.Label
        Me.lblBVhvs = New System.Windows.Forms.Label
        Me.lblBVvs = New System.Windows.Forms.Label
        Me.lblBVgvs = New System.Windows.Forms.Label
        Me.lblBVlc = New System.Windows.Forms.Label
        Me.cmdok = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.fraProgram.SuspendLayout()
        Me.fraDataInput.SuspendLayout()
        Me.fraDataOuput.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtBVdlmw
        '
        Me.txtBVdlmw.AcceptsReturn = True
        Me.txtBVdlmw.AutoSize = False
        Me.txtBVdlmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdlmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdlmw.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdlmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdlmw.Location = New System.Drawing.Point(496, 248)
        Me.txtBVdlmw.MaxLength = 0
        Me.txtBVdlmw.Name = "txtBVdlmw"
        Me.txtBVdlmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdlmw.Size = New System.Drawing.Size(59, 20)
        Me.txtBVdlmw.TabIndex = 60
        Me.txtBVdlmw.Text = ""
        Me.txtBVdlmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtBVdlmw, "井内的钻井液，由于混入了钻头切削出来的岩屑，因而其实际密度与地面所测量出来的密度是不相等的。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(152, 460)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 61
        Me.PicLogo.TabStop = False
        '
        'fraProgram
        '
        Me.fraProgram.BackColor = System.Drawing.SystemColors.Control
        Me.fraProgram.Controls.Add(Me.lblBVprogram)
        Me.fraProgram.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraProgram.Location = New System.Drawing.Point(318, 304)
        Me.fraProgram.Name = "fraProgram"
        Me.fraProgram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraProgram.Size = New System.Drawing.Size(566, 216)
        Me.fraProgram.TabIndex = 55
        Me.fraProgram.TabStop = False
        Me.fraProgram.Text = "程序说明"
        '
        'lblBVprogram
        '
        Me.lblBVprogram.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVprogram.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblBVprogram.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVprogram.ForeColor = System.Drawing.Color.Blue
        Me.lblBVprogram.Location = New System.Drawing.Point(10, 17)
        Me.lblBVprogram.Name = "lblBVprogram"
        Me.lblBVprogram.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVprogram.Size = New System.Drawing.Size(546, 191)
        Me.lblBVprogram.TabIndex = 58
        Me.lblBVprogram.Text = "1.在整个钻井过程中要解决破岩、清岩、携岩和除岩四个问题。为此，在提高钻速的同时，还要兼顾井眼的稳定和净化。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2.在现场施工过程中，优选钻井液流变参数有三个" & _
        "约束条件，它们是环空携岩能力LC要大于等于0.5，环空岩屑浓度Ca要小于9%，环空流态稳定参数Z要要满足井眼稳定要求的统计值（胜利油田在''六五''攻关时利用统" & _
        "计方法得到的裸眼稳定参数Z值参考值是：0～1500m时，Z=6000±；1500～2300m时，Z=3000±；2300～3200m时，Z=2000±；）。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "3.该模型能根据现场钻井液的范氏粘度计读数、泥浆密度、泥浆泵排量、当前的机械钻速、钻屑直径等对所采用泥浆的流型参数、携岩能力、裸眼稳定参数、岩屑浓度等进行分" & _
        "析计算，从而为钻井工程技术人员的施工指导提供理论帮助。采用计算公式模型参见《钻井手册（甲方）》。"
        '
        'fraDataInput
        '
        Me.fraDataInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataInput.Controls.Add(Me.txtBVvm)
        Me.fraDataInput.Controls.Add(Me.txtBVs6)
        Me.fraDataInput.Controls.Add(Me.txtBVdh)
        Me.fraDataInput.Controls.Add(Me.txtBVdp)
        Me.fraDataInput.Controls.Add(Me.txtBVdrc)
        Me.fraDataInput.Controls.Add(Me.txtBVprc)
        Me.fraDataInput.Controls.Add(Me.txtBVmw)
        Me.fraDataInput.Controls.Add(Me.txtBVq)
        Me.fraDataInput.Controls.Add(Me.txtBVs0)
        Me.fraDataInput.Controls.Add(Me.txtBVs3)
        Me.fraDataInput.Controls.Add(Me.lblBVvm)
        Me.fraDataInput.Controls.Add(Me.lblBVs6)
        Me.fraDataInput.Controls.Add(Me.lblBVq)
        Me.fraDataInput.Controls.Add(Me.lblBVdp)
        Me.fraDataInput.Controls.Add(Me.lblBVdrc)
        Me.fraDataInput.Controls.Add(Me.lblBVprc)
        Me.fraDataInput.Controls.Add(Me.lblBVmw)
        Me.fraDataInput.Controls.Add(Me.lblBVdh)
        Me.fraDataInput.Controls.Add(Me.lblBVs0)
        Me.fraDataInput.Controls.Add(Me.lblBVs3)
        Me.fraDataInput.ForeColor = System.Drawing.Color.Red
        Me.fraDataInput.Location = New System.Drawing.Point(8, 8)
        Me.fraDataInput.Name = "fraDataInput"
        Me.fraDataInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataInput.Size = New System.Drawing.Size(302, 312)
        Me.fraDataInput.TabIndex = 22
        Me.fraDataInput.TabStop = False
        Me.fraDataInput.Text = "参数输入"
        '
        'txtBVvm
        '
        Me.txtBVvm.AcceptsReturn = True
        Me.txtBVvm.AutoSize = False
        Me.txtBVvm.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVvm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVvm.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVvm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVvm.Location = New System.Drawing.Point(233, 276)
        Me.txtBVvm.MaxLength = 0
        Me.txtBVvm.Name = "txtBVvm"
        Me.txtBVvm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVvm.Size = New System.Drawing.Size(59, 20)
        Me.txtBVvm.TabIndex = 9
        Me.txtBVvm.Text = "150"
        Me.txtBVvm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVs6
        '
        Me.txtBVs6.AcceptsReturn = True
        Me.txtBVs6.AutoSize = False
        Me.txtBVs6.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVs6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVs6.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVs6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVs6.Location = New System.Drawing.Point(233, 248)
        Me.txtBVs6.MaxLength = 0
        Me.txtBVs6.Name = "txtBVs6"
        Me.txtBVs6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVs6.Size = New System.Drawing.Size(59, 20)
        Me.txtBVs6.TabIndex = 8
        Me.txtBVs6.Text = "6.256"
        Me.txtBVs6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdh
        '
        Me.txtBVdh.AcceptsReturn = True
        Me.txtBVdh.AutoSize = False
        Me.txtBVdh.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdh.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdh.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdh.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdh.Location = New System.Drawing.Point(233, 24)
        Me.txtBVdh.MaxLength = 0
        Me.txtBVdh.Name = "txtBVdh"
        Me.txtBVdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdh.Size = New System.Drawing.Size(59, 20)
        Me.txtBVdh.TabIndex = 0
        Me.txtBVdh.Text = "215.9"
        Me.txtBVdh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdp
        '
        Me.txtBVdp.AcceptsReturn = True
        Me.txtBVdp.AutoSize = False
        Me.txtBVdp.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdp.Location = New System.Drawing.Point(233, 52)
        Me.txtBVdp.MaxLength = 0
        Me.txtBVdp.Name = "txtBVdp"
        Me.txtBVdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdp.Size = New System.Drawing.Size(59, 20)
        Me.txtBVdp.TabIndex = 1
        Me.txtBVdp.Text = "127"
        Me.txtBVdp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVdrc
        '
        Me.txtBVdrc.AcceptsReturn = True
        Me.txtBVdrc.AutoSize = False
        Me.txtBVdrc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVdrc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVdrc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVdrc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVdrc.Location = New System.Drawing.Point(233, 80)
        Me.txtBVdrc.MaxLength = 0
        Me.txtBVdrc.Name = "txtBVdrc"
        Me.txtBVdrc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVdrc.Size = New System.Drawing.Size(59, 20)
        Me.txtBVdrc.TabIndex = 2
        Me.txtBVdrc.Text = "10"
        Me.txtBVdrc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVprc
        '
        Me.txtBVprc.AcceptsReturn = True
        Me.txtBVprc.AutoSize = False
        Me.txtBVprc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVprc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVprc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVprc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVprc.Location = New System.Drawing.Point(233, 108)
        Me.txtBVprc.MaxLength = 0
        Me.txtBVprc.Name = "txtBVprc"
        Me.txtBVprc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVprc.Size = New System.Drawing.Size(59, 20)
        Me.txtBVprc.TabIndex = 3
        Me.txtBVprc.Text = "2.52"
        Me.txtBVprc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVmw
        '
        Me.txtBVmw.AcceptsReturn = True
        Me.txtBVmw.AutoSize = False
        Me.txtBVmw.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVmw.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVmw.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVmw.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVmw.Location = New System.Drawing.Point(233, 136)
        Me.txtBVmw.MaxLength = 0
        Me.txtBVmw.Name = "txtBVmw"
        Me.txtBVmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVmw.Size = New System.Drawing.Size(59, 20)
        Me.txtBVmw.TabIndex = 4
        Me.txtBVmw.Text = "1.10"
        Me.txtBVmw.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVq
        '
        Me.txtBVq.AcceptsReturn = True
        Me.txtBVq.AutoSize = False
        Me.txtBVq.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVq.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVq.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVq.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVq.Location = New System.Drawing.Point(233, 164)
        Me.txtBVq.MaxLength = 0
        Me.txtBVq.Name = "txtBVq"
        Me.txtBVq.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVq.Size = New System.Drawing.Size(59, 20)
        Me.txtBVq.TabIndex = 5
        Me.txtBVq.Text = "35"
        Me.txtBVq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVs0
        '
        Me.txtBVs0.AcceptsReturn = True
        Me.txtBVs0.AutoSize = False
        Me.txtBVs0.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVs0.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVs0.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVs0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVs0.Location = New System.Drawing.Point(233, 192)
        Me.txtBVs0.MaxLength = 0
        Me.txtBVs0.Name = "txtBVs0"
        Me.txtBVs0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVs0.Size = New System.Drawing.Size(59, 20)
        Me.txtBVs0.TabIndex = 6
        Me.txtBVs0.Text = "0.5"
        Me.txtBVs0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVs3
        '
        Me.txtBVs3.AcceptsReturn = True
        Me.txtBVs3.AutoSize = False
        Me.txtBVs3.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVs3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVs3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVs3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVs3.Location = New System.Drawing.Point(233, 220)
        Me.txtBVs3.MaxLength = 0
        Me.txtBVs3.Name = "txtBVs3"
        Me.txtBVs3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVs3.Size = New System.Drawing.Size(59, 20)
        Me.txtBVs3.TabIndex = 7
        Me.txtBVs3.Text = "4.569"
        Me.txtBVs3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBVvm
        '
        Me.lblBVvm.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVvm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVvm.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVvm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVvm.Location = New System.Drawing.Point(8, 276)
        Me.lblBVvm.Name = "lblBVvm"
        Me.lblBVvm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVvm.Size = New System.Drawing.Size(215, 20)
        Me.lblBVvm.TabIndex = 32
        Me.lblBVvm.Text = "钻速(m/h)"
        '
        'lblBVs6
        '
        Me.lblBVs6.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVs6.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVs6.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVs6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVs6.Location = New System.Drawing.Point(8, 248)
        Me.lblBVs6.Name = "lblBVs6"
        Me.lblBVs6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVs6.Size = New System.Drawing.Size(215, 20)
        Me.lblBVs6.TabIndex = 31
        Me.lblBVs6.Text = "600转读数(Fann)"
        '
        'lblBVq
        '
        Me.lblBVq.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVq.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVq.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVq.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVq.Location = New System.Drawing.Point(8, 164)
        Me.lblBVq.Name = "lblBVq"
        Me.lblBVq.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVq.Size = New System.Drawing.Size(215, 20)
        Me.lblBVq.TabIndex = 30
        Me.lblBVq.Text = "排量(l/s)"
        '
        'lblBVdp
        '
        Me.lblBVdp.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdp.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdp.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdp.Location = New System.Drawing.Point(8, 52)
        Me.lblBVdp.Name = "lblBVdp"
        Me.lblBVdp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdp.Size = New System.Drawing.Size(215, 20)
        Me.lblBVdp.TabIndex = 29
        Me.lblBVdp.Text = "钻柱外径(mm)"
        '
        'lblBVdrc
        '
        Me.lblBVdrc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdrc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdrc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdrc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdrc.Location = New System.Drawing.Point(8, 80)
        Me.lblBVdrc.Name = "lblBVdrc"
        Me.lblBVdrc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdrc.Size = New System.Drawing.Size(215, 20)
        Me.lblBVdrc.TabIndex = 28
        Me.lblBVdrc.Text = "岩屑颗粒直径(mm)"
        '
        'lblBVprc
        '
        Me.lblBVprc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVprc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVprc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVprc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVprc.Location = New System.Drawing.Point(8, 108)
        Me.lblBVprc.Name = "lblBVprc"
        Me.lblBVprc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVprc.Size = New System.Drawing.Size(215, 20)
        Me.lblBVprc.TabIndex = 27
        Me.lblBVprc.Text = "岩屑密度(g/cm^3)"
        '
        'lblBVmw
        '
        Me.lblBVmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVmw.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVmw.Location = New System.Drawing.Point(8, 136)
        Me.lblBVmw.Name = "lblBVmw"
        Me.lblBVmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVmw.Size = New System.Drawing.Size(215, 20)
        Me.lblBVmw.TabIndex = 26
        Me.lblBVmw.Text = "钻井液密度(g/cm^3)"
        '
        'lblBVdh
        '
        Me.lblBVdh.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdh.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdh.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdh.Location = New System.Drawing.Point(8, 24)
        Me.lblBVdh.Name = "lblBVdh"
        Me.lblBVdh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdh.Size = New System.Drawing.Size(215, 20)
        Me.lblBVdh.TabIndex = 25
        Me.lblBVdh.Text = "井眼直径(mm)"
        '
        'lblBVs0
        '
        Me.lblBVs0.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVs0.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVs0.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVs0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVs0.Location = New System.Drawing.Point(8, 192)
        Me.lblBVs0.Name = "lblBVs0"
        Me.lblBVs0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVs0.Size = New System.Drawing.Size(215, 20)
        Me.lblBVs0.TabIndex = 24
        Me.lblBVs0.Text = "3转读数(Fann)"
        '
        'lblBVs3
        '
        Me.lblBVs3.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVs3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVs3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVs3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVs3.Location = New System.Drawing.Point(8, 220)
        Me.lblBVs3.Name = "lblBVs3"
        Me.lblBVs3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVs3.Size = New System.Drawing.Size(215, 20)
        Me.lblBVs3.TabIndex = 23
        Me.lblBVs3.Text = "300转读数(Fann)"
        '
        'fraDataOuput
        '
        Me.fraDataOuput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataOuput.Controls.Add(Me.txtBVdlmw)
        Me.fraDataOuput.Controls.Add(Me.txtBVtc)
        Me.fraDataOuput.Controls.Add(Me.txtBVvcr)
        Me.fraDataOuput.Controls.Add(Me.txtBVCa)
        Me.fraDataOuput.Controls.Add(Me.txtBVQc)
        Me.fraDataOuput.Controls.Add(Me.txtBVz)
        Me.fraDataOuput.Controls.Add(Me.txtBVs)
        Me.fraDataOuput.Controls.Add(Me.txtBVr)
        Me.fraDataOuput.Controls.Add(Me.txtBVmj)
        Me.fraDataOuput.Controls.Add(Me.txtBVuf)
        Me.fraDataOuput.Controls.Add(Me.txtBVpv)
        Me.fraDataOuput.Controls.Add(Me.txtBVk)
        Me.fraDataOuput.Controls.Add(Me.txtBVn)
        Me.fraDataOuput.Controls.Add(Me.txtBVhvs)
        Me.fraDataOuput.Controls.Add(Me.txtBVvs)
        Me.fraDataOuput.Controls.Add(Me.txtBVgvs)
        Me.fraDataOuput.Controls.Add(Me.txtBVlc)
        Me.fraDataOuput.Controls.Add(Me.lblBVdlmw)
        Me.fraDataOuput.Controls.Add(Me.lblBVtc)
        Me.fraDataOuput.Controls.Add(Me.lblBVvcr)
        Me.fraDataOuput.Controls.Add(Me.lblBVCa)
        Me.fraDataOuput.Controls.Add(Me.lblBVQc)
        Me.fraDataOuput.Controls.Add(Me.lblBVz)
        Me.fraDataOuput.Controls.Add(Me.lblBVs)
        Me.fraDataOuput.Controls.Add(Me.lblBVr)
        Me.fraDataOuput.Controls.Add(Me.lblBVmj)
        Me.fraDataOuput.Controls.Add(Me.lblBVuf)
        Me.fraDataOuput.Controls.Add(Me.lblBVpv)
        Me.fraDataOuput.Controls.Add(Me.lblBVk)
        Me.fraDataOuput.Controls.Add(Me.lblBVn)
        Me.fraDataOuput.Controls.Add(Me.lblBVhvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVgvs)
        Me.fraDataOuput.Controls.Add(Me.lblBVlc)
        Me.fraDataOuput.ForeColor = System.Drawing.Color.Red
        Me.fraDataOuput.Location = New System.Drawing.Point(318, 8)
        Me.fraDataOuput.Name = "fraDataOuput"
        Me.fraDataOuput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataOuput.Size = New System.Drawing.Size(566, 288)
        Me.fraDataOuput.TabIndex = 12
        Me.fraDataOuput.TabStop = False
        Me.fraDataOuput.Text = "计算结果"
        '
        'txtBVtc
        '
        Me.txtBVtc.AcceptsReturn = True
        Me.txtBVtc.AutoSize = False
        Me.txtBVtc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVtc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVtc.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVtc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVtc.Location = New System.Drawing.Point(496, 24)
        Me.txtBVtc.MaxLength = 0
        Me.txtBVtc.Name = "txtBVtc"
        Me.txtBVtc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVtc.Size = New System.Drawing.Size(59, 20)
        Me.txtBVtc.TabIndex = 57
        Me.txtBVtc.Text = ""
        Me.txtBVtc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVvcr
        '
        Me.txtBVvcr.AcceptsReturn = True
        Me.txtBVvcr.AutoSize = False
        Me.txtBVvcr.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVvcr.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVvcr.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVvcr.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVvcr.Location = New System.Drawing.Point(496, 220)
        Me.txtBVvcr.MaxLength = 0
        Me.txtBVvcr.Name = "txtBVvcr"
        Me.txtBVvcr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVvcr.Size = New System.Drawing.Size(59, 20)
        Me.txtBVvcr.TabIndex = 54
        Me.txtBVvcr.Text = ""
        Me.txtBVvcr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVCa
        '
        Me.txtBVCa.AcceptsReturn = True
        Me.txtBVCa.AutoSize = False
        Me.txtBVCa.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVCa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVCa.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVCa.ForeColor = System.Drawing.Color.Red
        Me.txtBVCa.Location = New System.Drawing.Point(496, 192)
        Me.txtBVCa.MaxLength = 0
        Me.txtBVCa.Name = "txtBVCa"
        Me.txtBVCa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVCa.Size = New System.Drawing.Size(59, 20)
        Me.txtBVCa.TabIndex = 52
        Me.txtBVCa.Text = ""
        Me.txtBVCa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVQc
        '
        Me.txtBVQc.AcceptsReturn = True
        Me.txtBVQc.AutoSize = False
        Me.txtBVQc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVQc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVQc.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVQc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVQc.Location = New System.Drawing.Point(496, 142)
        Me.txtBVQc.MaxLength = 0
        Me.txtBVQc.Name = "txtBVQc"
        Me.txtBVQc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVQc.Size = New System.Drawing.Size(59, 20)
        Me.txtBVQc.TabIndex = 51
        Me.txtBVQc.Text = ""
        Me.txtBVQc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVz
        '
        Me.txtBVz.AcceptsReturn = True
        Me.txtBVz.AutoSize = False
        Me.txtBVz.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVz.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVz.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVz.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVz.Location = New System.Drawing.Point(496, 108)
        Me.txtBVz.MaxLength = 0
        Me.txtBVz.Name = "txtBVz"
        Me.txtBVz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVz.Size = New System.Drawing.Size(59, 20)
        Me.txtBVz.TabIndex = 50
        Me.txtBVz.Text = ""
        Me.txtBVz.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVs
        '
        Me.txtBVs.AcceptsReturn = True
        Me.txtBVs.AutoSize = False
        Me.txtBVs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVs.Location = New System.Drawing.Point(496, 80)
        Me.txtBVs.MaxLength = 0
        Me.txtBVs.Name = "txtBVs"
        Me.txtBVs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVs.Size = New System.Drawing.Size(59, 20)
        Me.txtBVs.TabIndex = 46
        Me.txtBVs.Text = ""
        Me.txtBVs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVr
        '
        Me.txtBVr.AcceptsReturn = True
        Me.txtBVr.AutoSize = False
        Me.txtBVr.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVr.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVr.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVr.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVr.Location = New System.Drawing.Point(496, 50)
        Me.txtBVr.MaxLength = 0
        Me.txtBVr.Name = "txtBVr"
        Me.txtBVr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVr.Size = New System.Drawing.Size(59, 20)
        Me.txtBVr.TabIndex = 44
        Me.txtBVr.Text = ""
        Me.txtBVr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVmj
        '
        Me.txtBVmj.AcceptsReturn = True
        Me.txtBVmj.AutoSize = False
        Me.txtBVmj.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVmj.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVmj.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVmj.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVmj.Location = New System.Drawing.Point(202, 247)
        Me.txtBVmj.MaxLength = 0
        Me.txtBVmj.Name = "txtBVmj"
        Me.txtBVmj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVmj.Size = New System.Drawing.Size(59, 20)
        Me.txtBVmj.TabIndex = 42
        Me.txtBVmj.Text = ""
        Me.txtBVmj.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVuf
        '
        Me.txtBVuf.AcceptsReturn = True
        Me.txtBVuf.AutoSize = False
        Me.txtBVuf.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVuf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVuf.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVuf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVuf.Location = New System.Drawing.Point(202, 219)
        Me.txtBVuf.MaxLength = 0
        Me.txtBVuf.Name = "txtBVuf"
        Me.txtBVuf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVuf.Size = New System.Drawing.Size(59, 20)
        Me.txtBVuf.TabIndex = 41
        Me.txtBVuf.Text = ""
        Me.txtBVuf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVpv
        '
        Me.txtBVpv.AcceptsReturn = True
        Me.txtBVpv.AutoSize = False
        Me.txtBVpv.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVpv.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVpv.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVpv.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVpv.Location = New System.Drawing.Point(202, 191)
        Me.txtBVpv.MaxLength = 0
        Me.txtBVpv.Name = "txtBVpv"
        Me.txtBVpv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVpv.Size = New System.Drawing.Size(59, 20)
        Me.txtBVpv.TabIndex = 40
        Me.txtBVpv.Text = ""
        Me.txtBVpv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVk
        '
        Me.txtBVk.AcceptsReturn = True
        Me.txtBVk.AutoSize = False
        Me.txtBVk.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVk.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVk.Location = New System.Drawing.Point(202, 163)
        Me.txtBVk.MaxLength = 0
        Me.txtBVk.Name = "txtBVk"
        Me.txtBVk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVk.Size = New System.Drawing.Size(59, 20)
        Me.txtBVk.TabIndex = 39
        Me.txtBVk.Text = ""
        Me.txtBVk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVn
        '
        Me.txtBVn.AcceptsReturn = True
        Me.txtBVn.AutoSize = False
        Me.txtBVn.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVn.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVn.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVn.Location = New System.Drawing.Point(202, 135)
        Me.txtBVn.MaxLength = 0
        Me.txtBVn.Name = "txtBVn"
        Me.txtBVn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVn.Size = New System.Drawing.Size(59, 20)
        Me.txtBVn.TabIndex = 38
        Me.txtBVn.Text = ""
        Me.txtBVn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVhvs
        '
        Me.txtBVhvs.AcceptsReturn = True
        Me.txtBVhvs.AutoSize = False
        Me.txtBVhvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVhvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVhvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVhvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVhvs.Location = New System.Drawing.Point(202, 24)
        Me.txtBVhvs.MaxLength = 0
        Me.txtBVhvs.Name = "txtBVhvs"
        Me.txtBVhvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVhvs.Size = New System.Drawing.Size(59, 20)
        Me.txtBVhvs.TabIndex = 17
        Me.txtBVhvs.Text = ""
        Me.txtBVhvs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVvs
        '
        Me.txtBVvs.AcceptsReturn = True
        Me.txtBVvs.AutoSize = False
        Me.txtBVvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVvs.Location = New System.Drawing.Point(202, 51)
        Me.txtBVvs.MaxLength = 0
        Me.txtBVvs.Name = "txtBVvs"
        Me.txtBVvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVvs.Size = New System.Drawing.Size(59, 20)
        Me.txtBVvs.TabIndex = 16
        Me.txtBVvs.Text = ""
        Me.txtBVvs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVgvs
        '
        Me.txtBVgvs.AcceptsReturn = True
        Me.txtBVgvs.AutoSize = False
        Me.txtBVgvs.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVgvs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVgvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVgvs.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBVgvs.Location = New System.Drawing.Point(202, 79)
        Me.txtBVgvs.MaxLength = 0
        Me.txtBVgvs.Name = "txtBVgvs"
        Me.txtBVgvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVgvs.Size = New System.Drawing.Size(59, 20)
        Me.txtBVgvs.TabIndex = 15
        Me.txtBVgvs.Text = ""
        Me.txtBVgvs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBVlc
        '
        Me.txtBVlc.AcceptsReturn = True
        Me.txtBVlc.AutoSize = False
        Me.txtBVlc.BackColor = System.Drawing.SystemColors.Window
        Me.txtBVlc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBVlc.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtBVlc.ForeColor = System.Drawing.Color.Red
        Me.txtBVlc.Location = New System.Drawing.Point(202, 107)
        Me.txtBVlc.MaxLength = 0
        Me.txtBVlc.Name = "txtBVlc"
        Me.txtBVlc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBVlc.Size = New System.Drawing.Size(59, 20)
        Me.txtBVlc.TabIndex = 14
        Me.txtBVlc.Text = ""
        Me.txtBVlc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBVdlmw
        '
        Me.lblBVdlmw.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVdlmw.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVdlmw.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVdlmw.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVdlmw.Location = New System.Drawing.Point(264, 248)
        Me.lblBVdlmw.Name = "lblBVdlmw"
        Me.lblBVdlmw.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVdlmw.Size = New System.Drawing.Size(224, 20)
        Me.lblBVdlmw.TabIndex = 59
        Me.lblBVdlmw.Text = "环空钻井液有效密度(g/cm^3)"
        Me.lblBVdlmw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVtc
        '
        Me.lblBVtc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVtc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVtc.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVtc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVtc.Location = New System.Drawing.Point(264, 24)
        Me.lblBVtc.Name = "lblBVtc"
        Me.lblBVtc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVtc.Size = New System.Drawing.Size(224, 20)
        Me.lblBVtc.TabIndex = 56
        Me.lblBVtc.Text = "Casson屈服值τc(Pa)"
        Me.lblBVtc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVvcr
        '
        Me.lblBVvcr.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVvcr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVvcr.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVvcr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVvcr.Location = New System.Drawing.Point(264, 220)
        Me.lblBVvcr.Name = "lblBVvcr"
        Me.lblBVvcr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVvcr.Size = New System.Drawing.Size(224, 20)
        Me.lblBVvcr.TabIndex = 53
        Me.lblBVvcr.Text = "环空临界流速(m/s)"
        Me.lblBVvcr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVCa
        '
        Me.lblBVCa.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVCa.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVCa.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVCa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVCa.Location = New System.Drawing.Point(264, 192)
        Me.lblBVCa.Name = "lblBVCa"
        Me.lblBVCa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVCa.Size = New System.Drawing.Size(224, 20)
        Me.lblBVCa.TabIndex = 49
        Me.lblBVCa.Text = "环空岩屑浓度 %"
        Me.lblBVCa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVQc
        '
        Me.lblBVQc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVQc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVQc.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVQc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVQc.Location = New System.Drawing.Point(264, 136)
        Me.lblBVQc.Name = "lblBVQc"
        Me.lblBVQc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVQc.Size = New System.Drawing.Size(224, 32)
        Me.lblBVQc.TabIndex = 48
        Me.lblBVQc.Text = "每秒产生钻屑体积l/s"
        Me.lblBVQc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVz
        '
        Me.lblBVz.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVz.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVz.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVz.ForeColor = System.Drawing.Color.Red
        Me.lblBVz.Location = New System.Drawing.Point(264, 108)
        Me.lblBVz.Name = "lblBVz"
        Me.lblBVz.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVz.Size = New System.Drawing.Size(224, 20)
        Me.lblBVz.TabIndex = 47
        Me.lblBVz.Text = "稳定参数Z"
        Me.lblBVz.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBVs
        '
        Me.lblBVs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVs.Location = New System.Drawing.Point(264, 80)
        Me.lblBVs.Name = "lblBVs"
        Me.lblBVs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVs.Size = New System.Drawing.Size(224, 20)
        Me.lblBVs.TabIndex = 45
        Me.lblBVs.Text = "剪切应力(Pa)"
        Me.lblBVs.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBVr
        '
        Me.lblBVr.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVr.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVr.Location = New System.Drawing.Point(264, 50)
        Me.lblBVr.Name = "lblBVr"
        Me.lblBVr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVr.Size = New System.Drawing.Size(224, 20)
        Me.lblBVr.TabIndex = 43
        Me.lblBVr.Text = "剪切速率(s^-1)"
        Me.lblBVr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBVmj
        '
        Me.lblBVmj.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVmj.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVmj.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVmj.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVmj.Location = New System.Drawing.Point(8, 248)
        Me.lblBVmj.Name = "lblBVmj"
        Me.lblBVmj.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVmj.Size = New System.Drawing.Size(194, 18)
        Me.lblBVmj.TabIndex = 37
        Me.lblBVmj.Text = "钻头水眼粘度η∞(mPa.s)"
        Me.lblBVmj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVuf
        '
        Me.lblBVuf.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVuf.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVuf.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVuf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVuf.Location = New System.Drawing.Point(8, 220)
        Me.lblBVuf.Name = "lblBVuf"
        Me.lblBVuf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVuf.Size = New System.Drawing.Size(194, 18)
        Me.lblBVuf.TabIndex = 36
        Me.lblBVuf.Text = "视粘度(mPa.s)"
        Me.lblBVuf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVpv
        '
        Me.lblBVpv.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVpv.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVpv.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVpv.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVpv.Location = New System.Drawing.Point(8, 192)
        Me.lblBVpv.Name = "lblBVpv"
        Me.lblBVpv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVpv.Size = New System.Drawing.Size(194, 18)
        Me.lblBVpv.TabIndex = 35
        Me.lblBVpv.Text = "塑性粘度(mPa.s)"
        Me.lblBVpv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVk
        '
        Me.lblBVk.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVk.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVk.Location = New System.Drawing.Point(8, 164)
        Me.lblBVk.Name = "lblBVk"
        Me.lblBVk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVk.Size = New System.Drawing.Size(194, 18)
        Me.lblBVk.TabIndex = 34
        Me.lblBVk.Text = "稠度系数K (Pa.s^n)"
        Me.lblBVk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVn
        '
        Me.lblBVn.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVn.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVn.Location = New System.Drawing.Point(8, 136)
        Me.lblBVn.Name = "lblBVn"
        Me.lblBVn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVn.Size = New System.Drawing.Size(194, 18)
        Me.lblBVn.TabIndex = 33
        Me.lblBVn.Text = "流形指数n"
        Me.lblBVn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVhvs
        '
        Me.lblBVhvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVhvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVhvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVhvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVhvs.Location = New System.Drawing.Point(8, 24)
        Me.lblBVhvs.Name = "lblBVhvs"
        Me.lblBVhvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVhvs.Size = New System.Drawing.Size(194, 20)
        Me.lblBVhvs.TabIndex = 21
        Me.lblBVhvs.Text = "环空返速(m/s)"
        Me.lblBVhvs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVvs
        '
        Me.lblBVvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVvs.Location = New System.Drawing.Point(8, 52)
        Me.lblBVvs.Name = "lblBVvs"
        Me.lblBVvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVvs.Size = New System.Drawing.Size(194, 18)
        Me.lblBVvs.TabIndex = 20
        Me.lblBVvs.Text = "岩屑颗粒滑落速度(m/s)"
        Me.lblBVvs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVgvs
        '
        Me.lblBVgvs.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVgvs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVgvs.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVgvs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVgvs.Location = New System.Drawing.Point(8, 80)
        Me.lblBVgvs.Name = "lblBVgvs"
        Me.lblBVgvs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVgvs.Size = New System.Drawing.Size(194, 18)
        Me.lblBVgvs.TabIndex = 19
        Me.lblBVgvs.Text = "举升速度(m/s)"
        Me.lblBVgvs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBVlc
        '
        Me.lblBVlc.BackColor = System.Drawing.SystemColors.Control
        Me.lblBVlc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBVlc.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblBVlc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBVlc.Location = New System.Drawing.Point(8, 108)
        Me.lblBVlc.Name = "lblBVlc"
        Me.lblBVlc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBVlc.Size = New System.Drawing.Size(194, 18)
        Me.lblBVlc.TabIndex = 18
        Me.lblBVlc.Text = "环空净化能力(无因次量)"
        Me.lblBVlc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(40, 395)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 10
        Me.cmdok.Text = "Run"
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(40, 423)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(80, 20)
        Me.cmdclear.TabIndex = 11
        Me.cmdclear.Text = "Clean"
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(40, 451)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 13
        Me.cmdExit.Text = "Exit"
        '
        'frmHhydraulic
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 586)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.fraProgram)
        Me.Controls.Add(Me.fraDataInput)
        Me.Controls.Add(Me.fraDataOuput)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdExit)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmHhydraulic"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "环空岩屑浓度分析"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.fraProgram.ResumeLayout(False)
        Me.fraDataInput.ResumeLayout(False)
        Me.fraDataOuput.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmHhydraulic
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmHhydraulic
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmHhydraulic()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'在整个钻井过程中要解决破岩、清岩、携岩和除岩四个问题。为此，在提高钻速的同时，还要兼顾井眼的稳定和净化。
	'在现场施工过程中，优选钻井液流变参数有三个约束条件，它们是环空携岩能力LC要大于等于0.5，环空岩屑浓度Ca要
	'小于9%，环空流态稳定参数Z要要满足井眼稳定要求的统计值（胜利油田在"六五"攻关时利用统计方法得到的裸眼稳定
	'参数Z值参考值是：0～1500m时，Z=6000±；1500～2300m时，Z=3000±；2300～3200m时，Z=2000±；）。该模型能根
	'据现场钻井液的范氏粘度计读数、泥浆密度、泥浆泵排量、当前的机械钻速、钻屑直径等对所采用泥浆的流型参数、携
	'岩能力、裸眼稳定参数、岩屑浓度等进行分析计算，从而为钻井工程技术人员的施工指导提供理论帮助。采用计算公式
	'模型参见《钻井手册（甲方）》。
	
	
	
	'本模块编程人：王朝，完成时间2006年8月6日23:51于青海省海西洲大柴旦镇红山参2井。加入纠错功能。
	'环空水力参数分析计算评价
	
	
	Dim dh As Single
	Dim dp As Single
	Dim Q As Single
	Dim drc As Single
	Dim prc As Single
	Dim mw As Single
	Dim s0 As Single
	Dim s3 As Single
	Dim s6 As Single
	Dim Vm As Single
	
	
	Dim upv As Single
	Dim mj As Single
	Dim tc As Single
	Dim av As Single
	Dim gvs As Single
	Dim lc As Single
	Dim N As Single
	Dim K As Single
	Dim r As Single
	Dim s As Single
	Dim uf As Single
	Dim vcr As Single
	Dim Ca As Single
	Dim Ce As Single
	Dim Qc As Single
	Dim Vs As Single
	Dim z As Single
	Dim MWc As Single
	
	'2012年3月29日填加的屏幕截图----------
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	'*******************************************
	
    '测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		cmdclear.Visible = False '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MHydraulic.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MHydraulic.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        cmdclear.Visible = True '
        cmdExit.Visible = True
        cmdok.Visible = True

	End Sub
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		
		Me.Close()
		
	End Sub
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		dh = Val(txtBVdh.Text)
		If txtBVdh.Text = "" Then
			MsgBox("Please check that you have entered 'Hole Diameter'data!", MsgBoxStyle.Information, "Annulus Solid Content")
			txtBVdh.Text = ""
			Exit Sub '纠错功能解决。2006年8月8日王朝
		End If
		If dh = 0 Then
			MsgBox("Please check that you have entered 'Hole Diameter'data!", MsgBoxStyle.Information, "Annulus Solid Content")
			txtBVdh.Text = ""
			Exit Sub
		End If
		
		dp = Val(txtBVdp.Text)
		If dp >= dh Then
			MsgBox("Please check that you have entered 'Drillstring OD'data!", MsgBoxStyle.Information, "Annulus Solid Content")
			txtBVdp.Text = ""
			Exit Sub
		End If
		If dp <= 0 Then
			MsgBox("Please check that you have entered 'Drillstring OD'data!", MsgBoxStyle.Information, "Annulus Solid Content")
			txtBVdp.Text = ""
			Exit Sub
		End If
		
		drc = Val(txtBVdrc.Text)
		mw = Val(txtBVmw.Text)
		prc = Val(txtBVprc.Text)
		Q = Val(txtBVq.Text)
		s0 = Val(txtBVs0.Text)
		s3 = Val(txtBVs3.Text)
		s6 = Val(txtBVs6.Text)
		Vm = Val(txtBVvm.Text)
		
		upv = s6 - s3
		mj = (2.4142 * (s6 ^ 0.5 - s3 ^ 0.5)) ^ 2
		tc = (1.671 * ((2 * s3) ^ 0.5 - s6 ^ 0.5)) ^ 2
		'mt = (77.172 * (s3 ^ 0.5 - 0.707 * s6 ^ 0.5)) ^ 2
		
		av = (1273 * Q) / (dh ^ 2 - dp ^ 2)
		

        N = 3.32 * Math.Log10((s6 - s0) / (s3 - s0))
		K = 0.4788 * (s3 - s0) / (511 ^ N)
		
		
		'r = (471.46 * av) / (dh - dp)          'dh/dp的单位为英寸的公式
		r = (1198.88 * av) / (dh - dp)
		s = s0 + K * (r ^ N)
		uf = (514.9 * (N ^ 0.119) * s) / r
		Vs = (0.071 * drc * (prc - mw) ^ 0.667) / ((mw * uf) ^ 0.33)
		vcr = 0.00508 * ((20626 * N ^ 0.387 * K * 2.54 ^ N) / (mw * ((dh / 10) - (dp / 10)) ^ N)) ^ (1 / (2 - N)) '公式中dh单位为厘米，实际输入为毫米，已修改为厘米单位
		'vcr = 0.00508 * ((2.04 * 10 ^ 4 * n ^ 0.387 * k / mw) * (25.4 / (dh - dp)) ^ n) ^ (1 / (2 - n))             '公式中dh/dp单位为mm
		'z = 808 * (av / vcr) ^ (2 - n)
		z = 1517.83 * ((dh / 10 - dp / 10) ^ N * av ^ (2 - N) * mw) / ((500 ^ N) * (N ^ 0.387) * K) 'z值计算结果相同
		
		'Qc = ((3.1416 * dh ^ 2) / 144) * Vm * (1 - 0.25) * (10 ^ -5)                           '『钻井工程技术手册』P820中公式，0.25为岩石孔隙度取值。
		Qc = ((3.1416 * dh ^ 2) / 144) * Vm * (10 ^ -5)
		Ce = Qc / (Qc + Q)
		Ca = ((av / Vs) * Ce) / ((av / Vs - 1) + Ce)
		MWc = (prc * (Ca / 100)) + (mw * (1 - Ca / 100))
		
		gvs = av - Vs
		lc = 1 - Vs / av
		
		txtBVhvs.Text = VB6.Format(av, "0.00")
		txtBVvs.Text = VB6.Format(Vs, "0.00")
		txtBVgvs.Text = VB6.Format(gvs, "0.00")
		txtBVlc.Text = VB6.Format(lc, "0.00")
		
		txtBVn.Text = VB6.Format(N, "0.00")
		txtBVk.Text = VB6.Format(K, "0.0000")
		txtBVpv.Text = VB6.Format(upv, "0.00")
		txtBVmj.Text = VB6.Format(mj, "0.00")
		txtBVtc.Text = VB6.Format(tc, "0.00")
		txtBVr.Text = VB6.Format(r, "0.00")
		txtBVs.Text = VB6.Format(s, "0.00")
		txtBVuf.Text = VB6.Format(uf, "0.00")
		txtBVz.Text = VB6.Format(z, "0.00")
		txtBVQc.Text = VB6.Format(Qc, "0.00")
		txtBVCa.Text = VB6.Format(Ca, "0.00%") '显示数据为百分数
		txtBVvcr.Text = VB6.Format(vcr, "0.00")
		txtBVdlmw.Text = VB6.Format(MWc, "0.000")
		
		If Ca <= 0.09 Then
            MsgBox("环空净化合格！", MsgBoxStyle.Information, "环空岩屑浓度分析")
		Else
            MsgBox("环空净化失败！请你优化工程参数！", MsgBoxStyle.Information, "环空岩屑浓度分析")
		End If
	End Sub
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		txtBVdh.Text = ""
		txtBVdp.Text = ""
		txtBVdrc.Text = ""
		txtBVmw.Text = ""
		txtBVprc.Text = ""
		txtBVq.Text = ""
		txtBVs0.Text = ""
		txtBVs3.Text = ""
		txtBVs6.Text = ""
		txtBVvm.Text = ""
		txtBVhvs.Text = ""
		txtBVvs.Text = ""
		txtBVgvs.Text = ""
		txtBVlc.Text = ""
		txtBVn.Text = "" '使用该方法清除数据，可能存在bug隐患。
		txtBVk.Text = ""
		txtBVpv.Text = ""
		txtBVmj.Text = ""
		txtBVtc.Text = ""
		txtBVr.Text = ""
		txtBVs.Text = ""
		txtBVuf.Text = ""
		txtBVz.Text = ""
		txtBVQc.Text = ""
		txtBVCa.Text = ""
		txtBVvcr.Text = ""
		
	End Sub
	
	Private Sub frmHhydraulic_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************

        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
		Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdh_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdh.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdh_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdh.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dh = Val(txtBVdh.Text)
		
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdp_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdp.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdp_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdp.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		dp = Val(txtBVdp.Text)
		
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVdrc_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVdrc.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVdrc_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVdrc.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		drc = Val(txtBVdrc.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVmw_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVmw.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVmw_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVmw.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		mw = Val(txtBVmw.Text)
		If mw <= 0.99 Then
			MsgBox("Please check that you have entered datas!", MsgBoxStyle.Information)
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVprc_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVprc.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVprc_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVprc.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		prc = Val(txtBVprc.Text)
		If prc < 1 Or prc > 3 Then
			MsgBox("Please check that you have entered datas!", MsgBoxStyle.Information)
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVq_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVq.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVq_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVq.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Q = Val(txtBVq.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVs0_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVs0.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVs3_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVs3.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVs6_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVs6.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtBVvm_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtBVvm.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtBVvm_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVvm.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vm = Val(txtBVvm.Text)
	End Sub
	
	Private Sub txtBVs0_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVs0.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s0 = Val(txtBVs0.Text)
	End Sub
	
	Private Sub txtBVs3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVs3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s3 = Val(txtBVs3.Text)
	End Sub
	
	Private Sub txtBVs6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtBVs6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		s6 = Val(txtBVs6.Text)
	End Sub

   
    Private Sub lblBVlc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVlc.Click

    End Sub
    Private Sub lblBVgvs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVgvs.Click

    End Sub
    Private Sub lblBVhvs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVhvs.Click

    End Sub
    Private Sub lblBVn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVn.Click

    End Sub
    Private Sub lblBVk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVk.Click

    End Sub
    Private Sub lblBVpv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVpv.Click

    End Sub
    Private Sub lblBVuf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVuf.Click

    End Sub
    Private Sub lblBVmj_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVmj.Click

    End Sub
    Private Sub lblBVs6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVs6.Click

    End Sub
    Private Sub lblBVq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVq.Click

    End Sub
    Private Sub lblBVdp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVdp.Click

    End Sub
    Private Sub lblBVprc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVprc.Click

    End Sub
    Private Sub lblBVmw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVmw.Click

    End Sub
    Private Sub lblBVdh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVdh.Click

    End Sub
    Private Sub lblBVs0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVs0.Click

    End Sub
    Private Sub lblBVs3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVs3.Click

    End Sub
    Private Sub lblBVvm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblBVvm.Click

    End Sub

    Private Sub fraDataOuput_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fraDataOuput.Enter

    End Sub
End Class