Option Strict Off
Option Explicit On
Friend Class frmunitDensity
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdclear As System.Windows.Forms.Button
	Public WithEvents cmdquit As System.Windows.Forms.Button
	Public WithEvents txtDensitytyd3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitylbgals As System.Windows.Forms.TextBox
	Public WithEvents txtDensitylbgalk As System.Windows.Forms.TextBox
	Public WithEvents txtDensitylbft3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitylbin3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitytm3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitygm3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitygcm3 As System.Windows.Forms.TextBox
	Public WithEvents txtDensitygml As System.Windows.Forms.TextBox
	Public WithEvents txtDensitykgm3 As System.Windows.Forms.TextBox
	Public WithEvents lblDensitytyd3 As System.Windows.Forms.Label
	Public WithEvents lblDensitylbgals As System.Windows.Forms.Label
	Public WithEvents lblDensitylbgalk As System.Windows.Forms.Label
	Public WithEvents lblDensitylbft3 As System.Windows.Forms.Label
	Public WithEvents lblDensitylbin3 As System.Windows.Forms.Label
	Public WithEvents lblDensitytm3 As System.Windows.Forms.Label
	Public WithEvents lblDensitygm3 As System.Windows.Forms.Label
	Public WithEvents lblDensitygcm3 As System.Windows.Forms.Label
	Public WithEvents lblDensitygml As System.Windows.Forms.Label
	Public WithEvents lblDensitykgm3 As System.Windows.Forms.Label
	Public WithEvents lblDensity As System.Windows.Forms.Label
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmunitDensity))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblDensitytyd3 = New System.Windows.Forms.Label
        Me.lblDensitylbgals = New System.Windows.Forms.Label
        Me.lblDensitylbgalk = New System.Windows.Forms.Label
        Me.lblDensitylbft3 = New System.Windows.Forms.Label
        Me.lblDensitylbin3 = New System.Windows.Forms.Label
        Me.lblDensitytm3 = New System.Windows.Forms.Label
        Me.lblDensitygm3 = New System.Windows.Forms.Label
        Me.lblDensitygcm3 = New System.Windows.Forms.Label
        Me.lblDensitygml = New System.Windows.Forms.Label
        Me.lblDensitykgm3 = New System.Windows.Forms.Label
        Me.cmdclear = New System.Windows.Forms.Button
        Me.cmdquit = New System.Windows.Forms.Button
        Me.txtDensitytyd3 = New System.Windows.Forms.TextBox
        Me.txtDensitylbgals = New System.Windows.Forms.TextBox
        Me.txtDensitylbgalk = New System.Windows.Forms.TextBox
        Me.txtDensitylbft3 = New System.Windows.Forms.TextBox
        Me.txtDensitylbin3 = New System.Windows.Forms.TextBox
        Me.txtDensitytm3 = New System.Windows.Forms.TextBox
        Me.txtDensitygm3 = New System.Windows.Forms.TextBox
        Me.txtDensitygcm3 = New System.Windows.Forms.TextBox
        Me.txtDensitygml = New System.Windows.Forms.TextBox
        Me.txtDensitykgm3 = New System.Windows.Forms.TextBox
        Me.lblDensity = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblDensitytyd3
        '
        Me.lblDensitytyd3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitytyd3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitytyd3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitytyd3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitytyd3.Location = New System.Drawing.Point(326, 233)
        Me.lblDensitytyd3.Name = "lblDensitytyd3"
        Me.lblDensitytyd3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitytyd3.Size = New System.Drawing.Size(88, 18)
        Me.lblDensitytyd3.TabIndex = 20
        Me.lblDensitytyd3.Text = "t(UK)/yd^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitytyd3, "吨（英制）每立方码")
        '
        'lblDensitylbgals
        '
        Me.lblDensitylbgals.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitylbgals.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitylbgals.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitylbgals.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitylbgals.Location = New System.Drawing.Point(326, 187)
        Me.lblDensitylbgals.Name = "lblDensitylbgals"
        Me.lblDensitylbgals.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitylbgals.Size = New System.Drawing.Size(88, 19)
        Me.lblDensitylbgals.TabIndex = 19
        Me.lblDensitylbgals.Text = "lb/gal(US)"
        Me.ToolTip1.SetToolTip(Me.lblDensitylbgals, "磅每加仑（美制）")
        '
        'lblDensitylbgalk
        '
        Me.lblDensitylbgalk.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitylbgalk.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitylbgalk.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitylbgalk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitylbgalk.Location = New System.Drawing.Point(326, 142)
        Me.lblDensitylbgalk.Name = "lblDensitylbgalk"
        Me.lblDensitylbgalk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitylbgalk.Size = New System.Drawing.Size(88, 18)
        Me.lblDensitylbgalk.TabIndex = 18
        Me.lblDensitylbgalk.Text = "lb/gal(UK)"
        Me.ToolTip1.SetToolTip(Me.lblDensitylbgalk, "磅每加仑（英制）")
        '
        'lblDensitylbft3
        '
        Me.lblDensitylbft3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitylbft3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitylbft3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitylbft3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitylbft3.Location = New System.Drawing.Point(326, 97)
        Me.lblDensitylbft3.Name = "lblDensitylbft3"
        Me.lblDensitylbft3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitylbft3.Size = New System.Drawing.Size(88, 18)
        Me.lblDensitylbft3.TabIndex = 17
        Me.lblDensitylbft3.Text = "lb/ft^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitylbft3, "磅每立方英尺")
        '
        'lblDensitylbin3
        '
        Me.lblDensitylbin3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitylbin3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitylbin3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitylbin3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitylbin3.Location = New System.Drawing.Point(326, 52)
        Me.lblDensitylbin3.Name = "lblDensitylbin3"
        Me.lblDensitylbin3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitylbin3.Size = New System.Drawing.Size(88, 18)
        Me.lblDensitylbin3.TabIndex = 16
        Me.lblDensitylbin3.Text = "lb/in^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitylbin3, "磅每立方英寸")
        '
        'lblDensitytm3
        '
        Me.lblDensitytm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitytm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitytm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitytm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitytm3.Location = New System.Drawing.Point(125, 224)
        Me.lblDensitytm3.Name = "lblDensitytm3"
        Me.lblDensitytm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitytm3.Size = New System.Drawing.Size(68, 18)
        Me.lblDensitytm3.TabIndex = 15
        Me.lblDensitytm3.Text = "t/m^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitytm3, "吨每立方米")
        '
        'lblDensitygm3
        '
        Me.lblDensitygm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitygm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitygm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitygm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitygm3.Location = New System.Drawing.Point(125, 181)
        Me.lblDensitygm3.Name = "lblDensitygm3"
        Me.lblDensitygm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitygm3.Size = New System.Drawing.Size(68, 18)
        Me.lblDensitygm3.TabIndex = 14
        Me.lblDensitygm3.Text = "g/m^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitygm3, "克每立方米")
        '
        'lblDensitygcm3
        '
        Me.lblDensitygcm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitygcm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitygcm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitygcm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitygcm3.Location = New System.Drawing.Point(125, 138)
        Me.lblDensitygcm3.Name = "lblDensitygcm3"
        Me.lblDensitygcm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitygcm3.Size = New System.Drawing.Size(68, 18)
        Me.lblDensitygcm3.TabIndex = 13
        Me.lblDensitygcm3.Text = "g/cm^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitygcm3, "克每立方厘米")
        '
        'lblDensitygml
        '
        Me.lblDensitygml.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitygml.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitygml.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitygml.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitygml.Location = New System.Drawing.Point(125, 95)
        Me.lblDensitygml.Name = "lblDensitygml"
        Me.lblDensitygml.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitygml.Size = New System.Drawing.Size(68, 18)
        Me.lblDensitygml.TabIndex = 12
        Me.lblDensitygml.Text = "g/ml"
        Me.ToolTip1.SetToolTip(Me.lblDensitygml, "克每毫升")
        '
        'lblDensitykgm3
        '
        Me.lblDensitykgm3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensitykgm3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensitykgm3.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensitykgm3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensitykgm3.Location = New System.Drawing.Point(125, 52)
        Me.lblDensitykgm3.Name = "lblDensitykgm3"
        Me.lblDensitykgm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensitykgm3.Size = New System.Drawing.Size(68, 18)
        Me.lblDensitykgm3.TabIndex = 11
        Me.lblDensitykgm3.Text = "kg/m^3"
        Me.ToolTip1.SetToolTip(Me.lblDensitykgm3, "千克每立方米")
        '
        'cmdclear
        '
        Me.cmdclear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdclear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdclear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdclear.Location = New System.Drawing.Point(240, 267)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdclear.Size = New System.Drawing.Size(88, 27)
        Me.cmdclear.TabIndex = 22
        Me.cmdclear.Text = "清除"
        '
        'cmdquit
        '
        Me.cmdquit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdquit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdquit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdquit.Location = New System.Drawing.Point(77, 267)
        Me.cmdquit.Name = "cmdquit"
        Me.cmdquit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdquit.Size = New System.Drawing.Size(87, 27)
        Me.cmdquit.TabIndex = 21
        Me.cmdquit.Text = "退出"
        '
        'txtDensitytyd3
        '
        Me.txtDensitytyd3.AcceptsReturn = True
        Me.txtDensitytyd3.AutoSize = False
        Me.txtDensitytyd3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitytyd3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitytyd3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitytyd3.Location = New System.Drawing.Point(211, 233)
        Me.txtDensitytyd3.MaxLength = 0
        Me.txtDensitytyd3.Name = "txtDensitytyd3"
        Me.txtDensitytyd3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitytyd3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitytyd3.TabIndex = 10
        Me.txtDensitytyd3.Text = ""
        '
        'txtDensitylbgals
        '
        Me.txtDensitylbgals.AcceptsReturn = True
        Me.txtDensitylbgals.AutoSize = False
        Me.txtDensitylbgals.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitylbgals.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitylbgals.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitylbgals.Location = New System.Drawing.Point(211, 187)
        Me.txtDensitylbgals.MaxLength = 0
        Me.txtDensitylbgals.Name = "txtDensitylbgals"
        Me.txtDensitylbgals.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitylbgals.Size = New System.Drawing.Size(97, 20)
        Me.txtDensitylbgals.TabIndex = 9
        Me.txtDensitylbgals.Text = ""
        '
        'txtDensitylbgalk
        '
        Me.txtDensitylbgalk.AcceptsReturn = True
        Me.txtDensitylbgalk.AutoSize = False
        Me.txtDensitylbgalk.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitylbgalk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitylbgalk.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitylbgalk.Location = New System.Drawing.Point(211, 142)
        Me.txtDensitylbgalk.MaxLength = 0
        Me.txtDensitylbgalk.Name = "txtDensitylbgalk"
        Me.txtDensitylbgalk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitylbgalk.Size = New System.Drawing.Size(97, 20)
        Me.txtDensitylbgalk.TabIndex = 8
        Me.txtDensitylbgalk.Text = ""
        '
        'txtDensitylbft3
        '
        Me.txtDensitylbft3.AcceptsReturn = True
        Me.txtDensitylbft3.AutoSize = False
        Me.txtDensitylbft3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitylbft3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitylbft3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitylbft3.Location = New System.Drawing.Point(211, 97)
        Me.txtDensitylbft3.MaxLength = 0
        Me.txtDensitylbft3.Name = "txtDensitylbft3"
        Me.txtDensitylbft3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitylbft3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitylbft3.TabIndex = 7
        Me.txtDensitylbft3.Text = ""
        '
        'txtDensitylbin3
        '
        Me.txtDensitylbin3.AcceptsReturn = True
        Me.txtDensitylbin3.AutoSize = False
        Me.txtDensitylbin3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitylbin3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitylbin3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitylbin3.Location = New System.Drawing.Point(211, 52)
        Me.txtDensitylbin3.MaxLength = 0
        Me.txtDensitylbin3.Name = "txtDensitylbin3"
        Me.txtDensitylbin3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitylbin3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitylbin3.TabIndex = 6
        Me.txtDensitylbin3.Text = ""
        '
        'txtDensitytm3
        '
        Me.txtDensitytm3.AcceptsReturn = True
        Me.txtDensitytm3.AutoSize = False
        Me.txtDensitytm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitytm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitytm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitytm3.Location = New System.Drawing.Point(19, 224)
        Me.txtDensitytm3.MaxLength = 0
        Me.txtDensitytm3.Name = "txtDensitytm3"
        Me.txtDensitytm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitytm3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitytm3.TabIndex = 5
        Me.txtDensitytm3.Text = ""
        '
        'txtDensitygm3
        '
        Me.txtDensitygm3.AcceptsReturn = True
        Me.txtDensitygm3.AutoSize = False
        Me.txtDensitygm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitygm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitygm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitygm3.Location = New System.Drawing.Point(19, 181)
        Me.txtDensitygm3.MaxLength = 0
        Me.txtDensitygm3.Name = "txtDensitygm3"
        Me.txtDensitygm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitygm3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitygm3.TabIndex = 4
        Me.txtDensitygm3.Text = ""
        '
        'txtDensitygcm3
        '
        Me.txtDensitygcm3.AcceptsReturn = True
        Me.txtDensitygcm3.AutoSize = False
        Me.txtDensitygcm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitygcm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitygcm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitygcm3.Location = New System.Drawing.Point(19, 138)
        Me.txtDensitygcm3.MaxLength = 0
        Me.txtDensitygcm3.Name = "txtDensitygcm3"
        Me.txtDensitygcm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitygcm3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitygcm3.TabIndex = 3
        Me.txtDensitygcm3.Text = ""
        '
        'txtDensitygml
        '
        Me.txtDensitygml.AcceptsReturn = True
        Me.txtDensitygml.AutoSize = False
        Me.txtDensitygml.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitygml.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitygml.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitygml.Location = New System.Drawing.Point(19, 95)
        Me.txtDensitygml.MaxLength = 0
        Me.txtDensitygml.Name = "txtDensitygml"
        Me.txtDensitygml.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitygml.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitygml.TabIndex = 2
        Me.txtDensitygml.Text = ""
        '
        'txtDensitykgm3
        '
        Me.txtDensitykgm3.AcceptsReturn = True
        Me.txtDensitykgm3.AutoSize = False
        Me.txtDensitykgm3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDensitykgm3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDensitykgm3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDensitykgm3.Location = New System.Drawing.Point(19, 52)
        Me.txtDensitykgm3.MaxLength = 0
        Me.txtDensitykgm3.Name = "txtDensitykgm3"
        Me.txtDensitykgm3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDensitykgm3.Size = New System.Drawing.Size(97, 19)
        Me.txtDensitykgm3.TabIndex = 1
        Me.txtDensitykgm3.Text = ""
        '
        'lblDensity
        '
        Me.lblDensity.BackColor = System.Drawing.SystemColors.Control
        Me.lblDensity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDensity.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblDensity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDensity.Location = New System.Drawing.Point(10, 17)
        Me.lblDensity.Name = "lblDensity"
        Me.lblDensity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDensity.Size = New System.Drawing.Size(241, 19)
        Me.lblDensity.TabIndex = 0
        Me.lblDensity.Text = "输入密度单位换算数据："
        '
        'frmunitDensity
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(432, 312)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.cmdquit)
        Me.Controls.Add(Me.txtDensitytyd3)
        Me.Controls.Add(Me.txtDensitylbgals)
        Me.Controls.Add(Me.txtDensitylbgalk)
        Me.Controls.Add(Me.txtDensitylbft3)
        Me.Controls.Add(Me.txtDensitylbin3)
        Me.Controls.Add(Me.txtDensitytm3)
        Me.Controls.Add(Me.txtDensitygm3)
        Me.Controls.Add(Me.txtDensitygcm3)
        Me.Controls.Add(Me.txtDensitygml)
        Me.Controls.Add(Me.txtDensitykgm3)
        Me.Controls.Add(Me.lblDensitytyd3)
        Me.Controls.Add(Me.lblDensitylbgals)
        Me.Controls.Add(Me.lblDensitylbgalk)
        Me.Controls.Add(Me.lblDensitylbft3)
        Me.Controls.Add(Me.lblDensitylbin3)
        Me.Controls.Add(Me.lblDensitytm3)
        Me.Controls.Add(Me.lblDensitygm3)
        Me.Controls.Add(Me.lblDensitygcm3)
        Me.Controls.Add(Me.lblDensitygml)
        Me.Controls.Add(Me.lblDensitykgm3)
        Me.Controls.Add(Me.lblDensity)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 23)
        Me.MaximizeBox = False
        Me.Name = "frmunitDensity"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "密度(Density)单位换算"
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As frmunitDensity
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmunitDensity
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmunitDensity()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	'本模块编程人：王朝，完成时间2006年4月24日于青海省海西洲南八仙油田仙中23井
	'密度单位换算模块
	
	Dim gcm3 As Single 'g/cm^3
	Dim gm3 As Single 'g/m^3
	Dim gml As Single 'g/ml
	Dim kgm3 As Single 'kg/m^3
	Dim lbft3 As Single 'lb/ft^3
	Dim lbgalk As Single 'lb/gal 磅/加仑（英制）
	Dim lbgals As Single 'lb/gal 磅/加仑（美制）
	Dim lbin3 As Single 'lb/in^3
	Dim tm3 As Single 't/m^3
	Dim tyd3 As Single 't(UK)/yd3
	
	
	Private Sub cmdclear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdclear.Click
		Me.Close()
		frmunitDensity.DefInstance.Show() '使用该方法清除数据，可能存在bug隐患。
	End Sub
	
	Private Sub cmdquit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdquit.Click
		Me.Close()
	End Sub
	
	Private Sub txtDensitygcm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitygcm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gcm3 = Val(txtDensitygcm3.Text)
		
		gm3 = gcm3 * 1000000#
		gml = gcm3
		kgm3 = gcm3 * 1000#
		lbft3 = gcm3 * 62.428
		lbgalk = gcm3 * 10.0224
		lbgals = gcm3 * 8.3454
		lbin3 = gcm3 * 0.0361
		tm3 = gcm3
		tyd3 = gcm3 * 0.7525
		
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitygm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitygm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gm3 = Val(txtDensitygm3.Text)
		
		gcm3 = gm3 * 0.000001
		gml = gm3 * 0.000001
		kgm3 = gm3 * 0.001
		lbft3 = gm3 * 0.000062428
		lbgalk = gm3 * 0.000010022
		lbgals = gm3 * 0.0000083454
		lbin3 = gm3 * 0.000000036127
		tm3 = gm3 * 0.000001
		tyd3 = gm3 * 0.00000075248
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitygml_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitygml.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		gml = Val(txtDensitygml.Text)
		
		gcm3 = gml
		gm3 = gml * 1000000#
		kgm3 = gml * 1000
		lbft3 = gml * 62.428
		lbgalk = gml * 10.0224
		lbgals = gml * 8.3454
		lbin3 = gml * 0.0361
		tm3 = gml
		tyd3 = gml * 0.7525
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		'txtDensitygml.Text = gml
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitykgm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitykgm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		kgm3 = Val(txtDensitykgm3.Text)
		
		gcm3 = kgm3 * 0.001
		gm3 = kgm3 * 1000
		gml = kgm3 * 0.001
		lbft3 = kgm3 * 0.0624
		lbgalk = kgm3 * 0.01
		lbgals = kgm3 * 0.0083
		lbin3 = kgm3 * 0.000036127
		tm3 = kgm3 * 0.001
		tyd3 = kgm3 * 0.00075248
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		'txtDensitykgm3.Text = kgm3
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitylbft3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitylbft3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbft3 = Val(txtDensitylbft3.Text)
		
		gcm3 = lbft3 * 0.016
		gm3 = lbft3 * 16018#
		gml = lbft3 * 0.01601
		kgm3 = lbft3 * 16.0185
		'lbft3 = lbft3
		lbgalk = lbft3 * 0.1605
		lbgals = lbft3 * 0.1337
		lbin3 = lbft3 * 0.0006
		tm3 = lbft3 * 0.016
		tyd3 = lbft3 * 0.0121
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		'txtDensitylbft3.Text = lbft3
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitylbgalk_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitylbgalk.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbgalk = Val(txtDensitylbgalk.Text)
		
		gcm3 = lbgalk * 0.0998
		gm3 = lbgalk * 99776#
		gml = lbgalk * 0.0998
		kgm3 = lbgalk * 99.7763
		lbft3 = lbgalk * 6.2288
		'lbgalk = lbgalk
		lbgals = lbgalk * 0.8327
		lbin3 = lbgalk * 0.0036
		tm3 = lbgalk * 0.0998
		tyd3 = lbgalk * 0.0751
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		'txtDensitylbgalk.Text = lbgalk
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitylbgals_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitylbgals.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbgals = Val(txtDensitylbgals.Text)
		
		gcm3 = lbgals * 0.1198
		gm3 = lbgals * 119830#
		gml = lbgals * 0.1198
		kgm3 = lbgals * 119.8264
		lbft3 = lbgals * 7.4805
		lbgalk = lbgals * 1.201
		'lbgals = lbgals
		lbin3 = lbgals * 0.0036
		tm3 = lbgals * 0.1198
		tyd3 = lbgals * 0.0902
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		'txtDensitylbgals.Text = lbgals
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitylbin3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitylbin3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		lbin3 = Val(txtDensitylbin3.Text)
		
		gcm3 = lbin3 * 27.6799
		gm3 = lbin3 * 27680000#
		gml = lbin3 * 27.6799
		kgm3 = lbin3 * 27680#
		lbft3 = lbin3 * 1728#
		lbgalk = lbin3 * 277.42
		lbgals = lbin3 * 231#
		'lbin3 = lbin3
		tm3 = lbin3 * 27.6799
		tyd3 = lbin3 * 20.8286
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		'txtDensitylbin3.Text = lbin3
		txtDensitytm3.Text = CStr(tm3)
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitytm3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitytm3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		tm3 = Val(txtDensitytm3.Text)
		
		gcm3 = tm3
		gm3 = tm3 * 1000000#
		gml = tm3
		kgm3 = tm3 * 1000#
		lbft3 = tm3 * 62.428
		lbgalk = tm3 * 10.0224
		lbgals = tm3 * 8.3454
		lbin3 = tm3 * 0.0361
		'tm3 = tm3
		tyd3 = tm3 * 0.7525
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		'txtDensitytm3.Text = tm3
		txtDensitytyd3.Text = CStr(tyd3)
	End Sub
	
	Private Sub txtDensitytyd3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDensitytyd3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		tyd3 = Val(txtDensitytyd3.Text)
		
		gcm3 = tyd3 * 1.3289
		gm3 = tyd3 * 1328900#
		gml = tyd3 * 1.3289
		kgm3 = tyd3 * 1328.9392
		lbft3 = tyd3 * 82.963
		lbgalk = tyd3 * 13.3192
		lbgals = tyd3 * 11.0905
		lbin3 = tyd3 * 0.048
		tm3 = tyd3 * 1.3289
		'tyd3 = tyd3
		
		txtDensitygcm3.Text = CStr(gcm3)
		txtDensitygm3.Text = CStr(gm3)
		txtDensitygml.Text = CStr(gml)
		txtDensitykgm3.Text = CStr(kgm3)
		txtDensitylbft3.Text = CStr(lbft3)
		txtDensitylbgalk.Text = CStr(lbgalk)
		txtDensitylbgals.Text = CStr(lbgals)
		txtDensitylbin3.Text = CStr(lbin3)
		txtDensitytm3.Text = CStr(tm3)
		'txtDensitytyd3.Text = tyd3
	End Sub
End Class