Option Strict Off
Option Explicit On
Friend Class MDIFrmMud
    Inherits System.Windows.Forms.Form
    Implements IMessageFilter
    
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			m_vb6FormDefInstance = Me
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
    
	Public WithEvents mnuMudCalculate As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuQuit As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuShow As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuShowC As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuShowH As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuShowIcon As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuShowV As Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray
	Public WithEvents mnuMudMWMake As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudMWHweight As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudMWLweight As System.Windows.Forms.MenuItem
    Public WithEvents mnuMudCalWellVol As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudCalCircleTime As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudCalAnnularspaceVel As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudCalOilGasVel As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudCalDisplacement As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudPumpFlow As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudDataCal As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudBasePressureCal As System.Windows.Forms.MenuItem
    Public WithEvents _mnuMudCalculate_7 As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudSolidContent As System.Windows.Forms.MenuItem
    Public WithEvents mnuMudRcLc As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudHB As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudRchang As System.Windows.Forms.MenuItem
    Public WithEvents mnuMudHyCal As System.Windows.Forms.MenuItem
	Public WithEvents mnuMudAnalyse As System.Windows.Forms.MenuItem
	Public WithEvents mnuClCal As System.Windows.Forms.MenuItem
	Public WithEvents mnuCaMgCal As System.Windows.Forms.MenuItem
	Public WithEvents mnuPHCal As System.Windows.Forms.MenuItem
	Public WithEvents mnuChemistryFilter As System.Windows.Forms.MenuItem
	Public WithEvents mnuKillWellChange As System.Windows.Forms.MenuItem
	Public WithEvents mnuKillWellCalBase As System.Windows.Forms.MenuItem
	Public WithEvents mnuKillWell As System.Windows.Forms.MenuItem
    Public WithEvents mnuToolUnit As System.Windows.Forms.MenuItem
	Public WithEvents mnuToolCalc As System.Windows.Forms.MenuItem
	Public WithEvents mnuToolFilefind As System.Windows.Forms.MenuItem
	Public WithEvents mnuTool As System.Windows.Forms.MenuItem
	Public WithEvents _mnuShowC_2 As System.Windows.Forms.MenuItem
	Public WithEvents _mnuShowH_3 As System.Windows.Forms.MenuItem
	Public WithEvents _mnuShowV_4 As System.Windows.Forms.MenuItem
	Public WithEvents FENGGE3 As System.Windows.Forms.MenuItem
	Public WithEvents _mnuShowIcon_5 As System.Windows.Forms.MenuItem
	Public WithEvents _mnuShow_1 As System.Windows.Forms.MenuItem
	Public WithEvents mnuHelpContents As System.Windows.Forms.MenuItem
	Public WithEvents mnuHelpSearch As System.Windows.Forms.MenuItem
	Public WithEvents mnuHelpBar1 As System.Windows.Forms.MenuItem
	Public WithEvents mnuHelpAbout As System.Windows.Forms.MenuItem
	Public WithEvents mnuHelp As System.Windows.Forms.MenuItem
	Public WithEvents _mnuQuit_0 As System.Windows.Forms.MenuItem
    Public MainMenu1 As System.Windows.Forms.MainMenu
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents imlToolbarIcons As System.Windows.Forms.ImageList
    Friend WithEvents TlbMudToolIcon As System.Windows.Forms.ToolBar
    Friend WithEvents staMdifrmMud As System.Windows.Forms.StatusBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton6 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton8 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton9 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton10 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton11 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton12 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton13 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton14 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton15 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton16 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton17 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton18 As System.Windows.Forms.ToolBarButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolBarButton19 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton20 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton21 As System.Windows.Forms.ToolBarButton
    Friend WithEvents MainMenu2 As System.Windows.Forms.MainMenu
    Friend WithEvents ToolBarButton22 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton23 As System.Windows.Forms.ToolBarButton
    Friend WithEvents mnuMudSurgePressure As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MDIFrmMud))
        Me.mnuMudCalculate = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me._mnuMudCalculate_7 = New System.Windows.Forms.MenuItem
        Me.mnuMudCalWellVol = New System.Windows.Forms.MenuItem
        Me.mnuMudCalCircleTime = New System.Windows.Forms.MenuItem
        Me.mnuMudCalDisplacement = New System.Windows.Forms.MenuItem
        Me.mnuMudPumpFlow = New System.Windows.Forms.MenuItem
        Me.mnuMudMWMake = New System.Windows.Forms.MenuItem
        Me.mnuMudMWHweight = New System.Windows.Forms.MenuItem
        Me.mnuMudMWLweight = New System.Windows.Forms.MenuItem
        Me.mnuMudCalAnnularspaceVel = New System.Windows.Forms.MenuItem
        Me.mnuMudCalOilGasVel = New System.Windows.Forms.MenuItem
        Me.mnuMudDataCal = New System.Windows.Forms.MenuItem
        Me.mnuMudBasePressureCal = New System.Windows.Forms.MenuItem
        Me.mnuQuit = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me._mnuQuit_0 = New System.Windows.Forms.MenuItem
        Me.mnuShow = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me._mnuShow_1 = New System.Windows.Forms.MenuItem
        Me._mnuShowC_2 = New System.Windows.Forms.MenuItem
        Me._mnuShowH_3 = New System.Windows.Forms.MenuItem
        Me._mnuShowV_4 = New System.Windows.Forms.MenuItem
        Me.FENGGE3 = New System.Windows.Forms.MenuItem
        Me._mnuShowIcon_5 = New System.Windows.Forms.MenuItem
        Me.mnuShowC = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me.mnuShowH = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me.mnuShowIcon = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me.mnuShowV = New Microsoft.VisualBasic.Compatibility.VB6.MenuItemArray(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuKillWell = New System.Windows.Forms.MenuItem
        Me.mnuKillWellChange = New System.Windows.Forms.MenuItem
        Me.mnuKillWellCalBase = New System.Windows.Forms.MenuItem
        Me.mnuMudSurgePressure = New System.Windows.Forms.MenuItem
        Me.mnuMudAnalyse = New System.Windows.Forms.MenuItem
        Me.mnuMudRchang = New System.Windows.Forms.MenuItem
        Me.mnuMudRcLc = New System.Windows.Forms.MenuItem
        Me.mnuMudHB = New System.Windows.Forms.MenuItem
        Me.mnuMudHyCal = New System.Windows.Forms.MenuItem
        Me.mnuMudSolidContent = New System.Windows.Forms.MenuItem
        Me.mnuChemistryFilter = New System.Windows.Forms.MenuItem
        Me.mnuClCal = New System.Windows.Forms.MenuItem
        Me.mnuCaMgCal = New System.Windows.Forms.MenuItem
        Me.mnuPHCal = New System.Windows.Forms.MenuItem
        Me.mnuTool = New System.Windows.Forms.MenuItem
        Me.mnuToolUnit = New System.Windows.Forms.MenuItem
        Me.mnuToolCalc = New System.Windows.Forms.MenuItem
        Me.mnuToolFilefind = New System.Windows.Forms.MenuItem
        Me.mnuHelp = New System.Windows.Forms.MenuItem
        Me.mnuHelpContents = New System.Windows.Forms.MenuItem
        Me.mnuHelpSearch = New System.Windows.Forms.MenuItem
        Me.mnuHelpBar1 = New System.Windows.Forms.MenuItem
        Me.mnuHelpAbout = New System.Windows.Forms.MenuItem
        Me.staMdifrmMud = New System.Windows.Forms.StatusBar
        Me.TlbMudToolIcon = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton6 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton8 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton9 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton10 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton11 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton12 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton13 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton14 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton15 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton16 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton17 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton18 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton19 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton20 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton21 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton22 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton23 = New System.Windows.Forms.ToolBarButton
        Me.imlToolbarIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MainMenu2 = New System.Windows.Forms.MainMenu
        CType(Me.mnuMudCalculate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuQuit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuShow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuShowC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuShowH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuShowIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuShowV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_mnuMudCalculate_7
        '
        Me.mnuMudCalculate.SetIndex(Me._mnuMudCalculate_7, CType(7, Short))
        Me._mnuMudCalculate_7.Index = 1
        Me._mnuMudCalculate_7.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMudCalWellVol, Me.mnuMudCalCircleTime, Me.mnuMudCalDisplacement, Me.mnuMudPumpFlow})
        Me._mnuMudCalculate_7.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuMudCalculate_7.Text = "&V体积"
        '
        'mnuMudCalWellVol
        '
        Me.mnuMudCalWellVol.Index = 0
        Me.mnuMudCalWellVol.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudCalWellVol.Text = "空井筒体积计算"
        '
        'mnuMudCalCircleTime
        '
        Me.mnuMudCalCircleTime.Index = 1
        Me.mnuMudCalCircleTime.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudCalCircleTime.Text = "循环周时间"
        '
        'mnuMudCalDisplacement
        '
        Me.mnuMudCalDisplacement.Index = 2
        Me.mnuMudCalDisplacement.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudCalDisplacement.Text = "钻具替排放体积"
        '
        'mnuMudPumpFlow
        '
        Me.mnuMudPumpFlow.Index = 3
        Me.mnuMudPumpFlow.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudPumpFlow.Text = "泥浆泵排量"
        '
        'mnuMudMWMake
        '
        Me.mnuMudMWMake.Index = 3
        Me.mnuMudMWMake.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudMWMake.Text = "土粉浆钻井液"
        '
        'mnuMudMWHweight
        '
        Me.mnuMudMWHweight.Index = 1
        Me.mnuMudMWHweight.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudMWHweight.Text = "钻井液加重密度/降低密度"
        '
        'mnuMudMWLweight
        '
        Me.mnuMudMWLweight.Index = 0
        Me.mnuMudMWLweight.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudMWLweight.Text = "钻井液混合"
        '
        'mnuMudCalAnnularspaceVel
        '
        Me.mnuMudCalAnnularspaceVel.Index = 2
        Me.mnuMudCalAnnularspaceVel.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudCalAnnularspaceVel.Text = "钻井液环空返速"
        '
        'mnuMudCalOilGasVel
        '
        Me.mnuMudCalOilGasVel.Index = 5
        Me.mnuMudCalOilGasVel.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudCalOilGasVel.Text = "油气上窜速度"
        '
        'mnuMudDataCal
        '
        Me.mnuMudDataCal.Index = 1
        Me.mnuMudDataCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudDataCal.Text = "基础钻井液流变参数"
        '
        'mnuMudBasePressureCal
        '
        Me.mnuMudBasePressureCal.Index = 2
        Me.mnuMudBasePressureCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudBasePressureCal.Text = "简单压力计算"
        '
        'mnuQuit
        '
        '
        '_mnuQuit_0
        '
        Me.mnuQuit.SetIndex(Me._mnuQuit_0, CType(0, Short))
        Me._mnuQuit_0.Index = 7
        Me._mnuQuit_0.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuQuit_0.Text = "&Q退出"
        '
        '_mnuShow_1
        '
        Me.mnuShow.SetIndex(Me._mnuShow_1, CType(1, Short))
        Me._mnuShow_1.Index = 5
        Me._mnuShow_1.MdiList = True
        Me._mnuShow_1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me._mnuShowC_2, Me._mnuShowH_3, Me._mnuShowV_4, Me.FENGGE3, Me._mnuShowIcon_5})
        Me._mnuShow_1.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuShow_1.Text = "&S窗口"
        '
        '_mnuShowC_2
        '
        Me.mnuShowC.SetIndex(Me._mnuShowC_2, CType(2, Short))
        Me._mnuShowC_2.Index = 0
        Me._mnuShowC_2.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuShowC_2.Text = "&C叠加"
        '
        '_mnuShowH_3
        '
        Me.mnuShowH.SetIndex(Me._mnuShowH_3, CType(3, Short))
        Me._mnuShowH_3.Index = 1
        Me._mnuShowH_3.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuShowH_3.Text = "&H水平"
        '
        '_mnuShowV_4
        '
        Me.mnuShowV.SetIndex(Me._mnuShowV_4, CType(4, Short))
        Me._mnuShowV_4.Index = 2
        Me._mnuShowV_4.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuShowV_4.Text = "&V垂直"
        '
        'FENGGE3
        '
        Me.FENGGE3.Index = 3
        Me.FENGGE3.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.FENGGE3.Text = "-"
        Me.FENGGE3.Visible = False
        '
        '_mnuShowIcon_5
        '
        Me.mnuShowIcon.SetIndex(Me._mnuShowIcon_5, CType(5, Short))
        Me._mnuShowIcon_5.Index = 4
        Me._mnuShowIcon_5.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me._mnuShowIcon_5.Text = "&A图标"
        Me._mnuShowIcon_5.Visible = False
        '
        'mnuShowC
        '
        '
        'mnuShowH
        '
        '
        'mnuShowIcon
        '
        '
        'mnuShowV
        '
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuKillWell, Me._mnuMudCalculate_7, Me.mnuMudAnalyse, Me.mnuChemistryFilter, Me.mnuTool, Me._mnuShow_1, Me.mnuHelp, Me._mnuQuit_0})
        '
        'mnuKillWell
        '
        Me.mnuKillWell.Index = 0
        Me.mnuKillWell.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMudMWLweight, Me.mnuMudMWHweight, Me.mnuMudBasePressureCal, Me.mnuMudMWMake, Me.mnuKillWellChange, Me.mnuKillWellCalBase, Me.mnuMudSurgePressure})
        Me.mnuKillWell.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuKillWell.Text = "&P压力"
        '
        'mnuKillWellChange
        '
        Me.mnuKillWellChange.Index = 4
        Me.mnuKillWellChange.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuKillWellChange.Text = "溢流类型"
        '
        'mnuKillWellCalBase
        '
        Me.mnuKillWellCalBase.Index = 5
        Me.mnuKillWellCalBase.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuKillWellCalBase.Text = "压井基础计算"
        '
        'mnuMudSurgePressure
        '
        Me.mnuMudSurgePressure.Index = 6
        Me.mnuMudSurgePressure.Text = "激动压力计算"
        '
        'mnuMudAnalyse
        '
        Me.mnuMudAnalyse.Index = 2
        Me.mnuMudAnalyse.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMudRchang, Me.mnuMudDataCal, Me.mnuMudCalAnnularspaceVel, Me.mnuMudRcLc, Me.mnuMudHB, Me.mnuMudCalOilGasVel, Me.mnuMudHyCal, Me.mnuMudSolidContent})
        Me.mnuMudAnalyse.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudAnalyse.Text = "&API水力学"
        '
        'mnuMudRchang
        '
        Me.mnuMudRchang.Index = 0
        Me.mnuMudRchang.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudRchang.Text = "API 钻井液流变模式判断"
        '
        'mnuMudRcLc
        '
        Me.mnuMudRcLc.Index = 3
        Me.mnuMudRcLc.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudRcLc.Text = "井眼净化能力评价(LC)"
        '
        'mnuMudHB
        '
        Me.mnuMudHB.Index = 4
        Me.mnuMudHB.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudHB.Text = "环空岩屑浓度分析"
        '
        'mnuMudHyCal
        '
        Me.mnuMudHyCal.Index = 6
        Me.mnuMudHyCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudHyCal.Text = "环空水力计算(MILPARK公司)"
        '
        'mnuMudSolidContent
        '
        Me.mnuMudSolidContent.Index = 7
        Me.mnuMudSolidContent.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuMudSolidContent.Text = "钻井液固相数据分析"
        '
        'mnuChemistryFilter
        '
        Me.mnuChemistryFilter.Index = 3
        Me.mnuChemistryFilter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuClCal, Me.mnuCaMgCal, Me.mnuPHCal})
        Me.mnuChemistryFilter.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuChemistryFilter.Text = "&C化学"
        '
        'mnuClCal
        '
        Me.mnuClCal.Index = 0
        Me.mnuClCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuClCal.Text = "氯离子检测"
        '
        'mnuCaMgCal
        '
        Me.mnuCaMgCal.Index = 1
        Me.mnuCaMgCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuCaMgCal.Text = "Ca++/Mg++ 检测"
        '
        'mnuPHCal
        '
        Me.mnuPHCal.Index = 2
        Me.mnuPHCal.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuPHCal.Text = "钻井液碱度与石灰含量"
        '
        'mnuTool
        '
        Me.mnuTool.Index = 4
        Me.mnuTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolUnit, Me.mnuToolCalc, Me.mnuToolFilefind})
        Me.mnuTool.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuTool.RadioCheck = True
        Me.mnuTool.Text = "&T工具"
        '
        'mnuToolUnit
        '
        Me.mnuToolUnit.Index = 0
        Me.mnuToolUnit.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuToolUnit.Text = "计量单位换算"
        '
        'mnuToolCalc
        '
        Me.mnuToolCalc.Index = 1
        Me.mnuToolCalc.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuToolCalc.Shortcut = System.Windows.Forms.Shortcut.F2
        Me.mnuToolCalc.Text = "简易计算器"
        '
        'mnuToolFilefind
        '
        Me.mnuToolFilefind.Index = 2
        Me.mnuToolFilefind.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuToolFilefind.Text = "技术手册"
        '
        'mnuHelp
        '
        Me.mnuHelp.Index = 6
        Me.mnuHelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuHelpContents, Me.mnuHelpSearch, Me.mnuHelpBar1, Me.mnuHelpAbout})
        Me.mnuHelp.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuHelp.Text = "&H帮助"
        '
        'mnuHelpContents
        '
        Me.mnuHelpContents.Index = 0
        Me.mnuHelpContents.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuHelpContents.Shortcut = System.Windows.Forms.Shortcut.F1
        Me.mnuHelpContents.Text = "目录(&C)"
        '
        'mnuHelpSearch
        '
        Me.mnuHelpSearch.Index = 1
        Me.mnuHelpSearch.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuHelpSearch.Shortcut = System.Windows.Forms.Shortcut.CtrlS
        Me.mnuHelpSearch.Text = "search help(&S)..."
        '
        'mnuHelpBar1
        '
        Me.mnuHelpBar1.Index = 2
        Me.mnuHelpBar1.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuHelpBar1.Text = "-"
        '
        'mnuHelpAbout
        '
        Me.mnuHelpAbout.Index = 3
        Me.mnuHelpAbout.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuHelpAbout.Shortcut = System.Windows.Forms.Shortcut.CtrlA
        Me.mnuHelpAbout.Text = "关于Mud Calculator(&A)"
        '
        'staMdifrmMud
        '
        Me.staMdifrmMud.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.staMdifrmMud.Location = New System.Drawing.Point(0, 617)
        Me.staMdifrmMud.Name = "staMdifrmMud"
        Me.staMdifrmMud.ShowPanels = True
        Me.staMdifrmMud.Size = New System.Drawing.Size(897, 24)
        Me.staMdifrmMud.SizingGrip = False
        Me.staMdifrmMud.TabIndex = 1
        Me.staMdifrmMud.Text = "状态条"
        '
        'TlbMudToolIcon
        '
        Me.TlbMudToolIcon.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton5, Me.ToolBarButton6, Me.ToolBarButton7, Me.ToolBarButton8, Me.ToolBarButton9, Me.ToolBarButton10, Me.ToolBarButton11, Me.ToolBarButton12, Me.ToolBarButton13, Me.ToolBarButton14, Me.ToolBarButton15, Me.ToolBarButton16, Me.ToolBarButton17, Me.ToolBarButton18, Me.ToolBarButton19, Me.ToolBarButton20, Me.ToolBarButton21, Me.ToolBarButton22, Me.ToolBarButton23})
        Me.TlbMudToolIcon.ButtonSize = New System.Drawing.Size(16, 16)
        Me.TlbMudToolIcon.DropDownArrows = True
        Me.TlbMudToolIcon.ImageList = Me.imlToolbarIcons
        Me.TlbMudToolIcon.Location = New System.Drawing.Point(0, 0)
        Me.TlbMudToolIcon.Name = "TlbMudToolIcon"
        Me.TlbMudToolIcon.ShowToolTips = True
        Me.TlbMudToolIcon.Size = New System.Drawing.Size(897, 44)
        Me.TlbMudToolIcon.TabIndex = 2
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = CType(configurationAppSettings.GetValue("0", GetType(System.Int32)), Integer)
        Me.ToolBarButton1.ToolTipText = "钻井液固相含量分析"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.Enabled = CType(configurationAppSettings.GetValue("ToolBarButton2.Enabled", GetType(System.Boolean)), Boolean)
        Me.ToolBarButton2.ImageIndex = CType(configurationAppSettings.GetValue("1", GetType(System.Int32)), Integer)
        Me.ToolBarButton2.ToolTipText = "环空水力计算(MILPARK)"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.ToolTipText = "钻井液流变模式分析计算"
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.ToolTipText = "环空流态稳定参数Z计算"
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.ImageIndex = 4
        Me.ToolBarButton5.ToolTipText = "环空携岩能力评价LC"
        '
        'ToolBarButton6
        '
        Me.ToolBarButton6.ImageIndex = 5
        Me.ToolBarButton6.ToolTipText = "钻具排放体积计算"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.ToolTipText = "钻井液循环时间计算"
        '
        'ToolBarButton8
        '
        Me.ToolBarButton8.ImageIndex = 7
        Me.ToolBarButton8.ToolTipText = "油气上窜速度计算"
        '
        'ToolBarButton9
        '
        Me.ToolBarButton9.ImageIndex = 8
        Me.ToolBarButton9.ToolTipText = "钻井液环空返速计算"
        '
        'ToolBarButton10
        '
        Me.ToolBarButton10.ImageIndex = 9
        Me.ToolBarButton10.ToolTipText = "钻井泥浆泵排量计算"
        '
        'ToolBarButton11
        '
        Me.ToolBarButton11.ImageIndex = 10
        Me.ToolBarButton11.ToolTipText = "钻井液加重计算模块"
        '
        'ToolBarButton12
        '
        Me.ToolBarButton12.ImageIndex = 11
        Me.ToolBarButton12.ToolTipText = "钻井液碱度及滤液碱度测定"
        '
        'ToolBarButton13
        '
        Me.ToolBarButton13.ImageIndex = 12
        Me.ToolBarButton13.ToolTipText = "常规流变参数计算"
        '
        'ToolBarButton14
        '
        Me.ToolBarButton14.ImageIndex = 13
        Me.ToolBarButton14.ToolTipText = "钻井液混合后密度计算"
        '
        'ToolBarButton15
        '
        Me.ToolBarButton15.ImageIndex = 14
        Me.ToolBarButton15.ToolTipText = "井筒钻井液体积计算"
        '
        'ToolBarButton16
        '
        Me.ToolBarButton16.ImageIndex = 15
        Me.ToolBarButton16.ToolTipText = "土粉浆配置计算"
        '
        'ToolBarButton17
        '
        Me.ToolBarButton17.ImageIndex = 16
        Me.ToolBarButton17.ToolTipText = "基础压井计算模块"
        '
        'ToolBarButton18
        '
        Me.ToolBarButton18.ImageIndex = 17
        Me.ToolBarButton18.ToolTipText = "计算器"
        '
        'ToolBarButton19
        '
        Me.ToolBarButton19.ImageIndex = 18
        Me.ToolBarButton19.ToolTipText = "计量单位转换功能模块"
        '
        'ToolBarButton20
        '
        Me.ToolBarButton20.ImageIndex = 19
        Me.ToolBarButton20.ToolTipText = "技术手册"
        '
        'ToolBarButton21
        '
        Me.ToolBarButton21.ImageIndex = 20
        Me.ToolBarButton21.ToolTipText = "关于MudCalculator软件介绍"
        '
        'ToolBarButton22
        '
        Me.ToolBarButton22.ImageIndex = 21
        Me.ToolBarButton22.ToolTipText = "帮助"
        '
        'ToolBarButton23
        '
        Me.ToolBarButton23.ImageIndex = 22
        Me.ToolBarButton23.ToolTipText = "退出"
        '
        'imlToolbarIcons
        '
        Me.imlToolbarIcons.ImageSize = New System.Drawing.Size(32, 32)
        Me.imlToolbarIcons.ImageStream = CType(resources.GetObject("imlToolbarIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlToolbarIcons.TransparentColor = System.Drawing.Color.Transparent
        '
        'Timer1
        '
        '
        'MDIFrmMud
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ClientSize = New System.Drawing.Size(897, 641)
        Me.Controls.Add(Me.TlbMudToolIcon)
        Me.Controls.Add(Me.staMdifrmMud)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Location = New System.Drawing.Point(15, 57)
        Me.Menu = Me.MainMenu1
        Me.Name = "MDIFrmMud"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Mud Calculator"
        CType(Me.mnuMudCalculate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuQuit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuShow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuShowC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuShowH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuShowIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuShowV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As MDIFrmMud
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As MDIFrmMud
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New MDIFrmMud()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Const pi As Double = 3.1415926
    Dim buttons As MsgBoxStyle

    ' Create four StatusBarPanel objects to display in statusBar1.
    Dim panel0 As New StatusBarPanel
    Dim panel1 As New StatusBarPanel
    Dim panel2 As New StatusBarPanel
    Dim panel3 As New StatusBarPanel
	
	'UPGRADE_ISSUE: 不支持将参数声明为“As Any”。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"”
    Private Declare Function OSWinHelp Lib "user32" Alias "WinHelpA" (ByVal hwnd As Integer, ByVal HelpFile As String, ByVal wCommand As Short, ByRef dwData As Integer) As Short
	Private Declare Function HtmlHelpA Lib "c:\windows\system32\hhctrl.ocx" (ByVal hwndCaller As Integer, ByVal pszFile As String, ByVal uCommand As Integer, ByVal dwData As Integer) As Integer
	'888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888以下定义为删除最大化和最小化的定义函数
	
	Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Integer, ByVal bRevert As Integer) As Integer
	
	
	Private Declare Function RemoveMenu Lib "user32" (ByVal hMenu As Integer, ByVal nPosition As Integer, ByVal wFlags As Integer) As Integer
	Private Const SC_SIZE As Integer = &HF000
	Private Const MF_REMOVE As Integer = &H1000
	Private Declare Function DeleteMenu Lib "user32" (ByVal hMenu As Integer, ByVal nPosition As Integer, ByVal wFlags As Integer) As Integer
	Private Declare Function SetWindowLong Lib "user32"  Alias "SetWindowLongA"(ByVal hwnd As Integer, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
	Private Declare Function GetWindowLong Lib "user32"  Alias "GetWindowLongA"(ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer
	Private Const WS_MINIMIZEBOX As Integer = &H20000
	Private Const WS_MAXIMIZEBOX As Integer = &H10000
	Private Const SC_CLOSE As Short = &HF060s
	Private Const GWL_STYLE As Short = (-16)
	'得到菜单的项目数
	Private Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Integer) As Integer
    Private Const MF_BYPOSITION As Integer = &H400

    '=================================vb.net 20220727Chad Baobab N1-32升级状态条拷贝
    Public Declare Function GetKeyState Lib "user32" Alias "GetKeyState" (ByVal nVirtKey As Integer) As Integer

    Public Const WM_KEYUP As Integer = &H101


	Public Sub mnuCaMgCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCaMgCal.Popup
		mnuCaMgCal_Click(eventSender, eventArgs)
	End Sub
	Public Sub mnuCaMgCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCaMgCal.Click '钙镁离子计算模块
		FrmCaMgCal.DefInstance.Show()
		FrmCaMgCal.DefInstance.Activate()
	End Sub
	
	Public Sub mnuClCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuClCal.Popup
		mnuClCal_Click(eventSender, eventArgs)
	End Sub
	Public Sub mnuClCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuClCal.Click '氯离子计算模块
		FrmClCal.DefInstance.Show()
		FrmClCal.DefInstance.Activate()
	End Sub
	
	Public Sub mnuKillWellCalBase_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuKillWellCalBase.Popup
		mnuKillWellCalBase_Click(eventSender, eventArgs)
	End Sub
	Public Sub mnuKillWellCalBase_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuKillWellCalBase.Click '压井基本计算
		frmKillWellbasecal.DefInstance.Show()
		frmKillWellbasecal.DefInstance.Activate()
	End Sub
	
    Public Sub mnuKillWellChange_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuKillWellChange.Popup
        mnuKillWellChange_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuKillWellChange_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuKillWellChange.Click '溢流类型判断
        frmKillWellChange.DefInstance.Show()
        frmKillWellChange.DefInstance.Activate()
    End Sub

    Public Sub mnuMudBasePressureCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudBasePressureCal.Popup
        mnuMudBasePressureCal_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudBasePressureCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudBasePressureCal.Click '基本压力计算
        frmBasePressureCal.DefInstance.Show()
        frmBasePressureCal.DefInstance.Activate()
    End Sub

    Public Sub mnuMudCalAnnularspaceVel_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalAnnularspaceVel.Popup
        mnuMudCalAnnularspaceVel_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudCalAnnularspaceVel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalAnnularspaceVel.Click '钻井液环空返速计算
        Form6.DefInstance.Show()
        Form6.DefInstance.Activate()
    End Sub

    Public Sub mnuMudCalCircleTime_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalCircleTime.Popup
        mnuMudCalCircleTime_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudCalCircleTime_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalCircleTime.Click '钻井液循环周计算
        Form5.DefInstance.Show()
        Form5.DefInstance.Activate()
    End Sub

    Public Sub mnuMudCalDisplacement_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalDisplacement.Popup
        mnuMudCalDisplacement_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudCalDisplacement_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalDisplacement.Click '钻具替浆体积计算
        Form8.DefInstance.Show()
        Form8.DefInstance.Activate()
    End Sub

    Public Sub mnuMudCalOilGasVel_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalOilGasVel.Popup
        mnuMudCalOilGasVel_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudCalOilGasVel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalOilGasVel.Click 'oil-gas channelling veloccity  油气上窜速度
        Form7.DefInstance.Show()
        Form7.DefInstance.Activate()
    End Sub

    Public Sub mnuMudCalWellVol_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalWellVol.Popup
        mnuMudCalWellVol_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudCalWellVol_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudCalWellVol.Click '空井筒钻井液体积计算
        Form1.DefInstance.Show()
        Form1.DefInstance.Activate()
    End Sub

    Public Sub mnuMudDataCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudDataCal.Popup
        mnuMudDataCal_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudDataCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudDataCal.Click
        frmHhydraulicCal.DefInstance.Show()
        frmHhydraulicCal.DefInstance.Activate() '流变参数计算
    End Sub

    Public Sub mnuMudHB_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudHB.Popup
        mnuMudHB_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudHB_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudHB.Click '环空岩屑浓度分析
        frmHhydraulic.DefInstance.Show()
        frmHhydraulic.DefInstance.Activate()
    End Sub

    Public Sub mnuMudHyCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudHyCal.Popup
        mnuMudHyCal_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudHyCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudHyCal.Click '测试版本，2006年8月13日
        frmHyCal.DefInstance.Show()
        frmHyCal.DefInstance.Activate()
    End Sub

    Public Sub mnuMudMWHweight_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWHweight.Popup
        mnuMudMWHweight_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudMWHweight_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWHweight.Click '钻井液加重计算heavey weight
        frmMudweight.DefInstance.Show()
        frmMudweight.DefInstance.Activate()
    End Sub

    Public Sub mnuMudMWLweight_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWLweight.Popup
        mnuMudMWLweight_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudMWLweight_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWLweight.Click '两种不同密度钻井液混合后的密度light weight
        Form4.DefInstance.Show()
        Form4.DefInstance.Activate()
    End Sub

    Public Sub mnuMudMWMake_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWMake.Popup
        mnuMudMWMake_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudMWMake_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudMWMake.Click '使用粘土配制低密度钻井液计算
        Form2.DefInstance.Show()
        Form2.DefInstance.Activate()
    End Sub

    Public Sub mnuMudRchang_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudRchang.Popup
        mnuMudRchang_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudRchang_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudRchang.Click
        frmRheometerChange.DefInstance.Show() '钻井液流变模式判别
        frmRheometerChange.DefInstance.Activate()
    End Sub

    Public Sub mnuMudRcLc_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudRcLc.Popup
        mnuMudRcLc_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudRcLc_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudRcLc.Click '环空净化能力评价分析  钻屑滑落速度计算
        frmBVel.DefInstance.Show()
        frmBVel.DefInstance.Activate()
    End Sub

    Public Sub mnuMudSolidContent_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudSolidContent.Popup
        mnuMudSolidContent_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudSolidContent_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudSolidContent.Click 'Solid content of drilling fluid 钻井液固相含量分析计算
        frmSolid.DefInstance.Show()
        frmSolid.DefInstance.Activate()
    End Sub

    Public Sub mnuPHCal_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPHCal.Popup
        mnuPHCal_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuPHCal_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPHCal.Click '钻井液碱度计算模块
        FrmPHCal.DefInstance.Show()
        FrmPHCal.DefInstance.Activate()
    End Sub

    Public Sub mnuShowC_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowC.Popup
        mnuShowC_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuShowC_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowC.Click
        Dim Index As Short = mnuShowC.GetIndex(eventSender) '窗口级联显示 vbCascade
        MDIFrmMud.DefInstance.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade)
    End Sub

    Public Sub mnuShowH_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowH.Popup
        mnuShowH_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuShowH_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowH.Click
        Dim Index As Short = mnuShowH.GetIndex(eventSender) '窗口平铺水平显示vbTileHorizontal
        MDIFrmMud.DefInstance.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal)
    End Sub

    Public Sub mnuShowIcon_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowIcon.Popup
        mnuShowIcon_Click(eventSender, eventArgs)
    End Sub

    Private Sub mnuMudSurgePressure_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMudSurgePressure.Click
        frmSwabPressure.DefInstance.Show()              '激动压力计算
        frmSwabPressure.DefInstance.Activate()
    End Sub

    Private Sub mnuMudSurgePressure_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudSurgePressure.Popup
        mnuMudSurgePressure_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuShowIcon_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowIcon.Click
        Dim Index As Short = mnuShowIcon.GetIndex(eventSender) '窗口图标最小显示
        MDIFrmMud.DefInstance.LayoutMdi(System.Windows.Forms.MdiLayout.ArrangeIcons)
    End Sub

    Public Sub mnuShowV_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowV.Popup
        mnuShowV_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuShowV_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuShowV.Click
        Dim Index As Short = mnuShowV.GetIndex(eventSender) '窗口平铺垂直显示 2
        MDIFrmMud.DefInstance.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical)
    End Sub

    Private Sub mnuSolidoidEfficiency_Click() '固控设备使用效率分析，尚未建模
        MsgBox("未添加模块代码。")
    End Sub

    Public Sub mnuToolCalc_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolCalc.Popup
        mnuToolCalc_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuToolCalc_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolCalc.Click '调用小计算器
        Calculator.DefInstance.ShowDialog()

    End Sub

    Private Sub mnuToolDate_Click()
        'MsgBox "未添加模块代码。"
        On Error Resume Next

        Dim nRet As Short
        nRet = OSWinHelp(Me.Handle.ToInt32, "d:\vbtemp\Sgrjdr.hlp", 3, 0)
        If Err.Number Then
            MsgBox(Err.Description)
        End If
    End Sub

    Public Sub mnuToolFilefind_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolFilefind.Popup
        mnuToolFilefind_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuToolFilefind_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolFilefind.Click '调用 drilldata.chm钻井液技术文章汇总查询
        On Error Resume Next

        HtmlHelpA(Me.Handle.ToInt32, "drilldata.CHM", 0, 0)
        If Err.Number Then
            MsgBox(Err.Description)
        End If
    End Sub

    Public Sub mnuHelpAbout_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpAbout.Popup
        mnuHelpAbout_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuHelpAbout_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpAbout.Click '调用 关于钻井液计算软件窗口

        frmAbout.DefInstance.ShowDialog()
    End Sub

    Public Sub mnuHelpContents_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpContents.Popup
        mnuHelpContents_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuHelpContents_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpContents.Click
        On Error Resume Next

        '调用 mudhelp.chm软件介绍文件

        HtmlHelpA(Me.Handle.ToInt32, "mudhelp.CHM", 0, 0)
        If Err.Number Then
            MsgBox(Err.Description)
        End If
    End Sub

    Public Sub mnuHelpSearch_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpSearch.Popup
        mnuHelpSearch_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuHelpSearch_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpSearch.Click
        On Error Resume Next

       
        HtmlHelpA(Me.Handle.ToInt32, "mudhelp.CHM", 0, 0)
        If Err.Number Then
            MsgBox(Err.Description)
        End If
    End Sub

    Public Sub mnuMudPumpFlow_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudPumpFlow.Popup
        mnuMudPumpFlow_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuMudPumpFlow_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMudPumpFlow.Click '调用泵排量计算软件窗口
        FrmPump.DefInstance.Show()
        FrmPump.DefInstance.Activate()
    End Sub

    Public Sub mnuToolUnit_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolUnit.Popup
        mnuToolUnit_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuToolUnit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuToolUnit.Click '公制与英制单位计量转换
        frmunit.DefInstance.Show()
        frmunit.DefInstance.Activate()
    End Sub


    Private Sub MDIFrmMud_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Me.SetBounds(0, 0, 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)

        ' ********************************************
        ' /--------------------------------------------
        panel0.AutoSize = StatusBarPanelAutoSize.Spring
        panel1.Width = 80
        panel2.Width = 80
        panel3.Width = 150         '设置时间状态条宽度满足显示需求 

        ' Set the text alignment within each panel.
        panel0.Alignment = HorizontalAlignment.Left
        panel1.Alignment = HorizontalAlignment.Left
        panel2.Alignment = HorizontalAlignment.Right
        panel3.Alignment = HorizontalAlignment.Right

        ' Display the first panel without a border and the second
        ' with a raised border.
        panel0.BorderStyle = StatusBarPanelBorderStyle.None
        panel1.BorderStyle = StatusBarPanelBorderStyle.Raised
        panel2.BorderStyle = StatusBarPanelBorderStyle.Raised
        panel3.BorderStyle = StatusBarPanelBorderStyle.Raised

        ' Set the text of the panels. The panel1 object is reserved
        ' for line numbers, while panel3 is set to the current time.

        panel3.Text = System.DateTime.Now.ToShortTimeString

        ' Display panels in statusBar1 and add them to the
        ' status bar's StatusBarPanelCollection.
        staMdifrmMud.ShowPanels = True
        staMdifrmMud.Panels.Add(panel0)
        staMdifrmMud.Panels.Add(panel1)
        staMdifrmMud.Panels.Add(panel2)
        staMdifrmMud.Panels.Add(panel3)
        '添加4个panel

        Me.staMdifrmMud.Panels.Clear()

        Me.staMdifrmMud.Panels.Add("Cap State")

        Me.staMdifrmMud.Panels.Add("Num")

        Me.staMdifrmMud.Panels.Add("Scroll")

        Me.staMdifrmMud.Panels.Add("Now")

        Me.staMdifrmMud.ShowPanels = True

        Timer1.Interval = 500

        Timer1.Enabled = True

        '------------------------------------------启动splash       界面调用  20220728


        frmSplash.DefInstance.Show()

        frmSplash.DefInstance.Activate()


        Dim MdiMenuHwnd As Integer
        Dim hMenu As Integer
        '使MDI父窗体的最大化按钮变灰
        hMenu = GetWindowLong(Me.Handle.ToInt32, GWL_STYLE)
        hMenu = hMenu And Not (WS_MAXIMIZEBOX) '使最大化按钮变灰
        hMenu = SetWindowLong(Me.Handle.ToInt32, GWL_STYLE, hMenu)

        '删除MDI父窗体菜单中的最大化菜单、最小化菜单和关闭菜单
        MdiMenuHwnd = GetSystemMenu(Handle.ToInt32, False)
        'DeleteMenu MdiMenuHwnd, 4, &H400&          '删除最大化菜单
        'DeleteMenu MdiMenuHwnd, 3, &H400&          '删除最小化菜单
        '注意：如果两个都要删除的话必须先删除最大化才可实现
        '屏蔽掉拖拉鼠标窗体

        RemoveMenu(MdiMenuHwnd, SC_SIZE, MF_REMOVE)
        '屏蔽关闭按钮和菜单
        '第一个是删除关闭菜单并使关闭按钮变灰
        'hMenu = GetMenuItemCount(MdiMenuHwnd)
        ' DeleteMenu MdiMenuHwnd, hMenu - 1, MF_BYPOSITION
        '这个是删除菜单分界线
        'hMenu = GetMenuItemCount(MdiMenuHwnd)
        'DeleteMenu MdiMenuHwnd, hMenu - 1, MF_BYPOSITION
        '======================================================================





        '＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝vb.net更新编程后添加数字键盘、大小写、时间状态条，测试。20220727Chad Baobab N1-32

        Application.AddMessageFilter(Me)




        '初始化状态

        ToggleVirtualKeyStae()

    End Sub
    '＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝vb.net更新编程后添加数字键盘、大小写、时间状态条，测试。20220727Chad Baobab N1-32

    Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage

        Select Case m.Msg

            Case WM_KEYUP

                ToggleVirtualKeyStae()

        End Select

        Return False

    End Function
    '＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝vb.net更新编程后添加数字键盘、大小写、时间状态条，测试。20220727Chad Baobab N1-32

    Sub ToggleVirtualKeyStae()

        If GetKeyState(Keys.CapsLock) = 1 Then

            Me.staMdifrmMud.Panels(0).Text = "Cap"

        Else

            Me.staMdifrmMud.Panels(0).Text = ""

        End If

        If GetKeyState(Keys.NumLock) = 1 Then

            Me.staMdifrmMud.Panels(1).Text = "NUM"

        Else

            Me.staMdifrmMud.Panels(1).Text = ""

        End If

        If GetKeyState(Keys.Scroll) = 1 Then

            Me.staMdifrmMud.Panels(2).Text = "SCROLL"

        Else

            Me.staMdifrmMud.Panels(2).Text = ""

        End If

    End Sub

    Private Sub MDIFrmMud_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        ToggleVirtualKeyStae()





    End Sub
    '＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝vb.net更新编程后添加数字键盘、大小写、时间状态条，测试。20220727Chad Baobab N1-32

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick




        Me.staMdifrmMud.Panels(3).Text = Now.ToString

        'Me.staMdifrmMud.Panels(3).Text = DateTime.Now.ToShortTimeString()

    End Sub




    Public Sub mnuQuit_Popup(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuQuit.Popup
        mnuQuit_Click(eventSender, eventArgs)
    End Sub
    Public Sub mnuQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuQuit.Click
        Dim Index As Short = mnuQuit.GetIndex(eventSender) '退出钻井液计算软件
        End
    End Sub


    Private Overloads Sub TlbMudToolIcon_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles TlbMudToolIcon.ButtonClick
        On Error Resume Next


        Select Case TlbMudToolIcon.Buttons.IndexOf(e.Button)
            Case 0
                frmSolid.DefInstance.Show()
                frmSolid.DefInstance.Activate()

            Case 1
                frmHyCal.DefInstance.Show()
                frmHyCal.DefInstance.Activate()

            Case 2
                frmRheometerChange.DefInstance.Show()
                frmRheometerChange.DefInstance.Activate()

            Case 3
                frmHhydraulic.DefInstance.Show()
                frmHhydraulic.DefInstance.Activate()
            Case 4
                frmBVel.DefInstance.Show()
                frmBVel.DefInstance.Activate()

            Case 5
                '调用钻具排放体积计算模块
                Form8.DefInstance.Show()
                Form8.DefInstance.Activate()

            Case 6
                '循环周时间计算模块
                Form5.DefInstance.Show()
                Form5.DefInstance.Activate()

            Case 7
                '"OilGasVel"
                Form7.DefInstance.Show()
                Form7.DefInstance.Activate()

            Case 8
                '"Annularspace"
                Form6.DefInstance.Show()
                Form6.DefInstance.Activate()

            Case 9
                '"pump"
                FrmPump.DefInstance.Show()
                FrmPump.DefInstance.Activate()

            Case 10
                '"Weighting"四种类型tab模式
                frmMudweight.DefInstance.Show()
                frmMudweight.DefInstance.Activate()

            Case 11
                '钻井液滤液碱度和泥浆碱度测试计算模块
                FrmPHCal.DefInstance.Show()
                FrmPHCal.DefInstance.Activate()

            Case 12
                ' 流变参数计算模块
                frmHhydraulicCal.DefInstance.Show()
                frmHhydraulicCal.DefInstance.Activate()

            Case 13
                '"LWeighting"
                Form4.DefInstance.Show()
                Form4.DefInstance.Activate()

            Case 14
                '"WellVol"
                Form1.DefInstance.Show()
                Form1.DefInstance.Activate()

            Case 15
                '"Benntontie"
                Form2.DefInstance.Show()
                Form2.DefInstance.Activate()

            Case 16
                '*********************************************
                '
                '压井计算模块
                '
                '*******************************************

                frmKillWellbasecal.DefInstance.Show()
                frmKillWellbasecal.DefInstance.Activate()

            Case 17
                '"Calculator"
                'Shell ("c:\windows\system32\calc.exe")      '应该修改并完善，缺少调用操作系统目录的功能。可以调用
                Calculator.DefInstance.Show()

            Case 18
                '"unittool"计量单位转换模块
                frmunit.DefInstance.Show()
                frmunit.DefInstance.Activate()

            Case 19
                '"filefind"手册模块
                HtmlHelpA(Me.Handle.ToInt32, "drilldata.CHM", 0, 0)

            Case 20
                '"FrmAbout"about 对话框版权声明

                frmAbout.DefInstance.Show()

            Case 21
                '"帮助"
                On Error Resume Next
                
                HtmlHelpA(Me.Handle.ToInt32, "mudhelp.CHM", 0, 0)
                If Err.Number Then
                    MsgBox(Err.Description)
                End If

            Case 22
                '"exit"退出软件
                End


            Case "Benntontie"
                Form2.DefInstance.Show()
                Form2.DefInstance.Activate()




        End Select
    End Sub


    Private Sub staMdifrmMud_PanelClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.StatusBarPanelClickEventArgs) Handles staMdifrmMud.PanelClick

    End Sub
End Class