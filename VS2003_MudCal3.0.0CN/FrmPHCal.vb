Option Strict Off
Option Explicit On
Friend Class FrmPHCal
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdOk As System.Windows.Forms.Button
	Public WithEvents txtHelp As System.Windows.Forms.TextBox
	Public WithEvents FraHelp As System.Windows.Forms.GroupBox
	Public WithEvents txtHCO3 As System.Windows.Forms.TextBox
	Public WithEvents txtCO3 As System.Windows.Forms.TextBox
	Public WithEvents txtOH As System.Windows.Forms.TextBox
	Public WithEvents txtExplain As System.Windows.Forms.TextBox
	Public WithEvents txtMfPf As System.Windows.Forms.TextBox
	Public WithEvents txtCalc As System.Windows.Forms.TextBox
	Public WithEvents txtPm As System.Windows.Forms.TextBox
	Public WithEvents txtMf As System.Windows.Forms.TextBox
	Public WithEvents txtPf As System.Windows.Forms.TextBox
	Public WithEvents lblHCO3 As System.Windows.Forms.Label
	Public WithEvents lblCO3 As System.Windows.Forms.Label
	Public WithEvents lblOH As System.Windows.Forms.Label
	Public WithEvents lblMfPf As System.Windows.Forms.Label
	Public WithEvents lblCalc As System.Windows.Forms.Label
	Public WithEvents lblPm As System.Windows.Forms.Label
	Public WithEvents lblMf As System.Windows.Forms.Label
	Public WithEvents lblPf As System.Windows.Forms.Label
	Public WithEvents fraDataOutput As System.Windows.Forms.GroupBox
	Public WithEvents txtFwater As System.Windows.Forms.TextBox
	Public WithEvents txtMudV As System.Windows.Forms.TextBox
	Public WithEvents txtMudVPhenolphthalein As System.Windows.Forms.TextBox
	Public WithEvents txtCVitriol As System.Windows.Forms.TextBox
	Public WithEvents txtVPhenolphthalein As System.Windows.Forms.TextBox
	Public WithEvents txtFiltrateSample As System.Windows.Forms.TextBox
	Public WithEvents txtVMethylic As System.Windows.Forms.TextBox
	Public WithEvents lblFwater As System.Windows.Forms.Label
	Public WithEvents lblMudV As System.Windows.Forms.Label
	Public WithEvents lblMudVPhenolphthalein As System.Windows.Forms.Label
	Public WithEvents lblVMethylic As System.Windows.Forms.Label
	Public WithEvents LblFiltrateSample As System.Windows.Forms.Label
	Public WithEvents LblVPhenolphthalein As System.Windows.Forms.Label
	Public WithEvents LblCVitriol As System.Windows.Forms.Label
	Public WithEvents fraDataInput As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmPHCal))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtPm = New System.Windows.Forms.TextBox
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdOk = New System.Windows.Forms.Button
        Me.FraHelp = New System.Windows.Forms.GroupBox
        Me.txtHelp = New System.Windows.Forms.TextBox
        Me.fraDataOutput = New System.Windows.Forms.GroupBox
        Me.txtHCO3 = New System.Windows.Forms.TextBox
        Me.txtCO3 = New System.Windows.Forms.TextBox
        Me.txtOH = New System.Windows.Forms.TextBox
        Me.txtExplain = New System.Windows.Forms.TextBox
        Me.txtMfPf = New System.Windows.Forms.TextBox
        Me.txtCalc = New System.Windows.Forms.TextBox
        Me.txtMf = New System.Windows.Forms.TextBox
        Me.txtPf = New System.Windows.Forms.TextBox
        Me.lblHCO3 = New System.Windows.Forms.Label
        Me.lblCO3 = New System.Windows.Forms.Label
        Me.lblOH = New System.Windows.Forms.Label
        Me.lblMfPf = New System.Windows.Forms.Label
        Me.lblCalc = New System.Windows.Forms.Label
        Me.lblPm = New System.Windows.Forms.Label
        Me.lblMf = New System.Windows.Forms.Label
        Me.lblPf = New System.Windows.Forms.Label
        Me.fraDataInput = New System.Windows.Forms.GroupBox
        Me.txtFwater = New System.Windows.Forms.TextBox
        Me.txtMudV = New System.Windows.Forms.TextBox
        Me.txtMudVPhenolphthalein = New System.Windows.Forms.TextBox
        Me.txtCVitriol = New System.Windows.Forms.TextBox
        Me.txtVPhenolphthalein = New System.Windows.Forms.TextBox
        Me.txtFiltrateSample = New System.Windows.Forms.TextBox
        Me.txtVMethylic = New System.Windows.Forms.TextBox
        Me.lblFwater = New System.Windows.Forms.Label
        Me.lblMudV = New System.Windows.Forms.Label
        Me.lblMudVPhenolphthalein = New System.Windows.Forms.Label
        Me.lblVMethylic = New System.Windows.Forms.Label
        Me.LblFiltrateSample = New System.Windows.Forms.Label
        Me.LblVPhenolphthalein = New System.Windows.Forms.Label
        Me.LblCVitriol = New System.Windows.Forms.Label
        Me.FraHelp.SuspendLayout()
        Me.fraDataOutput.SuspendLayout()
        Me.fraDataInput.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtPm
        '
        Me.txtPm.AcceptsReturn = True
        Me.txtPm.AutoSize = False
        Me.txtPm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPm.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPm.Location = New System.Drawing.Point(221, 102)
        Me.txtPm.MaxLength = 0
        Me.txtPm.Name = "txtPm"
        Me.txtPm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPm.Size = New System.Drawing.Size(97, 20)
        Me.txtPm.TabIndex = 9
        Me.txtPm.Text = ""
        Me.txtPm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtPm, "实际生产中，钻井液中一般保留<200mg/l的Ca++含量，以利于钻井液性能的稳定。")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Window
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicLogo.Location = New System.Drawing.Point(694, 507)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(102, 60)
        Me.PicLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PicLogo.TabIndex = 37
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(480, 545)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 14
        Me.cmdExit.Text = "Exit"
        '
        'cmdOk
        '
        Me.cmdOk.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOk.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOk.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdOk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOk.Location = New System.Drawing.Point(480, 517)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOk.Size = New System.Drawing.Size(80, 20)
        Me.cmdOk.TabIndex = 13
        Me.cmdOk.Text = "Run"
        '
        'FraHelp
        '
        Me.FraHelp.BackColor = System.Drawing.SystemColors.Control
        Me.FraHelp.Controls.Add(Me.txtHelp)
        Me.FraHelp.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.FraHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FraHelp.Location = New System.Drawing.Point(355, 8)
        Me.FraHelp.Name = "FraHelp"
        Me.FraHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FraHelp.Size = New System.Drawing.Size(533, 490)
        Me.FraHelp.TabIndex = 21
        Me.FraHelp.TabStop = False
        Me.FraHelp.Text = "测试程序与污染物的处理方法"
        '
        'txtHelp
        '
        Me.txtHelp.AcceptsReturn = True
        Me.txtHelp.AutoSize = False
        Me.txtHelp.BackColor = System.Drawing.SystemColors.Control
        Me.txtHelp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHelp.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(0, Byte), CType(192, Byte))
        Me.txtHelp.Location = New System.Drawing.Point(10, 26)
        Me.txtHelp.MaxLength = 0
        Me.txtHelp.Multiline = True
        Me.txtHelp.Name = "txtHelp"
        Me.txtHelp.ReadOnly = True
        Me.txtHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHelp.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtHelp.Size = New System.Drawing.Size(513, 454)
        Me.txtHelp.TabIndex = 22
        Me.txtHelp.Text = "                            钻井液碱度与石灰含量" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1  仪器与试剂" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  a.  标准酸溶液:  0.02N(0.01mol/l)硫" & _
        "酸或0.02N(0.02mol/l)盐酸标准" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "溶液；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  酚酞指示剂溶液: 将1g酚酞溶于100ml浓度为50%的酒精水溶液中" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "配制而成；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "c." & _
        "  甲基橙指示剂溶液: 将0.1g甲基橙溶于100ml水中配制而成；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  d.  pH计；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  e.  滴定瓶:  100~150ml，最好白色；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & " f." & _
        "  带刻度移液管:  1ml和10ml各一支；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  g.  搅拌棒。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2  测定步骤" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "（1） 测定Pf和Mf" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  a.  用注射器或移液管取1ml或更多" & _
        "一些滤液于滴定瓶中，加入2滴或更多一" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "些酚酞指示剂溶液。如果显示粉红色，则用移液管逐滴加入0.02N 的硫酸并" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "不断搅拌，至粉红色恰好消失为止。如果样品颜色" & _
        "较深不能判断颜色变化，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "则可用pH计测定试样的变化，当pH值降至8.3时即为滴定终点。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  b.  记录所消耗的0.02N硫酸溶液的体积(V1 ml)。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "  c.  在上述试样中再加入2~3滴甲基橙指示剂溶液，用移液管逐滴加入" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "0.02N硫酸溶液并不断搅拌，直到颜色从黄色变为粉红色为止(如果用pH计，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "则pH" & _
        "值降到4.3时" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "即达到滴定终点)。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "  d.  记录加入甲基橙指示剂后所滴加的0.02N硫酸溶液的体积(V2 ml)。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      计算：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      " & _
        "                   V1" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "        滤液酚酞碱度Pf (ml) =－－－－－－－－－－－ " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "        滤液试样体积(ml)  " & _
        "" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                                  V1  +   V2" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "滤液甲基橙碱度Mf (ml) =－－－－－－－－－－－ " & _
        " " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                                滤液试样体积(ml) " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "（2） 测定Pm" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "a.  用注射器或移液管取1ml或更多一" & _
        "些泥浆于滴定瓶中，加入25~50ml蒸" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "馏水，再加入4~5滴酚酞指示剂溶液。边搅拌边用0.02N 的硫酸溶液迅速" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "滴定到粉红色消失，若用pH计测定试样的变化" & _
        "，则当pH值降至8.3时即为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "滴定终点。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "b." & Microsoft.VisualBasic.ChrW(9) & "记录所消耗的0.02N硫酸溶液的体积(V3 ml)。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      计算:" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "             " & _
        "                    V3" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      钻井液酚酞碱度Pm (ml) = －－－－－－－－－－" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                     " & _
        "         钻井液试样体积(ml)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "     石灰含量＝0.74(PM－Pf×FW)      (单位：Kg/m^3)" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "式中：PM－钻井液碱度；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & " " & _
        "     Pf－滤液酚酞碱度；" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      FW－钻井液中水的体积百分含量，由固相含量测定仪测出。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "                        " & _
        " 碳酸根/碳酸氢根(CO3--/HCO-)污染" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      当水基钻井液被碳酸盐污染时，流变性与失水便会出现问题，碳酸盐" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "依钻井液中pH值的不同以三种不" & _
        "同形式出现，这些形式是H2CO3、" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "HCO3-、CO32-。当pH低于5时，主要是H2CO3；pH 8～9时，主要" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "是HCO3-；pH大于12，主要是CO3" & _
        "--。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "碳酸盐可来源于：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1）处理钙或水泥污染时处理量过大。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2）从地层气、配浆泵和钻井液泵进入钻井液的CO2气的积累。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "3）有机化合物如铁络盐、木质素" & _
        "等在温度大于300℃时的热降解。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "4）受污染的重晶石。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1 碳酸盐的检测" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      若要准确检测碳酸盐，需使用一套叫GARRETT GAS TRAI" & _
        "N 的装置，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "这里不详细介绍。现场工程师往往用检测Mf和Pf来粗略估算碳酸盐的污" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "染情况，当Mf/Pf大于3时，认为有HCO3-污染；当Mf/Pf大于5时，" & _
        "有" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "较严重的CO3--污染。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2 碳酸盐污染的处理" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    大多数钻井液中碳酸根的浓度约在1200～2400mg/l之间，有些钻井" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "液在这浓度超过一" & _
        "倍时不受影响而有些在1200mg/l浓度时却大受影响，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "钻井液所能接受的碳酸根浓度取决于该钻井液的固相含量、温度和各种" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "化学材料的浓度。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      如果" & _
        "已证实流变性和失水的问题是由碳酸根污染所引起的，处理的方" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "法就是加入Ca++使其生成CaCO3沉淀，Ca++以石灰或石膏的形式加" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "入，如果用的是石膏，石灰或" & _
        "烧碱必须同时加入以使HCO3-转变成CO3--" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "，否则HCO3－ 与Ca++是不起反应的。如果使用石灰，pH值将增加，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "可能需要加入石膏或铁络盐来缓冲PH值" & _
        "的增大，不要加入木质素，因为" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "木质素会与石膏和石灰反应其结果会影响后者别的化学反应，另外推荐" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "不要把所有的碳酸盐都反应完，至少要留有1000～2000mg/" & _
        "l浓度的碳" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "酸盐在钻井液中，所发生的化学反应和处理浓度如下：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "加石灰：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "2Ca(OH)2 + HCO3- + CO3-- →  2CaCO3↓ + 3" & _
        "OH- + H2O" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "加石膏和石灰或石膏和烧碱：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    2Ca2＋ + OH- + HCO3- + CO3-- →  2CaCO3↓+ H2O " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & _
        "处理所需量如下：" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1.0mg/l CO3--需0.00123 kg/m3石灰" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1.0mg/l CO3--需0.00285 kg/m3石膏" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1.0mg/l " & _
        "HCO3-需0.000599 kg/m3石灰" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "1.0mg/l HCO3-需0.000656 kg/m3烧碱" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "3 处理评价" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      确认是碳酸根污染" & _
        "问题时，滤液中钙的存在并不表明无碳酸根污染，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "测总硬度时检出的钙可能不与碳酸根反应，通常螯合作用会减低反应速" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "率，化合价的变化也使所测到的钙难于和碳酸盐反应" & _
        "，至少应有100～" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "200mg/l的钙离子浓度才能确保有足够量的游离钙与碳酸根反应。" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "      按常规钻井液的碱度与钻井液中的CO3--、HCO3-和OH" & _
        "-是密切相" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "关的，但其他的碱性物质也会增加钻井液的碱度，这些干扰因素的存在" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "以及检测时的局限性使到分析碳酸根的碱度滴定只是个近似值，所以" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "若用GARRET" & _
        "T GAS TRAIN检测碳酸盐会更精确，不管采用那种方法，" & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "推荐在处理钻井液中的碳酸根时应做一个彻底的小型试验。"
        Me.txtHelp.WordWrap = False
        '
        'fraDataOutput
        '
        Me.fraDataOutput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataOutput.Controls.Add(Me.txtHCO3)
        Me.fraDataOutput.Controls.Add(Me.txtCO3)
        Me.fraDataOutput.Controls.Add(Me.txtOH)
        Me.fraDataOutput.Controls.Add(Me.txtExplain)
        Me.fraDataOutput.Controls.Add(Me.txtMfPf)
        Me.fraDataOutput.Controls.Add(Me.txtCalc)
        Me.fraDataOutput.Controls.Add(Me.txtPm)
        Me.fraDataOutput.Controls.Add(Me.txtMf)
        Me.fraDataOutput.Controls.Add(Me.txtPf)
        Me.fraDataOutput.Controls.Add(Me.lblHCO3)
        Me.fraDataOutput.Controls.Add(Me.lblCO3)
        Me.fraDataOutput.Controls.Add(Me.lblOH)
        Me.fraDataOutput.Controls.Add(Me.lblMfPf)
        Me.fraDataOutput.Controls.Add(Me.lblCalc)
        Me.fraDataOutput.Controls.Add(Me.lblPm)
        Me.fraDataOutput.Controls.Add(Me.lblMf)
        Me.fraDataOutput.Controls.Add(Me.lblPf)
        Me.fraDataOutput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataOutput.ForeColor = System.Drawing.Color.Red
        Me.fraDataOutput.Location = New System.Drawing.Point(8, 292)
        Me.fraDataOutput.Name = "fraDataOutput"
        Me.fraDataOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataOutput.Size = New System.Drawing.Size(327, 286)
        Me.fraDataOutput.TabIndex = 16
        Me.fraDataOutput.TabStop = False
        Me.fraDataOutput.Text = "计算结果"
        '
        'txtHCO3
        '
        Me.txtHCO3.AcceptsReturn = True
        Me.txtHCO3.AutoSize = False
        Me.txtHCO3.BackColor = System.Drawing.SystemColors.Window
        Me.txtHCO3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHCO3.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHCO3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHCO3.Location = New System.Drawing.Point(230, 256)
        Me.txtHCO3.MaxLength = 0
        Me.txtHCO3.Name = "txtHCO3"
        Me.txtHCO3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHCO3.Size = New System.Drawing.Size(68, 20)
        Me.txtHCO3.TabIndex = 33
        Me.txtHCO3.Text = ""
        Me.txtHCO3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCO3
        '
        Me.txtCO3.AcceptsReturn = True
        Me.txtCO3.AutoSize = False
        Me.txtCO3.BackColor = System.Drawing.SystemColors.Window
        Me.txtCO3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCO3.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCO3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCO3.Location = New System.Drawing.Point(120, 256)
        Me.txtCO3.MaxLength = 0
        Me.txtCO3.Name = "txtCO3"
        Me.txtCO3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCO3.Size = New System.Drawing.Size(68, 20)
        Me.txtCO3.TabIndex = 32
        Me.txtCO3.Text = ""
        Me.txtCO3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOH
        '
        Me.txtOH.AcceptsReturn = True
        Me.txtOH.AutoSize = False
        Me.txtOH.BackColor = System.Drawing.SystemColors.Window
        Me.txtOH.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtOH.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOH.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtOH.Location = New System.Drawing.Point(10, 256)
        Me.txtOH.MaxLength = 0
        Me.txtOH.Name = "txtOH"
        Me.txtOH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtOH.Size = New System.Drawing.Size(68, 20)
        Me.txtOH.TabIndex = 31
        Me.txtOH.Text = ""
        Me.txtOH.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtExplain
        '
        Me.txtExplain.AcceptsReturn = True
        Me.txtExplain.AutoSize = False
        Me.txtExplain.BackColor = System.Drawing.SystemColors.Window
        Me.txtExplain.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtExplain.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtExplain.Location = New System.Drawing.Point(10, 195)
        Me.txtExplain.MaxLength = 0
        Me.txtExplain.Name = "txtExplain"
        Me.txtExplain.ReadOnly = True
        Me.txtExplain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtExplain.Size = New System.Drawing.Size(289, 20)
        Me.txtExplain.TabIndex = 12
        Me.txtExplain.Text = ""
        Me.txtExplain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMfPf
        '
        Me.txtMfPf.AcceptsReturn = True
        Me.txtMfPf.AutoSize = False
        Me.txtMfPf.BackColor = System.Drawing.SystemColors.Window
        Me.txtMfPf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMfPf.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMfPf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMfPf.Location = New System.Drawing.Point(221, 163)
        Me.txtMfPf.MaxLength = 0
        Me.txtMfPf.Name = "txtMfPf"
        Me.txtMfPf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMfPf.Size = New System.Drawing.Size(97, 20)
        Me.txtMfPf.TabIndex = 11
        Me.txtMfPf.Text = ""
        Me.txtMfPf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCalc
        '
        Me.txtCalc.AcceptsReturn = True
        Me.txtCalc.AutoSize = False
        Me.txtCalc.BackColor = System.Drawing.SystemColors.Window
        Me.txtCalc.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCalc.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCalc.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCalc.Location = New System.Drawing.Point(221, 137)
        Me.txtCalc.MaxLength = 0
        Me.txtCalc.Name = "txtCalc"
        Me.txtCalc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCalc.Size = New System.Drawing.Size(97, 20)
        Me.txtCalc.TabIndex = 10
        Me.txtCalc.Text = ""
        Me.txtCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMf
        '
        Me.txtMf.AcceptsReturn = True
        Me.txtMf.AutoSize = False
        Me.txtMf.BackColor = System.Drawing.SystemColors.Window
        Me.txtMf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMf.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMf.Location = New System.Drawing.Point(221, 68)
        Me.txtMf.MaxLength = 0
        Me.txtMf.Name = "txtMf"
        Me.txtMf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMf.Size = New System.Drawing.Size(97, 20)
        Me.txtMf.TabIndex = 8
        Me.txtMf.Text = ""
        Me.txtMf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPf
        '
        Me.txtPf.AcceptsReturn = True
        Me.txtPf.AutoSize = False
        Me.txtPf.BackColor = System.Drawing.SystemColors.Window
        Me.txtPf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPf.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPf.Location = New System.Drawing.Point(221, 33)
        Me.txtPf.MaxLength = 0
        Me.txtPf.Name = "txtPf"
        Me.txtPf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPf.Size = New System.Drawing.Size(97, 20)
        Me.txtPf.TabIndex = 7
        Me.txtPf.Text = ""
        Me.txtPf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHCO3
        '
        Me.lblHCO3.BackColor = System.Drawing.SystemColors.Control
        Me.lblHCO3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHCO3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblHCO3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHCO3.Location = New System.Drawing.Point(230, 221)
        Me.lblHCO3.Name = "lblHCO3"
        Me.lblHCO3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHCO3.Size = New System.Drawing.Size(68, 28)
        Me.lblHCO3.TabIndex = 36
        Me.lblHCO3.Text = "HCO3-含量 mg/l"
        Me.lblHCO3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCO3
        '
        Me.lblCO3.BackColor = System.Drawing.SystemColors.Control
        Me.lblCO3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCO3.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblCO3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCO3.Location = New System.Drawing.Point(120, 221)
        Me.lblCO3.Name = "lblCO3"
        Me.lblCO3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCO3.Size = New System.Drawing.Size(68, 28)
        Me.lblCO3.TabIndex = 35
        Me.lblCO3.Text = "CO3--含量 mg/l"
        Me.lblCO3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblOH
        '
        Me.lblOH.BackColor = System.Drawing.SystemColors.Control
        Me.lblOH.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOH.Font = New System.Drawing.Font("宋体", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblOH.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOH.Location = New System.Drawing.Point(10, 221)
        Me.lblOH.Name = "lblOH"
        Me.lblOH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOH.Size = New System.Drawing.Size(68, 28)
        Me.lblOH.TabIndex = 34
        Me.lblOH.Text = "OH-含量mg/l"
        Me.lblOH.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMfPf
        '
        Me.lblMfPf.BackColor = System.Drawing.SystemColors.Control
        Me.lblMfPf.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMfPf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMfPf.Location = New System.Drawing.Point(10, 164)
        Me.lblMfPf.Name = "lblMfPf"
        Me.lblMfPf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMfPf.Size = New System.Drawing.Size(193, 18)
        Me.lblMfPf.TabIndex = 29
        Me.lblMfPf.Text = "Mf/Pf"
        '
        'lblCalc
        '
        Me.lblCalc.BackColor = System.Drawing.SystemColors.Control
        Me.lblCalc.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCalc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCalc.Location = New System.Drawing.Point(10, 138)
        Me.lblCalc.Name = "lblCalc"
        Me.lblCalc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCalc.Size = New System.Drawing.Size(183, 18)
        Me.lblCalc.TabIndex = 20
        Me.lblCalc.Text = "悬浮Ca(OH)2含量，Kg/m^3"
        '
        'lblPm
        '
        Me.lblPm.BackColor = System.Drawing.SystemColors.Control
        Me.lblPm.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPm.Location = New System.Drawing.Point(10, 103)
        Me.lblPm.Name = "lblPm"
        Me.lblPm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPm.Size = New System.Drawing.Size(183, 18)
        Me.lblPm.TabIndex = 19
        Me.lblPm.Text = "钻井液酚酞碱度Pm (ml) "
        '
        'lblMf
        '
        Me.lblMf.BackColor = System.Drawing.SystemColors.Control
        Me.lblMf.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMf.Location = New System.Drawing.Point(10, 69)
        Me.lblMf.Name = "lblMf"
        Me.lblMf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMf.Size = New System.Drawing.Size(183, 18)
        Me.lblMf.TabIndex = 18
        Me.lblMf.Text = "滤液甲基橙碱度Mf (ml) "
        '
        'lblPf
        '
        Me.lblPf.BackColor = System.Drawing.SystemColors.Control
        Me.lblPf.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPf.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPf.Location = New System.Drawing.Point(10, 34)
        Me.lblPf.Name = "lblPf"
        Me.lblPf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPf.Size = New System.Drawing.Size(183, 18)
        Me.lblPf.TabIndex = 17
        Me.lblPf.Text = "滤液酚酞碱度Pf (ml) "
        '
        'fraDataInput
        '
        Me.fraDataInput.BackColor = System.Drawing.SystemColors.Control
        Me.fraDataInput.Controls.Add(Me.txtFwater)
        Me.fraDataInput.Controls.Add(Me.txtMudV)
        Me.fraDataInput.Controls.Add(Me.txtMudVPhenolphthalein)
        Me.fraDataInput.Controls.Add(Me.txtCVitriol)
        Me.fraDataInput.Controls.Add(Me.txtVPhenolphthalein)
        Me.fraDataInput.Controls.Add(Me.txtFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.txtVMethylic)
        Me.fraDataInput.Controls.Add(Me.lblFwater)
        Me.fraDataInput.Controls.Add(Me.lblMudV)
        Me.fraDataInput.Controls.Add(Me.lblMudVPhenolphthalein)
        Me.fraDataInput.Controls.Add(Me.lblVMethylic)
        Me.fraDataInput.Controls.Add(Me.LblFiltrateSample)
        Me.fraDataInput.Controls.Add(Me.LblVPhenolphthalein)
        Me.fraDataInput.Controls.Add(Me.LblCVitriol)
        Me.fraDataInput.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.fraDataInput.ForeColor = System.Drawing.Color.Blue
        Me.fraDataInput.Location = New System.Drawing.Point(8, 8)
        Me.fraDataInput.Name = "fraDataInput"
        Me.fraDataInput.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraDataInput.Size = New System.Drawing.Size(327, 276)
        Me.fraDataInput.TabIndex = 15
        Me.fraDataInput.TabStop = False
        Me.fraDataInput.Text = "数据录入"
        '
        'txtFwater
        '
        Me.txtFwater.AcceptsReturn = True
        Me.txtFwater.AutoSize = False
        Me.txtFwater.BackColor = System.Drawing.SystemColors.Window
        Me.txtFwater.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFwater.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFwater.Location = New System.Drawing.Point(221, 243)
        Me.txtFwater.MaxLength = 0
        Me.txtFwater.Name = "txtFwater"
        Me.txtFwater.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFwater.Size = New System.Drawing.Size(97, 20)
        Me.txtFwater.TabIndex = 6
        Me.txtFwater.Text = ""
        Me.txtFwater.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMudV
        '
        Me.txtMudV.AcceptsReturn = True
        Me.txtMudV.AutoSize = False
        Me.txtMudV.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudV.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudV.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudV.Location = New System.Drawing.Point(221, 207)
        Me.txtMudV.MaxLength = 0
        Me.txtMudV.Name = "txtMudV"
        Me.txtMudV.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudV.Size = New System.Drawing.Size(97, 20)
        Me.txtMudV.TabIndex = 5
        Me.txtMudV.Text = ""
        Me.txtMudV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMudVPhenolphthalein
        '
        Me.txtMudVPhenolphthalein.AcceptsReturn = True
        Me.txtMudVPhenolphthalein.AutoSize = False
        Me.txtMudVPhenolphthalein.BackColor = System.Drawing.SystemColors.Window
        Me.txtMudVPhenolphthalein.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMudVPhenolphthalein.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMudVPhenolphthalein.Location = New System.Drawing.Point(221, 172)
        Me.txtMudVPhenolphthalein.MaxLength = 0
        Me.txtMudVPhenolphthalein.Name = "txtMudVPhenolphthalein"
        Me.txtMudVPhenolphthalein.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMudVPhenolphthalein.Size = New System.Drawing.Size(97, 20)
        Me.txtMudVPhenolphthalein.TabIndex = 4
        Me.txtMudVPhenolphthalein.Text = ""
        Me.txtMudVPhenolphthalein.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCVitriol
        '
        Me.txtCVitriol.AcceptsReturn = True
        Me.txtCVitriol.AutoSize = False
        Me.txtCVitriol.BackColor = System.Drawing.SystemColors.Window
        Me.txtCVitriol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCVitriol.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtCVitriol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCVitriol.Location = New System.Drawing.Point(221, 27)
        Me.txtCVitriol.MaxLength = 0
        Me.txtCVitriol.Name = "txtCVitriol"
        Me.txtCVitriol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCVitriol.Size = New System.Drawing.Size(97, 20)
        Me.txtCVitriol.TabIndex = 0
        Me.txtCVitriol.Text = ""
        Me.txtCVitriol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVPhenolphthalein
        '
        Me.txtVPhenolphthalein.AcceptsReturn = True
        Me.txtVPhenolphthalein.AutoSize = False
        Me.txtVPhenolphthalein.BackColor = System.Drawing.SystemColors.Window
        Me.txtVPhenolphthalein.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVPhenolphthalein.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtVPhenolphthalein.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVPhenolphthalein.Location = New System.Drawing.Point(221, 64)
        Me.txtVPhenolphthalein.MaxLength = 0
        Me.txtVPhenolphthalein.Name = "txtVPhenolphthalein"
        Me.txtVPhenolphthalein.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVPhenolphthalein.Size = New System.Drawing.Size(97, 20)
        Me.txtVPhenolphthalein.TabIndex = 1
        Me.txtVPhenolphthalein.Text = ""
        Me.txtVPhenolphthalein.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFiltrateSample
        '
        Me.txtFiltrateSample.AcceptsReturn = True
        Me.txtFiltrateSample.AutoSize = False
        Me.txtFiltrateSample.BackColor = System.Drawing.SystemColors.Window
        Me.txtFiltrateSample.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFiltrateSample.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.txtFiltrateSample.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFiltrateSample.Location = New System.Drawing.Point(221, 135)
        Me.txtFiltrateSample.MaxLength = 0
        Me.txtFiltrateSample.Name = "txtFiltrateSample"
        Me.txtFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFiltrateSample.Size = New System.Drawing.Size(97, 20)
        Me.txtFiltrateSample.TabIndex = 3
        Me.txtFiltrateSample.Text = ""
        Me.txtFiltrateSample.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVMethylic
        '
        Me.txtVMethylic.AcceptsReturn = True
        Me.txtVMethylic.AutoSize = False
        Me.txtVMethylic.BackColor = System.Drawing.SystemColors.Window
        Me.txtVMethylic.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVMethylic.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVMethylic.Location = New System.Drawing.Point(221, 100)
        Me.txtVMethylic.MaxLength = 0
        Me.txtVMethylic.Name = "txtVMethylic"
        Me.txtVMethylic.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVMethylic.Size = New System.Drawing.Size(97, 20)
        Me.txtVMethylic.TabIndex = 2
        Me.txtVMethylic.Text = ""
        Me.txtVMethylic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFwater
        '
        Me.lblFwater.BackColor = System.Drawing.SystemColors.Control
        Me.lblFwater.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFwater.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFwater.Location = New System.Drawing.Point(10, 244)
        Me.lblFwater.Name = "lblFwater"
        Me.lblFwater.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFwater.Size = New System.Drawing.Size(202, 18)
        Me.lblFwater.TabIndex = 30
        Me.lblFwater.Text = "钻井液中水的体积百分含量,%"
        '
        'lblMudV
        '
        Me.lblMudV.BackColor = System.Drawing.SystemColors.Control
        Me.lblMudV.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMudV.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMudV.Location = New System.Drawing.Point(10, 208)
        Me.lblMudV.Name = "lblMudV"
        Me.lblMudV.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMudV.Size = New System.Drawing.Size(202, 19)
        Me.lblMudV.TabIndex = 28
        Me.lblMudV.Text = "钻井液样品体积,ml"
        '
        'lblMudVPhenolphthalein
        '
        Me.lblMudVPhenolphthalein.BackColor = System.Drawing.SystemColors.Control
        Me.lblMudVPhenolphthalein.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMudVPhenolphthalein.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMudVPhenolphthalein.Location = New System.Drawing.Point(10, 173)
        Me.lblMudVPhenolphthalein.Name = "lblMudVPhenolphthalein"
        Me.lblMudVPhenolphthalein.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMudVPhenolphthalein.Size = New System.Drawing.Size(205, 18)
        Me.lblMudVPhenolphthalein.TabIndex = 27
        Me.lblMudVPhenolphthalein.Text = "钻井液酚酞终点硫酸用量V3,ml"
        '
        'lblVMethylic
        '
        Me.lblVMethylic.BackColor = System.Drawing.SystemColors.Control
        Me.lblVMethylic.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVMethylic.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVMethylic.Location = New System.Drawing.Point(10, 101)
        Me.lblVMethylic.Name = "lblVMethylic"
        Me.lblVMethylic.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVMethylic.Size = New System.Drawing.Size(202, 18)
        Me.lblVMethylic.TabIndex = 26
        Me.lblVMethylic.Text = "滤液甲基橙终点硫酸用量V2,ml"
        '
        'LblFiltrateSample
        '
        Me.LblFiltrateSample.BackColor = System.Drawing.SystemColors.Control
        Me.LblFiltrateSample.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblFiltrateSample.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblFiltrateSample.Location = New System.Drawing.Point(10, 136)
        Me.LblFiltrateSample.Name = "LblFiltrateSample"
        Me.LblFiltrateSample.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblFiltrateSample.Size = New System.Drawing.Size(202, 18)
        Me.LblFiltrateSample.TabIndex = 25
        Me.LblFiltrateSample.Text = "滤液样品体积,ml"
        '
        'LblVPhenolphthalein
        '
        Me.LblVPhenolphthalein.BackColor = System.Drawing.SystemColors.Control
        Me.LblVPhenolphthalein.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblVPhenolphthalein.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblVPhenolphthalein.Location = New System.Drawing.Point(10, 65)
        Me.LblVPhenolphthalein.Name = "LblVPhenolphthalein"
        Me.LblVPhenolphthalein.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblVPhenolphthalein.Size = New System.Drawing.Size(202, 18)
        Me.LblVPhenolphthalein.TabIndex = 24
        Me.LblVPhenolphthalein.Text = "滤液酚酞终点硫酸用量V1,ml"
        '
        'LblCVitriol
        '
        Me.LblCVitriol.BackColor = System.Drawing.SystemColors.Control
        Me.LblCVitriol.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblCVitriol.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblCVitriol.Location = New System.Drawing.Point(10, 28)
        Me.LblCVitriol.Name = "LblCVitriol"
        Me.LblCVitriol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblCVitriol.Size = New System.Drawing.Size(202, 19)
        Me.LblCVitriol.TabIndex = 23
        Me.LblCVitriol.Text = "硫酸标准溶液浓度,N"
        '
        'FrmPHCal
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(893, 588)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.FraHelp)
        Me.Controls.Add(Me.fraDataOutput)
        Me.Controls.Add(Me.fraDataInput)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "FrmPHCal"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液碱度与石灰含量计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.FraHelp.ResumeLayout(False)
        Me.fraDataOutput.ResumeLayout(False)
        Me.fraDataInput.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As FrmPHCal
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As FrmPHCal
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New FrmPHCal()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim CVitriol As Single '硫酸标准溶液的当量浓度
	Dim VPhenolphthalein As Single '滴定滤液酚酞终点中消耗硫酸标准溶液的体积，ml;
	Dim VMethylic As Single '滴定滤液甲基橙终点中消耗硫酸标准溶液的体积，ml;
	Dim VFiltrateSample As Single '滤液样品的体积，ml
	Dim MudVPhenolphthalein As Single '滴定钻井液酚酞终点中消耗硫酸标准溶液的体积，ml;
	Dim MudV As Single '钻井液样品的体积，ml
	Dim Fwater As Single '钻井液中水的体积百分含量， % 。由固相含量测定仪测出。
	Dim Pf As Single '滤液酚酞碱度Pf (ml)
	Dim Mf As Single '滤液甲基橙碱度Mf，ml
	Dim Pm As Single '钻井液酚酞碱度Pm，ml
	Dim Calc As Single '钻井液滤液中石灰含量，Kg/m^3
	Dim MfPf As Single '甲基橙/酚酞碱度比值
	Dim OH As Single 'OH-含量，mg/l
	Dim CO3 As Single 'CO3--含量，mg/l
	Dim HCO3 As Single 'HCO3-含量，mg/l
	'--------------------------------------------------------------------
	'***********************************
	'2012年3月30日填加的屏幕截图----------专用函数
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub cmdok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdok.Click
		CVitriol = Val(txtCVitriol.Text)
		If CVitriol = 0 Then
			MsgBox("请检查你输入的'硫酸标准溶液浓度'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtCVitriol.Text = ""
			Exit Sub
		End If
		
		VPhenolphthalein = Val(txtVPhenolphthalein.Text)
		If VPhenolphthalein = 0 Then
			MsgBox("请检查你输入的'滤液酚酞终点硫酸用量'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtVPhenolphthalein.Text = ""
			Exit Sub
		End If
		
		
		VMethylic = Val(txtVMethylic.Text)
		If VMethylic = 0 Then
			MsgBox("请检查你输入的'滤液甲基橙终点硫酸用量'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtVMethylic.Text = ""
			Exit Sub
		End If
		
		VFiltrateSample = Val(txtFiltrateSample.Text)
		If VFiltrateSample = 0 Then
			MsgBox("请检查你输入的'滤液样品体积'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtFiltrateSample.Text = ""
			Exit Sub
		End If
		
		MudVPhenolphthalein = Val(txtMudVPhenolphthalein.Text)
		If MudVPhenolphthalein = 0 Then
			MsgBox("请检查你输入的'钻井液酚酞终点硫酸用量'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtMudVPhenolphthalein.Text = ""
			Exit Sub
		End If
		
		MudV = Val(txtMudV.Text)
		If MudV = 0 Then
			MsgBox("请检查你输入的'钻井液样品体积'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtMudV.Text = ""
			Exit Sub
		End If
		
		Fwater = Val(txtFwater.Text)
		If Fwater = 0 Then
			MsgBox("请检查你输入的'钻井液中水的体积百分含量'数据是否有效！", MsgBoxStyle.Information, "钻井液碱度与石灰含量计算")
			txtFwater.Text = ""
			Exit Sub
		End If
		
		Pf = ((CVitriol / 0.02) * VPhenolphthalein) / VFiltrateSample '滤液酚酞碱度计算公式，单位ml
		Mf = ((CVitriol / 0.02) * (VMethylic + VPhenolphthalein)) / VFiltrateSample '滤液甲基橙碱度计算公式，单位ml
		Pm = ((CVitriol / 0.02) * MudVPhenolphthalein) / MudV '钻井液酚酞碱度计算公式，单位ml
		Calc = 0.74 * (Pm - Pf * (Fwater / 100)) '钻井液滤液中石灰含量计算公式，，Kg/m^3
		MfPf = Mf / Pf
		
		'数据结果显示
		
		txtPf.Text = VB6.Format(Pf, "0.00")
		txtMf.Text = VB6.Format(Mf, "0.00")
		txtPm.Text = VB6.Format(Pm, "0.00")
		txtCalc.Text = VB6.Format(Calc, "0.000")
		txtMfPf.Text = VB6.Format(MfPf, "0.00")
		If MfPf >= 5 Then
			txtExplain.Text = "钻井液存在严重的CO3--污染，需要进行处理。"
		ElseIf MfPf <= 3 Then 
			txtExplain.Text = "钻井液属轻度CO3--污染。"
		Else
			txtExplain.Text = "钻井液属中度CO3--污染。"
		End If
		
		'关于OH-、CO3--和HCO3-离子含量的计算部分
		If Pf = 0 Then
			OH = 0
			CO3 = 0
			HCO3 = 1220 * Mf
			txtOH.Text = VB6.Format(OH, "0.00")
			txtCO3.Text = VB6.Format(CO3, "0.00")
			txtHCO3.Text = VB6.Format(HCO3, "0.00")
		ElseIf 2 * Pf < Mf Then 
			OH = 0
			CO3 = 1220 * Pf
			HCO3 = 1220 * (Mf - 2 * Pf)
		ElseIf 2 * Pf = Mf Then 
			OH = 0
			CO3 = 1220 * Pf
			HCO3 = 0
		ElseIf 2 * Pf > Mf Then 
			OH = 340 * (2 * Pf - Mf)
			CO3 = 1220 * (Mf - Pf)
			HCO3 = 0
		ElseIf Pf = Mf Then 
			OH = 340 * Mf
			CO3 = 0
			HCO3 = 0
		End If
		
		txtOH.Text = VB6.Format(OH, "0.00")
		txtCO3.Text = VB6.Format(CO3, "0.00")
		txtHCO3.Text = VB6.Format(HCO3, "0.00")
	End Sub
	
	Private Sub FrmPHCal_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************
        '************************************************


        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		'-------------------------------------------
		Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
	End Sub
	
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MAlkalinities.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MAlkalinities.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If

        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdOk.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtCVitriol_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtCVitriol.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCVitriol_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCVitriol.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		CVitriol = Val(txtCVitriol.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtFiltrateSample_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtFiltrateSample.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtFwater_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtFwater.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtFwater_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFwater.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Fwater = Val(txtFwater.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMudV_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMudV.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMudV_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMudV.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		MudV = Val(txtMudV.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtMudVPhenolphthalein_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtMudVPhenolphthalein.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtMudVPhenolphthalein_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMudVPhenolphthalein.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		MudVPhenolphthalein = Val(txtMudVPhenolphthalein.Text)
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtVMethylic_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtVMethylic.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	'数据输入限制，仅支持.0123456780和删除键
	Private Sub txtVPhenolphthalein_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtVPhenolphthalein.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		'UPGRADE_ISSUE: 不支持赋值: KeyAscii 被赋予非零值 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1058"”
		KeyAscii = ValiText(KeyAscii, "0123456789.", True)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtVPhenolphthalein_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVPhenolphthalein.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		VPhenolphthalein = Val(txtVPhenolphthalein.Text)
	End Sub
	
	Private Sub txtFiltrateSample_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFiltrateSample.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		VFiltrateSample = Val(txtFiltrateSample.Text)
	End Sub
	
	Private Sub txtVMethylic_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtVMethylic.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		VMethylic = Val(txtVMethylic.Text)
	End Sub
End Class