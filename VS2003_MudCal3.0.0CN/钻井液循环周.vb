Option Strict Off
Option Explicit On
Friend Class Form5
	Inherits System.Windows.Forms.Form
#Region "Windows 窗体设计器生成的代码"
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'对于启动窗体，所创建的第一个实例为默认实例。
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'此调用是 Windows 窗体设计器所必需的。
		InitializeComponent()
		'此窗体是 MDI 子窗体。
		'此代码模拟 VB6 
		' 的自动加载和显示
		' MDI 子级的父级
		' 的功能。
		Me.MDIParent = 工程1.MDIFrmMud.DefInstance.DefInstance
		工程1.MDIFrmMud.DefInstance.DefInstance.Show
		'VB6 项目中的 MDI 窗体已将其
		'AutoShowChildren 属性设置为“True”
		'要模拟 VB6 行为，需要
		'在每次加载窗体时自动显示
		'该窗体。如果您不需要此行为，
		'请删除以下代码行
		'UPGRADE_NOTE: 移除下一行代码以阻止窗体自动显示。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2018"”
		Me.Show
	End Sub
	'窗体重写处置，以清理组件列表。
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows 窗体设计器所必需的
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents PicLogo As System.Windows.Forms.PictureBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdok As System.Windows.Forms.Button
	Public WithEvents Text19 As System.Windows.Forms.TextBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents Text17 As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents Text13 As System.Windows.Forms.TextBox
	Public WithEvents Text12 As System.Windows.Forms.TextBox
	Public WithEvents Label23 As System.Windows.Forms.Label
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Combo1 As System.Windows.Forms.ComboBox
	Public WithEvents Combo2 As System.Windows.Forms.ComboBox
	Public WithEvents Combo3 As System.Windows.Forms.ComboBox
	Public WithEvents Combo4 As System.Windows.Forms.ComboBox
	Public WithEvents Combo5 As System.Windows.Forms.ComboBox
	Public WithEvents Combo7 As System.Windows.Forms.ComboBox
	Public WithEvents Combo8 As System.Windows.Forms.ComboBox
	Public WithEvents Combo9 As System.Windows.Forms.ComboBox
	Public WithEvents Combo10 As System.Windows.Forms.ComboBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents Combo6 As System.Windows.Forms.ComboBox
	Public WithEvents Combo11 As System.Windows.Forms.ComboBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	'注意: 以下过程是 Windows 窗体设计器所必需的
	'可以使用 Windows 窗体设计器来修改它。
	'不要使用代码编辑器修改它。
    Friend WithEvents picOilGasOutflow As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form5))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text9 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.PicLogo = New System.Windows.Forms.PictureBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdok = New System.Windows.Forms.Button
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.Text19 = New System.Windows.Forms.TextBox
        Me.Text18 = New System.Windows.Forms.TextBox
        Me.Text17 = New System.Windows.Forms.TextBox
        Me.Text16 = New System.Windows.Forms.TextBox
        Me.Text15 = New System.Windows.Forms.TextBox
        Me.Text14 = New System.Windows.Forms.TextBox
        Me.Text13 = New System.Windows.Forms.TextBox
        Me.Text12 = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.Combo1 = New System.Windows.Forms.ComboBox
        Me.Combo2 = New System.Windows.Forms.ComboBox
        Me.Combo3 = New System.Windows.Forms.ComboBox
        Me.Combo4 = New System.Windows.Forms.ComboBox
        Me.Combo5 = New System.Windows.Forms.ComboBox
        Me.Combo7 = New System.Windows.Forms.ComboBox
        Me.Combo8 = New System.Windows.Forms.ComboBox
        Me.Combo9 = New System.Windows.Forms.ComboBox
        Me.Combo10 = New System.Windows.Forms.ComboBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Combo6 = New System.Windows.Forms.ComboBox
        Me.Combo11 = New System.Windows.Forms.ComboBox
        Me.Text11 = New System.Windows.Forms.TextBox
        Me.Text10 = New System.Windows.Forms.TextBox
        Me.Text8 = New System.Windows.Forms.TextBox
        Me.Text7 = New System.Windows.Forms.TextBox
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.picOilGasOutflow = New System.Windows.Forms.PictureBox
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Text5
        '
        Me.Text5.AcceptsReturn = True
        Me.Text5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text5.AutoSize = False
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.Location = New System.Drawing.Point(670, 241)
        Me.Text5.MaxLength = 0
        Me.Text5.Name = "Text5"
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.Size = New System.Drawing.Size(68, 22)
        Me.Text5.TabIndex = 35
        Me.Text5.Text = ""
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text5, "请点击鼠标左键，自动取得长度数据。")
        '
        'Text9
        '
        Me.Text9.AcceptsReturn = True
        Me.Text9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text9.AutoSize = False
        Me.Text9.BackColor = System.Drawing.SystemColors.Window
        Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text9.Location = New System.Drawing.Point(242, 134)
        Me.Text9.MaxLength = 0
        Me.Text9.Name = "Text9"
        Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text9.Size = New System.Drawing.Size(69, 23)
        Me.Text9.TabIndex = 4
        Me.Text9.Text = "1.05"
        Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.Text9, "数据输入举例：井径扩大率9.5％，应在此填写：1.095。")
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(12, 142)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(222, 18)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "裸眼井径扩大率"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.ToolTip1.SetToolTip(Me.Label12, "举例：1.05")
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.SystemColors.Control
        Me.PicLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PicLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.PicLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PicLogo.Location = New System.Drawing.Point(636, 409)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PicLogo.Size = New System.Drawing.Size(174, 61)
        Me.PicLogo.TabIndex = 57
        Me.PicLogo.TabStop = False
        '
        'cmdExit
        '
        Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdExit.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExit.Location = New System.Drawing.Point(683, 364)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExit.Size = New System.Drawing.Size(80, 20)
        Me.cmdExit.TabIndex = 9
        Me.cmdExit.Text = "Exit"
        '
        'cmdok
        '
        Me.cmdok.BackColor = System.Drawing.SystemColors.Control
        Me.cmdok.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdok.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.cmdok.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdok.Location = New System.Drawing.Point(683, 336)
        Me.cmdok.Name = "cmdok"
        Me.cmdok.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdok.Size = New System.Drawing.Size(80, 20)
        Me.cmdok.TabIndex = 8
        Me.cmdok.Text = "Run"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Control
        Me.Frame2.Controls.Add(Me.Text19)
        Me.Frame2.Controls.Add(Me.Text18)
        Me.Frame2.Controls.Add(Me.Text17)
        Me.Frame2.Controls.Add(Me.Text16)
        Me.Frame2.Controls.Add(Me.Text15)
        Me.Frame2.Controls.Add(Me.Text14)
        Me.Frame2.Controls.Add(Me.Text13)
        Me.Frame2.Controls.Add(Me.Text12)
        Me.Frame2.Controls.Add(Me.Label23)
        Me.Frame2.Controls.Add(Me.Label22)
        Me.Frame2.Controls.Add(Me.Label21)
        Me.Frame2.Controls.Add(Me.Label20)
        Me.Frame2.Controls.Add(Me.Label19)
        Me.Frame2.Controls.Add(Me.Label18)
        Me.Frame2.Controls.Add(Me.Label16)
        Me.Frame2.Controls.Add(Me.Label15)
        Me.Frame2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame2.ForeColor = System.Drawing.Color.Red
        Me.Frame2.Location = New System.Drawing.Point(8, 302)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(618, 168)
        Me.Frame2.TabIndex = 13
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "结果"
        '
        'Text19
        '
        Me.Text19.AcceptsReturn = True
        Me.Text19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text19.AutoSize = False
        Me.Text19.BackColor = System.Drawing.SystemColors.Window
        Me.Text19.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text19.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text19.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text19.Location = New System.Drawing.Point(518, 129)
        Me.Text19.MaxLength = 0
        Me.Text19.Name = "Text19"
        Me.Text19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text19.Size = New System.Drawing.Size(78, 24)
        Me.Text19.TabIndex = 56
        Me.Text19.Text = ""
        Me.Text19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text18
        '
        Me.Text18.AcceptsReturn = True
        Me.Text18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text18.AutoSize = False
        Me.Text18.BackColor = System.Drawing.SystemColors.Window
        Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text18.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text18.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text18.Location = New System.Drawing.Point(518, 95)
        Me.Text18.MaxLength = 0
        Me.Text18.Name = "Text18"
        Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text18.Size = New System.Drawing.Size(78, 23)
        Me.Text18.TabIndex = 54
        Me.Text18.Text = ""
        Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text17
        '
        Me.Text17.AcceptsReturn = True
        Me.Text17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text17.AutoSize = False
        Me.Text17.BackColor = System.Drawing.SystemColors.Window
        Me.Text17.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text17.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text17.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text17.Location = New System.Drawing.Point(518, 60)
        Me.Text17.MaxLength = 0
        Me.Text17.Name = "Text17"
        Me.Text17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text17.Size = New System.Drawing.Size(78, 24)
        Me.Text17.TabIndex = 53
        Me.Text17.Text = ""
        Me.Text17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text16
        '
        Me.Text16.AcceptsReturn = True
        Me.Text16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text16.AutoSize = False
        Me.Text16.BackColor = System.Drawing.SystemColors.Window
        Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text16.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text16.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text16.Location = New System.Drawing.Point(518, 26)
        Me.Text16.MaxLength = 0
        Me.Text16.Name = "Text16"
        Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text16.Size = New System.Drawing.Size(78, 24)
        Me.Text16.TabIndex = 52
        Me.Text16.Text = ""
        Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text15
        '
        Me.Text15.AcceptsReturn = True
        Me.Text15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text15.AutoSize = False
        Me.Text15.BackColor = System.Drawing.SystemColors.Window
        Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text15.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text15.Location = New System.Drawing.Point(211, 129)
        Me.Text15.MaxLength = 0
        Me.Text15.Name = "Text15"
        Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text15.Size = New System.Drawing.Size(78, 24)
        Me.Text15.TabIndex = 28
        Me.Text15.Text = ""
        Me.Text15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text14
        '
        Me.Text14.AcceptsReturn = True
        Me.Text14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text14.AutoSize = False
        Me.Text14.BackColor = System.Drawing.SystemColors.Window
        Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text14.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text14.Location = New System.Drawing.Point(211, 94)
        Me.Text14.MaxLength = 0
        Me.Text14.Name = "Text14"
        Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text14.Size = New System.Drawing.Size(78, 23)
        Me.Text14.TabIndex = 12
        Me.Text14.Text = ""
        Me.Text14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text13
        '
        Me.Text13.AcceptsReturn = True
        Me.Text13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text13.AutoSize = False
        Me.Text13.BackColor = System.Drawing.SystemColors.Window
        Me.Text13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text13.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text13.Location = New System.Drawing.Point(211, 58)
        Me.Text13.MaxLength = 0
        Me.Text13.Name = "Text13"
        Me.Text13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text13.Size = New System.Drawing.Size(78, 24)
        Me.Text13.TabIndex = 11
        Me.Text13.Text = ""
        Me.Text13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text12
        '
        Me.Text12.AcceptsReturn = True
        Me.Text12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text12.AutoSize = False
        Me.Text12.BackColor = System.Drawing.SystemColors.Window
        Me.Text12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text12.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Text12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text12.Location = New System.Drawing.Point(211, 26)
        Me.Text12.MaxLength = 0
        Me.Text12.Name = "Text12"
        Me.Text12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text12.Size = New System.Drawing.Size(78, 24)
        Me.Text12.TabIndex = 10
        Me.Text12.Text = ""
        Me.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label23.Location = New System.Drawing.Point(298, 138)
        Me.Label23.Name = "Label23"
        Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label23.Size = New System.Drawing.Size(212, 18)
        Me.Label23.TabIndex = 55
        Me.Label23.Text = "钻具(钻铤、钻杆)体积(m^3)"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(326, 103)
        Me.Label22.Name = "Label22"
        Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label22.Size = New System.Drawing.Size(184, 19)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "钻具内钻井液体积(m^3)"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(326, 69)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label21.Size = New System.Drawing.Size(184, 18)
        Me.Label21.TabIndex = 50
        Me.Label21.Text = "环空钻井液体积(m^3)"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(326, 34)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label20.Size = New System.Drawing.Size(184, 19)
        Me.Label20.TabIndex = 49
        Me.Label20.Text = "钻井液总循环量(m^3)"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(10, 138)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label19.Size = New System.Drawing.Size(193, 18)
        Me.Label19.TabIndex = 27
        Me.Label19.Text = "井眼钻井液体积(m^3)"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(10, 103)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(193, 19)
        Me.Label18.TabIndex = 26
        Me.Label18.Text = "下行时间(min)"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(10, 69)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(193, 18)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "上行时间(min)"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(10, 34)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(193, 19)
        Me.Label15.TabIndex = 23
        Me.Label15.Text = "总循环时间(min)"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.Combo1)
        Me.Frame1.Controls.Add(Me.Combo2)
        Me.Frame1.Controls.Add(Me.Combo3)
        Me.Frame1.Controls.Add(Me.Combo4)
        Me.Frame1.Controls.Add(Me.Combo5)
        Me.Frame1.Controls.Add(Me.Combo7)
        Me.Frame1.Controls.Add(Me.Combo8)
        Me.Frame1.Controls.Add(Me.Combo9)
        Me.Frame1.Controls.Add(Me.Combo10)
        Me.Frame1.Controls.Add(Me.Text1)
        Me.Frame1.Controls.Add(Me.Text2)
        Me.Frame1.Controls.Add(Me.Text3)
        Me.Frame1.Controls.Add(Me.Text4)
        Me.Frame1.Controls.Add(Me.Text5)
        Me.Frame1.Controls.Add(Me.Combo6)
        Me.Frame1.Controls.Add(Me.Combo11)
        Me.Frame1.Controls.Add(Me.Text11)
        Me.Frame1.Controls.Add(Me.Text10)
        Me.Frame1.Controls.Add(Me.Text9)
        Me.Frame1.Controls.Add(Me.Text8)
        Me.Frame1.Controls.Add(Me.Text7)
        Me.Frame1.Controls.Add(Me.Text6)
        Me.Frame1.Controls.Add(Me.Label1)
        Me.Frame1.Controls.Add(Me.Label2)
        Me.Frame1.Controls.Add(Me.Label3)
        Me.Frame1.Controls.Add(Me.Label4)
        Me.Frame1.Controls.Add(Me.Label5)
        Me.Frame1.Controls.Add(Me.Label17)
        Me.Frame1.Controls.Add(Me.Label14)
        Me.Frame1.Controls.Add(Me.Label13)
        Me.Frame1.Controls.Add(Me.Label12)
        Me.Frame1.Controls.Add(Me.Label11)
        Me.Frame1.Controls.Add(Me.Label10)
        Me.Frame1.Controls.Add(Me.Label9)
        Me.Frame1.Controls.Add(Me.Label8)
        Me.Frame1.Controls.Add(Me.Label7)
        Me.Frame1.Controls.Add(Me.Label6)
        Me.Frame1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Frame1.ForeColor = System.Drawing.Color.Red
        Me.Frame1.Location = New System.Drawing.Point(8, 8)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(760, 286)
        Me.Frame1.TabIndex = 0
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "数据录入"
        '
        'Combo1
        '
        Me.Combo1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo1.BackColor = System.Drawing.SystemColors.Window
        Me.Combo1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo1.Location = New System.Drawing.Point(468, 60)
        Me.Combo1.Name = "Combo1"
        Me.Combo1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo1.Size = New System.Drawing.Size(78, 22)
        Me.Combo1.TabIndex = 48
        Me.Combo1.Text = "158.75"
        '
        'Combo2
        '
        Me.Combo2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo2.BackColor = System.Drawing.SystemColors.Window
        Me.Combo2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo2.Location = New System.Drawing.Point(468, 103)
        Me.Combo2.Name = "Combo2"
        Me.Combo2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo2.Size = New System.Drawing.Size(78, 22)
        Me.Combo2.TabIndex = 47
        '
        'Combo3
        '
        Me.Combo3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo3.BackColor = System.Drawing.SystemColors.Window
        Me.Combo3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo3.Location = New System.Drawing.Point(468, 152)
        Me.Combo3.Name = "Combo3"
        Me.Combo3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo3.Size = New System.Drawing.Size(78, 22)
        Me.Combo3.TabIndex = 46
        '
        'Combo4
        '
        Me.Combo4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo4.BackColor = System.Drawing.SystemColors.Window
        Me.Combo4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo4.Location = New System.Drawing.Point(468, 197)
        Me.Combo4.Name = "Combo4"
        Me.Combo4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo4.Size = New System.Drawing.Size(78, 22)
        Me.Combo4.TabIndex = 45
        Me.Combo4.Text = "127"
        '
        'Combo5
        '
        Me.Combo5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo5.BackColor = System.Drawing.SystemColors.Window
        Me.Combo5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo5.Location = New System.Drawing.Point(468, 241)
        Me.Combo5.Name = "Combo5"
        Me.Combo5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo5.Size = New System.Drawing.Size(78, 22)
        Me.Combo5.TabIndex = 44
        '
        'Combo7
        '
        Me.Combo7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo7.BackColor = System.Drawing.SystemColors.Window
        Me.Combo7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo7.Location = New System.Drawing.Point(576, 106)
        Me.Combo7.Name = "Combo7"
        Me.Combo7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo7.Size = New System.Drawing.Size(68, 22)
        Me.Combo7.TabIndex = 43
        '
        'Combo8
        '
        Me.Combo8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo8.BackColor = System.Drawing.SystemColors.Window
        Me.Combo8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo8.Location = New System.Drawing.Point(576, 151)
        Me.Combo8.Name = "Combo8"
        Me.Combo8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo8.Size = New System.Drawing.Size(68, 22)
        Me.Combo8.TabIndex = 42
        '
        'Combo9
        '
        Me.Combo9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo9.BackColor = System.Drawing.SystemColors.Window
        Me.Combo9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo9.Location = New System.Drawing.Point(576, 196)
        Me.Combo9.Name = "Combo9"
        Me.Combo9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo9.Size = New System.Drawing.Size(68, 22)
        Me.Combo9.TabIndex = 41
        Me.Combo9.Text = "108.6"
        '
        'Combo10
        '
        Me.Combo10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo10.BackColor = System.Drawing.SystemColors.Window
        Me.Combo10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo10.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo10.Location = New System.Drawing.Point(576, 241)
        Me.Combo10.Name = "Combo10"
        Me.Combo10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo10.Size = New System.Drawing.Size(68, 22)
        Me.Combo10.TabIndex = 40
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text1.AutoSize = False
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(670, 62)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(68, 24)
        Me.Text1.TabIndex = 39
        Me.Text1.Text = "129"
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text2.AutoSize = False
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(670, 108)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(68, 23)
        Me.Text2.TabIndex = 38
        Me.Text2.Text = ""
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text3.AutoSize = False
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(670, 152)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(68, 24)
        Me.Text3.TabIndex = 37
        Me.Text3.Text = ""
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text4.AutoSize = False
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(670, 197)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(68, 24)
        Me.Text4.TabIndex = 36
        Me.Text4.Text = "2871"
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Combo6
        '
        Me.Combo6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo6.BackColor = System.Drawing.SystemColors.Window
        Me.Combo6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo6.Location = New System.Drawing.Point(576, 60)
        Me.Combo6.Name = "Combo6"
        Me.Combo6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo6.Size = New System.Drawing.Size(68, 22)
        Me.Combo6.TabIndex = 34
        Me.Combo6.Text = "71.44"
        '
        'Combo11
        '
        Me.Combo11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo11.BackColor = System.Drawing.SystemColors.Window
        Me.Combo11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Combo11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Combo11.Location = New System.Drawing.Point(242, 168)
        Me.Combo11.Name = "Combo11"
        Me.Combo11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Combo11.Size = New System.Drawing.Size(69, 22)
        Me.Combo11.TabIndex = 5
        Me.Combo11.Text = "224.4"
        '
        'Text11
        '
        Me.Text11.AcceptsReturn = True
        Me.Text11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text11.AutoSize = False
        Me.Text11.BackColor = System.Drawing.SystemColors.Window
        Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text11.Location = New System.Drawing.Point(242, 237)
        Me.Text11.MaxLength = 0
        Me.Text11.Name = "Text11"
        Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text11.Size = New System.Drawing.Size(69, 24)
        Me.Text11.TabIndex = 7
        Me.Text11.Text = "80"
        Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text10
        '
        Me.Text10.AcceptsReturn = True
        Me.Text10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text10.AutoSize = False
        Me.Text10.BackColor = System.Drawing.SystemColors.Window
        Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text10.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text10.Location = New System.Drawing.Point(242, 202)
        Me.Text10.MaxLength = 0
        Me.Text10.Name = "Text10"
        Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text10.Size = New System.Drawing.Size(69, 24)
        Me.Text10.TabIndex = 6
        Me.Text10.Text = "1500"
        Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text8
        '
        Me.Text8.AcceptsReturn = True
        Me.Text8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text8.AutoSize = False
        Me.Text8.BackColor = System.Drawing.SystemColors.Window
        Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text8.Location = New System.Drawing.Point(242, 99)
        Me.Text8.MaxLength = 0
        Me.Text8.Name = "Text8"
        Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text8.Size = New System.Drawing.Size(69, 24)
        Me.Text8.TabIndex = 3
        Me.Text8.Text = "215.9"
        Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text7
        '
        Me.Text7.AcceptsReturn = True
        Me.Text7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text7.AutoSize = False
        Me.Text7.BackColor = System.Drawing.SystemColors.Window
        Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text7.Location = New System.Drawing.Point(242, 65)
        Me.Text7.MaxLength = 0
        Me.Text7.Name = "Text7"
        Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text7.Size = New System.Drawing.Size(69, 23)
        Me.Text7.TabIndex = 2
        Me.Text7.Text = "30"
        Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Text6
        '
        Me.Text6.AcceptsReturn = True
        Me.Text6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text6.AutoSize = False
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.Location = New System.Drawing.Point(242, 30)
        Me.Text6.MaxLength = 0
        Me.Text6.Name = "Text6"
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.Size = New System.Drawing.Size(69, 20)
        Me.Text6.TabIndex = 1
        Me.Text6.Text = "3000"
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(343, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(126, 18)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "1# 钻铤"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(343, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(126, 18)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "2# 钻铤"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(343, 159)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(126, 19)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "3# 钻铤"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(343, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(126, 18)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "1# 钻杆"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(343, 250)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(126, 18)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "2# 钻杆"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(70, 246)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(164, 18)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "地面罐钻井液体积(m^3)"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(50, 211)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(184, 18)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "套管下深(m)"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(70, 177)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(164, 18)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "套管内径(mm)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(70, 108)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(164, 18)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "钻头直径(mm)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(70, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(164, 19)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "泥浆泵排量(l/S)"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(70, 39)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(164, 18)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "井深(m)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(672, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(68, 18)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "长度(m)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(583, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(69, 18)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "内径(mm)"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(487, 28)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(69, 18)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "外径(mm)"
        '
        'picOilGasOutflow
        '
        Me.picOilGasOutflow.Image = CType(resources.GetObject("picOilGasOutflow.Image"), System.Drawing.Image)
        Me.picOilGasOutflow.Location = New System.Drawing.Point(167, 482)
        Me.picOilGasOutflow.Name = "picOilGasOutflow"
        Me.picOilGasOutflow.Size = New System.Drawing.Size(559, 91)
        Me.picOilGasOutflow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picOilGasOutflow.TabIndex = 58
        Me.picOilGasOutflow.TabStop = False
        '
        'Form5
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.cmdExit
        Me.ClientSize = New System.Drawing.Size(893, 592)
        Me.Controls.Add(Me.picOilGasOutflow)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdok)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 8)
        Me.Name = "Form5"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "钻井液循环周计算"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
#Region "升级支持"
	Private Shared m_vb6FormDefInstance As Form5
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As Form5
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New Form5()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim Tw1 As Single 'Tw钻铤外径 Tn钻铤内径 Tc钻铤长度
	Dim Tw2 As Single
	Dim Tw3 As Single
	Dim Gw1 As Single ' Gw钻杆外径 Gn钻杆内径 Gc钻杆长度
	Dim Gw2 As Single
	Dim Tn1 As Single
	Dim Tn2 As Single
	Dim Tn3 As Single
	Dim Gn1 As Single
	Dim Gn2 As Single
	Dim Tc1 As Single
	Dim Tc2 As Single
	Dim Tc3 As Single
	Dim Gc1 As Single
	Dim Gc2 As Single
	Dim Hwell As Single '井深
	Dim PumpQ As Single '泵排量
	Dim Bitd As Single '钻头直径
	Dim Kwell As Single '井径扩大率
	Dim Tgn As Single '套管内径
	Dim Tgc As Single '套管长度
	Dim Vfield As Single '地面钻井液体积
	Dim hourzhong As Short '总循环时间
	Dim hourshang As Short '上返时间
	Dim hourxia As Short '下行时间
	Dim Vhuan As Single '环空体积
	Dim Vnei As Single '钻具水眼体积
	Dim Vzong As Single '总钻井液循环体积
	Dim Vw As Single '钻具外径体积
	Dim Vk As Single '空井体积
	Dim Vm As Single '井内钻井液体积
	Const pi As Double = 3.1415926
	
	'2012年3月29日填加的屏幕截图----------
	
	
	Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)
	Private Const KEYEVENTF_KEYUP As Short = &H2s
	'*******************************************
	
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo1.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo1.SelectedIndexChanged
		Tw1 = Val(Combo1.Text)
	End Sub
	
	Private Sub Combo1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw1 = Val(Combo1.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo10.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo10_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo10.SelectedIndexChanged
		Gn2 = Val(Combo10.Text)
		Gc2 = Hwell - Tc1 - Tc2 - Tc3 - Gc1
		Text5.Text = CStr(Gc2)
	End Sub
	
	Private Sub Combo10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gn2 = Val(Combo10.Text)
		Gc2 = Hwell - Tc1 - Tc2 - Tc3 - Gc1
		Text5.Text = CStr(Gc2)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo11.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo11_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo11.SelectedIndexChanged
		Tgn = Val(Combo11.Text)
	End Sub
	
	Private Sub Combo11_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo11.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tgn = Val(Combo11.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo2.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo2.SelectedIndexChanged
		Tw2 = Val(Combo2.Text)
	End Sub
	
	Private Sub Combo2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw2 = Val(Combo2.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo3.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo3.SelectedIndexChanged
		Tw3 = Val(Combo3.Text)
	End Sub
	
	Private Sub Combo3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tw3 = Val(Combo3.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo4.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo4.SelectedIndexChanged
		Gw1 = Val(Combo4.Text)
	End Sub
	
	Private Sub Combo4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw1 = Val(Combo4.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo5.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo5_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo5.SelectedIndexChanged
		Gw2 = Val(Combo5.Text)
	End Sub
	
	Private Sub Combo5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gw2 = Val(Combo5.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo6.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo6_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo6.SelectedIndexChanged
		Tn1 = Val(Combo6.Text)
	End Sub
	
	Private Sub Combo6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn1 = Val(Combo6.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo7.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo7_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo7.SelectedIndexChanged
		Tn2 = Val(Combo7.Text)
	End Sub
	
	Private Sub Combo7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn2 = Val(Combo7.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo8.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo8_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo8.SelectedIndexChanged
		Tn3 = Val(Combo8.Text)
	End Sub
	
	Private Sub Combo8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tn3 = Val(Combo8.Text)
	End Sub
	
	'UPGRADE_WARNING: 初始化窗体时可能激发事件 Combo9.SelectedIndexChanged。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"”
	Private Sub Combo9_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Combo9.SelectedIndexChanged
		Gn1 = Val(Combo9.Text)
	End Sub
	
	Private Sub Combo9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Combo9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gn1 = Val(Combo9.Text)
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Hwell = Val(Text6.Text)
		If Hwell <= 0 Then
			MsgBox("Please check that you have entered the 'Depth' data!", MsgBoxStyle.Information, "Mud Circulation times")
			Text6.Text = ""
			Exit Sub
		End If
		
		PumpQ = Val(Text7.Text)
		If PumpQ <= 0 Then
			MsgBox("Please check that you have entered the 'pump flow rate' data!", MsgBoxStyle.Information, "Mud Circulation times")
			Text7.Text = ""
			Exit Sub
		End If
		
		Kwell = Val(Text9.Text)
		If Kwell < 1 Or Kwell >= 2 Then
			MsgBox("Please check that you have entered the 'Hole diameter enlargement rate' data!", MsgBoxStyle.Information, "Mud Circulation times")
			Text9.Text = ""
			Exit Sub
		End If
		
		Tw1 = Val(Combo1.Text)
		Gn2 = Val(Combo10.Text)
		Tgn = Val(Combo11.Text)
		Tw2 = Val(Combo2.Text)
		Tw3 = Val(Combo3.Text)
		Gw1 = Val(Combo4.Text)
		Gw2 = Val(Combo5.Text)
		Tn1 = Val(Combo6.Text)
		Tn2 = Val(Combo7.Text)
		Tn3 = Val(Combo8.Text)
		Gn1 = Val(Combo9.Text)
		Tc1 = Val(Text1.Text)
		Tgc = Val(Text10.Text)
		Vfield = Val(Text11.Text)
		Tc2 = Val(Text2.Text)
		Tc3 = Val(Text3.Text)
		Gc1 = Val(Text4.Text)
		Gc2 = Val(Text5.Text)
		Bitd = Val(Text8.Text)
		
		Vnei = 0.25 * pi * (Tn1 / 1000) ^ 2 * Tc1 + 0.25 * pi * (Tn2 / 1000) ^ 2 * Tc2 + 0.25 * pi * (Tn3 / 1000) ^ 2 * Tc3 + 0.25 * pi * (Gn1 / 1000) ^ 2 * Gc1 + 0.25 * pi * (Gn2 / 1000) ^ 2 * Gc2
		Vk = 0.25 * pi * (Tgn / 1000) ^ 2 * Tgc + 0.25 * pi * ((Bitd * Kwell) / 1000) ^ 2 * (Hwell - Tgc)
		Vw = 0.25 * pi * (Tw1 / 1000) ^ 2 * Tc1 + 0.25 * pi * (Tw2 / 1000) ^ 2 * Tc2 + 0.25 * pi * (Tw3 / 1000) ^ 2 * Tc3 + 0.25 * pi * (Gw1 / 1000) ^ 2 * Gc1 + 0.25 * pi * (Gw2 / 1000) ^ 2 * Gc2
		Vhuan = Vk - Vw
		Vm = Vnei + Vhuan
		Vzong = Vhuan + Vnei + Vfield
		
		If PumpQ > 0 Then
			hourzhong = (Vhuan + Vnei + Vfield) / (PumpQ * 60 / 1000)
			hourxia = Vnei / (PumpQ * 60 / 1000)
			hourshang = Vhuan / (PumpQ * 60 / 1000)
			Text12.Text = CStr(hourzhong)
			Text13.Text = CStr(hourshang)
			Text14.Text = CStr(hourxia)
			Text15.Text = VB6.Format(Vnei + Vhuan, "00.0")
			
			Text16.Text = VB6.Format(Vzong, "00.0")
			Text17.Text = VB6.Format(Vhuan, "00.0")
			Text18.Text = VB6.Format(Vnei, "00.0")
			Text19.Text = VB6.Format(Vw - Vnei, "#0.0")
		Else
			MsgBox("Please check that you have entered the 'pump flow rate' data!")
			Me.Close()
			'UPGRADE_ISSUE: 不支持 Load 语句。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1039"”
            Dim Form5 As New Form5

		End If
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		Me.Close()
	End Sub
	
	Private Sub Form5_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Combo1.Items.Add("79.4") '1#钻铤外径
		Combo1.Items.Add("88.9")
		Combo1.Items.Add("104.78")
		Combo1.Items.Add("120.65")
		Combo1.Items.Add("127.00")
		Combo1.Items.Add("146.05")
		Combo1.Items.Add("152.40")
		Combo1.Items.Add("158.75")
		Combo1.Items.Add("165.10")
		Combo1.Items.Add("171.45")
		Combo1.Items.Add("177.80")
		Combo1.Items.Add("184.15")
		Combo1.Items.Add("196.85")
		Combo1.Items.Add("203.20")
		Combo1.Items.Add("209.6")
		Combo1.Items.Add("228.60")
		Combo1.Items.Add("241.3")
		Combo1.Items.Add("247.7")
		Combo1.Items.Add("254.0")
		Combo1.Items.Add("279.4")
		Combo2.Items.Add("79.4") '2#钻铤外径
		Combo2.Items.Add("88.9")
		Combo2.Items.Add("104.78")
		Combo2.Items.Add("120.65")
		Combo2.Items.Add("127.00")
		Combo2.Items.Add("146.05")
		Combo2.Items.Add("152.40")
		Combo2.Items.Add("158.75")
		Combo2.Items.Add("165.10")
		Combo2.Items.Add("171.45")
		Combo2.Items.Add("177.80")
		Combo2.Items.Add("184.15")
		Combo2.Items.Add("196.85")
		Combo2.Items.Add("203.20")
		Combo2.Items.Add("209.6")
		Combo2.Items.Add("228.60")
		Combo2.Items.Add("241.3")
		Combo2.Items.Add("247.7")
		Combo2.Items.Add("254.0")
		Combo2.Items.Add("279.4")
		Combo3.Items.Add("79.4") '3#钻铤外径
		Combo3.Items.Add("88.9")
		Combo3.Items.Add("104.78")
		Combo3.Items.Add("120.65")
		Combo3.Items.Add("127.00")
		Combo3.Items.Add("146.05")
		Combo3.Items.Add("152.40")
		Combo3.Items.Add("158.75")
		Combo3.Items.Add("165.10")
		Combo3.Items.Add("171.45")
		Combo3.Items.Add("177.80")
		Combo3.Items.Add("184.15")
		Combo3.Items.Add("196.85")
		Combo3.Items.Add("203.20")
		Combo3.Items.Add("209.6")
		Combo3.Items.Add("228.60")
		Combo3.Items.Add("241.3")
		Combo3.Items.Add("247.7")
		Combo3.Items.Add("254.0")
		Combo3.Items.Add("279.4")
		Combo4.Items.Add("60.324") '1#钻杆外径
		Combo4.Items.Add("73.024")
		Combo4.Items.Add("88.90")
		Combo4.Items.Add("101.60")
		Combo4.Items.Add("114.30")
		Combo4.Items.Add("118.60")
		Combo4.Items.Add("127.00")
		Combo4.Items.Add("139.70")
		Combo4.Items.Add("168.27")
		Combo5.Items.Add("60.324") '2#钻杆外径
		Combo5.Items.Add("73.024")
		Combo5.Items.Add("88.90")
		Combo5.Items.Add("101.60")
		Combo5.Items.Add("114.30")
		Combo5.Items.Add("118.60")
		Combo5.Items.Add("127.00")
		Combo5.Items.Add("139.70")
		Combo5.Items.Add("168.27")
		Combo6.Items.Add("38.1") '1#钻铤内径
		Combo6.Items.Add("50.8")
		Combo6.Items.Add("57.15")
		Combo6.Items.Add("71.44")
		Combo6.Items.Add("76.20")
		Combo7.Items.Add("38.1") '2#钻铤内径
		Combo7.Items.Add("50.8")
		Combo7.Items.Add("57.15")
		Combo7.Items.Add("71.44")
		Combo7.Items.Add("76.20")
		Combo8.Items.Add("38.1") '3#钻铤内径
		Combo8.Items.Add("50.8")
		Combo8.Items.Add("57.15")
		Combo8.Items.Add("71.44")
		Combo8.Items.Add("76.20")
		Combo9.Items.Add("46.10") '1#钻杆内径
		Combo9.Items.Add("50.7")
		Combo9.Items.Add("50.8")
		Combo9.Items.Add("54.64")
		Combo9.Items.Add("62.00")
		Combo9.Items.Add("63.50")
		Combo9.Items.Add("66.09")
		Combo9.Items.Add("70.21")
		Combo9.Items.Add("71.40")
		Combo9.Items.Add("76.00")
		Combo9.Items.Add("76.20")
		Combo9.Items.Add("84.84")
		Combo9.Items.Add("88.29")
		Combo9.Items.Add("92.46")
		Combo9.Items.Add("95.18")
		Combo9.Items.Add("95.35")
		Combo9.Items.Add("100.53")
		Combo9.Items.Add("108.61")
		Combo9.Items.Add("111.96")
		Combo9.Items.Add("118.62")
		Combo9.Items.Add("121.36")
		Combo9.Items.Add("151.51") '1#钻杆内径
		Combo10.Items.Add("46.10") '2#钻杆内径
		Combo10.Items.Add("50.7")
		Combo10.Items.Add("50.8")
		Combo10.Items.Add("54.64")
		Combo10.Items.Add("62.00")
		Combo10.Items.Add("63.50")
		Combo10.Items.Add("66.09")
		Combo10.Items.Add("70.21")
		Combo10.Items.Add("71.40")
		Combo10.Items.Add("76.00")
		Combo10.Items.Add("76.20")
		Combo10.Items.Add("84.84")
		Combo10.Items.Add("88.29")
		Combo10.Items.Add("92.46")
		Combo10.Items.Add("95.18")
		Combo10.Items.Add("95.35")
		Combo10.Items.Add("100.53")
		Combo10.Items.Add("108.61")
		Combo10.Items.Add("111.96")
		Combo10.Items.Add("118.62")
		Combo10.Items.Add("121.36")
		Combo10.Items.Add("151.51") '2#钻杆内径
		Combo11.Items.Add("99.6") '套管内径输入
		Combo11.Items.Add("101.6")
		Combo11.Items.Add("105.9")
		Combo11.Items.Add("108.6")
		Combo11.Items.Add("112.0")
		Combo11.Items.Add("118.6")
		Combo11.Items.Add("121.4")
		Combo11.Items.Add("124.3")
		Combo11.Items.Add("144.1")
		Combo11.Items.Add("147.1")
		Combo11.Items.Add("150.4")
		Combo11.Items.Add("159.4")
		Combo11.Items.Add("165.1")
		Combo11.Items.Add("168.3")
		Combo11.Items.Add("171.8")
		Combo11.Items.Add("193.7")
		Combo11.Items.Add("198.8")
		Combo11.Items.Add("201.2")
		Combo11.Items.Add("224.4")
		Combo11.Items.Add("252.7")
		Combo11.Items.Add("273.6")
		Combo11.Items.Add("313.6")
		Combo11.Items.Add("315.3")
		Combo11.Items.Add("317.9")
		Combo11.Items.Add("320.4")
		Combo11.Items.Add("381.3")
		Combo11.Items.Add("451.0")
		Combo11.Items.Add("482.6")

        '*********************************************************************
        '手工添加自ABOUT采集logo图标名,下列第一行语句。20220801
        '***********************************
        Me.PicLogo.SizeMode = frmAbout.DefInstance.picIcon.SizeMode.AutoSize
        Me.PicLogo.Image = frmAbout.DefInstance.picIcon.Image
        '******************************************************



        '************************************************
		'17 de Abril de 2012 Peru Talara 测试vb6.0中设置一张图片在所有的窗口显示成功
		'以下代码调用的为About中的公司logo 图片。
		
		
		'-------------------------------------------
        'Call PicGWDC(PicLogo) '调用的是模块内的过程，过程参数是控件类型
		
		'-------------------------------------------
	End Sub
	'测试屏幕截图的基本功能添加的命令函数，时间：2012年3月29日
	'****************************************************
	'***************************************************
	
	Private Sub PicLogo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PicLogo.DoubleClick
		'cmdclear.Visible = False         '点击截图后设置命令按钮隐藏
		cmdExit.Visible = False
		cmdok.Visible = False
		'cmdHelpSolid.Visible = False '
		System.Windows.Forms.Application.DoEvents() '
		
		
		
		
		
		keybd_event(18, 0, 0, 0)
		keybd_event(System.Windows.Forms.Keys.Snapshot, 0, 0, 0)
		System.Windows.Forms.Application.DoEvents()
		keybd_event(18, 0, KEYEVENTF_KEYUP, 0)
		'UPGRADE_ISSUE: Clipboard 方法 Clipboard.GetData 未升级。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"”
		'UPGRADE_WARNING: SavePicture 已升级到 System.Drawing.Image.Save 并具有新行为。 单击以获得更多信息:“ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"”
        'Clipboard.GetData.Save("MCirTime.bmp")

        'https://oomake.com/question/4484292  来源编码   王朝 20210930

        If Not System.Windows.Forms.Clipboard.GetDataObject() Is Nothing Then
            Dim oDataObj As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()
            If oDataObj.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then
                Dim oImgObj As System.Drawing.Image = oDataObj.GetData(DataFormats.Bitmap, True)
                'To Save as Bitmap
                oImgObj.Save("MCirTime.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
                'To Save as Jpeg
                'oImgObj.Save("d:\Test\test.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
                'To Save as Gif
                'oImgObj.Save("c:\Test.gif", System.Drawing.Imaging.ImageFormat.Gif)
            End If
        End If


        'cmdclear.Visible = True '
        'cmdPicSave.Visible = True        此控件已经作废，为了其它窗口的原因暂时未删除
        cmdExit.Visible = True
        cmdok.Visible = True
        'cmdHelpSolid.Visible = True
	End Sub
	Private Sub Text1_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text1.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc1 = Val(Text1.Text)
	End Sub
	
	Private Sub Text10_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text10.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tgc = Val(Text10.Text)
	End Sub
	
	Private Sub Text11_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text11.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Vfield = Val(Text11.Text)
	End Sub
	
	Private Sub Text2_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text2.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc2 = Val(Text2.Text)
	End Sub
	
	Private Sub Text3_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text3.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Tc3 = Val(Text3.Text)
	End Sub
	
	Private Sub Text4_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text4.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gc1 = Val(Text4.Text)
	End Sub
	
	Private Sub Text5_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Text5.Click
		Gc2 = Hwell - (Tc1 + Tc2 + Tc3 + Gc1)
	End Sub
	
	Private Sub Text5_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text5.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Gc2 = Val(Text5.Text)
		'If Tc1 + Tc2 + Tc3 + Gc1 + Gc2 <> Hwell Then
		'MsgBox "输入钻具长度不等于井深，请用鼠标左键点击“确定”键，用键盘重新输入数据。"
		'End If
	End Sub
	
	Private Sub Text6_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text6.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Hwell = Val(Text6.Text)
	End Sub
	
	Private Sub Text7_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text7.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		PumpQ = Val(Text7.Text)
		If PumpQ <= 0 Then
			MsgBox("Please check that you have entered the 'pump flow rate' data!", MsgBoxStyle.Information, "Mud Circulation times")
			Text7.Text = ""
			Exit Sub
		End If
	End Sub
	
	Private Sub Text8_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text8.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Bitd = Val(Text8.Text)
	End Sub
	
	Private Sub Text9_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles Text9.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		Kwell = Val(Text9.Text)
		If Kwell < 1 Or Kwell >= 2 Then
			MsgBox("Please check that you have entered the 'Hole diameter enlargement rate' data!", MsgBoxStyle.Information, "Mud Circulation times")
			Text9.Text = ""
			Exit Sub
		End If
	End Sub
End Class